//
//  PickerViewContainerView.swift
//  Nourish
//
//  Created by Nova on 11/14/16.
//  Copyright © 2016 SSIC. All rights reserved.
//

import UIKit

class PickerViewContainerView: UIView, NRPickerViewDelegate, NRPickerViewDataSource {

    var picker : NRPickerView?
    var viewController : MealDiaryViewController?
    
    var pickerDates : [Date] {
        get {
            return self.prepareDates()
        }
    }
    
    var formatter : DateFormatter {
        get {
            let formatter = DateFormatter()
            formatter.dateFormat = "EE| d"
            return formatter
        }
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
    }
    
    func setupPicker() {
        picker = NRChatUtility.horizontalPickerView()
        self.addSubview(picker!)
        picker?.pickerViewStyle = .wheel
        picker?.delegate = self
        picker?.dataSource = self
        picker?.maskDisabled = true
        picker?.pickerViewStyle = .flat
        picker?.font = UIFont(name: "HelveticaNeue-Light", size: 20)!
        picker?.highlightedFont = UIFont(name: "HelveticaNeue-Light", size: 20)!
        picker?.reloadData()
        
    }
    
    func prepareDates() -> [Date] {
        var proxyDate = Date()
        var array : [Date]? = []
        for _ in 0...30 {
            array?.insert(proxyDate, at: 0)
            proxyDate = Calendar.current.date(byAdding: .day, value: -1, to: proxyDate)!
        }
        return array!
    }
    
    // MARK: Picker Delegates and data source
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
    }
    
    func numberOfItemsInPickerView(_ pickerView: NRPickerView) -> Int {
        return pickerDates.count
    }
    
    func pickerView(_ pickerView: NRPickerView, titleForItem item: Int) -> String {
        return formatter.string(from: pickerDates[item])
    }
    
    func pickerView(_ pickerView: NRPickerView, didSelectItem item: Int) {
        let date = self.pickerDates[item]
        viewController?.tabledView?.presenter.showDataFor(date: date)
        viewController?.dateToFetch = date
    }
    
    func selectIndexFor(date : Date) {
        var index = 0
        for tmpIndex in 0...self.pickerDates.count - 1 {
            let dateToCompare = self.pickerDates[tmpIndex]
            if Calendar.current.startOfDay(for: date) == Calendar.current.startOfDay(for: dateToCompare) {
                index = tmpIndex
                break
            }
        }
        
        picker?.selectItem(index)
    }

}

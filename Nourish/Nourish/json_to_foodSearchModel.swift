//
//  json_to_foodSearchModel.swift
//  Nourish
//
//  Created by Gena Mironchyk on 8/15/17.
//  Copyright © 2017 SSIC. All rights reserved.
//

import UIKit

class json_to_foodSearchModel: NSObject {
    func transform(dict : [String : Any]) -> foodSearchModel {
        var model = foodSearchModel()
        model.amount = dict["amount"] as? Double ?? 0.0
        let calories = dict["calories"] as? Double ?? 0.0
        model.calories = String(format:"%.2f",calories)
        model.unit = dict["unit"] as? String ?? ""
        model.grams = dict["grams"] as? Double
        model.foodId = dict["id"] as? String ?? ""
        model.mealIdString = dict["id"] as? String ?? ""
        model.foodTitle = dict["name"] as? String ?? ""
        
        if dict["user_photo_id"] != nil {
            model.userPhotoId = dict["user_photo_id"] as? String
        }
    return model
    }
}

//
//  NRFoodSearchInteractor.swift
//  Nourish
//
//  Created by Vino (Vinodha) Sundaramoorthy on 11/16/16.
//  Copyright © 2016 SSIC. All rights reserved.
//

import UIKit
import RxSwift
//import FoodDatabaseKit
import AWSCore
import AWSLambda

class NRFoodSearchInteractor: NSObject {
    let repo = NoomRepository.shared
    let userRepo = UserRepository.shared
    let favRepo = FoodRecordRepository.shared
    let fnddsRepo = AmazonLamdaManager()
    let favCache = FavoritesRepository()
    
    func getFrequentMealsForOcassion(ocassion:Ocasion , showPrediction:Bool) -> Observable<[MealRecordModel]> {
        return Observable.create { observer in
            self.userRepo.getCurrentUserID().subscribe(onNext: { [unowned self] userId in
                AmazonLamdaManager.getCreds()
                var mealRecordArray : [MealRecordModel] = []
                
                if showPrediction {
                    let invocationRequest = AWSLambdaInvokerInvocationRequest()
                    invocationRequest?.functionName = kMealPredictionLambaFunction
                    invocationRequest?.payload = ["accessToken" : NRUserSession.sharedInstance.accessToken!, "userId" : userId,"occasion":ocassion.rawValue,"include_foodgroup_summary":"true"]
                
                    let lambdaInvoker = AWSLambdaInvoker.default()
                    lambdaInvoker.invoke(invocationRequest!) { (response, error) in
                        print("Error \(error.debugDescription)")
                        if response != nil {
                            mealRecordArray = Json_to_MealRecordModel.transform(dict: response?.payload as! [String : Any])
                            observer.onNext(mealRecordArray)
                            observer.onCompleted()
                        } else if error != nil {
                            observer.onNext(mealRecordArray)
                            observer.onCompleted()
                        }
                    }
                } else {
                        observer.onNext(mealRecordArray)
                        observer.onCompleted()
                }
            })
        }
    }
    func searchFNDDSFood(searchString : String) -> Observable<[foodSearchModel]> {
        let result = self.userRepo.getCurrentUser(policy: .Cached).flatMap { user in
            Observable.just(user.userID)
        }.flatMap { userID -> Observable<[foodSearchModel]> in
            let token = NRUserSession.sharedInstance.accessToken!
            return self.repo.loadEntriesFromFndds(search: searchString, token: token, userID: userID!)
        }
        return result
    }
    
    func getFavouriteFood() -> Observable<[MealRecordModel]> {
        return self.favCache.fetchFavoritesWithCache()
    }
    
    /*func searchFoodWith(searchString : String, ocasion : Ocasion) -> Observable<[NMFood]> {
        return self.repo.loadEntriesFrom(string: searchString, ocasion: ocasion)
    }
    
    func searchFoodWith(barcode : String, ocasion : Ocasion) -> Observable<[NMFood]> {
        return self.repo.loadEntriesFromBarcode(barcode: barcode, ocasion: ocasion)
    }*/
    
    func addToCountedFavourites(model : foodSearchModel) {
        self.favRepo.addToCounted(model: model)
    }
    
    func getCountedFavourites() -> Observable<[foodSearchModel]> {
        return self.favRepo.getCountedFavourites()
    }
    
    func getCountedFavoritesFor(ocasion : Ocasion) -> Observable<[foodSearchModel]> {
        let favs = self.getFavouriteFood()
        let counted = self.favRepo.getCountedFavoritesFor(ocasion: ocasion, count: 10, minimalCount: 2)
        
        let zip = Observable.zip(favs, counted, resultSelector: {favorites, frequents -> [foodSearchModel] in
            
            var results = [foodSearchModel]()
            let strings = self.foodIDsFrom(models: favorites)
            
            for (var object) in frequents {
                if object.foodId != "" && strings[object.foodId] != nil {
                    object.isFavorite = true
                    object.groupID = strings[object.foodId]
                    results.append(object)
                } else {
                    results.append(object)
                }
            }
            
            return results
        })
        
        return zip
    }
    
    private func foodIDsFrom(models : [MealRecordModel]) -> [String : String] {
        var strings = [String : String]()
        
        let singleFavs = models.filter {$0.groupedMealArray.count == 1}
        
        for object in singleFavs {
            let representationModel = object.groupedMealArray.first
            if representationModel != nil && representationModel?.idString != nil {
                strings[(representationModel?.idString)!] = object.groupID
            }
        }
        
        return strings
    }
    
}

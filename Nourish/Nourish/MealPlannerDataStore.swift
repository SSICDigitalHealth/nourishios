//
//  MealPlannerDataStore.swift
//  Nourish
//
//  Created by Gena Mironchyk on 9/12/17.
//  Copyright © 2017 SSIC. All rights reserved.
//

import UIKit
import RxSwift

let mealPlannerUrl = kNourishApiAddress + "recommendations/weekplan"
let localDay = "&local_day="
let dietaryPrefs = "&diet_pref="
class MealPlannerDataStore: BaseNestleDataStore {
    
    func getWeeklyPlan(token : String, userId: String, date : Date, dietaryPreference : String) -> Observable<[String : Any]> {
        return Observable.create{ observer in
            let url = self.getMealPlannerUrl(token: token, userId: userId, date : date, dietaryPreference : dietaryPreference)
            var request = self.requestWithToken(token: token, url: url)//URLRequest(url : url)
            request.httpMethod = "GET"
            let session = URLSession.shared.dataTask(with: request) {
                data, response, error in
                
                if error == nil && data != nil {
                    let dataStr = String(data : data!, encoding : .utf8)
                    if let dictionary = self.convertStringToDictionary(text: dataStr!) {
                        observer.onNext(dictionary)
                    }
                } else {
                    if error != nil {
                        observer.onError(error!)
                    }
                }
                self.log(foodProcessingOrWithings: false, url: url.absoluteString, requestMethod: request.httpMethod, urlResponse: response, requestBody: nil, error: error, responceBody: nil)
                observer.onCompleted()
            }
            session.resume()
            
            return Disposables.create {
                session.cancel()
            }

        }
    }
    
    private func addCalorieBudgetToUrl(url : String) -> String {
        if defaults.object(forKey: kCalorieBudgetKey) != nil {
            let calorieString = String(format: "&calorie_consumption_goal=%0.f", defaults.double(forKey: kCalorieBudgetKey))
            let authorizedUrl = url.appending(calorieString)
            return authorizedUrl
        } else {
            return url
        }
    }
    
    
    private func getMealPlannerUrl(token : String, userId : String, date : Date, dietaryPreference : String) -> URL {
        var stringUrl = mealPlannerUrl
        stringUrl = self.addAuthorizationHeader(url: stringUrl, token: token) + kNestleUserID + userId
        stringUrl = self.addLocalDay(stringUrl: stringUrl,local: localDay, date : date) + dietaryPrefs + dietaryPreference
        stringUrl = self.addCalorieBudgetToUrl(url: stringUrl)
        return URL(string:stringUrl)!
    }
    
    private func addLocalDay(stringUrl : String, local : String, date: Date) -> String {
        let url = stringUrl + local + self.getLocalDayString(date : date)
        return url
    }
    
    private func getLocalDayString(date : Date) -> String {
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyyMMdd"
        let localTime = formatter.string(from: date)
        return localTime
    }
    
    private func addAuthorizationHeader(url : String, token : String) -> String {
        let authorizedUrl = url.appending(kNestleToken + token)
        return authorizedUrl
    }
    
    private func convertStringToDictionary(text: String) -> [String : Any]? {
        if let data = text.data(using: String.Encoding.utf8) {
            do {
                return try JSONSerialization.jsonObject(with: data, options: []) as? [String : Any]
            } catch let error as NSError {
                LogUtility.logToFile(error)
            }
        }
        return nil
    }

}

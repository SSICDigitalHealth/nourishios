//
//  OptifastProductDetailViewController.swift
//  Nourish
//
//  Created by Gena Mironchyk on 10/20/17.
//  Copyright © 2017 SSIC. All rights reserved.
//

import UIKit

class OptifastProductDetailViewController: BasePresentationViewController {

    var optifastProductModel : foodSearchModel!
    var localSettingsDictionary : [String : Bool]!
    var settingsVC : OptifastSettingsViewController?
    @IBOutlet weak var detailsView : OptifastProductDetailsView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.baseViews = [self.detailsView]
        self.detailsView.viewDidLoad()
        self.detailsView.presenter.foodModel = self.optifastProductModel
        self.setupNavBar()
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        NavigationUtility.hideTabBar(animated: true)
    }
    
    private func setupNavBar() {
        let titleAttribute = self.navigationBarTitleAttribute()
        self.title = "OPTIFAST product"
        
        let selectedButton = UIButton(type: .custom)
        selectedButton.frame = CGRect(x: 0.0, y: 0.0, width: 24.0, height: 24.0)
        selectedButton.setImage(#imageLiteral(resourceName: "fav_icon_00-1"), for: .normal)
        selectedButton.addTarget(self, action: #selector(addOrDeleteFromPlan(sender:)), for: .touchUpInside)
        self.setupBarButton(sender: selectedButton)
        let button = UIBarButtonItem.init(customView: selectedButton)
        self.navigationItem.rightBarButtonItem = button
        self.navigationController?.navigationBar.barTintColor = UIColor.white
        self.navigationController?.navigationBar.tintColor = UIColor.gray
        self.navigationController?.navigationBar.titleTextAttributes = titleAttribute
    }
    
    override func willMove(toParentViewController parent: UIViewController?) {
        super.willMove(toParentViewController: parent)
        if parent == nil {
            if self.settingsVC != nil {
                self.settingsVC!.localSettingsDict = self.localSettingsDictionary
            }
        }
    }
    
    func backButtonTapped () {
        if self.settingsVC != nil {
            self.settingsVC!.localSettingsDict = self.localSettingsDictionary
        }
        self.navigationController?.popViewController(animated: true)
    }
    
    func addOrDeleteFromPlan(sender: UIButton) {
        let selected = self.localSettingsDictionary[self.optifastProductModel.foodId]!
        self.localSettingsDictionary[self.optifastProductModel.foodId] = !selected
        self.setupBarButton(sender: sender)
    }
    
    func setupBarButton(sender : UIButton) {
        if self.localSettingsDictionary[self.optifastProductModel.foodId] == true {
            sender.setImage(#imageLiteral(resourceName: "fav_green"), for: .normal)
        } else {
            sender.setImage(#imageLiteral(resourceName: "fav_icon_00-1"), for: .normal)
        }
    }
    

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

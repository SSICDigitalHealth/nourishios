//
//  OptifastMealPlannerChangeDishCell.swift
//  Optifast
//
//  Created by Gena Mironchyk on 1/11/18.
//  Copyright © 2018 SSIC. All rights reserved.
//

import UIKit

class OptifastMealPlannerChangeDishCell: UITableViewCell {

    @IBOutlet weak var selectedImageView : UIImageView!
    @IBOutlet weak var foodNameLabel : UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}

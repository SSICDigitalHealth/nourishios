//
//  ActivityWeightChartCellPresenter.swift
//  Nourish
//
//  Created by Andrey Zmushko on 9/7/17.
//  Copyright © 2017 SSIC. All rights reserved.
//

import UIKit
import RxSwift


class ActivityWeightChartCellPresenter: ActivityBaseCellPresenter, ActivityWeightChartDelegate {
    let interactor = WeightInteractor()
    let mapperObjectWeight = WeightModel_to_WeightViewModel()
    var weightViewModel = WeightViewModel()
    var userId = ""
    
    var epoch: (startDate: Date, endDate: Date)?
    
    var maxWeight = CGFloat()
    var minWeight = CGFloat()
    var avrWeight = CGFloat()
    
    private func getInformation(cell: ActivityWeightChartCell, startDate: Date, endDate: Date) {
        cell.baseView.viewWillAppear(true)
        cell.baseView.removeActivityIndicator()
        self.subscribtions.append(self.interactor.execute(startDate: startDate, endDate: endDate).observeOn(MainScheduler.instance).subscribe(onNext: { [unowned self] weightActive in
            self.weightViewModel = self.mapperObjectWeight.transform(model: weightActive)
        }, onError: {error in
            cell.baseView.parseError(error: error,completion: nil)
        }, onCompleted: {
            self.setupViewChart(cell: cell)
            cell.baseView.stopActivityAnimation()
        }, onDisposed: {
        }))
    }
    
    override func fetchModel(For basecell: ActivityBaseCell) {
        super.fetchModel(For: basecell)
        
        let cell = basecell as! ActivityWeightChartCell
        
        cell.iconImageView.imageNamedWithTint(named: "weight_icon", tintColor: UIColor.white)
        cell.iconFrame.backgroundColor = NRColorUtility.hexStringToUIColor(hex: "f0C517")
        
        cell.titleLabel.attributedText = NSMutableAttributedString(string: "Weight", attributes: cell.boldAttr)
        
        if let delegate = self.delegate {
            self.epoch = delegate.fetchEpoch()
            if let epoch = self.epoch {
                self.getInformation(cell: cell, startDate: epoch.startDate, endDate: epoch.endDate)
            }
        }
    }
    
    private func setupViewChart(cell: ActivityWeightChartCell) {
        let weightUnit = self.weightViewModel.isMetric ? " kg" : " lbs"
        
        if let weight = self.weightViewModel.weight {
            let weightValue = weight.filter{$0.value != nil}
            let weightStr = NSMutableAttributedString(string: "", attributes: cell.boldAttr)

            var average: CGFloat {
                let sum = weightValue.reduce(0.0, { (a, b) -> Double in
                    return a + (b.value ?? 0)
                })
                if weightValue.count > 0 {
                    return CGFloat(sum / Double(weightValue.count))
                } else {
                    return CGFloat(0)
                }
            }
            
            self.avrWeight = self.weightViewModel.isMetric ? average : CGFloat(NRConversionUtility.kiloToPounds(valueInKilo: Double(average)))
            
                if self.avrWeight == 0 {
                    weightStr.append(NSMutableAttributedString(string: "--", attributes: cell.bookAttr))
                    weightStr.append(NSMutableAttributedString(string: weightUnit, attributes: cell.bookAttr))
                    cell.chartHeight.constant = 0
                    cell.chartView.isHidden = true
                    self.reload()
                }
                else {
                    weightStr.append(NSMutableAttributedString(string: String(format: "%.0f", self.avrWeight), attributes: cell.boldAttr))
                    weightStr.append(NSMutableAttributedString(string: weightUnit, attributes: cell.bookAttr))
                }
                cell.valueLabel.attributedText = weightStr

            self.maxWeight = CGFloat(weightValue.max(by: {(a, b) -> Bool in
                return (a.value ?? 0) < (b.value ?? 0)
            })?.value ?? Double(self.avrWeight))
            self.minWeight = CGFloat(weightValue.max(by: {(a, b) -> Bool in
                return (a.value ?? 0) > (b.value ?? 0)
            })?.value ?? Double(self.avrWeight))
            
            cell.chartView.setupWith(dataSource: self)
        }
    }
    
    func numberOfBars(in ActivityWeightChart: ActivityWeightChart) -> Int {
        if let weight = self.weightViewModel.weight {
            return weight.count
        }
        return 0
    }
    
    func topLabel(in ActivityWeightChart: ActivityWeightChart) -> String {
        var max = self.maxWeight
        if !self.weightViewModel.isMetric {
            max = CGFloat(NRConversionUtility.kiloToPounds(valueInKilo: Double(max)))
        }
        if max <= self.avrWeight {
            max = self.avrWeight + 1
        }
        return String(format: "%.0f", max)
    }

    func bottomLabel(in ActivityWeightChart: ActivityWeightChart) -> String {
        var min = self.minWeight
        if !self.weightViewModel.isMetric {
            min = CGFloat(NRConversionUtility.kiloToPounds(valueInKilo: Double(min)))
        }
        if min >= self.avrWeight && self.avrWeight != 0.0 {
            min = self.avrWeight - 1
        }
        return String(format: "%.0f", min)
    }

    func middleAxisValue(in ActivityWeightChart: ActivityWeightChart) -> CGFloat {
        var min = self.minWeight
        var max = self.maxWeight
        let avr = self.avrWeight

        if !self.weightViewModel.isMetric {
            min = CGFloat(NRConversionUtility.kiloToPounds(valueInKilo: Double(min)))
        }

        if !self.weightViewModel.isMetric {
            max = CGFloat(NRConversionUtility.kiloToPounds(valueInKilo: Double(max)))
        }

        if min >= avr {
            min = avr - 1
        }
        
        if max <= avr {
            max = avr + 1
        }
        
        return CGFloat((avr - min) / (max - min))
    }

    func activityWeightChart(_ ActivityWeightChart: ActivityWeightChart, barAtIndex: Int) -> ActivityWeightChart.Bar? {
        var legenda: String {
            let formatter = DateFormatter()
            if let weight = self.weightViewModel.weight {
                formatter.dateFormat = {
                    if weight.count <= 7 {
                        if let epoch = self.epoch {
                            if Calendar.current.isDateInToday(epoch.endDate) == false {
                                return "E"
                            }
                        }
                    }
                    return "MM/dd"
                }()
                if weight.count <= 7 || (barAtIndex == 1 || barAtIndex == weight.count - 2) {
                    var index: Int {
                        if weight.count > 7 && barAtIndex == 1 {
                            return 0
                        }
                        else if weight.count > 7 && barAtIndex == weight.count - 2 {
                            return weight.count - 1
                        }
                        return barAtIndex
                    }
                    return formatter.string(from: weight[index].subtitle).uppercased()
                }
            }
            return ""
        }
        
        var weight: CGFloat {
            if let weight = self.weightViewModel.weight {
                if weight[barAtIndex].value == nil {
                    return CGFloat(-1)
                } else {
                    return CGFloat((CGFloat(weight[barAtIndex].value ?? 0.0)) / (self.maxWeight))
                }
            }
            return CGFloat()
        }

        return (weight, legenda)
    }

}

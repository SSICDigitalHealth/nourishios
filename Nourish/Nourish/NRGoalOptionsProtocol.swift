//
//  NRGoalOptionsProtocol.swift
//  Nourish
//
//  Created by Vino (Vinodha) Sundaramoorthy on 11/28/16.
//  Copyright © 2016 SSIC. All rights reserved.
//


protocol NRGoalOptionsProtocol {
    func loadOptions()
    func setUserGoal()
    func getGoal() -> UserGoal
}

//
//  ScoreBreakDownModel_to_ScoreBreakDownViewModel.swift
//  Nourish
//
//  Created by Vlad Birukov on 15.06.17.
//  Copyright © 2017 SSIC. All rights reserved.
//

import Foundation

class ScoreBreakDownModel_to_ScoreBreakDownViewModel {
    
    func transform(userModel : ScoreBreakDownModel) -> ScoreBreakDownViewModel {
        let scoreViewModel = ScoreBreakDownViewModel()
        
        scoreViewModel.scoreUser = userModel.scoreUser
        scoreViewModel.isDairyEmpty = userModel.isDairyEmpty
        
        scoreViewModel.historyScore = userModel.historyScore
        
        return scoreViewModel
    }
}

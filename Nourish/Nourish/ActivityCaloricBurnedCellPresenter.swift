//
//  ActivityCaloricBurnedCellPresenter.swift
//  Nourish
//
//  Created by Andrey Zmushko on 8/26/17.
//  Copyright © 2017 SSIC. All rights reserved.
//

import UIKit
import RxSwift


class ActivityCaloricBurnedCellPresenter: ActivityBaseCellPresenter {
    
    let interactor = CaloricBurnInteractor()
    let mapperObjectCaloricBurn = CaloricBurnedModel_to_CaloricBurnedViewModel()
    var caloricBurnedViewModel = CaloricBurnedViewModel()

    private func getInformation(cell: ActivityBaseTableViewCell, startDate: Date, endDate: Date) {
        cell.baseView.viewWillAppear(true)
        cell.baseView.removeActivityIndicator()
        self.subscribtions.append(self.interactor.execute(startDate: startDate, endDate: endDate).observeOn(MainScheduler.instance).subscribe(onNext: { [unowned self] caloricBurned in
            self.caloricBurnedViewModel = self.mapperObjectCaloricBurn.transform(model: caloricBurned)
        }, onError: {error in
            cell.baseView.parseError(error: error, completion: nil)
        }, onCompleted: {
            self.setupViewWithData(cell: cell)
            cell.baseView.stopActivityAnimation()
        }, onDisposed: {
        }))
    }
    
    
    
    override func fetchModel(For basecell: ActivityBaseCell) {
        super.fetchModel(For: basecell)
        
        
        let cell = basecell as! ActivityBaseTableViewCell
        
        cell.iconImageView.imageNamedWithTint(named: "calburned_icon", tintColor: UIColor.white)
        cell.iconFrame.backgroundColor = NRColorUtility.appBackgroundColor()
        
        cell.titleLabel.attributedText = NSMutableAttributedString(string: "Burned", attributes: cell.boldAttr)
        
        if let delegate = self.delegate {
            let date = delegate.fetchEpoch()
            self.getInformation(cell: cell, startDate: date.startDate, endDate: date.endDate)
        }

        cell.descriptionLabel.text = "Avg. burned/day:\nAvg. active/day:"
        cell.descriptionLabel.setLineSpacing(spacing: 8)
        cell.bottomGap.constant = 0
    }
    
    private func setupViewWithData(cell: ActivityBaseTableViewCell) {
        if let userVurnedCal = self.caloricBurnedViewModel.userBurnedCal {
            let value = NSMutableAttributedString(string: String(format: "%0.f", userVurnedCal), attributes: cell.boldAttr)
            value.append(NSMutableAttributedString(string: " kcal", attributes: cell.bookAttr))
            
            cell.valueLabel.attributedText = value

            if self.caloricBurnedViewModel.avgBurnedCal != nil && self.caloricBurnedViewModel.avgActiveCal != nil {
                cell.descriptionValueLabel.text = String(format: "%.0f kcal\n%.0f kcal", self.caloricBurnedViewModel.avgBurnedCal!, self.caloricBurnedViewModel.avgActiveCal!)
                cell.descriptionValueLabel.setLineSpacing(spacing: 8, alignment: .right)
            }
        }

    }
}



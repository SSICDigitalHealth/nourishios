//
//  ActivityStressCellPresenter.swift
//  Nourish
//
//  Created by Andrey Zmushko on 8/25/17.
//  Copyright © 2017 SSIC. All rights reserved.
//

import UIKit
import RxSwift


class ActivityStressCellPresenter: ActivityBaseCellPresenter {
    
    let interactor = StressActivityInteractor()
    let mapperObjectStress = StressModel_to_StressViewModel()
    var stressViewModel = StressViewModel()
    
    private func getInformation(cell: ActivityBaseTableViewCell, startDate: Date, endDate: Date) {
        cell.baseView.viewWillAppear(true)
        cell.baseView.removeActivityIndicator()
        self.subscribtions.append(self.interactor.execute(startDate: startDate, endDate: endDate).observeOn(MainScheduler.instance).subscribe(onNext: { stressActive in
           
                self.stressViewModel = self.mapperObjectStress.transform(model: stressActive)
        }, onError: {error in
            cell.baseView.parseError(error: error, completion: nil)
        }, onCompleted: {
                self.setupViewWithData(cell: cell)
                self.reloadTable(cell: cell)
                cell.baseView.stopActivityAnimation()
        }, onDisposed: {
        }))
    }

    override func fetchModel(For basecell: ActivityBaseCell) {
        super.fetchModel(For: basecell)
        
        let cell = basecell as! ActivityBaseTableViewCell
        if let delegate = self.delegate {
            let date = delegate.fetchEpoch()
        
            if (Calendar.current.isDateInToday(date.startDate)) && (Calendar.current.isDateInToday(date.endDate)) {
                cell.isUserInteractionEnabled = true
           }
        }
        
        cell.iconImageView.imageNamedWithTint(named: "heartrate_icon", tintColor: UIColor.white)
        cell.iconFrame.backgroundColor = NRColorUtility.hexStringToUIColor(hex: "009247")
        
        cell.titleLabel.attributedText = NSMutableAttributedString(string: "Stress", attributes: cell.boldAttr)
        
        cell.separatorView.isHidden = false
        cell.bottomGap.constant = 8
        
        if let delegate = self.delegate {
            let date = delegate.fetchEpoch()
            self.getInformation(cell: cell, startDate: date.startDate, endDate: date.endDate)
        }

    }
    
    private func setupViewWithData(cell: ActivityBaseTableViewCell) {
        if let stressValue = self.stressViewModel.currentBPM {
            let stressStr = NSMutableAttributedString(string:"", attributes: cell.boldAttr)
            stressStr.append(NSMutableAttributedString(string: String(format: "%.0f", stressValue), attributes: cell.boldAttr))
            stressStr.append(NSMutableAttributedString(string: " bpm", attributes: cell.bookAttr))
            
            cell.valueLabel.attributedText = stressStr
        }
        
        if isTrackStress(model: self.stressViewModel) {
            cell.descriptionLabel.text = "Max. heart rate:\nAvg. heart rate:\nMin. heart rate:"
            cell.descriptionLabel.setLineSpacing(spacing: 8)
            
            cell.descriptionValueLabel.text = String(format: "%.0f bpm\n%.0f bpm\n%.0f bpm", self.stressViewModel.maxBPM!, self.stressViewModel.avgBPM!, self.stressViewModel.minBPM!)
            cell.descriptionValueLabel.setLineSpacing(spacing: 8, alignment: .right)
            
            cell.bottomGap.constant = 16
        } else {
            if self.stressViewModel.currentBPM == nil && self.stressViewModel.avgBPM == nil && self.stressViewModel.maxBPM == nil && self.stressViewModel.minBPM == nil {
                cell.valueLabel.font = UIFont(name: "Campton-Book", size: 18)!
                cell.valueLabel.textColor = NRColorUtility.hexStringToUIColor(hex: "303030")
                cell.valueLabel.text = "-- bmp"
            }
        }
    }

    private func isTrackStress(model: StressViewModel) -> Bool {
        if  model.avgBPM != 0.0 && model.maxBPM != 0.0 && model.minBPM != 0.0 && model.avgBPM != nil && model.maxBPM != nil && model.minBPM != nil {
            return true
        } else {
            return false
        }
    }
}

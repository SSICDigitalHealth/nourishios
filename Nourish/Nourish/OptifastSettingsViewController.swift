//
//  OptifastSettingsViewController.swift
//  Nourish
//
//  Created by Gena Mironchyk on 10/17/17.
//  Copyright © 2017 SSIC. All rights reserved.
//

import UIKit

protocol OptifastSettingsViewControllerProtocol {
    func popToRootVC()
    func openOptifastFoodDetailsFor(model : foodSearchModel, localSettings : [String : Bool])
}


class OptifastSettingsViewController: BasePresentationViewController, OptifastSettingsViewControllerProtocol {
    
    @IBOutlet weak var optifastSettingsView : OptifastSettingsView!
    var localSettingsDict : [String : Bool]?
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupNavBar()
        self.baseViews = [self.optifastSettingsView]
        self.optifastSettingsView.viewDidLoad()
        self.optifastSettingsView.presenter.optifastVC = self
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.optifastSettingsView.presenter.localPickedDictionary = self.localSettingsDict
        super.viewWillAppear(animated)
        NavigationUtility.hideTabBar(animated: true)
        self.title = "OPTIFAST preferences"
        self.navigationController?.setNavigationBarHidden(false, animated: false)

    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.title = ""
    }
    
    func popToRootVC () {
        self.navigationController?.popToRootViewController(animated: true)
    }
    
    private func setupNavBar() {
        let titleAttribute = self.navigationBarTitleAttribute()
        
        self.navigationController?.navigationBar.barTintColor = UIColor.white
        self.navigationController?.navigationBar.tintColor = UIColor.gray
        self.navigationController?.navigationBar.titleTextAttributes = titleAttribute
    }
    
    func openOptifastFoodDetailsFor(model: foodSearchModel, localSettings : [String : Bool]) {
        let detailsVC = OptifastProductDetailViewController(nibName: "OptifastProductDetailViewController", bundle: nil)
        detailsVC.optifastProductModel = model
        detailsVC.localSettingsDictionary = localSettings
        detailsVC.settingsVC = self
        self.navigationController?.pushViewController(detailsVC, animated: true)
    }
}

//
//  HomeScreenInteractor.swift
//  Nourish
//
//  Created by Andrey Zmushko on 9/14/17.
//  Copyright © 2017 SSIC. All rights reserved.
//

import UIKit
import RxSwift

class HomeScreenInteractor {
    
    private let nestleProgressRepository = NestleProgressRepository.shared
    private let weightLossMeterInteractor = WeightLossMeterInteractor()
    private let userActivityInteractor = UserActivityInteractor()
    private let stressInteractor = StressInteractor()
    private let userRepository = UserRepository.shared
    private let newActivityRepository = NewActivityRepository.shared
    private let calendar = Calendar.current
    
    func execute() -> Observable <HomeScreenModel> {

        let weightProgressStruct = self.weightLossMeterInteractor.getRealWeighLossMeterModel()
        let progress = self.nestleProgressRepository.getProgressFor(startDate: Date(), endDate: Date(), returnHistory: false)
       // let activities = self.userActivityInteractor.userActivitiesForToday()
        let user = self.userRepository.getCurrentUser(policy: .Cached)
        let dates = self.datesFromDate(date: Date())
        let userSteps = self.newActivityRepository.stepsModelFrom(startDate: dates.0, endDate: dates.1)
        let stressData = self.stressInteractor.dailyStressData(period: .today)
        let sleepModelObs = self.newActivityRepository.sleepModel(startDate: self.calendar.startOfDay(for: Date()), endDate: self.endOfDay(Date()))
        return Observable.combineLatest(weightProgressStruct, progress, sleepModelObs, user, userSteps,stressData , resultSelector : { weightProgress, userProgress, sleepModel, userProfile, steps, stress -> HomeScreenModel in
            return self.createModelFrom(userProgress: userProgress, weightProgress: weightProgress, userProfile: userProfile, steps: steps.numberSteps ?? 0.0, sleepModel: sleepModel, stress: stress )
        })
    }
    
    
    private func createModelFrom(userProgress : Progress, weightProgress : weightLossMeter, userProfile : UserProfile, steps : Double, sleepModel : SleepModel, stress : [DailyStress]) -> HomeScreenModel {
        let model = HomeScreenModel()
        
        model.score = userProgress.userScore.scoreActivity
        model.fruits = self.totalValueFrom(progress: userProgress, key: "Fruit_total")
        model.dairy = self.totalValueFrom(progress: userProgress, key: "Dairy_total")
        model.vegetable = self.totalValueFrom(progress: userProgress, key: "Veg_total")
        model.protein = self.totalValueFrom(progress: userProgress, key: "Prot_total")
        model.grains = self.totalValueFrom(progress: userProgress, key: "Grain_total")
        model.consumed = weightProgress.userConsumedCal/weightProgress.maxConsumedCal
        model.burned = weightProgress.userBurnedCal/weightProgress.maxBurnedCal
        
        model.activity = self.stepsValueFrom(userProfile: userProfile, userSteps: steps, calorieModel: weightProgress)
        model.sleep = self.sleepCoeffFrom(sleepModel: sleepModel)
        model.stress = stress.first?.stressValue
        return model
    }
    
    
    private func sleepCoeffFrom(sleepModel : SleepModel) -> Double{
        let coef = (sleepModel.sleepHours ?? 0.0) / 8.0
        return coef
    }
    
    private func endOfDay(_ date: Date) -> Date {
        var components = DateComponents()
        components.day = 1
        components.second = -1
        return self.calendar.date(byAdding: components, to: startOfDay(date)) ?? date
    }
    
    private func stepsValueFrom(userProfile : UserProfile, userSteps : Double, calorieModel : weightLossMeter) -> Double {
        var goalSteps : Double = 6000
        if userProfile.userGoal != .EatBetter {
            let weeklyPlan = defaults.double(forKey: kCalorieBudgetKey)
            if calorieModel.userConsumedCal > weeklyPlan {
                goalSteps += (calorieModel.userConsumedCal - weeklyPlan)/500.0 * 10000.0
            }
        }
        return userSteps/goalSteps
    }
    

    
    private func totalValueFrom(progress : Progress, key : String) -> Double {
        let microelement = progress.userMicroelement.first(where: {$0.idNameMicroelement == key})
        if microelement == nil {
            return 0.0
        } else {
            return microelement!.graphRepresentation
        }
    }
    
    private func datesFromDate(date:Date) -> (Date, Date)
    {
        let calendar = Calendar.current
        let nowDate = date
        
        let starDate: Date = calendar.startOfDay(for: nowDate)
        var dateComps = DateComponents()
        dateComps.day = 1
        dateComps.second = -1
        
        let endDate: Date = calendar.date(byAdding: dateComps, to: starDate)!
        return (starDate, endDate)
    }
}

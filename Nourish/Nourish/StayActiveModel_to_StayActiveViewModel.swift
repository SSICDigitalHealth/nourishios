//
//  StayActiveModel_to_StayActiveViewMode.swift
//  Nourish
//
//  Created by Vlad Birukov on 29.08.17.
//  Copyright © 2017 SSIC. All rights reserved.
//

import Foundation

class StayActiveModel_to_StayActiveViewMode {
    func transform(model: StayActiveModel) -> StayActiveViewModel {
        let stayActiveViewModel = StayActiveViewModel()
        if model.currentStepsToGoal != nil {
            stayActiveViewModel.currentStepsToGoal = model.currentStepsToGoal
        }
        return stayActiveViewModel
    }
}

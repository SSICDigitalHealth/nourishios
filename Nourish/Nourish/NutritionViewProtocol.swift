//
//  NutritionViewProtocol.swift
//  Nourish
//
//  Created by Nova on 11/21/16.
//  Copyright © 2016 SSIC. All rights reserved.
//

import UIKit

protocol NutritionViewProtocol {
    func setupWith(vitValue : Double, eleValue : Double, macroValue : Double, mineralValue : Double, score : Double, period:period)
}

//
//  WeightStatsPresenter.swift
//  Nourish
//
//  Created by Vino (Vinodha) Sundaramoorthy on 1/9/17.
//  Copyright © 2017 SSIC. All rights reserved.
//

import UIKit
import RxSwift

class WeightStatsPresenter: BasePresenter,UITableViewDelegate,UITableViewDataSource , NRSegmentedControlDelegate{
    var weightsView : WeightStatsProtocol?
    var weightsArray : [weightStatsModel] = []
    var weightPeriod : period = .weekly
    var interactor = WeightstatsInteractor()
    var weightModel : weightModel!
    let toModel = WeightStatsTotal_to_weightModel()
   
    let userProfileInteractor = UserProfileInteractor()
    let mapperObject = UserProfile_to_UserProfileModel()
    let mapperModel = UserProfileModel_to_UserProfile()
    var userModel : UserProfileModel = UserProfileModel()

    override func viewWillAppear(_ animated: Bool) {
        self.subscribtions.append(self.currentUser().subscribe(onNext: { [unowned self] user in
            self.userModel = user
        }, onError: {error in
            LogUtility.logToFile("error is ", error)
        }, onCompleted: {
            self.setUpWeightsForPeriod(period: self.weightPeriod)
        }, onDisposed: {}))
    }
    
    func setUpWeightsForPeriod(period : period) {
        self.subscribtions.append(self.interactor.getWeightStatsDataFor(period: period).observeOn(MainScheduler.instance).subscribe(onNext: { [unowned self] weight in
            self.weightModel = self.toModel.transform(object: weight, period: period)
            self.weightsArray = self.weightModel.weightsArray.reversed()
            self.weightsArray = self.weightsArray.filter{$0.weight > 0}
            
        }, onError: {error in }, onCompleted: {
            self.weightsView?.setWeightModel(model: self.weightModel)
        }, onDisposed: {}))
    }
    
    
    func currentUser() -> Observable<UserProfileModel> {
        let object = self.userProfileInteractor.getRawUser().flatMap({ userProfile -> Observable<UserProfileModel> in
            self.userModel = self.mapperObject.transform(user: userProfile)
            return Observable.just(self.mapperObject.transform(user: userProfile))
        })
        return object
    }
    
    
    // MARK:UITableViewDataSource Implementation
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return weightsArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var data = weightsArray[indexPath.row]
        let cell : WeightsStatsCellTableViewCell = tableView.dequeueReusableCell(withIdentifier: "kWeightStatsCell") as! WeightsStatsCellTableViewCell
        data.weight =  (self.userModel.metricPreference?.isMetric)! ? data.weight : NRConversionUtility.kiloToPounds(valueInKilo: data.weight)
        cell.setUpStatsCell(model: data)
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        let cell = self.tableView(tableView, cellForRowAt: indexPath)
        return cell.frame.size.height
    }
    
    // MARK:NRSegmentedControlDelegate Implementation
    func segmentedControlDidPressedItemAtIndex(segmentedControl: NRSegmentedControl, index: Int) {
        weightPeriod = period(rawValue: index + 1)!
        EventLogger.logOnWeightScreenClickForPerion(period : weightPeriod)
        self.weightsView?.startActivityIndicator()
        self.setUpWeightsForPeriod(period: weightPeriod)
    }

}

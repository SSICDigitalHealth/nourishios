//
//  NRManageGoalViewController.swift
//  Nourish
//
//  Created by Vino (Vinodha) Sundaramoorthy on 11/23/16.
//  Copyright © 2016 SSIC. All rights reserved.
//

import UIKit

class NRManageGoalViewController: BasePresentationViewController {
    
    @IBOutlet weak var manageGoalsView : NRManageGoalView!

    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = kManageGoalsViewTitle
        self.setUpNavigationBarButtons()
        self.manageGoalsView.controller = self
        self.baseViews = [self.manageGoalsView]
    }

    func setUpNavigationBarButtons () {
        let titleAttribute = self.navigationBarTitleAttribute()
        let closeButton : UIBarButtonItem =  UIBarButtonItem(image: UIImage(named:"close_icon"), style: .plain, target: self, action: #selector(self.closeAction(_:)))
        closeButton.setTitleTextAttributes(titleAttribute, for: .normal)
        navigationItem.title = self.title
        self.navigationController?.navigationBar.titleTextAttributes = titleAttribute
        self.navigationController?.navigationBar.barTintColor = UIColor.white
        self.navigationController?.navigationBar.tintColor = NRColorUtility.navigationTintColor()
        self.navigationController?.navigationItem.leftBarButtonItem = closeButton
        navigationItem.leftBarButtonItem = closeButton
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
    }
    
    @IBAction func update(_ sender : UIButton){
        self.manageGoalsView.saveGoalPreference()
    }

    // MARK:NRManageGoalsViewProtocol Implementation
    func showGoalOptionsListWithSelectedModel(goal:UserGoal) {
        let recVC = NRGoalOptionViewController(nibName: "NRGoalOptionViewController", bundle: nil)
        recVC.userGoal = self.manageGoalsView.getCurrentGoal()
        self.navigationController?.pushViewController(recVC, animated: false)
    }
    
    
   override func closeAction(_ sender : UIButton){
    if self.manageGoalsView.hasChanges {
        DispatchQueue.main.async {
            let alert = UIAlertController(title: "Save Changes?", message: "You have made changes to your goal, would you like to update/save your changes? Click the “Update” button below to update/save your goal.", preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: "Yes", style: UIAlertActionStyle.default, handler: { action in
                return
            }))
            alert.addAction(UIAlertAction(title: "No", style: UIAlertActionStyle.cancel, handler: { action in
                self.dismiss(animated: true, completion: nil)
            }))
            self.present(alert, animated: true, completion: nil)
            }
    } else {
        self.dismiss(animated: true, completion: nil)
        }
    }
    
    override func respondToSwipeGesture(gesture: UISwipeGestureRecognizer) {
        let touchPoint = gesture.location(in: self.view)
        if touchPoint.x < 120 {
            super.respondToSwipeGesture(gesture: gesture)
        }
    }
    
    @IBAction func resignKeyboard(sender: AnyObject) {
        let _ = sender.resignFirstResponder()
    }
}


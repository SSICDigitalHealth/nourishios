//
//  NutrientHeaderTableViewCell.swift
//  Nourish
//
//  Created by Vlad Birukov on 26.10.2017.
//  Copyright © 2017 SSIC. All rights reserved.
//

import UIKit

class NutrientHeaderTableViewCell: UITableViewCell {
    @IBOutlet weak var nameNutrient: UILabel!
    @IBOutlet weak var nutrientLabel: UILabel!
    @IBOutlet weak var chartNutrient: NutrientChartView!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var progressBarChart: ProgressBarChart!
    @IBOutlet weak var chartHeight: NSLayoutConstraint!

    var presenter = NutrientWeeklyMonthlyPresenter()
    let chartheight = 117
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func setupWith(model: NutrientViewModel, nutrientInformationModel: NutrientDetailInformationViewModel) {
        var heightChart = 0
        let nutrientDetailInformation = nutrientInformationModel.dataChart
        let message = nutrientInformationModel.message
        
        
        if let datachart = nutrientInformationModel.dataChart.dataCharts {
            if datachart.count > 1 {
                self.presenter.progressBarChart = self.progressBarChart
                self.presenter.setupWith(model: nutrientInformationModel)
                heightChart = self.chartheight
            }
        }
        
        self.chartHeight.constant = CGFloat(heightChart)
        self.layoutIfNeeded()
        
        self.setWith(messages: message)
        
        var target = 0.0
        self.nameNutrient.text = model.nameNutritional
        
        if model.consumedNutritional > nutrientDetailInformation.upperLimit &&  nutrientDetailInformation.upperLimit != 0.0 {
            self.nutrientLabel.textColor = NRColorUtility.hexStringToUIColor(hex: "303030")
            self.nutrientLabel.font = UIFont(name: "Campton-Bold", size: 15)
            
        } else {
            self.nutrientLabel.textColor = NRColorUtility.hexStringToUIColor(hex: "808080")
            self.nutrientLabel.font = UIFont(name: "Roboto-Regular", size: 14)
        }
        
        if model.consumedNutritional <= nutrientDetailInformation.lowerLimit {
            target = nutrientDetailInformation.lowerLimit
            
        } else if model.consumedNutritional > nutrientDetailInformation.lowerLimit && model.consumedNutritional <= nutrientDetailInformation.upperLimit && nutrientDetailInformation.upperLimit != 0 {
            
            target = nutrientDetailInformation.lowerLimit
        } else if model.consumedNutritional > nutrientDetailInformation.lowerLimit && nutrientDetailInformation.upperLimit == 0 {
            target = nutrientDetailInformation.lowerLimit
        } else {
            target = nutrientDetailInformation.upperLimit
        }
        
        self.nutrientLabel.text = String(format: "%@/%@ %@", model.consumedNutritional.roundToTenIfLessOrMore(target: target), target.roundToTenIfLessOrMore(target: target), model.unit)
        
        self.chartNutrient.setupWith(consumedNutrient: model.consumedNutritional, upperLimit: nutrientDetailInformation.upperLimit, lowerLimit: nutrientDetailInformation.lowerLimit)
        
        
    }
    
    private func setWith(messages: [String]?) {
        if messages != nil{
            for i in 0..<messages!.count {
                self.descriptionLabel.text = messages?[i]
                self.descriptionLabel.sizeToFit()
            }
        }
    }
}

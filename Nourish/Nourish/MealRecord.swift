//
//  MealRecordObject.swift
//  Nourish
//
//  Created by Nova on 11/15/16.
//  Copyright © 2016 SSIC. All rights reserved.
//

import UIKit
//import FoodDatabaseKit

class MealRecord: NSObject {
 
    // specified by the user
    var ocasion : Ocasion = .breakfast
    dynamic var grams : Double = 0.0
    dynamic var amount : Double = 0.0
    var isFromOperation : Bool = false
    
    //from server
    var idString : String? = nil
    var name : String? = nil
    var msreDesc : String? = nil
    dynamic var msreAmount : Double = 0.0
    dynamic var msreGrams : Double = 0.0
    dynamic var kCal : Double = 0.0
    dynamic var portionKCal : Double = 0.0
    var brand : String? = nil
    dynamic var score : Double = 0.0
    var userFoodId : String? = nil
    dynamic var calories : Double = 0.0
    var unit : String?
    var isFromNoom : Bool = false
    
    //Sound Hound
    var sourceFoodDB : String = ""
    var sourceFoodDBID : String = ""
    
    var soundHoundFoodUnit : [SHFoodUnit] = []
    
    //Local
    dynamic var isFavourite : Bool = false
    dynamic var searchCount : Int = 0
    dynamic var caloriesPerMilliliter : Double = 0.0
    dynamic var caloriesPerGramm : Double = 0.0
    var barCode : String? = nil
    var brandAndName : String? = nil
    var nutrientsFromNoom : [Nutrient]?
    var foodUid : String? = nil
   /* var foodUnits : [NMFoodUnit]? = []
    var defaultFoodUnit : NMFoodUnit? = nil*/
    var originalSearchString : String? = nil
    
    var date : Date?
    // groups
    var isGroupFavourite : Bool = false
    var isMarkedForFav : Bool = false
    var groupedMealArray : [MealRecordModel] = []
    var groupName : String? = nil
    var groupID : String? = nil
    var noomID : String? = nil
    var numberOfItems : Int?
    
    //photo
    var userPhotoId : String?
    var groupPhotoID : String?
    
    var groupFoodUUID : String?
    
    var localCacheID : String?
    /*
    var date : Date?
    var foodTitle : String?
    var foodId : String?
    var servingAmmount : Float?
    var servingType : ServingType?
    var caloriesPerServing : Float?
    var ocasion : Ocasion?
    var isFavourite : Bool = false
    */
}



//
//  DailyStressChartView.swift
//  Nourish
//
//  Created by Gena Mironchyk on 1/24/17.
//  Copyright © 2017 SSIC. All rights reserved.
//
import UIKit
class DailyStressChartView : BaseView, DailyStressChartProtocol, ChartViewDelegate, UIScrollViewDelegate{
    
    var barChart : BarChartView!
    @IBOutlet weak var tableView = UITableView()
    @IBOutlet weak var scroller  = UIScrollView()
    @IBOutlet weak var bpmLabel = UILabel()
    var separator : UIView!
    
    var startXPosition = 0
    var presenter = StressPresenter()
    let barWidth = 0.75
    var titles : [UILabel] = []
    let calendar = Calendar.current
    func setupView() {
        
        let view = UINib(nibName: "DailyStressChartView", bundle: nil).instantiate(withOwner: self, options: nil)[0] as! UIView
        view.translatesAutoresizingMaskIntoConstraints = false
        self.translatesAutoresizingMaskIntoConstraints = false
        self.addSubview(view)
        NSLayoutConstraint(item: view, attribute: .leading, relatedBy: .equal, toItem: self, attribute: .leading, multiplier: 1, constant: 0).isActive = true
        NSLayoutConstraint(item: view, attribute: .trailing, relatedBy: .equal, toItem: self, attribute: .trailing, multiplier: 1, constant: 0).isActive = true
        NSLayoutConstraint(item: view, attribute: .top, relatedBy: .equal, toItem: self, attribute: .top, multiplier: 1, constant: 0).isActive = true
        NSLayoutConstraint(item: view, attribute: .bottom, relatedBy: .equal, toItem: self, attribute: .bottom, multiplier: 1, constant: 0).isActive = true
        
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        self.setupView()
        self.basePresenter = presenter
        presenter.dailyChartView = self
        scroller?.delegate = self
        
        self.basePresenter = presenter
        tableView?.delegate = presenter
        tableView?.dataSource = presenter
        presenter.currentView = .dailyStressView
        tableView?.register(UINib(nibName: "DailyStressCell", bundle: nil), forCellReuseIdentifier: "DailyStressCell")

        super.viewWillAppear(animated)
        
        
    }
    
    
    
    
    /*override func viewDidLoad() {
        self.setupView()
        self.basePresenter = presenter
        presenter.dailyChartView = self
        barChart.delegate = self
        
        self.basePresenter = presenter
        tableView?.delegate = presenter
        tableView?.dataSource = presenter
        presenter.currentView = .dailyStressView

        tableView?.register(UINib(nibName: "DailyStressCell", bundle: nil), forCellReuseIdentifier: "DailyStressCell")
        presenter.updateDailyStress()

    }*/
    
    

    func scrollViewDidScroll(_ scrollView: UIScrollView) {
            let separatorPosition = self.separator!.frame.origin.x - self.separator!.frame.size.width/2
            let subviewPosition = self.scroller!.contentOffset.x
            let offsetCorrection = self.barChart.frame.size.width / CGFloat((self.barChart.barData?.dataSetCount)!)
            //let positionForSet = Int(((separatorPosition! + subviewPosition)/offsetCorrection))
            let positionForSetFloat = (separatorPosition + subviewPosition)/offsetCorrection
            let positionForSet = Int(positionForSetFloat)
            var dataSet = BarChartDataSet ()
            if self.barChart.barData?.getDataSetByIndex(positionForSet) != nil {
                dataSet = self.barChart.barData?.getDataSetByIndex(positionForSet) as! BarChartDataSet
                let dataEntry = dataSet.entryForIndex(0) as! BarChartDataEntry
                let date = dataEntry.data as! Date
                if (date.compare(Date()) == ComparisonResult.orderedSame || date.compare(Date()) == ComparisonResult.orderedDescending){
                    self.scroller!.isScrollEnabled = false
                    self.scroller!.setContentOffset(CGPoint(x:self.scroller!.contentOffset.x - 1, y:self.scroller!.contentOffset.y), animated: false)
                    self.scroller!.isScrollEnabled = true
                }
                
                if (date.compare(self.calendar.startOfDay(for: Date())) == ComparisonResult.orderedSame || date.compare(self.calendar.startOfDay(for: Date())) == ComparisonResult.orderedAscending) {
                    self.scroller!.isScrollEnabled = false
                    self.scroller!.setContentOffset(CGPoint(x:self.scroller!.contentOffset.x + 1, y:self.scroller!.contentOffset.y), animated: false)
                    self.scroller!.isScrollEnabled = true
                }
                
                self.bpmLabel?.textColor = dataSet.color(atIndex: 0)
                let bpmValue = dataEntry.y
                if Int(bpmValue) > 4 {
                    self.bpmLabel?.text = String.init(format: "%0.0f", bpmValue)
                } else {
                    self.bpmLabel?.text = "0"
                }
            }
    }
    
    func setChartData(data: [DailyStressModel]) {
        self.tableView?.reloadData()
        self.setupChartWithData(data: data)
        self.stopActivityAnimation()
    }
    
    
    private func setupChartWithData(data : [DailyStressModel]){
        if (self.barChart == nil){
            self.barChart = BarChartView()
            self.barChart.delegate = self
        }
            let chartData = self.prepareDatesArray(samples: data)
            self.barChart.frame = CGRect(x: 0.0, y: 0.0, width: Double(chartData.dataSetCount)*4, height: 180.0)
            
            self.barChart.legend.enabled = false
            self.barChart.drawValueAboveBarEnabled = false
            self.barChart.drawRoundedBarEnabled = true
            self.barChart.dragEnabled = true
        
   
            self.barChart.xAxis.axisMinimum = 0.5
            self.barChart.xAxis.axisMaximum = Double(chartData.dataSetCount) + 0.5
            self.barChart.xAxis.drawLabelsEnabled = false
            self.barChart.xAxis.drawGridLinesEnabled = false
            self.barChart.xAxis.drawAxisLineEnabled = false
            
            
            self.barChart.leftAxis.drawAxisLineEnabled = false
            self.barChart.leftAxis.drawGridLinesEnabled = false
            self.barChart.leftAxis.drawLabelsEnabled = false
            self.barChart.leftAxis.drawZeroLineEnabled = false
            
            self.barChart.rightAxis.drawLabelsEnabled = false
            self.barChart.rightAxis.drawAxisLineEnabled = false
            self.barChart.rightAxis.drawGridLinesEnabled = false
            self.barChart.rightAxis.drawZeroLineEnabled = false

            self.barChart.scaleXEnabled = false
            self.barChart.scaleYEnabled = false
            
            self.barChart.isUserInteractionEnabled = false
            chartData.barWidth = Double(barWidth)
            self.barChart.data = chartData
            self.barChart.animate(yAxisDuration: 0.5)

            let viewPortHandler = self.barChart.viewPortHandler
        
        
            for label  in self.barChart.subviews {
                if label is UILabel  {
                    label.removeFromSuperview()
                }
            }
        if (data.count == 0 ){
            barChart.leftAxis.axisMinimum = 0.0
            barChart.leftAxis.axisMaximum = 200
        }

        
        
            for title in titles {
                let labelYPostition = viewPortHandler!.contentBottom
                let frame = title.frame
                var offset:CGFloat = 5.0
                if data.count == 0 {
                    offset = -2.0
                }
                title.frame = CGRect(x: frame.origin.x - frame.size.width, y: labelYPostition - offset, width: frame.size.width, height: frame.size.height)
                self.barChart.addSubview(title)
            }
            
            if(self.separator == nil) {
                self.separator = UIView()
            }
            self.separator.removeFromSuperview()
        
            let yPosition = self.scroller!.frame.origin.y + (self.scroller!.frame.size.height - viewPortHandler!.contentBottom)
            let separatorFrame = CGRect(x: UIScreen.main.bounds.width / 2.0 - 1, y: yPosition, width: 1.0, height: self.scroller!.frame.size.height - 28.0)
            self.separator.frame = separatorFrame
            self.separator.backgroundColor = UIColor.black
            self.addSubview(self.separator)


            
            

        self.scroller!.contentSize = CGSize.init(width: self.barChart.frame.size.width, height: 180.0)
        self.scroller!.addSubview(barChart)
        self.scroller!.setNeedsDisplay()
        self.scroller!.setNeedsLayout()
        if (self.startXPosition != 0){
            self.scroller?.setContentOffset(CGPoint(x: Int(self.startXPosition), y: Int(self.scroller!.contentOffset.y)), animated: false)
        }
}

    
    
    
    private func prepareDatesArray(samples : [DailyStressModel]) -> BarChartData {
        var startDate : Date = calendar.startOfDay(for: calendar.date(byAdding: .day, value: -1, to: Date())!)
        
        var array : [Date] = []
        
        for _ in 0...431{
            array.append(startDate)
            startDate = calendar.date(byAdding: .minute, value: 10, to: startDate)!
        }
       return self.dictionaryWithSamples(array: array, samples: samples)
    }
    
    private func dictionaryWithSamples(array : [Date], samples : [DailyStressModel]) -> BarChartData {
        var dict : [Date : [DailyStressModel]] = [:]
        var previousDate = array.first
        var nextDate : Date?
        for dateIndex in 0...array.count-1  {
            if samples.count > 0 {
                let samplesArray : [DailyStressModel] = []
                dict[previousDate!] = samplesArray
                if array.count > dateIndex+1{
                    nextDate = array[dateIndex+1]
                }
                for sample in samples {
                    let fallsBetween = (previousDate!...nextDate!).contains(sample.dateToShow!)
                    if fallsBetween {
                        dict[previousDate!]?.append(sample)
                    }
                }
                if dict[previousDate!]!.count == 0 {
                    dict[previousDate!]?.append(self.emptySampleFor(date: previousDate!))
                }
                dict[previousDate!] =  (dict[previousDate!]?.sorted(by:{ $0.dateToShow!.compare($1.dateToShow!) == ComparisonResult.orderedAscending }))!
                previousDate = nextDate
            } else {
                let current = array[dateIndex]
                dict[current] = [self.emptySampleFor(date: current)]
            }
        }
        return self.stressDataSet(dictionary: dict)
        
    }

    
    private func stressDataSet(dictionary: [Date : [DailyStressModel]]) -> BarChartData{
        var dataSets : [BarChartDataSet] = []
        var index = 0
        let sortedKeys = Array(dictionary.keys).sorted(by: <)
        var currentKey : Date!
        var previousKey = sortedKeys.first
        self.titles.removeAll()
        
        for key in sortedKeys {
            currentKey = key
            let samples = dictionary[key]
            for sample in samples! {
                if (self.startXPosition == 0) {
                    if (sample.dateToShow?.compare(Date()) == ComparisonResult.orderedSame || sample.dateToShow?.compare(Date()) == ComparisonResult.orderedDescending) {
                        self.startXPosition = index * 4
                    }
                }
                let dataEntry = BarChartDataEntry.init(x: Double(index + 1), y: sample.bpmValue!, data: sample.dateToShow as AnyObject!)
                let set = BarChartDataSet.init(values: [dataEntry], label: "")
                set.setColor(stressLevel.graphColorForStress(stress: sample.stressValue!))
                if Int(sample.bpmValue!) < 10 {
                    set.setColor(NRColorUtility.dotColor())
                }
                set.drawValuesEnabled = false
                set.barRoundingCorners = [.allCorners]
                dataSets.append(set)
                index += 1
            }
            
            if (calendar.component(.hour, from: previousKey!) != calendar.component(.hour, from: currentKey)){
                var components = DateComponents()
                components.day = 1
                components.second = -1
                let startOfDay = calendar.startOfDay(for: Date())
                let fallsBetween = (startOfDay...calendar.date(byAdding: components, to: startOfDay)!).contains(currentKey!)
                if fallsBetween {
                    let dateFormatter = DateFormatter()
                    dateFormatter.dateFormat = "hh"
                    let viewPortHandler = self.barChart.viewPortHandler
                    
                
                    let label = UILabel()
                    label.contentMode = .top
                    label.font = UIFont.systemFont(ofSize: 9.0)
                    label.text = dateFormatter.string(from: currentKey)
                    var xPosition = CGFloat(index * 4)
                    if (currentKey.compare(startOfDay) == ComparisonResult.orderedSame || (currentKey.compare(calendar.date(byAdding: .hour, value: 12, to: startOfDay)!) == ComparisonResult.orderedSame)) {
                        dateFormatter.dateFormat = "ha"
                        let dateStr = dateFormatter.string(from: currentKey)
                        label.text = dateStr
                        xPosition = xPosition + label.intrinsicContentSize.width/2.0
                    }
                    let labelFrame = CGRect(x: xPosition , y: viewPortHandler!.contentBottom, width: label.intrinsicContentSize.width, height: 7.0)
                    label.frame = labelFrame
                    titles.append(label)
                }
                previousKey = currentKey
            }
            
        }
        let chartData = BarChartData.init(dataSets: dataSets)
        return chartData
    }
    
    private func emptySampleFor(date : Date)->DailyStressModel{
        let model = DailyStressModel()
        model.bpmValue = 2
        model.stressValue = .Undetermined
        model.dateToShow = date
        return model
    }
    
}

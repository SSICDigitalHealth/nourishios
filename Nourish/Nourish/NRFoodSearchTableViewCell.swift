//
//  NRFoodSearchTableViewCell.swift
//  Nourish
//
//  Created by Vino (Vinodha) Sundaramoorthy on 11/15/16.
//  Copyright © 2016 SSIC. All rights reserved.
//

import UIKit

class NRFoodSearchTableViewCell: UITableViewCell {
    
    @IBOutlet weak var foodImageView : UIImageView!
    @IBOutlet weak var foodName : UILabel!
    @IBOutlet weak var measure : UILabel!
    @IBOutlet weak var calories : UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        self.backgroundColor = NRColorUtility.nourishHomeColor()
    }
    
    func setupFoodResultWithModel(foodModel : foodSearchModel) {
        self.foodName.text = foodModel.foodTitle
        self.measure.text = String(format:"%.1f %@",foodModel.amount,foodModel.measure)
        self.measure.textColor = NRColorUtility.foodLabelColor()
        self.calories.textColor = NRColorUtility.foodLabelColor()
        self.calories.text = ""
        
        if let cal = Double(foodModel.calories) {
            self.calories.text = String(format:"%.1f Cal",(cal))
        }
    }
}

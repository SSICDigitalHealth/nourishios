//
//  NutrientsPresenter.swift
//  Nourish
//
//  Created by Vlad Birukov on 11.09.17.
//  Copyright © 2017 SSIC. All rights reserved.
//

import Foundation
import RxSwift


class NutrientsPresenter: BasePresenter, UITableViewDataSource, UITableViewDelegate {
    let objectToModelMapper = NutrientsModel_to_NutrientsViewModel()
    let interactor = NutrientsInteractor()
    var nutrientsView: NutrientsProtocol!
    var nutrientsViewModel: NutrientsViewModel?
    var progressViewController: NewProgressViewController?
    
    var date: (Date, Date)?
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.getInformation()
    }
    
    func getInformation(){
        if self.nutrientsView.config() != nil {
            self.date = self.nutrientsView.config()?.getDate()
        }
        
        if let view = self.nutrientsView {
            if self.date != nil {
                let proxyDate = nutrientsView?.config()?.date

                    self.subscribtions.append(self.interactor.execute(startDate: (self.date?.0)!, endDate: (self.date?.1)!).observeOn(MainScheduler.instance).subscribe(onNext: { [weak self] nutrientsModel in
                        if let instance = self {
                            if proxyDate == instance.nutrientsView?.config()?.date {
                                instance.nutrientsViewModel = instance.objectToModelMapper.transform(model: nutrientsModel)
                                view.reloadData()
                            }
                            view.stopActivityAnimation()
                        }
                    }, onError: { error in
                        view.stopActivityAnimation()
                        view.parseError(error : error, completion: nil)
                    }, onCompleted: {
                    }, onDisposed: {
                        view.stopActivityAnimation()
                    }))
            }
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if let model = nutrientsViewModel {
            return model.nutrientChartData.count
        } else {
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cellDetailNutrients") as! NutrientsDetailTableViewCell
        
        if let model = nutrientsViewModel {
            if indexPath.row == model.nutrientChartData.count - 1 {
                cell.separatorLine.backgroundColor = UIColor.white
            } else {
                cell.separatorLine.backgroundColor = NRColorUtility.hexStringToUIColor(hex: "#BFBFBF")
            }
        }
        
        if let model = nutrientsViewModel {
            cell.setupWithModel(model: model.nutrientChartData[indexPath.row], nutrientDetailInformation: model.nutrientDetail[indexPath.row].dataChart)
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let newController = NutritionDetailViewController()
        
        if let model = nutrientsViewModel {
            EventLogger.logNutritionTipsClicked()
            newController.titleCaption = model.nutrientChartData[indexPath.row].nameNutritional
            newController.nutrientViewModel = model.nutrientChartData[indexPath.row]
            newController.nutrientDetailInformationViewModel = model.nutrientDetail[indexPath.row]
            if let config = self.nutrientsView.config() {
                newController.progressConfig = config
            }
        }
        
        if let controller = progressViewController {
            if let navigation = controller.navigationController {
                navigation.pushViewController(newController, animated: false)
                NavigationUtility.hideTabBar(animated: false)

            }
        }
    }

}

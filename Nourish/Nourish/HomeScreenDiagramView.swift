//
//  HomeScreenDiagramView.swift
//  Nourish
//
//  Created by Andrey Zmushko on 9/13/17.
//  Copyright © 2017 SSIC. All rights reserved.
//

import UIKit

class HomeScreenDiagramView: UIView {
    
    var activity: Double?
    var sleep: Double?
    
    var lines: (width: CGFloat, gap: CGFloat) {
        if UIScreen.main.bounds.width <= 320  {
            return (14.0, (self.bounds.width - 118) / 4)
        }
        return (24.0, (self.bounds.width - 158) / 4)
    }
    
    var icons: (activity: UIImage, sleep: UIImage) {
        if UIScreen.main.bounds.width <= 320 {
            return (UIImage(named: "hmcircle_activity_small") ?? UIImage(),
                    UIImage(named: "hmcircle_sleep_small") ?? UIImage())
        }
        return (UIImage(named: "hmcircle_activity_big") ?? UIImage(),
                UIImage(named: "hmcircle_sleep_big") ?? UIImage())
    }
    
    var gaps: (x: CGFloat, y: CGFloat) {
        if UIScreen.main.bounds.width <= 320  {
            return (1.2, 0.4)
        }
        return (2.0, 1.5)
    }
    
    var activitiImageView: UIImageView!
    var sleepImageView: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.activitiImageView = UIImageView(image: self.icons.activity)
        self.sleepImageView = UIImageView(image: self.icons.sleep)
        
        self.addSubview(self.activitiImageView)
        self.addSubview(self.sleepImageView)
    }

    override func draw(_ rect: CGRect) {
        self.activitiImageView.frame = CGRect(x: self.bounds.width / 2 - self.activitiImageView.bounds.width - self.gaps.x,
                                              y: self.lines.width - self.activitiImageView.bounds.height - self.gaps.y,
                                              width: self.activitiImageView.bounds.width,
                                              height: self.activitiImageView.bounds.height)
        self.sleepImageView.frame = CGRect(x: self.bounds.width / 2 - self.sleepImageView.bounds.width - self.gaps.x,
                                           y: self.lines.width + self.lines.gap + self.lines.width - self.sleepImageView.bounds.height - self.gaps.y,
                                           width: self.sleepImageView.bounds.width,
                                           height: self.sleepImageView.bounds.height)
        
        self.activitiImageView.image = self.activitiImageView.image?.withRenderingMode(.alwaysTemplate)
        self.activitiImageView.tintColor = NRColorUtility.homeScreenIconsColor()
        self.sleepImageView.image = self.sleepImageView.image?.withRenderingMode(.alwaysTemplate)
        self.sleepImageView.tintColor = NRColorUtility.homeScreenIconsColor()

        self.drawArc(lineWidth: self.lines.width,
                     diameter: self.bounds.width,
                     value: 1.0,
                     color: UIColor.white)
        self.drawArc(lineWidth: self.lines.width,
                     diameter: self.bounds.width - 2 * (self.lines.width + self.lines.gap),
                     value: 1.0,
                     color: UIColor.white)
        self.drawArc(lineWidth: self.lines.width,
                     diameter: self.bounds.width,
                     value: CGFloat(self.activity ?? 0),
                     color: NRColorUtility.appBackgroundColor())
        self.drawArc(lineWidth: self.lines.width,
                     diameter: self.bounds.width - 2 * (lines.width + self.lines.gap),
                     value: CGFloat(self.sleep ?? 0),
                     color: NRColorUtility.appBackgroundColor())
    }
    
    private func drawArc(lineWidth: CGFloat, diameter: CGFloat, value: CGFloat, color: UIColor) {
        let center = CGPoint(x: self.bounds.width / 2,
                             y: self.bounds.height / 2)
        
        let startAngle: CGFloat = 3 * .pi / 2
        let endAngle: CGFloat = startAngle + 2 * .pi * value
        
        let path = UIBezierPath(arcCenter: center,
                                radius: diameter / 2 - lineWidth / 2,
                                startAngle: startAngle,
                                endAngle: endAngle,
                                clockwise: true)
        
        path.lineWidth = lineWidth
        color.setStroke()
        path.stroke()
    }
}

//
//  StartMealPlannerViewModel.swift
//  Nourish
//
//  Created by Vlad Birukov on 29.08.17.
//  Copyright © 2017 SSIC. All rights reserved.
//

import Foundation

class NouriqStartMealPlannerViewModel: NSObject {
    var goal: String?
    var diet: String?
    var caloriesDay: Double?
}


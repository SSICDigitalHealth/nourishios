//
//  NRRecommendationsCell.swift
//  Nourish
//
//  Created by Vino (Vinodha) Sundaramoorthy on 11/14/16.
//  Copyright © 2016 SSIC. All rights reserved.
//

import UIKit

let kLabelWidthPadding : CGFloat = 55
let kLabelHeightPadding : CGFloat = 15
let kMinimumHeightPadding : CGFloat = 10

class NRRecommendationsCell: MGSwipeTableCell {
    @IBOutlet weak var bgView : UIView!
    var recommendationLabel : UILabel!
    var titleLabel : UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.layoutMargins = UIEdgeInsets.zero
        self.preservesSuperviewLayoutMargins = false;
        self.separatorInset = UIEdgeInsets.zero
        self.recommendationLabel = UILabel()
        self.recommendationLabel.textColor = NRColorUtility.appChatTextColor()
        self.recommendationLabel.font = UIFont.systemFont(ofSize: 12)
        self.recommendationLabel.numberOfLines = 0
        self.recommendationLabel.sizeToFit()
        self.recommendationLabel.textAlignment = .left
        self.titleLabel = UILabel()
        self.titleLabel.font = UIFont.boldSystemFont(ofSize: 12)
        self.titleLabel.textColor = NRColorUtility.appChatTextColor()
        self.titleLabel.lineBreakMode = .byWordWrapping
        self.titleLabel.numberOfLines = 0
        
        self.bgView.layer.cornerRadius = 2
        self.bgView.layer.shadowColor = UIColor.lightGray.cgColor
        self.bgView.layer.shadowOffset = CGSize(width: 0, height: 3)
        self.bgView.layer.shadowOpacity = 1.0
        self.bgView.layer.shadowRadius = 5.0
        self.bgView.layer.masksToBounds = false
    }
    
    func setWithModel(model : recommendation ,collapsed:Bool,loggedOcasion:Bool) {
        let recommMaxWidth = UIScreen.main.bounds.width - kLabelWidthPadding
        
        let isMealsSection = (recommendationType.stringFromEnum(type: model.type) == kLunchString || recommendationType.stringFromEnum(type: model.type) == kSnackString || recommendationType.stringFromEnum(type: model.type) == kBreakfastString  || recommendationType.stringFromEnum(type: model.type) == kDinnerString)
            self.bgView.isHidden = isMealsSection
        
        if  !collapsed && !loggedOcasion {
            if model.title.count == 0 {
                let descHeight = model.description.sizeOfBoundingRect(width: recommMaxWidth, height: maxLabelSize.height, font: recommendationLabel.font).height
                self.recommendationLabel.frame = CGRect(x: 25, y: 10, width: recommMaxWidth, height: descHeight + 10)
                self.titleLabel.text = ""
            } else {
                let titleAttribute = [ NSForegroundColorAttributeName: NRColorUtility.appChatTextColor() , NSFontAttributeName : titleLabel.font] as [String : Any]
                let titleString = NSMutableAttributedString(string: model.title, attributes: titleAttribute)
           
                if  model.subTitle.count > 0 {
                    let labelAttributes = [ NSForegroundColorAttributeName: NRColorUtility.appChatTextColor() , NSFontAttributeName : UIFont.systemFont(ofSize: 12)]
                    titleString.append(NSAttributedString(string:model.subTitle, attributes: labelAttributes))
                }
                let titleHeight = model.title.sizeOfBoundingRect(width: recommMaxWidth, height: maxLabelSize.height, font: titleLabel.font).height
                let descHeight = model.description.sizeOfBoundingRect(width: recommMaxWidth, height: maxLabelSize.height, font: recommendationLabel.font).height

                self.titleLabel.frame = CGRect(x: 25, y: 5, width: recommMaxWidth, height: titleHeight + 20)
                self.recommendationLabel.frame = CGRect(x: 25, y: self.titleLabel.frame.height + 5, width: recommMaxWidth, height: descHeight)
                titleLabel.attributedText = titleString
            }

            recommendationLabel.text = model.description
            self.recommendationLabel.isHidden = false
            self.titleLabel.isHidden = false
            self.contentView.addSubview(self.recommendationLabel)
            self.contentView.addSubview(self.titleLabel)
        } else {
            self.recommendationLabel.isHidden = true
            self.titleLabel.isHidden = true
        }
    }
}

//
//  DailyReportPresenter.swift
//  Nourish
//
//  Created by Vino (Vinodha) Sundaramoorthy on 2/8/17.
//  Copyright © 2017 SSIC. All rights reserved.
//

import UIKit
import RxSwift

class DailyReportPresenter: BasePresenter {

    var dailyView : DailyReportProtocol? = nil
    let interactor = ReportsInteractor()
    var report : DailyReportModel!
    
    let profileInteractor = UserProfileInteractor()
    let mapperObject = UserProfile_to_UserProfileModel()
    let mapperModel = UserProfileModel_to_UserProfile()
    var userModel : UserProfileModel = UserProfileModel()
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    
        self.subscribtions.append(self.interactor.getReportFor(date: (self.dailyView?.getReportDate())!).observeOn(MainScheduler.instance).subscribe(onNext: { [unowned self] model in
                NutritionsUtility.formatNutrientsName(nutritions: model.justRightNutrients!)
                NutritionsUtility.formatNutrientsName(nutritions: model.tooLittleNutrients!)
                NutritionsUtility.formatNutrientsName(nutritions: model.tooMuchNutrients!)

                self.dailyView?.setReport(report: model)
                self.report = model
            
        }, onError: {error in }, onCompleted: {}, onDisposed: {}))
        
    }
    
    func currentUser() -> Observable<UserProfileModel> {
        let object = self.profileInteractor.getRawUser().flatMap({ userProfile -> Observable<UserProfileModel> in
            self.userModel = self.mapperObject.transform(user: userProfile)
            return Observable.just(self.mapperObject.transform(user: userProfile))
        })
        return object
    }
    
}

//
//  ChatPresenter.swift
//  Nourish
//
//  Created by Gena Mironchyk on 2/7/17.
//  Copyright © 2017 SSIC. All rights reserved.
//

import UIKit
import RxSwift
import RealmSwift
import SwiftKeychainWrapper

let firstShowKey = "FirstShowOfChat"
let onBoardingKey = "onBoardingCompleted"
let kOnBoardingKeyDate = "kOnBoardingKeyDate"
let kDiaryDumpLastDate = "kDiaryDumpLastDate"
let noThanksMessage = "No thanks"

enum OnboardingQuestionType : Int {
    case Welcome
    case Sex
    case Age
    case Diet
    case ActivityLevel
    case HealthKitPermissions
    case Metrics
    case Height
    case Weight
    case Goal
    case TargetLoseWeight
    case WeightLossRecommendations
    case MaintainWeightRecommendations
    case GoalSettings
    case WatchQuestion
    case InfoMessage
    case LogWeight
}

class ChatPresenter: BasePresenter, UITableViewDataSource, UITableViewDelegate,NRPickerViewDelegate,NRPickerViewDataSource{
    let interactor = ChatBoxInterractor()
    var chat : ChatViewProtocol!
    let userProfileInteractor = UserProfileInteractor()
    var fitBitCacher = BackgroundFitBitCacher.shared

    var homeView : OnboardingHomeViewProtocol!
    
    var initialQuestionsDictionary : [OnboardingQuestionType:[ChatDataBaseItemViewModel]] = [:]
    var initialAnswersDictionary : [OnboardingQuestionType:[ChatDataBaseItemViewModel]] = [:]
    
    var tableData : [ChatDataBaseItemViewModel] = []
    
    var requiredOnboardingQuestions : [Int] = []
    
    var currentQuestionType : OnboardingQuestionType = .Welcome
    
    var prefilledUser : UserProfileModel = UserProfileModel()
    
    var responseViews : [UIView] = []

    var chatSession : ChatSession?
    
    var needToShowWeightReccomendations : Bool = false
    
    var needToShowMaintainWeightRecommendations : Bool = false
        
    var pieChartView = NRPieChartView()
    var circularSliderView : CircularSliderControl?  = nil
    var previousCurrentValue : Float =  0
    var sliderValue : Float = 0
    var segments = [
        Segment(color: NRColorUtility.fitnessPieColor(), value: 25 , imageName:kFitnessIcon,description :kFitnessDesc),
        Segment(color: NRColorUtility.lifeStylePieColor(), value: 20 , imageName: kLifeStyleIcon,description:kLifeStyleDesc),
        Segment(color: NRColorUtility.nutritionPieColor(), value: 55 , imageName:kFoodIcon,description: kNutritionDesc)
    ]
    var startAngle : Float = 0
    
    var userModel : UserProfileModel!
    var loggedUserModel : UserProfileModel?
    let userMapperObject = UserProfile_to_UserProfileModel()
    let userMapperModel = UserProfileModel_to_UserProfile()
    
    let databaseItemToViewModelMapper = ChatDataBaseItem_to_ChatDataBaseItemViewModel()
    let viewModelToDatabaseItemMapper = ChatDataBaseItemViewModel_to_ChatDataBaseItem()

    
    var awakedFromBackgroud = false
    
    @IBOutlet weak var agePicker : NRPickerView!
    @IBOutlet weak var heightPicker : NRPickerView!
    @IBOutlet weak var weightPicker : NRPickerView!
    
    
    override func viewWillAppear(_ animated: Bool) {
        EventLogger.logVisitingHomeScreen()
        self.chat.hideResponseView()
        self.fetchActualUser()
        
        
       // self.loadUser(completion: {granted in
            //if granted == true {
        
                    
            //}
        //})
        
    }
    
    private func fetchActualUser() {
        if self.loggedUserModel == nil {
            self.subscribtions.append(self.userProfileInteractor.getCurrentUserWith(fetchingPolicy: .DefaultPolicy).observeOn(MainScheduler.instance).subscribe(onNext: { [unowned self] userProfile in
                self.userModel = self.userMapperObject.transform(user: userProfile)
                
            }, onError: {error in
                self.chat.parseError(error : error, completion: {completed in
                    if error.code == 429 {
                        self.chat.onboardingFinished()
                    }
                })
            }, onCompleted: {
                self.updateAll()
            }, onDisposed: {}))
        } else {
            self.userModel = self.loggedUserModel!
            self.updateAll()
        }
    }
    
    private func updateAll () {
        if self.awakedFromBackgroud == true {
            self.awakedFromBackgroud = false
            self.synchronizeWithings()
        } else {
            self.prepareChatHistory()
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        self.loggedUserModel = nil
    }
    
    
    func synchronizeWithings () {
        let withingsInteractor = WithingsInteractor()
        self.subscribtions.append(withingsInteractor.syncWithingsMessages().observeOn(MainScheduler.instance).subscribe(onNext: { [unowned self] synced in
            if synced == true {
                self.prepareChatHistory()
            }
        }, onError: {error in
            self.chat.parseError(error : error, completion: {completed in
                if error.code == 429 {
                    self.chat.onboardingFinished()
                }
            })

        }, onCompleted: {
        }, onDisposed: {}))
    }
    
    func synchronizeFitBitActivities () {
        if KeychainWrapper.standard.string(forKey: kActivitySourceKey) != nil {
            if KeychainWrapper.standard.string(forKey: kActivitySourceKey) == "Fit Bit" {
                self.fitBitCacher.startCaching()
            }
        }
    }
    
    var ageDataSource :[Int] = []
    var heightDataSource : [String] = []
    var weightDataSource : [String] = []
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.tableData.count
    }

    
    
    private func getPrefilledUser () -> Observable<UserProfileModel> {
        let object = self.userProfileInteractor.getCurrentUser().flatMap({ userProfile -> Observable<UserProfileModel> in
            self.prefilledUser = self.userMapperObject.transform(user: userProfile)
            return Observable.just(self.userMapperObject.transform(user: userProfile))
        })
        return object
    }

    func loadPreffiledUser() {
        self.subscribtions.append(self.getPrefilledUser().observeOn(MainScheduler.instance).subscribe(onNext: { [unowned self] user in
            self.prefilledUser = user
        }, onError: {error in
            self.chat.parseError(error : error, completion: {completed in
                if error.code == 429 {
                    self.chat.onboardingFinished()
                }
            })
        }, onCompleted: {
            
        }, onDisposed: {}))
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell : ChatCell! = ChatCell.init(style: UITableViewCellStyle.default, reuseIdentifier: "ChatCell")
        let chatModel : ChatDataBaseItemViewModel = self.tableData[indexPath.row]
       // if !chatModel.isInvalidated {
            cell!.configureWithModel(chatModel: chatModel)
            cell.selectionStyle = .none
      //  }
        return cell!
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        let cell : ChatCell = self.tableView(tableView, cellForRowAt: indexPath) as! ChatCell
        let chatModel =  self.tableData[indexPath.row]
        let cellPadding  : CGFloat = indexPath.row == self.tableData.count - 1 ? 25 : 5
       // if !chatModel.isInvalidated {
            cell.configureWithModel(chatModel: chatModel)
       // }
        return cell.frame.size.height + cellPadding
    }
    
    
    private func messageObject(type : chatBoxType, content : String, fromUser : Bool)->ChatDataBaseItemViewModel {
        let message = ChatDataBaseItemViewModel()
        message.date = Date()
        message.chatBoxType = type
        message.content = content
        message.isFromUser = fromUser
        
        if self.userModel.userID != nil {
            message.userID = self.userModel.userID!
        }
        
        return message
    }
    
    
    func prepareUserResponces(){
        if self.currentQuestionType == .InfoMessage {
            self.responseButtonCliked(sender: nil)
        } else {
        let arrayOfMessages = self.initialAnswersDictionary[self.currentQuestionType]
        self.responseViews.removeAll()
        var xPosition : CGFloat = 20.0
        var yPosition : CGFloat = self.yPosition(questionType: self.currentQuestionType)
        
        if (self.currentQuestionType == .GoalSettings) {
            self.responseViews.append(contentsOf: self.prepareChartView())
        }
        var responseIndex = 0
        var tag = 0
        for userResponse in arrayOfMessages!{
            
            var height = NRChatUtility.getSizeOfString(message: userResponse.content).height
            if  height < 48.0 {
                height = 48.0
            }
            var width = NRChatUtility.getSizeOfString(message: userResponse.content).width
            if width < 120.0{
                width = 120.0
            }
            
            if self.currentQuestionType != .LogWeight && (arrayOfMessages?.count)! < 4 {
                width = UIScreen.main.bounds.width - 40
            } else {
                width = UIScreen.main.bounds.width / 2 - 30
            }
            
            if self.currentQuestionType == .LogWeight || arrayOfMessages?.count == 4 {
                if ( xPosition == 20.0) {
                    if ((arrayOfMessages?.count)! % 2 == 0  && arrayOfMessages != nil || (arrayOfMessages?.count)! > 2) {
                        xPosition = UIScreen.main.bounds.width/2 - width - 10.0
                    } else {
                        xPosition = UIScreen.main.bounds.width/2 - width/2.0
                    }
                }
            }
            
            
            let frame = CGRect(x: xPosition, y: yPosition, width: width, height: height)
            
            let respButton = self.createResponseButton(message: userResponse, frame: frame)
            if self.currentQuestionType == .LogWeight || self.currentQuestionType == .HealthKitPermissions {
                respButton.tag = tag
            }
            self.responseViews.append(respButton)
            tag += 1
            responseIndex += 1
            
            if self.currentQuestionType == .LogWeight || arrayOfMessages?.count == 4 {
                if (responseIndex % 2 == 0){
                    if (arrayOfMessages?.count == 3) {
                        xPosition = UIScreen.main.bounds.width/2 - width/2.0
                    } else {
                        xPosition = UIScreen.main.bounds.width/2 - width - 10.0
                    }
                    yPosition+=height + 20.0
                } else {
                    xPosition = xPosition + width + 20
                }
            } else {
                xPosition = 20.0
                yPosition += height + 20.0
            }
        }
        
        if (self.currentQuestionType == .Age || self.currentQuestionType == .Height || self.currentQuestionType == .Weight || self.currentQuestionType == .TargetLoseWeight || self.currentQuestionType == .LogWeight) {
            let picker = self.preparePicker(questionType: self.currentQuestionType)
            self.responseViews.append(picker)
        }
        
        if (self.currentQuestionType == .GoalSettings) {
            self.chat.updateUserResponcesWithChartView(responses: self.responseViews)
        } else {
            self.chat.updateUserResponces(responces: responseViews)
        }
    }
    }
    
    
    private func yPosition(questionType : OnboardingQuestionType) -> CGFloat {
        switch questionType {
        case .GoalSettings:
            return 310.0
        case .Age: return 70.0
        case .Height: return 70.0
        case .Weight, .LogWeight: return 70.0
        case .TargetLoseWeight: return 70.0
        default:
            return 10.0
        }
    }
    
    private func prepareChartView () ->[UIView] {
        let pieChart = NRPieChartView()
        pieChart.segments = [
            Segment(color: NRColorUtility.fitnessPieColor(), value: CGFloat(self.prefilledUser.goalPrefsActivity!) , imageName:kFitnessIcon,description :kFitnessDesc),
            Segment(color: NRColorUtility.lifeStylePieColor(), value: CGFloat(self.prefilledUser.goalPrefsLifestyle!) , imageName: kLifeStyleIcon,description:kLifeStyleDesc),
            Segment(color: NRColorUtility.nutritionPieColor(), value: CGFloat(self.prefilledUser.goalPrefsFood!) , imageName:kFoodIcon,description: kNutritionDesc)

        ]
        pieChart.frame = CGRect(x:20, y:0.0 , width: 300, height: 300)
        
        let circularView  = CircularSliderControl(frame:  CGRect(x:20, y:0.0, width: 300, height: 300),currentAngle:0)
        circularView.radius = Float(pieChart.getPieRadius())
        circularView.centerPoint = pieChart.getCenterPoint()
        circularView.currentValue = 0
        circularView.addTarget(self, action:#selector(self.valueChanged(sender:)), for: .valueChanged)
        
        self.pieChartView = pieChart
        self.circularSliderView = circularView
        
        self.userModel.goalPrefsLifestyle = self.prefilledUser.goalPrefsLifestyle
        self.userModel.goalPrefsFood = self.prefilledUser.goalPrefsFood
        self.userModel.goalPrefsActivity = self.prefilledUser.goalPrefsActivity
        
        return [self.pieChartView, self.circularSliderView!]
    }
    
    
    func valueChanged(sender:CircularSliderControl) {
        let adjustedValue = sender.currentValue
        var delta : Float  = 0.0
        
        if self.previousCurrentValue == 0 && adjustedValue > 100 {
            delta = 360 - adjustedValue
        } else if adjustedValue == 0 && self.previousCurrentValue > 100 {
            delta = 360 - self.previousCurrentValue
        }
        else {
            delta = abs(adjustedValue - self.previousCurrentValue)
        }
        
        if delta > 180 && delta < 359 {
            delta = 360 - delta
        }
        
        if delta > 0 {
            var fitness : CGFloat = 0
            var nutrition : CGFloat = 0
            
            if previousCurrentValue < adjustedValue {
                fitness = segments[0].value - CGFloat(delta)
                nutrition = segments[2].value + CGFloat(delta)
            } else {
                fitness = segments[0].value + CGFloat(delta)
                nutrition = segments[2].value - CGFloat(delta)
            }
            
            if (fitness >= kMinFitnessValue && fitness <= kMaxFitnessValue) || (nutrition >= kMinNutritionValue && nutrition <= kMaxNutritionValue) {
                segments[0].value = CGFloat(fitness)
                segments[2].value = CGFloat(nutrition)
                self.previousCurrentValue = adjustedValue
                
                self.userModel.goalPrefsActivity = Float(self.segments[0].value)
                self.userModel.goalPrefsFood = Float(self.segments[2].value)
                self.userModel.goalPrefsLifestyle = 100.0 - self.userModel.goalPrefsFood! - self.userModel.goalPrefsActivity!

                
                //Slider handle position
                if self.userModel.goalPrefsActivity! < kDefaultNutritionValue {
                    self.startAngle = kDefaultNutritionValue - self.userModel.goalPrefsActivity!
                } else if self.userModel.goalPrefsActivity! > kDefaultNutritionValue {
                    self.startAngle = 360 - abs(self.userModel.goalPrefsActivity! - kDefaultNutritionValue)
                } else {
                    self.startAngle = 0
                }
                self.sliderValue = sender.currentValue
                self.pieChartView.reDraw = true
                self.pieChartView.currentSliderAngle = CGFloat(self.sliderValue)
                self.pieChartView.segments = segments
            } else {
                self.circularSliderView!.currentValue = self.previousCurrentValue
                self.circularSliderView!.angleFromNorth = Int(self.previousCurrentValue)
            }
        }
    }

    
    
    
    
//    func loadUser(completion : @escaping (Bool) -> ()){
//        let _ = self.currentRawUser().subscribe(onNext: {user in
//            self.userModel = user
//            completion(true)
//        }, onError: {error in
//
//        }, onCompleted: {
//
//        }, onDisposed: {})
//    }
    
    func prepareChatHistory () {
            if self.userModel.userID != nil {
            self.subscribtions.append(self.interactor.getChatHistoryFor(date: Date(), userProfile: self.userMapperModel.transform(userModel: self.userModel)).observeOn(MainScheduler.instance).subscribe(onNext: { [unowned self] history in
                self.tableData = self.databaseItemToViewModelMapper.transform(items: history)
                
            }, onError: {error in
                self.chat.parseError(error : error, completion: {completed in
                    if error.code == 429 {
                        self.chat.onboardingFinished()
                    }
                })
            }, onCompleted: {
                self.chat.showGoalChangedInfo()
                self.rebuildDataStoreIfNeeded()
            }, onDisposed: {}))
        }
    }
    
    private func rebuildDataStoreIfNeeded() {
        if self.tableData.contains(where: {type in type.chatBoxType == .typeLogWeight}) && !self.tableData.contains(where: {type in type.chatBoxType == .typeWeightRecord}) {
            let indexOfQuestion = self.tableData.index(where: {type in type.chatBoxType == .typeLogWeight})
            if indexOfQuestion != self.tableData.count - 1 {
                let question = self.tableData[indexOfQuestion!]
                self.tableData.remove(at: indexOfQuestion!)
                self.tableData.append(question)
                self.chat.updateChat()
            }
            
        }
        self.prepareInitialDictionaries()
     
    }
    
    private func createResponseButton(message : ChatDataBaseItemViewModel, frame : CGRect) -> ResponseButton {
        let responseButton = NRChatUtility.responseButtonWithMessage(message: message)
        responseButton.frame = frame
        responseButton.layer.cornerRadius = responseButton.frame.size.height / 2
        responseButton.layer.masksToBounds = true
        responseButton.addTarget(self, action:#selector(self.responseButtonCliked(sender:)), for: .touchUpInside)
        responseButton.isUserInteractionEnabled = true
        return responseButton
    }
    
    
    func responseButtonCliked(sender : ResponseButton?){
        if self.currentQuestionType == .InfoMessage {
            self.chatSession?.needToSave = true
            self.chatSession?.questions.append(contentsOf:self.initialQuestionsDictionary[self.currentQuestionType]!)
            self.processSession(session: self.chatSession!)
            self.prepareChatHistory()
        } else {
        self.tableData.append((sender?.message)!)
        let path = IndexPath(row: self.tableData.count - 1 , section: 0)
        self.chat.insertRowAtIndexPath(path : [path] ,fromUser : true)
        //self.tableData.append(sender.message)
        self.chat.clearResponseView(responses: [])
        
        if self.chatSession != nil {
            if self.currentQuestionType == .LogWeight {
                let questions = self.tableData.filter({type in type.chatBoxType == .typeLogWeight})
                let question = questions.last
                let realm = try! Realm()
                try! realm.write {
                    question!.date =  Calendar.current.date(byAdding: .second, value: -1, to: (sender?.message.date)!)!
                }
                //self.chatSession?.questions.append(question!)
                if sender?.tag == 0 {
                    sender?.message.content = noThanksMessage
                    self.chatSession?.answers.append((sender?.message)!)
                    self.chatSession?.needToSave = true
                    self.processSession(session: self.chatSession!)
                    self.prepareChatHistory()
                } else {
                    self.chatSession?.answers.append((sender?.message)!)
                    self.processUserResponse(session: self.chatSession!, questionType: self.currentQuestionType, response : (sender?.message.content)!)
                }
            } else {
                self.chatSession?.questions.append(contentsOf:self.initialQuestionsDictionary[self.currentQuestionType]!)

                self.chatSession?.answers.append((sender?.message)!)
                if self.currentQuestionType == .HealthKitPermissions{
                    if sender?.tag == 1 {
                        PermissionsUtility.shared.askHealthPermissions()
                    }
                }
                self.processUserResponse(session: self.chatSession!, questionType: self.currentQuestionType, response : (sender?.message.content)!)
                }

        }
        
        if self.currentQuestionType == .LogWeight {
            self.chat.hideResponseView()
        } else {
            self.currentQuestionType = OnboardingQuestionType(rawValue: self.currentQuestionType.rawValue + 1)!
            let nextQuestion = self.initialQuestionsDictionary[self.currentQuestionType]
            if ((nextQuestion) == nil) {
                self.chat.hideResponseView()
                }
            }
        }
    }
    
    func processUserResponse(session: ChatSession, questionType: OnboardingQuestionType, response : String){
        self.chatSession?.needToSave = true
        switch questionType {
        case .ActivityLevel:
            self.userModel.activityLevel = ActivityLevel.enumFromString(string: response)
            PermissionsUtility.shared.askMotionPermissions()
            self.saveCurrentUser(session: session, questionType: questionType)
            break
        case .Age:
            self.saveCurrentUser(session: session, questionType: questionType)
            break
        case .Diet:
            self.userModel.dietaryPreference = DietaryPreference.enumFromString(string: response)
            self.saveCurrentUser(session: session, questionType: questionType)
            break
        case .Goal:
            self.userModel.userGoal = UserGoal.enumFromString(string: response)
            if (self.userModel.userGoal == .EatBetter) {
                self.saveCurrentUser(session: session, questionType: questionType)
            } else if self.userModel.userGoal == .LooseWeight {
                self.userModel.weightToMaintain = 0.0
                self.needToShowWeightReccomendations = true
                self.showTargetPicker()
            } else {
                self.needToShowMaintainWeightRecommendations = true
                self.userModel.weightToMaintain = self.userModel.weight
                self.saveCurrentUser(session: session, questionType: questionType)
            }
            break
        case .GoalSettings:
            PermissionsUtility.shared.askLocationPermissions()
            self.saveCurrentUser(session: session, questionType: questionType)
            break
        case .Height:
            self.saveCurrentUser(session: session, questionType: questionType)
            break
        case .Metrics:
            self.userModel.metricPreference = MetricPreference.enumFromString(string: response)
            self.saveCurrentUser(session: session, questionType: questionType)
            break
        case .Sex:
            self.userModel.biologicalSex = BiologicalSex.enumFromString(string: response)
            self.saveCurrentUser(session: session, questionType: questionType)
            break
        case .Weight:
            #if OPTIFASTVERSION
            self.userModel.weightToMaintain = self.userModel.weight
            #endif
            self.saveCurrentUser(session: session, questionType: questionType)
            break
        case .Welcome:
            self.saveCurrentUser(session: session, questionType: questionType)
            defaults.setValue(true, forKey: firstShowKey)
            break
        case .TargetLoseWeight:
            self.saveCurrentUser(session: session, questionType: questionType)
            break
        case .LogWeight:
            EventLogger.logLogsWeight()
            self.saveCurrentUser(session: session, questionType: questionType)
            break
        case .HealthKitPermissions:
            self.saveCurrentUser(session: session, questionType: questionType)
            break
        case .WeightLossRecommendations:
            self.saveCurrentUser(session: session, questionType: questionType)
            break
        case .WatchQuestion:
            //self.saveCurrentUser(session: session, questionType: questionType)
            self.saveActivitySource(session: session, source: response)
            break
        case .InfoMessage:
            self.saveCurrentUser(session: session, questionType: questionType)
            break
        case .MaintainWeightRecommendations:
            self.saveCurrentUser(session: session, questionType: questionType)
            break
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let message = self.tableData[indexPath.row]
        if message.chatBoxType == .typeDailyReport {
//            let startDayForMessage = Calendar.current.startOfDay(for: message.date)
//            let dateToShow = Calendar.current.date(byAdding: .day, value: -1, to: startDayForMessage)
//            NavigationUtility.showDetailedReportFor(date : dateToShow!)
        } else if message.chatBoxType == .typeLoggedFood {
            let realmRepo = ScoreFeedbackRepository()
            let scoreCahce =  realmRepo.getScoreFeedback(date:message.date)
            if scoreCahce.nutrientList.count >= 3 {
                self.chat.showScoreFeedback(date : message.date)
            }
        }
    }
    private func showTargetPicker () {
        DispatchQueue.main.async {
        self.tableData.append(contentsOf:self.initialQuestionsDictionary[self.currentQuestionType]!)
        self.prepareUserResponces()
        self.chat.updateChat()
        }
    }
    
    private func processSession(session : ChatSession){
        if session.needToSave {
            var arrayOfMessages : [ChatDataBaseItemViewModel] = []
            arrayOfMessages.append(contentsOf:session.questions)
            arrayOfMessages.append(contentsOf:session.answers)
            self.interactor.storeRecordsArray(array: self.viewModelToDatabaseItemMapper.transform(viewModels: arrayOfMessages))
        }
        self.chatSession = nil
    }
    
    func saveCurrentUser(session : ChatSession, questionType : OnboardingQuestionType) {
        var needToRefreshToken = true
        if defaults.bool(forKey: onBoardingKey) == false {
            needToRefreshToken = false
        }
         self.subscribtions.append(self.userProfileInteractor.storeUser(user: userMapperModel.transform(userModel: self.userModel), needToRefreshToken: needToRefreshToken).observeOn(MainScheduler.instance).subscribe(onNext: {artikError in
            if artikError != nil {
                LogUtility.logToFile("error in user saving \(String(describing: artikError!))")
            }
        }, onError: {error in
            self.chat.parseError(error : error, completion: {completed in
                if error.code == 429 {
                    self.chat.onboardingFinished()
                }
            })
        }, onCompleted: {
            LogUtility.logToFile("save to Artik completed")
            self.processSession(session: session)
            self.prepareChatHistory()
        }, onDisposed: {
        }))
    }

    func saveActivitySource(session: ChatSession ,source:String) {
        var chatMessageString : String = source
        if source == kWatchQuestionResponceMessage3 {
            KeychainWrapper.standard.set("None", forKey: kActivitySourceKey)
            self.processSession(session: session)
            chatMessageString = "Activity source can be changed from the Connect Devices menu."
            let answer = self.messageObject(type: .typeString, content: chatMessageString, fromUser: false)
            self.interactor.storeRecordsArray(array: self.viewModelToDatabaseItemMapper.transform(viewModels: [answer]))
            self.prepareChatHistory()
        } else if source == kWatchQuestionResponceMessage2 {
            //Redirect to FitBit login
            self.subscribtions.append(WithingsInteractor().pairedDevices().observeOn(MainScheduler.instance).subscribe(onNext: { [unowned self] pairedDevices in
                    if pairedDevices.count > 0 {

                    let pariedFitBitDevices = pairedDevices.filter({$0.dtidString == kFitBitDeviceTypeId})
                    
                    if pariedFitBitDevices.count > 0 {
                        let fitBitDevice = pariedFitBitDevices.first
                        
                        if !(fitBitDevice?.isAuthorized)! {
                            self.chat.openFitBitLogin()
                        } else {
                            KeychainWrapper.standard.set("Fit Bit", forKey: kActivitySourceKey)
                            self.processSession(session: session)
                            self.prepareChatHistory()
                            print("Fit Bit Authorized already")
                        }
                    }
                }
            }, onError: {error in
                self.chat.parseError(error : error, completion: {completed in
                    if error.code == 429 {
                        self.chat.onboardingFinished()
                    }
                })
            }, onCompleted: {
                
            }, onDisposed: {}))
            
        } else {
            KeychainWrapper.standard.set("Apple Watch", forKey: kActivitySourceKey)
            self.processSession(session: session)
            self.prepareChatHistory()
        }
    }
    
    private func currentRawUser() -> Observable<UserProfileModel> {
        let object = self.userProfileInteractor.getRawUser().flatMap({ userProfile -> Observable<UserProfileModel> in
            self.userModel = self.userMapperObject.transform(user: userProfile)
            return Observable.just(self.userMapperObject.transform(user: userProfile))
        })
        return object
    }
    
    
    private func preparePicker(questionType: OnboardingQuestionType) -> NRPickerView {
        let responsePicker = NRChatUtility.horizontalPickerView()
        if questionType == .Age {
            for age in 18...kMaxAge {
                self.ageDataSource.append(age)
            }
        }
        responsePicker.dataSource = self
        responsePicker.delegate = self
        responsePicker.maskDisabled = true
        responsePicker.pickerViewStyle = .flat
        responsePicker.frame = CGRect(x: 0, y: 0, width: UIScreen.main.bounds.size.width, height : 56)
        let pickerImageView = UIImageView(image: UIImage(named: kBackgroudNumberPicker)?.withRenderingMode(.alwaysOriginal))
        pickerImageView.frame = responsePicker.frame
        responsePicker.addSubview(pickerImageView)
        responsePicker.sendSubview(toBack: pickerImageView)
        switch questionType {
        case .Age:
            self.agePicker = responsePicker
            self.agePicker.reloadData()
            let index = self.ageDataSource.index(of: self.prefilledUser.age!) ?? 25
            self.agePicker.selectItem(index, animated: false, notifySelection: true)
            return self.agePicker
            
        case .Weight,.TargetLoseWeight, .LogWeight:
            self.weightPicker = responsePicker
            self.populatePickerDataSource()
            self.weightPicker.reloadData()
            
            var weight = self.getUserWeight(newWeight: self.prefilledUser.weight!)
            
            if questionType == .TargetLoseWeight {
                if Int(self.userModel.weight!) <= 65 {
                    weight = self.getUserWeight(newWeight: self.userModel.weight! - 2)
                } else {
                    weight = self.getUserWeight(newWeight: 65.0)
                }
            }
            if questionType == .LogWeight {
                weight = self.getUserWeight(newWeight: self.userModel.weight!)
            }
            let index = self.weightDataSource.index(of: String(weight))
            if index != nil {
                self.weightPicker.selectItem(index!, animated: false, notifySelection: true)
            } else {
                self.weightPicker.selectItem(self.weightDataSource.count - 1, animated: false, notifySelection: true)
            }
            return self.weightPicker
            
        case .Height:
            self.heightPicker = responsePicker
            self.populatePickerDataSource()
            self.heightPicker.reloadData()
            let height = self.getUserHeight(newHeight: String(self.prefilledUser.height ?? 1.65))
            let index = self.heightDataSource.index(of: height)
            self.heightPicker.selectItem(index!, animated: false, notifySelection: true)
            
            
            return self.heightPicker
            
        default: return responsePicker
        }
    }
    
    
    func getUserHeight(newHeight : String) -> String {
        var heightString = ""
        
        heightString = self.userModel.metricPreference == MetricPreference.Metric ?  String(format:"%.0f", NRConversionUtility.metersToCentimeter(valueInMeters: Double(newHeight)!)) :  NRConversionUtility.meresToFeetAndInch(valueInMeter: Double(newHeight)!)
        return heightString
    }
    
    func getUserWeight(newWeight : Double) -> String {
        var weight = ""
        weight = self.userModel.metricPreference == MetricPreference.Metric ?  String(format:"%.0f", newWeight) :  String(format:"%.0f",NRConversionUtility.kiloToPounds(valueInKilo: newWeight))
        
        return weight
    }

    
    func populatePickerDataSource() {
        //Change datasource of pickers based on metrics preference
        for age in 18...kMaxAge {
            self.ageDataSource.append(age)
        }
        
        
        
        //Metric - centimeter ,kilogram
       
        var count : Int = self.userModel.metricPreference == MetricPreference.Metric ? kMaxWeightMetric : kMaxWeightImperial
        self.weightDataSource.removeAll()
        
        if self.currentQuestionType == .TargetLoseWeight {
            var startWeight : Int = 0

            if self.userModel.metricPreference == .Metric {
                count = Int(self.userModel.weight! - 2)
            } else {
                let userWeight =  round(NRConversionUtility.kiloToPounds(valueInKilo: self.userModel.weight!))
                count = Int(userWeight - 5)
            }
            
            let height =  self.userModel.height!
            var expectedStartWeightInKg = Int(round((18.5) * (height * height)))
            if expectedStartWeightInKg < kMinWeightMetric {
                expectedStartWeightInKg = kMinWeightMetric
            }
            startWeight =  (self.userModel.metricPreference?.isMetric)! ?  expectedStartWeightInKg : Int(round(NRConversionUtility.kiloToPounds(valueInKilo: Double(expectedStartWeightInKg))))
            
        }
        
        
            if self.userModel.metricPreference == MetricPreference.Metric {
                for height  in kMinHeight...kMaxHeight {
                    self.heightDataSource.append(String(height))
                }
                
                
                for weight in kMinWeightMetric...count {
                    self.weightDataSource.append(String(weight))
                }
            }
            else {
                //Feet , inches , pounds
                self.heightDataSource = ["1'8''","1'9''","1'10''","1'11''"
                    ,"2'0''","2'1''","2'2''","2'3''","2'4''","2'5''","2'6''","2'7''","2'8''","2'9''","2'10''","2'11''"
                    ,"3'0''","3'1''","3'2''","3'3''","3'4''","3'5''","3'6''","3'7''","3'8''","3'9''","3'10''","3'11''",
                     "4'0''","4'1''","4'2''","4'3''","4'4''","4'5''","4'6''","4'7''","4'8''","4'9''","4'10''","4'11''",
                     "5'0''","5'1''","5'2''","5'3''","5'4''","5'5''","5'6''","5'7''","5'8''","5'9''","5'10''","5'11''",
                     "6'0''","6'1''","6'2''","6'3''","6'4''","6'5''","6'6''","6'7''","6'8''","6'9''","6'10''","6'11''"
                    ,"7'0''","7'1''","7'2''","7'3''","7'4''","7'5''","7'6''","7'7''","7'8''","7'9''","7'10''","7'11''","8'0''","8'1''","8'2''"]
                
                for weight in Int(NRConversionUtility.kiloToPounds(valueInKilo: Double(kMinWeightMetric)))...count {
                    self.weightDataSource.append(String(weight))
            }
        }
    }

    
    func prepareInitialDictionaries () {
        self.requiredOnboardingQuestions.removeAll()
        if self.tableData.count == 0 && self.interactor.getAllUserMessagesCount(userID: self.userModel.userID!) == 0 {
            let welcomeMessage = self.messageObject(type: .typeString, content: kWelcomeMessage, fromUser: false)
            self.initialQuestionsDictionary[.Welcome] = [welcomeMessage]
        
            let welcomeAnswer = self.messageObject(type: .typeString, content: kWelcomeResponseMessage, fromUser: true)
            self.initialAnswersDictionary[.Welcome] = [welcomeAnswer]
            self.requiredOnboardingQuestions.append(OnboardingQuestionType.Welcome.rawValue)

        }
        self.subscribtions.append(self.userProfileInteractor.requiredPropertiesFor(userProfile : self.userMapperModel.transform(userModel: self.userModel)).observeOn(MainScheduler.instance).subscribe(onNext: { [unowned self] props in
            let requiredProperties : [userPropertyField] = props
            for  item in requiredProperties{
                switch item {
                    case .userGoal:
                        #if !OPTIFASTVERSION
                            let goalMessage = self.messageObject(type: .typeString, content: kUserGoalMessage, fromUser: false)
                            self.initialQuestionsDictionary[.Goal] = [goalMessage]
                        
                            let goalAnswerOne = self.messageObject(type: .typeString, content: kGoalResponseMessage1, fromUser: true)
                            let goalAnswerTwo = self.messageObject(type: .typeString, content: kGoalResponseMessage2, fromUser: true)
                            let goalAnswerThree = self.messageObject(type: .typeString, content: kGoalResponseMessage3, fromUser: true)
                            self.initialAnswersDictionary[.Goal] = [goalAnswerOne, goalAnswerTwo, goalAnswerThree]
                            self.requiredOnboardingQuestions.append(OnboardingQuestionType.Goal.rawValue)
                        
                            let loseWeightMessage = self.messageObject(type: .typeString, content: kTargetWeightMessage, fromUser: false)
                            self.initialQuestionsDictionary[.TargetLoseWeight] = [loseWeightMessage]
                        
                            let emptyMessage = self.messageObject(type: .typeString, content: "", fromUser: true)
                            self.initialAnswersDictionary[.TargetLoseWeight] = [emptyMessage]
                            self.requiredOnboardingQuestions.append(OnboardingQuestionType.TargetLoseWeight.rawValue)
                        #endif
                    break
                case .biologicalSex:
                        let biologicalSexMessage = self.messageObject(type: .typeString, content: kProfileSetupMessage, fromUser: false)
                        self.initialQuestionsDictionary[.Sex] = [biologicalSexMessage]
                        
                        let maleAnswer = self.messageObject(type: .typeString, content: kSexResponseMessage1, fromUser: true)
                        let femaleAnswer = self.messageObject(type: .typeString, content: kSexResponseMessage2, fromUser: true)
                        self.initialAnswersDictionary[.Sex] = [maleAnswer, femaleAnswer]
                        self.requiredOnboardingQuestions.append(OnboardingQuestionType.Sex.rawValue)
                        
                    break
                case .age:
                        let ageMessage = self.messageObject(type: .typeString, content: kAgeMessage, fromUser: false)
                        self.initialQuestionsDictionary[.Age] = [ageMessage]
                        
                        let emptyMessage = self.messageObject(type: .typeString, content: "", fromUser: true)
                        self.initialAnswersDictionary[.Age] = [emptyMessage]
                        
                        self.requiredOnboardingQuestions.append(OnboardingQuestionType.Age.rawValue)
                    break
                case .metricPreference:
                        let metricsMessage = self.messageObject(type: .typeString, content: kMetricsPreferenceMessage, fromUser: false)
                        self.initialQuestionsDictionary[.Metrics] = [metricsMessage]
                        
                        let metricsAnswerCM = self.messageObject(type: .typeString, content: kMetricsResponseMessage1, fromUser: true)
                        let metricAnswerInch = self.messageObject(type: .typeString, content: kMetricsResponseMessage2, fromUser: true)
                        self.initialAnswersDictionary[.Metrics] = [metricsAnswerCM, metricAnswerInch]
                        
                        self.requiredOnboardingQuestions.append(OnboardingQuestionType.Metrics.rawValue)
                    break
                case .height:
                        let heightMessage = self.messageObject(type: .typeString, content: kHeightMessage, fromUser: false)
                        self.initialQuestionsDictionary[.Height] = [heightMessage]
                        
                        let emptyMessage = self.messageObject(type: .typeString, content: "", fromUser: true)
                        self.initialAnswersDictionary[.Height] = [emptyMessage]
                        
                        self.requiredOnboardingQuestions.append(OnboardingQuestionType.Height.rawValue)

                    break
                case .weight:
                    let weightMessage = self.messageObject(type: .typeString, content: kWeightMessage, fromUser: false)
                    self.initialQuestionsDictionary[.Weight] = [weightMessage]
                    
                    let emptyMessage = self.messageObject(type: .typeString, content: "", fromUser: true)
                    self.initialAnswersDictionary[.Weight] = [emptyMessage]
                    
                    
                    self.requiredOnboardingQuestions.append(OnboardingQuestionType.Weight.rawValue)
                    break
                case .goalRounder:
                    let goalRounderMessage = self.messageObject(type: .typeString, content: kGoalPreferenceMessage, fromUser: false)
                    self.initialQuestionsDictionary[.GoalSettings] = [goalRounderMessage]
                    
                    let goalConfirmationAnswer = self.messageObject(type: .typeString, content: kGoalConfirmationResponseMessage, fromUser: true)
                    self.initialAnswersDictionary[.GoalSettings] = [goalConfirmationAnswer]
                    
                    self.requiredOnboardingQuestions.append(OnboardingQuestionType.GoalSettings.rawValue)
                    
                    break
                case .dietaryPreference:
                    let dietaryPreferenceMessage = self.messageObject(type: .typeString, content: kDietPreferenceMessage, fromUser: false)
                    self.initialQuestionsDictionary[.Diet] = [dietaryPreferenceMessage]
                    
                    let dietaryAnswerTwo = self.messageObject(type: .typeString, content: kDietResponseMessage3, fromUser: true)
                    let dietaryAnswerOne = self.messageObject(type: .typeString, content: kDietResponseMessage1, fromUser: true)
                    self.initialAnswersDictionary[.Diet] = [dietaryAnswerOne,dietaryAnswerTwo]
                    
                    self.requiredOnboardingQuestions.append(OnboardingQuestionType.Diet.rawValue)
                
                    break
                case .activityLevel:
                    let activityLevelMessage = self.messageObject(type: .typeString, content: kActivityLevelMessage, fromUser: false)
                    self.initialQuestionsDictionary[.ActivityLevel] = [activityLevelMessage]
                    
                    let activityLevelAnswerOne = self.messageObject(type: .typeString, content: kActiveLevelResponseMessage1, fromUser: true)
                    let activityLevelAnswerTwo = self.messageObject(type: .typeString, content: kActivityLevelResponseMessage2, fromUser: true)
                    let activityLevelAnswerThree = self.messageObject(type: .typeString, content: kActiveLevelResponseMessage3, fromUser: true)
                    let activityLevelAnswerFour = self.messageObject(type: .typeString, content: kActiveLevelResponseMessage4, fromUser: true)
                    self.initialAnswersDictionary[.ActivityLevel] = [activityLevelAnswerOne,activityLevelAnswerTwo,activityLevelAnswerThree,activityLevelAnswerFour]
                    
                    self.requiredOnboardingQuestions.append(OnboardingQuestionType.ActivityLevel.rawValue)
                
                    break
                    
                }
                
            }
        }, onError: {error in
            self.chat.parseError(error : error, completion: {completed in
                if error.code == 429 {
                    self.chat.onboardingFinished()
                }
            })

        }, onCompleted: {
            self.addAditionalQuestions()
            if (self.requiredOnboardingQuestions.count != 0) {
                self.chat.updateChat()
                self.currentQuestionType = OnboardingQuestionType(rawValue: self.requiredOnboardingQuestions.min()!)!
                let arrayOfQuestions = self.initialQuestionsDictionary[self.currentQuestionType]!
                for message in arrayOfQuestions {
                    self.tableData.append(message)
                    let path = IndexPath(row: self.tableData.count - 1 , section: 0)
                    self.chat.insertRowAtIndexPath(path : [path] ,fromUser : false)
                    
                }
                //self.tableData.append(contentsOf:)
                self.chatSession = ChatSession()
                self.prepareUserResponces()
            } else {
                self.synchronizeFitBitActivities()
                let lastMessage = self.tableData.last
                if lastMessage?.chatBoxType == .typeLogWeight {
                    self.chat.hideTable()
                    self.prepareLogWeightViews()
                } else {
                    //self.synchronizeFitBitActivities()
                    defaults.set(true, forKey: onBoardingKey)
                    if defaults.value(forKey: kOnBoardingKeyDate) == nil {
                        self.processSentiancePermissions()
                        defaults.set(Date(), forKey: kOnBoardingKeyDate)
                        defaults.synchronize()
                        #if OPTIFASTVERSION
                            self.userModel.userGoal = UserGoal.MaintainWeight
                            self.userModel.weightToMaintain = self.userModel.weight
                        #endif
                        self.subscribtions.append(self.userProfileInteractor.storeUser(user: self.userMapperModel.transform(userModel: self.userModel), needToRefreshToken: true).observeOn(MainScheduler.instance).subscribe(onNext: {error in }))
                        self.synchronizeWithings()
                    } else {
                      self.synchronizeFitBitActivities()
                    }
                    let lastMessage = self.tableData.last
                    if lastMessage?.chatBoxType == .typeLogWeight {
                        self.chat.hideTable()
                        self.prepareLogWeightViews()
                    } else {
                        self.chat.onboardingFinished()
                    }
                
                    self.chat.updateChat()
                }
            
                self.chat.updateChat()
            }
        }, onDisposed: {}))
        
    }
    private func processSentiancePermissions() {
        NotificationCenter.default.addObserver(self, selector: #selector(askLocationPermissions), name: NSNotification.Name("Motion permissions was asked"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(initSentiance), name: NSNotification.Name("Location permissions was asked"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(startFitBitSync), name: NSNotification.Name("Health permissions was asked"), object: nil)

        PermissionsUtility.shared.askMotionPermissions()
    }
    
    @objc private func startFitBitSync () {
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name("Health permissions was asked"), object: nil)
        let dispatchTime = DispatchTime.now() + 0.5
        DispatchQueue.main.asyncAfter(deadline: dispatchTime) {
            self.synchronizeFitBitActivities()
        }
    }
    
    @objc private func askLocationPermissions () {
        PermissionsUtility.shared.askLocationPermissions()
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name("Motion permissions was asked"), object: nil)
    }
    
    @objc private func initSentiance () {
        PermissionsUtility.shared.initSentianceSDK(launchOptions: nil)
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name("Location permissions was asked"), object: nil)
    
    }
    
    private func addAditionalQuestions () {
        if PermissionsUtility.shared.checkHealthKitPermissionsWasAsked() == false && self.tableData.contains(where: { message in message.chatBoxType == .typeHealthKitPermissions }) == false {
            let healthKitMessage = self.messageObject(type: .typeHealthKitPermissions, content: kAppleHealthPermissionsMessage, fromUser: false)
            self.initialQuestionsDictionary[.HealthKitPermissions] = [healthKitMessage]
            
            let healtKitAnswerNo = self.messageObject(type: .typeHealthKitPermissions, content: kAppleHealthResponseMessage, fromUser: true)
            let healthKItAnswerYes = self.messageObject(type: .typeHealthKitPermissions, content: kAppleHealthResponseMessage2, fromUser: true)
            self.initialAnswersDictionary[.HealthKitPermissions] = [healtKitAnswerNo, healthKItAnswerYes]
            
            self.requiredOnboardingQuestions.append(OnboardingQuestionType.HealthKitPermissions.rawValue)
        }
        
        
        if defaults.bool(forKey: onBoardingKey) != true && KeychainWrapper.standard.string(forKey: kActivitySourceKey) == nil {
            let watchMessage = self.messageObject(type: .typeWatchMessage, content: kWatchQuestionMessage, fromUser: false)
            self.initialQuestionsDictionary[.WatchQuestion] = [watchMessage]
            
            let watchAnswer = self.messageObject(type: .typeWatchMessage, content: kWatchQuestionResponceMessage, fromUser: true)
            let watchAnswerTwo = self.messageObject(type: .typeWatchMessage, content: kWatchQuestionResponceMessage2, fromUser: true)
            let watchAnswerThree = self.messageObject(type: .typeWatchMessage, content: kWatchQuestionResponceMessage3, fromUser: true)

            self.initialAnswersDictionary[.WatchQuestion] = [watchAnswer, watchAnswerTwo,watchAnswerThree]
            
            self.requiredOnboardingQuestions.append(OnboardingQuestionType.WatchQuestion.rawValue)

        }
        if defaults.bool(forKey: onBoardingKey) != true && self.tableData.contains(where: { message in message.chatBoxType == .typeWeightlossRecommendationMessage }) == false && self.userModel.userGoal == .LooseWeight  && self.needToShowWeightReccomendations == true{
            let weightLossReccomendationMessage = self.messageObject(type: .typeWeightlossRecommendationMessage, content: kWeightLossRecommendationMessage, fromUser: false)
            let weeklyLoss = self.convertWeight(newWeight: self.calculateWeeklyLoss())
            let weightLossReccomendationMessage2 = self.messageObject(type: .typeWeightlossRecommendationMessage, content: String(format: kWeightLossRecommendationMessage2, weeklyLoss, (self.userModel.metricPreference?.descriptionForChat)!), fromUser: false)
            self.initialQuestionsDictionary[.WeightLossRecommendations] = [weightLossReccomendationMessage,weightLossReccomendationMessage2]
                
            let weightLossRecAnswer = self.messageObject(type: .typeWeightlossRecommendationMessage, content: kWeightLossRecommendationResponseMessage, fromUser: true)
                
            self.initialAnswersDictionary[.WeightLossRecommendations] = [weightLossRecAnswer]
            self.requiredOnboardingQuestions.append(OnboardingQuestionType.WeightLossRecommendations.rawValue)
        }
        
        if  defaults.bool(forKey: onBoardingKey) != true && self.tableData.contains(where: { message in message.chatBoxType == .typeInfoMessage }) == false {
            let infoMessage = self.messageObject(type: .typeInfoMessage, content: "", fromUser: false)
            self.initialQuestionsDictionary[.InfoMessage] = [infoMessage]
            self.requiredOnboardingQuestions.append(OnboardingQuestionType.InfoMessage.rawValue)
        }
        if  defaults.bool(forKey: onBoardingKey) != true && self.tableData.contains(where: { message in message.chatBoxType == .typeMaintainWeightReccomendationMessage }) == false && self.needToShowMaintainWeightRecommendations == true && self.userModel.userGoal == .MaintainWeight {
            let maintatainMessage = self.messageObject(type: .typeMaintainWeightReccomendationMessage, content: kMaintainWeightRecommendationMessage, fromUser: false)
            self.initialQuestionsDictionary[.MaintainWeightRecommendations] = [maintatainMessage]
            
            let maintainMessageAnswer = self.messageObject(type: .typeMaintainWeightReccomendationMessage, content: kWeightLossRecommendationResponseMessage, fromUser: true)
            self.initialAnswersDictionary[.MaintainWeightRecommendations] = [maintainMessageAnswer]
        
            self.requiredOnboardingQuestions.append(OnboardingQuestionType.MaintainWeightRecommendations.rawValue)
        }
        
        
    }
    
    private func calculateWeeklyLoss () -> Double{
        let userProfile = self.userMapperModel.transform(userModel: self.userModel)
        let bmr = userProfile.getBMRCalories(weight: self.userModel.weight!, height: self.userModel.height!, date: Date(), isCurrentDay: false)
        let calorieNeed = userProfile.getWeightKeepCalorieBudget(bmrValue: bmr)
        let bmrDiff = calorieNeed - 1000
        let weeklyWeightLoss = bmrDiff * 2 / bmr
        return weeklyWeightLoss
    }
    
    
    private func prepareLogWeightViews () {
        
        self.chatSession = ChatSession()
        let emptyMessage = self.messageObject(type: .typeWeightRecord, content: "", fromUser: true)
        let skipMessage = self.messageObject(type: .typeWeightRecord, content: "Skip", fromUser: true)
        self.currentQuestionType = .LogWeight
        self.initialAnswersDictionary[.LogWeight] = [skipMessage,emptyMessage]
        self.chatSession = ChatSession()
        self.prepareUserResponces()


    }
    
    
    // Picker delegate and data source
    func numberOfItemsInPickerView(_ pickerView: NRPickerView) -> Int {
        if (self.currentQuestionType == .Age) {
            return self.ageDataSource.count
        } else if (self.currentQuestionType == .Height) {
            return self.heightDataSource.count
        } else  if (self.currentQuestionType == .Weight || self.currentQuestionType == .TargetLoseWeight || self.currentQuestionType == .LogWeight){
            return self.weightDataSource.count
        }
        return 0
    }
    
    func pickerView(_ pickerView: NRPickerView, titleForItem item: Int) -> String {
        if (self.currentQuestionType == .Age){
            if item >= self.ageDataSource.count {
                return ""
            }

            return String(self.ageDataSource[item])
        }
        else if (self.currentQuestionType == .Height) {
            if item >= self.heightDataSource.count {
                return ""
            }
            return self.heightDataSource[item]
        } else if (self.currentQuestionType == .Weight || self.currentQuestionType == .TargetLoseWeight || self.currentQuestionType == .LogWeight) {
            if item >= self.weightDataSource.count {
                return ""
            }
            return String(self.weightDataSource[item])
        }
        return ""
    }
    
    
    
    func pickerView(_ pickerView: NRPickerView, didSelectItem item: Int) {
        //Update the corresponding response button title
        var title = ""
        if (self.currentQuestionType == .Age) {
            let age = Int(self.ageDataSource[item])
            title = String(age)
            self.userModel.age = age
        } else if (self.currentQuestionType == .Height) {
            let newHeight : Double = self.userModel.metricPreference == MetricPreference.Metric ? Double(self.convertHeight(newHeight: Double(self.heightDataSource[item])!)) : NRConversionUtility.feetAndInchToMeters(feetAndInch: self.heightDataSource[item])
            self.userModel.height = newHeight
            title = self.heightDataSource[item]
        } else if (self.currentQuestionType == .Weight || self.currentQuestionType == .LogWeight) {
            let weight = self.convertWeight(newWeight:Double(self.weightDataSource[item])!)
            self.userModel.weight = weight
            title = self.weightDataSource[item]
        } else if (self.currentQuestionType == .TargetLoseWeight) {
            let weight = self.convertWeight(newWeight:Double(self.weightDataSource[item])!)
            self.userModel.weightLossStartDate = Date()
            self.userModel.goalValue = weight
            self.userModel.startWeight = self.userModel.weight
            title = self.weightDataSource[item]

        }
    
        for v in self.responseViews {
            if v.isKind(of: ResponseButton.self) {
                let button =  v as! ResponseButton
                if self.currentQuestionType == .LogWeight{
                    if button.tag == 1 {
                        let content : String = String(format:"%@ %@",title, (self.userModel.metricPreference?.descriptionForChat)!)
                        button.message.content = content
                        button.setTitle(content, for: .normal)
                    }
                } else if self.currentQuestionType == .Weight || self.currentQuestionType == .TargetLoseWeight {
                    let content : String = String(format:"%@ %@",title, (self.userModel.metricPreference?.descriptionForChat)!)
                    button.message.content = content
                    button.setTitle(content, for: .normal)
                }
                else {
                    button.message.content = title
                    button.setTitle(title, for: .normal)
                }
            }
        }
    }

    func convertHeight(newHeight : Double) -> Double {
        let height : Double = self.userModel.metricPreference == MetricPreference.Metric ? NRConversionUtility.centimeterToMeter(valueInCenti: newHeight) :  NRConversionUtility.feetAndInchToMeters(feetAndInch: String(format:"%0.f",newHeight))
        return height
    }
    
    func convertWeight(newWeight : Double) -> Double {
        let weight : Double =  self.userModel.metricPreference == MetricPreference.Metric ? newWeight :  NRConversionUtility.poundsToKil(valueInPounds:newWeight)
        return weight
    }
    
}

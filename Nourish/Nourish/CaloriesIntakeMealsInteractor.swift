//
//  CaloriesIntakeMealsInteractor.swift
//  Nourish
//
//  Created by Vlad Birukov on 20.06.17.
//  Copyright © 2017 SSIC. All rights reserved.
//

import Foundation
import RxSwift

class CaloriesIntakeMealsInteractor {
    let repository = NestleProgressRepository.shared
    
    func execute(startDate: Date, endDate: Date) -> Observable<CaloriesIntakeMealsModel> {
        return repository.getProgressFor(startDate: startDate, endDate: endDate, returnHistory: true).flatMap({observable -> Observable<CaloriesIntakeMealsModel> in
            
            return Observable.just(self.transform(model: observable))
        })
    }
    
    private func transform(model: Progress) ->CaloriesIntakeMealsModel {
        let caloriesIntakeMeals = CaloriesIntakeMealsModel()
        let consumedCalories = model.userCallories.consumedCalories
        let nameFoodIntake = [Ocasion.breakfast, Ocasion.lunch, Ocasion.dinner, Ocasion.snacks]
        
        for i in 0..<nameFoodIntake.count {
            let currentFood = model.userOcasion.filter({Ocasion.enumFromMealPlanner(string: $0.nameFoodIntake) == nameFoodIntake[i] })
            let caloriesMealModel = CaloriesMealModel()
            caloriesMealModel.type = nameFoodIntake[i]
            
            if currentFood.count > 0 {
                var sumCaloriesCurrent = 0.0
                for food in currentFood {
                    sumCaloriesCurrent = sumCaloriesCurrent + food.consumCalories
                }
                if consumedCalories != 0{
                    caloriesMealModel.percent = (sumCaloriesCurrent / consumedCalories) * 100
                }
            }
            caloriesIntakeMeals.arrCaloriesMeal.append(caloriesMealModel)
        }
        
        return caloriesIntakeMeals
    }
}

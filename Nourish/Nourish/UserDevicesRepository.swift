//
//  UserDevicesRepository.swift
//  Nourish
//
//  Created by Nova on 12/7/16.
//  Copyright © 2016 SSIC. All rights reserved.
//

import UIKit
import RxSwift
let WITHINGS_DTID = "dt29673f0481b4401bb73a622353b96150"
let WITHINGS_NAME = "Withings Device"
let kFitBitDeviceTypeId = "dt8e71cabde68b4028b106832247cd6d72"
let kFitBitDeviceName = "FitBit Device"

final class UserDevicesRepository: NSObject {
    let cache = UserDeviceCache()
    let artikStore = ArtikCloudConnectionManager()
    let fromJsonMapper = Json_to_UserDevice()
    let userRepo = UserRepository.shared
    static let shared = UserDevicesRepository()
    /*
    func defaultDevice() -> UserDevice {
       return cache.defaultDevice()
    }
    */
    
    func currentDevice(userID : String) -> Observable<UserDevice> {
        return cache.currentDevice(userID : userID)
    }
 /*
    func defaultUdidString() -> String {
        let device = cache.currentDevice()
        if device != nil {
            return device.udidString
        } else {
            return cache.defaultDevice().udidString
        }
    }
*/
    
    func createWithingsDevice() -> Observable<[UserDevice]> {
        return self.createDevice(deviceType: WITHINGS_DTID, name: WITHINGS_NAME)
    }
    
    func createFitBitDevice() -> Observable<[UserDevice]> {
        return self.createDevice(deviceType: kFitBitDeviceTypeId, name: kFitBitDeviceName)
    }
    
    func createDevice(deviceType:String , name:String) -> Observable<[UserDevice]> {
        return self.userRepo.getCurrentUser(policy: .DefaultPolicy).flatMap({userProfile -> Observable<[UserDevice]> in
            if userProfile.userID != nil {
                let device = UserDevice()
                device.dtidString = deviceType
                device.deviceName = name
                device.userID = userProfile.userID!
                return self.artikStore.saveDevice(device: device).flatMap({dictionary -> Observable<[UserDevice]> in
                    let mapper = Json_to_UserDevice()
                    let userDevice = mapper.transform(device: dictionary, userID: userProfile.userID!)
                    return Observable.just([userDevice])
                })
                
            } else {
                return Observable.just([])
            }
        })
    }
    
    
    func storeDevices(devices : [UserDevice]) {
        for device in devices {
            if device.dtidString == kDeviceID && device.deviceName == kDeviceString  {
                self.storeUserDevice(device: device)
            }
        }
    }
    
    func authorizationURLForDevice(device : UserDevice) -> Observable<URL> {
        return self.artikStore.authorizeDevice(device:device)
    }
    
    func unauthorizeDevice(device : UserDevice) -> Observable<Bool> {
        return self.artikStore.unathorizeDevice(device : device)
    }
    
    
    func userDevices() -> Observable<[UserDevice]> {
        return self.userRepo.getCurrentUser(policy: .ForceReload).flatMap({userProfile -> Observable<[UserDevice]> in
            if userProfile.userID != nil {
                return self.artikStore.getUserDevices().flatMap({devicesList -> Observable<[UserDevice]> in
                    let devices = self.fromJsonMapper.transform(devices: devicesList, userID: userProfile.userID!)
                    self.storeDevices(devices: devices)
                    return Observable.just(devices)
                })
            } else {
                return Observable.just([])
            }
        })
    }
    
 /*
    func userDevices(policy : FetchingPolicy) -> Observable<UserDevice> {
        switch policy {
            
        case .Cached:
            return self.userRepo.getCurrentUser(policy: .Cached).flatMap {user -> Observable<UserDevice> in
                return self.cache.currentDevice(userID: user.userID!)
            }
        case .ForceReload:
            let userRepo = UserRepository.shared
            let user = self.userRepo.getCurrentUser(policy: .DefaultPolicy).flatMap { user ->
                Observable<UserDevice> in
                if user.userID != nil {
                    return self.artikStore.getUserDevices().flatMap({ devicelist -> Observable<UserDevice> in
                        let devices = self.fromJsonMapper.transform(devices: devicelist,userID : user.userID!)
                        self.storeDevices(devices: devices)
                        return Observable.just(devices)
                    })
                } else {
                    return Observable.just([self.defaultDevice()])
                }
            }
            /*
            let object = artikStore.getUserDevices().flatMap({ devicelist -> Observable<[UserDevice]> in
                let devices = self.fromJsonMapper.transform(devices: devicelist)
                self.storeDevices(devices: devices)
                return Observable.just(devices)
            })
 */
            return user
            
        case .DefaultPolicy:
            if cache.past5minutesLastSync() == true {
                return self.userRepo.getCurrentUser(policy: .Cached).flatMap {user -> Observable<UserDevice> in
                    return self.cache.currentDevice(userID: user.userID!)
                }
                
            } else {
                let user = self.userRepo.getCurrentUser(policy: .DefaultPolicy).flatMap { user ->
                    Observable<UserDevice> in
                    if user.userID != nil {
                        return self.artikStore.getUserDevices().flatMap({ devicelist -> Observable<[UserDevice]> in
                            let devices = self.fromJsonMapper.transform(devices: devicelist, userID : user.userID!)
                            self.storeDevices(devices: devices)
                            return Observable.just(devices)
                        })
                    } else {
                        return Observable.just([self.defaultDevice()])
                    }
                }
                return user
            }
        }
    }
 */
    
    func storeUserDevice(device : UserDevice) {
        cache.storeCurrentDevuce(device: device)
    }
}

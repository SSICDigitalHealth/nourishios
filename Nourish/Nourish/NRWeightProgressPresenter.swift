//
//  NRWeightProgressPresenter.swift
//  Nourish
//
//  Created by Vino (Vinodha) Sundaramoorthy on 12/6/16.
//  Copyright © 2016 SSIC. All rights reserved.
//

import UIKit
import RxSwift

class NRWeightProgressPresenter: BasePresenter {
    
    let interactor = NRWeightProgressInteractor()
    var weightProgressView : NRWeightProgressProtocol!
    let mapperObject = NRWeightProgress_toNRWeightProgressModel()
    let mapperModel = NRWeightProgressModel_toNRWeightProgress()
    
    // MARK:BasePresenterProrocol Implementation
    override func viewWillAppear(_ animated : Bool) {
        if weightProgressView != nil {
            let _ = interactor.weightProgress().observeOn(MainScheduler.instance).subscribe(onNext: { [unowned self] object in
                let model = self.mapperObject.transform(weight: object)
                self.weightProgressView.setupWith(weightProgress: model)
            }, onError: {error in
            }, onCompleted: {
            }, onDisposed: {
            })
        }
    }

}

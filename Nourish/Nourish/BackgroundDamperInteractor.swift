//
//  BackgroundDamperInteractor.swift
//  NourIQ
//
//  Created by Igor on 12/23/17.
//  Copyright © 2017 SSIC. All rights reserved.
//

import UIKit

class BackgroundDamperInteractor: NSObject {

    let diaryDumper = BackgroundDiaryDumperRepository()
    let healthDumper = BackgroundActivityDumperRepository()
    
    func execute() {
        self.diaryDumper.startDump()
        self.healthDumper.startDump()
    }
}

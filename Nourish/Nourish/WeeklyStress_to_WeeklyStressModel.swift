//
//  WeeklyStress_to_WeeklyStressModel.swift
//  Nourish
//
//  Created by Gena Mironchyk on 1/23/17.
//  Copyright © 2017 SSIC. All rights reserved.
//

import UIKit

class WeeklyStress_to_WeeklyStressModel : NSObject {
    
    func transform(weeklyStress : WeeklyStress) -> WeeklyStressModel {
        let model = WeeklyStressModel()
        model.avgBPM = weeklyStress.avgBPM
        model.maxBPM = weeklyStress.maxBPM
        model.minBPM = weeklyStress.minBPM
        model.weeklyArray = weeklyStress.weeklyArray
        return model
    }
    
    func transform(array : [WeeklyStress]) -> [WeeklyStressModel] {
        var modelsArray : [WeeklyStressModel] = []
        for stress in array {
            modelsArray.append(self.transform(weeklyStress : stress))
        }
        return modelsArray
    }
}

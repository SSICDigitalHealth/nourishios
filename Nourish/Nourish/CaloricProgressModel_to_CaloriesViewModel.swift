//
//  CaloricProgressModel_to_CaloriesViewModel.swift
//  Nourish
//
//  Created by Vlad Birukov on 19.09.17.
//  Copyright © 2017 SSIC. All rights reserved.
//

import Foundation

class CaloricProgressModel_to_CaloriesViewModel {
    func transform(model: CaloricProgressModel) -> CaloriesViewModel {
        let caloriesViewModel = CaloriesViewModel()
        if let caloricState = model.caloricState {
            caloriesViewModel.caloricState = caloricState
        }
        
        return caloriesViewModel
    }
}

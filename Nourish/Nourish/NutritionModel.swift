//
//  NutritionModel.swift
//  Nourish
//
//  Created by Nova on 11/21/16.
//  Copyright © 2016 SSIC. All rights reserved.
//

import UIKit

class NutritionModel: NSObject {
    var vitamins : Double = 0.0
    var electrolytes : Double = 0.0
    var minerals : Double = 0.0
    var macronutrients : Double = 0.0
    var nutritionScore : Double = 0.0
}

//
//  ScoreFeedbackRepository.swift
//  Nourish
//
//  Created by Vino (Vinodha) Sundaramoorthy on 4/18/17.
//  Copyright © 2017 SSIC. All rights reserved.
//

import UIKit
import RealmSwift

class ScoreFeedbackRepository: NSObject {
    
    func storeScoreFeedback(scoreModel : ScoreCacheRealm) {
        let realm = try! Realm()
        let dateInRealmCache = realm.objects(ScoreCacheRealm.self).sorted(byKeyPath: "date", ascending: true).last?.date
        
        let calendar = Calendar.current
        
        //If top object is not today , then delete all the feedback
        if dateInRealmCache != nil {
            if !calendar.isDateInToday(dateInRealmCache!) {
                self.deleteAllScoreFeedback()
            }
        }
        
        try! realm.write {
            realm.add(scoreModel)
        }
    }
    
    
    func getScoreFeedback(date:Date) -> ScoreCacheRealm {
        let predicate = NSPredicate(format : "date == %@",date as NSDate)
        let searchResults = try? Realm().objects(ScoreCacheRealm.self).filter(predicate)
        if searchResults != nil && (searchResults?.count)! > 0 {
            return (searchResults?[0])!
        }
        return ScoreCacheRealm()
    }

    func deleteAllFeedback(model : ScoreCacheRealm) {
        let realm = try! Realm()
        let searchResults = try! Realm().objects(ScoreCacheRealm.self)
        for object in searchResults {
            if object != model {
                try! realm.write {
                    realm.delete(object, cascading: true)
                }
            }
        }
    }
    
    func deleteAllScoreFeedback() {
        let realm = try! Realm()
        let searchResults = realm.objects(ScoreCacheRealm.self)
        if searchResults.count > 0 {
            for user in searchResults {
                try! realm.write {
                    realm.delete(user, cascading: true)
                }
            }
        }
    }

}

//
//  PermissionsRequestView.swift
//  Nourish
//
//  Created by Gena Mironchyk on 6/14/17.
//  Copyright © 2017 SSIC. All rights reserved.
//

import UIKit

class PermissionsRequestView: BaseView {
    
    @IBOutlet weak var gotItButton : UIButton!
    @IBOutlet weak var presenter = PermissionRequestPresenter()
    
    override func viewDidLoad() {
        let view = UINib(nibName: "PermissionsRequestView", bundle: nil).instantiate(withOwner: self, options: nil)[0] as! UIView
        view.translatesAutoresizingMaskIntoConstraints = false
        self.translatesAutoresizingMaskIntoConstraints = false
        self.addSubview(view)
        NSLayoutConstraint(item: view, attribute: .leading, relatedBy: .equal, toItem: self, attribute: .leading, multiplier: 1, constant: 0).isActive = true
        NSLayoutConstraint(item: view, attribute: .trailing, relatedBy: .equal, toItem: self, attribute: .trailing, multiplier: 1, constant: 0).isActive = true
        NSLayoutConstraint(item: view, attribute: .top, relatedBy: .equal, toItem: self, attribute: .top, multiplier: 1, constant: 0).isActive = true
        NSLayoutConstraint(item: view, attribute: .bottom, relatedBy: .equal, toItem: self, attribute: .bottom, multiplier: 1, constant: 0).isActive = true
        self.gotItButton.layer.cornerRadius = 24.0
        self.gotItButton.backgroundColor = NRColorUtility.appBackgroundColor()
        self.basePresenter = presenter
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.stopActivityAnimation()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
    }
    
}

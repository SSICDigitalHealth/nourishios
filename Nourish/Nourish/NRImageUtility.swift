//
//  NRImageUtility.swift
//  Nourish
//
//  Created by Vino (Vinodha) Sundaramoorthy on 9/15/17.
//  Copyright © 2017 SSIC. All rights reserved.
//

import UIKit

class NRImageUtility: NSObject {

    class func saveImageAsProfilePic(image:UIImage,userId:String) {
        let documentsDirectoryURL = try! FileManager().url(for: .documentDirectory, in: .userDomainMask, appropriateFor: nil, create: true)
        let fileURL = documentsDirectoryURL.appendingPathComponent(String(format:"%@_%@.jpg",kProfilePicturePath,userId))
 
        do {
            try UIImagePNGRepresentation(image)!.write(to: fileURL)
            LogUtility.logToFile("Profile picture was added successfully")
        } catch {
            LogUtility.logToFile("Error saving profile picture ", error)
        }
    }
    
    class func getProfilePicture(isGlobalDrawer:Bool,userId:String) -> UIImage {
        let paths = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0] as String
        
        let url = URL(fileURLWithPath: paths).appendingPathComponent(String(format:"%@_%@.jpg",kProfilePicturePath,userId))
        
        if UIImage(contentsOfFile: url.path) != nil {
            return UIImage(contentsOfFile: url.path)!
        } else {
            return  isGlobalDrawer ? UIImage(named:"default_avatar_small")! : UIImage(named:"default_avatar")!
        }
    }
}

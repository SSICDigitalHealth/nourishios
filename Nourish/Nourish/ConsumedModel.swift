//
//  ConsumedModel.swift
//  Nourish
//
//  Created by Vlad Birukov on 31.08.17.
//  Copyright © 2017 SSIC. All rights reserved.
//

import Foundation

class ConsumedModel {
    var consumedCalories: Double?
    var avgConsumed: Double?
}

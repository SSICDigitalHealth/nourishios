//
//  Element.swift
//  Nourish
//
//  Created by Vlad Birukov on 27.06.17.
//  Copyright © 2017 SSIC. All rights reserved.
//

import Foundation


class Element: NSObject {
    var consumCalories: Double = 0.0
    var nameFood = ""
    var amountFood: Double?
    var unit: String?
    var nameFoodIntake = ""
    
    override init() {
    }
    
    init(nameFood: String, consumedCalories: Double) {
        self.nameFood = nameFood
        self.consumCalories = consumedCalories
    }
    
}

//
//  WeightModel.swift
//  Nourish
//
//  Created by Vlad Birukov on 28.08.17.
//  Copyright © 2017 SSIC. All rights reserved.
//

import Foundation

typealias weightList = (subtitle:Date, value:Double?)

class WeightModel {
    var weight: [weightList]?
    var progress: Double?
    var isMetric: Bool = false
    var dailyWeightLogged: Bool = false
    var userId : String?
}

//
//  StartMealPlannerView.swift
//  Nourish
//
//  Created by Vlad Birukov on 18.08.17.
//  Copyright © 2017 SSIC. All rights reserved.
//

import UIKit

protocol NouriqStartMealPlannerProtocol : BaseViewProtocol{
    func setUpWithModel(model: NouriqStartMealPlannerViewModel)
}

class NouriqStartMealPlannerView: BaseView, NouriqStartMealPlannerProtocol {
    
    @IBOutlet var presenter: NouriqStartMealPlannerPresenter!
    @IBOutlet weak var goalLabel: UILabel!
    @IBOutlet weak var dietLabel: UILabel!
    @IBOutlet weak var firstMessage: UILabel!
    @IBOutlet weak var secondMessage: UILabel!
    @IBOutlet weak var thirdMessage: UILabel!
    @IBOutlet weak var titleTop: NSLayoutConstraint!
    @IBOutlet weak var firstTop: NSLayoutConstraint!
    @IBOutlet weak var firstBotton: NSLayoutConstraint!
    @IBOutlet weak var secondMessageButton: NSLayoutConstraint!
    @IBOutlet weak var secondMessageTop: NSLayoutConstraint!
    
    override func viewDidLoad() {
        self.xibAutoLayouting(nibName: "NouriqStartMealPlannerView")
        self.basePresenter = presenter
        self.presenter.startMealPlannerView = self
        settingView()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
   
    
    func setUpWithModel(model: NouriqStartMealPlannerViewModel) {
        DispatchQueue.main.async {
            if model.diet != nil {
                self.dietLabel.text = String(format: "%@", model.diet!)
            }
            
            if model.goal != nil && model.caloriesDay != nil {
                self.goalLabel.text = String(format: "%@ %.0f kcal/day", model.goal!, model.caloriesDay!)
            }
        }
    }
    
    private func settingView() {
        if UIScreen.main.bounds.width <= 320 {
            titleTop.constant = 5
            firstTop.constant = 3.5
            firstBotton.constant = 8
            secondMessageButton.constant = 10

            self.firstMessage.setLineSpacing(spacing: 2)
            self.secondMessage.setLineSpacing(spacing: 2)
            self.thirdMessage.setLineSpacing(spacing: 2)
        } else {
            self.firstMessage.setLineSpacing(spacing: 4)
            self.secondMessage.setLineSpacing(spacing: 4)
            self.thirdMessage.setLineSpacing(spacing: 4)
        }
    }
}

//
//  MicroelementDetailPresenter.swift
//  Nourish
//
//  Created by Vlad Birukov on 16.06.17.
//  Copyright © 2017 SSIC. All rights reserved.
//

import Foundation
import RxSwift


class MicroelementDetailPresenter: BasePresenter, UITableViewDataSource, UITableViewDelegate {

    let objectToModelMapper = MicroelementDetailBalanceModel_to_MicroelementBalanceViewModel()
    let interactor = MicroelementDetailInteractor()
    var microelementDetailView : MicroelementDetailProtocol!
    var microelementDetailModel = MicroelementBalanceViewModel()
    var date: (Date, Date)?
    let unit = ["tsp", "g", "mg"]

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.getMicroelementDetail()
    }
    
    private func getMicroelementDetail() {
        if self.microelementDetailView.config() != nil {
            self.date = self.microelementDetailView.config()?.getDate()
        }
        
        if self.date != nil {
            self.subscribtions.append(self.interactor.execute(startDate: (self.date?.0)!, endDate: (self.date?.1)!).observeOn(MainScheduler.instance).subscribe(onNext: { [unowned self] microelementDetail in
                self.microelementDetailModel = self.objectToModelMapper.transform(microelementDetail: microelementDetail)
                self.microelementDetailView.reloadTable()
                self.microelementDetailView.stopActivityAnimation()
            }, onError: {error in
                self.microelementDetailView.stopActivityAnimation()
                self.microelementDetailView.parseError(error : error,completion: nil)
            }, onCompleted: {
            }, onDisposed: {
                self.microelementDetailView.stopActivityAnimation()
            }))
        }
    }
 
    func numberOfSections(in tableView: UITableView) -> Int {
        if microelementDetailModel.arrMicroelementName != nil {
            return microelementDetailModel.arrMicroelementName!.count
        } else {
            return 0
        }
    }
 
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 60
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let  headerCell = tableView.dequeueReusableCell(withIdentifier: "cellCustomHeaderMicroelement") as! CustomHeaderMicroelementDetailTableViewCell
        
        headerCell.setupWithModel(model: microelementDetailModel, section: section)
        
        return headerCell
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if microelementDetailModel.arrMicroelementName != nil && microelementDetailModel.arrMicroelementName?[section].arrMicroelement != nil {
            return (microelementDetailModel.arrMicroelementName![section].arrMicroelement?.count)!
        } else {
            return 0
        }
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cellMicroelementDetail", for: indexPath) as! MicroelementDetailTableViewCell
        
        if let microelement = microelementDetailModel.arrMicroelementName {
            let currentModel = microelement[indexPath.section]
            
            cell.setupWithModel(model: currentModel, indexPath: indexPath, unit: self.unit[indexPath.section])
        }
        return cell
    }
    
}

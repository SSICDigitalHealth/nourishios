//
//  UserActivity_to_UserActivityModel.swift
//  Nourish
//
//  Created by Nova on 11/21/16.
//  Copyright © 2016 SSIC. All rights reserved.
//

import UIKit

class UserActivity_to_UserActivityModel: NSObject {
    func transform(object : UserActivity) -> UserActivityModel {
        let model = UserActivityModel()
        model.startDate = object.startDate
        model.endDate = object.endDate
        model.activityType = object.activityType
        return model
    }
    
    func transform(objectArray : [UserActivity]) -> [UserActivityModel] {
        var arrayToReturn : [UserActivityModel] = []
        for object in objectArray {
            arrayToReturn.append(self.transform(object: object))
        }
        return arrayToReturn
    }
}

//
//  RecommendationCache.swift
//  Nourish
//
//  Created by Nova on 7/18/17.
//  Copyright © 2017 SSIC. All rights reserved.
//

import Foundation
import RealmSwift

enum RecommendationType : Int{
    case recommendationTip
    case recommendationMeal
}

class RecommendationCache: Object {
    dynamic var date : Date?
    dynamic var data : Data?
    dynamic var privateType = RecommendationType.recommendationTip.rawValue
    
    var type : RecommendationType {
        get {
            return RecommendationType(rawValue: privateType)!
        }
        set {
            privateType = newValue.rawValue
        }
    }
    
}

//
//  Nutrition_to_nutritionStatsModel.swift
//  Nourish
//
//  Created by Nova on 1/31/17.
//  Copyright © 2017 SSIC. All rights reserved.
//

import UIKit

class Nutrition_to_nutritionStatsModel: NSObject {
    var calendar = Calendar.current
    
    func transform(nutr : [Nutrition], calorie : [CaloriesStats], period : period) -> nutritionStatsModel {
        let model = nutritionStatsModel()

        var caloriesArray : [caloriesConsumedModel] = []
        var nutritionsArray : [nutritionalScoreModel] = []
        
        var maxCalorie = 0.0
        var calorieSum = 0.0
        var nutritionSum = 0.0
        var count = 0.0
        
        for stat in calorie {
            let stringValue = self.dateStringFrom(date: stat.day!, period: period)
            
            if period != .today {
                caloriesArray.append(caloriesConsumedModel(calorie : stat.consumedCal!, caloriePeriodRange : stringValue))
            } else {
                let breakDown = stat.breakDownCals
                caloriesArray.append(caloriesConsumedModel(calorie : (breakDown?.breakfastCals)!, caloriePeriodRange : Ocasion.stringRepresentation(servingType: .breakfast)))
                    
                caloriesArray.append(caloriesConsumedModel(calorie : (breakDown?.lunchCals)!, caloriePeriodRange : Ocasion.stringRepresentation(servingType: .lunch)))
                    
                 caloriesArray.append(caloriesConsumedModel(calorie : (breakDown?.dinnerCals)!, caloriePeriodRange : Ocasion.stringRepresentation(servingType: .dinner)))
                    
                 caloriesArray.append(caloriesConsumedModel(calorie : (breakDown?.snacksCals)!, caloriePeriodRange : Ocasion.stringRepresentation(servingType: .snacks)))
                
                caloriesArray = Array(caloriesArray.reversed())
                
                model.recomCalories = stat.eerCal
            }
            
            
            let nutrition = self.filterNutritionsFrom(array: nutr, periodValue: period, startDate: stat.day!)
            var avgValue = 0.0
            
            let nutritionAvg = self.mapArrayIntoAverages(array: nutrition)
            
            avgValue = nutritionAvg.nutritionScore
            
            nutritionsArray.append(nutritionalScoreModel(score : avgValue, scorePeriod: stringValue))
            
            if stat.consumedCal! > maxCalorie {
                maxCalorie = stat.consumedCal!
            }
            
            calorieSum = calorieSum + stat.consumedCal!
            nutritionSum = nutritionSum + stat.score!
            
        }
        let reversedCalories = Array(caloriesArray.reversed())
        let reversedNutr = Array(nutritionsArray.reversed())
        let totalNutrAvg = self.mapArrayIntoAverages(array: nutr)
        count = Double(calorie.count)
        
        model.caloriesArray = reversedCalories
        model.nutritionArray = reversedNutr
        model.avgCalorie = calorieSum / count
        model.avgScore = nutritionSum / count
        model.maxCalories = maxCalorie
        model.vitamins = totalNutrAvg.vitamins
        model.electrolytes = totalNutrAvg.electrolytes
        model.minerals = totalNutrAvg.minerals
        model.macronutrients = totalNutrAvg.macronutrients
        model.nutritionScore = totalNutrAvg.nutritionScore
        model.nutrArray = totalNutrAvg.nutrArray
        
        return model
    }
    
    private func filterNutritionsFrom(array : [Nutrition], periodValue : period, startDate : Date) -> [Nutrition] {
        let start = Calendar.current.startOfDay(for: startDate)
        let endDate = period.endDateFrom(period : periodValue, startDate : start)
        
        var nutr : [Nutrition] = []
        
        for object in array {
            if object.date! >= start && object.date! < endDate {
                nutr.append(object)
            }
        }
        
        return nutr
        
    }
        
        private func dateStringFrom(date : Date, period : period) -> String {
            let dateFormatter = DateFormatter()
            var stringValue = ""
            dateFormatter.dateFormat = "dd/MM"
            let startOfWeek = Calendar.current.date(from: Calendar.current.dateComponents([.yearForWeekOfYear, .weekOfYear], from: date))!
            
            switch period {
            case .today:
                stringValue = dateFormatter.string(from: date)
            case .weekly:
                let startDate = Calendar.current.startOfDay(for: date)
                let myCalendar = NSCalendar(calendarIdentifier: NSCalendar.Identifier.gregorian)!
                let myComponents = myCalendar.components(.weekday, from: startDate)
                let weekDay = myComponents.weekday
                let weekdays = [
                    "Su",
                    "Mo",
                    "Tu",
                    "We",
                    "Th",
                    "Fr",
                    "Sa"
                ]
                stringValue = weekdays[weekDay! - 1]
            case .monthly:
                var comps = DateComponents()
                comps.weekdayOrdinal = 1
                comps.second = -1
                let endDate = Calendar.current.date(byAdding: comps, to: startOfWeek)
                let endString = dateFormatter.string(from: endDate!)
                let startString = dateFormatter.string(from: startOfWeek)
                stringValue = "\(startString)-\(endString)"
                
            }
            return stringValue
        }
    
    private func mapArrayIntoAverages(array : [Nutrition]) -> Nutrition {
        var sumVits = 0.0
        var sumEle = 0.0
        var sumMinerals = 0.0
        var sumMacr = 0.0
        var sumNutrScore = 0.0
        var sumNutrArray : [[nutrients]] = []
        
        var counter = 0
        
        for object in array {
            counter = counter + 1
            sumVits = sumVits + object.vitamins
            sumMinerals = sumMinerals + object.minerals
            sumMacr = sumMacr + object.macronutrients
            sumNutrScore = sumNutrScore + object.nutritionScore
            sumEle = sumEle + object.electrolytes
            sumNutrArray.append(object.nutrArray)
        }
        
        let nutr = Nutrition()
        if counter > 0 {
            nutr.vitamins = sumVits / Double(counter)
            nutr.minerals = sumMinerals / Double(counter)
            nutr.macronutrients = sumMacr / Double(counter)
            nutr.nutritionScore = sumNutrScore / Double(counter)
            nutr.electrolytes = sumEle / Double(counter)
            nutr.nutrArray = self.mapNutrientsIntoAverage(array: sumNutrArray)
        } else {
            nutr.vitamins = 0.0
            nutr.minerals = 0.0
            nutr.macronutrients = 0.0
            nutr.electrolytes = 0.0
            nutr.nutrArray = []
        }
        return nutr
    }
    
    
    
    
    private func mapNutrientsIntoAverage(array : [[nutrients]]) -> [nutrients] {
        if array.count == 1 {
            return array.first!
        } else {
            var arrayToReturn : [nutrients] = []
            
            if array.count > 0 {
                let index = self.maxArrayIndex(array: array)
                let proxyArray = array[index]
                var proxyValue = 0.0
                var proxyScore = 0.0
                var proxyName = ""
                
                
                for nutr in proxyArray {
                    proxyName = nutr.name
                    
                    for object in array {
                        for nutr in object {
                            if nutr.name == proxyName {
                                proxyValue = proxyValue + nutr.value
                                proxyScore = proxyScore + nutr.score
                            }
                        }
                    }
                    
                    let avgValue = proxyValue / Double(array.count)
                    let nutrToAppend = nutrients()
                    nutrToAppend.name = proxyName
                    //nutrToAppend.min = nutr.min
                    //nutrToAppend.max = nutr.max
                    nutrToAppend.value = avgValue
                    nutrToAppend.descriptionString = nutr.descriptionString
                    nutrToAppend.deficiency = nutr.deficiency
                    nutrToAppend.excess = nutr.excess
                    nutrToAppend.sources = nutr.sources
                    nutrToAppend.score = proxyScore / Double(array.count)
                    nutrToAppend.unit = nutr.unit
                    nutrToAppend.lhr = nutr.lhr
                    nutrToAppend.uhr = nutr.uhr
                    //let nutrToAppend = nutrients(name : proxyName, min : nutr.min, max : nutr.max, value : avgValue, description : nutr.description, deficiency : nutr.deficiency, excess : nutr.excess, sources : nutr.sources, unit : nutr.unit, flag : "", quantityString : "")
                    
                    arrayToReturn.append(nutrToAppend)
                }
                
            }
            return arrayToReturn
        }
        
    }
    
    private func maxArrayIndex(array : [[nutrients]]) -> Int {
        var indexToReturn = 0
        var maxValue = 0
        for index in 0..<array.count {
            let object = array[index]
            if object.count > maxValue {
                maxValue = array.count
                indexToReturn = index
            }
        }
        return indexToReturn
        
    }
}

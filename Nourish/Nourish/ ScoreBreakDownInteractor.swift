//
//   ScoreBreakDownInteractor.swift
//  Nourish
//
//  Created by Vlad Birukov on 14.06.17.
//  Copyright © 2017 SSIC. All rights reserved.
//

import Foundation
import RxSwift

class  ScoreBreakDownInteractor {
    
    let nestleProgressRepository = NestleProgressRepository.shared

    func execute(startDate: Date, endDate: Date) -> Observable<ScoreBreakDownModel> {
        
        return self.nestleProgressRepository.getProgressFor(startDate: startDate, endDate: endDate, returnHistory: true).flatMap{progress -> Observable<ScoreBreakDownModel> in
            let scoreBreakDownModel = ScoreBreakDownModel()
            let userScore = progress.userScore
            scoreBreakDownModel.scoreUser = userScore.scoreActivity
            scoreBreakDownModel.historyScore = userScore.historyScore

            return Observable.just(scoreBreakDownModel)
        }
    }
}

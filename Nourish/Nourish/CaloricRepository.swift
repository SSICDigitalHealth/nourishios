//
//  CaloricRepository.swift
//  Nourish
//
//  Created by Gena Mironchyk on 9/19/17.
//  Copyright © 2017 SSIC. All rights reserved.
//

import UIKit
import RxSwift
import HealthKit

class CaloricRepository: NSObject {
    private let userRepo = UserRepository.shared
    private let nestleProgressRepository = NestleProgressRepository.shared
    private let userActivityRepo = UserActivityRepository()
    private let calendar = Calendar.current
    
    
    func fetchCaloricProgressModelFor(startDate : Date, endDate : Date) -> Observable<CaloricProgressModel> {
        var calorieSamples = [Observable<[HKSample]>]()
        for day in 0 ... self.daysBetweenDates(startDate: startDate, endDate: endDate) {
            calorieSamples.append(self.userActivityRepo.getActviveCaloriesFor(date: self.addDay(startDate: startDate, number: day)))
        }
        let calsSamplesConcated = Observable.from(calorieSamples).concat()
        let calsSampesArrayed = calsSamplesConcated.toArray()
        
        let user = self.userRepo.getCurrentUser(policy: .Cached)
        var returnHistory = true
        if self.calendar.startOfDay(for: startDate) == self.calendar.startOfDay(for: endDate) {
            returnHistory = false
        }
        let progress = self.nestleProgressRepository.getProgressFor(startDate: startDate, endDate: endDate, returnHistory: returnHistory)
        return Observable.zip(user, progress, calsSampesArrayed , resultSelector : {userProfile, userProgress,calories -> CaloricProgressModel in
            let model = CaloricProgressModel()
            var weeklyPlan = userProfile.getWeeklyWeightLoss()
            model.userGoal = userProfile.userGoal
            model.activityLevel = userProfile.activityLevel
            model.isMetric = (userProfile.metricPreference?.isMetric)!
            
            if weeklyPlan > NRConversionUtility.poundsToKil(valueInPounds: 2) {
                weeklyPlan = NRConversionUtility.poundsToKil(valueInPounds: 2)
            }
            
            model.weeklyPlan = weeklyPlan
            let maxBurnedCals = userProfile.getActiveCalorieMinimum()
            var arrayForModel = [caloricProgress]()
            let cal = Calendar.current
            
            if userProgress.calorieHistory != nil {
                for historyModel in userProgress.calorieHistory! {
                    let dateToFetch = (cal.isDateInToday(historyModel.date!) ? Date() : historyModel.date) ?? Date()
                    
                    let bmr = userProfile.getBMRCalories(weight: userProfile.weight!, height: userProfile.height!, date: dateToFetch, isCurrentDay: cal.isDateInToday(historyModel.date!))
                    let maxConsumedCals = userProfile.calculateAndStoreCaloriePlan()
                    let userConsumedCals = historyModel.consumed
                    let date = historyModel.date
                    let index = userProgress.calorieHistory!.index(of: historyModel)!
                    var burnedCalories = 0.0
                    if index <  calories.count {
                        let cals = calories[index]
                        burnedCalories = self.burnedCalories(samples: cals)
                    }
                    let modelToReturn = caloricProgress(subtitle: date!, cal: (bmrCal: bmr, maxConsumedCal: maxConsumedCals, maxBurnedCal: maxBurnedCals, userConsumedCal: userConsumedCals, userBurnedCal: burnedCalories))
                    arrayForModel.append(modelToReturn)
                }
                model.caloricState = arrayForModel
                
            } else if self.daysBetweenDates(startDate: startDate, endDate: endDate) != 0 {
                for day in 0 ... self.daysBetweenDates(startDate: startDate, endDate: endDate) {
                    let date = self.addDay(startDate: startDate, number: day)
                    let bmr = userProfile.getBMRCalories(weight: userProfile.weight!, height: userProfile.height!, date: Date(), isCurrentDay: cal.isDateInToday(date))
                    let maxConsumedCals = userProgress.userCallories.targetCalories / Double(calories.count)
                    
                    arrayForModel.append(caloricProgress(subtitle: date, cal: (bmrCal: bmr, maxConsumedCal: maxConsumedCals, maxBurnedCal: 0.0, userConsumedCal: 0.0, userBurnedCal: 0.0)))
                    model.caloricState = arrayForModel
                }
            } else {
                let bmr = userProfile.getBMRCalories(weight: userProfile.weight!, height: userProfile.height!, date: Date(), isCurrentDay: cal.isDateInToday(Date()))
                let maxConsumedCals = userProgress.userCallories.targetCalories
                let userConsumedCals = userProgress.userCallories.consumedCalories
                let burnedCalories = self.burnedCalories(samples: calories[0])
                let modelToReturn = caloricProgress(subtitle: Date(), cal: (bmrCal: bmr, maxConsumedCal: maxConsumedCals, maxBurnedCal: maxBurnedCals, userConsumedCal: userConsumedCals, userBurnedCal: burnedCalories))
                arrayForModel.append(modelToReturn)
                model.caloricState = arrayForModel

            }
            return model
            
        })
    }
    
    
    func getAverageCalorieModelFor(startDate : Date, endDate : Date) -> Observable<CaloriesDailyAverageModel> {
        return self.fetchCaloricProgressModelFor(startDate: startDate, endDate: endDate).flatMap{ calorieProgressModel -> Observable<CaloriesDailyAverageModel> in
            let model = CaloriesDailyAverageModel()
            let caloricState = calorieProgressModel.caloricState
            let daysBetween = Double(self.daysBetweenDates(startDate: startDate, endDate: endDate)) + 1
            var consumedCals = 0.0
            var targetCals = 0.0
            var dayTotal = 0
            
            for object in caloricState! {
                consumedCals += object.cal.userConsumedCal
                targetCals += object.cal.maxConsumedCal
            }

            if let caloric = caloricState {
                dayTotal = caloric.filter({$0.cal.userConsumedCal != 0.0}).count
            }
            var finalConsumed = consumedCals
            
            if dayTotal != 0 {
                finalConsumed = finalConsumed / Double(dayTotal)
            }
            let finalTarget = targetCals/daysBetween
            model.caloriesDailyAverage = finalConsumed
            model.caloriesOverTarget = finalConsumed - finalTarget
            return Observable.just(model)
            
        }
    }
    
    
    private func addDay(startDate: Date, number: Int) -> Date {
        return Calendar.current.date(byAdding: .day, value: number, to: startDate)!
    }
    
    private func daysBetweenDates(startDate: Date, endDate: Date) -> Int
    {
        let calendar = Calendar.current
        let components = calendar.dateComponents([.day], from: startDate, to: endDate)
        return components.day!
    }

    
    
    private  func burnedCalories(samples : [HKSample]) -> Double {
        var summary = 0.0
        for samlpe in samples {
            let key = samlpe as! HKQuantitySample
            summary = summary + key.quantity.doubleValue(for: HKUnit.kilocalorie())
        }
        return summary
    }

    
}

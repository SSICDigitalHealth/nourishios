//
//  HydrationBalanceViewController.swift
//  Nourish
//
//  Created by Vlad Birukov on 16.06.17.
//  Copyright © 2017 SSIC. All rights reserved.
//

import UIKit

class HydrationBalanceViewController: BaseCardViewController {
    
    @IBOutlet weak var hydrationView: HydrationView!
    @IBOutlet weak var hydrationDetailView: HydrationDetailBalanceView!
    @IBOutlet weak var cardNavigationBar: NewCardNavigationBarView!

    let top = 7
    let bottom = 6
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.baseViews = [cardNavigationBar, hydrationView, hydrationDetailView]
        
        for v in baseViews! {
            v.viewDidLoad()
        }

        if let config = self.progressConfig {
            hydrationView.progressConfig = config
            hydrationDetailView.progressConfig = config
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        setUpNavigationBar(title: "Hydration", style: .other, navigationBar: cardNavigationBar)
        self.hydrationView.contentHolderTop.constant = CGFloat(self.top)
        self.hydrationView.contentHolderBottom.constant = CGFloat(self.bottom)
        super.viewWillAppear(animated)
    }
    

}

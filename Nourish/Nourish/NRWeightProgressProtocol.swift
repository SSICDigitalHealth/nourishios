//
//  NRWeightProgressProtocol.swift
//  Nourish
//
//  Created by Vino (Vinodha) Sundaramoorthy on 12/6/16.
//  Copyright © 2016 SSIC. All rights reserved.
//

import UIKit

protocol NRWeightProgressProtocol {
    func setupWith(weightProgress : NRWeightProgressModel)
}

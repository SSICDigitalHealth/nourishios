//
//  RecipesDetailTableViewCell.swift
//  Nourish
//
//  Created by Vlad Birukov on 07.09.17.
//  Copyright © 2017 SSIC. All rights reserved.
//

import UIKit

class RecipesDetailTableViewCell: UITableViewCell {
    @IBOutlet weak var foodImage: UIImageView!
    @IBOutlet weak var detailLabel: UILabel!
    @IBOutlet weak var ingredientLabel: UILabel!
    @IBOutlet weak var favoriteImage: UIButton!
    @IBOutlet weak var heightImage: NSLayoutConstraint!

    var presenter: RecipesDetailPresenter?
    var recipiesViewModel: RecipiesViewModel?

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func setupWith(model: RecipiesViewModel, present: RecipesDetailPresenter) {
        self.presenter = present
        self.recipiesViewModel = model
        
        var ingridient = ""
        
        if model.image != nil {
            self.foodImage.image = model.image
            self.heightImage.constant = UIScreen.main.bounds.width - 32
        } else {
            self.heightImage.constant = 0

        }
        
        if model.isFavorite == true {
            self.favoriteImage.setImage(UIImage(named: "fav_green"), for: .normal)
        } else {
            self.favoriteImage.setImage(UIImage(named: "fav_icon_00-1"), for: .normal)
        }
        
        self.detailLabel.text = String(format: "%d ingredients | %0.f kcal %@", model.ingredient.count, model.numberCal, "per person")
        
        for i in  0..<model.ingredient.count {
            if i == model.ingredient.count - 1 {
                ingridient += model.ingredient[i]
            } else {
                ingridient += model.ingredient[i] + "\n"
            }
        }
        self.ingredientLabel.text = ingridient
        self.ingredientLabel.setLineSpacing(spacing: 16)
    }
    
    @IBAction func favoriteFood(_ sender: Any) {
        if self.recipiesViewModel != nil && self.presenter != nil {
            if presenter?.currentStateFavorite == true {
                self.presenter?.currentStateFavorite = false
                self.isFavorite(favorite: false)
            } else {
                self.presenter?.currentStateFavorite = true
                self.isFavorite(favorite: true)
            }
        }
    }
    
    func isFavorite(favorite: Bool) {
        if favorite == true {
            self.favoriteImage.setImage(UIImage(named: "fav_green"), for: .normal)
        } else {
            self.favoriteImage.setImage(UIImage(named: "fav_icon_00-1"), for: .normal)
        }
    }
}

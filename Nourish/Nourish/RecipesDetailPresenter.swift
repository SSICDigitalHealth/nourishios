//
//  RecipesDetailPresenter.swift
//  Nourish
//
//  Created by Vlad Birukov on 07.09.17.
//  Copyright © 2017 SSIC. All rights reserved.
//

import Foundation
import RxSwift

class RecipesDetailPresenter: BasePresenter, UITableViewDataSource, UITableViewDelegate {
    
    var recipesDetailView: RecipesDetailProtocol!
    var recipesViewModel: RecipiesViewModel?
    let mapper = RecipiesViewModel_to_foodSearchModel()
    var controller: RecipesDetailViewController?
    
    let favInteractor = FavouritesInteractor()
    var initalStateFavorite = false
    var currentStateFavorite = false
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return recipesViewModel == nil ? 0 : 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if recipesViewModel != nil {
            return (recipesViewModel?.instructions.count)!
        }
        
        return 0
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerCell = tableView.dequeueReusableCell(withIdentifier: "recipesCell") as! RecipesDetailTableViewCell
        if recipesViewModel != nil {
            headerCell.setupWith(model: recipesViewModel!, present: self)
        }
        
        return headerCell
    }

    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cellRecepies", for: indexPath) as! RecepiesTableViewCell
        if recipesViewModel != nil {
            cell.setupWith(instructionElement: (recipesViewModel?.instructions[indexPath.row])!, index: (indexPath.row + 1))
        }
        
        return cell
    }
    
    
    func addFavorite(model: foodSearchModel) {
        let _ = self.favInteractor.addRecToFavourites(searchModel: model, name: UUID().uuidString).subscribe(onNext: { [unowned self] success in
        }, onError: {error in
            DispatchQueue.main.async {
                self.recipesDetailView.parseError(error : error, completion: nil)
            }
        }, onCompleted: {
        }, onDisposed: {})
    }
    
    func deleteFavorite(groupId: String) {
        let _ = self.favInteractor.deleteFavouriteGroup(groupID: groupId).subscribe(onNext: {
            success in
        }, onError: {error in
            DispatchQueue.main.async {
                self.recipesDetailView.parseError(error : error, completion: nil)
            }
        }, onCompleted: {
        }, onDisposed: {})

    }
    
    func saveStateFavoriteAndBack(showTabBar: Bool) {
        if self.currentStateFavorite == self.initalStateFavorite {
            self.back(showTabBar: showTabBar)
        } else if self.currentStateFavorite == true {
            if self.recipesViewModel != nil {
                self.addFavorite(model: self.mapper.transform(model: self.recipesViewModel!))
            }
            self.back(showTabBar: showTabBar)
        } else {
            if self.recipesViewModel?.groupId != nil {
                self.deleteFavorite(groupId: (self.recipesViewModel?.groupId)!)
            }
            self.back(showTabBar: showTabBar)
        }
        
    }
    
    private func back(showTabBar: Bool) {
        if self.controller?.navigationController != nil {
            self.controller?.navigationController?.popViewController(animated: true)
            if showTabBar == true {
                NavigationUtility.showTabBar(animated: false)
            } else {
                NavigationUtility.hideTabBar(animated: false)

            }
        }
    }
}

//
//  CaloriesDailyAverageModel.swift
//  Nourish
//
//  Created by Vlad Birukov on 20.06.17.
//  Copyright © 2017 SSIC. All rights reserved.
//

import Foundation


class CaloriesDailyAverageModel: NSObject {
    var caloriesDailyAverage: Double = 0.0
    var caloriesOverTarget: Double = 0.0
}

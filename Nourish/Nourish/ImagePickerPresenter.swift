//
//  ImagePickerPresenter.swift
//  Nourish
//
//  Created by Nova on 5/11/17.
//  Copyright © 2017 SSIC. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

class ImagePickerPresenter: BasePresenter, UITableViewDelegate, UITableViewDataSource,NRPickerViewDataSource,NRPickerViewDelegate,FoodDetailsViewControllerDelegate {
    
    @IBOutlet var logButton : UIButton?
    @IBOutlet var searchButton : UIButton?
    
    @IBOutlet var viewToHide: [UIView]!
    
    var foodModels : [foodSearchModel]?
    var checkMarkedModels : [foodSearchModel]? = []
    
    var foodImage : UIImage?
    var imagePickerView : ImageFoodPickerView?
    var imagePickerController : RecognitionViewControllerProtocol?
    
    var pickedOcasion : Ocasion!
    let interactor = UploadFoodWithPhotoInteractor()
    let noom = NMFood_to_MealRecordRoutine()
    let foodInt = NRFoodDetailsInteractor()
    
    var date : Date?
    
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        self.checkLogInButtonAvailability()
        
    }
    
    func roundButtonCorners() {
        self.logButton?.layer.cornerRadius = (self.logButton?.frame.height)! / 2.0
        self.searchButton?.layer.cornerRadius = (self.searchButton?.frame.height)! / 2.0
        self.logButton?.layer.masksToBounds = true
        self.searchButton?.layer.masksToBounds = true
    }
    
    func checkLogInButtonAvailability() {
        if self.checkMarkedModels != nil && self.checkMarkedModels!.count > 0 {
            self.logButton?.isEnabled = true
            self.logButton?.backgroundColor = NRColorUtility.appBackgroundColor()
        } else {
            self.logButton?.isEnabled = false
            self.logButton?.backgroundColor = NRColorUtility.hexStringToUIColor(hex: "#d3d5da")
        }
    }

    
    override func viewWillAppear(_ animated: Bool) {
        
        if self.foodImage != nil {
            self.imagePickerView?.setupImageViewWith(image : self.foodImage!)
        }
        
        if self.foodModels != nil && self.foodModels!.count > 0 {
            self.imagePickerView?.reloadTableViewData()
            self.imagePickerView?.reloadPickerView(ocasion: self.pickedOcasion)
        } else {
            for view in self.viewToHide {
                view.isHidden = true
            }
        }
        self.searchButton?.backgroundColor = NRColorUtility.appBackgroundColor()
        self.imagePickerView?.stopActivityAnimation()
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell : ImagePickerTableViewCell = tableView.dequeueReusableCell(withIdentifier: "ImagePickerTableViewCell") as! ImagePickerTableViewCell
    
        let food = self.foodModels?[indexPath.row]
        var contains = false
        
        let available = self.checkMarkedModels?.contains { element in
            if element.foodTitle == food?.foodTitle {
                return true
            } else {
                return false
            }
        }
        
        if available != nil {
            contains = available!
        }
        
        
        cell.drawWithModel(model: food!, selected: contains)
        
        cell.checkMarkButton?.tag = indexPath.row
        
        cell.checkMarkButton?.addTarget(self, action: #selector(self.cellButtonPressed(sender:)), for: .touchUpInside)
        
        return cell
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return (self.foodModels?.count)!
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let meal = self.foodModels?[indexPath.row]
        //meal?.isFromNoom = true
        self.imagePickerController?.showFoodDetailsViewFor(meal : meal!, delegate: self, updatingAnmount: true)
    }
    
    func numberOfItemsInPickerView(_ pickerView: NRPickerView) -> Int {
        return Ocasion.snacks.rawValue
    }
    
    func pickerView(_ pickerView: NRPickerView, titleForItem item: Int) -> String {
        let ocasion = Ocasion(rawValue: item + 1)
        return Ocasion.stringDescription(servingType: ocasion!)
    }
    
    func pickerView(_ pickerView: NRPickerView, didSelectItem item: Int) {
        self.pickedOcasion = Ocasion(rawValue : item + 1)!
    }
    
    //actions
    @IBAction func closeButtonPressed(sender : UIButton) {
        self.imagePickerController?.performDelegateActionDidCancel()
    }
    
    @IBAction func searchButtonPressed(sender : UIButton) {
        NavigationUtility.openFoodSearchFor(ocasion: self.pickedOcasion)
    }
    
    func cellButtonPressed(sender : UIButton) {
        let meal = self.foodModels?[sender.tag]
        
        let contains = self.checkMarkedModels?.contains { element in
            if element.foodId == meal?.foodId {
                return true
            } else {
                return false
            }
        }
        
        if contains == false {
            self.checkMarkedModels?.append(meal!)
        } else {
            let index = self.checkMarkedModels?.index {$0.foodId == meal?.foodId}
            if index != nil {
                self.checkMarkedModels?.remove(at: index!)
            }
        }
        self.checkLogInButtonAvailability()
        
        self.imagePickerView?.reloadTableViewData()
    }
    
    @IBAction func logButtonPressed(sender : UIButton) {
        self.imagePickerView?.prepareLoadView()
        
        if (checkMarkedModels?.count)! > 0 && self.foodImage != nil {
            let dateToAdd = date ?? Date()
            let _ = self.interactor.execute(models: self.checkMarkedModels!, photo: self.foodImage!, ocasion: self.pickedOcasion, date: dateToAdd).observeOn(MainScheduler.instance).subscribe(onNext: {success in
                EventLogger.logSaveFoodUsingCameraFor(ocasion: self.pickedOcasion)
            }, onError: {error in
                LogUtility.logToFile(error)
                DispatchQueue.main.async {
                    self.imagePickerView?.parseError(error: error, completion: nil)
                }
            }, onCompleted: {
                
            }, onDisposed: {})
            

        }
            self.imagePickerView?.stopActivityAnimation()
            self.imagePickerController?.performDelegateActionDidAdded()
    }
    
    func foodDetailsViewControllerDidFinishWith(foodSearchModel: foodSearchModel, presentedModal: Bool) {
        var newFoodModel : [foodSearchModel] = []
        for object in self.foodModels! {
            if object.foodId == foodSearchModel.foodId {
                newFoodModel.append(foodSearchModel)
            } else {
                newFoodModel.append(object)
            }
        }
        
        var newCheckMarkedModels : [foodSearchModel] = []
        
        if self.checkMarkedModels != nil {
            for objeсtC in self.checkMarkedModels! {
                if objeсtC.foodId == foodSearchModel.foodId {
                    newCheckMarkedModels.append(foodSearchModel)
                } else {
                    newCheckMarkedModels.append(objeсtC)
                }
            }
        }
        
        self.checkMarkedModels = newCheckMarkedModels
        self.foodModels = newFoodModel
        self.imagePickerView?.foodList = newFoodModel
        self.imagePickerView?.reloadTableViewData()
    }
}

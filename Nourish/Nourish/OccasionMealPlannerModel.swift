//
//  OccasionMealPlannerModel.swift
//  Nourish
//
//  Created by Vlad Birukov on 22.08.17.
//  Copyright © 2017 SSIC. All rights reserved.
//

import Foundation

class OccasionMealPlannerModel: NSObject {
    var message: String?
    var occasion: [(foodSearchModel, RecipiesModel?)]?
}


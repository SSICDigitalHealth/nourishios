//
//  FoodCardCacheDataStore.swift
//  Nourish
//
//  Created by Gena Mironchyk on 8/21/17.
//  Copyright © 2017 SSIC. All rights reserved.
//

import UIKit
import RealmSwift
class FoodCardCacheDataStore: NSObject {
    func storeCache(date : Date, foodId : String, cache : FoodCardDetails) {
        
        let realm = try! Realm()
        let predicate = NSPredicate(format: "foodId == %@", foodId)
        let objs = realm.objects(FoodCardCache.self).filter(predicate)
        if objs.count>0 {
            try! realm.write {
                realm.delete(objs, cascading: true)
            }
        }
        
        try! realm.write {
            realm.add(self.mapFoodDetailsToCache(foodDetails: cache, foodId: foodId, date: date))
        }
    }
    
    private func mapFoodDetailsToCache(foodDetails : FoodCardDetails, foodId : String, date : Date) -> FoodCardCache {
        let foodCardCache = FoodCardCache()
        foodCardCache.date = date
        foodCardCache.foodId = foodId
        for nutr in foodDetails.nutrients! {
            let nutrient = Nutrient()
            nutrient.name = nutr.name
            nutrient.clearValue = nutr.clearValue
            nutrient.representationValue = nutr.representationValue
            nutrient.unit = nutr.unit
            foodCardCache.nutrients.append(nutrient)
        }
        for servinType in foodDetails.servingTypes! {
            let realmString = RealmString()
            realmString.stringValue = servinType
            foodCardCache.servingTypes.append(RealmString(value: realmString))
        }
        return foodCardCache
    }
    
    func fetchCache(foodId : String) -> FoodCardDetails? {
        let realm = try! Realm()
        let predicate = NSPredicate(format: "foodId == %@", foodId)
        let obj = realm.objects(FoodCardCache.self).filter(predicate).first
        if obj != nil{
            if self.moreThan28Days(date: (obj?.date)!) == true {
                try! realm.write {
                    realm.delete(obj!, cascading: true)
                }
                return nil
            } else {
                return self.mapCacheToFoodDetails(cache: obj!)
            }
        }
        else {
            return nil
        }
    }
    
    private func mapCacheToFoodDetails(cache : FoodCardCache) -> FoodCardDetails {
        var nutrientsArray = [Nutrient] ()
        for nutr in cache.nutrients {
            let nutrient = Nutrient()
            nutrient.name = nutr.name
            nutrient.clearValue = nutr.clearValue
            nutrient.representationValue = nutr.representationValue
            nutrient.unit = nutr.unit
            nutrientsArray.append(nutrient)
        }
        var listOfServingTypes = [String]()
        for realmString in cache.servingTypes {
            listOfServingTypes.append(realmString.stringValue)
        }
        
        return FoodCardDetails(nutrients: nutrientsArray, servingTypes : listOfServingTypes)
    }
    
    
    private func moreThan28Days(date:Date) -> Bool {
        var moreThanSevenDays = false
        let componentsFromDate = Calendar.current.dateComponents([.day], from: date)
        let componentsFromNow = Calendar.current.dateComponents([.day], from: Date())
        if componentsFromNow.day! - componentsFromDate.day! > 28 {
            moreThanSevenDays = true
        }
        
        return moreThanSevenDays
    }
    
}

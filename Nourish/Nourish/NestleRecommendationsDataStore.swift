
//
//  NestleRecommendationsDataStore.swift
//  Nourish
//
//  Created by Gena Mironchyk on 9/11/17.
//  Copyright © 2017 SSIC. All rights reserved.
//

import UIKit
import RxSwift

let nestleRecommendationsUrl = kNourishApiAddress + "recommendations"

class NestleRecommendationsDataStore: BaseNestleDataStore {
    let mapper = json_to_NestleRecommendations()
    
    func fetchRecommendations(token : String, userId : String, startDate : Date, endDate : Date) -> Observable<NestleRecommendationsCache> {
        return Observable.create{observer in
            let url = self.getRecommendationsUrl(token: token, userId: userId, startDate: startDate, endDate: endDate)
            var request = self.requestWithToken(token: token, url: url)//URLRequest(url: url)
            request.httpMethod = "GET"
            let session = URLSession.shared.dataTask(with: request) {
                data, response, error in
                
                if error == nil && data != nil {
                    let dataStr = String(data : data!, encoding : .utf8)
                    if let dictionary = self.convertStringToDictionary(text: dataStr!) {
                        observer.onNext(self.mapper.transform(dictionary: dictionary))
                    }
                } else {
                    if error != nil {
                        observer.onError(error!)
                    }
                }
                self.log(foodProcessingOrWithings: false, url: url.absoluteString, requestMethod: request.httpMethod, urlResponse: response, requestBody: nil, error: error, responceBody: nil)
                observer.onCompleted()
            }
            session.resume()
            
            return Disposables.create {
                session.cancel()
            }
        }
    }
    
    private func getRecommendationsUrl(token : String , userId : String, startDate : Date, endDate : Date) -> URL {
        var stringUrl = nestleRecommendationsUrl
        stringUrl = self.addAuthorizationHeader(url: stringUrl, token: token) + kNestleUserID + userId
        stringUrl = self.addLocalTime(stringUrl: stringUrl, local: kNestleLocalTime)
        stringUrl = self.appendTimeTo(url: stringUrl, startDate: startDate, endDate: endDate)
        return URL(string: stringUrl)!
    }
    
    private func appendTimeTo(url : String, startDate : Date, endDate : Date) -> String {
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyyMMdd"
        let startInterval = formatter.string(from: startDate)
        let endInterval = formatter.string(from: endDate)
        let stringToReturn = url.appending(kNestleStartDay + startInterval + kNestleEndDay + endInterval)
        return stringToReturn
    }
    
    private func addLocalTime(stringUrl : String, local : String) -> String {
        let url = stringUrl + local + self.getLocalTimeString()
        return url
    }
    
    private func getLocalTimeString() -> String {
        let formatter = DateFormatter()
        formatter.dateFormat = "HHmmss"
        let localTime = formatter.string(from: Date())
        return localTime
    }
    
    private func addAuthorizationHeader(url : String, token : String) -> String {
        let authorizedUrl = url.appending(kNestleToken + token)
        return authorizedUrl
    }
    
    private func convertStringToDictionary(text: String) -> [String : Any]? {
        if let data = text.data(using: String.Encoding.utf8) {
            do {
                return try JSONSerialization.jsonObject(with: data, options: []) as? [String : Any]
            } catch let error as NSError {
                LogUtility.logToFile(error)
            }
        }
        return nil
    }
}

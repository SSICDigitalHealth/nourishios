//
//  RecipiesTableViewCell.swift
//  Nourish
//
//  Created by Vlad Birukov on 07.09.17.
//  Copyright © 2017 SSIC. All rights reserved.
//

import UIKit

class RecipiesTableViewCell: UITableViewCell {
    @IBOutlet weak var nameFood: UILabel!
    @IBOutlet weak var imageFood: UIImageView!
    @IBOutlet weak var detailLabel: UILabel!
    @IBOutlet weak var favoriteImage: UIButton!
    @IBOutlet weak var heightImage: NSLayoutConstraint!
    var recipiesViewModel: RecipiesViewModel?
    var presenter: RecomendationDetailsPresenter?
    var indexPath: IndexPath?

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    @IBAction func favoriteFood(_ sender: Any) {
        if self.recipiesViewModel != nil && self.presenter != nil {
            self.presenter?.detailView.prepareLoadView()
            if self.recipiesViewModel?.isFavorite == true {
                if self.recipiesViewModel?.groupId != nil {
                    self.isFavorite(favorite: false)
                    self.presenter?.selectedCell = self.indexPath
                    self.presenter?.deleteFavorite(groupId: (recipiesViewModel?.groupId)!, cell: nil, cellRecepies: self)
                }
            } else {
                if self.recipiesViewModel != nil && self.presenter != nil {
                    self.isFavorite(favorite: true)
                    self.presenter?.selectedCell = self.indexPath
                    self.presenter?.addFavorite(model: (self.presenter?.transformRecipiesViewModel_to_foodSearchModel(model: recipiesViewModel!))!, cell: nil, cellRecepies: self)
                }
            }
        }
    }
    
    func setupWith(model: RecipiesViewModel, present: RecomendationDetailsPresenter, indexPath: IndexPath) {
        self.indexPath = indexPath
        self.presenter = present
        self.recipiesViewModel = model
        self.nameFood.text = String(format: "%@", model.nameFood)
        
        if model.image != nil {
            self.imageFood.image = model.image
            self.heightImage.constant = UIScreen.main.bounds.width - 32
        } else {
            self.heightImage.constant = 0
        }
        
        if model.isFavorite == true {
            self.favoriteImage.setImage(UIImage(named: "fav_green"), for: .normal)
        } else {
            self.favoriteImage.setImage(UIImage(named: "fav_icon_00-1"), for: .normal)
        }
        
        self.detailLabel.text = String(format: "%d ingredients | %0.f kcal %@", model.ingredient.count, model.numberCal, "per person")
    }
    
    func isFavorite(favorite: Bool) {
        if favorite == true {
            self.favoriteImage.setImage(UIImage(named: "fav_green"), for: .normal)
        } else {
            self.favoriteImage.setImage(UIImage(named: "fav_icon_00-1"), for: .normal)
        }
    }
}

//
//  NRSpeechKitConfiguration.swift
//  Nourish
//
//  Created by Vino (Vinodha) Sundaramoorthy on 11/17/16.
//  Copyright © 2016 SSIC. All rights reserved.
//

import UIKit

import Foundation

//var SKSAppKey = "a4978bcf300b6e68d97fb449f2d653fff7595f98d66578658992a1f6102c3cb4c3c883f3d3f15cac7010f662e74962dea2c1767826efddaf817ea691abe2456e"
var SKSAppKey = "6eb4379b0694b562eb90b35de5ab84359233fbe3540d5623506a9c57ac4e7d1a02b5b508496a6b35fb1e92e0c916165b49113e9d1e94c06ffe5cc6aaaa44c5ac"
var SKSAppId = "NMDPTRIAL_s_adhikari_ssi_samsung_com20160701115856"
//var SKSAppId = "NMDPTRIAL_vino_s_ssi_samsung_com20161117184838"
//var SKSServerHost = "sslsandbox-nmdp.nuancemobility.net"
var SKSServerHost = "nmsps.dev.nuance.com"
var SKSServerPort = "443"

var SKSLanguage = "eng-USA"

var SKSServerUrl = String(format: "nmsps://%@@%@:%@", SKSAppId, SKSServerHost, SKSServerPort)

// Only needed if using NLU/Bolt
var SKSNLUContextTag = "M2922_A1536"

let LANGUAGE = SKSLanguage == "eng-USA" ? "eng-USA" : SKSLanguage


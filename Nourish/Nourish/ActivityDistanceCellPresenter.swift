//
//  ActivityDistanceCellPresenter.swift
//  Nourish
//
//  Created by Andrey Zmushko on 8/25/17.
//  Copyright © 2017 SSIC. All rights reserved.
//

import UIKit
import RxSwift


class ActivityDistanceCellPresenter: ActivityBaseCellPresenter {
    
    let interactor = DistanceInteractor()
    let mapperObjectDistance = DistanceModel_to_DistanceViewModel()
    var distanceViewModel = DistanceViewModel()
    
    private func getInformation(cell: ActivityBaseTableViewCell, startDate: Date, endDate: Date) {
        cell.baseView.viewWillAppear(true)
        cell.baseView.removeActivityIndicator()
        self.subscribtions.append(self.interactor.execute(startDate: startDate, endDate: endDate).observeOn(MainScheduler.instance).subscribe(onNext: { [unowned self] distanceActive in
            self.distanceViewModel = self.mapperObjectDistance.transform(model: distanceActive)
        }, onError: {error in
            cell.baseView.parseError(error: error,completion: nil)
        }, onCompleted: {
            self.setupViewForData(cell: cell)
            cell.baseView.stopActivityAnimation()
        }, onDisposed: {
        }))
    }
    
    override func fetchModel(For basecell: ActivityBaseCell) {
        super.fetchModel(For: basecell)
        
        let cell = basecell as! ActivityBaseTableViewCell
        
        cell.iconImageView.imageNamedWithTint(named: "distance_icon", tintColor: UIColor.white)
        cell.iconFrame.backgroundColor = NRColorUtility.hexStringToUIColor(hex: "40AD75")
        
        cell.titleLabel.attributedText = NSMutableAttributedString(string: "Distance", attributes: cell.boldAttr)
        
        if let delegate = self.delegate {
            let date = delegate.fetchEpoch()
            self.getInformation(cell: cell, startDate: date.startDate, endDate: date.endDate)
            
            if Calendar.current.compare(date.startDate, to: date.endDate, toGranularity: .day) != .orderedSame {
                cell.descriptionLabel.text = "Avg. distance/day"
                cell.bottomGap.constant = 0
            }
        }
    }
    
    private func setupViewForData(cell: ActivityBaseTableViewCell) {
        let distStr = self.distanceViewModel.isMetric ?  String(format: "%.1f",self.distanceViewModel.distance) : String(format:"%.1f",NRConversionUtility.kiloToMiles(valueInKilo: self.distanceViewModel.distance))
        
        let distString = NSMutableAttributedString(string: distStr, attributes: cell.boldAttr)
        let disUnitString = self.distanceViewModel.isMetric ? " km" :" mi"

        distString.append(NSMutableAttributedString(string: disUnitString, attributes: cell.bookAttr))
        
        cell.valueLabel.attributedText = distString
        
        if let avgDistance = self.distanceViewModel.avgDistance {
            if let delegate = self.delegate {
                let date = delegate.fetchEpoch()
                
                if Calendar.current.compare(date.startDate, to: date.endDate, toGranularity: .day) != .orderedSame {
                
                    let avgDistanceStr = self.distanceViewModel.isMetric ? String(format: "%.1f", avgDistance) : String(format:"%.1f",NRConversionUtility.kiloToMiles(valueInKilo: avgDistance))
                    
                    cell.descriptionValueLabel.text = String(format: "%@%@", avgDistanceStr, disUnitString)
                }
            }
        }
    }
    
}

//
//  ActivityNavigationPresenter.swift
//  Nourish
//
//  Created by Andrey Zmushko on 8/29/17.
//  Copyright © 2017 SSIC. All rights reserved.
//

import UIKit

protocol ActivityNavigationProtocol {
    func activityNavigation(_ activityNavigationPresenter: ActivityNavigationPresenter, startDate: Date, endDate: Date)
}

class ActivityNavigationPresenter: NSObject, NRSegmentedControlDelegate, DatePickerProtocol, ActivityDetailDelegate {
    
    var datePicker: DatePickerView?
    
    var dailyDate = Date()
    var weeklyDate = Date()
    var monthlyDate = Date()
    
    private func periodDate() -> Date {
        switch self.period {
        case .daily:
            return self.dailyDate
        case .weekly:
            return self.weeklyDate
        case .monthly:
            return self.monthlyDate
        }
    }
    
    func setupDatePicker(datePicker: DatePickerView) {
        self.datePicker = datePicker
        datePicker.setupWith(deligate: self, schema: .activity, date: self.periodDate())
        datePicker.setupPeriod(period: self.period.datePickerPeriod)
    }
    
    enum Period {
        case daily
        case weekly
        case monthly
        
        var datePickerPeriod: DatePickerView.Period {
            switch self {
            case .daily:
                return .daily
            case .weekly:
                return .weekly
            case .monthly:
                return .monthly
            }
        }
        
        var calendar: Calendar {
            return Calendar.current
        }
        
        func isToday(_ date: Date) -> Bool {
            return self.calendar.isDateInToday(date)
        }
        
        func startDate(date: Date) -> Date {
            switch self {
            case .daily:
                return date
            case .weekly:
                if self.isToday(date) {
                    return self.calendar.date(byAdding: .day, value: -6, to: date)!
                }
                return date
            case .monthly:
                if self.isToday(date) {
                    return calendar.date(byAdding: .day, value: -29, to: date)!
                }
                return date
            }
        }
        
        func endDate(date: Date) -> Date {
            switch self {
            case .daily:
                return date
            case .weekly:
                if self.isToday(date) {
                    return date
                }
                return self.calendar.date(byAdding: .day, value: 6, to: date)!
            case .monthly:
                if self.isToday(date) {
                    return date
                }
                let month = self.calendar.date(byAdding: .month, value: 1, to: date)!
                return self.calendar.date(byAdding: .day, value: -1, to: month)!
            }
        }
    }
    
    var date = Date()
    var period: Period = .daily
    var delegate: ActivityNavigationProtocol?

    func segmentedControlDidPressedItemAtIndex(segmentedControl: NRSegmentedControl, index: Int) {
        self.period = {
            switch index {
            case 1:
                return .weekly
            case 2:
                return .monthly
            default:
                return .daily
            }
        }()
        self.updateDelegate()
    }
    
    func onChange(datePicker: DatePickerView, newDate: Date) {
        
        switch self.period {
        case .daily:
            self.dailyDate = newDate
        case .weekly:
            self.weeklyDate = newDate
        case .monthly:
            self.monthlyDate = newDate
        }
        
        //self.date = newDate
        self.updateDelegate()
    }

    func cellOrder() -> [ActivityCells] {
        switch self.period {
        case .daily:
            if self.period.isToday(self.dailyDate) {
                return [.stayActive, .caloricProgress, .caloriesBurned, .steps, .distance, .sleep, .exercise, .stress, .weight]
            }
            return [.caloricProgress, .caloriesBurned, .steps, .distance, .sleep, .exercise, .stress, .weight]
        case .weekly:
            if self.period.isToday(self.weeklyDate) {
                return [.caloricProgressChart, .caloricBurned, .caloricConsumed, .steps, .distance, .sleep, .exercise, .stress, .weightChart]
            }
            return [.caloricProgressChart, .caloricBurned, .caloricConsumed, .steps, .distance, .sleep, .exercise, .stress, .weightChart]
        case .monthly:
            if self.period.isToday(self.monthlyDate) {
                return [.caloricProgressChart, .caloricBurned, .caloricConsumed, .steps, .distance, .sleep, .exercise, .stress, .weightChart]
            }
            return [.caloricProgressChart, .caloricBurned, .caloricConsumed, .steps, .distance, .sleep, .exercise, .stress, .weightChart]
        }
    }

    private func updateDelegate() {
        if let delegate = self.delegate {
            EventLogger.logOnActivityTScreenClickFor(period: self.period)
            delegate.activityNavigation(self, startDate: self.period.startDate(date: periodDate()),
                                        endDate: self.period.endDate(date: periodDate()))
        }
    }
}

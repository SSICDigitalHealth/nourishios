//
//  NRGoalOptionsPresenter.swift
//  Nourish
//
//  Created by Vino (Vinodha) Sundaramoorthy on 11/28/16.
//  Copyright © 2016 SSIC. All rights reserved.
//

import UIKit
import RxSwift

class NRGoalOptionsPresenter: BasePresenter, UITableViewDelegate,UITableViewDataSource {
    
    var goalOptionView : NRGoalOptionsProtocol?
    var userGoal : UserGoal!
    var goalOptionListArray :[goalOptionsList] = [(goal:UserGoal.EatBetter ,isSelected:false),(goal:UserGoal.LooseWeight ,isSelected:false), (goal:UserGoal.MaintainWeight, isSelected : false)]
    
    // MARK: Base Presenter Protocol
    override func viewWillAppear(_ animated : Bool) {
        let goal = self.goalOptionView?.getGoal()
        if goal == UserGoal.LooseWeight {
                self.goalOptionListArray[2].isSelected = false
                self.goalOptionListArray[1].isSelected = true
                self.goalOptionListArray[0].isSelected = false
            } else if goal == UserGoal.MaintainWeight {
            self.goalOptionListArray[0].isSelected = false
            self.goalOptionListArray[1].isSelected = false
            self.goalOptionListArray[2].isSelected = true
        } else {
            self.goalOptionListArray[0].isSelected = true
            self.goalOptionListArray[1].isSelected = false
            self.goalOptionListArray[2].isSelected = false

        }
            self.goalOptionView?.loadOptions()
    }

    // MARK : TableView Datasource
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return goalOptionListArray.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        let cell = self.tableView(tableView, cellForRowAt: indexPath)
        return cell.frame.size.height
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let data = goalOptionListArray[indexPath.row]
        let cell : NRGoalOptionsTableViewCell = tableView.dequeueReusableCell(withIdentifier: "kGoalOptionsCell") as! NRGoalOptionsTableViewCell
        cell.setUpGoalWithModel(goal: data)
        return cell
    }
    
    // MARK: Tableview Delegate
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        //Pass the selected goal to manage goals
        let data = goalOptionListArray[indexPath.row]
        self.userGoal = data.goal
        self.goalOptionView?.setUserGoal()

    }
}

//
//  OptifastSettingsPresenter.swift
//  Nourish
//
//  Created by Gena Mironchyk on 10/17/17.
//  Copyright © 2017 SSIC. All rights reserved.
//

import UIKit

class OptifastSettingsPresenter: BasePresenter, UITableViewDataSource, UITableViewDelegate {
    private var model = OptifastSettingsViewModel()
    private var interactor = GetOptifastSettingsInteractor()
    private var arrayOfOcasions : [Ocasion]!
    private var arrayOfSectionTitles : [String]!
    private var totalRows : Int = 0
    private var saveSettingsInteractor = SaveOptifastSettingsInteractor()
    
    var optifastVC : OptifastSettingsViewControllerProtocol!
    
    var settingsView : OptifastSettingsViewProtocol!
    
    var localPickedDictionary : [String : Bool]?
    
    override func viewWillAppear(_ animated: Bool) {
        let _ = self.interactor.execute().subscribe(onNext: { [unowned self] viewModel in
            self.model = viewModel
            if self.localPickedDictionary == nil {
                self.localPickedDictionary = self.createLocalPickedDictionary(model: viewModel)
            }
            self.mergeLocalChanges()
            DispatchQueue.main.async {
                self.prepareTableData()
                self.settingsView.reloadData()
            }
        }, onError: {error in
            DispatchQueue.main.async {
                self.settingsView.parseError(error : error, completion : nil)
            }
        }, onCompleted: {}, onDisposed: {})
    }
    
    
    private func mergeLocalChanges() {
        for key in self.localPickedDictionary!.keys {
            if self.model.optifastUserProducts.contains(where: {$0.foodId == key}) {
                if self.localPickedDictionary![key] == false {
                    self.model.optifastUserProducts = self.model.optifastUserProducts.filter({$0.foodId != key})
                }
            } else {
                if self.localPickedDictionary![key] == true {
                    self.model.optifastUserProducts.append(self.model.optifastProductsTotal.first(where: {$0.foodId == key})!)
                }
            }
        }
    }
    
    private func createLocalPickedDictionary (model : OptifastSettingsViewModel) -> [String : Bool] {
        var dictToReturn = [String : Bool]()
        for item in model.optifastProductsTotal {
            dictToReturn[item.foodId] = model.optifastUserProducts.contains(where: {$0.foodId == item.foodId})
        }
        return dictToReturn
    }
    
    private func prepareTableData() {
        self.arrayOfOcasions = [.breakfast, .lunch, .dinner, .snacks]
        self.arrayOfSectionTitles = ["Choose when do you have\nOPTIFAST Mix","Choose preferred OPTIFAST \nproducts"]
        self.totalRows = 1 + self.arrayOfOcasions.count + self.arrayOfSectionTitles.count + self.model.optifastProductsTotal.count + 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.totalRows
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch indexPath.row {
        case 0:
            let infoCell = tableView.dequeueReusableCell(withIdentifier: optifastInfoCellID) as? OptifastSettingsInfoCell ?? OptifastSettingsInfoCell()
            infoCell.selectionStyle = .none
            return infoCell
        case 1:
            let sectionCell = tableView.dequeueReusableCell(withIdentifier: optifastSectionCellID) as? OptifastSettingsSectionCell ?? OptifastSettingsSectionCell()
            sectionCell.setTitleLabelText(message: self.arrayOfSectionTitles[0])
            sectionCell.selectionStyle = .none
            return sectionCell
        case 2, 3, 4, 5 :
            let optifastOcasionCell = tableView.dequeueReusableCell(withIdentifier: optifastOcasionCellID) as? OptifastSettingsOcasionCell ?? OptifastSettingsOcasionCell()
            let ocasion = self.arrayOfOcasions[indexPath.row - 2]
            let contains = self.model.optifastUserOcasions.contains(ocasion)
            optifastOcasionCell.checkMarkButton.tag = indexPath.row - 2
            optifastOcasionCell.checkMarkButton.addTarget(self, action: #selector(changeOcasion(sender:)), for: .touchUpInside)
            optifastOcasionCell.setupWith(ocasionTitle: Ocasion.stringDescription(servingType: ocasion), isSelected: contains)
            optifastOcasionCell.selectionStyle = .none
            return optifastOcasionCell
        case 6:
            let sectionCell = tableView.dequeueReusableCell(withIdentifier: optifastSectionCellID) as? OptifastSettingsSectionCell ?? OptifastSettingsSectionCell()
            sectionCell.setTitleLabelText(message: self.arrayOfSectionTitles[1])
            sectionCell.selectionStyle = .none
            return sectionCell
        case self.totalRows - 1:
         let saveCell = tableView.dequeueReusableCell(withIdentifier: optifastSaveButtonCellID) as? OptifastSettingsSaveButtonCell ?? OptifastSettingsSaveButtonCell()
             saveCell.selectionStyle = .none
             saveCell.savePrefsButton.addTarget(self, action: #selector(saveSettings), for: .touchUpInside)
             return saveCell
        default:
           let optifastProductCell = tableView.dequeueReusableCell(withIdentifier: optifasctProductCellID) as? OptifastProductCell ?? OptifastProductCell()
           let product = self.model.optifastProductsTotal[indexPath.row - 7]
           let contains = self.model.optifastUserProducts.contains(where: {product.foodId == $0.foodId})
           optifastProductCell.setupWith(optifastTitle: product.foodTitle, isSelected: contains)
           optifastProductCell.checkmarkButton.tag = indexPath.row - 7
           optifastProductCell.checkmarkButton.addTarget(self, action: #selector(changeOptifastMeal(sender:)), for: .touchUpInside)
           optifastProductCell.selectionStyle = .default
            return optifastProductCell
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.row != self.totalRows - 1 && indexPath.row > 6 {
            self.optifastVC.openOptifastFoodDetailsFor(model: self.model.optifastProductsTotal[indexPath.row - 7], localSettings: self.createLocalPickedDictionary(model: self.model))
            tableView.deselectRow(at: indexPath, animated: true)
        }
    }
    
    
    func changeOptifastMeal(sender : UIButton) {
        let isSelected = !sender.isSelected
        if isSelected == true {
            self.model.optifastUserProducts.append(self.model.optifastProductsTotal[sender.tag])
        } else {
            self.model.optifastUserProducts = self.model.optifastUserProducts.filter({$0.foodId != self.model.optifastProductsTotal[sender.tag].foodId})
        }
        if self.localPickedDictionary != nil {
            self.localPickedDictionary![self.model.optifastProductsTotal[sender.tag].foodId] = isSelected
        }
        sender.isSelected = isSelected
    }
    
    func changeOcasion(sender : UIButton) {
        let isSelected = !sender.isSelected
        if isSelected == true {
            self.model.optifastUserOcasions.append(self.arrayOfOcasions[sender.tag])
        } else {
            let obj = self.model.optifastUserOcasions.first(where: {$0.rawValue == self.arrayOfOcasions[sender.tag].rawValue})
            if obj != nil {
                let index = self.model.optifastUserOcasions.index(of: obj!)
                if index != nil {
                    self.model.optifastUserOcasions.remove(at: index!)
                }
            }
        }
        sender.isSelected = isSelected
    }
    
    
    func saveSettings() {
        let _ = self.saveSettingsInteractor.execute(settingsViewModel: self.model).subscribe(onNext: { [unowned self] completed in
            DispatchQueue.main.async {
                self.optifastVC.popToRootVC()
            }
        })
    }
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.row == 0 {
            return 150.0
        } else {
            return UITableViewAutomaticDimension
        }
    }
    
}

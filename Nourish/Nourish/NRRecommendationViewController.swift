//
//  NRRecommendationViewController.swift
//  Nourish
//
//  Created by Vino (Vinodha) Sundaramoorthy on 11/14/16.
//  Copyright © 2016 SSIC. All rights reserved.
//

import UIKit

protocol RecommendationProtocol {
    func showRecommendationList()
    func getRecomendationIndexPathForCell(cell:UITableViewCell) -> IndexPath
    func reloadSectionWithIndex(section : Int)
    func startActivityIndicator()    
    func stopActivittyIndicator()
    func showFoodCardwithModel(model:foodSearchModel)
}

class NRRecommendationViewController: BasePresentationViewController ,FoodDetailsViewControllerDelegate {
    
    @IBOutlet weak var recommListView : NRRecommendationsView?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = kRecommendationViewTitle
        self.setUpNavigationBarButtons()
        self.recommListView?.controller = self
        self.baseViews = [recommListView!]
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func setUpNavigationBarButtons () {
        let titleAttribute = self.simplyfiedBarTitileAttributes()
        navigationItem.title = self.title
        self.navigationController?.navigationBar.isTranslucent = false
        self.navigationController?.navigationBar.titleTextAttributes = titleAttribute
        self.navigationController?.navigationBar.barTintColor = NRColorUtility.symplifiedNavigationColor()
        self.navigationController?.navigationBar.tintColor = UIColor.white
    }
    
    func showRecommendationList() {
        LogUtility.logToFile("Load Recommendation")
    }
    
    func showFoodCardwithModel(model:foodSearchModel) {
        let vc = NRFoodDetailsViewController(nibName: "NRFoodDetailsViewController", bundle: nil)
        vc.title = model.foodTitle
        vc.foodSearchModel = model
        vc.updatingAnmount = false
        vc.ocassion = model.occasion
        if model.userPhotoId == nil {
            vc.backgroundImage = NavigationUtility.takeScreenShot()
            vc.delegate = self
            self.present(vc, animated : true)
        } else {
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    
    func foodDetailsViewControllerDidFinishWith(foodSearchModel: foodSearchModel, presentedModal: Bool) {
        self.recommListView?.itemAdded = true
        if presentedModal == true {
            self.dismiss(animated: false) {
            }
        }
    }
    
}

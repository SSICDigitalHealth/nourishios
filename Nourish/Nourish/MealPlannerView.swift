//
//  MealPlannerView.swift
//  Nourish
//
//  Created by Vlad Birukov on 21.08.17.
//  Copyright © 2017 SSIC. All rights reserved.
//

import UIKit

protocol MealPlannerProtocol : BaseViewProtocol {
    func registerTableViewCells()
    func reloadData()
    func getIndexCell(cell: UITableViewCell) -> IndexPath
}

class MealPlannerView: BaseView, MealPlannerProtocol {
    @IBOutlet weak var mealPlannerTable: UITableView!
    @IBOutlet var presenter: MealPlannerPresenter!

    let heightCell = 150
    
    override func viewDidLoad() {
        self.xibAutoLayouting(nibName: "MealPlannerView")
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.basePresenter = presenter
        self.presenter.mealPlannerView = self
        self.registerTableViewCells()
        self.settingTableCell()
        super.viewWillAppear(animated)
    }
    

    
    func reloadData() {
        self.mealPlannerTable.reloadData()
    }

    func getIndexCell(cell: UITableViewCell) -> IndexPath {
        return  self.mealPlannerTable.indexPath(for: cell)!
    }

    
    func registerTableViewCells() {
        self.mealPlannerTable.register(UINib(nibName: "MealPlannerTableViewCell", bundle: nil), forCellReuseIdentifier: "cellMealPlanner")
        self.mealPlannerTable.register(UINib(nibName: "HeaderMealPlannerTableViewCell", bundle: nil), forCellReuseIdentifier: "cellheaderMealPlanner")
    }
    
    private func settingTableCell() {
        self.mealPlannerTable.rowHeight = UITableViewAutomaticDimension
        self.mealPlannerTable.estimatedRowHeight = CGFloat(self.heightCell)
    }
}

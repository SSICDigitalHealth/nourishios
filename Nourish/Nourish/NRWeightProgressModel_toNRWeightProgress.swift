//
//  NRWeightProgressModel_toNRWeightProgress.swift
//  Nourish
//
//  Created by Vino (Vinodha) Sundaramoorthy on 12/6/16.
//  Copyright © 2016 SSIC. All rights reserved.
//

import UIKit

class NRWeightProgressModel_toNRWeightProgress: NSObject {
    func transform(model : NRWeightProgressModel) -> NRWeightProgress {
        let object = NRWeightProgress()
        object.currentWeight = model.currentWeight
        object.metrics = model.metrics
        object.progressPercentage = model.progressPercentage
        object.weightProgress = model.weightProgress
        object.dailyWeightLogged = model.dailyWeightLogged
        object.userId = model.userId
        return object
    }
    
    func transform(modelArray : [NRWeightProgressModel]) -> [NRWeightProgress] {
        var arrayToReturn : [NRWeightProgress] = []
        for model in modelArray {
            arrayToReturn.append(self.transform(model: model))
        }
        return arrayToReturn
    }

}

//
//  CaloriesDailyAverageViewModel.swift
//  Nourish
//
//  Created by Vlad Birukov on 29.08.17.
//  Copyright © 2017 SSIC. All rights reserved.
//

import Foundation

class CaloriesDailyAverageViewModel: NSObject {
    var caloriesDailyAverage: Double = 0.0
    var caloriesOverTarget: Double = 0.0
}

//
//  NestleRecommendatitionCacheDataStore.swift
//  Nourish
//
//  Created by Gena Mironchyk on 9/20/17.
//  Copyright © 2017 SSIC. All rights reserved.
//

import UIKit
import RealmSwift
class NestleRecommendatitionCacheDataStore: NSObject {
    
    func store(cache : NestleRecommendationsCache, startDate : Date, endDate : Date) {
        
        
        let realm = try! Realm()
        let predicate = NSPredicate(format: "startDate == %@ && endDate == %@", Calendar.current.startOfDay(for:startDate) as NSDate, Calendar.current.startOfDay(for:endDate) as NSDate)
        let objs = realm.objects(NestleRecommendationsCache.self).filter(predicate)
        if objs.count > 0 {
            try! realm.write {
                realm.delete(objs, cascading: true)
            }
        }
        var recsCache = NestleRecommendationsCache()
        recsCache = cache
        recsCache.startDate = Calendar.current.startOfDay(for:startDate)
        recsCache.endDate = Calendar.current.startOfDay(for:endDate)
        
        try! realm.write {
            realm.add(recsCache)
        }
        
    }
    
    func getRecsCache(startDate : Date, endDate : Date) -> NestleRecommendationsCache? {
        let realm = try! Realm()
        let predicate = NSPredicate(format: "startDate == %@ && endDate == %@", Calendar.current.startOfDay(for:startDate) as NSDate, Calendar.current.startOfDay(for:endDate) as NSDate)
        let objs = realm.objects(NestleRecommendationsCache.self).filter(predicate)
        if objs.count > 0 {
            return objs.first!
        } else {
            return nil
        }
    }
    
    func getDatesToRefetchFrom(date : Date) -> [(Date, Date)] {
        let realm = try! Realm()
        let currentDate = Calendar.current.startOfDay(for: date)
        let predicate = NSPredicate(format : "startDate <= %@ && endDate >= %@",currentDate as NSDate, currentDate as NSDate)
        let objs = realm.objects(NestleRecommendationsCache.self).filter(predicate)
        
        var array = [(Date, Date)]()
        
        for obj in objs {
            if obj.startDate != nil && obj.endDate != nil {
                array.append((obj.startDate!, obj.endDate!))
            }
        }
        
        return array
    }
    
    func removeCacheFrom(date: Date) {
        let realm = try! Realm()
        let calendar = Calendar.current
        let currentDate = calendar.startOfDay(for: date)
        
        let predicate = NSPredicate(format : "startDate <= %@ && endDate => %@", currentDate as NSDate, currentDate as NSDate)
        
        let objs = realm.objects(NestleRecommendationsCache.self).filter(predicate)
        if objs.count > 0 {
            try! realm.write {
                realm.delete(objs, cascading: true)
            }
        }
    }
    
    func removeAllCache () {
        let realm = try! Realm()
        let objs = realm.objects(NestleRecommendationsCache.self)
        if objs.count > 0 {
            try! realm.write {
                realm.delete(objs, cascading: true)
            }
        }        
    }
}

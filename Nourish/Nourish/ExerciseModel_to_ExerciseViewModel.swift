//
//  ExerciseModel_to_ExerciseViewModel.swift
//  Nourish
//
//  Created by Vlad Birukov on 29.08.17.
//  Copyright © 2017 SSIC. All rights reserved.
//

import Foundation

class ExerciseModel_to_ExerciseViewModel {
    func transform(model: ExerciseModel) -> ExerciseViewModel {
        let exerciseViewModel = ExerciseViewModel()
        exerciseViewModel.activeHours = model.activeHours
        if model.activityList.count != 0 {
            exerciseViewModel.activityList = model.activityList
        }
        return exerciseViewModel
    }
}


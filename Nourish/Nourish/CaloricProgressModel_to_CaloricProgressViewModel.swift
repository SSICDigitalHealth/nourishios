//
//  CaloricProgressModel_to_CaloricProgressViewModel.swift
//  Nourish
//
//  Created by Vlad Birukov on 29.08.17.
//  Copyright © 2017 SSIC. All rights reserved.
//

import Foundation

class CaloricProgressModel_to_CaloricProgressViewModel {
    func transform(model: CaloricProgressModel) -> CaloricProgressViewModel {
        let caloricProgressViewModel = CaloricProgressViewModel()
        
        if model.userGoal != nil {
            caloricProgressViewModel.userGoal = model.userGoal
        }
        
        if model.weeklyPlan != nil {
            caloricProgressViewModel.weeklyPlan = model.weeklyPlan
        }
        
        if model.activityLevel != nil {
            caloricProgressViewModel.activityLevel = model.activityLevel
        }
        
        if model.caloricState != nil {
            caloricProgressViewModel.caloricState = [caloricProgress]()

            caloricProgressViewModel.caloricState = model.caloricState
        }
        
        caloricProgressViewModel.isMetric = model.isMetric
        
        return caloricProgressViewModel
    }
}

//
//  DatePickerTableViewCell.swift
//  Nourish
//
//  Created by Vlad Birukov on 24.08.17.
//  Copyright © 2017 SSIC. All rights reserved.
//

import UIKit

class DatePickerTableViewCell: UITableViewCell {
    var presenter: ActivityNavigationPresenter?
    var datePickerView: DatePickerView?

    override func awakeFromNib() {
        super.awakeFromNib()
        self.setupContent(&self.datePickerView, nibName: "DatePickerView")
    }
    
    func setUpWith(presenter: ActivityNavigationPresenter) {
        self.presenter = presenter
        if let presenter = self.presenter {
            self.datePickerView?.setupWith(deligate: presenter, schema: .activity, date: Date())
            
        }
        self.presenter?.datePicker = self.datePickerView
    }
    
}

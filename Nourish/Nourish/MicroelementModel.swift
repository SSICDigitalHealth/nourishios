//
//  MicroelementModel.swift
//  Nourish
//
//  Created by Vlad Birukov on 06.06.17.
//  Copyright © 2017 SSIC. All rights reserved.
//

import Foundation

enum MicroelementType : Int {
    case sugars = 1
    case salt
    case saturatedFat

    static func description(type:MicroelementType) -> String {
        switch (type) {
        case .sugars:
            return "Added sugars"
        case .salt:
            return "Sodium"
        case .saturatedFat:
            return "Saturated fat"
        }
    }
    static func typeFrom(foodId : String) -> MicroelementType{
        switch foodId {
        case "Added_sugars": return self.sugars
        case "Fat_saturated": return self.saturatedFat
        case "Sodium" : return self.salt
        default : return self.salt
        }
    }
}

typealias microelementList = (date: Date?, elements: [(type: MicroelementType, element: UserMicroelement)])

class MicroelementModel: NSObject {
    var data: [microelementList]?
    var detailInformation: [DetailInformationModel]?
}

class UserMicroelement {
    var consumedMicroelement: Double = 0.0
    var targetMicroelement: Double = 0.0
    var unit: String = ""
    
    init() {
    }
    
    init(consumed: Double, target: Double, unit: String) {
        self.consumedMicroelement = consumed
        self.targetMicroelement = target
        self.unit = unit
    }
}

class DetailInformationModel {
    var title: String = ""
    var message: String = ""
    var topFood: [String] = [String]()
}

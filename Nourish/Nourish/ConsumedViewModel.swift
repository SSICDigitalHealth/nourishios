//
//  ConsumedViewModel.swift
//  Nourish
//
//  Created by Vlad Birukov on 31.08.17.
//  Copyright © 2017 SSIC. All rights reserved.
//

import Foundation

class ConsumedViewModel {
    var consumedCalories: Double?
    var avgConsumed: Double?
}

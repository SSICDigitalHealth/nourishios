//
//  FrequentMealTableViewCell.swift
//  Nourish
//
//  Created by Vino (Vinodha) Sundaramoorthy on 5/2/17.
//  Copyright © 2017 SSIC. All rights reserved.
//

import UIKit

class FrequentMealTableViewCell: MGSwipeTableCell {
    @IBOutlet var titleLabel : UILabel?
    @IBOutlet var servingCountLabel : UILabel?
    @IBOutlet var caloriesLabel : UILabel?
    var ocassion : Ocasion? = nil
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.servingCountLabel?.textColor = UIColor.black.withAlphaComponent(0.54)
        self.caloriesLabel?.textColor = UIColor.black.withAlphaComponent(0.54)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func setupWithModel(model : foodSearchModel) {
        var servingType : String = ""
        titleLabel?.text = model.foodTitle
        self.ocassion = model.occasion
        
        let scoreFormatter    = NumberFormatter()
        scoreFormatter.minimumFractionDigits = 0
        scoreFormatter.maximumFractionDigits = 2
        
        if model.unit.count > 0 {
            servingType = model.unit
        }
        
        if (servingType.count) > 0  {
            let servingAmount = scoreFormatter.string(from: NSNumber(value: model.amount))
            servingCountLabel?.text = String(format:"%@ %@",servingAmount!,model.unit)
        } else {
            servingCountLabel?.text = servingType
        }
        caloriesLabel?.text = String(format : "%.1f cal", Double(model.calories)!)
    }
    
    func setupWithModel(record : MealRecordModel) {
        var servingType : String = ""
        titleLabel?.text = record.descriptionText()
        self.ocassion = record.ocasion

        let scoreFormatter    = NumberFormatter()
        scoreFormatter.minimumFractionDigits = 0
        scoreFormatter.maximumFractionDigits = 2
        
        if let  ser = record.servingType {
            servingType = ser
        }
        
        if (servingType.count) > 0  {
            let servingAmount = scoreFormatter.string(from: NSNumber(value: record.servingAmmount!))
            servingCountLabel?.text = String(format:"%@ %@",servingAmount!,record.servingType!)
        } else {
            servingCountLabel?.text = servingType
        }
        caloriesLabel?.text = String(format : "%.1f cal",record.groupCalories())

    }
    
}

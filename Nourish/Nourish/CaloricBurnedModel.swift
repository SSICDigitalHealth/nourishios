//
//  CaloricBurnedModel.swift
//  Nourish
//
//  Created by Vlad Birukov on 28.08.17.
//  Copyright © 2017 SSIC. All rights reserved.
//

import Foundation

typealias caloricStateData = (subtitle: Date, cal: (basalCalories: Double, activeCalories: Double))

class CaloricBurnedModel {
    var userBurnedCal: Double?
    var avgBurnedCal: Double?
    var avgActiveCal: Double?
    var caloricState: [caloricStateData]?

}

//
//  UserDeviceCache.swift
//  Nourish
//
//  Created by Nova on 12/9/16.
//  Copyright © 2016 SSIC. All rights reserved.
//

import UIKit
import RealmSwift
import RxSwift


let kDtidString = "dtidDevice"
let kUdidString = "udidDevice"
let kDeviceNameString = "nameDeviceString"
let kDeviceLastSynced = "kDeviceLastSynced"

class UserDeviceCache: NSObject {
    
    let artik = ArtikCloudConnectionManager()
    let fromJsonMapper = Json_to_UserDevice()
    
    /*
    func defaultDevice() -> UserDevice {
        let device = UserDevice()
        device.dtidString = kDeviceID
        device.udidString = kDeviceID
        device.name = kDeviceString
        //self.storeCurrentDevice(device: device)
        return device
    }
    */
   /*
    func currentDevice() -> UserDevice {
        
        let device = self.defaultDevice()
        
        if defaults.object(forKey: kDtidString) != nil {
            device.dtidString = defaults.object(forKey: kDtidString) as! String
        }
        
        if defaults.object(forKey: kUdidString) != nil {
            device.udidString = defaults.object(forKey: kUdidString) as! String
        }
        
        if defaults.object(forKey: kDeviceNameString) != nil {
            device.name = defaults.object(forKey: kDeviceNameString) as! String
        }
        
        return device
    }
 */
    
    func currentDevice(userID : String) -> Observable<UserDevice> {
        let results = try! Realm().objects(UserDevice.self).filter("userID == %@",userID)
        
        if results.count > 0 {
            let device = Array(results).first
            return Observable.just(device!)
        } else {
            let object = self.artik.getUserDevices().flatMap {devicesDict -> Observable<UserDevice> in
                
                let device = UserDevice()
                device.userID = userID
                device.dtidString = kDeviceID
                device.deviceName = UUID().uuidString
                self.storeCurrentDevuce(device: device)
                let deviceName = device.deviceName
                
                let devices = self.fromJsonMapper.transform(devices: devicesDict, userID: userID)
                var contains = false
                
                
                for proxyDevice in devices {
                    if proxyDevice.deviceName == deviceName {
                        contains = true
                    }
                }

                if contains == false {
                    let some = self.artik.saveUserDevice(userDevice: device).flatMap{stored -> Observable<UserDevice> in
                        return Observable.just(device)
                    }
                    return some
                } else {
                    return Observable.just(device)
                }
                
            }
            return object
        }
        
    }
    
    func storeCurrentDevuce(device : UserDevice) {
        let realm = try! Realm()
        try! realm.write {
            realm.add(device)
            self.storeLastSyncedDate(date: Date())
        }
    }
  /*
    func storeCurrentDevice(device : UserDevice) {
        defaults.setValue(device.dtidString, forKey: kDtidString)
        defaults.setValue(device.udidString, forKey: kUdidString)
        defaults.setValue(device.name, forKey: kDeviceNameString)
        storeLastSyncedDate(date: Date())
        defaults.synchronize()
    }
   */
    func storeLastSyncedDate(date : Date) {
        defaults.setValue(date, forKey: kDeviceLastSynced)
        defaults.synchronize()
    }
    
    func lastSyncedDate() -> Date? {
        return defaults.value(forKey: kDeviceLastSynced) as? Date
    }
    
    func past5minutesLastSync() -> Bool {
        let dateLastSynced = self.lastSyncedDate()
        if dateLastSynced != nil {
            let calendar = Calendar.current
            let date = calendar.date(byAdding: .minute, value: -5, to: Date())
            return dateLastSynced!.compare(date!) == .orderedDescending ? true : false
        }
        return false
        
    }
}

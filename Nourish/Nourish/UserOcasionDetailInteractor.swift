//
//  UserOcasionDetailInteractor.swift
//  Nourish
//
//  Created by Vlad Birukov on 19.06.17.
//  Copyright © 2017 SSIC. All rights reserved.
//

import Foundation
import RxSwift


class UserOcasionDetailInteractor {
    let repository = NestleProgressRepository.shared
    
    func execute(startDate: Date, endDate: Date) -> Observable<CurrentStateFoodStuffs> {
        return repository.getProgressFor(startDate: startDate, endDate: endDate, returnHistory: true).flatMap({observable -> Observable<CurrentStateFoodStuffs> in
            
            return Observable.just(self.transform(model: observable.userOcasion))
        })
    }
    
    private func transform(model: [Element]) -> CurrentStateFoodStuffs {
        let currentStateFoodStuffs = CurrentStateFoodStuffs()
        let nameFoodIntake = [Ocasion.breakfast, Ocasion.lunch, Ocasion.snacks ,Ocasion.dinner]
        
        for i in 0..<nameFoodIntake.count {
            let stateFoodStuf = StateFoodStuffsModel()
            stateFoodStuf.type = nameFoodIntake[i]
            if model.filter({Ocasion.enumFrom(stringRep: $0.nameFoodIntake) == nameFoodIntake[i] }).count > 0 {
                stateFoodStuf.arrFoodElement = [FoodElementModel]()
                for element in model.filter({Ocasion.enumFrom(stringRep: $0.nameFoodIntake) == nameFoodIntake[i] }) {
                    let foodElement = FoodElementModel()
                    foodElement.nameFood = element.nameFood
                    if element.amountFood != nil {
                        foodElement.amountFood = element.amountFood
                    }
                    
                    if element.unit != nil {
                        foodElement.unit = element.unit
                    }
                    stateFoodStuf.arrFoodElement?.append(foodElement)
                }
            }
            currentStateFoodStuffs.arrCurentStateFood.append(stateFoodStuf)
        }

        return currentStateFoodStuffs
    }

}

//
//  NRFormatterUtility.swift
//  Nourish
//
//  Created by Vlad Birukov on 27.09.17.
//  Copyright © 2017 SSIC. All rights reserved.
//

import Foundation

class NRFormatterUtility {
    
    class func numberToStringMealPlanner(number: Double) -> String {
        var resultString = ""
        let numberDecimal = Decimal(number).significantFractionalDecimalDigits
        
        if numberDecimal >= 2 {
            resultString = String(format: "%.2f", number.roundToPlaces(places: 2))
        } else {
            resultString = String(format: "%.1f", number.roundToPlaces(places: 1))
        }
        return resultString
    }
    
    class func numberToStringNutrition(number: Double) -> String {
        var resultString = ""
        let numberDecimal = Decimal(number.roundToPlaces(places: 1)).significantFractionalDecimalDigits
        if numberDecimal >= 1 {
            resultString = String(format: "%.1f", number)
        } else {
            resultString = String(format: "%.0f", number.roundToPlaces(places: 0))

        }
        return resultString
    }
    
    class func roundCupElements(number: Double) -> Double {
        if number == 0.0 {
            return 0
        } else {
            let roundNumber = round(number * 4 ) / 4
            let numberDecimal = Decimal(roundNumber).significantFractionalDecimalDigits
            
            if numberDecimal == 0 {
                return roundNumber
            } else {
                return roundNumber
            }
        }
    }
    
    class func doubleToString(num: Double) -> String {
        let numberDecimal = Decimal(num).significantFractionalDecimalDigits
        
        if numberDecimal == 0 {
            return  String(format: "%.0f", num)
        } else {
            return String(num)
        }
    }
    
  }

extension Decimal {
    var significantFractionalDecimalDigits: Int {
        return max(-exponent, 0)
    }
}

extension Double {
    func roundToPlaces(places:Int) -> Double {
        let divisor = pow(10.0, Double(places))
        return Darwin.round(self * divisor) / divisor
    }
    
    func roundMicroelement(target: Double) -> Double {
        if target < 10 {
            return self.roundToPlaces(places: 1)
        } else {
            return self.roundToPlaces(places: 0)
        }
    }
    
    func roundToTenIfLessOrMore(target: Double) -> String {
        if target < 10 {
            return String(format: "%.1f", self.roundToPlaces(places: 1))
        }
        return String(format: "%.0f", self)
    }
}

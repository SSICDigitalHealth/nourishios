//
//  UserActivity.swift
//  Nourish
//
//  Created by Nova on 11/21/16.
//  Copyright © 2016 SSIC. All rights reserved.
//

import UIKit

class UserActivity: NSObject {
    var activityType : userActivityType? = .UserIsActive
    var startDate : Date?
    var endDate : Date?
}

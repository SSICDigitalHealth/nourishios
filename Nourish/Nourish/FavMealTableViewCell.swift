//
//  FavMealTableViewCell.swift
//  Nourish
//
//  Created by Vino (Vinodha) Sundaramoorthy on 5/3/17.
//  Copyright © 2017 SSIC. All rights reserved.
//

import UIKit

class FavMealTableViewCell: MGSwipeTableCell {
    @IBOutlet var titleLabel : UILabel?
    @IBOutlet var caloriesLabel : UILabel?
    @IBOutlet var favImage : UIImageView?
    @IBOutlet var unitLabel : UILabel!

    var ocassion : Ocasion? = nil
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.caloriesLabel?.textColor = UIColor.black.withAlphaComponent(0.54)
    }

    func setupWithModel(record : MealRecordModel) {
        titleLabel?.text = record.descriptionText()
        self.ocassion = record.ocasion
        caloriesLabel?.text = String(format : "%.1f cal",record.groupCalories())
        
        let imageName = record.isGroupFavourite ? "favgroup_icon_01" : "fav_icon_01"
        self.favImage?.image = UIImage(named:imageName)
        
        if record.groupedMealArray.count > 1 {
            self.unitLabel.isHidden = true
        } else {
            let initialRecord = record.groupedMealArray.first
            if initialRecord != nil {
                if initialRecord?.servingAmmount != nil && initialRecord?.servingType != nil {
                    self.unitLabel.isHidden = false
                    self.unitLabel.text = String(format : "%.1f %@",initialRecord!.servingAmmount!, initialRecord!.servingType!)
                }
            }
        }
        
    }
}

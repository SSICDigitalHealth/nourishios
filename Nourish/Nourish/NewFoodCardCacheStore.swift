//
//  NewFoodCardCacheStore.swift
//  Nourish
//
//  Created by Nova on 10/3/17.
//  Copyright © 2017 SSIC. All rights reserved.
//

import UIKit
import RxSwift
import RealmSwift

class NewFoodCardCacheStore: NSObject {
    
    func fetchForIdent(identifier : String) -> Data? {
        let realm = try! Realm()
        let predicate = NSPredicate(format : "ident == %@", identifier)
        
        let object = realm.objects(NewFoodCardCache.self).filter(predicate).first
        
        return object?.data
    }
    
    func storeData(data : Data, date : Date, ident : String) {
        let realm = try! Realm()
        let predicate = NSPredicate(format : "ident == %@", ident)
        
        let objects = realm.objects(NewFoodCardCache.self).filter(predicate)
        
        try! realm.write {
            for object in objects {
                realm.delete(object)
            }
            
            let obj = NewFoodCardCache()
            obj.data = data
            obj.date = date
            obj.ident = ident
            realm.add(obj)
        }
    }
    
}

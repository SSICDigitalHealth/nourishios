//
//  NRFoodSearchView.swift
//  Nourish
//
//  Created by Vino (Vinodha) Sundaramoorthy on 11/16/16.
//  Copyright © 2016 SSIC. All rights reserved.
//

import UIKit
import AVFoundation
import RxSwift

enum SelectedTab : Int{
    case kFrequentMealsTab
    case kFavouritesTab
    case kPhotoTab
    case kVoiceSearchTab
    case kSelectedNone
}

class NRFoodSearchView: BaseView ,NRFoodSearchProtocol {
    
    var controller = NRFoodSearchViewController()
    var presenter = NRFoodSearchPresenter()
    var barCodeScanComplete : Bool = false
    var itemAdded : Bool = false
    var searchBarTopConstraint : CGFloat = 60

    @IBOutlet weak var resultsTableView : UITableView!
    @IBOutlet weak var voiceSearchButton = UIButton()
    @IBOutlet weak var searchBar = UISearchBar()
    @IBOutlet weak var voiceProgressView : UIView!
    @IBOutlet weak var voiceProgressImageView : UIImageView!
    @IBOutlet weak var searchBarView : UIView!
    @IBOutlet weak var segmentControl : NRSegmentedControl!
    @IBOutlet weak var noResultsLabel : UILabel!
    @IBOutlet weak var inactiveSearchImage : UIImageView!
    @IBOutlet weak var searchBarConstraint : NSLayoutConstraint!

    var currentSelectedIndex : SelectedTab = .kSelectedNone
    
    // MARK : UIView Lifecycle
    
    func updateView () {
        self.searchBar?.layoutSubviews()
        self.searchBar?.setNeedsDisplay()
        
        if self.controller.automaticSearchText.count > 0 {
            self.deselctAllTabs(dismissKeyboard: true)
            self.segmentControl.delegate?.segmentedControlDidDeselectAll!(segmentedControl: self.segmentControl, dismissKeyboard: true)
            self.searchBar?.text = self.controller.automaticSearchText
            self.controller.automaticSearchText = ""
            self.toggleInActiveSearchImage(canShow:true)
            let searchField = searchBar?.value(forKey: "_searchField") as! UITextField
            searchField.layer.borderColor = NRColorUtility.appBackgroundColor().cgColor
            self.deselctAllTabs(dismissKeyboard: false)
            self.presenter.searchBarSearchButtonClicked(searchBar!, dismissKeyboard: false)
        } else {
            if self.currentSelectedIndex != .kSelectedNone {
                self.segmentControl.selectItemAtIndex(index: self.currentSelectedIndex.rawValue, withAnimation: true)
                self.segmentControl.delegate?.segmentedControlDidPressedItemAtIndex!(segmentedControl: self.segmentControl, index: self.currentSelectedIndex.rawValue)
            } else {
                self.deselctAllTabs(dismissKeyboard: true)
                self.segmentControl.delegate?.segmentedControlDidDeselectAll!(segmentedControl: self.segmentControl, dismissKeyboard: true)
            }
        }
    }
    
    override func layoutSubviews() {
        print("Layout Subviews")
        super.layoutSubviews()
        self.backgroundColor = UIColor.white
        self.searchBar?.layoutSubviews()

        let searchField = self.searchBar?.value(forKey: "_searchField") as! UITextField
        searchField.frame = CGRect(x:searchField.frame.origin.x, y:searchField.frame.origin.y, width:searchField.frame.size.width, height:36)
    }
    
    
    // MARK : Set up
    func initialSetUp(canShowPrediction : Bool) {
        self.resultsTableView?.register(UINib(nibName: "NRFoodSearchTableViewCell", bundle: nil), forCellReuseIdentifier: "kFoodSearchCell")
        self.resultsTableView?.register(UINib(nibName: "MealPreviewTableViewCell", bundle: nil), forCellReuseIdentifier: "FoodCell")
        self.resultsTableView?.register(UINib(nibName: "FrequentMealTableViewCell", bundle: nil), forCellReuseIdentifier: "kFrequentMealCell")
        self.resultsTableView?.register(UINib(nibName: "FavMealTableViewCell", bundle: nil), forCellReuseIdentifier: "kFavMealCell")
        self.setUpSearchBar()
        self.setUpSearchResult()
        self.setUpVoiceSearch()
        self.setUpSegmentControl()
        self.presenter.occasion = self.controller.occasion
        self.presenter.date = self.controller.date
        self.presenter.showPredictMeal = canShowPrediction
        self.noResultsLabel.isHidden = true
        self.searchBar?.layoutSubviews()
        self.searchBar?.setNeedsLayout()
    }
    
    func setSearchBarFrame() {
        self.searchBar?.layoutSubviews()
        
        let searchField = self.searchBar?.value(forKey: "_searchField") as! UITextField
        searchField.frame = CGRect(x:searchField.frame.origin.x, y:searchField.frame.origin.y, width:searchField.frame.size.width, height:36)
    }
    
    func setUpSegmentControl() {
        let segmentColor = NRColorUtility.newSearchBarBackgroundColor()
        let paddingHeight = UIApplication.shared.statusBarFrame.height > 20 ? searchBarTopConstraint + 84 : searchBarTopConstraint + 60
        let yPosition : CGFloat = self.controller.showAsModel ? paddingHeight : searchBarTopConstraint
        self.segmentControl = NRSegmentedControl(frame: CGRect(x: 0, y: yPosition, width: UIScreen.main.bounds.width, height: 54), titles: ["freq_inactive","fav_inactive","imagrec_inactive","voice_inactive"],selectedtitles:["freq_active","fav_active","imagrec_active","voice_active"], action: {
            control, index in
        })
        self.segmentControl.appearance = segmentAppearance(backgroundColor: segmentColor, selectedBackgroundColor:segmentColor, textColor: segmentColor, font: UIFont.systemFont(ofSize: 10), selectedTextColor: segmentColor, selectedFont: UIFont.systemFont(ofSize: 15), bottomLineColor: UIColor.black.withAlphaComponent(0.12), selectorColor:NRColorUtility.appBackgroundColor(), bottomLineHeight: 1.0, selectorHeight: 2, labelTopPadding: 0, hasImage: true, imageSize: CGSize(width: 24, height: 24))
        self.segmentControl.delegate = presenter
       
        /*self.segmentControl.selectItemAtIndex(index: 0, withAnimation: true)
        self.segmentControl.delegate?.segmentedControlDidPressedItemAtIndex!(segmentedControl: self.segmentControl, index: 0)
        self.currentSelectedIndex = .kSelectedNone*/
        self.addSubview(self.segmentControl)
    }
    
    
    func imageWithColor(color: UIColor) -> UIImage {
        let rect = CGRect(x: 0.0, y: 0.0, width: 1.0, height: 30.0)
        UIGraphicsBeginImageContext(rect.size)
        let context = UIGraphicsGetCurrentContext()
        context!.setFillColor(color.cgColor);
        context!.fill(rect);
        let image = UIGraphicsGetImageFromCurrentImageContext();
        UIGraphicsEndImageContext();
        return image!
    }
    
    func setUpSearchBar() {
        print("Status bar height \(UIApplication.shared.statusBarFrame.height)")
        if self.controller.showAsModel {
            self.searchBarConstraint.constant = UIApplication.shared.statusBarFrame.height > 20 ? searchBarTopConstraint + 24 : searchBarTopConstraint
        }
        self.searchBar?.setBackgroundImage(self.searchBar?.imageWithColor(color:  UIColor.white), for: .any ,barMetrics: .default)
        self.searchBar?.setSearchFieldBackgroundImage(self.imageWithColor(color: UIColor.white), for: .normal)

        self.searchBar?.setImage(UIImage(named: "searchbar"), for: .search, state: .normal)
        self.searchBar?.setImage(UIImage(named: "searchbar_clear_active"), for: .clear, state: .normal)

        let searchField = self.searchBar?.value(forKey: "_searchField") as! UITextField
        searchField.layer.borderColor = UIColor.gray.cgColor
        searchField.layer.borderWidth = 1
        searchField.frame = CGRect(x:searchField.frame.origin.x, y:searchField.frame.origin.y, width:searchField.frame.size.width, height:36)

        self.searchBar?.delegate = self.presenter
        self.searchBar?.text = ""
        self.searchBar?.placeholder = "Search food or brand"
        self.setSearchBarFrame()
    }
    
    func setUpVoiceSearch() {
        //Hide voice search progress view
        self.voiceProgressView.isHidden = true
    }
    
    
    func setUpSearchResult() {
        self.resultsTableView?.delegate = self.presenter
        self.resultsTableView?.dataSource = self.presenter
        self.presenter.searchView = self
        self.resultsTableView?.keyboardDismissMode = .onDrag
        self.resultsTableView?.reloadData()
    }
    
    // MARK: IBActions
    func showVoiceSearch() {
        self.checkPermissionsForAudio()
        self.searchBar?.resignFirstResponder()
        
    }
    
    func hideTableView() {
        DispatchQueue.main.async {
            self.resultsTableView.isHidden = true
        }
    }
    
    func toggleInActiveSearchImage(canShow:Bool) {
        DispatchQueue.main.async {
            self.inactiveSearchImage.isHidden = canShow
        }
    }
    
    func showTableView() {
        DispatchQueue.main.async {
            self.resultsTableView.isHidden = false
        }
    }
    
    private func checkPermissionsForAudio() {
        let mediaType = AVMediaTypeAudio
        let authStatus = AVCaptureDevice.authorizationStatus(forMediaType: mediaType)
        switch authStatus {
        case .authorized:
            self.controller.showSoundHoundVoiceSearch()
            break
        case .denied:
            self.controller.showErrorAlertFor(type : mediaType)
            break
            
        case .restricted:
            self.controller.showSoundHoundVoiceSearch()
            break
            
        case .notDetermined:
            AVCaptureDevice.requestAccess(forMediaType : mediaType, completionHandler: { granted in
                if granted{
                    self.controller.showSoundHoundVoiceSearch()
                } else {
                    self.controller.showErrorAlertFor(type : mediaType)
                }
            })
            break
        }
    }

    
    func showSearchResult(dismissKeyboard : Bool) {
        if dismissKeyboard == true {
            self.searchBar?.resignFirstResponder()
        }
        self.presenter.searchBarSearchButtonClicked(searchBar!, dismissKeyboard: dismissKeyboard)
        self.showToast()
    }
    
    func reloadTableView() {
        self.resultsTableView?.isHidden = false
        self.resultsTableView.tableHeaderView = UIView(frame: CGRect(x: 0, y: 0, width: self.resultsTableView.bounds.width, height: 1))
        self.resultsTableView.tableFooterView = UIView(frame: CGRect(x: 0, y: 0, width: self.resultsTableView.bounds.width, height: 1))
        self.noResultsLabel.isHidden = true
        self.resultsTableView?.reloadData()
        self.stopActivity()
        if self.itemAdded {
            self.controller.showToast(message: "Item Added", showingError: false, completion: nil)
            self.itemAdded = false
        }

    }
    
    private func showNoResultsLabelWith(text : String) {
        self.resultsTableView?.isHidden = true
        if self.currentSelectedIndex != .kSelectedNone {
            self.noResultsLabel.isHidden = false
            self.noResultsLabel.text = text
        }
    }
    
    func showNoResultsLabel() {
        self.showNoResultsLabelWith(text: "No matches found")
    }
    
    func showEmptyFrequentMealsLabel () {
        self.showNoResultsLabelWith(text: "Your frequently logged meals will appear here")
    }
    
    func openFoodDetailsWithModel(model:foodSearchModel, isFrequentMeal : Bool) {
        self.controller.openFoodDetailsWithModel(model: model, isFrequentMeal : isFrequentMeal)
    }
    
    func openGroupDetailswithModel(model:MealRecordModel, date : Date) {
        self.controller.openGroupDetailswithModel(model:model, date : date)
    }
    
    func updateVoiceSearchState(state : SKSState) {
        self.controller.updateVoiceSearchState(state: state)
    }
    
    func stopScanning(barcode: String) {
        self.controller.stopScanning(barcode: barcode)
        if barcode.count > 0 {
            self.searchBar?.text = barcode
        }
    }
    
    func finishScanning(barcode: String) {
        self.controller.stopScanning(barcode: barcode)
        self.segmentControl.selectItemAtIndex(index: 1, withAnimation: false)
        self.barCodeScanComplete = true
       // self.segmentValueChanged(selectedIndex: 1)
        
        if barcode.count > 0 {
            self.searchBar?.text = barcode
        }
    }
    
    func hideVoiceProgressView (searchString : String) {
        self.stopVoiceSearchWithText(searchString: searchString)
    }
    
    func showVoiceSearchProgressView () {        
        let waveGif = UIImage.gif(name: "wave")
        self.voiceProgressImageView.image = waveGif
        self.voiceProgressView.backgroundColor = UIColor.black
        self.voiceProgressView.isHidden = false
        self.voiceSearchButton?.setImage(UIImage(named:"cansel_icon"), for: .normal)
    }
    
    func stopVoiceSearchWithText (searchString : String) {
        self.voiceProgressView.isHidden = true
        if searchString.count > 0 {
            self.searchBar?.text = searchString
            self.deselctAllTabs(dismissKeyboard : true)
           // self.presenter.searchBarSearchButtonClicked(self.searchBar!, dismissKeyboard: true)
        }
    }
    
    func getGroupIndexPathForCell(cell:UITableViewCell) -> IndexPath? {
        return (self.resultsTableView?.indexPath(for: cell))
    }
    
    func startActivity() {
        self.prepareLoadView()
    }
    
    func stopActivity() {
        self.stopActivityAnimation()
    }
    
    func showToast() {
        if self.itemAdded {
            self.controller.showToast(message: "Item Added",showingError: false, completion: nil)
            self.itemAdded = false
        }
    }
    
    
    func clearSearchBar () {
        self.searchBar?.text = ""
        self.searchBar?.resignFirstResponder()
    }
    
    func deselctAllTabs(dismissKeyboard:Bool){
        self.hideVoiceProgressView(searchString: "")
        self.currentSelectedIndex = .kSelectedNone
        self.segmentControl.deselectAll(dismissKeyboard : dismissKeyboard)

    }
    func showCameraSnap () {
        self.controller.showCameraSnapViewController()
    }
    
    func setPreviousSelectedSegment(segmentIndex : Int) {
        if !(segmentIndex == SelectedTab.kPhotoTab.rawValue || segmentIndex == SelectedTab.kVoiceSearchTab.rawValue) {
            self.currentSelectedIndex = SelectedTab.init(rawValue: segmentIndex)!
        }
    }
    
    func selectedPickerIndex() -> SelectedTab {
        return self.currentSelectedIndex
    }
    
    func startSoundHoundVoiceSearch() {
        self.controller.showSoundHoundVoiceSearch()
    }

}

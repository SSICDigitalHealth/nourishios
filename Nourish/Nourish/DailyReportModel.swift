//
//  DailyReportModel.swift
//  Nourish
//
//  Created by Vino (Vinodha) Sundaramoorthy on 1/26/17.
//  Copyright © 2017 SSIC. All rights reserved.
//

import UIKit

struct reportsModel {
    var date : Date
    var report : DailyReportModel?
}

struct weightLossMeter {
    var maxConsumedCal : Double = 0.0
    var maxBurnedCal : Double = 0.0
    var userConsumedCal : Double = 0.0
    var userBurnedCal : Double = 0.0
    var weeklyPlan : Double = 0.0
}

class DailyReportModel : NSObject {
    var steps : Double = 0.0
    var distance : Double = 0.0
    var activeHours : Double = 0.0
    var sleep : Double = 0.0
    var activityArray : [activityTime] = []
    var justRightNutrients : [nutrients]? = []
    var tooLittleNutrients : [nutrients]? = []
    var tooMuchNutrients : [nutrients]? = []
    var weightProgress : weightLossMeter?
    var isMetric : Bool = false
    var goal : UserGoal = .LooseWeight
}

//
//  UserMessage_to_Json.swift
//  Nourish
//
//  Created by Nova on 12/7/16.
//  Copyright © 2016 SSIC. All rights reserved.
//

import UIKit

class UserMessage_to_Json: NSObject {
    
    func transform(message : UserMessage) -> [String : Any] {
        var dict : [String : Any] = [:]
        dict["weight"] = message.weight
        //dict["stepsFor10Minutes"] = message.stepsFor10Minutes
        dict["calories"] = message.calories
        dict["height"] = message.height
        dict["dietrestriction"] = message.dietrestriction?.description
        dict["age"] = message.age
        dict["userid"] = message.userId
        dict["hoursSinceMidnight"] = message.hoursSinceMidnight
        dict["heartRate"] = message.heartRate
        dict["steps"] = message.steps
        dict["activity"] = message.activity?.description
        //dict["averageHeartRateFor10Minutes"] = message.averageHeartRateFor10Minutes
        dict["gender"] = message.gender
        return dict
    }
}

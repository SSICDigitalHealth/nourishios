//
//  WeightsStatsCellTableViewCell.swift
//  Nourish
//
//  Created by Vino (Vinodha) Sundaramoorthy on 1/9/17.
//  Copyright © 2017 SSIC. All rights reserved.
//

import UIKit

class WeightsStatsCellTableViewCell: UITableViewCell {
    @IBOutlet weak var weightLabel : UILabel!
    @IBOutlet weak var weightPeiodLabel : UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
    }

    func setUpStatsCell(model : weightStatsModel) {
        weightLabel.text = String(format:"%.0f",model.weight)
        weightPeiodLabel.text = model.weightPeriodRange
    }
    
}

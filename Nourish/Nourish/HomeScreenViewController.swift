//
//  HomeScreenViewController.swift
//  Nourish
//
//  Created by Andrey Zmushko on 9/8/17.
//  Copyright © 2017 SSIC. All rights reserved.
//

import UIKit
import RxSwift
import HealthKit

class HomeScreenViewController: BasePresentationViewController,RecognitionViewContollerDelegate,VoiceMealListDelegate {

    enum Actions {
        case profile
        case nutrition
        case lifestyle
        case microphone
        case camera
        case foodDiary
        
        static func actionBy(Tag tag: Int) -> Actions {
            switch tag {
            case 2001:
                return .microphone
            case 2002:
                return .camera
            case 2003:
                return .foodDiary
            default:
                return .profile
            }
        }
    }
    
    @IBOutlet weak var overNutrition: UIView!
    @IBOutlet weak var overLifestyle: UIView!
    
    @IBOutlet weak var navigationBar: UIView!
    
    @IBOutlet weak var nutritionProgress: NSLayoutConstraint!
    @IBOutlet weak var lifestyleProgress: NSLayoutConstraint!
    
    @IBOutlet weak var fruitsProgress: NSLayoutConstraint!
    @IBOutlet weak var fruitsImage: UIImageView!
    
    @IBOutlet weak var vegetableProgress: NSLayoutConstraint!
    @IBOutlet weak var vegetableImage: UIImageView!
    
    @IBOutlet weak var dairyProgress: NSLayoutConstraint!
    @IBOutlet weak var dairyImage: UIImageView!
    
    @IBOutlet weak var proteinProgress: NSLayoutConstraint!
    @IBOutlet weak var proteinImage: UIImageView!
    
    @IBOutlet weak var grainsProgress: NSLayoutConstraint!
    @IBOutlet weak var grainsImage: UIImageView!
    
    @IBOutlet weak var consumedImage: UIImageView!
    @IBOutlet weak var calburnedImage: UIImageView!
    
    
    @IBOutlet weak var heartView: UIView!
    @IBOutlet weak var diagramView: HomeScreenDiagramView!
    
    @IBOutlet var presenter: HomeScreenPresenter!
    
    @IBOutlet var nutritionGesture: UITapGestureRecognizer!
    @IBOutlet var lifestyleGesture: UITapGestureRecognizer!
    
    @IBOutlet weak var lifestyleView : UIView!
    @IBOutlet weak var lifestyleTitleLabel : UILabel!
    @IBOutlet weak var lifestyleProgressView : UIView!
    
    @IBOutlet var scoreLabel : UILabel!
    
    @IBOutlet weak var logoImageView : UIImageView!
    
    @IBOutlet weak var baseView: BaseView!
    @IBOutlet weak var profilePicButton : UIButton!
    
    
    @IBAction func tapAction(_ sender: Any) {
        switch sender {
        case is UIButton:
            self.presenter.actionFor(controller: self, action: Actions.actionBy(Tag: (sender as! UIButton).tag))
        default:
            switch (sender as! UITapGestureRecognizer) {
            case nutritionGesture:
                self.presenter.actionFor(controller: self, action: .nutrition)
            default:
                self.presenter.actionFor(controller: self, action: .lifestyle)
            }
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        self.lifestyleView.backgroundColor = NRColorUtility.lifestyleBackgroundColor()
        self.lifestyleTitleLabel.textColor = NRColorUtility.appBackgroundColor()
        self.lifestyleProgressView.backgroundColor = NRColorUtility.appBackgroundColor()
        self.overLifestyle.backgroundColor = NRColorUtility.overConsumedCaloriesDailyWeeklyColor()
        #if OPTIFASTVERSION
            self.logoImageView.image = #imageLiteral(resourceName: "optiLogoSmall")
        #else
            self.logoImageView.image = #imageLiteral(resourceName: "horizontal_logo")
        #endif
        
        
        self.setupNavigationBar()
        self.setupImages()
        NotificationCenter.default.addObserver(self, selector: #selector(foodDiaryHasBeenSynced), name: NSNotification.Name(rawValue: kOperationsDidFinised), object: nil)
    }

    func foodDiaryHasBeenSynced(){
        self.presenter.setupWith(controller: self)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.baseView.viewWillAppear(true)
        self.setupWith(model: nil)
        self.presenter.setupWith(controller: self)
        
        let _ = UserRepository.shared.getCurrentUserID().observeOn(MainScheduler.instance).subscribe(onNext : { [unowned self] userID in
                self.profilePicButton.setImage(NRImageUtility.getProfilePicture(isGlobalDrawer: true,userId:userID), for: .normal)
        })
    }
    
    
    func setupWith(model: HomeScreenViewModel?) {
        let totalProgressHeight: CGFloat = 100.0
        let totalProgressWidth: CGFloat = UIScreen.main.bounds.width
        func progressFrom(total: CGFloat, multiplier: Double) -> CGFloat {
            if multiplier > 1 {
                return CGFloat(-total) * (1 - 1 / CGFloat(multiplier))
            }
            return CGFloat(-total) * (1 - CGFloat(multiplier))
        }
        
        if let model = model {
            if model.consumed ?? 0 > 1 {
                self.overNutrition.isHidden = false
            } else {
                self.overNutrition.isHidden = true
            }
            
            if model.burned ?? 0 > 1 {
                self.overLifestyle.isHidden = false
            } else {
                self.overLifestyle.isHidden = true
            }
            self.scoreLabel.text = String(format : "%0.0f", model.score!)
            self.nutritionProgress.constant = progressFrom(total: totalProgressWidth, multiplier: model.consumed ?? 0)
            self.lifestyleProgress.constant = progressFrom(total: totalProgressWidth, multiplier: model.burned ?? 0)
            
            self.fruitsProgress.constant = progressFrom(total:totalProgressHeight, multiplier: model.fruits ?? 0)
            self.vegetableProgress.constant = progressFrom(total:totalProgressHeight, multiplier: model.vegetable ?? 0)
            self.dairyProgress.constant = progressFrom(total:totalProgressHeight, multiplier: model.dairy ?? 0)
            self.proteinProgress.constant = progressFrom(total:totalProgressHeight, multiplier: model.protein ?? 0)
            self.grainsProgress.constant = progressFrom(total:totalProgressHeight, multiplier: model.grains ?? 0)
            
            self.diagramView.activity = model.activity ?? 0
            self.diagramView.sleep = model.sleep ?? 0
            
            self.heartView.backgroundColor = {
                if let level = model.stress {
                    switch level {
                    case .Stressed:
                        return NRColorUtility.hexStringToUIColor(hex: "EC1C28")
                    case .Normal:
                        return NRColorUtility.hexStringToUIColor(hex: "70C397")
                    default:
                        break
                    }
                }
                return NRColorUtility.hexStringToUIColor(hex: "#4a4a4a")
            }()
            
            self.baseView.stopActivityAnimation()
        }
        else {
            self.nutritionProgress.constant = progressFrom(total: totalProgressWidth, multiplier: 1)
            self.lifestyleProgress.constant = progressFrom(total: totalProgressWidth, multiplier: 1)
            
            self.fruitsProgress.constant = progressFrom(total:totalProgressHeight, multiplier: 1)
            self.vegetableProgress.constant = progressFrom(total:totalProgressHeight, multiplier: 1)
            self.dairyProgress.constant = progressFrom(total:totalProgressHeight, multiplier: 1)
            self.proteinProgress.constant = progressFrom(total:totalProgressHeight, multiplier: 1)
            self.grainsProgress.constant = progressFrom(total:totalProgressHeight, multiplier: 1)
            
            self.diagramView.activity = 1
            self.diagramView.sleep = 1
        }
        
        self.diagramView.setNeedsDisplay()
    }
    
    private func setupImages() {
        self.fruitsImage.imageNamedWithTint(named: "hmbargraph_fruits", tintColor: NRColorUtility.hexStringToUIColor(hex: "#f16527"))
        self.vegetableImage.imageNamedWithTint(named: "hmbargraph_vegetable", tintColor: NRColorUtility.hexStringToUIColor(hex: "#70c397"))
        self.dairyImage.imageNamedWithTint(named: "hmbargraph_dairy", tintColor: NRColorUtility.hexStringToUIColor(hex: "#1172b9"))
        self.proteinImage.imageNamedWithTint(named: "hmbargraph_protein", tintColor: NRColorUtility.hexStringToUIColor(hex: "#c02859"))
        self.grainsImage.imageNamedWithTint(named: "hmbargraph_grains", tintColor: NRColorUtility.hexStringToUIColor(hex: "#f0c517"))
        self.consumedImage.imageNamedWithTint(named: "hmbargraph_consumed_icon", tintColor: NRColorUtility.hexStringToUIColor(hex: "#aadbbf"))
        var color = NRColorUtility.homeScreenIconsColor()
        #if OPTIFASTVERSION
            color = UIColor.white
        #endif
        self.calburnedImage.imageNamedWithTint(named: "hmbargraph_calburned_icon", tintColor: color)
    }
    
    private func setupNavigationBar() {
        self.navigationController?.setNavigationBarHidden(true, animated: false)
        
        let shadowPath = UIBezierPath(rect: CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: self.navigationBar.frame.height))
        self.navigationBar.layer.shadowColor = UIColor.darkGray.cgColor
        self.navigationBar.layer.shadowOffset = CGSize(width: 0.0, height: 1.0)
        self.navigationBar.layer.shadowOpacity = 1.0
        self.navigationBar.layer.shadowRadius = 2.0
        self.navigationBar.layer.masksToBounds =  false
        self.navigationBar.layer.shadowPath = shadowPath.cgPath
    }

    func recognitionViewControllerDidAddedFood() {
        self.dismiss(animated: true, completion: {
            self.showToast(message: "Item added", showingError: false, completion: nil)
        })
    }
    
    func recognitionViewControllerDidCancel() {
        self.dismiss(animated: true, completion: {
            
        })
    }
    
    func soundHoundViewControllerDidAddedFood() {
        self.dismiss(animated: true, completion: {
            self.showToast(message: "Item added", showingError: false, completion: nil)
        })
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

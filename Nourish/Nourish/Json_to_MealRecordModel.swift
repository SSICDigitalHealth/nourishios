//
//  Json_to_MealRecordModel.swift
//  Nourish
//
//  Created by Vino (Vinodha) Sundaramoorthy on 4/26/17.
//  Copyright © 2017 SSIC. All rights reserved.
//

import UIKit

class Json_to_MealRecordModel: NSObject {
    
    class func transform(dict:[String:Any]) -> [MealRecordModel] {
        var mealRecordArray : [MealRecordModel] = []
        if let amountArray = dict["foodAmount"] as? [Double] {
            let foodItemsArray : [String] = dict["foodItems"] != nil ? dict["foodItems"] as! [String] : []
            let foodUnitsArray : [String] = dict["foodUnits"] != nil ? dict["foodUnits"] as! [String] : []
            let gramsArray : [Double] = dict["grams"] != nil ? dict["grams"] as! [Double]: []
            let caloriesArray : [Double] = dict["kcal"] != nil ? dict["kcal"] as! [Double] : []
            let userSelectedNameArray : [String] = dict["userSelectName"] != nil ? dict["userSelectName"] as! [String] : []
            var noomIdArray : [String] = []
            
            if dict["noomId"] != nil {
                let array : [Any] = dict["noomId"] as! [Any]
                
                for i in 0...array.count - 1 {
                    if let noomId = array[i] as? String
                    {
                        noomIdArray.append(noomId)
                    }
                    else {
                        noomIdArray.append("")
                    }
                }
            }
            
            for i in 0...foodItemsArray.count - 1 {
                let mealRecord = MealRecordModel()
                mealRecord.foodTitle = userSelectedNameArray[i]
                mealRecord.servingAmmount = Double(amountArray[i])
                mealRecord.servingType = foodUnitsArray[i]
                mealRecord.isFromNoom = noomIdArray[i] == "" ? false : true
                mealRecord.calories = Double(caloriesArray[i])
                mealRecord.caloriesPerServing = mealRecord.calories! / mealRecord.servingAmmount!
                mealRecord.grams = Double(gramsArray[i])
                mealRecord.userFoodID = ""
                mealRecord.idString = foodItemsArray[i]
                mealRecordArray.append(mealRecord)
            }
            return mealRecordArray
        } else {
            return mealRecordArray
        }
    }

}

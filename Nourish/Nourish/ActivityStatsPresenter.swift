//
//  ActivityStatsPresenter.swift
//  Nourish
//
//  Created by Vino (Vinodha) Sundaramoorthy on 12/23/16.
//  Copyright © 2016 SSIC. All rights reserved.
//

import UIKit
import RxSwift

class ActivityStatsPresenter: BasePresenter  , NRSegmentedControlDelegate{
    
    var activitystatsView : ActivityStatsProtocol?
    var interactor = ActivityStatsInteractor()
    var activityPeriod : period = .today {
        didSet {
            self.viewWillAppear(false)
        }
    }
    
    let profileInteractor = UserProfileInteractor()
    let mapperObject = UserProfile_to_UserProfileModel()
    let mapperModel = UserProfileModel_to_UserProfile()
    var userModel : UserProfileModel = UserProfileModel()

    override func viewWillAppear(_ animated: Bool) {
        self.subscribtions.append(self.currentUser().observeOn(MainScheduler.instance).subscribe(onNext: { [unowned self] user in
            self.userModel = user
        }, onError: {error in
            LogUtility.logToFile("error is ", error)
        }, onCompleted: {
            self.activitystatsView?.setMetricsPreference(isMetric: (self.userModel.metricPreference?.isMetric)!)
        }, onDisposed: {}))
    
        self.subscribtions.append(self.interactor.getRealActivityForPeriod(period: activityPeriod).subscribe(onNext: { [unowned self] model in
        self.activitystatsView?.setActivityModel(model:model, period: self.activityPeriod)
        }, onError: {error in }, onCompleted: {}, onDisposed: {}))
    }
    
    func currentUser() -> Observable<UserProfileModel> {
        let object = self.profileInteractor.getRawUser().flatMap({ userProfile -> Observable<UserProfileModel> in
            self.userModel = self.mapperObject.transform(user: userProfile)
            return Observable.just(self.mapperObject.transform(user: userProfile))
        })
        return object
    }

    // MARK: NRSegmentedControlDelegate Implementation
    func segmentedControlDidPressedItemAtIndex(segmentedControl: NRSegmentedControl, index: Int) {
        self.activitystatsView?.startActivity()
        activityPeriod = period(rawValue: index)!
    }
}

//
//  NestleFoodComponentsCacheDataStore.swift
//  Nourish
//
//  Created by Gena Mironchyk on 9/20/17.
//  Copyright © 2017 SSIC. All rights reserved.
//

import UIKit
import RealmSwift
class NestleFoodComponentsCacheDataStore: NSObject {
    func storeTo(array : [[String : Any]]) {
        let realm = try! Realm()
        let objs = realm.objects(NestleFoodComponentsCache.self)
        if objs.count > 0 {
            try! realm.write {
                realm.delete(objs)
            }
        }
        
        let cache = NestleFoodComponentsCache()
        cache.cachedData = self.transformToData(cache: array)
        
        try? realm.write {
            realm.add(cache)
        }
        
    }
    
    func getFoodComponentsCache() -> [[String : Any]]? {
        let realm = try! Realm()
        let objects = realm.objects(NestleFoodComponentsCache.self)
        if objects.count > 0 {
            return self.transformToArray(cachedData: objects.first!.cachedData)
        } else {
            return nil
        }
    }
    
    private func transformToArray(cachedData : Data) -> [[String : Any]]? {
        return try! JSONSerialization.jsonObject(with: cachedData, options: .mutableLeaves) as? [[String : Any]]
    }
    
    private func transformToData(cache : [[String : Any]]) -> Data{
        return try! JSONSerialization.data(withJSONObject: cache, options: .prettyPrinted)
    }
    
}

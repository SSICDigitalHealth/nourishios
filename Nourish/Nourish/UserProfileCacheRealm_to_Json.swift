//
//  UserProfileCacheRealm_to_Json.swift
//  Nourish
//
//  Created by Nova on 1/17/17.
//  Copyright © 2017 SSIC. All rights reserved.
//

import UIKit

class UserProfileCacheRealm_to_Json: NSObject {
    func transform(userCache : UserProfileCacheRealm) -> [String:Any] {
        var dictionary : Dictionary<String, Any> = [:]
        
        dictionary["activity"] = userCache.activityLevel?.description
        dictionary["age"] = userCache.age.value
        dictionary["dietrestriction"] = userCache.dietaryPreference?.description
        dictionary["gender"] = userCache.biologicalSex?.description
        dictionary["height"] = userCache.height.value
        
        if userCache.metricPreference != nil {
            dictionary["isMetric"] = MetricPreference.enumFromString(string: userCache.metricPreference!)?.isMetric
        }
        
        dictionary["weight"] = userCache.weight.value
        dictionary["goalPrefsActivity"] = userCache.goalPrefsActivity.value
        dictionary["goalPrefsFood"] = userCache.goalPrefsFood.value
        dictionary["goalPrefsLifestyle"] = userCache.goalPrefsLifestyle.value
        dictionary["goal"] = userCache.userGoal?.description
        dictionary["startWeight"] = userCache.startWeightTest.value
        dictionary["goalValue"] = userCache.goalTest.value
        
        if  userCache.weightLossStartDate != nil {
            dictionary["weightLossStartDate"] = Int64((userCache.weightLossStartDate?.timeIntervalSince1970)! * 1000)//milisecs
        }
        
        if userCache.listOfOptifastOcasions.count > 0 {
            var arrayOfStringOcasions = [String]()
            arrayOfStringOcasions = userCache.listOfOptifastOcasions.map({return Ocasion.stringRepresentation(servingType: Ocasion(rawValue : $0.value)!).lowercased()})
            dictionary["optifast_occasions"] = arrayOfStringOcasions
        }
        
        if userCache.listOfOptifastFoodIds.count > 0 {
            var optifastFoodIDs = [String]()
            optifastFoodIDs = userCache.listOfOptifastFoodIds.map({return $0.stringValue})
            dictionary["optifast_products"] = optifastFoodIDs
        }
        
        dictionary["maintain_weight"] = userCache.weightToMaintain.value
        dictionary["email"] = userCache.email
        
        return dictionary
    }
}

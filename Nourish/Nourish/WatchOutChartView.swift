//
//  WatchOutChartView.swift
//  Nourish
//
//  Created by Andrey Zmushko on 7/27/17.
//  Copyright © 2017 SSIC. All rights reserved.
//

import UIKit

class WatchOutChartView: UIView {

    public typealias Data = (consumed: Double, target: Double)
    var data: Data?
    
    func setupWith(Data data: Data) {
        self.data = data
        self.setNeedsDisplay()
    }

    override func draw(_ rect: CGRect) {
        let center = CGPoint(x: self.bounds.width / 2, y: self.bounds.height / 2)
        let radius: CGFloat = self.bounds.width / 2
        
        if data != nil {
            let bgColor: UIColor = {
                if data!.consumed < data!.target {
                    return NRColorUtility.hexStringToUIColor(hex: "70C397").withAlphaComponent(0.2)
                }
                else {
                    return NRColorUtility.hexStringToUIColor(hex: "EC1C28").withAlphaComponent(0.4)
                }
            }()

            let chartColor: UIColor = {
                if data!.consumed < data!.target {
                    return NRColorUtility.hexStringToUIColor(hex: "70C397")
                }
                else {
                    return NRColorUtility.hexStringToUIColor(hex: "EC1C28")
                }
            }()

            let angle:CGFloat = {
                if data!.target == 0 {
                    return 0
                }
                let val = data!.consumed / data!.target
                let reminder = val - Double(val.rounded(.towardZero))
                return 360 * CGFloat(reminder) - 90
            }()
            
            self.drawSector(fillColor: bgColor,
                                        center: center,
                                        radius: radius,
                                        startAngle: 0.0,
                                        endAngle: 360.0,
                                        clockwise: true)
            
            self.drawSector(fillColor: chartColor,
                                        center: center,
                                        radius: radius,
                                        startAngle: -90.0,
                                        endAngle: angle,
                                        clockwise: true)
        }
        else {
            self.drawSector(fillColor: NRColorUtility.hexStringToUIColor(hex: "70C397").withAlphaComponent(0.2),
                                        center: center,
                                        radius: radius,
                                        startAngle: 0.0,
                                        endAngle: 360.0,
                                        clockwise: true)
        }
    }

}

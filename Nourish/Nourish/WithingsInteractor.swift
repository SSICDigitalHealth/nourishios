//
//  WithingsInteractor.swift
//  Nourish
//
//  Created by Gena Mironchyk on 4/18/17.
//  Copyright © 2017 SSIC. All rights reserved.
//

import UIKit
import RxSwift
let kLastSyncWithingsDateKey = "Last sync withings"

class WithingsInteractor: NSObject {
    let devicesRepo = UserDevicesRepository.shared
    let messagesRepository = ArtikCloudMessagesRepository()
    let userRepository = UserRepository.shared
    let chatBoxRepository = ChatBoxRepository()
    
    func userDevices() -> Observable<[UserDevice]> {
        return self.devicesRepo.userDevices()
    }
    
    func pairedDevices() -> Observable<[UserDevice]> {
        return self.userDevices().flatMap({devices -> Observable<[UserDevice]> in
            //let pairedDevices = devices.filter({$0.dtidString == WITHINGS_DTID})
            return Observable.just(devices)
        })
    }
    
    func unathorizeWithingsDevice() -> Observable<Bool> {
        return self.pairedDevices().flatMap{devices -> Observable<Bool> in
            let filteredDevices = devices.filter({$0.dtidString == WITHINGS_DTID})
            if let device = filteredDevices.first {
                if  !device.isAuthorized {
                    return Observable.just(true)
                } else {
                    return self.devicesRepo.unauthorizeDevice(device: device)
                }
            }
            return Observable.just(true)
        }
    }
    
    
    func pairedWithigsDevices() -> Observable<[UserDevice]> {
        return self.userDevices().flatMap({devices -> Observable<[UserDevice]> in
            let pairedDevices = devices.filter({$0.dtidString == WITHINGS_DTID})
            return Observable.just(pairedDevices)
        })
    }
    
    func createWithingsDevice() -> Observable<[UserDevice]> {
        return self.devicesRepo.createWithingsDevice()
    }
    
    func authurizationURLForWithingsDevice(withingsDevice : UserDevice) -> Observable<URL> {
        return self.devicesRepo.authorizationURLForDevice(device:withingsDevice)
    }
    
    func syncWithingsMessages() -> Observable<Bool> {
        var startDate = Calendar.current.startOfDay(for: Date())
        if defaults.object(forKey: kLastSyncWithingsDateKey) != nil {
            startDate = defaults.object(forKey: kLastSyncWithingsDateKey) as! Date
        }
        return self.pairedWithigsDevices().flatMap({devices -> Observable<Bool> in
            if devices.count > 0 {
                let withingsDevice = devices.first
                if withingsDevice?.isAuthorized == true {
                    return self.messagesRepository.getWithingsMessages(device: withingsDevice!, startDate: startDate, endDate: Date()).flatMap({withingsMessages -> Observable<Bool> in
                        if withingsMessages.count > 0{
                            for message in withingsMessages {
                                if message.weight > 0.0 {
                                    ActivityUtility.storeWeight(weight: message.weight, date: message.date!)
                                }
                            }
                            let lastMessage = withingsMessages.last
                            defaults.set(lastMessage!.dateAddedToArtik, forKey: kLastSyncWithingsDateKey)
                            return self.userRepository.getCurrentUser(policy: .DefaultPolicy).flatMap({ userProfile -> Observable<Bool> in
                                if lastMessage!.weight > 0.0 && Calendar.current.startOfDay(for: (lastMessage?.date)!) == Calendar.current.startOfDay(for: Date()) {
                                        userProfile.weight = lastMessage!.weight
                                        self.userRepository.storeCurrentUserInBackground(user: userProfile, writeToHealth: false)
                                        DispatchQueue.main.async {
                                            let message = ChatDataBaseItem()
                                            message.chatBoxType = .typeWeightRecord
                                            message.content = String(format: "We recorded a new weight of %0.2f %@", self.convertWeight(newWeight: (lastMessage?.weight)!, userModel: userProfile), (userProfile.metricPreference?.descriptionForChat)!)
                                            message.isFromUser = false
                                            message.userID = userProfile.userID!
                                            self.chatBoxRepository.storeRecordsArray(array: [message])
                                            NotificationCenter.default.post(name: NSNotification.Name("Withings weight synchronized"), object: nil)
                                            EventLogger.logLogsWeightWithWithings()
                                        }
                                        return Observable.just(true)
                                } else {
                                    return Observable.just(true)
                                }
                            })

                        } else {
                            return Observable.just(true)
                        }
                    })
                } else {
                    return Observable.just(true)
                }
            } else {
                return Observable.just(true)
            }
        })
    }
    
    
    private func convertWeight(newWeight : Double, userModel : UserProfile) -> Double {
        let weight : Double =  userModel.metricPreference == MetricPreference.Metric ? newWeight :  NRConversionUtility.kiloToPounds(valueInKilo: newWeight)
        return weight
    }
}

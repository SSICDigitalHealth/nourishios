//
//  ResetMealPlannerPresenter.swift
//  Nourish
//
//  Created by Vlad Birukov on 23.08.17.
//  Copyright © 2017 SSIC. All rights reserved.
//

import Foundation
import RxSwift


class ResetMealPlannerPresenter: BasePresenter {
    var resetMealPlannerView: ResetMealPlannerView?
    var resetMealPlannerViewController: ResetMealPlannerViewController?
    private let deleteMealPlanInteractor = DeleteMealPlanInteractor()
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    @IBAction func cancel(_ sender: Any) {
        if self.resetMealPlannerViewController != nil {
            self.resetMealPlannerViewController?.dismiss(animated: false, completion: nil)
            NavigationUtility.showTabBar(animated: false)
        }
    }
    
    @IBAction func resetPlan(_ sender: Any) {
       let _ = self.deleteMealPlanInteractor.execute().observeOn(MainScheduler.instance).subscribe(onNext: { deleted in }, onError: {error in}, onCompleted: {
            self.resetMealPlannerViewController?.dismiss(animated: false, completion: {
                if self.resetMealPlannerViewController?.delegate != nil {
                    self.resetMealPlannerViewController?.delegate?.resetMealPlanControllerDidReset()
                }
            })
        }, onDisposed: {})
        
    }
}

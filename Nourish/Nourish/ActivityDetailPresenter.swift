//
//  ActivityDetailPresenter.swift
//  Nourish
//
//  Created by Vlad Birukov on 24.08.17.
//  Copyright © 2017 SSIC. All rights reserved.
//

import Foundation
import RxSwift


enum ActivityCells {
    case stayActive
    case caloricProgress
    case caloricProgressChart
    case caloricProgressNotification
    case caloriesBurned
    case caloricBurned
    case caloricConsumed
    case caloriesBurnedChart
    case steps
    case distance
    case sleep
    case exercise
    case stress
    case weight
    case weightChart
    case defaultCell
    
    var attributes: (nibName: String, presenter: ActivityBaseCellPresenter) {
        switch self {
        case .stayActive:
            return ("StayActiveTableViewCell", ActivityStayActiveCellPresenter())
        case .caloricProgress:
            return ("CaloricProgressTableViewCell", ActivityCaloricProgressCellPresenter())
        case .caloricProgressChart:
            return ("ActivityCaloricProgressChartCell", ActivityCaloricProgressChartPresenter())
        case .caloricProgressNotification:
            return ("ActivityBaseTableViewCell", ActivityBaseCellPresenter())
        case .caloriesBurned:
            return ("CaloricBurnedTableViewCell", ActivityCaloriesBurnedCellPresenter())
        case .caloricBurned:
            return ("ActivityBaseTableViewCell", ActivityCaloricBurnedCellPresenter())
        case .caloricConsumed:
            return ("ActivityBaseTableViewCell", ActivityCaloricConsumedCellPresenter())
        case .caloriesBurnedChart:
            return ("ActivityBaseTableViewCell", ActivityBaseCellPresenter())
        case .steps:
            return ("ActivityBaseTableViewCell", ActivityStepsCellPresenter())
        case .distance:
            return ("ActivityBaseTableViewCell", ActivityDistanceCellPresenter())
        case .sleep:
            return ("ActivityBaseTableViewCell", ActivitySleepCellPresenter())
        case .exercise:
            return ("ActivityBaseTableViewCell", ActivityExerciseCellPresenter())
        case .stress:
            return ("ActivityBaseTableViewCell", ActivityStressCellPresenter())
        case .weight:
            return ("ActivityBaseTableViewCell", ActivityWeightCellPresenter())
        case .weightChart:
            return ("ActivityWeightChartCell", ActivityWeightChartCellPresenter())
        case .defaultCell:
            return ("ActivityBaseTableViewCell", ActivityBaseCellPresenter())
        }
    }
}

protocol ActivityDetailDelegate {
    func cellOrder() -> [ActivityCells]
}

func startOfDay(_ date: Date) -> Date {
    let calendar = Calendar.current
    return calendar.startOfDay(for: date)
}

func endOfDay(_ date: Date) -> Date {
    var components = DateComponents()
    components.day = 1
    components.second = -1
    return Calendar.current.date(byAdding: components, to: startOfDay(date)) ?? date
}

class ActivityDetailPresenter: BasePresenter, UITableViewDataSource, UITableViewDelegate, ActivityBaseCellDelegate {
    var activityDetailView: ActivityDetailProtocol?
    var activityViewController: ActivityViewController?
    var activityDetailDelegate: ActivityDetailDelegate?
    let interactor = UserProfileInteractor()
    let mapperToModel = UserProfile_to_UserProfileModel()
    
    let calendar = Calendar.current
    var cells = [UITableViewCell]()
    var userId: String?
    
    var epoch = (startDate: startOfDay(Date()), endDate: endOfDay(Date()))
    
    override func viewWillAppear(_ animated: Bool) {
        if let delegate = self.activityDetailDelegate {
            self.makeCells(inOrder: delegate.cellOrder())
        }
        if let detailView = self.activityDetailView {
            detailView.reloadData()
        }
        getCurrentUser()

        super.viewWillAppear(animated)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
    }
    
    func getCurrentInformationFor(startDate: Date, endDate: Date) {
        
        self.epoch = (startDate: startOfDay(startDate), endDate: endOfDay(endDate))
        if let delegate = self.activityDetailDelegate {
            self.makeCells(inOrder: delegate.cellOrder())
        }
        if let detailView = self.activityDetailView {
            detailView.reloadData()
        }
        self.getCurrentUser()
    }
    
    func fetchEpoch() -> (startDate: Date, endDate: Date) {
        return self.epoch
    }
    
    func transferUserId(userId: String) {
        self.userId = userId
    }
    
    func actionTap(sender: ActivityBaseCellPresenter) {
        switch sender {
        case is ActivityCaloricProgressCellPresenter:
            let activityCaloricProgress = sender as! ActivityCaloricProgressCellPresenter
            self.showBudgetInfo(activity: activityCaloricProgress.activityLevel, budgetCal: activityCaloricProgress.maxconsumedCal, budgetActivity: activityCaloricProgress.maxBurnedCal)
        default:
            print("undefined sender")
        }
    }
    
    func reloadRowAt(indexRow: Int) {
        if let view = activityDetailView {
            let indexPath = IndexPath(row: indexRow + 1, section: 0)
            view.reloadCell(indexPath: indexPath)
        }
    }
    
    private func makeCells(inOrder: [ActivityCells]) {
        self.cells.removeAll()
        let datePickerCell = Bundle.main.loadNibNamed("DatePickerTableViewCell", owner: nil, options: nil)?.first as! DatePickerTableViewCell
        if let picker = datePickerCell.datePickerView {
            if let controller = self.activityViewController {
                controller.activityNavigationPresenter.setupDatePicker(datePicker: picker)
            }
        }
        self.cells.append(datePickerCell)
        for (index, element) in inOrder.enumerated() {
            let cell = Bundle.main.loadNibNamed(element.attributes.nibName, owner: nil, options: nil)?.first as! ActivityBaseCell
            let presenter = element.attributes.presenter
            presenter.delegate = self
            presenter.indexRow = index
            cell.setupWith(presenter: presenter)
            self.cells.append(cell)
        }
    }
    
    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if cells.count == 0 {
            if let delegate = self.activityDetailDelegate {
                self.makeCells(inOrder: delegate.cellOrder())
            }
        }
        return self.cells.count
    }
    
    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        return self.cells[indexPath.row]
    }
    
    
    public func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        var isStress: Bool {
            if let delegate = self.activityDetailDelegate {
                if indexPath.row > 0 && delegate.cellOrder()[indexPath.row - 1] == .stress {
                    return true
                }
            }
            return false
        }
        
        var isWeight: Bool {
            if let delegate = self.activityDetailDelegate {
                if indexPath.row > 0 && delegate.cellOrder()[indexPath.row - 1] == .weight {
                    return true
                }
            }
            return false
        }

        
        var isDailyToday: Bool {
            return calendar.compare(self.epoch.startDate, to: self.epoch.endDate, toGranularity: .day) == .orderedSame &&
                self.calendar.isDateInToday(self.epoch.startDate)
        }
        
        if isDailyToday && isStress {
            if let controller = self.activityViewController {
                if let navigator = controller.navigationController {
                    navigator.pushViewController(NewStressViewController(), animated: true)
                    NavigationUtility.hideTabBar(animated: false)
                }
            }
        }
        
        if isDailyToday && isWeight {
            if userId != nil {
                self.backToChat(userId: userId!)
            }
        }
        
        tableView.deselectRow(at: indexPath, animated: true)
    }
    
    private func showBudgetInfo(activity:String, budgetCal:Double, budgetActivity:Double) {
        let viewController = WeightLossMeterInfoViewController(nibName: "WeightLossMeterInfoViewController", bundle: nil)
        
            viewController.activityBudget = budgetActivity
            viewController.activityLevel = activity
            viewController.consumedBudget = budgetCal
            viewController.providesPresentationContextTransitionStyle = true;
            viewController.definesPresentationContext = true;
            viewController.modalPresentationStyle=UIModalPresentationStyle.overCurrentContext
            viewController.view.backgroundColor = UIColor.black.withAlphaComponent(0.3)
        
        if let controller = self.activityViewController {
                controller.present(viewController, animated: true, completion: nil)
        }
    }
    
    private func getCurrentUser() {
        self.subscribtions.append(self.interactor.getCurrentUserWith(fetchingPolicy: .DefaultPolicy).observeOn(MainScheduler.instance).subscribe(onNext: { [weak self] userProfile in
            if let instance = self {
                instance.listShowCell(userProfile: instance.mapperToModel.transform(user: userProfile))
            }
        }, onError: { [weak self] error in
            if let instance = self {
                instance.activityDetailView?.parseError(error : error, completion: nil)
            }
        }, onCompleted: {
        }, onDisposed: {
        }))
        
    }
    
    private func  listShowCell(userProfile: UserProfileModel) {
        if userProfile.userGoal == .EatBetter && (self.daysBetweenDates(startDate: self.epoch.startDate, endDate: self.epoch.endDate) == 0){
            // Remove Weight cell - is last cell in array
            let count = self.cells.count
            if count > 1 {
                self.cells.remove(at: count - 1)
                if let view = self.activityDetailView {
                    view.reloadData()
                }
            }
        }
    }
    
    private func backToChat (userId: String) {
        let chatInteractor = ChatBoxInterractor()
        let logWeightMessage = ChatDataBaseItem()
        
        logWeightMessage.isFromUser = false
        logWeightMessage.chatBoxType = .typeLogWeight
        logWeightMessage.content = "Please update your weight today."
        logWeightMessage.date = Date()
        logWeightMessage.userID = userId
        
        chatInteractor.storeRecordsArray(array: [logWeightMessage])
        NavigationUtility.showChatView()
    }
    
    private func daysBetweenDates(startDate: Date, endDate: Date) -> Int
    {
        let calendar = Calendar.current
        let components = calendar.dateComponents([.day], from: startDate, to: endDate)
        return components.day!
    }
}

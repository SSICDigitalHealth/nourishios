//
//  NutritionalBalancePresenter.swift
//  Nourish
//
//  Created by Vlad Birukov on 07.06.17.
//  Copyright © 2017 SSIC. All rights reserved.
//

import Foundation
import RxSwift


class NutritionalBalancePresenter: BasePresenter {
    
    let objectToModelMapper = NutritionalBalanceModel_to_NutritionalBalanceViewModel()
    let interactor = NutritionalBalanceInteractor()
    var userNutritionBalanceView: NutritionalBalanceProtocol!
    var date: (Date, Date)?
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.getNutritionalBalance()
    }
    
    private func getNutritionalBalance() {
        if self.userNutritionBalanceView.config() != nil {
            self.date = self.userNutritionBalanceView.config()?.getDate()
        }
        
        if let view = self.userNutritionBalanceView {
            if self.date != nil {
                let proxyDate = userNutritionBalanceView?.config()?.date
                
                self.subscribtions.append(self.interactor.execute(startDate: (self.date?.0)!, endDate: (self.date?.1)!).observeOn(MainScheduler.instance).subscribe(onNext: { [weak self] nutritionModel in
                    if let instance = self {
                        if proxyDate == instance.userNutritionBalanceView?.config()?.date {
                            view.setupWith(Model: instance.objectToModelMapper.transform(model: nutritionModel))
                        }
                        view.stopActivityAnimation()
                    }
                }, onError: {error in
                    view.stopActivityAnimation()
                    view.parseError(error: error, completion: nil)
                }, onCompleted: {
                }, onDisposed: {
                    view.stopActivityAnimation()
                }))
            }
        }
    }
}

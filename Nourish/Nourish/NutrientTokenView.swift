//
//  NutrientTokenView.swift
//  Nourish
//
//  Created by Vino (Vinodha) Sundaramoorthy on 12/16/16.
//  Copyright © 2016 SSIC. All rights reserved.
//

import UIKit

let kTokenWidthPadding : CGFloat = 32
let kTokenSpacePadding : CGFloat = 10
let kTokenHeight : CGFloat = 32
let kScrollViewPadding : CGFloat = 15
let kScrollHeightPadding : CGFloat = 15
let kTokenLinePadding : CGFloat = 10
let kNumberOfTokensInPage : Int = 9
let kSourceViewPadding : CGFloat = 30

protocol NutrientTokenViewDelegate {
    func setSelectedNutrient(nutrient : nutrients)
}

class NutrientTokenView: BaseView , NutrientTokenProtocol , UIScrollViewDelegate  {
    var nutrient : UILabel!
    var nutrientPercentage : UILabel!
    var nutrientDetailsView : UIView!
    var nutrientSource: UITextView!
    var progressView : UIProgressView!
    var pageControl : UIPageControl!
    var nutrientsScrollView : UIScrollView!
    var scrollBgView : UIView!
    var bgView : UIView!
    var sourceLabel : UILabel!
    var sourceRecomm : UITextView!
    var delegate : NutrientTokenViewDelegate?
    var nutrientRecommendation: UITextView!
    var recommendationLabel : UILabel!
    var outlineView : UIView!
    var period : period!
    var needReloaded = false

    var selectedNutrient : nutrients? = nil {
        didSet {
            self.setupNurtientDetails()
        }
    }
    var nutrientList : [nutrients]?
    var highlightedButton : UIButton!
    
    // MARK: NutrientTokenProtocol Implementation
    func setUpNutrient(tokens: [nutrients], period:period) {
        self.period = period
        self.nutrientList = tokens
        self.needReloaded = true
        self.setNeedsDisplay()
    }
    
    // MARK: View Setup Methods
    override func draw(_ rect: CGRect) {
        if needReloaded {
            self.backgroundColor = NRColorUtility.nourishHomeColor()
            if scrollBgView == nil {
                scrollBgView = UIView(frame:  CGRect(x: 0, y: 0, width: self.bounds.width, height: 375))
                scrollBgView.backgroundColor = NRColorUtility.progressLabelColor()
                scrollBgView.addShadow()
                self.addSubview(scrollBgView)
            }
            self.setupScrollView()
        }
    }

    func setupScrollView () {
        var page = ceil(Double((nutrientList?.count)! / 9))
        page = page == 0 ? page : page + 1
        
        if nutrientsScrollView == nil {
            nutrientsScrollView = UIScrollView(frame: CGRect(x: 0, y: 0, width: self.bounds.width, height: 150))
            nutrientsScrollView.backgroundColor = NRColorUtility.progressLabelColor()
            nutrientsScrollView.alwaysBounceHorizontal = true
            nutrientsScrollView.showsHorizontalScrollIndicator = false
            nutrientsScrollView.delegate = self
            nutrientsScrollView.isPagingEnabled = true
            nutrientsScrollView.contentSize = CGSize(width: nutrientsScrollView.frame.size.width * CGFloat(page), height: nutrientsScrollView.frame.size.height)
            self.scrollBgView.addSubview(nutrientsScrollView)
        }
        self.setupNutrientTokens()
        self.setupPageControl()
        if (nutrientList?.count)! > 0 {
            selectedNutrient = nutrientList?[0]
        }
        
    }
    
    func setupPageControl() {
        let frame = CGRect(x: 70, y: nutrientsScrollView.bounds.size.height, width:200, height: 25)
        let page = ceil(Double((nutrientList?.count)! / 9))

        if pageControl == nil {
            pageControl = UIPageControl.init(frame: frame)
            pageControl.backgroundColor = UIColor.clear
            pageControl.currentPageIndicatorTintColor = UIColor.white
            pageControl.tintColor = UIColor.white.withAlphaComponent(0.3)
            pageControl.transform = CGAffineTransform(scaleX: 1.5, y: 1.5)
            pageControl.currentPage = 0
            pageControl.numberOfPages = Int(page == 0 ? page : page + 1)
            pageControl.addTarget(self, action: #selector(self.changePage(sender:)), for: .valueChanged)
            pageControl.addTarget(self, action: #selector(self.changePage(sender:)), for: .touchUpInside)
            scrollBgView.addSubview(pageControl)
        } else {
            pageControl.currentPage = 0
            pageControl.numberOfPages = Int(page == 0 ? page : page + 1)
            self.changePage(sender: pageControl)
        }
        
    }
    
    func calculateProgress(value : Double) -> Float {
        //Calculate percentage
        
        let max = selectedNutrient?.max ?? 0.0 > 0.0 ? selectedNutrient?.max : selectedNutrient?.min
        let progValue = value / max!
        return Float(progValue)
    }
    
    func setupNurtientDetails() {
        //Nutrient view 
        var height : CGFloat = 0.0
        let frame = CGRect(x: 0, y: pageControl.bounds.size.height + nutrientsScrollView.bounds.size.height, width: UIScreen.main.bounds.size.width - 30, height: 200)
        if nutrientDetailsView == nil {
            nutrientDetailsView = UIView.init(frame: frame)
            nutrientDetailsView.backgroundColor = UIColor.white
        }
        //title
        
        if nutrient == nil {
            nutrient = UILabel.init(frame:  CGRect(x: nutrientDetailsView.bounds.width / 2, y: 10, width: 200, height: 25))
            nutrient.center = CGPoint(x: nutrientDetailsView.bounds.width / 2, y: 20)
            nutrient.text = selectedNutrient?.name
            nutrient.font = UIFont.systemFont(ofSize: 17, weight: 0.1)
            nutrient.textColor = UIColor.black
            nutrient.textAlignment = .center
            nutrientDetailsView.addSubview(nutrient)
        } else {
            nutrient.text = selectedNutrient?.name
        }
        
        height = nutrient.bounds.height
        
        if progressView == nil {
            progressView = UIProgressView(progressViewStyle: .default)
            progressView.progressTintColor = selectedNutrient?.progressColor()
            progressView.trackTintColor = NRColorUtility.defaultHealthProgressColor()
            progressView.progress = self.calculateProgress(value: (selectedNutrient?.value)!)
            progressView.center = CGPoint(x: nutrientDetailsView.bounds.width / 2, y: 50)
            progressView.transform = CGAffineTransform(scaleX: 1.75, y: 5)
            progressView.layer.cornerRadius = progressView.bounds.height
            progressView.layer.masksToBounds = true
            progressView.clipsToBounds = true
            nutrientDetailsView.addSubview(progressView)
        } else {
            progressView.progressTintColor = selectedNutrient?.progressColor()
            progressView.progress = self.calculateProgress(value: (selectedNutrient?.value)!)
        }
        
        height = height + progressView.bounds.height
        
        let nutAmount = String(format:"%0.f %@/%0.f %@",(selectedNutrient?.value)!,(selectedNutrient?.unit)!,(selectedNutrient?.max)!,(selectedNutrient?.unit)!)
       
        if nutrientPercentage == nil {
            nutrientPercentage = UILabel.init(frame:  CGRect(x: nutrientDetailsView.bounds.width / 2, y:progressView.center.y + 25 , width: self.bounds.size.width, height: 25))
            nutrientPercentage.text = nutAmount
            nutrientPercentage.textColor = UIColor.black.withAlphaComponent(0.54)
            nutrientPercentage.textAlignment = .center
            nutrientPercentage.font = UIFont.systemFont(ofSize: 15)
            nutrientPercentage.center = CGPoint(x: nutrientDetailsView.bounds.width / 2, y: progressView.center.y + 25)
            nutrientDetailsView.addSubview(nutrientPercentage)
        } else {
            nutrientPercentage.text = nutAmount
        }
        
        height = height + nutrientPercentage.bounds.height + 25
        
        //Source 
        let descSize = selectedNutrient?.textDescription.sizeOfBoundingRect(width: self.bounds.width - 30, height: 999, font: UIFont.systemFont(ofSize: 13))
        
        height = height + (descSize?.height)! + kSourceViewPadding
        let nutrientSourceFrame = CGRect(x: 15, y:nutrientPercentage.center.y + 15 , width: nutrientDetailsView.bounds.size.width - 15 ,height: (descSize?.height)! + 10)
        if nutrientSource == nil {
            nutrientSource = UITextView(frame: nutrientSourceFrame)
            nutrientSource.textColor = NRColorUtility.progressLabelColor()
            nutrientSource.font = UIFont.systemFont(ofSize: 13)
            nutrientSource.text = selectedNutrient?.textDescription
            nutrientSource.textAlignment = .left
            nutrientSource.isEditable = false
            nutrientSource.isSelectable = false
            nutrientSource.backgroundColor = UIColor.clear

            nutrientDetailsView.frame = CGRect(x: frame.origin.x, y: frame.origin.y, width: frame.width, height: height)
            nutrientDetailsView.layer.shadowColor = UIColor.lightGray.cgColor
            nutrientDetailsView.layer.shadowOpacity = 1
            nutrientDetailsView.layer.shadowOffset = CGSize.zero
            nutrientDetailsView.layer.shadowRadius = 5
            nutrientDetailsView.addSubview(nutrientSource)

            let scrollHeight = self.nutrientsScrollView.frame.height + self.nutrientDetailsView.frame.height +  self.pageControl.frame.height
            scrollBgView.frame = CGRect(x: 0, y: 0, width: self.bounds.width, height:scrollHeight - 14)
            scrollBgView.addSubview(nutrientDetailsView)
        } else {
            nutrientSource.text = selectedNutrient?.textDescription
            nutrientSource.backgroundColor = UIColor.clear
            nutrientSource.frame = CGRect(x: nutrientSourceFrame.origin.x, y: nutrientSourceFrame.origin.y, width: nutrientSourceFrame.width, height: height)
            nutrientDetailsView.frame = CGRect(x: frame.origin.x, y: frame.origin.y, width: frame.width, height: height)
            let scrollHeight = self.nutrientsScrollView.frame.height + self.nutrientDetailsView.frame.height +  self.pageControl.frame.height

            scrollBgView.frame = CGRect(x: 0, y: 0, width: self.bounds.width, height: scrollHeight - 14)
        }
        
        self.setUpNutrientSourceView()
    }

    func setupNutrientTokens() {
        let maxAllowedWidth = self.bounds.width - (2 * kScrollViewPadding)
        var buttonArray : [UIButton] = []
       
        if ((nutrientList?.count)! > 0) {
            let initialXPosition = kScrollViewPadding
            let initialYPosition = kTokenLinePadding
            var xPosition : CGFloat = 0
            var yPosition : CGFloat = CGFloat(initialYPosition)
            var position = 1
            let startIndex = 0
            let endIndex = (nutrientList?.count)! - 1
            let scrollViewWidth = nutrientsScrollView.bounds.width
            var column = 1
        
            for i in startIndex...endIndex {
                let page = ceil(Double(i / 9))

                let data = nutrientList?[i]
                let text = data?.name
                let textFont = UIFont.systemFont(ofSize: 15)
                let stringWidth = text?.sizeOfBoundingRect(width: maxLabelSize.width, height: maxLabelSize.height, font: textFont).width

                //New row , new page , change y to inital position and x to next page
                if position % 4 == 0 && i % 9 == 0 {
                    column = i / 9
                    xPosition = initialXPosition + (CGFloat(column) * scrollViewWidth)
                    yPosition = initialYPosition
                    position = 1
                } else if position % 4 == 0 {
                    let rows = position / 4
                    position = 1
                    column = i / 9
                    xPosition = initialXPosition + (CGFloat(column) * scrollViewWidth)
                    yPosition = yPosition + kTokenHeight * CGFloat(rows) + kTokenLinePadding
                } else if (position == 1) {
                    xPosition = initialXPosition +  (CGFloat(page) * scrollViewWidth)
                } else {
                    xPosition = xPosition  + kTokenWidthPadding + kTokenLinePadding
                }
                
                let button = UIButton(frame: CGRect(x: xPosition, y: yPosition, width: stringWidth!, height: kTokenHeight))
                button.titleLabel?.textAlignment = .center
                button.setTitleColor(data?.tokenTextColor(), for: .normal)
                button.setTitleColor(data?.tokenColor(), for: .highlighted)
                button.titleLabel?.font = textFont
                button.setTitle(text, for: .normal)
                button.layer.borderColor = UIColor.clear.cgColor
                button.backgroundColor = data?.tokenColor()
                button.addTarget(self, action: #selector(self.showNutrientInfo(sender:)), for: .touchUpInside)
                button.layer.cornerRadius =  15
                xPosition = xPosition + stringWidth!
                position = position + 1
                buttonArray.append(button)
                
                if buttonArray.count == 3 || i == endIndex {
                    var finalWidth : CGFloat = 0
                    
                    for btn in buttonArray {
                        finalWidth = finalWidth + btn.frame.width
                    }
                    
                    var extraWidthPadding : CGFloat = 0
                    
                    if   maxAllowedWidth - finalWidth > 0 {
                        extraWidthPadding = maxAllowedWidth - finalWidth
                    } else {
                        extraWidthPadding = -kScrollViewPadding
                    }
                    var newX = buttonArray[0].frame.origin.x
                    for i  in 0...buttonArray.count-1 {
                        let curFrame = buttonArray[i].frame
                        buttonArray[i].frame = CGRect(x: newX , y: curFrame.origin.y, width: buttonArray[i].frame.width + (extraWidthPadding/CGFloat(buttonArray.count)) - 5 , height: buttonArray[i].frame.height)
                        nutrientsScrollView.addSubview(buttonArray[i])
                        newX = newX  + buttonArray[i].frame.width + 5
                    }
                    buttonArray = []
                }
            }
        }
    }
    
    func setUpNutrientSourceView() {
        let yPosition = pageControl.bounds.size.height + nutrientsScrollView.bounds.size.height + nutrientDetailsView.bounds.size.height
        var height : CGFloat = 0.0
        let bgFrame = CGRect(x: self.bounds.origin.x, y: yPosition, width: UIScreen.main.bounds.size.width - 30, height: 200)
       
        if self.bgView == nil {
            self.bgView = UIView(frame: bgFrame)
            bgView.backgroundColor = UIColor.clear
            self.addSubview(bgView)
        }

        let outlineFrame = CGRect(x: self.bounds.origin.x, y:15, width: UIScreen.main.bounds.size.width - 30, height: 150)
        
        if outlineView == nil {
            outlineView = UIView(frame: outlineFrame)
            outlineView.backgroundColor = UIColor.white
            outlineView.addShadow()
            bgView.addSubview(outlineView)
        }
        
        let labelFrame = CGRect(x: 15, y: 5, width: 150, height: 22)
        
        if recommendationLabel == nil {
            recommendationLabel = UILabel(frame: labelFrame)
            recommendationLabel.text = "Sources"
            recommendationLabel.textColor = NRColorUtility.sourcesLabelColor()
            recommendationLabel.font = UIFont.systemFont(ofSize: 15)
            outlineView.addSubview(recommendationLabel)
        }
        height = height + recommendationLabel.bounds.height

        let sourceSize = self.selectedNutrient?.sources.sizeOfBoundingRect(width: outlineView.frame.width - 30, height: 999, font: UIFont.systemFont(ofSize: 13))
        height = height + 15 + (sourceSize?.height)!

        let textFrame = CGRect(x: 15, y: labelFrame.height + 5, width: outlineView.frame.width - 30, height: ((sourceSize?.height)!))
        
        if nutrientRecommendation == nil {
            nutrientRecommendation = UITextView(frame: textFrame)
            nutrientRecommendation.text = self.selectedNutrient?.sources
            nutrientRecommendation.textColor = NRColorUtility.progressLabelColor()
            nutrientRecommendation.font = UIFont.systemFont(ofSize: 13)
            nutrientRecommendation.isEditable = false
            nutrientRecommendation.isSelectable = false
            outlineView.addSubview(nutrientRecommendation)
        } else {
            nutrientRecommendation.text = self.selectedNutrient?.sources
        }
        
        outlineView.frame = CGRect(x: outlineFrame.origin.x, y: outlineFrame.origin.y, width: outlineFrame.width, height: height + 25)
        nutrientRecommendation.frame = CGRect(x: textFrame.origin.x, y: textFrame.origin.y, width: textFrame.width, height: (sourceSize?.height)! + 22)
        bgView.frame = CGRect(x: bgFrame.origin.x, y: bgFrame.origin.y, width: bgFrame.width, height: height + 20)
        
        let tokenViewHeight = bgView.frame.height + nutrientDetailsView.frame.height + nutrientsScrollView.frame.height + pageControl.frame.height
        let containerView =  self.superview?.superview as! NutritionStatsView
        containerView.tokenViewHeight.constant = tokenViewHeight
    }
    
    // MARK: IBAction methods
    @IBAction func showNutrientInfo(sender:UIButton) {
        if  highlightedButton != sender {
            if ((highlightedButton) != nil) {
                highlightedButton.isSelected = false
                highlightedButton.isHighlighted = false
                self.setButtonHiglightedColor(button: highlightedButton)
            }
        
            highlightedButton = sender
            var nutrientData = nutrientList?.filter{$0.name == sender.titleLabel?.text}
            selectedNutrient = nutrientData?[0]
            self.setButtonHiglightedColor(button: sender)
            
            if delegate != nil {
                delegate?.setSelectedNutrient(nutrient: selectedNutrient!)
            }
        }
    }
    
    @IBAction func changePage(sender: UIPageControl) -> () {
        let x = CGFloat(pageControl.currentPage) * nutrientsScrollView.frame.size.width
        nutrientsScrollView.setContentOffset(CGPoint(x: x,y :0), animated: true)
    }
    
    func setButtonHiglightedColor(button : UIButton) {
        let bgColor = highlightedButton.backgroundColor
        let borderColor = highlightedButton.layer.borderColor
        
        if (button.isHighlighted) {
            button.backgroundColor = UIColor.white
            button.setTitleColor(bgColor , for: .highlighted)
            button.setTitleColor(bgColor, for: .normal)
        } else {
            let titleColor = button.titleColor(for: .normal)
            var labelColor = bgColor
            button.backgroundColor = titleColor
            
            if (titleColor == NRColorUtility.healthGreenColor()) {
                labelColor = NRColorUtility.progressLabelColor()
            }
            
            button.setTitleColor(labelColor , for: .highlighted)
            button.setTitleColor(labelColor, for: .normal)
        }
        
        button.layer.borderColor = borderColor == UIColor.clear.cgColor ? bgColor?.cgColor : UIColor.clear.cgColor
        button.layer.borderWidth = 2
    }
    
    // MARK: UIScrollViewDelegate
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        let pageNumber = round(scrollView.contentOffset.x / scrollView.frame.size.width)
        pageControl.currentPage = Int(pageNumber)
    }
}

//
//  NutritionScoreCache.swift
//  Nourish
//
//  Created by Nova on 7/28/17.
//  Copyright © 2017 SSIC. All rights reserved.
//

import Foundation
import RealmSwift

class NutritionScoreCache: Object {
    
// Specify properties to ignore (Realm won't persist these)
    
//  override static func ignoredProperties() -> [String] {
//    return []
//  }
    
    dynamic var data : Data!
    dynamic var date : Date!
}

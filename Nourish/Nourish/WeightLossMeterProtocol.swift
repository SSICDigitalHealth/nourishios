//
//  WeightLossMeterProtocol.swift
//  Nourish
//
//  Created by Vino (Vinodha) Sundaramoorthy on 2/7/17.
//  Copyright © 2017 SSIC. All rights reserved.
//

import UIKit

protocol  WeightLossMeterProtocol {
    func setMeterModel(model:weightLossMeter,bgColor:UIColor, isMetric : Bool,goal: UserGoal)
}

//
//  CameraSnapPresenter.swift
//  Nourish
//
//  Created by Nova on 5/16/17.
//  Copyright © 2017 SSIC. All rights reserved.
//

import UIKit
import AVFoundation
import Clarifai
import RxSwift

class CameraSnapPresenter: BasePresenter, AVCaptureMetadataOutputObjectsDelegate {
    
    var session: AVCaptureSession!
    var stillImageOutput: AVCaptureStillImageOutput?
    var videoPreviewLayer: AVCaptureVideoPreviewLayer!
    var proxyRect : CGRect?
    var cameraSnapView : CameraSnapView!
    var date : Date?
    var ocasion : Ocasion?
    
    @IBOutlet weak var barcodeButton : UIButton!
    @IBOutlet weak var takePhotoButton : UIButton!
    @IBOutlet weak var previewView : UIView!
    @IBOutlet weak var imageView : UIImageView!
    @IBOutlet weak var cameraButton : UIButton!
    var barcodeScanned = false
    var isBarCodeMode = false
    
    
    var recognitionDelegate : RecognitionViewContollerDelegate?
    
    let imageInteractor = ImageRecognitionInteractor()
    
    var cameraController : CameraSnapViewControllerProtocol?

    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.cameraButton.isSelected = true
        if AVCaptureDevice.authorizationStatus(forMediaType: AVMediaTypeVideo) ==  AVAuthorizationStatus.authorized
        {
            self.configureSession(isBarCodeMode: false)
        }
        else
        {
            AVCaptureDevice.requestAccess(forMediaType: AVMediaTypeVideo, completionHandler: { (granted :Bool) -> Void in
                if granted == true
                {
                    self.configureSession(isBarCodeMode: false)
                }
                else
                {
                    
                }
            });
        }
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }
    
    private func configureSession(isBarCodeMode : Bool) {
        self.session = AVCaptureSession()
        self.session!.sessionPreset = AVCaptureSessionPresetPhoto
        let camera = AVCaptureDevice.defaultDevice(withMediaType: AVMediaTypeVideo)
        
        var error : Error?
        var input : AVCaptureDeviceInput!
        
        do {
            input = try AVCaptureDeviceInput.init(device: camera)
        } catch let errorAV {
            input = nil
            error = errorAV
            LogUtility.logToFile(errorAV)
        }
        
        if error == nil && (self.session?.canAddInput(input))! {
            self.session?.addInput(input)
        }
        
        self.stillImageOutput = AVCaptureStillImageOutput()
        self.stillImageOutput?.outputSettings = [AVVideoCodecKey: AVVideoCodecJPEG]
        
        self.configureOutputs(isBarCodeMode: isBarCodeMode)
        self.configurePreview()
        
    }
    
    private func configureOutputs(isBarCodeMode : Bool) {
        self.isBarCodeMode = isBarCodeMode
        
        let metaDataOutput = AVCaptureMetadataOutput()
        
        if self.session!.canAddOutput(self.stillImageOutput) {
            self.session!.addOutput(self.stillImageOutput)
        }
        
        if (self.session.canAddOutput(metaDataOutput)) {
            self.session.addOutput(metaDataOutput)
            metaDataOutput.setMetadataObjectsDelegate(self, queue: DispatchQueue.main)
            metaDataOutput.metadataObjectTypes = [AVMetadataObjectTypeEAN13Code,AVMetadataObjectTypeUPCECode,AVMetadataObjectTypeEAN8Code,AVMetadataObjectTypeCode39Code,AVMetadataObjectTypeQRCode,AVMetadataObjectTypeCode93Code]
        }
    }
    
    private func configurePreview() {
        videoPreviewLayer = AVCaptureVideoPreviewLayer(session: session)
        videoPreviewLayer!.videoGravity = AVLayerVideoGravityResizeAspectFill
        videoPreviewLayer!.connection?.videoOrientation = self.videoOrientation()
        self.previewView?.layer.addSublayer(videoPreviewLayer!)
        session!.startRunning()
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
    }
    
    func layoutVideoPreviewLayer() {
        if AVCaptureDevice.authorizationStatus(forMediaType: AVMediaTypeVideo) ==  AVAuthorizationStatus.authorized
        {
            self.proxyRect = self.previewView.bounds
            self.videoPreviewLayer!.videoGravity = AVLayerVideoGravityResizeAspectFill
            self.videoPreviewLayer!.connection?.videoOrientation = self.videoOrientation()
            self.videoPreviewLayer!.frame = self.previewView.bounds
        }
        
    }
    
    func snapPhoto(barCode : String?) {
        if let videoConnection = self.stillImageOutput!.connection(withMediaType: AVMediaTypeVideo) {
            self.stillImageOutput?.captureStillImageAsynchronously(from: videoConnection, completionHandler: { (imageBuffer, error) -> Void in
                if imageBuffer != nil {
                    let imageData = AVCaptureStillImageOutput.jpegStillImageNSDataRepresentation(imageBuffer)
                    let dataProvider = CGDataProvider(data: imageData! as CFData)
                    let cgImageRef = CGImage(jpegDataProviderSource: dataProvider!, decode: nil, shouldInterpolate: true, intent: CGColorRenderingIntent.defaultIntent)
                    
                    let image = UIImage(cgImage: cgImageRef!, scale: 1.0, orientation: self.imageOrientation())
                    let outputRect = self.videoPreviewLayer?.metadataOutputRectOfInterest(for: (self.videoPreviewLayer?.bounds)!)
                    let takenCGImage = image.cgImage!
                    let width = CGFloat(takenCGImage.width)
                    let height = CGFloat(takenCGImage.height)
                    let cropRect = CGRect(x: (outputRect?.origin.x)! * width, y: (outputRect?.origin.y)! * height, width: (outputRect?.size.width)! * width, height: (outputRect?.size.height)! * height)
                    
                    let cropCGImage = takenCGImage.cropping(to: cropRect)
                    let cropTakenImage = UIImage(cgImage: cropCGImage!, scale: 1, orientation: self.imageOrientation())
                    self.imageView.image = cropTakenImage
                    self.previewView.bringSubview(toFront: self.imageView)
                    self.recognizeImage(image: cropTakenImage, barCode : barCode)
                }
            })
        }

    }
    @IBAction func photoButtonPressed() {
        self.snapPhoto(barCode: nil)
    }
    
    private func correctCropRectFrom(metaRect : CGRect, originalSize : CGSize) -> CGRect {
        var cropRect = CGRect.zero
        
        cropRect.origin.x = metaRect.origin.x * originalSize.width
        cropRect.origin.y = (metaRect.origin.y * originalSize.height)
        cropRect.size.width = metaRect.size.width * originalSize.width
        cropRect.size.height = metaRect.size.height * originalSize.height
        
        cropRect = cropRect.integral
        
        return cropRect
        
    }
    private func resizeImage(image: UIImage, newWidth: CGFloat) -> UIImage? {
        let scale = newWidth / image.size.width
        let newHeight = image.size.height * scale
        UIGraphicsBeginImageContext(CGSize(width: newWidth, height: newHeight))
        image.draw(in: CGRect(x: 0, y: 0, width: newWidth, height: newHeight))
        
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        return newImage
    }
    private func recognizeImage(image : UIImage, barCode : String?) {
        let resizedImage = self.resizeImage(image: image, newWidth: 920)
        var modelsLocal : [foodSearchModel]? = []
        var imageToProcide = image
        
        if resizedImage != nil {
            imageToProcide = resizedImage!
        }
        
        self.cameraSnapView.prepareLoadView()
        
        if barCode != nil {
           /* let _ = self.imageInteractor.recognizeBarcode(barCode: barCode!).subscribe(onNext: {models in
                modelsLocal = models
                self.cameraSnapView.stopActivityAnimation()
            }, onError: {error in
                DispatchQueue.main.async {
                    self.cameraSnapView.parseError(error: error, completion: nil)
                }
            }, onCompleted: {
                DispatchQueue.main.async {
                    let vc = RecognitionFoodListViewController(nibName : "RecognitionFoodListViewController", bundle : nil)
                    vc.foodImage = image
                    vc.foods = modelsLocal
                    vc.date = self.date
                    vc.ocasion = self.ocasion
                    if self.recognitionDelegate != nil {
                        vc.delegate = self.recognitionDelegate
                        self.cameraController?.present(vc: vc, needDismission: false)
                    } else {
                        NavigationUtility.presentVC(vc: vc, needDismission: true)
                    }
                
                }
            }, onDisposed: {})*/
        } else {
            
            let recognizingWithTimeout = self.imageInteractor.recognizeImage(image: image).timeout(30, scheduler: MainScheduler.instance)
            let _ = recognizingWithTimeout.subscribe(onNext: { [unowned self] models in
                modelsLocal = models
                self.cameraSnapView.stopActivityAnimation()
            }, onError: {error in
                self.showVcWith(foods: [foodSearchModel](), image: image)
            }, onCompleted: {
                self.showVcWith(foods: modelsLocal, image: image)
            }, onDisposed: {})
        }
    }
    
    private func showVcWith(foods : [foodSearchModel]?, image : UIImage) {
        let vc = RecognitionFoodListViewController(nibName : "RecognitionFoodListViewController", bundle : nil)
        vc.foodImage = image
        vc.foods = foods
        vc.date = self.date
        vc.ocasion = self.ocasion
        if self.recognitionDelegate != nil {
            vc.delegate = self.recognitionDelegate
            self.cameraController?.present(vc: vc, needDismission: false)
        } else {
            NavigationUtility.presentVC(vc: vc, needDismission: true)
        }
    }
    
    
    private func imageOrientation() -> UIImageOrientation {
        
        var orientation = UIImageOrientation.down
        
        if UIDevice.current.orientation == UIDeviceOrientation.landscapeLeft{
            orientation = UIImageOrientation.up
        }
        else if UIDevice.current.orientation == UIDeviceOrientation.landscapeRight{
            orientation = UIImageOrientation.down
        }
        else if UIDevice.current.orientation == UIDeviceOrientation.portraitUpsideDown{
            orientation = UIImageOrientation.left
        }
        else {
            orientation = UIImageOrientation.right
        }
        
        return orientation
    }
    
    private func videoOrientation() -> AVCaptureVideoOrientation {
        var orientation : AVCaptureVideoOrientation = .portrait
        
        if UIDevice.current.orientation == UIDeviceOrientation.landscapeLeft{
            orientation = .landscapeRight
        }
        else if UIDevice.current.orientation == UIDeviceOrientation.landscapeRight{
            orientation = .landscapeLeft
        }
        else if UIDevice.current.orientation == UIDeviceOrientation.portraitUpsideDown{
            orientation = .portraitUpsideDown
        }
        else {
            orientation = .portrait
        }
        return orientation
    }
    
    @IBAction func cameraButtonPressed(sender : UIButton) {
        self.takePhotoButton.isHidden = false
        self.cameraButton.isSelected = true
        self.barcodeButton.isSelected = false
        self.configureOutputs(isBarCodeMode: false)
    }
    
    @IBAction func barCodeButtonPressed(sender : UIButton) {
        self.takePhotoButton.isHidden = true
        self.cameraButton.isSelected = false
        self.barcodeButton.isSelected = true
        self.configureOutputs(isBarCodeMode: true)
    }
    
    func captureOutput(_ captureOutput: AVCaptureOutput!, didOutputMetadataObjects metadataObjects: [Any]!, from connection: AVCaptureConnection!) {
        
        
        // Get the first object from the metadataObjects array.
        if self.barcodeScanned == false && self.isBarCodeMode == true {
            print("Output Captured")
            if let barcodeData = metadataObjects.first {
                var barcode : String!
                // Turn it into machine readable code
                let barcodeReadable = barcodeData as? AVMetadataMachineReadableCodeObject;
                if let readableCode = barcodeReadable {
                    DispatchQueue.main.async {
                        self.barcodeScanned = true
                        print("Bar code detected \(readableCode.stringValue)")
                        barcode = self.rightBarcodeFromScanned(barcode: readableCode.stringValue)
                        self.snapPhoto(barCode: barcode)
                        AudioServicesPlaySystemSound(SystemSoundID(kSystemSoundID_Vibrate))
                    }
                    
                }
                
            }
  
        }
    }
    private func rightBarcodeFromScanned(barcode : String) ->String {
        var rightBarcode = barcode
        if rightBarcode[rightBarcode.startIndex] == "0" && rightBarcode.count == 13 {
            rightBarcode.remove(at: rightBarcode.startIndex)
        }
        return rightBarcode
        
    }
}

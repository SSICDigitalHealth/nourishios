//
//  ActivityCaloriesBurnedCellPresenter.swift
//  Nourish
//
//  Created by Andrey Zmushko on 8/25/17.
//  Copyright © 2017 SSIC. All rights reserved.
//

import UIKit
import RxSwift


class ActivityCaloriesBurnedCellPresenter: ActivityBaseCellPresenter, ProgressBarChartDelegate {
    
    let interactor = CaloricBurnInteractor()
    let mapperObjectCaloricBurn = CaloricBurnedModel_to_CaloricBurnedViewModel()
    var caloricBurnedViewModel = CaloricBurnedViewModel()
    var proportionWithChart = (0.0 , 0.0, 0.0)
    
    var label = [String]()
    let basalColor = NRColorUtility.consumedCaloriesDailyWeeklyColor()
    let activeColor = NRColorUtility.overConsumedCaloriesDailyWeeklyColor()
    
    private func getInformation(cell: CaloricBurnedTableViewCell, startDate: Date, endDate: Date) {
        cell.baseView.viewWillAppear(true)
        cell.baseView.removeActivityIndicator()
        self.subscribtions.append(self.interactor.execute(startDate: startDate, endDate: endDate).observeOn(MainScheduler.instance).subscribe(onNext: { [unowned self] caloricBurned in
            self.caloricBurnedViewModel = self.mapperObjectCaloricBurn.transform(model: caloricBurned)
        }, onError: {error in
            cell.baseView.parseError(error: error,completion: nil)
        }, onCompleted: {
            self.setupViewWithData(cell: cell)
            self.reloadTable(cell: cell)
            cell.baseView.stopActivityAnimation()
        }, onDisposed: {
        }))
    }
    
    
    
    override func fetchModel(For basecell: ActivityBaseCell) {
        super.fetchModel(For: basecell)
        
        let cell = basecell as! CaloricBurnedTableViewCell
        
        if let delegate = self.delegate {
            let date = delegate.fetchEpoch()
            self.getInformation(cell: cell, startDate: date.startDate, endDate: date.endDate)
        }
    }
    
    private func setupViewWithData(cell: CaloricBurnedTableViewCell) {
        if let caloricBurn = self.caloricBurnedViewModel.userBurnedCal {
            let value = NSMutableAttributedString(string: String(format: "%.0f", caloricBurn), attributes: cell.boldAttr)
            value.append(NSMutableAttributedString(string: " kcal", attributes: cell.bookAttr))
            cell.valueLabel.attributedText = value
        }
        
        if self.caloricBurnedViewModel.caloricState != nil {
            self.createLabelLegend()
            self.proportionWithChart = getProportionWithChartLine()
            cell.progressBarChart.widthBarPattern = (0.8, 0)
            cell.progressBarChart.delimiterMultiplier = 63 / UIScreen.main.bounds.width
            
            cell.progressBarChart.reloadData(DataSource: self)
        }
        
    }
    
    func numberOfBars(in progressBarChart: ProgressBarChart) -> Int {
        if let caloricData = self.caloricBurnedViewModel.caloricState {
            return caloricData.count
        } else {
            return 0
        }
    }
    
    func numberOfAxis(in progressBarChart: ProgressBarChart) -> Int {
        return 3
    }
    
    func progressBarChart(_ progressBarChart: ProgressBarChart, axisAtIndex:Int) -> ProgressBarChart.Axis? {
        let type: ProgressBarChart.AxisType!
        let value: Double!
        var color = UIColor.gray
        var label: ProgressBarChartLabel?
        
        switch axisAtIndex {
        case 2:
            type = .solid
            value = 1.0
            color = UIColor.clear
        case 1:
            type = .dashed
            value = proportionWithChart.0
            color = NRColorUtility.hexStringToUIColor(hex: "#cccccc")
            label = ProgressBarChartLabel()
            label?.minHeightWith(height: 1,
                                 text: String(format: "%.0f kcal", proportionWithChart.2),
                                 numberOfLines: 1,
                                 font: UIFont.systemFont(ofSize: 12, weight: UIFontWeightRegular),
                                 color: NRColorUtility.hexStringToUIColor(hex: "747474"),
                                 verticalAlignment: ProgressBarChartLabel.LabelAlignment.top)
        default:
            type = .solid
            value = 0.0
            color = NRColorUtility.hexStringToUIColor(hex: "#cccccc")
            label = ProgressBarChartLabel()
            label?.minHeightWith(height: 1,
                                 text: String(format: "%@", "0 kcal"),
                                 numberOfLines: 1,
                                 font: UIFont.systemFont(ofSize: 12, weight: UIFontWeightRegular),
                                 color: NRColorUtility.hexStringToUIColor(hex: "747474"),
                                 verticalAlignment: ProgressBarChartLabel.LabelAlignment.top)
        }
        
        return (color, type, value, label, nil) as ProgressBarChart.Axis
    }
    
    func progressBarChart(_ progressBarChart: ProgressBarChart, barAtIndex:Int) -> ProgressBarChart.Bar? {
        var legendaView: ProgressBarChartLabel?
        
        let dataForChart = convertInformationForChart(index: barAtIndex)

        legendaView = ProgressBarChartLabel()
        legendaView?.label.textAlignment = .center
        legendaView?.minHeightWith(height: 4.5,
                                    text: label[barAtIndex] ,
                                    numberOfLines: 1,
                                    font: UIFont.systemFont(ofSize: 11, weight: UIFontWeightRegular),
                                    color: NRColorUtility.hexStringToUIColor(hex: "747474"),
                                    verticalAlignment: ProgressBarChartLabel.LabelAlignment.bottom)
        
        return (dataForChart.0, dataForChart.1 , legendaView as UIView?, ProgressBarChart.LegendaAlignment.center) as ProgressBarChart.Bar
    }
    
    private func getProportionWithChartLine() -> (Double, Double, Double) {
        var maxValueWithConsumed = 0.0
        var maxBaselCaloric = 0.0
        
        if let dataCaloricBurned = self.caloricBurnedViewModel.caloricState {
            maxBaselCaloric = (dataCaloricBurned.max(by: {$0.cal.basalCalories < $1.cal.basalCalories})?.cal.basalCalories)!
            let maxData = dataCaloricBurned.max(by: {$0.cal.basalCalories + $0.cal.activeCalories < $1.cal.basalCalories + $1.cal.activeCalories})
            maxValueWithConsumed = (maxData?.cal.activeCalories)! + (maxData?.cal.basalCalories)!
        }
        
        let middleLine = maxValueWithConsumed > maxBaselCaloric ? (maxBaselCaloric/maxValueWithConsumed) : 1
        
        return (middleLine, maxValueWithConsumed, maxBaselCaloric)
    }
    
    private func convertInformationForChart(index: Int) -> ([CGFloat], [UIColor]) {
        var resultProportion = [CGFloat]()
        var resultColor = [UIColor]()
        
        if let dataChart  = self.caloricBurnedViewModel.caloricState?[index] {
            resultProportion.append(CGFloat(dataChart.cal.basalCalories/proportionWithChart.1))
            resultProportion.append(CGFloat(dataChart.cal.activeCalories/proportionWithChart.1))
        }
        
        resultColor = [basalColor, activeColor]

        return (resultProportion, resultColor)
    }
    
    private func createLabelLegend() {
        self.label = Array(repeating: "", count: 24)
        label[0] = "12A"
        label[12] = "12P"
        label[23] = label[0]
    }
}

//
//  NutritionModel_to_Nutrition.swift
//  Nourish
//
//  Created by Nova on 11/21/16.
//  Copyright © 2016 SSIC. All rights reserved.
//

import UIKit

class NutritionModel_to_Nutrition: NSObject {
    func transform(nutritionModel : NutritionModel) -> Nutrition {
        let nutrition = Nutrition()
        nutrition.vitamins = nutritionModel.vitamins
        nutrition.electrolytes = nutritionModel.electrolytes
        nutrition.macronutrients = nutritionModel.macronutrients
        nutrition.minerals = nutritionModel.minerals
        nutrition.nutritionScore = nutritionModel.nutritionScore
        return nutrition
    }
    
    func transform(modelArray : [NutritionModel]) -> [Nutrition] {
        var arrayToReturn : [Nutrition] = []
        for model in modelArray {
            arrayToReturn.append(self.transform(nutritionModel: model))
        }
        return arrayToReturn
    }
}

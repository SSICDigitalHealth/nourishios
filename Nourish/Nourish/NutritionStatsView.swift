//
//  NutritionStatsView.swift
//  Nourish
//
//  Created by Vino (Vinodha) Sundaramoorthy on 12/13/16.
//  Copyright © 2016 SSIC. All rights reserved.
//

import UIKit

class NutritionStatsView: BaseView , NRNutritionStatsProtocol {
    
    @IBOutlet weak var periodSegmentControl : NRSegmentedControl!
    @IBOutlet weak var nutritionView : NutritionView!
    @IBOutlet weak var tokenView : NutrientTokenView!
    @IBOutlet weak var consumedChartView : NutritionCombinedChartView!
    @IBOutlet weak var scrollView : UIScrollView!
    @IBOutlet weak var tokenViewHeight : NSLayoutConstraint!

    var selectedNutrient : nutrients? = nil
    var presenter = NRNutritionStatsPresenter()
    var controller = NRNutritionViewController()
    var period : period = .today
    var nutritionModel : nutritionStatsModel!
    
    
    override func viewWillAppear(_ animated: Bool) {
        self.basePresenter = presenter
        presenter.statsView = self
        self.setupSegmentControl()
        tokenView.delegate = presenter
        super.viewWillAppear(animated)
    }

    func setupSegmentControl() {
        let segmentColor = NRColorUtility.nourishNavigationColor()
        let textFont = UIFont.systemFont(ofSize: 15)
        let textColor = UIColor.white
        
        periodSegmentControl = NRSegmentedControl(frame: CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: 45), titles: ["TODAY","WEEKLY","MONTHLY"], selectedtitles:[],action: {
            control, index in
        })
        periodSegmentControl.appearance = segmentAppearance(backgroundColor: segmentColor, selectedBackgroundColor:segmentColor, textColor: textColor, font: textFont, selectedTextColor: textColor, selectedFont: textFont, bottomLineColor: segmentColor, selectorColor: UIColor.white, bottomLineHeight: 5, selectorHeight: 5, labelTopPadding: 0, hasImage: false, imageSize: nil)
        periodSegmentControl.delegate = presenter
        self.addSubview(periodSegmentControl)
    }
    
    // MARK: NRNutritionStatsProtocol Implementation
    func startActivityProgress() {
        self.prepareLoadView()
    }
    
    func setUpNutritionStats(model : nutritionStatsModel , period:period) {
        self.period = period
        self.nutritionModel = model
        
        //Nutriton View
        nutritionView.setupWith(vitValue: model.vitamins, eleValue: model.electrolytes, macroValue: model.macronutrients, mineralValue: model.minerals, score: model.nutritionScore, period: period)
        
        //Token View 
        tokenView.setUpNutrient(tokens: model.nutrArray, period: period)
        
        //Nutrition vs calories chart 
        consumedChartView.setCalorieConsumedStats(model: model, forPeriod: period)
        self.scrollView.scrollToTop()
        self.stopActivityAnimation()
    }
}

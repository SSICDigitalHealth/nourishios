//
//  ActivityWeightChartCell.swift
//  Nourish
//
//  Created by Andrey Zmushko on 9/7/17.
//  Copyright © 2017 SSIC. All rights reserved.
//

import UIKit

class ActivityWeightChartCell: ActivityBaseCell {

    @IBOutlet weak var iconImageView: UIImageView!
    @IBOutlet weak var iconFrame: UIView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var valueLabel: UILabel!
    @IBOutlet weak var chartView: ActivityWeightChart!
    @IBOutlet weak var baseView: BaseView!
    @IBOutlet weak var chartHeight: NSLayoutConstraint!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.iconFrame.layer.cornerRadius = self.iconFrame.bounds.width / 2
        self.selectionStyle = .none
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}

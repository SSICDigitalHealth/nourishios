//
//  MicroelementBalanceInteractor.swift
//  Nourish
//
//  Created by Vlad Birukov on 08.06.17.
//  Copyright © 2017 SSIC. All rights reserved.
//

import Foundation
import RxSwift

class MicroelementBalanceInteractor {
    private let nestleProgressRepository = NestleProgressRepository.shared
    
    func execute(startDate: Date, endDate: Date) -> Observable <MicroelementModel> {
        return self.nestleProgressRepository.getWatchoutListFor(startDate:startDate, endDate:endDate)
        
//        return Observable<MicroelementModel>.create{(observer) -> Disposable in
//            let pause = Int(arc4random_uniform(1))
//            let dispat = DispatchTime.now() + .seconds(pause)
//            DispatchQueue.main.asyncAfter(deadline: dispat, execute: {
//                let microelementModel = MicroelementModel()
//                microelementModel.data = [(date: Date?, elements: [(type: MicroelementType, element: UserMicroelement)])]()
//                let message = ["You had too much sugar for 4 days this week. Adding sugar to your tea or coffee ? Try to decrease over time to amount.", "You’ve eaten too much fats this month. Avoid breading, frying and creamy sauces, they add calories and fat.", "14% less salt than last week"]
//                let title = ["Your top sugar sources this month:", "Your top salt sources this month:", "Your top saturated fats sources this month:"]
//                let topRich = [["Sugar added to coffee (10g/day)", "Cakes", "Soft drinks"], ["Bread rolls", "Cured meals", "Cheeses"], ["Potato chips", "Pizzas", "Bacon"]]
//                
//                for i in 0 ... self.daysBetweenDates(startDate: startDate, endDate: endDate) {
//                    microelementModel.data?.append((date: Calendar.current.date(byAdding: .day, value: i - 6, to: Date()), elements: [
//                        (type: .sugars, element: UserMicroelement(consumed: Double(CGFloat(arc4random() % 50)), target: 37, unit: "mg")),
//                        (type: .salt, element: UserMicroelement(consumed: Double(CGFloat(arc4random() % 10)), target: 6.1, unit: "g")),
//                        (type: .saturatedFat, element: UserMicroelement(consumed: Double(CGFloat(arc4random() % 200)), target: 177, unit: "g"))
//                        ]))
//                }
//                
//                microelementModel.detailInformation = [DetailInformationModel]()
//                
//                for i in 0..<3 {
//                    let detailInformation = DetailInformationModel()
//                    detailInformation.title = title[i]
//                    detailInformation.message = message[i]
//                    detailInformation.topFood = topRich[i]
//                    microelementModel.detailInformation?.append(detailInformation)
//                }
//                
//                observer.onNext(microelementModel)
//                observer.onCompleted()
//            })
//            
//            return Disposables.create()
//        }
    }
    
    private func daysBetweenDates(startDate: Date, endDate: Date) -> Int
    {
        let calendar = Calendar.current
        let components = calendar.dateComponents([.day], from: startDate, to: endDate)
        return components.day!
    }
}

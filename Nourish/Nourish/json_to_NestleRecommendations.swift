//
//  json_to_NestleRecommendations.swift
//  Nourish
//
//  Created by Gena Mironchyk on 9/11/17.
//  Copyright © 2017 SSIC. All rights reserved.
//

import UIKit
import Foundation

class json_to_NestleRecommendations: NSObject {
    func transform(dictionary : [String : Any]) -> NestleRecommendationsCache {
        let model = NestleRecommendationsCache()
        let additionalInfo = dictionary["additional_info"] as? [String: Any]
        if additionalInfo != nil {
            if !(additionalInfo?["meal_display_unit"] is NSNull) {
                model.caption = additionalInfo?["meal_display_unit"] as? String
            }
        }
        
        let tips = dictionary["tips"] as? [[String : Any]]
        if tips != nil {
            let convertedArray = self.jsonToNestleTipsFrom(arraysOfDictionaries: tips!)
            let _ = convertedArray.map({ model.tips.append($0)})
        }
        
        let recipes = dictionary["recipes"] as? [[String: Any]]
        if recipes != nil {
            let convertedArray = self.jsonToNestleRecipes(arraysOfDictionaries: recipes!)
            let _ = convertedArray.map({ model.dataRecipies.append($0)})
        }
        
        let mapper = jsonNestle_to_foodSearchModel()
        if let models = dictionary["meals"] as? [[String : Any]] {
            let foodModels = mapper.transform(arrayOfMeals:models)
            let _ = foodModels.map({model.meals.append($0)})
        }
        
        
        
        
        return model
    }
    
    private func jsonToNestleTipsFrom(arraysOfDictionaries : [[String : Any]]) -> [NestleCacheTipsModel] {
        var arrayOfTipsToReturn : [NestleCacheTipsModel] = []
        for tipObj in arraysOfDictionaries {
            arrayOfTipsToReturn.append(self.dictToTip(tipDict: tipObj))
        }
        return arrayOfTipsToReturn
    }
    
    
    private func dictToTip(tipDict : [String : Any]) -> NestleCacheTipsModel {
        let tipModel = NestleCacheTipsModel()
        
        if !(tipDict["component_id"] is NSNull) {
            tipModel.componentId = tipDict["component_id"] as? String
        }
        
        if !(tipDict["indicator"] is NSNull) {
            tipModel.indicator = tipDict["indicator"] as? String
        }
        
        if !(tipDict["messages"] is NSNull) {
            let convertedArray = tipDict["messages"] as? [String]
            if convertedArray != nil {
                for item in convertedArray! {
                    let realmStr = RealmString()
                    realmStr.stringValue = item
                    tipModel.messages.append(RealmString(value: realmStr))
                }
            }
        }
        
        if !(tipDict["title"] is NSNull) {
            tipModel.tipTitle = tipDict["title"] as? String
        }
        
        if !(tipDict["type"] is NSNull) {
            tipModel.tipType = tipDict["type"] as? String
        }
        
        return tipModel
    }
    
    private func jsonToNestleRecipes(arraysOfDictionaries: [[String : Any]]) -> [RecipiesCacheModel] {
        var arrayOfRecipesReturn = [RecipiesCacheModel]()

        for recipesObj in arraysOfDictionaries {
            arrayOfRecipesReturn.append(self.arrayToRecipes(recepiesDict: recipesObj))
        }
        
        return arrayOfRecipesReturn
    }
    
    private func arrayToRecipes(recepiesDict: [String : Any]) -> RecipiesCacheModel {
        let recipesModel = RecipiesCacheModel()
        
        if !(recepiesDict["food_id"] is NSNull) {
            recipesModel.foodId = recepiesDict["food_id"] as? String ?? ""
        }
        
        if !(recepiesDict["food_name"] is NSNull) {
            recipesModel.nameFood = recepiesDict["food_name"] as? String ?? ""
        }
        
        if !(recepiesDict["unit"] is NSNull) {
            recipesModel.unit = recepiesDict["unit"] as? String ?? ""
        }
        
        if !(recepiesDict["grams"] is NSNull) && !(recepiesDict["kcal_per_gram"] is NSNull) {
            recipesModel.numberCal = (recepiesDict["grams"] as? Double ?? 0.0) * (recepiesDict["kcal_per_gram"] as? Double ?? 0.0)
        }
        
        if recepiesDict["ingredients"] != nil {
            let convertedArray =  recepiesDict["ingredients"] as? [String]
            if convertedArray != nil {
                for item in convertedArray! {
                    let realmStr = RealmString()
                    realmStr.stringValue = item
                    recipesModel.ingredient.append(RealmString(value: realmStr))
                }
            }
        }
        
        if !(recepiesDict["amount"] is NSNull) {
            recipesModel.amount = recepiesDict["amount"] as? Double ?? 0.0 
        }
        
        if !(recepiesDict["grams"] is NSNull) {
            recipesModel.grams = recepiesDict["grams"] as? Double ?? 0.0
        }
        
        if !(recepiesDict["image_url"] is NSNull) && !(recepiesDict["food_id"] is NSNull) {
            let result  = self.saveImage(url: recepiesDict["image_url"] as? String, foodId: recepiesDict["food_id"] as? String)
            if result.1 == true {
                recipesModel.pathImage = result.0
            }
        }
        
        if recepiesDict["instructions"] != nil {
            let convertedArray =  recepiesDict["instructions"] as? [String]
            if convertedArray != nil {
                for item in convertedArray! {
                    let realmStr = RealmString()
                    realmStr.stringValue = item
                    recipesModel.instruction.append(RealmString(value: realmStr))
                }
            }
        }
        
        return recipesModel
    }
    
    private func saveImage(url: String?, foodId: String?) -> (String, Bool) {
        var path = ""
        var result  = false
        
        if url != nil && foodId != nil {
            path = String(format: "%@.png", (foodId?.replacingOccurrences(of: " ", with: "_"))!)
            result = true
            
            if !(self.isLoadImageLater(foodId: foodId!)) {
                DispatchQueue.global().async {
                    let urlImage = URL(string: url!)
                    let data = try? Data(contentsOf: urlImage!)
                    if data != nil {
                        let imageRecipes = UIImage(data: data!)
                        
                        let documentsURL = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first!
                        let fileURL = documentsURL.appendingPathComponent(String(format: "%@", path))
                        if let pngImageData = UIImagePNGRepresentation(imageRecipes!) {
                            try? pngImageData.write(to: fileURL, options: .atomic)
                        }
                    }
                }
            }
            
        }
        
        return (path, result)
    }
    
    private func isLoadImageLater(foodId: String) -> Bool {
        let documentsURL = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first!
        let filePath = documentsURL.appendingPathComponent(String(format: "%@.png", foodId.replacingOccurrences(of: " ", with: "_"))).path
        
        if FileManager.default.fileExists(atPath: filePath) {
            return true
        }
        
        return false
    }
}

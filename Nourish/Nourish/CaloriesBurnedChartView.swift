//
//  CaloriesBurnedChartView.swift
//  Nourish
//
//  Created by Vino (Vinodha) Sundaramoorthy on 12/24/16.
//  Copyright © 2016 SSIC. All rights reserved.
//

import UIKit

let kDailyMultiplier : Int = 1
let kWeeklyMonthlyMultiplier : Int = 10
let kDailyCount : Int = 24

class CaloriesBurnedChartView : BaseView,IAxisValueFormatter {
    var chatView : BarChartView!
    var chartData : BarChartData!
    var period : period = .weekly
    var titleLabel : UILabel!
    var calImageView : UIImageView!
    var stepsImageView : UIImageView!
    var calorieLabel : UILabel!
    var stepsLabel : UILabel!
    var activityModel : activityModel!
    
    override func draw(_ rect: CGRect) {
        
        self.addShadow()

        if chatView == nil {
            chatView = BarChartView.init(frame:  CGRect(x: 0, y: 100, width: self.bounds.width - 30, height: self.bounds.height - 100))
            self.addSubview(chatView)
            if self.activityModel != nil {
                self.setUpBarChartView()
                self.drawTitle()
            }
            
        } else {
            if self.activityModel != nil {
                self.setUpBarChartView()
                self.drawTitle()
            }
        }
    }
    
    func setUpBarChartView() {
        chatView.drawGridBackgroundEnabled = false
        chatView.chartDescription?.enabled = false;
        chatView.dragEnabled = false;
        chatView.setScaleEnabled(true)
        chatView.autoScaleMinMaxEnabled = false
        chatView.backgroundColor = UIColor.clear
        chatView.isUserInteractionEnabled = false
        chatView.legendRenderer.legend = nil
        chatView.drawRoundedBarEnabled = true
        chatView.renderer?.inverseData = true
        chartData =  BarChartData.init()
        
        if self.period == .today {
            chatView.extraLeftOffset = 15
        }

        // x Axis
        let xAxis : XAxis = chatView.xAxis
        xAxis.labelPosition = .bottom
        xAxis.centerAxisLabelsEnabled = true
        xAxis.drawGridLinesEnabled = false
        xAxis.labelCount = activityModel.caloriesArray.count - 1
        xAxis.granularity = 1
        xAxis.axisLineColor = UIColor.clear
        xAxis.valueFormatter = self

        // right axis
        let rightAxis = chatView.rightAxis
        rightAxis.enabled = false
        
        // left Axis
        let leftAxis = chatView.leftAxis
        leftAxis.enabled = true
        leftAxis.axisLineColor = UIColor.clear
        leftAxis.zeroLineColor = UIColor.clear
        leftAxis.gridLineDashPhase = 0
        leftAxis.gridLineDashLengths = [2,2]
        leftAxis.labelCount =  period == .today ? 3 : 5
        leftAxis.valueFormatter = self
        leftAxis.gridColor = NRColorUtility.activityBarChartAxisColor()
        
        let data = self.setupBarChartData()
        xAxis.axisMinimum = 0.5
        xAxis.axisMaximum = data.xMax + 0.5
        leftAxis.granularity =  data.yMax / 2
       
        if period == .today {
            leftAxis.axisMinimum = 0
        } else {
            leftAxis.axisMinimum = data.yMin + 10
        }

        chatView.data = data
        chatView.animate(yAxisDuration: 1)
    }
    
    func setupBarChartData() -> BarChartData {
        var yValues : [BarChartDataEntry] = []
        var yValues1 : [BarChartDataEntry] = []
        
        let multiplier : Int = period == .today ? kDailyMultiplier : kWeeklyMonthlyMultiplier
        var xAxisRange = period == .today ? kDailyCount : self.activityModel.caloriesArray.count-1
        
    
        var allKeys : [String] = []
        for i in 0...self.activityModel.caloriesArray.count - 1 {
            allKeys.append(self.activityModel.caloriesArray[i].caloriePeriodRange)
        }
        
       /* if self.activityModel.activityArray.count > 0 {
            for j in 0...self.activityModel.activityArray.count - 1 {
                allKeys.append(self.activityModel.activityArray[j].periodString)
            }
        }*/
        
        /*for i in  0...xAxisRange {
            var y : Double = 0
            if i <= self.activityModel.caloriesArray.count-1 {
                y = self.activityModel.caloriesArray[i].calorie
            }
            yValues.append(BarChartDataEntry(x:Double(i+1) , y: y * Double(multiplier)))
        }
        
        if self.activityModel.activityArray.count > 0 {
            for j in  0...self.activityModel.activityArray.count-1 {
                yValues1.append(BarChartDataEntry(x:Double(j+1) , y: -self.activityModel.activityArray[j].activeHr))
            }
        }*/
        
        xAxisRange = 0
        //allKeys = Array(Set(allKeys))
        
        if period == .today {
            xAxisRange = period == .today ? kDailyCount : self.activityModel.caloriesArray.count-1
            for i in  0...xAxisRange {
                var y : Double = 0
                if i <= self.activityModel.caloriesArray.count-1 {
                    y = self.activityModel.caloriesArray[i].calorie
                }
                yValues.append(BarChartDataEntry(x:Double(i+1) , y: y * Double(multiplier)))
            }
        } else {
        for i in 0...allKeys.count-1 {
            let keyValue = allKeys[i]
            let activityValue = self.activityModel.activityArray.filter{$0.periodString == keyValue}
            let calorieValue = self.activityModel.caloriesArray.filter{$0.caloriePeriodRange == keyValue}
            
            
            if calorieValue.count > 0 {
                yValues.append(BarChartDataEntry(x:Double(xAxisRange+1) , y: calorieValue[0].calorie * Double(multiplier)))
            }
            
            if activityValue.count > 0 {
                yValues1.append(BarChartDataEntry(x:Double(xAxisRange+1) , y: -activityValue[0].activeHr))
            }
            xAxisRange += 1
        }
        }
        
        //Set - Calories
        var set : BarChartDataSet? = nil
        set = BarChartDataSet.init(values: yValues, label: "")
        set?.valueFont = UIFont.systemFont(ofSize: 7)
        set?.axisDependency = .left
        set?.colors = [NRColorUtility.healthGreenColor()]
        set?.drawValuesEnabled = false

        if period != .today {
            set?.barRoundingCorners = [.topLeft,.topRight]
        } else {
            set?.barRoundingCorners = [.allCorners]
        }
        
        //Set - Steps
        var set1 : BarChartDataSet? = nil
        set1 = BarChartDataSet.init(values: yValues1, label:"")
        set1?.axisDependency = .left
        set1?.drawValuesEnabled = false
        set1?.colors = [NRColorUtility.progressLabelColor()]
        
        if period != .today {
            set1?.barRoundingCorners = [.bottomRight,.bottomLeft]
        }
        
        var data : BarChartData? = nil
        let dataSets = yValues1.count > 0 ? [set!,set1!] :[set!]
        data = BarChartData.init(dataSets: dataSets)
        data?.barWidth = period == .weekly ? 0.5 : period == .monthly ? 0.4 : 0.7

        chatView.animate(yAxisDuration: 1)
        return data!
    }
    
    func drawTitle() {
        let titleFrame = CGRect(x: 15, y: 15, width: 150, height: 22)
        
        if titleLabel == nil {
            titleLabel = UILabel(frame: titleFrame)
            titleLabel.text = self.period == .today ? "Calories Burned" : "Activity"
            titleLabel.textColor = NRColorUtility.progressLabelColor()
            titleLabel.font = UIFont.systemFont(ofSize: 17)
            self.addSubview(titleLabel)
        } else {
            titleLabel.text = self.period == .today ? "Calories Burned" : "Activity"
        }
        
        //Draw calorie legend
        let calImageFrame = CGRect(x:15 , y: titleFrame.origin.y + titleFrame.height + 25, width: 16, height: 20)
        
        if calImageView == nil {
            calImageView = UIImageView(frame: calImageFrame)
            calImageView.image = UIImage(named: "calories_sm_icon")
            self.addSubview(calImageView)
        }
        if calorieLabel != nil {
            calorieLabel.removeFromSuperview()
            calorieLabel = nil
        }
        
        if calorieLabel == nil {
            calorieLabel = UILabel(frame: CGRect(x: calImageFrame.origin.x + calImageFrame.width + 5, y: titleFrame.origin.y + titleFrame.height + 25, width: 150, height: 22))
            let myString = String(format: "%2.f",activityModel.avgCalorie)
            let myAttribute = [ NSForegroundColorAttributeName: NRColorUtility.progressLabelColor() , NSFontAttributeName : UIFont.systemFont(ofSize: 20, weight: UIFontWeightMedium)]
            let myAttrString = NSMutableAttributedString(string: myString, attributes: myAttribute)
            let labelString = " Avg Cal"
            let labelAttributes = [ NSForegroundColorAttributeName: NRColorUtility.progressLabelColor(), NSFontAttributeName : UIFont.systemFont(ofSize: 14)]
            myAttrString.append(NSAttributedString(string: labelString, attributes: labelAttributes))
            calorieLabel.attributedText = myAttrString
            self.addSubview(calorieLabel)
        }
        
        if self.period != .today {
            //Draw steps legend
            let stepsImageFrame = CGRect(x:UIScreen.main.bounds.width/2  , y: titleFrame.origin.y + titleFrame.height + 25, width: 16, height: 20)
        
            if stepsImageView == nil {
                stepsImageView = UIImageView(frame: stepsImageFrame)
                stepsImageView.image = UIImage(named: "steps_sm_icon")
                self.addSubview(stepsImageView)
            } else {
                self.stepsImageView.isHidden = false
            }
            
            if stepsLabel != nil {
                stepsLabel.removeFromSuperview()
                stepsLabel = nil
            }
            
            if stepsLabel == nil {
                stepsLabel = UILabel(frame: CGRect(x: stepsImageFrame.origin.x + stepsImageFrame.width + 5, y: titleFrame.origin.y + titleFrame.height + 25, width: 150, height: 22))
                let myString = String(format: "%2.f",activityModel.avgSteps)
                let myAttribute = [ NSForegroundColorAttributeName: NRColorUtility.progressLabelColor() , NSFontAttributeName : UIFont.systemFont(ofSize: 20, weight: UIFontWeightMedium)]
                let myAttrString = NSMutableAttributedString(string: myString, attributes: myAttribute)
                let labelString = " Avg Steps"
                let labelAttributes = [ NSForegroundColorAttributeName: NRColorUtility.progressLabelColor(), NSFontAttributeName : UIFont.systemFont(ofSize: 14)]
                myAttrString.append(NSAttributedString(string: labelString, attributes: labelAttributes))
                stepsLabel.attributedText = myAttrString
                self.addSubview(stepsLabel)
            }
        } else {
            if self.stepsLabel != nil {
                self.stepsLabel.isHidden = true
            }
            
            if self.stepsImageView != nil {
                self.stepsImageView.isHidden = true
            }
        }
    }
    
    func setUpDataSetForOccassion(period : period, data:BarChartData, set:BarChartDataSet) {
        if (period == .today) {
            set.colors = [NRColorUtility.healthGreenColor()]
        } else {
            set.barRoundingCorners = [.topLeft,.topRight]
            data.barWidth = period == .weekly ? 0.65 : 0.4
            set.colors = [NRColorUtility.progressLabelColor(),NRColorUtility.healthGreenColor()]
        }
    }
    
    // MARK: IAxisValueFormatter Implementation
    func stringForValue(_ value: Double, axis: AxisBase?) -> String {
        
        let currentAxisBase = self.chatView.xAxis as AxisBase
        if currentAxisBase == axis {
        let index = Int(value)
        if period == .today {
            if  value == 0 {
                return "12A"
            } else if Int(value) == 12 {
                return "12P"
            } else if Int(value) == kDailyCount {
                return "12A"
            } else {
                return ""
            }
        } else {
        if index <  Int(activityModel.caloriesArray.count) {
             var rangeString = String(format:"           %@",(activityModel?.caloriesArray[Int(value)].caloriePeriodRange)!)
            let periodRange = rangeString.components(separatedBy: "-")
            if periodRange.count > 1 {
                rangeString = String(format:"             %@-\n                   %@",periodRange[0],periodRange[1])
            }
            return rangeString
        }
            
        return ""
            }
        } else {
            var axisValue : String = ""
                if period == .today {
                    return String(format: "%0.f", value)
                } else {
                    if value == 0.0 {
                        axisValue = "0"
                    } else if value < 0.0 {
                        axisValue = String(format:"%0.f",value * -1)
                    } else {
                        let multiplier : Int = period == .today ? kDailyMultiplier : kWeeklyMonthlyMultiplier
                        axisValue = String(format:"%0.f",value / Double(multiplier))
                    }
                    return axisValue
            }
        }
    }
}

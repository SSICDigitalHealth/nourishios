//
//  NutritionalWeeklyMonthlyTableViewCell.swift
//  Nourish
//
//  Created by Vlad Birukov on 16.08.17.
//  Copyright © 2017 SSIC. All rights reserved.
//

import UIKit

class NutritionalWeeklyMonthlyTableViewCell: UITableViewCell {
    @IBOutlet weak var grainsView: UIView!
    @IBOutlet weak var progressBarChart: ProgressBarChart!
    @IBOutlet weak var captionLabel: UILabel!
    @IBOutlet weak var nameFood: UILabel!
    @IBOutlet weak var grainsViewHeight: NSLayoutConstraint!
    
    let discriptionAttr: [String: Any] = [NSFontAttributeName : UIFont(name: "Roboto-Regular", size: 14)!, NSForegroundColorAttributeName : NRColorUtility.hexStringToUIColor(hex: "808080")]
    let boldDiscriptionAttr: [String: Any] = [NSFontAttributeName: UIFont(name: "Campton-Bold", size: 14)!, NSForegroundColorAttributeName: NRColorUtility.progressTableBlackColor()]


    let presenter = NutritionalWeeklyMonthlyPresenter()
    
    override func awakeFromNib() {
        super.awakeFromNib()
        presenter.progressBarChart = self.progressBarChart
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func setupWithModel(model:NutritionBalanceViewModel, indexPath :IndexPath, date: (Date, Date)?) {
        
            if model.arrNutritionElement[indexPath.section - 1].type != .grains {
                self.deleteGrainsView()
            } else {
                self.showGrainsView()
            }
    
            if model.arrNutritionElement[indexPath.section - 1].caption != nil {
                
                if let currentDate = date {
                    if self.daysBetweenDates(startDate: currentDate.0, endDate: currentDate.1) > 7 {
                        self.captionLabel.text = String(format: "%@ month:", model.arrNutritionElement[indexPath.section - 1].caption!)
                    } else {
                        self.captionLabel.text = String(format: "%@ week:", model.arrNutritionElement[indexPath.section - 1].caption!)
                    }
                }
            }
            
            if let richNutrition = model.arrNutritionElement[indexPath.section - 1].richNutritionFood {
                let numberMessage = richNutrition.count
                
                let nameFood = NSMutableAttributedString(string:"", attributes: boldDiscriptionAttr)

                
                for i in 0..<numberMessage {
                    if i == numberMessage - 1 {
                        if richNutrition[i].nameElement == "Refined grains" {
                            nameFood.append(NSAttributedString(string: richNutrition[i].nameElement, attributes: boldDiscriptionAttr))
                        } else {
                            nameFood.append(NSAttributedString(string: String(format: " •    %@", richNutrition[i].nameElement), attributes: discriptionAttr))
                        }
                    } else {
                        if richNutrition[i].nameElement == "Whole grains" || richNutrition[i].nameElement == "Refined grains" {
                            nameFood.append(NSAttributedString(string: String(format: "%@", richNutrition[i].nameElement) + "\n", attributes: boldDiscriptionAttr))
                        } else {
                            nameFood.append(NSAttributedString(string: String(format: " •    %@", richNutrition[i].nameElement) + "\n", attributes: discriptionAttr))
                        }
                    }
                }
                
                let style = NSMutableParagraphStyle()
                style.lineSpacing = 8
                nameFood.addAttribute(NSParagraphStyleAttributeName,
                                      value: style,
                                      range: NSMakeRange(0, nameFood.length))
                
                self.nameFood.attributedText = nameFood
            }
            
            self.presenter.setupWith(model: model.arrNutritionElement[indexPath.section - 1])
                    
    }
    
    private func deleteGrainsView() {
        self.grainsViewHeight.constant = 0
        self.grainsView.isHidden = true
        self.layoutIfNeeded()
    }
    
    private func showGrainsView() {
        self.grainsView.isHidden = false
        self.grainsViewHeight.constant = 28
        self.layoutIfNeeded()
    }
    
    private func daysBetweenDates(startDate: Date, endDate: Date) -> Int
    {
        let calendar = Calendar.current
        let components = calendar.dateComponents([.day], from: startDate, to: endDate)
        return components.day!
    }

}

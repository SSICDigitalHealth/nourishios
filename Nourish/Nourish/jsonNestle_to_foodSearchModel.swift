//
//  jsonNestle_to_foodSearchModel.swift
//  Nourish
//
//  Created by Gena Mironchyk on 9/11/17.
//  Copyright © 2017 SSIC. All rights reserved.
//

import UIKit

class jsonNestle_to_foodSearchModel: NSObject {
    func transform(dictionary : [String : Any]) -> FoodSearchCacheModel {
        let model = FoodSearchCacheModel()
        model.amount = dictionary["amount"] as? Double ?? 0.0
        model.displayUnit = dictionary["display_unit"] as? String ?? ""
        model.unit = dictionary["unit"] as? String ?? ""
        model.foodId = dictionary["food_id"] as? String ?? ""
        model.mealIdString = dictionary["food_id"] as? String ?? ""
        model.foodTitle = dictionary["food_name"] as? String ?? ""
        model.grams.value = dictionary["grams"] as? Double
        model.caloriesPerGram = dictionary["kcal_per_gram"] as? Double ?? 0.0
        model.calories = String((dictionary["kcal_per_gram"] as? Double ?? 0.0) * (dictionary["grams"] as? Double ?? 0.0))
        return model
    }
    
    func transform(arrayOfMeals : [[String : Any]]) -> [FoodSearchCacheModel] {
        var arrayToReturn : [FoodSearchCacheModel] = []
        for obj in arrayOfMeals {
            arrayToReturn.append(self.transform(dictionary: obj))
        }
        return arrayToReturn
    }
}

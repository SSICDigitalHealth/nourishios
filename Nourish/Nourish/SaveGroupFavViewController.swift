//
//  SaveGroupFavViewController.swift
//  Nourish
//
//  Created by Vino (Vinodha) Sundaramoorthy on 3/8/17.
//  Copyright © 2017 SSIC. All rights reserved.
//
import UIKit
import RxSwift

class SaveGroupFavViewController: BasePresentationViewController , UITextFieldDelegate{

    @IBOutlet weak var mealNameTextField : UITextField!
    @IBOutlet weak var cancelButton : UIButton!
    @IBOutlet weak var  saveButton: UIButton!
    @IBOutlet weak var baseView : BaseView!
    var mealVC : MealDiaryViewController!

    var groupFavMealArray : [MealRecordModel] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.saveButton.isEnabled = true
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func saveToGroupFavourites(sender : UIButton) {
        let interactor = FavouritesInteractor()
       // self.baseView.prepareLoadView()
        
        let name = interactor.checkName(name: self.mealNameTextField.text!)
        
        if name != self.mealNameTextField.text {
            self.mealVC.showToast(message: String(format : "%@ saved as %@",self.mealNameTextField.text!, name), showingError: false, completion: nil)
        }
        
        let _ = interactor.addToFavourites(models: self.groupFavMealArray, name : self.mealNameTextField.text!).observeOn(MainScheduler.instance).subscribe(onNext: { [unowned self] success in
            print(success)
            
        }, onError: {error in }, onCompleted: {
            self.dismiss(animated: true, completion: {
                if name != self.mealNameTextField.text {
                    self.mealVC.showToast(message: String(format : "%@ saved as %@",self.mealNameTextField.text!, name), showingError: false, completion: nil)
                }
                self.mealVC.tabledView?.presenter.viewWillAppear(false)
            })
            
        }, onDisposed: {})
    }
    
    @IBAction func cancelAction(sender:UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
    
    // MARK : UITextFieldDelegate 
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if (textField.text?.count)! > 0 {
            self.saveButton.isEnabled = true
        }
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        if (textField.text?.count)! > 0 {
            self.saveButton.isEnabled = true
        }
    }
    
    @IBAction func resignKeyboard(sender: AnyObject) {
        let _ = sender.resignFirstResponder()
    }
}

//
//  CaloricBurnInteractor.swift
//  Nourish
//
//  Created by Vlad Birukov on 31.08.17.
//  Copyright © 2017 SSIC. All rights reserved.
//

import Foundation
import RxSwift

class CaloricBurnInteractor {
    
    let repo = NewActivityRepository.shared
   
    /*
    func execute(startDate: Date, endDate: Date) -> Observable <CaloricBurnedModel> {
        return Observable<CaloricBurnedModel>.create{(observer) -> Disposable in
            let pause = Int(arc4random_uniform(4))
            let dispat = DispatchTime.now() + .seconds(pause)
            DispatchQueue.main.asyncAfter(deadline: dispat, execute: {
                let caloricBurnedModel = CaloricBurnedModel()
                if Calendar.current.compare(startDate, to: endDate, toGranularity: .day) == .orderedSame {
                    caloricBurnedModel.userBurnedCal = Double(self.generateRandomBetween(max: 3000, min: 400))
                    caloricBurnedModel.caloricState = [(subtitle: Date, cal: (basalCalories: Double, activeCalories: Double))]()
                    let baselCal = Double(self.generateRandomBetween(max: 150, min: 130))
                    for _ in 0...21 {
                        caloricBurnedModel.caloricState?.append((subtitle: startDate, cal: (basalCalories: baselCal, activeCalories: Double(self.generateRandomBetween(max: 50, min: 0)))))
                    }
                } else {
                    caloricBurnedModel.userBurnedCal = Double(self.generateRandomBetween(max: 3000, min: 400))
                    caloricBurnedModel.avgBurnedCal = Double(self.generateRandomBetween(max: 150, min: 130))
                    caloricBurnedModel.avgActiveCal = Double(self.generateRandomBetween(max: 2000, min: 130))
                }
                observer.onNext(caloricBurnedModel)
                observer.onCompleted()
            })
            return Disposables.create()
        }
    }
    */
    
    func execute(startDate : Date, endDate : Date) -> Observable<CaloricBurnedModel> {
        return self.repo.caloricBurnModel(startDate: startDate, endDate: endDate)
    }
    
    private func generateRandomBetween(max : Int, min : Int) -> Int {
        return min + Int(arc4random_uniform(UInt32(max - min + 1)))
    }
}

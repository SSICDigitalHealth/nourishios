//
//  NRNutritionStatsProtocol.swift
//  Nourish
//
//  Created by Vino (Vinodha) Sundaramoorthy on 12/14/16.
//  Copyright © 2016 SSIC. All rights reserved.
//

import UIKit

protocol NRNutritionStatsProtocol {
    func setUpNutritionStats(model : nutritionStatsModel , period:period)
    func startActivityProgress()
}

//
//  HydrationInteractor.swift
//  Nourish
//
//  Created by Vlad Birukov on 14.06.17.
//  Copyright © 2017 SSIC. All rights reserved.
//

import Foundation
import RxSwift

class HydrationInteractor {
    
    let repository = NestleProgressRepository.shared
    
    func execute(startDate: Date, endDate: Date) -> Observable<HydrationModel> {
        return repository.getProgressFor(startDate: startDate, endDate: endDate, returnHistory: true).flatMap({observer -> Observable<HydrationModel> in
            return Observable.just(self.transform(model: observer.userMicroelement))
        })
    }
    
    private func transform(model: [MicroelementInformation]) -> HydrationModel {
        let hydration = HydrationModel()
        
        let hydrationInformation = model.filter({$0.idNameMicroelement == "Water_total"})
        
        if hydrationInformation.count > 0 {
            hydration.consumedHydration = hydrationInformation[0].consumedMicroelement / hydrationInformation[0].numDays
        
            if hydrationInformation[0].upperLimit != nil {
                if hydrationInformation[0].consumedMicroelement > hydrationInformation[0].lowerLimit! {
                    hydration.targetHydration = hydrationInformation[0].upperLimit!
                } else {
                    hydration.targetHydration = hydrationInformation[0].lowerLimit!
                }
            } else {
                hydration.targetHydration = hydrationInformation[0].lowerLimit!
            }
            hydration.targetHydration = hydration.targetHydration / hydrationInformation[0].numDays
            hydration.unit = "cup"

        }
    
        return hydration
    }
}

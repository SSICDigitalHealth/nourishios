//
//  WeightViewModel.swift
//  Nourish
//
//  Created by Vlad Birukov on 29.08.17.
//  Copyright © 2017 SSIC. All rights reserved.
//

import Foundation

class WeightViewModel {
    var weight: [weightList]?
    var progress: Double?
    var isMetric: Bool = false
    var dailyWeightLogged: Bool = false
    var userId : String?
}

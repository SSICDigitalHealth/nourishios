//
//  BackgroundActivityDumperRepository.swift
//  NourIQ
//
//  Created by Igor on 12/11/17.
//  Copyright © 2017 SSIC. All rights reserved.
//

import UIKit
import HealthKit
import RxSwift

typealias deviceSamples = (type : HKSampleType, device : HKSource)
typealias chunk = (date : Date, samples : [HKSample], devSamples : deviceSamples)
typealias mappedChunk = (date : Date, mappedSamples : [[String : Any]], deviceSamples : deviceSamples)



class BackgroundActivityDumperRepository: NSObject {
    let dataStore = BackgroundActivityDumperDataStore()
    let mapper = HKSample_to_JSONmapper()
    let store = BackgroundActivityDumperDataStore()
    let toModel = MealRecordModel_to_MealRecord()
    let toCacheMapper = MealRecord_to_MealRecordCache()
    
    func startDump() {
        
        if let token = NRUserSession.sharedInstance.accessToken {
            let types = ActivityUtility.dataTypesForDumper()
            var arrayOfObs = [Observable<[HKSampleType: [HKSource]]>]()
            
            for object in types {
                if object.isKind(of: HKSampleType.self) {
                    arrayOfObs.append(self.fetchDevicesFor(type: object as! HKSampleType))
                }
            }
            
            var chunksArray = [Observable<chunk>]()
            for obs in arrayOfObs {
                let chunks = obs.map { dict -> [deviceSamples] in
                    var proxy = [deviceSamples]()
                    for (key, values) in dict {
                        for value in values {
                            let obj : deviceSamples = deviceSamples(type : key, device : value)
                            proxy.append(obj)
                        }
                    }
                    return proxy
                }.flatMap { array -> Observable<chunk> in
                    var dated = [Observable<(Date?, deviceSamples)>]()
                    
                    for object in array {
                        dated.append(self.startDateFor(type: object, token: token))
                    }
                    let nonNillDatedArray = Observable.from(dated).merge().toArray().map { array -> [(Date, deviceSamples)] in
                        return self.transFormToNoNillDates(array: array)
                    }.flatMap { arr -> Observable<chunk> in
                        var chunks = [Observable<chunk>]()
                        
                        for obj in arr {
                            chunks.append(self.fetchSamplesFor(obj: obj))
                        }
                        
                        return Observable.from(chunks).concat()
                    }
                    return nonNillDatedArray
                }
                chunksArray.append(chunks)
            }
            let scheduler = ConcurrentDispatchQueueScheduler(qos: .background)

            let _ = Observable.from(chunksArray).concat().flatMap { chunk -> Observable<chunkFlag> in
                let mappedChunk = self.mapChunkIntoMapped(chunk: chunk)
                
                return self.store.uploadChunkData(chunk: mappedChunk, token: token)
            }.subscribeOn(scheduler).subscribe(onNext: {success in
                
            }, onError: {error in
                print(error)
            }, onCompleted: {}, onDisposed: {})
            
        }
    }
    
    private func mapChunkIntoMapped(chunk : chunk) -> mappedChunk {
        let mappedSamples = self.mapper.transform(samples: chunk.samples)
        
        let chk : mappedChunk = mappedChunk(date : chunk.date, mappedSamples : mappedSamples, deviceSamples : chunk.devSamples)
        return chk
    }
    
    
    private func fetchDevicesFor(type : HKSampleType) -> Observable<[HKSampleType: [HKSource]]> {
        return Observable.create { observer in
            let _ = ActivityUtility.fetchDevicesForType(type: type, completion: {sources, error in
                if error != nil {
                    observer.onError(error!)
                } else {
                    observer.onNext([type : sources])
                    observer.onCompleted()
                }
            })
            return Disposables.create()
        }
    }
    
    private func startDateFor(type : deviceSamples, token : String) -> Observable<(Date?, deviceSamples)> {
        let object = self.store.getLastDateFor(dataType: self.dataTypeFrom(type: type.type), dataSource: self.sourceIdentFrom(device: type.device), token: token).flatMap { date -> Observable<(Date?, deviceSamples)> in
            if date != nil {
                return Observable.just((date, type))
            } else {
                return self.localStartDateFor(type: type)
            }
        }
        return object
    }

    private func localStartDateFor(type : deviceSamples) -> Observable<(Date?, deviceSamples)> {
        return Observable.create { observer in
            
            let _ = ActivityUtility.fetchFirstActivityDateFor(type: type.type, device: type.device, completion: {date, error in
                if error != nil {
                    observer.onError(error!)
                } else {
                    observer.onNext((date, type))
                    observer.onCompleted()
                }
            })
            
            return Disposables.create()
        }
    }

    
    private func dataTypeFrom(type : HKSampleType) -> String {
        return type.description
    }
    
    private func sourceIdentFrom(device : HKSource) -> String {
        return String(device.bundleIdentifier.hashValue)
    }
    
    private func transFormToNoNillDates(array : [(Date?, deviceSamples)]) -> [(Date, deviceSamples)] {
        let filtered = array.filter {$0.0 != nil}
        var returnValue : [(Date, deviceSamples)] = []
        
        for object in filtered {
            let new : (Date, deviceSamples) = (object.0!, object.1)
            returnValue.append(new)
        }
        
        return returnValue
    }
    
    private func fetchSamplesFor(obj : (Date, deviceSamples)) -> Observable<chunk> {
        return Observable.create { observer in
            let calendar = Calendar.current
            let yesterday = calendar.date(byAdding: .day, value: -1, to: Date())
            let yesterdayMidnight = calendar.startOfDay(for: yesterday ?? Date())
            
            var endDate = Calendar.current.date(byAdding: .day, value: 15, to: obj.0) ?? calendar.startOfDay(for: Date())
            endDate = endDate < yesterdayMidnight ? endDate : yesterdayMidnight
            
            ActivityUtility.getSamplesForSource(source: obj.1.device, quantityType: obj.1.type, startDate: obj.0, endDate: endDate, completion: {fetchedSamples, error in
                var resultSamples = [HKSample]()
                resultSamples.append(contentsOf: fetchedSamples)
                
                if resultSamples.count == 0 { //scan for next item
                    ActivityUtility.scanForNextSampleFor(type: obj.1.type, device: obj.1.device, startDate: obj.0, completion: { singleSample, error in
                        
                        if singleSample != nil {
                            resultSamples.append(singleSample!)
                        }
                        
                        if let chunk = self.chunkFromSamples(array: resultSamples, devSample: obj.1) {
                            observer.onNext(chunk)
                            observer.onCompleted()
                        }
                    })
                } else {
                    if let chunk = self.chunkFromSamples(array: resultSamples, devSample: obj.1) {
                        observer.onNext(chunk)
                        observer.onCompleted()
                    }
                }
            })
            
            return Disposables.create()
        }
    }
    
    private func chunkFromSamples(array : [HKSample], devSample : deviceSamples) -> chunk? {
        if let lastSample = array.last {
            let chunk = (lastSample.endDate, array, devSample)
            return chunk
        }
        return nil
    }
    
    func storeGroupModel(model : groudFoodDetailsModel) {
        let store = BackgroundActivityDumperDataStore()
        let metaRepo = MetaDataRepository()
        
        var array : [Observable<[String : Any]>] = []
        var grams = 0.0
        var calories = 0.0
        
        for object in model.groupMealArray {
           let record = self.toCacheMapper.transform(record: self.toModel.transform(mealRecord: object))
            grams += record.grams
            calories += record.calories
            array.append(record.dhMealRepresentation())
        }
        let singleObs = Observable.from(array).merge()
        let sequence = singleObs.toArray()
        
        let meta = metaRepo.getDHMetaDataFor(dhSchema: .jSON, dhType: .food)
        
        let _ = Observable.zip(meta, sequence, resultSelector: {metaData, foodArray -> [String : Any] in
            var totalDict = [String : Any]()
            
            var subdict = [String : Any]()
            subdict["Calories"] = calories
            subdict["Size"] = String(grams)
            subdict["Food"] = model.groupTitle
            subdict["OptionalData"] = self.prettyPrint(with: foodArray) 
            
            totalDict["Data"] = subdict
            totalDict["Meta"] = metaData
            return totalDict
        }).flatMap {result -> Observable<Bool> in
            return store.storeDict(dict: result, userID: NRUserSession.sharedInstance.userID!, type: .food, scheme: .jSON, dataSet: UUID().uuidString.replacingOccurrences(of: "-", with: "").lowercased(), token: NRUserSession.sharedInstance.accessToken!)
        }.subscribe(onNext: {dhUpload in
            print("dhupload success \(dhUpload)")
        }, onError: {error in }, onCompleted: {}, onDisposed: {})
        
        
    }
    
    private func prettyPrint(with json: [Any]) -> String{
        let data = try! JSONSerialization.data(withJSONObject: json, options: .prettyPrinted)
        let string = NSString(data: data, encoding: String.Encoding.utf8.rawValue)
        return string as! String
    }
}


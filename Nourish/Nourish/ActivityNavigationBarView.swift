//
//  ActivityNavigationBarView.swift
//  Nourish
//
//  Created by Andrey Zmushko on 8/23/17.
//  Copyright © 2017 SSIC. All rights reserved.
//

import UIKit

class ActivityNavigationBarView: BaseView {
    
    @IBOutlet weak var segmentedControl: NRSegmentedControl!
    @IBOutlet weak var title: UILabel!
    
    override func viewDidLoad() {
        self.xibAutoLayouting(nibName: "ActivityNavigationBarView")

        self.setupActivitySegmentedControl()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.stopActivityAnimation()
        
    }

    private func setupActivitySegmentedControl() {
        self.segmentedControl.bounds = CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: 47.0)
        self.segmentedControl.titles = ["DAILY", "WEEKLY", "MONTHLY"]
        self.segmentedControl.appearance = segmentAppearance(backgroundColor: UIColor.white,
                                                                     selectedBackgroundColor: UIColor.white,
                                                                     textColor: NRColorUtility.hexStringToUIColor(hex: "808080"),
                                                                     font: UIFont(name: "Campton-Book", size: 15)!,
                                                                     selectedTextColor: NRColorUtility.appBackgroundColor(),
                                                                     selectedFont: UIFont(name: "Campton-Bold", size: 15)!,
                                                                     bottomLineColor: UIColor.white,
                                                                     selectorColor: NRColorUtility.appBackgroundColor(),
                                                                     bottomLineHeight: 0.0,
                                                                     selectorHeight: 2.0,
                                                                     labelTopPadding: 0.0, hasImage: false, imageSize: nil)
    }
}

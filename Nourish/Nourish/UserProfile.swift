//
//  NRUserProfileObject.swift
//  Nourish
//
//  Created by Nova on 11/15/16.
//  Copyright © 2016 SSIC. All rights reserved.
//

import UIKit

let kSecondsInDay = 86400.0
let kCalorieBudgetKey = "calorieBudgetToUse"

class UserProfile : NSObject {
    var userID : String? = ""
    var userFirstName : String? = ""
    var userLastName : String? = ""
    var activeSinceTimeStamp : Date? = nil
    var height : Double? = 0
    var weight : Double? = 0
    var age : Int? = 0
    var activityLevel : ActivityLevel? = nil
    var dietaryPreference : DietaryPreference? = nil
    var metricPreference : MetricPreference? = nil
    var biologicalSex : BiologicalSex? = nil
    var goalPrefsActivity : Float? = 0
    var goalPrefsFood : Float? = 0
    var goalPrefsLifestyle : Float? = 0
    var userGoal : UserGoal?
    var startWeight : Double?
    var goalValue : Double?
    var weightLossStartDate : Date?
    var email : String?
    var weightToMaintain : Double? = nil
    var needToInformUser : Bool = false
    
    var optifastOcasionsList = [Ocasion]()
    var optifastFoodIds = [String]()
    
    func calculateAndStoreCaloriePlan() -> Double {
        if self.height != nil && self.weight != nil {
            let bmr = self.getBMRCalories(weight: self.weight!, height: self.height!, date: Date(), isCurrentDay: false)
            let calorieNeed = self.getWeightKeepCalorieBudget(bmrValue: bmr)
            let bmrDiff = calorieNeed - 1000
            var calorieBudget = 0.0
        
            if self.userGoal == .LooseWeight {
                if bmrDiff < bmr {
                    calorieBudget = bmr
                } else {
                    calorieBudget = bmrDiff
                }
            } else {
                calorieBudget = calorieNeed
            }
        defaults.set(calorieBudget, forKey: kCalorieBudgetKey)
        defaults.synchronize()
            
        return calorieBudget
        
        }
        
        return 0
    }
    
    func getWeeklyWeightLoss () -> Double {
        
        var weightValueToUse = 65.0
        var heightValueToUse = 1.65
        
        if self.weight != nil {
            weightValueToUse = self.weight!
        }
        
        if self.height != nil {
            heightValueToUse = self.height!
        }
        
        let bmr = self.getBMRCalories(weight: weightValueToUse, height: heightValueToUse, date: Date(), isCurrentDay: false)
        
        let calorieNeed = self.getWeightKeepCalorieBudget(bmrValue: bmr)
        
        let bmrDiff = calorieNeed - 1000
        var calorieBudget = 0.0
        
        if bmrDiff < bmr {
            calorieBudget = bmr
        } else {
            calorieBudget = bmrDiff
        }
        LogUtility.logToFile("Calorie budget \(calorieBudget)")
        
        let weeklyWeightLoss = bmrDiff * 2 / bmr
        
        return weeklyWeightLoss
    }
    
    
    func getActiveCalorieMinimum() -> Double {
        let mDict : [ActivityLevel : Double] = [ActivityLevel.Sedentary : 50.0, ActivityLevel.Moderate : 160.0, ActivityLevel.Active : 580.0, ActivityLevel.VeryActive : 1400.0]
        let wDict =  [ActivityLevel.Sedentary : 50.0, ActivityLevel.Moderate : 130.0, ActivityLevel.Active : 470.0, ActivityLevel.VeryActive : 1150.0]
        
        if self.activityLevel != nil {
            if self.biologicalSex != nil {
                if self.biologicalSex == .Male {
                    return mDict[self.activityLevel!]!
                } else {
                    return wDict[self.activityLevel!]!
                }
            } else {
                return mDict[self.activityLevel!]!
            }
            
        } else {
            return 0.0
        }
    }
    
    func getBMRCalories(weight : Double, height : Double, date : Date, isCurrentDay : Bool) -> Double {
        var bmr = 0.0
        var weightValue = 65.0
        var heightValue = 165.0
        
        if weight > 0.0 {
            weightValue = weight
        } else if self.weight != nil {
            weightValue = self.weight!
        }
        
        if height > 0.0 {
            heightValue = height
        } else if self.height != nil {
            heightValue = self.height!
        }
        
        //let isCurrentDay = Calendar.current.startOfDay(for: date) == Calendar.current.startOfDay(for: Date())
        
        var age = 0
        
        if self.age != nil {
            age = self.age!
        }
        
        if self.biologicalSex == .Male {
            let weightProxy = (13.397 * weightValue)
            let heightProxy = (4.799 * heightValue * 100)
            bmr = (88.362 + weightProxy + heightProxy - (5.677 * Double(age)))
        } else {
            let weightProxy = (9.247 * weight)
            let heightProxy = (3.098 * heightValue * 100)
            bmr = (447.593 + weightProxy + heightProxy - (4.330 * Double(age)))
        }
        
        return isCurrentDay == true ? bmr * self.percentageOfDay(date: date) : bmr
       
    }
    
    func getWeightKeepCalorieBudget(bmrValue : Double) -> Double {
        let activityLvl = self.activityLevel ?? .Moderate
        switch activityLvl {
            case .Sedentary:
                return bmrValue * 1.25
            case .Moderate:
                return bmrValue * 1.5
            case .Active:
                return bmrValue * 1.75
            case .VeryActive:
                return bmrValue * 2.2
            }
    }
    
    func percentageOfDay(date : Date) -> Double {
        if Calendar.current.startOfDay(for: date) == Calendar.current.startOfDay(for: Date()) {
            let start = Calendar.current.startOfDay(for: date)
            let inteval = date.timeIntervalSince(start)
            
            return inteval / kSecondsInDay
 
        } else {
            return 1.0
        }
    }
    
}

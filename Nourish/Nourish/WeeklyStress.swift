//
//  WeeklyStress.swift
//  Nourish
//
//  Created by Gena Mironchyk on 1/19/17.
//  Copyright © 2017 SSIC. All rights reserved.
//

import UIKit

struct weeklyStruct {
    var dateToShow : Date
    var hoursArray : [hoursStruct]?
}

struct hoursStruct {
    var index : Int
    var stressValue : stressLevel?
}

class WeeklyStress: NSObject{
    var avgBPM : Double?
    var maxBPM : Double?
    var minBPM : Double?
    var weeklyArray : [weeklyStruct]?
}

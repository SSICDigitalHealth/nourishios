//
//  ImageRealmDataStore.swift
//  Nourish
//
//  Created by Nova on 5/12/17.
//  Copyright © 2017 SSIC. All rights reserved.
//

import UIKit
import RxSwift
import RealmSwift


class ImageRealmDataStore: NSObject {
    
    func store(image : UIImage, photoID : String) {
        if photoID.count > 0 {
            let obj = FoodImageObject()
            obj.userPhotoID = photoID
            let path = self.saveImageToDisk(image: image, photoID: photoID)
            if path != nil {
                obj.imagePath = path!
                let realm = try! Realm()
                try! realm.write {
                    realm.add(obj)
                }
            }
        }
    }
    
    func store(image : UIImage) -> String {
        let obj = FoodImageObject()
        let cacheID = obj.localCacheID
        let path = self.saveImageToDisk(image: image, photoID: cacheID)
        if path != nil {
            obj.imagePath = path
            let realm = try! Realm()
            try! realm.write {
                realm.add(obj)
            }
        }
        return cacheID
    }
    
    func getImage(ident : String) -> UIImage? {
        if ident.hasPrefix("local") {
            return self.getImage(localCacheID:ident)
        } else {
            return self.getImage(photoID:ident)
        }
    }
    
    func getImage(localCacheID : String) -> UIImage? {
        let predicate = NSPredicate(format : "localCacheID == %@",localCacheID)
        let results = try! Realm().objects(FoodImageObject.self).filter(predicate)
        let obj = results.last
        if obj?.imagePath != nil {
            return self.readImageFromDisk(path: (obj?.imagePath)!)
        } else {
            return nil
        }
    }
    
    func getImageObject(ident : String) -> FoodImageObject? {
        var predicate = NSPredicate(format : "localCacheID == %@",ident)
        
        if !ident.hasPrefix("local") {
            predicate = NSPredicate(format : "userPhotoID == %@",ident)
        }
        let results = try! Realm().objects(FoodImageObject.self).filter(predicate)
        
        return results.first
    }
    
    func getImage(photoID : String) -> UIImage? {
        let predicate = NSPredicate(format : "userPhotoID == %@",photoID)
        let results = try! Realm().objects(FoodImageObject.self).filter(predicate)
        let obj = results.last
        if obj?.imagePath != nil {
            return self.readImageFromDisk(path: (obj?.imagePath)!)
        } else {
            return nil
        }
    }
    
    func saveImageToDisk(image : UIImage, photoID : String) -> String? {
        do {
            let uuid = "/mealPhotos/" + photoID
            let fileDirectory = self.getDocumentsDirectory().path + "/mealPhotos/"
            let fileURL = self.getDocumentsDirectory().appendingPathComponent(uuid)
            
            do {
                try FileManager.default.createDirectory(atPath: fileDirectory, withIntermediateDirectories: false, attributes: nil)
            } catch let error as NSError {
                print(error.localizedDescription);
            }
            
            if let pngImageData = UIImageJPEGRepresentation(image, 0.8) {
                try pngImageData.write(to: URL(fileURLWithPath: fileURL.path), options: .atomic)
            }
            return uuid
        } catch let error {
            LogUtility.logToFile("Error saving photo %@", error)
            return nil
        }
    }
    
    func readImageFromDisk(path : String) -> UIImage? {
        let path = self.getDocumentsDirectory().appendingPathComponent(path)
        if FileManager.default.fileExists(atPath: path.path) {
            return UIImage(contentsOfFile: path.path)
        } else {
            return nil
        }
    }
    
    func getDocumentsDirectory() -> URL {
        return try! FileManager.default.url(for: .documentDirectory, in: .userDomainMask, appropriateFor: nil, create: true)
    }
    
}

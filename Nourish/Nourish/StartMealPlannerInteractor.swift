//
//  StartMealPlannerInteractor.swift
//  Nourish
//
//  Created by Vlad Birukov on 18.08.17.
//  Copyright © 2017 SSIC. All rights reserved.
//

import Foundation
import RxSwift

class StartMealPlannerInteractor {
    private let repository = UserRepository.shared
    private let optifastRepo = OptifastSettingsRepository()
    
    func execute() -> Observable<UserProfile> {
        let getCurrentUser = self.repository.getCurrentUser(policy: .DefaultPolicy)
        let optifastModel = self.optifastRepo.getOptifastSettingsModel()
        
        let zip = Observable.zip(getCurrentUser, optifastModel, resultSelector: {user, optifast -> UserProfile in
            let userModel = user
            
            if optifast.optifastUserProducts.count > 0 {
                userModel.optifastFoodIds = optifast.optifastUserProducts.map({return $0.foodTitle})
            }
          
            return userModel
        })
        
        return zip
    }
}

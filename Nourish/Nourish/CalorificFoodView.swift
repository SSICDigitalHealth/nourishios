//
//  CalorificFoodView.swift
//  Nourish
//
//  Created by Vlad Birukov on 09.08.17.
//  Copyright © 2017 SSIC. All rights reserved.
//

import UIKit

import UIKit

protocol CalorificFoodProtocol : BaseViewProtocol{
    func reloadTable()
    func registerTableViewCells()
    func config() -> ProgressConfig?

}

class CalorificFoodView: BaseView, CalorificFoodProtocol {
    
    @IBOutlet weak var caption: UILabel!
    @IBOutlet  var presenter: CalorificFoodPresenter!
    @IBOutlet weak var calorificFoodTableView: UITableView!
    
    let labelString = "Most calorific food this"
    var progressConfig: ProgressConfig?
    
    func config() -> ProgressConfig? {
        return self.progressConfig
    }
    
    override func viewDidLoad() {
        self.xibAutoLayouting(nibName: "CalorificFoodView")
        settingTableCell()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.basePresenter = presenter
        self.presenter.calorificFoodView = self
        registerTableViewCells()
        setCaption()
        super.viewWillAppear(animated)
    }
    
    func stopAnimation() {
        self.stopActivityAnimation()
    }
    
    
    func reloadTable() {
        calorificFoodTableView.reloadData()
    }
    
    func registerTableViewCells() {
        self.calorificFoodTableView?.register(UINib(nibName: "CalorificFoodOfWeekTableViewCell", bundle: nil), forCellReuseIdentifier: "cellCalorificFood")
    }
    
    func settingTableCell() {
        calorificFoodTableView.rowHeight = UITableViewAutomaticDimension
        calorificFoodTableView.estimatedRowHeight = 30
    }
    
    func setCaption() {
        if let config = progressConfig {
            switch config.period {
            case .daily:
                caption.text = String(format: "%@ day", labelString)
            case .monthly:
                caption.text = String(format: "%@ month", labelString)
            case .weekly:
                caption.text = String(format: "%@ week", labelString)
            }
        }
    }
}


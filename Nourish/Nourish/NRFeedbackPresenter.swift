//
//  NRFeedbackPresenter.swift
//  Nourish
//
//  Created by Vino (Vinodha) Sundaramoorthy on 11/29/16.
//  Copyright © 2016 SSIC. All rights reserved.
//

import UIKit
import RxSwift

class NRFeedbackPresenter: BasePresenter , UITextViewDelegate {
    
    let userInteractor = UserProfileInteractor()
    
    
    // MARK: UITextViewDelegate
    func textViewDidBeginEditing(_ textView: UITextView) {
        if (textView.text == kFeedbackPlaceholderString) {
            textView.text = ""
            textView.textColor = UIColor.black
        }
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        if (textView.text.count == 0) {
            textView.text = kFeedbackPlaceholderString
            textView.textColor = UIColor.gray
        }
    }
    
    func textViewShouldEndEditing(_ textView: UITextView) -> Bool {
        textView.resignFirstResponder()
        return true
    }
    
    func storeFeedback(message : String, log : String) -> Observable<Bool> {
        let object = self.userInteractor.storeUserFeedBack(message: message, log : log)
        return object
    }

}

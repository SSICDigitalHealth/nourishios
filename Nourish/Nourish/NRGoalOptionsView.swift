//
//  NRGoalOptionsView.swift
//  Nourish
//
//  Created by Vino (Vinodha) Sundaramoorthy on 11/28/16.
//  Copyright © 2016 SSIC. All rights reserved.
//

import UIKit

class NRGoalOptionsView: BaseView ,NRGoalOptionsProtocol{
   
    @IBOutlet weak var goalOptionsTableView : UITableView!
    var controller = NRGoalOptionViewController()
    var presenter = NRGoalOptionsPresenter()
    
    override func viewWillAppear(_ animated: Bool) {
        goalOptionsTableView?.register(UINib(nibName: "NRGoalOptionsTableViewCell", bundle: nil), forCellReuseIdentifier: "kGoalOptionsCell")
        presenter.goalOptionView = self
        self.basePresenter = presenter
        goalOptionsTableView.delegate = presenter
        goalOptionsTableView.dataSource = presenter
        super.viewWillAppear(animated)
    }
    
    func getGoal() -> UserGoal {
        return self.controller.userGoal
    }

    // MARK: NRGoalOptionsProtocol Implementation
    func loadOptions() {
        DispatchQueue.main.async {
            self.goalOptionsTableView.reloadData()
            self.stopActivityAnimation()
        }
    }
    
    func setUserGoal() {
        self.controller.setUserGoal()
    }
}

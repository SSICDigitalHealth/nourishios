//
//  HeaderUserOcasionTableViewCell.swift
//  Nourish
//
//  Created by Vlad Birukov on 19.06.17.
//  Copyright © 2017 SSIC. All rights reserved.
//

import UIKit

class HeaderUserOcasionTableViewCell: UITableViewCell {

    @IBOutlet weak var nameOcasion: UILabel!
    @IBOutlet weak var separatorLine: UIView!
    @IBOutlet weak var bottomConstraint: NSLayoutConstraint!
    @IBOutlet weak var topNewConstraint: NSLayoutConstraint!
    @IBOutlet weak var topOldConstraint: NSLayoutConstraint!

    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func setupWithModel(model: StateFoodStuffsViewModel) {
        if model.type != nil {
            nameOcasion.text = Ocasion.stringDescription(servingType: model.type!)
        }
        
        if model.arrFoodElement != nil {
            bottomConstraint.constant = 0
        }
    }
    
}

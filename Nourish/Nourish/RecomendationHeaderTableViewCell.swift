//
//  RecomendationHeaderTableViewCell.swift
//  Nourish
//
//  Created by Vlad Birukov on 17.10.2017.
//  Copyright © 2017 SSIC. All rights reserved.
//

import UIKit

class RecomendationHeaderTableViewCell: UITableViewCell {
    @IBOutlet weak var headerLabel: UILabel!
    @IBOutlet weak var ideaImage: UIImageView!
    @IBOutlet weak var lampHolder: UIView!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.lampHolder.layer.cornerRadius = self.lampHolder.bounds.size.width / 2
        self.ideaImage.imageNamedWithTint(named: "idea", tintColor: UIColor.white)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func setupWith(header: String, description: String) {
        let paragraphStyle = NSMutableParagraphStyle()
        paragraphStyle.lineSpacing = 1
        
        let bodyAttr: [String: Any] = [NSFontAttributeName : UIFont.systemFont(ofSize: 12, weight: UIFontWeightRegular), NSForegroundColorAttributeName : NRColorUtility.hexStringToUIColor(hex: "808080"), NSParagraphStyleAttributeName: paragraphStyle]
        
        paragraphStyle.lineSpacing = 4
        
        let headerAttr: [String : Any] = [NSFontAttributeName : UIFont(name: "Campton-Bold", size: 16)!, NSForegroundColorAttributeName : NRColorUtility.hexStringToUIColor(hex: "1172b9"), NSParagraphStyleAttributeName:  paragraphStyle]
        
        let attributedText = NSMutableAttributedString(string: "", attributes: headerAttr)

        if header != "" {
            attributedText.append(NSMutableAttributedString(string: String(format: "%@\n", header), attributes: headerAttr))
        }
        
        if description != "" {
            attributedText.append(NSMutableAttributedString(string: String(format: "%@", description), attributes: bodyAttr))
        }
        
        self.headerLabel.attributedText = attributedText
    }
    
}

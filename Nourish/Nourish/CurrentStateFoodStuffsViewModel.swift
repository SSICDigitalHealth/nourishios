//
//  CurrentStateFoodStuffsViewModel.swift
//  Nourish
//
//  Created by Vlad Birukov on 29.08.17.
//  Copyright © 2017 SSIC. All rights reserved.
//

import Foundation

class CurrentStateFoodStuffsViewModel : NSObject {
    var arrCurentStateFood = [StateFoodStuffsViewModel]()
}

class StateFoodStuffsViewModel {
    var type : Ocasion?
    var arrFoodElement : [FoodElementViewModel]?
}

class FoodElementViewModel {
    var nameFood : String = ""
    var amountFood : Double?
    var unit: String?
    
}

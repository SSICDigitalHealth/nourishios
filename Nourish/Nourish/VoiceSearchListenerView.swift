//
//  VoiceSearchListenerView.swift
//  Nourish
//
//  Created by Vino (Vinodha) Sundaramoorthy on 6/15/17.
//  Copyright © 2017 SSIC. All rights reserved.
//

import UIKit
import AVFoundation

class VoiceSearchListenerView: UIView , VoiceSearchListenerProtocol {

    @IBOutlet weak var voiceSearchStatusLabel : UILabel!
    @IBOutlet weak var voiceTranscritpionLabel : UILabel!
    @IBOutlet weak var listeningAnimationView : PulsatingView!
    @IBOutlet weak var responseButtonView : UIView!
    @IBOutlet weak var listenButton : UIButton!
    
    var presenter = VoiceSearchPresenter()
    var controller : VoiceSearchListenerViewController!
    var buttonArray : [UIButton] = []
    var date : Date!
    var speechManager = TextToSpeechManager.shared

    func viewDidLoad () {
        self.voiceSearchStatusLabel.textColor = NRColorUtility.progressLabelColor()
        self.voiceTranscritpionLabel.textColor = NRColorUtility.progressLabelColor()
        self.voiceTranscritpionLabel.text = ""
        presenter.voiceSearchListenerView = self
        presenter.date = self.date
        self.responseButtonView.isHidden = true
        self.listenButton.addTarget(self, action: #selector(self.startVoiceSearch(sender:)), for: .touchUpInside)
        presenter.viewWillAppear(false)
    }
    
    func showSearchButton()  {
        DispatchQueue.main.async {
            for button in self.responseButtonView.subviews {
                button.removeFromSuperview()
            }
            self.responseButtonView.isHidden = false
            self.setupSearchButton()
        }
    }
    
    func setupSearchButton() {
        let height : CGFloat = 48
        let width : CGFloat = UIScreen.main.bounds.width - 40
        let x : CGFloat = 20
        let y : CGFloat = 50
        
        let mealButton = UIButton(frame: CGRect(x: x, y: y, width: width, height: height))
        mealButton.titleLabel?.font = UIFont.init(name: "Campton-Bold", size: 15)
        mealButton.setTitle("SEARCH", for: .normal)
        mealButton.layer.backgroundColor = NRColorUtility.appBackgroundColor().cgColor
        mealButton.layer.cornerRadius = height / 2
        mealButton.layer.masksToBounds = true
        mealButton.addTarget(self, action: #selector(self.searchButtonAction(sender:)), for: .touchUpInside)
        
        let transition = CATransition()
        transition.duration = 0.5
        transition.type = kCATransitionMoveIn
        transition.subtype = x == 16 ? kCATransitionFromLeft :  kCATransitionFromRight
        transition.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionEaseInEaseOut)
        mealButton.layer.add(transition, forKey: "")
        self.buttonArray.append(mealButton)
        self.responseButtonView.addSubview(mealButton)
    }
    
    func clearSearchButton() {
        DispatchQueue.main.async {
            for button in self.responseButtonView.subviews {
                button.removeFromSuperview()
            }
        }
    }
        
    func showOcassionButtons() {
        DispatchQueue.main.async {
            for button in self.responseButtonView.subviews {
                button.removeFromSuperview()
            }
            self.responseButtonView.isHidden = false
            self.setupOcassionButtons()
        }
    }

    func setupOcassionButtons() {
        let height : CGFloat = 48
        let width : CGFloat = UIScreen.main.bounds.width/2 - (3 * 16)
        var x : CGFloat = 16
        var y : CGFloat = 25
        
        let ocassionArray = ["BREAKFAST","LUNCH","DINNER","SNACK"]
        
        for ocassion in ocassionArray {
            let mealButton = UIButton(frame: CGRect(x: x, y: y, width: width, height: height))
            mealButton.titleLabel?.font = UIFont.init(name: "Campton-Bold", size: 15)
            mealButton.setTitle(ocassion, for: .normal)
            mealButton.layer.backgroundColor = NRColorUtility.appBackgroundColor().cgColor
            mealButton.layer.cornerRadius = height / 2
            mealButton.layer.masksToBounds = true
            mealButton.addTarget(self, action: #selector(self.selectOcassion(sender:)), for: .touchUpInside)

            let transition = CATransition()
            transition.duration = 0.5
            transition.type = kCATransitionMoveIn
            transition.subtype = x == 16 ? kCATransitionFromLeft :  kCATransitionFromRight
            transition.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionEaseInEaseOut)
            mealButton.layer.add(transition, forKey: "")
            self.buttonArray.append(mealButton)
            self.responseButtonView.addSubview(mealButton)
            
            // Move x & y
            x =  UIScreen.main.bounds.width/2 + 16
            
            if ocassion == "LUNCH" {
                y = 25 + height + 16
                x = 16
            }
        }
    }
    
    // MARK : VoiceSearchListenerProtocol Implementation
    
    func startListening() {
        DispatchQueue.main.async {
            self.voiceSearchStatusLabel.text = "Listening..."
            self.voiceSearchStatusLabel.textColor = NRColorUtility.progressLabelColor()
            self.listeningAnimationView.startAnimation()
            self.listenButton.setImage(UIImage(named:"mic_button_cancel"), for: .normal)
        }
    }
    
    func stopListenining() {
        DispatchQueue.main.async {
            self.listeningAnimationView.stopListeningAnimation()
            self.listenButton.setImage(UIImage(named:"mic_button"), for: .normal)
        }
    }
    
    func speechInProgress(spokenResponse:String) {
        DispatchQueue.main.async {
            self.listeningAnimationView.stopListeningAnimation()
            self.listenButton.setImage(UIImage(named:"mic_button"), for: .normal)
            self.voiceSearchStatusLabel.text = spokenResponse
            self.voiceTranscritpionLabel.text = kTapMicrophoneMessage
            if self.presenter.mealRecordArray.count > 0 && self.presenter.mealOcassion == nil {
                self.showOcassionButtons()
            } else if self.presenter.mealRecordArray.count == 0 {
                Hound.clearConversationState()
                self.showSearchButton()
            }
        }
    }
    
    func updateSpeechTranscript(text : String) {
        DispatchQueue.main.async {
            print("Transcript:\(text)")
            self.voiceTranscritpionLabel.text = text
        }
    }

    func updateVoiceSearchStatus(text:String) {
        DispatchQueue.main.async {
            self.voiceSearchStatusLabel.text = text
        }
    }
    
    // MARK : Button action methods
    func startVoiceSearch(sender:UIButton) {
        
        if self.presenter.unrecognizedWords.count > 0 {
            self.clearSearchButton()
        }
        
        if HoundifyManager().getVoiceSearchStatus() == "Ready" {
            self.presenter.startVoiceSearch(requestInfo: [:])
            self.speechManager.stop() //If speech is in progress stop it
            self.startListening()
        } else if HoundifyManager().getVoiceSearchStatus() == "Not Ready" {
            self.presenter.needReload = true
            self.speechManager.stop() //If speech is in progress stop it
            HoundifyManager().startListening()
        }
        else if HoundifyManager().getVoiceSearchStatus() == "Recording" {
            DispatchQueue.main.async {
                self.voiceSearchStatusLabel.text = kTapMicrophoneMessage
            }
            self.presenter.stopListening()
        }
    }
    
    func searchButtonAction(sender:UIButton) {
        self.speechManager.stop()
        let mealOcassion = self.presenter.mealOcassion != nil ? self.presenter.mealOcassion : self.occasionFromDate(date: Date())
        Hound.clearConversationState()
        HoundVoiceSearch.instance().stopListening(completionHandler: { (error) in
            self.controller.showSearchVC(date:self.presenter.date,ocassion:mealOcassion!)
        })
    }
    
    private func occasionFromDate(date : Date) -> Ocasion {
        let hour = Double(Calendar.current.component(.hour, from: date)) + Double(Calendar.current.component(.minute, from: date))/60.0
        if hour >= 5 && hour <= 10.5 {
            return .breakfast
        } else if hour >= 11 && hour <= 13.5 {
            return .lunch
        } else if hour >= 17 && hour <= 21.5 {
            return .dinner
        } else {
            return .snacks
        }
    }

    func selectOcassion(sender:UIButton) {
        let title = sender.currentTitle?.lowercased()
        self.presenter.mealOcassion = Ocasion.enumFrom(stringRep: title!)
        
        for meal in self.presenter.mealRecordArray {
            meal.ocasion = self.presenter.mealOcassion
        }
        
        self.showMealListVC(modelArray: self.presenter.mealRecordArray, date: Date(), ocasion: self.presenter.mealOcassion)
    }

    func showMealListVC(modelArray:[MealRecord],date:Date,ocasion:Ocasion) {
        self.controller.showMealListVC(modelArray: modelArray, date: date,ocassion:ocasion)
    }
    
    func dismiss() {
        self.speechManager.stop() //If speech is in progress stop it
        self.controller.closeAction(UIButton())
    }
}

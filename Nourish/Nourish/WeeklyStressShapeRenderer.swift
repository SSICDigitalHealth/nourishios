//
//  WeeklyStressShapeRenderer.swift
//  Nourish
//
//  Created by Gena Mironchyk on 2/1/17.
//  Copyright © 2017 SSIC. All rights reserved.
//

import Foundation
import CoreGraphics
import UIKit

open class WeeklyStressShapeRenderer : NSObject, IShapeRenderer
{
    open func renderShape(
        context: CGContext,
        dataSet: IScatterChartDataSet,
        viewPortHandler: ViewPortHandler,
        point: CGPoint,
        color: NSUIColor)
    {
        let shapeSize = dataSet.scatterShapeSize
        let shapeHalf = shapeSize / 2.0
        let shapeSixth = shapeSize / 6.0
        
        context.setFillColor(color.cgColor)
        var rect = CGRect()
        rect.origin.x = point.x - shapeHalf
        rect.origin.y = point.y - shapeSixth/2.0
        rect.size.width = shapeSize
        rect.size.height = shapeSixth
        let path = UIBezierPath(roundedRect: rect, cornerRadius: 2.0)
        context.addPath(path.cgPath)
        context.setStrokeColor(UIColor.clear.cgColor)
        context.drawPath(using: CGPathDrawingMode.fillStroke)
        
        context.fill(rect)
        
    }
}

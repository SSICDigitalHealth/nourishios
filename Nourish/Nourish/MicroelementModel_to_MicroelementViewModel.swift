//
//  MicroelementModel_to_MicroelementViewModel.swift
//  Nourish
//
//  Created by Vlad Birukov on 30.08.17.
//  Copyright © 2017 SSIC. All rights reserved.
//

import Foundation

class MicroelementModel_to_MicroelementViewModel {
    func transform(model: MicroelementModel) -> MicroelementViewModel {
        let microelementViewModel = MicroelementViewModel()
        if model.data != nil {
            microelementViewModel.data = [microelementViewList]()
            var mass = [microelementViewList]()
            for microelement in model.data! {
                if microelement.date != nil {
                    let variable : microelementViewList = microelementViewList(date: microelement.date, elements: self.transform(model: microelement.elements))
                    mass.append(variable)
                }
            }
            
            if model.detailInformation != nil {
                microelementViewModel.detailInformation = [DetailInformationViewModel]()
                for item in model.detailInformation! {
                    microelementViewModel.detailInformation?.append(self.transform(model: item))
                }
            }
            
            microelementViewModel.data = mass
        }
        return microelementViewModel
    }
    
    private func transform(model: [(type: MicroelementType, element: UserMicroelement)]) -> [(type: MicroelementType, element: UserMicroelementViewModel)] {
        var result = [(type: MicroelementType, element: UserMicroelementViewModel)]()
        for element in model {
            let variable: (type: MicroelementType, element: UserMicroelementViewModel) = (type: element.type, self.transform(model: element.element))
            result.append(variable)
        }
        return result
    }
    
    private func transform(model: UserMicroelement) -> UserMicroelementViewModel {
        let userMicroelementViewModel = UserMicroelementViewModel()
        userMicroelementViewModel.consumedMicroelement = model.consumedMicroelement
        userMicroelementViewModel.targetMicroelement = model.targetMicroelement
        userMicroelementViewModel.unit = model.unit
        
        return userMicroelementViewModel
    }
    
    private func transform(model: DetailInformationModel) -> DetailInformationViewModel {
        let detailInformationModel = DetailInformationViewModel()
        
        detailInformationModel.title = model.title
        detailInformationModel.message = model.message
        detailInformationModel.topFood = model.topFood
        
        return detailInformationModel
    }
 }

//
//  ActivityCaloricConsumedCellPresenter.swift
//  Nourish
//
//  Created by Andrey Zmushko on 8/26/17.
//  Copyright © 2017 SSIC. All rights reserved.
//

import UIKit
import RxSwift


class ActivityCaloricConsumedCellPresenter: ActivityBaseCellPresenter {
    
    let interactor = ConsumedInteractor()
    let mapperObjectConsumed = ConsumedModel_to_ConsumedViewModel()
    var consumedViewModel = ConsumedViewModel()
    
    private func getInformation(cell: ActivityBaseTableViewCell, startDate: Date, endDate: Date) {
        cell.baseView.viewWillAppear(true)
        cell.baseView.removeActivityIndicator()
        self.subscribtions.append(self.interactor.execute(startDate: startDate, endDate: endDate).observeOn(MainScheduler.instance).subscribe(onNext: { [unowned self] consumed in
            self.consumedViewModel = self.mapperObjectConsumed.transform(model: consumed)
        }, onError: {error in
            cell.baseView.parseError(error: error, completion: nil)
        }, onCompleted: {
            self.setupViewWithData(cell: cell)
            cell.baseView.stopActivityAnimation()
        }, onDisposed: {
        }))
    }
    
    override func fetchModel(For basecell: ActivityBaseCell) {
        super.fetchModel(For: basecell)
        
        let cell = basecell as! ActivityBaseTableViewCell
        
        cell.iconImageView.imageNamedWithTint(named: "icon_consumed_icon", tintColor: UIColor.white)
        cell.iconFrame.backgroundColor = NRColorUtility.hexStringToUIColor(hex: "40ad75")
        
        cell.titleLabel.attributedText = NSMutableAttributedString(string: "Consumed", attributes: cell.boldAttr)
        
        if let delegate = self.delegate {
            let date = delegate.fetchEpoch()
            self.getInformation(cell: cell, startDate: date.startDate, endDate: date.endDate)
        }

        cell.descriptionLabel.text = "Avg. consumed/day:"
        
        cell.separatorView.isHidden = false
        cell.bottomGap.constant = 16
    }
    
    private func setupViewWithData(cell: ActivityBaseTableViewCell) {
        if let consumedCal = self.consumedViewModel.consumedCalories {
            let value = NSMutableAttributedString(string: String(format: "%0.f", consumedCal), attributes: cell.boldAttr)
            value.append(NSMutableAttributedString(string: " kcal", attributes: cell.bookAttr))
            
            cell.valueLabel.attributedText = value
            
            if let avgConsumed = self.consumedViewModel.avgConsumed {
                cell.descriptionValueLabel.text = String(format: "%.0f kcal", avgConsumed)
            }
            
        }
    }

}

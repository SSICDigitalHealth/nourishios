//
//  NestleRecommendationsRepository.swift
//  Nourish
//
//  Created by Gena Mironchyk on 9/11/17.
//  Copyright © 2017 SSIC. All rights reserved.
//

import UIKit
import RxSwift

class NestleRecommendationsRepository: NSObject {
    
    private let dataStore = NestleRecommendationsDataStore()
    private let userRepo = UserRepository.shared
    private let toRecsMapper = json_to_NestleRecommendations()
    private let cachetoModelMapper = NestleRecommendationCache_to_RecomendationModel()
    private let foodRecordRepo = FoodRecordRepository.shared
    private let recsCacheDataStore = NestleRecommendatitionCacheDataStore()
    
    func getRecommendationsFor(startDate : Date, endDate : Date) -> Observable<RecomendationModel> {
        return self.userRepo.getCurrentUser(policy: .Cached).flatMap{ userProfile -> Observable<RecomendationModel> in
            let cache = self.recsCacheDataStore.getRecsCache(startDate : startDate, endDate : endDate)
            if cache != nil {
                let model = self.cachetoModelMapper.transform(model: cache!)
                return Observable.just(model)
            } else {
                let token = NRUserSession.sharedInstance.accessToken
                return self.dataStore.fetchRecommendations(token: token!, userId: userProfile.userID!, startDate: startDate, endDate: endDate).flatMap{recsDict -> Observable<RecomendationModel> in
                    self.recsCacheDataStore.store(cache : recsDict, startDate : startDate, endDate : endDate)
                    let model = self.cachetoModelMapper.transform(model: recsDict)

                    return Observable.just(model)
                }
            }
        }
    }
    
    func refetchRecomendationFor(date : Date) {
        let array = self.recsCacheDataStore.getDatesToRefetchFrom(date:date)
        
        self.recsCacheDataStore.removeCacheFrom(date: date)
        
        if array.count > 0 {
            for obj in array {
                let _ = self.getRecommendationsFor(startDate: obj.0, endDate: obj.1).subscribe(onNext: {model in
                    print("------> model refetched \(obj.0) - \(obj.1)")
                }, onError: {error in }, onCompleted: {}, onDisposed: {})
            }
        } else {
            let _ = self.refetchRecommendationsForToday().subscribe(onNext: {model in }, onError: {error in }, onCompleted: {}, onDisposed: {})
        }
        
    }
    
    func refetchRecommendationsForToday()  -> Observable<RecomendationModel>  {
        return self.userRepo.getCurrentUser(policy: .DefaultPolicy).flatMap{ userProfile -> Observable<RecomendationModel> in
            let token = NRUserSession.sharedInstance.accessToken
            return self.dataStore.fetchRecommendations(token: token!, userId: userProfile.userID!, startDate: Date(), endDate: Date()).flatMap{recsDict -> Observable<RecomendationModel> in
                self.recsCacheDataStore.store(cache : recsDict, startDate : Date(), endDate : Date())
                let model = self.cachetoModelMapper.transform(model: recsDict)
                if model.meals.count >= 1 {
                    for index in 0...model.meals.count - 1 {
                        var meal = model.meals[index]
                        meal.isFavorite = self.foodRecordRepo.isFavourite(foodItem: meal)
                    }
                }
                
                return Observable.just(model)
            }
        }
    }
    
    func removeAllCache() {
        self.recsCacheDataStore.removeAllCache()
    }
    
    
    func deleteCacheFrom(date: Date) {
        recsCacheDataStore.removeCacheFrom(date: date)
    }
}

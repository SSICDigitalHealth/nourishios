//
//  ArtikError.swift
//  Nourish
//
//  Created by Nova on 11/24/16.
//  Copyright © 2016 SSIC. All rights reserved.
//

import UIKit

class ArtikError: Error {
    var code : Int?
    var errorDescription : String?
}

//
//  CaloricBurnedModel_to_CaloricBurnedViewModel.swift
//  Nourish
//
//  Created by Vlad Birukov on 29.08.17.
//  Copyright © 2017 SSIC. All rights reserved.
//

import Foundation

class CaloricBurnedModel_to_CaloricBurnedViewModel {
    func transform(model: CaloricBurnedModel) -> CaloricBurnedViewModel {
        let caloricBurnedViewModel = CaloricBurnedViewModel()
        
        if model.userBurnedCal != nil {
            caloricBurnedViewModel.userBurnedCal = model.userBurnedCal
        }
        
        if model.avgBurnedCal != nil {
            caloricBurnedViewModel.avgBurnedCal = model.avgBurnedCal
        }
        
        if model.avgActiveCal != nil {
            caloricBurnedViewModel.avgActiveCal = model.avgActiveCal
        }
        
        if model.caloricState != nil {
            caloricBurnedViewModel.caloricState = [caloricStateData]()
            caloricBurnedViewModel.caloricState = model.caloricState
            let numberAddElement = 24 - (model.caloricState?.count)!
        
            if model.caloricState?.count != 24 {
                for _ in 0..<numberAddElement {
                    caloricBurnedViewModel.caloricState?.append(caloricStateData(subtitle: Date(), cal: (basalCalories: 0.0, activeCalories: 0.0)))
                }
            }
        }
        
        return caloricBurnedViewModel
    }
}

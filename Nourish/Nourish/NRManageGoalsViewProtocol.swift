//
//  NRManageGoalsViewProtocol.swift
//  Nourish
//
//  Created by Vino (Vinodha) Sundaramoorthy on 11/23/16.
//  Copyright © 2016 SSIC. All rights reserved.
//

import UIKit

protocol NRManageGoalsViewProtocol : BaseViewProtocol {
    func setUserProfile(model:UserProfileModel)
    func showToast(message:String)
}

//
//  DietChangeTableViewCell.swift
//  Nourish
//
//  Created by Vlad Birukov on 22.08.17.
//  Copyright © 2017 SSIC. All rights reserved.
//

import UIKit

class DietChangeTableViewCell: UITableViewCell {
    @IBOutlet weak var radioImage: UIImageView!
    @IBOutlet weak var nameDiet: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func setupWith(diet: (String, Bool)) {
        if diet.1 == true {
            self.radioImage.imageNamedWithTint(named: "radio_on", tintColor: NRColorUtility.progressScaleBackgroundColor())
        } else {
            self.radioImage.imageNamedWithTint(named: "radio_off", tintColor: NRColorUtility.hexStringToUIColor(hex: "303030"))
        }
        
        self.nameDiet.text = String(format: "%@", diet.0)
    }
    
}

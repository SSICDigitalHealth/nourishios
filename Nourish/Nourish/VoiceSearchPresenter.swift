//
//  VoiceSearchPresenter.swift
//  Nourish
//
//  Created by Vino (Vinodha) Sundaramoorthy on 6/16/17.
//  Copyright © 2017 SSIC. All rights reserved.
//

import UIKit

class VoiceSearchPresenter: BasePresenter {
    var voiceSearchListenerView : VoiceSearchListenerProtocol?
    var jsonMapper = json_SoundHound_to_MealRecord()
    var conversationState : [String : Any]!
    var partialTranscript : String = ""
    var spokenResponse : String = ""
    var mealOcassion : Ocasion!
    var mealRecordArray : [MealRecord] = []
    var date : Date = Date()
    var needReload : Bool = true
    var speechManager = TextToSpeechManager.shared
    var unrecognizedWords : String = ""
    
    override func viewWillAppear(_ animated: Bool) {
        NotificationCenter.default.addObserver(self, selector:#selector(voiceSearchStateChanged) , name: NSNotification.Name.HoundVoiceSearchStateChange, object: nil)
        HoundifyManager().startListening()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        NotificationCenter.default.removeObserver(self)
    }
    
    func voiceSearchStateChanged() {
        if HoundVoiceSearch.instance().state == HoundVoiceSearchState.ready {
            if needReload {
                LogUtility.logToFile("Sound Hound Start Voice Search")
                self.startVoiceSearch(requestInfo: [:])
                self.voiceSearchListenerView?.startListening()
                needReload = false
            }
        }
    }
    

    func startVoiceSearch(requestInfo:[String:Any]) {
        let url = URL(string:kVoiceSearchEndPoint)
        HoundVoiceSearch.instance().enableSpeech = false
        self.unrecognizedWords = ""
        HoundVoiceSearch.instance().start(withRequestInfo: requestInfo, endPointURL: url) { (error, responseType, response, dict) in
            EventLogger.logVoiceSearch()
            if (error != nil) {
                LogUtility.logToFile("Error Starting Voice Search \(error!)")
            } else {
                if responseType == HoundVoiceSearchResponseType.partialTranscription {
                    let partialTranscription = response as! HoundDataPartialTranscript
                    self.partialTranscript = partialTranscription.partialTranscript
                    self.voiceSearchListenerView?.updateSpeechTranscript(text: self.partialTranscript)
                    EventLogger.logVoiceSearchReturnResults()
                } else if responseType == HoundVoiceSearchResponseType.houndServer {
                    self.conversationState = dict as! [String : Any]!
                    self.mealRecordArray = []
                    self.mealOcassion = nil
                    
                    if self.conversationState[kVoiceSearchResults] != nil {
                        let resultsArray = self.conversationState[kVoiceSearchResults] as! [Any]
                        for result in resultsArray {
                            let resultDict = result as! [String:Any]
                            self.spokenResponse = resultDict[kVoiceSpokenRespone] as! String
                            
                            if resultDict[kVoiceConversationState] != nil {
                                let conversationState = resultDict[kVoiceConversationState] as! [String:Any]
                                let loggingData = conversationState[kVoiceFoodLoggingDomain] as! [String:Any]
                            
                                if loggingData[kVoiceUnrecognizedWords] != nil {
                                    self.unrecognizedWords = loggingData[kVoiceUnrecognizedWords] as! String
                                }
                            }
                            if self.unrecognizedWords.count == 0 {
                                if self.spokenResponse == kVoiceSearchExitString {
                                    self.voiceSearchListenerView?.dismiss()
                                    return
                                }
                            
                                if resultDict[kVoiceFoodRespone] != nil {
                                    let infoArray = resultDict[kVoiceFoodRespone] as! [Any]
                                    let mealDict = self.jsonMapper.transform(foodinfo: infoArray, date: self.date)
                                    
                                    if mealDict["MealArray"] != nil {
                                        self.mealRecordArray = mealDict["MealArray"] as! [MealRecord]
                                    }
                                    if mealDict["ocassion"] != nil {
                                        self.mealOcassion = mealDict["ocassion"] as! Ocasion
                                    }
                                }
                            } else {
                                self.spokenResponse = "Sorry, I could not find that item.\nTap on the search button to continue searching manually or tap on the microphone button to start a new search with voice."
                            }
                        }
                    }
                    
                    if self.mealRecordArray.count > 0 && self.mealOcassion != nil {
                        self.voiceSearchListenerView?.showMealListVC(modelArray: self.mealRecordArray, date: self.date,ocasion: self.mealOcassion)
                    } else {
                        self.speechManager.speak(str: self.spokenResponse)
                        self.voiceSearchListenerView?.speechInProgress(spokenResponse:self.spokenResponse)
                    }
                    EventLogger.logVoiceSearchReturnResults()
                }
            }
        }
    }
    
    func startListening() {
        self.startVoiceSearch(requestInfo: [:])
    }
    
    func stopListening() {
        HoundifyManager().stopListening()
        self.voiceSearchListenerView?.stopListenining()
    }
}

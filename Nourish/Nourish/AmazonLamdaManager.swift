//
//  AmazonLamdaManager.swift
//  Nourish
//
//  Created by Vino (Vinodha) Sundaramoorthy on 4/24/17.
//  Copyright © 2017 SSIC. All rights reserved.
//

import UIKit
import AWSCore
import AWSLambda
import RxSwift

let kAmazonAccessKey : String = "AKIAIP6R437VWFOBG72A"
let kAmazonSecretKey : String = "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"
let kMealPredictionLambaFunction : String = "arn:aws:lambda:us-east-1:379419836112:function:meal_prediction"
let kPushTokenLambdaFunction : String = "ndh-msg-push-token-upload-dev-01"
let kTimeZoneLambdaFuncrion : String = "ndh-update-user-time-zone-dev-01"
let kSearchFoodLambdaFunction : String = "ndh-search-food-dev-01"

class AmazonLamdaManager: NSObject {

    let userRepository = UserRepository.shared

   class func getCreds()  {
        let credentialsProvider = AWSStaticCredentialsProvider.init(accessKey:kAmazonAccessKey, secretKey: kAmazonSecretKey)
        let defaultServiceConfiguration = AWSServiceConfiguration(region: AWSRegionType(rawValue: 1)!, credentialsProvider: credentialsProvider)
        AWSServiceManager.default().defaultServiceConfiguration = defaultServiceConfiguration
    }
    
    class func getMealPrediction(ocasion:Ocasion , userId:String) -> [MealRecordModel] {
        AmazonLamdaManager.getCreds()
        var mealRecordArray : [MealRecordModel] = []
        let invocationRequest = AWSLambdaInvokerInvocationRequest()
        invocationRequest?.functionName = kMealPredictionLambaFunction

        invocationRequest?.payload = ["accessToken" : NRUserSession.sharedInstance.accessToken!, "userId" : userId,"occasion":ocasion.rawValue,"include_foodgroup_summary":"true"]
        
        let lambdaInvoker = AWSLambdaInvoker.default()
        lambdaInvoker.invoke(invocationRequest!) { (response, error) in
            print("Error \(error.debugDescription)")
            //print("Response: \(response)")
            if response != nil {
                mealRecordArray = Json_to_MealRecordModel.transform(dict: response?.payload as! [String : Any])
            }
        }
        return mealRecordArray
    }

    func uploadPushToken(token : String, userId : String, deviceId : String) -> Observable<Bool>{
        return Observable.create { observer in
            AmazonLamdaManager.getCreds()
            let invocationRequest = AWSLambdaInvokerInvocationRequest()
            invocationRequest?.functionName = kPushTokenLambdaFunction
            let tokenType = "APNS_DEV"
            invocationRequest?.payload = ["pushToken" : token, "deviceId" : deviceId, "userId" : userId, "tokenType" : tokenType]
            let invoker = AWSLambdaInvoker.default()
            invoker.invoke(invocationRequest!) { responce, error in
                if error != nil {
                    observer.onNext(false)
                    observer.onError(error!)
                }
                else {
                    observer.onNext(true)
                    observer.onCompleted()
                }
            }
            return Disposables.create()
        }
    }
    
    func uploadUserTimeZone(token : String, userID : String, deviceID : String, timeZone : String) -> Observable<Bool> {
        return Observable.create { observer in
            AmazonLamdaManager.getCreds()
            let invocationRequest = AWSLambdaInvokerInvocationRequest()
            invocationRequest?.functionName = kTimeZoneLambdaFuncrion
            invocationRequest?.payload = ["accessToken" : token, "deviceId" : deviceID, "userId" : userID, "timeZone" : timeZone]
            let invoker = AWSLambdaInvoker.default()
            invoker.invoke(invocationRequest!, completionHandler: {responce, error in
                if error != nil {
                    observer.onError(error!)
                } else {
                    if responce != nil && responce?.statusCode == 200 {
                        
                        let data = responce?.payload as? [String : Any]
                        if data != nil {
                            let message = data!["message"] as? String
                            if message != nil && message == "Timezone was updated" {
                                observer.onNext(true)
                            }
                        } else {
                            observer.onNext(false)
                        }
                    }
                    observer.onCompleted()
                }
            })
            return Disposables.create()
        }
    }
    
    func searchFnddsFoodWith(search : String, token : String, userID : String) -> Observable<[foodSearchModel]> {
        return Observable.create { observer in
            AmazonLamdaManager.getCreds()
            let invocationRequest = AWSLambdaInvokerInvocationRequest()
            invocationRequest?.functionName = kSearchFoodLambdaFunction
            let dict = ["q" : search, "limit" : 30, "token" : token, "calorie_consumption_goal" : 1860, "local_time" : self.localTime(), "userid" : userID] as [String : Any]
            invocationRequest?.payload = ["queryParams" : dict]
            let invoker = AWSLambdaInvoker.default()
            
            invoker.invoke(invocationRequest!, completionHandler: {responce, error in
                if error != nil {
                    observer.onError(error!)
                } else {
                    if responce != nil && responce?.statusCode == 200 {
                        
                        let data = responce?.payload as? [String : Any]
                        if data != nil {
                            let array = data!["body"] as! [[String : Any]]
                            let proxyArray = self.mapIntoObjects(array: array)
                            observer.onNext(proxyArray)
                        } else {
                            
                        }
                        
                    }
                    observer.onCompleted()
                }
            })
            return Disposables.create()
        }
    }
    
    private func mapIntoObjects(array : [[String : Any]]) -> [foodSearchModel] {
        var proxyArray = [foodSearchModel]()
        
        for object in array {
            var model = foodSearchModel()
            model.unit = object["msre_desc"] as! String
            let porCal = object["por_kcal"] as! Double
            let grams = object["msre_grams"] as? Double
            let amount = object["msre_amount"] as! Double
            let calories = amount * porCal
            model.calories = "\(calories)"
            model.amount = amount
            model.grams = grams
            model.foodId = object["id"] as! String
            model.foodTitle = object["name"] as! String
            model.mealIdString = object["id"] as? String
            proxyArray.append(model)
        }
        return proxyArray
    }
    
    private func localTime() -> String {
        let formatter = DateFormatter()
        formatter.dateFormat = "HHmmss"
        let localTime = formatter.string(from: Date())
        return localTime
    }
}

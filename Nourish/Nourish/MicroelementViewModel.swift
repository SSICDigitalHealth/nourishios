//
//  MicroelementViewModel.swift
//  Nourish
//
//  Created by Vlad Birukov on 29.08.17.
//  Copyright © 2017 SSIC. All rights reserved.
//

import Foundation

typealias microelementViewList = (date: Date?, elements: [(type: MicroelementType, element: UserMicroelementViewModel)])
class MicroelementViewModel: NSObject {
    var data: [microelementViewList]?
    var detailInformation: [DetailInformationViewModel]?
}

class UserMicroelementViewModel {
    var consumedMicroelement: Double = 0.0
    var targetMicroelement: Double = 0.0
    var unit: String = ""
    
    init() {
    }
    
    init(consumed: Double, target: Double, unit: String) {
        self.consumedMicroelement = consumed
        self.targetMicroelement = target
        self.unit = unit
    }
}

class DetailInformationViewModel {
    var title: String = ""
    var message: String = ""
    var topFood: [String] = [String]()
}

//
//  FitBitView.swift
//  Nourish
//
//  Created by Vino (Vinodha) Sundaramoorthy on 7/20/17.
//  Copyright © 2017 SSIC. All rights reserved.
//

import UIKit

protocol FitBitWebViewProtocol {
    func loadRequest(request : URLRequest)
    func stopAnimation()
}

class FitBitView: BaseView , FitBitWebViewProtocol {

    @IBOutlet weak var fitBitWebView = UIWebView()
    var presenter = FitBitPresenter()
    
    override func viewWillAppear(_ animated: Bool) {
        self.basePresenter = presenter
        self.fitBitWebView?.delegate = presenter
        presenter.fitBitView = self
        super.viewWillAppear(animated)
    }
    
    // MARK : FitBitWebViewProtocol Implementation
    func stopAnimation() {
        self.stopActivityAnimation()
    }
    
    func loadRequest(request: URLRequest) {
        self.fitBitWebView?.loadRequest(request)
    }
    
    var fitBitCacher = BackgroundFitBitCacher.shared

}

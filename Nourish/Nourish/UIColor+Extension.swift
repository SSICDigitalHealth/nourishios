//
//  UIColor+Extension.swift
//  Nourish
//
//  Created by Vino (Vinodha) Sundaramoorthy on 12/14/16.
//  Copyright © 2016 SSIC. All rights reserved.
//

import UIKit

extension UIColor {
    func as1ptImage() -> UIImage {
        UIGraphicsBeginImageContext(CGSize(width: 1, height: 1))
        let ctx = UIGraphicsGetCurrentContext()
        self.setFill()
        ctx!.fill(CGRect(x: 0, y: 0, width: 1, height: 1))
        let image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return image!
    }
    
    func imageWithColor() -> UIImage {
        let rect = CGRect(x: 0.0, y: 0.0, width: 1.0, height: 25.0)
        UIGraphicsBeginImageContext(rect.size)
        let context = UIGraphicsGetCurrentContext()
        context!.setFillColor(self.cgColor);
        context!.fill(rect);
        let image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext();
        return image!
    }
    
    func roundedCornerForImage(image : UIImage) -> UIImage {
        let oldImage = image
        UIGraphicsBeginImageContext(CGSize(width: 1.0, height: 25.0))
        UIBezierPath.init(roundedRect: CGRect(x: 0.0, y: 0.0, width: 1.0, height: 25.0), cornerRadius: 10).addClip()
        oldImage.draw(in: CGRect(x: 0.0, y: 0.0, width: 1.0, height: 25.0))
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext();
        return newImage!
    }

}


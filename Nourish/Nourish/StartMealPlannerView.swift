//
//  StartMealPlannerView.swift
//  Nourish
//
//  Created by Vlad Birukov on 18.08.17.
//  Copyright © 2017 SSIC. All rights reserved.
//

import UIKit

protocol StartMealPlannerProtocol : BaseViewProtocol{
    func setUpWithModel(model: StartMealPlannerViewModel)
}

class StartMealPlannerView: BaseView, StartMealPlannerProtocol {
    @IBOutlet var presenter: StartMealPlannerPresenter!
    @IBOutlet weak var goalLabel: UILabel!
    @IBOutlet weak var dietLabel: UILabel!
    @IBOutlet weak var optifastLabel: UILabel!
    @IBOutlet weak var targetTop: NSLayoutConstraint!
    @IBOutlet weak var messageTop: NSLayoutConstraint!
    @IBOutlet weak var heightView: NSLayoutConstraint!

    let spacing = 4
    let height = 260
    let optifastText = "Select an occasion\nSelect a meal"
    
    override func viewDidLoad() {
        self.xibAutoLayouting(nibName: "StartMealPlannerView")
        self.basePresenter = presenter
        self.presenter.startMealPlannerView = self
        self.settingView()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    
    func setUpWithModel(model: StartMealPlannerViewModel) {
        let numberFormatter = NumberFormatter()
        numberFormatter.numberStyle = .decimal
        
        DispatchQueue.main.async {
            if model.diet != nil {
                self.dietLabel.text = String(format: "%@", model.diet!)
            }
            
            if model.caloriesDay != nil {
                self.goalLabel.text = numberFormatter.string(from: Int(model.caloriesDay!.rounded()) as NSNumber)
            }
            
            if model.optifast != nil {
                self.optifastLabel.text = String(format: "%@", model.optifast!)
            } else {
                self.optifastLabel.text = self.optifastText
            }
        }
    }
    
    private func settingView() {
        if UIScreen.main.bounds.width <= 320 {
            self.heightView.constant = CGFloat(self.height)
            self.targetTop.constant = CGFloat(self.spacing)
            self.messageTop.constant = CGFloat(self.spacing)
        }

    }
}

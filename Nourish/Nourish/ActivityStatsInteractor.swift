//
//  ActivityStatsInteractor.swift
//  Nourish
//
//  Created by Vino (Vinodha) Sundaramoorthy on 12/24/16.
//  Copyright © 2016 SSIC. All rights reserved.
//

import UIKit
import RxSwift

class ActivityStatsInteractor: NSObject {
    
    let activityRepo = UserActivityRepository()
    /*
    func getActivityForPeriod(period : period) -> activityModel {
        let activityList = self.getActivityList(period:period)
        let model = activityModel()
      //  let model = activityModel(steps: 5235, distance: 2.5, activeHours: 2, sleepHours: 6, activityList: activityList, caloriesArray: self.getCaloriesArray(period: period), activityArray: self.getActivityTimeArray(period: period), avgCalorie: 2650, avgSteps: 5750)
        return model
    }
    */
    
    func getActivityList(period:period) -> [(activityName: String, value: Double, type: String)] {
        var list : [(activityName: String, value: Double, type: String)] = []
        list.append((activityName: "Running", value: 6, type: "hr"))
        list.append((activityName: "Walking", value: 1, type: "hr"))
        list.append((activityName: "Treadmill", value: 1.6, type: "hr"))
        list.append((activityName: "Exercise Bike", value: 3.6, type: "hr"))
        return list
    }
    
    func getRealActivityForPeriod(period : period) -> Observable<activityModel> {
        return self.activityRepo.getActivityModelFor(period: period)
    }
    
    func getCaloriesArray(period : period) -> [caloriesModel] {
        var calorieArray : [caloriesModel] = []
        var date = Date()
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "MM/dd"
        let montlyDataCount = 3
        
        if period == .today {
            for _ in 0...23 {
                var dateRange = ""
                if date == Date() {
                    dateRange = "Today"
                } else {
                    dateRange = dateFormatter.string(from: date)
                }
                
                let model = caloriesModel(calorie:Double(self.generateRandomBetween(max: 90, min:50)), caloriePeriodRange: dateRange)
                calorieArray.append(model)
                date = date.addingTimeInterval(-(24*60*60))
            }
        } else if period == .weekly {
            for _ in 0...6 {
                var dateRange = ""
                if date == Date() {
                    dateRange = "Today"
                } else {
                    dateRange = dateFormatter.string(from: date)
                }
                
                let model = caloriesModel(calorie:Double(self.generateRandomBetween(max: 90, min:50)), caloriePeriodRange: dateRange)
                calorieArray.append(model)
                date = date.addingTimeInterval(-(24*60*60))
            }
        } else {
            for _ in 0...montlyDataCount {
                var dateRange = ""
                let firstDay = date
                let lastDay = date.addingTimeInterval(-(24*60*60*7))
                
                //Calculate the date range
                dateRange = String(format:"%@-%@",dateFormatter.string(from: firstDay),dateFormatter.string(from: lastDay))
                let model = caloriesModel(calorie:Double(self.generateRandomBetween(max: 90, min:50)), caloriePeriodRange: dateRange)
                calorieArray.append(model)
                date = date.addingTimeInterval(-(24*60*60*8))
            }
        }
        return calorieArray
    }
    
    func getActivityTimeArray(period : period) -> [activityTime] {
        var timeArray : [activityTime] = []
        var date = Date()
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "MM/dd"
        let montlyDataCount = 3
    
        if period == .weekly {
            for _ in 0...6 {
                var dateRange = ""
                if date == Date() {
                    dateRange = "Today"
                } else {
                    dateRange = dateFormatter.string(from: date)
                }
                let model = activityTime(activeHr: Double(self.generateRandomBetween(max: 500, min:100)), periodString: dateRange)
                timeArray.append(model)
                date = date.addingTimeInterval(-(24*60*60))
            }
        } else {
            for _ in 0...montlyDataCount {
                var dateRange = ""
                let firstDay = date
                let lastDay = date.addingTimeInterval(-(24*60*60*7))
                
                //Calculate the date range
                dateRange = String(format:"%@-%@",dateFormatter.string(from: firstDay),dateFormatter.string(from: lastDay))
                let model = activityTime(activeHr:Double(self.generateRandomBetween(max: 18, min:6)), periodString: dateRange)
                timeArray.append(model)
                date = date.addingTimeInterval(-(24*60*60*8))
            }
        }
        return timeArray
    }
    
    func generateRandomBetween(max : Int, min : Int) -> Int {
        return min + Int(arc4random_uniform(UInt32(max - min + 1)))
    }

}

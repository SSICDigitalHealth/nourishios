//
//  HydrationPresenter.swift
//  Nourish
//
//  Created by Vlad Birukov on 14.06.17.
//  Copyright © 2017 SSIC. All rights reserved.
//

import Foundation
import RxSwift


class HydrationPresenter: BasePresenter {
    
    let objectToModelMapper = HydrationModel_to_HydrationViewModel()
    let interactor = HydrationInteractor()
    var hydrationView: HydrationProtocol!
    var date: (Date, Date)?
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.getHydrationBalance()
    }
    
    private func getHydrationBalance() {
        if self.hydrationView.config() != nil {
            self.date = self.hydrationView.config()?.getDate()
        }
        
        if let view = self.hydrationView {

            if self.date != nil {
                let proxyDate = hydrationView?.config()?.date

                self.subscribtions.append(self.interactor.execute(startDate: (self.date?.0)!, endDate: (self.date?.1)!).observeOn(MainScheduler.instance).subscribe(onNext: { [weak self] hydrationInformation in
                    if let instance = self {
                        if proxyDate == instance.hydrationView?.config()?.date {
                            view.setupWithModel(model: instance.objectToModelMapper.transform(model: hydrationInformation))
                        }
                    }
                    view.stopActivityAnimation()
                }, onError: {error in
                    view.stopActivityAnimation()
                    view.parseError(error : error, completion: nil)
                }, onCompleted: {
                }, onDisposed: {
                }))
            }
        }
    }
}

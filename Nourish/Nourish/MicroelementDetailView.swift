//
//  MicroelementDetailView.swift
//  Nourish
//
//  Created by Vlad Birukov on 16.06.17.
//  Copyright © 2017 SSIC. All rights reserved.
//

import UIKit

protocol MicroelementDetailProtocol : BaseViewProtocol{
    func reloadTable()
    func registerTableViewCells()
    func config() -> ProgressConfig?

}

class MicroelementDetailView: BaseView, MicroelementDetailProtocol {
    @IBOutlet weak var microelementTableView: UITableView!
    @IBOutlet var presenter: MicroelementDetailPresenter!

    var progressConfig: ProgressConfig?
    
    func config() -> ProgressConfig? {
        return self.progressConfig
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.basePresenter = presenter
        self.presenter.microelementDetailView = self
        registerTableViewCells()
        settingTableCell()
        super.viewWillAppear(animated)
    }
    
    func reloadTable() {
        microelementTableView.reloadData()
    }
    
    func settingTableCell() {
        microelementTableView.rowHeight = UITableViewAutomaticDimension
        microelementTableView.estimatedRowHeight = 40
    }
    
    func registerTableViewCells() {
        self.microelementTableView?.register(UINib(nibName: "CustomHeaderMicroelementDetailTableViewCell", bundle: nil), forCellReuseIdentifier: "cellCustomHeaderMicroelement")
        
        self.microelementTableView?.register(UINib(nibName: "MicroelementDetailTableViewCell", bundle: nil), forCellReuseIdentifier: "cellMicroelementDetail")
    }

}

//
//  ChatInfoDialogViewController.swift
//  Nourish
//
//  Created by Vino (Vinodha) Sundaramoorthy on 11/17/17.
//  Copyright © 2017 SSIC. All rights reserved.
//

import UIKit

class ChatInfoDialogViewController: UIViewController {

    @IBOutlet weak var okButton : UIButton!
    @IBOutlet weak var titleLabel : UILabel!
    @IBOutlet weak var descriptionLabel : UILabel!

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    func setDialogInfo(title:String,description:String) {
        self.titleLabel.text = title
        self.descriptionLabel.text = description
    }
    
    @IBAction func dismissAction(sender:UIButton) {
        self.dismiss(animated: false, completion: nil)
    }

}

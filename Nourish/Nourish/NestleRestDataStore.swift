//
//  NestleRestDataStore.swift
//  Nourish
//
//  Created by Nova on 10/4/17.
//  Copyright © 2017 SSIC. All rights reserved.
//

import UIKit
import RxSwift

let urlForFooDCard = kNourishApiAddress + "food"

class NestleRestDataStore: BaseNestleDataStore {
    
    func fetchFoodCardFor(ident : [String], token : String, userID : String) -> Observable<Data> {
        return Observable.create { observer in
            
            let url = self.urlForFoodCard(token: token, userID: userID)
            var request = self.requestWithToken(token: token, url: url)//URLRequest(url : url)
            let method = "POST"
            request.httpMethod = method
            
            let dict = ["foodids" : ident, "include_nutrients" : true] as [String : Any]
            
            var data : Data? = nil
            
            do {
                data = try JSONSerialization.data(withJSONObject: dict, options: .prettyPrinted)
            } catch let error as NSError {
                LogUtility.logToFile(error)
            }
            
            request.setValue("application/json", forHTTPHeaderField: "Content-Type")
            request.httpBody = data
            
            let session = URLSession.shared.dataTask(with: request) {
                data, response, error in
                if error == nil && data != nil {
                    let string = String(data : data!, encoding : .utf8)
                    
                    if string != "INTERNAL SERVER ERROR" {
                        observer.onNext(data!)
                    }
                } else {
                    observer.onError(error!)
                }
                self.log(foodProcessingOrWithings: false, url: url.absoluteString, requestMethod: request.httpMethod, urlResponse: response, requestBody: nil, error: error, responceBody: nil)
                observer.onCompleted()
            }
            session.resume()
            
            return Disposables.create()
        }
    }
    
    private func addCalorieBudgetToUrl(url : String) -> String {
        if defaults.object(forKey: kCalorieBudgetKey) != nil {
            let calorieString = String(format: "&calorie_consumption_goal=%0.f", defaults.double(forKey: kCalorieBudgetKey))
            let authorizedUrl = url.appending(calorieString)
            return authorizedUrl
        } else {
            return url
        }
    }
    
    
    private func urlForFoodCard(token : String, userID : String) -> URL {
        var stringUrl = urlForFooDCard + kNourishUserID + userID
        stringUrl = addAuthorizationHeader(url: stringUrl, token: token)
        return URL(string : stringUrl)!
    }
    
    private func addAuthorizationHeader(url : String, token : String) -> String {
        let authorizedUrl = url.appending(kNourishToken + token)
        return self.addCalorieBudgetToUrl(url: authorizedUrl)
    }

    
}

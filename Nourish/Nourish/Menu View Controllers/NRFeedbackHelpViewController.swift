//
//  NRFeedbackHelpViewController.swift
//  Nourish
//
//  Created by Vino (Vinodha) Sundaramoorthy on 11/1/16.
//  Copyright © 2016 SSIC. All rights reserved.
//

import UIKit

class NRFeedbackHelpViewController: BasePresentationViewController, UITextViewDelegate {
    
    @IBOutlet weak var feedbackView : NRFeedbackView!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Feedback and Help"
        self.setUpNavigationBarButtons()
        feedbackView.controller = self
        feedbackView.viewDidLoad()
    }
    
    func setUpNavigationBarButtons () {
        let titleAttribute = self.navigationBarTitleAttribute()
        let closeButton : UIBarButtonItem =  UIBarButtonItem(image: UIImage(named:"close_icon"), style: .plain, target: self, action: #selector(self.closeAction(_:)))
        closeButton.setTitleTextAttributes(titleAttribute, for: .normal)
        navigationItem.title = self.title
        self.navigationController?.navigationBar.titleTextAttributes = titleAttribute
        self.navigationController?.navigationBar.barTintColor = UIColor.white
        self.navigationController?.navigationBar.tintColor = NRColorUtility.navigationTintColor()
        self.navigationController?.navigationItem.leftBarButtonItem = closeButton
        navigationItem.leftBarButtonItem = closeButton
    }
    
    @IBAction func sendFeedback(sender:UIButton) {
        self.feedbackView.sendUserFeedBack(completion: {stored, error in
            if stored == true {
                self.dismiss(animated: true, completion: nil)
            }
        })
    }
    
}

//
//  NRAboutAppViewController.swift
//  Nourish
//
//  Created by Vino (Vinodha) Sundaramoorthy on 11/1/16.
//  Copyright © 2016 SSIC. All rights reserved.
//

import UIKit

class NRAboutAppViewController: BasePresentationViewController {
    @IBOutlet weak var aboutAppView : NRAboutAppView!
    @IBOutlet weak var logoImage : UIImageView!
    @IBOutlet weak var logoHegihtProportionConstraint : NSLayoutConstraint!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        //self.setUpNavigationBarButtons()
        aboutAppView.controller = self
        aboutAppView.viewDidLoad()
        self.setupView()
    }
    
    private func setupView(){
        #if OPTIFASTVERSION
            self.logoImage.image = #imageLiteral(resourceName: "optiLogoWhite")
            self.logoHegihtProportionConstraint.constant = -52.0
        #else
            self.logoImage.image = #imageLiteral(resourceName: "large_white_nouriq_logo")
            self.logoHegihtProportionConstraint.constant = 0.0
        #endif
        self.view.layoutIfNeeded()
    }
    
    func setUpNavigationBarButtons () {
        let closeButton : UIBarButtonItem =  UIBarButtonItem(image: UIImage(named:"close_icon"), style: .plain, target: self, action: #selector(self.closeAction(_:)))
        self.navigationController?.navigationBar.barTintColor = NRColorUtility.appBackgroundColor()
        self.navigationController?.navigationBar.tintColor = UIColor.white
        self.navigationController?.navigationItem.leftBarButtonItem = closeButton
        navigationItem.leftBarButtonItem = closeButton
    }
    
    @IBAction func close(sender:UIButton) {
        self.dismiss(animated: false, completion: nil)
    }
    
    @IBAction func showTerms(sender:UIButton) {
        let termsVC =  NRTermsConditionViewController()
        termsVC.showAgreementButton = false
        let navigationController = BaseNavigationController.init(rootViewController: termsVC)
        self.present(navigationController, animated: true, completion: nil)
    }

}

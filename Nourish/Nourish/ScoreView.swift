//
//  ScoreView.swift
//  Nourish
//
//  Created by Vino (Vinodha) Sundaramoorthy on 4/18/17.
//  Copyright © 2017 SSIC. All rights reserved.
//

import UIKit

class ScoreView: BaseView , ScoreProtocol {
    @IBOutlet weak var scoreLabel : UILabel!
    @IBOutlet weak var nutrientTableView : UITableView!
    @IBOutlet weak var scoreButton : UIButton!

    var presenter = ScoreFeedbackPresenter()
    var controller : ScoreFeedbackViewController!
    var scoreModel : ScoreModel!

    override func viewWillAppear(_ animated: Bool) {
        self.basePresenter = presenter
        presenter.scoreView = self
        presenter.date = self.controller.date
        
        let blueColor = NRColorUtility.hexStringToUIColor(hex: "#1172B9")
        scoreButton.backgroundColor = blueColor
        scoreButton.tintColor = UIColor.white
        nutrientTableView?.register(UINib(nibName: "NutrientsTableViewCell", bundle: nil), forCellReuseIdentifier: "kNutrientInfo")
        super.viewWillAppear(animated)
    }
    
    // MARK : ScoreProtocol 
    func refreshScore(model: ScoreModel) {
        self.scoreModel = model
        
        self.scoreLabel?.text = String(format:"%.0f",round(self.scoreModel.valueToShow))
        self.setUpNutrientTableView()
    }
    
    func setUpNutrientTableView() {
        nutrientTableView.delegate = presenter
        nutrientTableView.dataSource = presenter
        self.nutrientTableView.reloadData()
    }
    
    func stopActivity() {
        self.stopActivityAnimation()
    }

}

//
//  UserProfileModel_to_UserProfile.swift
//  Nourish
//
//  Created by Nova on 11/15/16.
//  Copyright © 2016 SSIC. All rights reserved.
//

import UIKit

class UserProfileModel_to_UserProfile: NSObject {
    func transform(userModel : UserProfileModel) ->  UserProfile {
        let user = UserProfile()
        user.userID = userModel.userID
        user.userFirstName = userModel.userFirstName
        user.userLastName = userModel.userLastName
        user.activeSinceTimeStamp = userModel.activeSinceTimeStamp
        user.activityLevel = userModel.activityLevel
        user.height = userModel.height
        user.weight = userModel.weight
        user.biologicalSex = userModel.biologicalSex
        user.dietaryPreference = userModel.dietaryPreference
        user.metricPreference = userModel.metricPreference
        user.goalPrefsFood = userModel.goalPrefsFood
        user.goalPrefsLifestyle = userModel.goalPrefsLifestyle
        user.goalPrefsActivity = userModel.goalPrefsActivity
        user.userGoal = userModel.userGoal
        user.age = userModel.age
        user.weightLossStartDate = userModel.weightLossStartDate
        user.startWeight = userModel.startWeight
        user.goalValue = userModel.goalValue
        user.weightToMaintain = userModel.weightToMaintain
        user.needToInformUser = userModel.needToInfromUser
        user.optifastFoodIds = userModel.optifastFoodIds
        user.optifastOcasionsList = userModel.optifastOcasionsList
        return user
    }
}

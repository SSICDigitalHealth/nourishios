//
//  CaloriesModel.swift
//  Nourish
//
//  Created by Vlad Birukov on 06.06.17.
//  Copyright © 2017 SSIC. All rights reserved.
//

import Foundation

class CaloriesModel: NSObject {
    var consumedCalories: Double = 0.0
    var targetCalories: Double = 0.0
    var сaloriesForActivity: Double = 0.0
    var unit: String = ""
}

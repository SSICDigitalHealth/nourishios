//
//  StartMealPlannerPresenter.swift
//  Nourish
//
//  Created by Vlad Birukov on 18.08.17.
//  Copyright © 2017 SSIC. All rights reserved.
//

import Foundation


class NouriqStartMealPlannerPresenter: BasePresenter {
    let mapperObjectStartMealPlanner = NouriqUserProfile_to_StartMealPlannerViewModel()
    let interactor = StartMealPlannerInteractor()
    let interactorMealPlan = MealPlannerInteractor()
    let generateMealPlanInteractor = GenerateMealPlanInteractor()
    var startMealPlannerViewController: NouriqStartMealPlannerViewController?
    var startMealPlannerView: NouriqStartMealPlannerProtocol?

    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.getUserInformation()
    }
    
    private func getUserInformation() {
        self.subscribtions.append(self.interactor.execute().subscribe(onNext: { [unowned self] userInform in
            self.startMealPlannerView?.setUpWithModel(model: self.mapperObjectStartMealPlanner.transform(model: userInform))
        }, onError: {error in
            DispatchQueue.main.async {
                self.startMealPlannerView?.stopActivityAnimation()
                self.startMealPlannerView?.parseError(error : error, completion: nil)
            }
        }, onCompleted: {
            self.startMealPlannerView?.stopActivityAnimation()
        }, onDisposed: {
            self.startMealPlannerView?.stopActivityAnimation()
        }))
    }
    
    @IBAction func changeDiet(_ sender: Any) {
        if self.startMealPlannerViewController != nil {
            self.startMealPlannerViewController?.navigationController?.pushViewController(ChangeDietViewController(), animated: true)
            NavigationUtility.hideTabBar(animated: false)
        }
    }
    
    @IBAction func generatePlan(_ sender: Any) {
        self.startMealPlannerView?.startActivityAnimation()
        self.subscribtions.append(self.generateMealPlanInteractor.executeFor(date: Date()).subscribe(onNext: { [unowned self] generated in
            DispatchQueue.main.async {
                self.startMealPlannerView?.stopActivityAnimation()
                if self.startMealPlannerViewController != nil {
                    self.startMealPlannerViewController?.navigationController?.pushViewController(MealPlannerViewController(), animated: true)
                }
            }
        }, onError: {error in
            DispatchQueue.main.async {
                self.startMealPlannerView?.stopActivityAnimation()
                self.startMealPlannerView?.parseError(error : error, completion: nil)
            }
        }, onCompleted:{}, onDisposed: {}))
        
    }
    
    @IBAction func changeGoal(_ sender: Any) {
        if self.startMealPlannerViewController != nil {
            let navigationController = BaseNavigationController.init(rootViewController: NRManageGoalViewController())
            self.startMealPlannerViewController?.present(navigationController, animated: true, completion: nil)
        }
    }
    
    func isCache() {
        self.subscribtions.append(self.interactorMealPlan.execute(date: Date()).subscribe(onNext: { [unowned self] mealPlannerInform in
            if mealPlannerInform != nil {
                if self.startMealPlannerViewController != nil {
                    self.startMealPlannerViewController?.navigationController?.pushViewController(MealPlannerViewController(), animated: false)
                }
            }
        }, onError: {error in
            DispatchQueue.main.async {
                self.startMealPlannerView?.parseError(error : error, completion: nil)
            }
        }, onCompleted: {}, onDisposed: {

        }))
    }
}

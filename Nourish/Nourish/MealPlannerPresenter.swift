 //
//  MealPlannerPresenter.swift
//  Nourish
//
//  Created by Vlad Birukov on 21.08.17.
//  Copyright © 2017 SSIC. All rights reserved.
//

import Foundation
import RxSwift


class MealPlannerPresenter: BasePresenter, UITableViewDataSource,UITableViewDelegate, ResetMealControllerDelegate, MGSwipeTableCellDelegate {
    var mapperObjectMealPlanner = MealPlannerModel_to_MealPlannerViewModel()
    let interactor = MealPlannerInteractor()
    let changePreferenceInteractor = ChangePreferenceMealPlanInteractor()
    var mealPlannerView: MealPlannerProtocol?
    var mealPlannerModel = MealPlannerViewModel()
    var mealPlannerViewController: MealPlannerViewController?
    var dateRange = 0
    
    let one = 1
    let null = 0
    let swapMealColor = NRColorUtility.hexStringToUIColor(hex: "70C397")
    let heightHeaderCell = 71
    let heightHeaderLess = 41
    
    var lunchIndex = 0
    var snacksIndex = 0
    var breakfastIndex = 0
    var dinnerIndex = 0

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.getMealPlannerInformation(date: self.calculateChooseDate(rage: self.dateRange))
    }
    
    func getMealPlannerInformation(date: Date) {
        self.subscribtions.append(self.interactor.execute(date: date).observeOn(MainScheduler.instance).subscribe(onNext: { [unowned self] mealPlannerInform in

            if mealPlannerInform != nil {
                self.mealPlannerModel = self.mapperObjectMealPlanner.transform(model: mealPlannerInform!)
            }
            
            self.getPreference()
            self.mealPlannerViewController?.mealPlannerNavigationBarView.resetButton.isEnabled = true
            self.mealPlannerView?.stopActivityAnimation()
            self.mealPlannerView?.reloadData()

        }, onError: {error in
            self.mealPlannerView?.stopActivityAnimation()
            self.mealPlannerView?.parseError(error: error, completion: nil)
        }, onCompleted: {
        }, onDisposed: {
            self.mealPlannerView?.stopActivityAnimation()
        }))
    }
    
    func resetMealPlanControllerDidReset() {
        if self.mealPlannerViewController != nil {
            self.mealPlannerViewController?.navigationController?.popViewController(animated: true)
        }
    }
    
    func showResetView() {
        self.mealPlannerViewController?.mealPlannerNavigationBarView.resetButton.isHighlighted = false
        let vc = ResetMealPlannerViewController(nibName : "ResetMealPlannerViewController", bundle : nil)
        
        vc.delegate = self
        vc.backgroundImage = NavigationUtility.takeScreenShot()
        
        self.mealPlannerViewController?.present(vc, animated: false, completion: nil)

    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return self.mealPlannerModel.meal != nil ? 1 : 0
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return self.isHidenViewInHeaderCell() ? CGFloat(self.heightHeaderLess) : CGFloat(self.heightHeaderCell)
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let  headerCell = tableView.dequeueReusableCell(withIdentifier: "cellheaderMealPlanner") as! HeaderMealPlannerTableViewCell
                
        headerCell.setUpWithModel(model: self.mealPlannerModel, presenter: self, date: calculateChooseDate(rage: self.dateRange), isHideView: self.isHidenViewInHeaderCell())
        
        return headerCell
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if self.mealPlannerModel.meal != nil {
            return (self.mealPlannerModel.meal?.count)!
        } else {
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cellMealPlanner", for: indexPath) as! MealPlannerTableViewCell
        
        if indexPath.row == ((self.mealPlannerModel.meal?.count)! - 1) {
            cell.line.backgroundColor = UIColor.white
        } else {
            cell.line.backgroundColor = NRColorUtility.mealPlannerColor()
        }
        
        if self.mealPlannerModel.meal?[indexPath.row] != nil {
            let model = self.mealPlannerModel.meal?[indexPath.row]
            let currentIndex = self.getCurrentIndex(occasion: (model?.nameOccasion)!)
            
            cell.setUpWith(model: (mealPlannerModel.meal?[indexPath.row])!, index: currentIndex, presenter: self, indexPath: indexPath)
        }
        
        cell.delegate = self
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.getMealPlannerInformation(date: calculateChooseDate(rage: self.dateRange))
        let viewController = OccasionMealPlannerViewController()
        if self.mealPlannerModel.meal != nil {
            
            let currentIndex = self.getCurrentIndex(occasion: self.mealPlannerModel.meal![indexPath.row].nameOccasion)
            
            viewController.occasionViewModel = self.mealPlannerModel.meal?[indexPath.row].detail[currentIndex]
            viewController.titleText = String(format: "%@   %.0f kcal", Ocasion.stringDescription(servingType:(self.mealPlannerModel.meal?[indexPath.row].nameOccasion)!), (self.mealPlannerModel.meal?[indexPath.row].calories[currentIndex])!)
        }
        
        if let controller = mealPlannerViewController {
            if let navigation = controller.navigationController {
                navigation.pushViewController(viewController, animated: true)
                NavigationUtility.hideTabBar(animated: false)
            }
        }
    }
    
    func swipeTableCell(_ cell: MGSwipeTableCell, swipeButtonsFor direction: MGSwipeDirection, swipeSettings: MGSwipeSettings, expansionSettings: MGSwipeExpansionSettings) -> [UIView]? {
        swipeSettings.transition = MGSwipeTransition.border
        expansionSettings.buttonIndex = 0
        
        let index = self.mealPlannerView?.getIndexCell(cell: cell)
        
        let data = self.mealPlannerModel.meal![(index?.row)!]

        if data.nameFood.count > 1 {
            if direction == MGSwipeDirection.rightToLeft {
                expansionSettings.fillOnTrigger = true
                expansionSettings.threshold = 1
                
                return [
                    MGSwipeButton(title: "Swap\nMeal", backgroundColor: self.swapMealColor, callback: {(cell) -> Bool in
                        
                        switch data.nameOccasion {
                        case .breakfast:
                            if self.breakfastIndex == data.nameFood.count - 1 {
                                self.breakfastIndex = 0
                            } else {
                                self.breakfastIndex += 1
                            }
                            
                        case .lunch:
                            if self.lunchIndex == data.nameFood.count - 1 {
                                self.lunchIndex = 0
                            } else {
                                self.lunchIndex += 1
                            }
                            
                        case .dinner:
                            if self.dinnerIndex == data.nameFood.count - 1 {
                                self.dinnerIndex = 0
                            } else {
                                self.dinnerIndex += 1
                            }
                            
                        case .snacks:
                            if self.snacksIndex == data.nameFood.count - 1 {
                                self.snacksIndex = 0
                            } else {
                                self.snacksIndex += 1
                            }
                        }
                        self.changePreference()
                        self.mealPlannerView?.reloadData()
                        
                        return true
                    })
                ]
            }
        }
        
        return nil
    }
    
    func calculateChooseDate(rage: Int) -> Date {
        let calendar = Calendar.current
        let date = calendar.date(byAdding: .day, value: rage, to: Date())
        
        return date!
    }
    
    func getCurrentIndex(occasion: Ocasion) -> Int {
        var curentIndex = 0
        
        switch occasion {
            case .breakfast:
                curentIndex = self.breakfastIndex
            case .dinner:
                curentIndex = self.dinnerIndex
            case .lunch:
                curentIndex = self.lunchIndex
            case .snacks:
                curentIndex = self.snacksIndex
        }
        
        return curentIndex
    }
    
    func showRecipies(i: Int, indexPath: IndexPath, index: Int) {
        self.getMealPlannerInformation(date: self.calculateChooseDate(rage: self.dateRange))
        
        if self.mealPlannerViewController?.navigationController != nil {
            if let recipies = self.mealPlannerModel.meal![indexPath.row].detail[index].occasion![i].1 {
                
                let newController = RecipesDetailViewController()
                newController.titleNavigationBar = recipies.nameFood
                newController.recipiesViewModel = recipies
                newController.isShowTabBar = true
                self.mealPlannerViewController?.navigationController?.pushViewController(newController, animated: true)
                NavigationUtility.hideTabBar(animated: false)
            }

        }
    }
    
    func showOptifastFood() {
        UIApplication.shared.openURL(kOptifastShopURL)
    }
    
    private func changePreference() {
        self.subscribtions.append(self.changePreferenceInteractor.execute(date: self.calculateChooseDate(rage: self.dateRange), lunch: self.lunchIndex, breakfast: self.breakfastIndex, dinner: self.dinnerIndex, snaks: self.snacksIndex).subscribe(onNext: {bool in
        }, onError: {error in
            self.mealPlannerView?.parseError(error: error, completion: nil)
        }, onCompleted: {
        }, onDisposed: {
        }))
    }
    
    private func isHidenViewInHeaderCell() -> Bool {
        if self.lunchIndex != 0 || self.breakfastIndex != 0 || self.snacksIndex != 0 || self.dinnerIndex != 0 {
            return true
        }
        
        return false
    }
    
    private func getPreference() {
        self.lunchIndex = self.mealPlannerModel.preference.lunch
        self.breakfastIndex = self.mealPlannerModel.preference.breakfast
        self.dinnerIndex = self.mealPlannerModel.preference.dinner
        self.snacksIndex = self.mealPlannerModel.preference.snacks
    }
 }

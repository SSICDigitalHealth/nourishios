//
//  BaseViewController.swift
//  Nourish
//
//  Created by Vino (Vinodha) Sundaramoorthy on 11/1/16.
//  Copyright © 2016 SSIC. All rights reserved.
//

import UIKit

import Reachability
import RxSwift
import MessageUI
import Gzip
let lastVersionCheckKey = "Last version check date"

class BaseViewController: UIViewController, SlideMenuDelegate, MFMailComposeViewControllerDelegate {
    
    var showingMenu : Bool = false
    var menuVC : MenuViewController?
    var swipeRight : UISwipeGestureRecognizer!
    var reachability : Reachability?
    var bannerView : UIView!
    var alertController : UIAlertController?
    func slideMenuItemSelectedAtIndex(_ index : Int32 , _ menuVC : String) {
        if index != -1 {
            if menuVC == "NRFeedbackHelpViewController" {
                self.sendEmailWithLogs()
            } else {
                self.openViewControllerBasedOnIdentifier(menuVC)
            }
        }
    }
    
    private func sendEmailWithLogs() {
        let userRepo = UserRepository.shared
        let _ = userRepo.getCurrentUser(policy: .Cached).observeOn(MainScheduler.instance).subscribe(onNext: { [unowned self] userProfile in
            let userCache = UserProfile_to_UserProfileCacheRealm().transform(userProfile: userProfile)
            let userJson = UserProfileCacheRealm_to_Json().transform(userCache: userCache)
            let df = DateFormatter()
            df.dateFormat = "dd-MM-yyyy"
            let timeString = df.string(from: Date())
            
            let mailController = MFMailComposeViewController()
            mailController.setSubject(String(format : "%@ %@",kMailSubject, timeString))
            let bodyString = String(format : "Hello support team,\n\nMy feeedback:\n\n\n\nApp v %@\n",Bundle.main.infoDictionary?["CFBundleShortVersionString"] as? String ?? "")
            mailController.setMessageBody(bodyString, isHTML: false)
            mailController.setToRecipients([kMailSupportAdress])
            mailController.mailComposeDelegate = self
            if let logData = LogUtility.dailyLogAsData() {
                let fileName = timeString.appending("-log.zip")
                mailController.addAttachmentData(try! logData.gzipped(), mimeType: "application/zip", fileName: fileName)
            }
            
            if let userData = try? JSONSerialization.data(withJSONObject: userJson, options: []) {
                mailController.addAttachmentData(userData, mimeType: "text/plain", fileName: "userData.txt")
            }
            
            self.present(mailController, animated: true, completion: nil)
            
        }, onError: {error in}, onCompleted: {}, onDisposed: {})
    }
    
    func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
        controller.dismiss(animated:true, completion: nil)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        #if OPTIFASTVERSION
            let date = defaults.value(forKey: lastVersionCheckKey) as? Date ?? Date()
            if defaults.value(forKey: lastVersionCheckKey) == nil || Calendar.current.isDateInToday(date) == false {
                let checkVerInteractor = OptifastVersionCheckInteractor()
                let _ = checkVerInteractor.execute().observeOn(MainScheduler.instance).subscribe(onNext: {model in
                    let dictionary = Bundle.main.infoDictionary!
                    let version = dictionary["CFBundleShortVersionString"] as? String ?? ""
                    if self.needToShowAlert(appVer: version, remoteVer: model.version) {
                        self.showVersionAlertWith(model: model)
                    }
                }, onError: {error in}, onCompleted: {}, onDisposed: {})
            }
        #endif
        
        
        self.reachability = Reachability()
        do {
            try self.reachability?.startNotifier()
        } catch {
            print("Unable to start notifier")
        }
        self.reachability?.whenUnreachable = { [weak self] _ in
            self?.showNoNetworkError()
        }
    }
    
    #if OPTIFASTVERSION
    private func needToShowAlert(appVer : String, remoteVer : String) -> Bool {
        var needToShow = false
        let localAppVer = appVer.components(separatedBy: ".")
        let remoteAppVer = remoteVer.components(separatedBy: ".")
        if localAppVer.count > 0 && remoteAppVer.count > 0 {
            if Int(localAppVer[0]) ?? 0 < Int(remoteAppVer[0]) ?? 0 {
                needToShow = true
            }
        }
        if localAppVer.count > 1 && remoteAppVer.count > 1 {
            if Int(localAppVer[1]) ?? 0 < Int(remoteAppVer[1]) ?? 0 {
                needToShow = true
            }
        }
        if localAppVer.count > 2 && remoteAppVer.count > 2 {
            if Int(localAppVer[2]) ?? 0 < Int(remoteAppVer[2]) ?? 0 {
                needToShow = true
            }
        }
        return needToShow
        
    }
    
    
    
    
    private func showVersionAlertWith(model : OptifastCheckVerModel) {
        if self.alertController == nil && self.isViewLoaded && (self.view.window != nil) {
         self.alertController = UIAlertController(title: model.messageTitle, message: model.messageText, preferredStyle: .alert)
            if let alertVC = self.alertController {
                alertVC.addAction(UIAlertAction(title: model.buttonText, style: .default, handler: { action in
                    let url = URL(string: model.detailsUrl)!
                    UIApplication.shared.openURL(url)
                    defaults.setValue(Date(), forKey: lastVersionCheckKey)
                    defaults.synchronize()
                    self.alertController = nil
                    
                }))
                alertVC.addAction(UIAlertAction(title: "Not now", style: .default, handler: {action in
                    defaults.setValue(Date(), forKey: lastVersionCheckKey)
                    defaults.synchronize()
                    self.alertController = nil
                    
                }))
                self.present(alertVC, animated: true, completion: nil)
                }
            }
    }
    #endif

    override func viewWillDisappear(_ animated: Bool) {
        self.reachability?.stopNotifier()
        self.reachability = nil
        if self.bannerView != nil {
            self.bannerView.removeFromSuperview()
            self.bannerView = nil
        }
    }
    
    override func viewDidLoad() {
        NotificationCenter.default.addObserver(self, selector: #selector(didEnterForeground), name: .UIApplicationDidBecomeActive, object: nil)
        ;
        if self.swipeRight == nil {
            self.swipeRight = UISwipeGestureRecognizer(target: self, action: #selector(self.respondToSwipeGesture))
            self.swipeRight.direction = UISwipeGestureRecognizerDirection.right
            self.view.addGestureRecognizer(self.swipeRight)
            self.swipeRight.isEnabled = false
        }
    }
    
    
    @objc private func didEnterForeground ()
    {
        if self.isViewLoaded && (self.view.window != nil) && !self.isKind(of: NRLoginSplashViewController.self) {
            self.viewWillAppear(true)
        }
    }
    
    func canShowNoNetworkBanner() -> Bool {
        return true
    }
    
    func showNoNetworkError () {
        //Subviews will override method
    }
    
    func openViewControllerBasedOnIdentifier(_ strIdentifier:String){
        var subString = "NourIQ"
    #if NESTLEVERSION
            subString = "Nourish_nestle"
    #elseif OPTIFASTVERSION
        subString = "Optifast"
    #endif
        if strIdentifier.count > 0 {
            if strIdentifier != "NRLoginSplashViewController" {
                let className = String(format:"%@.%@",subString,strIdentifier)
                let aClass = NSClassFromString(className) as! UIViewController.Type
                let viewController = aClass.init()
                if  strIdentifier == "NRAboutAppViewController" {
                    self.present(viewController, animated: false, completion: nil)
                } else {
                    let navigationController = BaseNavigationController.init(rootViewController: viewController)
                    self.present(navigationController, animated: false, completion: nil)
                }
            } else {
                //log out
                NRUserSession.sharedInstance.logout(keepCurrent: false)
                NavigationUtility.changeRootViewController(userProfile:nil, awakedFromBackground: false)
            }
           
        }
    }
        
    func onSlideMenuButtonPressed(_ sender : UIButton){
        if (self.showingMenu)
        {
            // To Hide Menu If it already there
            self.slideMenuItemSelectedAtIndex(-1,"");
            let viewwMenuBack : UIView = (self.menuVC?.view)!
            self.showingMenu = false
            
            UIView.animate(withDuration: 0.3, animations: { () -> Void in
                var frameMenu : CGRect = viewwMenuBack.frame
                frameMenu.origin.x = -1 * UIScreen.main.bounds.size.width
                viewwMenuBack.frame = frameMenu
                viewwMenuBack.layoutIfNeeded()
                viewwMenuBack.backgroundColor = UIColor.clear
                }, completion: { (finished) -> Void in
                    viewwMenuBack.removeFromSuperview()
            })
            return
        }
        
        self.showingMenu = true
        let menuViewC = MenuViewController(nibName: "MenuViewController", bundle: nil)
        menuViewC.delegate = self
        
        UIApplication.shared.keyWindow?.addSubview(menuViewC.view)
        menuViewC.view.layoutIfNeeded()
        self.setNeedsStatusBarAppearanceUpdate()
        self.menuVC = menuViewC
    
        let parentView = UIApplication.shared.keyWindow
        
        menuViewC.view.frame=CGRect(x: 0 - UIScreen.main.bounds.size.width, y: 0, width: (parentView?.frame.width)!, height: (parentView?.frame.height)!);
        
        UIView.animate(withDuration: 0.3, animations: { () -> Void in
            menuViewC.view.frame = (parentView?.frame)!
            }, completion:nil)
    }
    
    func respondToSwipeGesture(gesture: UISwipeGestureRecognizer) {
        if ((defaults.value(forKey: onBoardingKey)) != nil) {
            self.onSlideMenuButtonPressed(UIButton.init())
        }
    }
    
    override var supportedInterfaceOrientations: UIInterfaceOrientationMask {
        return UIInterfaceOrientationMask.portrait
    }
    
    override var shouldAutorotate: Bool {
        return false
    }
 }

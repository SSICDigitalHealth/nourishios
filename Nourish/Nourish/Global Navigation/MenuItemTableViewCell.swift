//
//  MenuItemTableViewCell.swift
//  Nourish
//
//  Created by Vino (Vinodha) Sundaramoorthy on 2/10/17.
//  Copyright © 2017 SSIC. All rights reserved.
//

import UIKit

class MenuItemTableViewCell: UITableViewCell {
    @IBOutlet weak var menuImage : UIImageView!
    @IBOutlet weak var menuLabel : UILabel!
    @IBOutlet weak var cellSeparator : UIView?
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}

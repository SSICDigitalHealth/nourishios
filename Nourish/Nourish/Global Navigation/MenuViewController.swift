//
//  MenuViewController.swift
//  Nourish
//
//  Created by Vino (Vinodha) Sundaramoorthy on 11/1/16.
//  Copyright © 2016 SSIC. All rights reserved.
//

import UIKit
import SwiftKeychainWrapper
import RxSwift
import MessageUI
protocol SlideMenuDelegate {
    func slideMenuItemSelectedAtIndex(_ index : Int32 , _ menuVC : String)
}

class MenuViewController: BasePresentationViewController, UITableViewDataSource, UITableViewDelegate {

    @IBOutlet var tblMenuOptions : UITableView!
    @IBOutlet var btnCloseMenuOverlay : UIButton!
    @IBOutlet weak var coverImageView : UIImageView!
    @IBOutlet weak var profileImageView : UIImageView!
    @IBOutlet weak var bgView : UIView!
    @IBOutlet weak var name : UILabel!
    @IBOutlet weak var joinedTimeStamp : UILabel!
    let withingsInteractor = WithingsInteractor()

    var arrayMenuOptions = [Dictionary<String,String>]()
    var delegate : SlideMenuDelegate?
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.tblMenuOptions?.register(UINib(nibName: "MenuItemTableViewCell", bundle: nil), forCellReuseIdentifier: "menuItemCell")
        self.updateArrayMenuOptionsWithWithings()
       
        self.navigationController?.navigationBar.layer.zPosition = -1;
        self.navigationController?.navigationBar.isHidden = true
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.setNeedsStatusBarAppearanceUpdate()
        self.bgView.layer.shadowColor = UIColor.gray.cgColor
        self.bgView.backgroundColor = NRColorUtility.appBackgroundColor()
        self.bgView.layer.shadowOpacity = 2
        self.bgView.layer.shadowOffset = CGSize.zero
        self.bgView.layer.shadowRadius = 15
        tblMenuOptions.tableFooterView = UIView()
        
        let _ = UserRepository.shared.getUserName().observeOn(MainScheduler.instance).subscribe(onNext : { [weak self] userName in
            if let instance = self {
                instance.name.text = userName
            }
        })
        let _ = UserRepository.shared.getCurrentUser(policy: .Cached).observeOn(MainScheduler.instance).subscribe(onNext: {[weak self] userProfile in
            if let instance = self {
                instance.profileImageView.image = NRImageUtility.getProfilePicture(isGlobalDrawer: false,userId:userProfile.userID ?? "")

            }
        })
        
        let _ = UserRepository.shared.getUserJoinedString().observeOn(MainScheduler.instance).subscribe(onNext : { [weak self] userDate in
            if let instance = self {
                instance.joinedTimeStamp.text = userDate
            }
        })
    }
    
    
    
    func updateArrayMenuOptionsWithWithings() {
        if arrayMenuOptions.count > 0 {
            arrayMenuOptions.removeAll()
        }
        arrayMenuOptions.append(["title":"Profile Settings", "icon":"settings_profile.png","menuVC":"NRProfileSettingsViewController"])
        arrayMenuOptions.append(["title" : "Buy OPTIFAST", "icon" : "shopping_cart_green", "menuVC" : ""])
        arrayMenuOptions.append(["title":kManageGoalsViewTitle, "icon":"settings_mng_goal.png","menuVC":"NRManageGoalViewController"])
        arrayMenuOptions.append(["title":"Portion Size Reference", "icon":"icon_drawer_portion_size","menuVC":"PortionSizeViewController"])
        arrayMenuOptions.append(["title":"Connect Devices", "icon":"settings_adddevice.png","menuVC":"ConnectDevicesViewController"])
        arrayMenuOptions.append(["title":"About", "icon":"settings_aboutus.png","menuVC":"NRAboutAppViewController"])
       
        if MFMailComposeViewController.canSendMail(){
            arrayMenuOptions.append(["title":"Give Feedback", "icon":"settings_feedback.png","menuVC":"NRFeedbackHelpViewController"])
        }
        
        arrayMenuOptions.append(["title":"Logout", "icon":"settings_logout.png","menuVC":"NRLoginSplashViewController"])
        tblMenuOptions.reloadData()
    }
    
    @IBAction func onCloseMenuClick(_ button:UIButton!){
        if (self.delegate != nil) {
            var index = Int32(button.tag)
            if(button == self.btnCloseMenuOverlay){
                index = -1
            }
            let vcString = arrayMenuOptions[button.tag]["menuVC"]
            
            let delMenu = delegate as! BaseViewController
            delMenu.showingMenu = false
            delegate?.slideMenuItemSelectedAtIndex(index,vcString!)
        }
        
        UIView.animate(withDuration: 0.3, animations: { () -> Void in
            self.view.frame = CGRect(x: -UIScreen.main.bounds.size.width, y: 0, width: UIScreen.main.bounds.size.width,height: UIScreen.main.bounds.size.height)
            self.view.layoutIfNeeded()
            self.view.backgroundColor = UIColor.clear
            }, completion: { (finished) -> Void in
                self.view.removeFromSuperview()
                self.removeFromParentViewController()
        })
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell : MenuItemTableViewCell = tableView.dequeueReusableCell(withIdentifier: "menuItemCell") as! MenuItemTableViewCell
        cell.selectionStyle = UITableViewCellSelectionStyle.none
        cell.layoutMargins = UIEdgeInsets.zero
        cell.preservesSuperviewLayoutMargins = false
        cell.backgroundColor = UIColor.clear
        cell.menuImage.image = UIImage(named: arrayMenuOptions[indexPath.row]["icon"]!)?.withRenderingMode(.alwaysTemplate)
        cell.menuImage.tintColor = NRColorUtility.hexStringToUIColor(hex: "#70c397")
        cell.menuLabel.text = arrayMenuOptions[indexPath.row]["title"]!
        if cell.cellSeparator != nil {
            cell.cellSeparator?.removeFromSuperview()
        }

        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.row == 0 {
            EventLogger.logOnProfileSettingsClick()
        }
        if indexPath.row == 1 {
            EventLogger.logEstoreOpenedMenu()
            self.showOptifastFood()
        }
        if indexPath.row == 2 {
            EventLogger.logOnManageGoalClick()
        }
        
        let btn = UIButton(type: UIButtonType.custom)
        btn.tag = indexPath.row
        self.onCloseMenuClick(btn)
    }
    
    func showOptifastFood() {
        UIApplication.shared.openURL(kOptifastShopURL)
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrayMenuOptions.count
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1;
    }
    
    override var prefersStatusBarHidden: Bool {
        return false
    }
    
    override func canShowNoNetworkBanner() -> Bool {
        return false
    }

}

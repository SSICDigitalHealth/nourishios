//
//  OptifastMealPlannerPresenter.swift
//  Optifast
//
//  Created by Gena Mironchyk on 1/9/18.
//  Copyright © 2018 SSIC. All rights reserved.
//

import UIKit
import RxSwift
class OptifastMealPlannerPresenter: BasePresenter, ResetMealControllerDelegate, UITableViewDelegate, UITableViewDataSource{
    var mealPlannerView : OptifastMealPlannerProtocol?
    var optifastMealPlannerViewController : OptifastMealPlannerViewController?
    
    var mapperObjectMealPlanner = OptifastMealPlannerModel_to_OptifastMealPlannerViewModel()
    let interactor = OptifastMealPlannerInteractor()
    let changePreferenceInteractor = ChangePreferenceMealPlanInteractor()
    var mealPlannerModel = OptifastMealPlannerViewModel()
    var dateRange = 0
    
    let heightHeaderCell = 71

    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        NotificationCenter.default.addObserver(self, selector: #selector(reloadData), name: NSNotification.Name(rawValue: kFavReload), object: nil)
        self.loadDataFor(date: self.calculateChooseDate(rage: self.dateRange))
    }
    
    @objc private func reloadData() {
        self.loadDataFor(date: self.calculateChooseDate(rage: self.dateRange))
    }
    
    func loadDataFor(date : Date) {
        self.subscribtions.append(self.interactor.execute(date: date).observeOn(MainScheduler.instance).subscribe(onNext: { [unowned self] mealPlan in
            
            if mealPlan != nil {
                self.mealPlannerModel = self.mapperObjectMealPlanner.transform(model: mealPlan!)
            }
            
            self.optifastMealPlannerViewController?.mealPlannerNavigationBarView.resetButton.isEnabled = true
            self.mealPlannerView?.stopActivityAnimation()
            self.mealPlannerView?.reloadData()
            
        }, onError: {error in
            self.mealPlannerView?.stopActivityAnimation()
            self.mealPlannerView?.parseError(error: error, completion: nil)
        }, onCompleted: {
        }, onDisposed: {
            self.mealPlannerView?.stopActivityAnimation()
        }))
    }
    
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return self.mealPlannerModel.foods != nil ? self.mealPlannerModel.foods!.count + 1 : 0
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if section == 0 {
            return CGFloat(self.heightHeaderCell)
        } else {
            return 44.0
        }
    }
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        if section == 0 {
            let  headerCell = tableView.dequeueReusableCell(withIdentifier: "oprifastHeaderCell") as! OptifastHeaderMealPlannerTableViewCell
            headerCell.setUpWithModel(model: self.mealPlannerModel, presenter: self, date: calculateChooseDate(rage: self.dateRange), isHideView: false)
            return headerCell
        } else {
            let sectionCell = tableView.dequeueReusableCell(withIdentifier: "optifastSectionCell") as! OptifastMealPlannerSectionCell
            if let model = self.mealPlannerModel.foods?[section - 1] {
                sectionCell.setUpWith(model: model, index: section - 1)
                sectionCell.actionButton.tag = section - 1
                sectionCell.actionButton.addTarget(self, action: #selector(showDetailedPlan(sender:)), for: .touchUpInside)
            }
            return sectionCell
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section != 0 {
            if let model = self.mealPlannerModel.foods?[section - 1] {
                return model.ocasionFoods.count
            } else  {
                return 0
            }
        } else {
            return 0
        }
    }
    
    

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "optifastMealCell") as! OptifastMealPlannerMealCell
        if indexPath.section > 0 {
            if let foods = self.mealPlannerModel.foods?[indexPath.section - 1] {
                cell.setupWith(food: foods.ocasionFoods[indexPath.row])
                cell.buyButton.addTarget(self, action: #selector(processBuyButton), for: .touchUpInside)
                cell.foodButton.section = indexPath.section - 1
                cell.foodButton.index = indexPath.row
                cell.foodButton.addTarget(self, action:#selector(processShowReciepe(sender:)) , for: .touchUpInside)
            }
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 44.0
    }
    
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        let view = UIView()
        if section > 0 {
            view.backgroundColor = NRColorUtility.mealPlannerColor()
        } else {
            view.backgroundColor = UIColor.clear
        }
        return view
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        if section > 0 {
            return 1.0
        }
        return 0.0
    }
    
    func resetMealPlanControllerDidReset() {
        if self.optifastMealPlannerViewController != nil {
            self.optifastMealPlannerViewController?.navigationController?.popViewController(animated: true)
        }
    }
    
    func showResetView() {
        self.optifastMealPlannerViewController?.mealPlannerNavigationBarView.resetButton.isHighlighted = false
        let vc = ResetMealPlannerViewController(nibName : "ResetMealPlannerViewController", bundle : nil)
        
        vc.delegate = self
        vc.backgroundImage = NavigationUtility.takeScreenShot()
        
        self.optifastMealPlannerViewController?.present(vc, animated: false, completion: nil)
        
    }
    
    func processShowReciepe(sender : RecipeButton) {
        EventLogger.logReceiptClicked()
        if self.optifastMealPlannerViewController?.navigationController != nil {
            if let recipe = self.mealPlannerModel.foods![sender.section].ocasionFoods[sender.index].mealReciepe {
                let newController = RecipesDetailViewController()
                newController.titleNavigationBar = recipe.nameFood
                newController.recipiesViewModel = recipe
                newController.isShowTabBar = true
                self.optifastMealPlannerViewController?.navigationController?.pushViewController(newController, animated: true)
                NavigationUtility.hideTabBar(animated: false)
            }
        }
    }
    
    func processBuyButton () {
        EventLogger.logEstoreOpened()
        UIApplication.shared.openURL(kOptifastShopURL)
    }
    
    
    func showDetailedPlan(sender : UIButton) {
        let viewController = OccasionMealPlannerViewController()
        if self.mealPlannerModel.foods != nil {
            let detailedFoods = self.mealPlannerModel.foods![sender.tag]
            let ocasionViewModel = OccasionMealPlannerViewModel()
            ocasionViewModel.occasion = detailedFoods.ocasionFoods as [(foodSearchModel, RecipiesViewModel?)]
            viewController.occasionViewModel = ocasionViewModel
            viewController.titleText = String(format: "%@   %.0f kcal", Ocasion.stringDescription(servingType:(detailedFoods.occasion)), (detailedFoods.totalCalories))
        }
        
        if self.optifastMealPlannerViewController?.navigationController != nil{
            self.optifastMealPlannerViewController?.navigationController?.pushViewController(viewController, animated: true)
            NavigationUtility.hideTabBar(animated: false)
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.section > 0 {
            if let foods = self.mealPlannerModel.foods?[indexPath.section - 1] {
                let foodToCompare = foods.ocasionFoods[indexPath.row].meal
                if foodToCompare.foodId.contains("[opti]") {
                    let vc = OptifastMealPlannerChangeDishViewController(nibName: "OptifastMealPlannerChangeDishViewController", bundle: nil)
                    vc.ocasion = self.mealPlannerModel.foods![indexPath.section - 1].occasion
                    vc.model = self.mealPlannerModel
                    if self.optifastMealPlannerViewController?.navigationController != nil{
                        self.optifastMealPlannerViewController?.navigationController?.pushViewController(vc, animated: true)
                        NavigationUtility.hideTabBar(animated: false)
                    }
                }
            }
        }
    }
    
    
    private func calculateChooseDate(rage: Int) -> Date {
        let calendar = Calendar.current
        let date = calendar.date(byAdding: .day, value: rage, to: Date())
        
        return date!
    }
    
}

//
//  UserDeviceRoutine.swift
//  Nourish
//
//  Created by Nova on 12/6/16.
//  Copyright © 2016 SSIC. All rights reserved.
//

import UIKit
import RxSwift

class UserDeviceRoutine: NSObject {
    let artikRepo = ArtikCloudConnectionManager()
    let deviceRepo = UserDevicesRepository.shared
    let fromJsonMapper = Json_to_UserDevice()
}

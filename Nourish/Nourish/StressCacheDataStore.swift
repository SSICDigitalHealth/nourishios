//
//  StressCacheDataStore.swift
//  Nourish
//
//  Created by Nova on 2/23/17.
//  Copyright © 2017 SSIC. All rights reserved.
//

import UIKit
import RealmSwift
import RxSwift


class StressCacheDataStore: NSObject {
    
    static let shared = StressCacheDataStore()
    
    //let realm = try! Realm()
    let calendar = Calendar.current
    
    func restBPMFor(date : Date) -> Double {
        let array = fetchValuesFor(date: date)
        return (array.last?.bpmValue)!
        
    }
    
    func lastCachedValue() -> Observable<StressCacheRealm> {
        return Observable.create { observer in
            let realm = try! Realm()
            let obj = realm.objects(StressCacheRealm.self).sorted(byKeyPath: "timeStamp", ascending : false).first
            var stress = StressCacheRealm()
            
            if obj != nil && Calendar.current.isDateInToday((obj?.timeStamp)!) == true  {
                stress = obj!
            } else {
                stress.stressLevel = stressLevel.stringFromEnum(level: .Undetermined)
                stress.timeStamp = Date()
            }
            observer.onNext(stress)
            observer.onCompleted()
            
            return Disposables.create()
        }
        
    }
    
    private func fetchValuesFor(date : Date) -> [StressCacheRealm] {
        let realm = try! Realm()
        let startDate = calendar.startOfDay(for: date)
        let predicate = NSPredicate(format : "timeStamp == %@",startDate as NSDate)
        
        let results = realm.objects(StressCacheRealm.self).filter(predicate)
        let array = Array(results)
        return array
    }
    
    func store(object : StressCacheRealm) {
        let realm = try! Realm()
            try! realm.write {
                realm.add(object)
        }
    }
    
    func fetchFor(id : String) -> StressCacheRealm? {
        let realm = try! Realm()
        let predicate = NSPredicate(format : "idString == %@",id)
        let stress = realm.objects(StressCacheRealm.self).filter(predicate)
        
        let array = Array(stress)
        return array.first
    }
    func fetchForTime(stamp : Date) -> StressCacheRealm? {
        let realm = try! Realm()
        let predicate = NSPredicate(format : "timeStamp == %@",stamp as NSDate)
        let stress = realm.objects(StressCacheRealm.self).filter(predicate)
        
        let array = Array(stress)
        
        return array.first
    }
}

//
//  UserMessageInteractor.swift
//  Nourish
//
//  Created by Nova on 12/1/16.
//  Copyright © 2016 SSIC. All rights reserved.
//

import UIKit
import RxSwift

let kUserMessageKey = "kUserMessageKey"

class UserMessageRoutine: NSObject {
    let profileRepo = UserRepository.shared
    let activityRepo = UserActivityRepository()
    let artikRepo = ArtikCloudMessagesRepository()
    let toJsonMapper = UserMessage_to_Json()
    let deviceRoutine = UserDeviceRoutine()
    let deviceRepo = UserDevicesRepository.shared
    
    func getCurrentMessage() -> Observable<UserMessage> {
        let user = self.profileRepo.getCurrentUser(policy: .DefaultPolicy)
        let todaySteps = self.activityRepo.getUserStepsForToday()
        let calories = self.activityRepo.getUserCaloriesTotalBurned()
        //let steps10minutes = self.activityRepo.getStepsLast10Minutes()
        let height = self.activityRepo.getUserHeight()
        let weight = self.activityRepo.getUserWeight()
        //let hr10min = self.activityRepo.getUserHeartRateLast10Minutes()
        let message = UserMessage()
        
        return Observable.zip(user, todaySteps, calories, height, weight, resultSelector : { userValue, todayStepsValue, caloriesValue, heightValue, weightValue -> UserMessage in
            
            message.weight = (weightValue != nil) ? weightValue! : userValue.weight!
            message.height = (heightValue != nil) ? heightValue! : userValue.height!
            message.userId = userValue.userID
            message.activity = userValue.activityLevel?.description
            message.age = userValue.age
            message.dietrestriction = userValue.dietaryPreference
            message.userId = userValue.userID
            message.steps = todayStepsValue
            message.calories = caloriesValue
            //message.stepsFor10Minutes = steps10minutesValue
            message.gender = userValue.biologicalSex!.description
            //message.averageHeartRateFor10Minutes = hr10minValue
    
            return message
        })
            
    }
    
    func uploadUserMessage(completion : @escaping (Bool) ->()) {
        if NRUserSession.sharedInstance.accessToken != nil {
            let _ = self.profileRepo.getCurrentUser(policy: .DefaultPolicy).flatMap {user -> Observable<UserDevice> in
                return self.deviceRepo.currentDevice(userID: user.userID!)
                }.subscribe(onNext: { [unowned self] userDevice in
                    let flat = self.getCurrentMessage().flatMap{ message -> Observable<Bool> in
                        return self.artikRepo.uploadUserMessage(message: message)
                    }
                    let _ = flat.subscribe(onNext: {uploaded in
                    completion(uploaded)
                    }, onError: {error in }, onCompleted: {}, onDisposed: {})
                }, onError: {error in }, onCompleted: {}, onDisposed: {})
        }
    }
}

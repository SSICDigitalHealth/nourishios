//
//  RecognitionFoodListViewController.swift
//  Nourish
//
//  Created by Nova on 5/11/17.
//  Copyright © 2017 SSIC. All rights reserved.
//

import UIKit

protocol RecognitionViewContollerDelegate{
    func recognitionViewControllerDidAddedFood()
    func recognitionViewControllerDidCancel()
}
protocol RecognitionViewControllerProtocol {
    func showFoodDetailsViewFor(meal : foodSearchModel, delegate : FoodDetailsViewControllerDelegate, updatingAnmount : Bool)
    func performDelegateActionDidAdded()
    func performDelegateActionDidCancel()
}

class RecognitionFoodListViewController: BasePresentationViewController, RecognitionViewControllerProtocol {
    @IBOutlet var imagePickerView : ImageFoodPickerView!
    var foodImage : UIImage?
    var foods : [foodSearchModel]?
    var delegate : RecognitionViewContollerDelegate?
    var date : Date?
    var ocasion : Ocasion?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.baseViews = [self.imagePickerView!]
        self.imagePickerView.presenter?.imagePickerController = self
        self.imagePickerView.presenter?.date = self.date
        self.imagePickerView.presenter?.pickedOcasion = ocasion ?? .breakfast
        self.imagePickerView.foodImage = self.foodImage
        self.imagePickerView.foodList = self.foods
        
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func performDelegateActionDidCancel(){
        if self.delegate != nil {
            delegate?.recognitionViewControllerDidCancel()
        } else {
            NavigationUtility.dismiss(animated: true)
        }
    }
    
    func performDelegateActionDidAdded() {
        if self.delegate != nil {
            delegate?.recognitionViewControllerDidAddedFood()
        } else {
            NavigationUtility.navigateToFoodDiaryFor(date: Date())
        }
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
    }
    
    func showFoodDetailsViewFor(meal: foodSearchModel, delegate : FoodDetailsViewControllerDelegate, updatingAnmount : Bool) {
        let vc = NRFoodDetailsViewController(nibName: "NRFoodDetailsViewController", bundle: nil)
        vc.title = meal.foodTitle
        vc.foodSearchModel = meal
        vc.backgroundImage = NavigationUtility.takeScreenShot()
        vc.delegate = delegate
        vc.updatingAnmount = updatingAnmount
        vc.hideFavButton = true
        self.present(vc, animated: true, completion: nil)
    }

}

//
//  FoodMealPlannerView.swift
//  Nourish
//
//  Created by Vlad Birukov on 20.10.2017.
//  Copyright © 2017 SSIC. All rights reserved.
//

import UIKit

protocol FoodMealPlannerProtocol {
    func setupWith(nameFood: String, model: (foodSearchModel, RecipiesViewModel?), presenter: MealPlannerPresenter, i: Int, indexPath: IndexPath, index: Int)
}

class FoodMealPlannerView: BaseView, FoodMealPlannerProtocol {
    @IBOutlet weak var nameFood: UILabel!
    @IBOutlet weak var mealButton: UIButton!
    
    var presenter: MealPlannerPresenter?
    var model: RecipiesViewModel?
    var i = 0
    var indexPath = IndexPath(row: 0, section: 0)
    var index = 0

    let optifastName = "Optifast"
    
    override func viewDidLoad() {
        self.xibAutoLayouting(nibName: "FoodMealPlannerView")
        self.stopActivityAnimation()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    
    func setupWith(nameFood: String, model: (foodSearchModel, RecipiesViewModel?), presenter: MealPlannerPresenter, i: Int, indexPath: IndexPath, index: Int) {
        self.presenter = presenter
        self.model = model.1
        self.i = i
        self.indexPath = indexPath
        self.index = index
        
        self.nameFood.text = String(format: "%@", nameFood)
        
        if model.1 != nil {
            self.mealButton.setImage(UIImage(named: "recipe"), for: .normal)
        } else if model.0.foodId.range(of: self.optifastName) != nil  {
            self.mealButton.setImage(UIImage(named: "shoping_cart"), for: .normal)
        } else {
            self.mealButton.isEnabled = false
        }
    }
    
    @IBAction func navigationTo(_ sender: Any) {

        if self.presenter != nil && self.model != nil {
            self.presenter?.showRecipies(i: self.i, indexPath: self.indexPath, index: self.index)
        } else if self.presenter != nil  {
            self.presenter?.showOptifastFood()
        }
    }
}

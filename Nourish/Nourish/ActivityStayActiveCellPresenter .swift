//
//  ActivityStayActiveCellPresenter .swift
//  Nourish
//
//  Created by Vlad Birukov on 27.08.17.
//  Copyright © 2017 SSIC. All rights reserved.
//

import Foundation
import RxSwift



class ActivityStayActiveCellPresenter: ActivityBaseCellPresenter {
    
    let interactor = StayActiveInteractor()
    let mapperObjectStayActive = StayActiveModel_to_StayActiveViewMode()
    
    private func getInformation(cell: StayActiveTableViewCell, startDate: Date, endDate: Date) {
        cell.baseView.viewWillAppear(true)
        cell.baseView.removeActivityIndicator()
        self.subscribtions.append(self.interactor.execute(startDate: startDate, endDate: endDate).observeOn(MainScheduler.instance).subscribe(onNext: {stayActive in
                cell.setupWith(model: self.mapperObjectStayActive.transform(model: stayActive))
        }, onError: {error in
            cell.baseView.parseError(error: error,completion: nil)
        }, onCompleted: {
                self.reloadTable(cell: cell)
                cell.baseView.stopActivityAnimation()
        }, onDisposed: {
        }))
    }
    
    override func fetchModel(For baseCell: ActivityBaseCell) {
        super.fetchModel(For: baseCell)
        
        let cell = baseCell as! StayActiveTableViewCell
        if let delegate = self.delegate {
            let date = delegate.fetchEpoch()
            self.getInformation(cell: cell, startDate: date.startDate, endDate: date.endDate)
        }
    }
}

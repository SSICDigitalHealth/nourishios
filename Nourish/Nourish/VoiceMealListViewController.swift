//
//  VoiceMealListViewController.swift
//  Nourish
//
//  Created by Vino (Vinodha) Sundaramoorthy on 6/30/17.
//  Copyright © 2017 SSIC. All rights reserved.
//

import UIKit

protocol VoiceMealListDelegate {
    func soundHoundViewControllerDidAddedFood()
}

class VoiceMealListViewController: BasePresentationViewController {
    @IBOutlet weak var voiceMealListView : VoiceMealListView!
    
    
    var mealArray : [MealRecord] = []
    var date : Date!
    var ocasion : Ocasion!
    var delegate : VoiceMealListDelegate?

    override func viewDidLoad() {
        super.viewDidLoad()
        self.setUpNavigationBarButtons()
        self.voiceMealListView.controller = self
        self.baseViews = [voiceMealListView]
    }
    
    func setUpNavigationBarButtons () {
        let titleAttribute =  self.navigationBarTitleAttribute()

        let closeButton : UIBarButtonItem =  UIBarButtonItem(image: UIImage(named:"close_icon"), style: .plain, target: self, action: #selector(self.closeAction(_:)))
        navigationItem.title = kVoiceMealListVCTitle
        self.navigationController?.navigationBar.titleTextAttributes = titleAttribute
        self.navigationController?.navigationBar.barTintColor = NRColorUtility.symplifiedNavigationColor()
        self.navigationController?.navigationBar.tintColor = NRColorUtility.coolGrey()
        self.navigationController?.navigationItem.leftBarButtonItem = closeButton
        navigationItem.leftBarButtonItem = closeButton
    }
    
    @IBAction func addToDiary(sender:UIButton) {
        self.voiceMealListView.presenter.addToDiary()
    }
    
    func finishSave() {
        if self.delegate != nil {
                self.delegate?.soundHoundViewControllerDidAddedFood()
        } else {
            self.dismiss(animated: false, completion: nil)
        }
    }

}

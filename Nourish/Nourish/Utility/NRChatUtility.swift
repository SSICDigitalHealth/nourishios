 //
//  NRChatUtility.swift
//  Nourish
//
//  Created by Vino (Vinodha) Sundaramoorthy on 11/6/16.
//  Copyright © 2016 SSIC. All rights reserved.
//

import UIKit

let kImageWidth : CGFloat = 180

let chatAvatarWidth : CGFloat = 25.0
let chatPadding : CGFloat  = 30.0
let maxLabelSize: CGSize = CGSize(width: 200, height: 999)
let chatBubbleHeightPadding :CGFloat = 20.0
let bgImageViewWidthPadding :CGFloat = 0.0
let minAppChatHeight : CGFloat = 36.0
class ResponseButton : UIButton {
    var message  = ChatDataBaseItemViewModel()
}



class NRChatUtility: NSObject {
    
    //App message
    class func appChatWithMessage(message : String) -> UIView {
        let nourishChatView = UIView()
        let textFont = UIFont.systemFont(ofSize: 14)
        //chat avatar
        let avatarImageView = UIImageView(image: UIImage(named: kAppChatAvatarName)?.withRenderingMode(.alwaysOriginal))
        let paragraphStyle = NSMutableParagraphStyle()
        paragraphStyle.alignment = NSTextAlignment.justified
        paragraphStyle.firstLineHeadIndent = 10.0
        paragraphStyle.headIndent = 10.0
        let attributedString = NSAttributedString(string: message,
                                                  attributes: [
                                                    NSParagraphStyleAttributeName: paragraphStyle])
        //Message view
        let messageLabel = UILabel()
        messageLabel.backgroundColor = NRColorUtility.appChatBackgroundColor()
        messageLabel.textColor = NRColorUtility.appChatTextColor()
        messageLabel.font = textFont
        messageLabel.lineBreakMode = NSLineBreakMode.byWordWrapping
        messageLabel.attributedText = attributedString
        messageLabel.numberOfLines = 0
        messageLabel.textAlignment = .left
        messageLabel.shadowColor = UIColor.clear
        messageLabel.layer.borderColor = UIColor.clear.cgColor
        
        let stringHeight = message.sizeOfBoundingRect(width: maxLabelWidth(), height: maxLabelSize.height, font: textFont).height
        let messageWidth = message.sizeOfBoundingRect(width: maxLabelWidth(), height: maxLabelSize.height, font: textFont).width
        let messageHeightWithPaddding = stringHeight + chatBubbleHeightPadding < minAppChatHeight ? minAppChatHeight : stringHeight + chatBubbleHeightPadding
        let messageWidthWithPadding = chatAvatarWidth + chatPadding + messageWidth + 5
        
        let bubbleFrame = CGRect(x:chatAvatarWidth + 5,y:0,width:messageWidthWithPadding + 10,height:messageHeightWithPaddding + 5)
        let bgView = UIView(frame: bubbleFrame)
        bgView.backgroundColor = NRColorUtility.appChatBackgroundColor()
        
        avatarImageView.frame = CGRect(x: 0, y: 0, width: chatAvatarWidth , height: chatAvatarWidth)
        nourishChatView.addSubview(avatarImageView)
        messageLabel.frame = CGRect(x:10,y:0,width:messageWidthWithPadding,height:messageHeightWithPaddding)

        let maskLayer = CAShapeLayer()
        maskLayer.path = appChatMask(view:bgView).cgPath

        bgView.layer.mask = maskLayer
        bgView.addSubview(messageLabel)
        nourishChatView.addSubview(bgView)
        return nourishChatView
    }
    
   class func appChatMask(view : UIView) -> UIBezierPath {
        let xPosition : CGFloat = view.bounds.origin.x
        let yPosition = view.bounds.origin.y
        let width = view.bounds.origin.x + view.bounds.size.width
        let height = yPosition + view.bounds.size.height
        
        let maskPath = UIBezierPath()
        let firstPoint = CGPoint(x: xPosition, y: yPosition)
        let secondPoint = CGPoint(x: width , y: firstPoint.y)
        let thirdPoint = CGPoint(x: secondPoint.x, y: height)
        let fourthPoint = CGPoint(x: xPosition + 10, y: thirdPoint.y)
        let fifthPoint = CGPoint(x: fourthPoint.x, y: yPosition + 10)
        
        maskPath.move(to: firstPoint)
        maskPath.addLine(to: secondPoint)
        maskPath.addLine(to: thirdPoint)
        maskPath.addLine(to: fourthPoint)
        maskPath.addLine(to: fifthPoint)
        maskPath.addLine(to: firstPoint)
        return maskPath
    }
    
    //User chat message
    class func userChatWithMessage(message : String, photoID : String?) -> UIView {
        let nourishChatView = UIView()
        let textFont = UIFont.systemFont(ofSize: 14)

        //chat avatar
        let paragraphStyle = NSMutableParagraphStyle()
        paragraphStyle.alignment = NSTextAlignment.justified
        let attributedString = NSAttributedString(string: message,
                                                  attributes: [
                                                    NSParagraphStyleAttributeName: paragraphStyle])
        //Message view
        let messageLabel = UILabel()
        messageLabel.backgroundColor = NRColorUtility.userChatBackgroundColor()
        messageLabel.textColor = NRColorUtility.userChatTextColor()
        messageLabel.font = textFont
        messageLabel.lineBreakMode = NSLineBreakMode.byWordWrapping
        messageLabel.attributedText = attributedString
        messageLabel.numberOfLines = 0
        messageLabel.textAlignment = .center
        
        var yOffset = 0.0
        var xWidth : CGFloat = chatPadding
        
        if (photoID?.count)! > 0 {
            
            let imageWidth = UIScreen.main.bounds.width - 96
            let imageHeight = 0.244 * UIScreen.main.bounds.height
            xWidth = imageWidth

            
            let imageView = UIImageView()
            
            let imageBgFrame = CGRect(x: 0, y: 35, width: imageWidth, height: imageHeight)
            let imageBgView : UIView = UIView(frame: imageBgFrame)
            imageBgView.backgroundColor = NRColorUtility.userChatBackgroundColor()

            imageView.frame = CGRect(x:16,y:16,width:imageWidth - 40 ,height:imageHeight - 32)
            yOffset = Double(imageBgView.bounds.origin.y) + Double(imageBgView.bounds.height) + 45
            
            imageBgView.addSubview(imageView)
            nourishChatView.addSubview(imageBgView)

            let imageMaskLayer = CAShapeLayer()
            imageMaskLayer.path = userChatMask(view: imageBgView).cgPath
            imageBgView.layer.mask = imageMaskLayer
            imageView.contentMode = .scaleAspectFill
            
            let imageRepo = ImageRepository()
            let _ = imageRepo.getPhoto(userPhotoID: photoID!).subscribe(onNext: {image in
                DispatchQueue.main.async {
                    imageView.image = image
                }
            
            }, onError: {error in}, onCompleted: {
                
                
            }, onDisposed: {})
        }
        
        //Get the frame of the message based on the message length
        let stringHeight = message.sizeOfBoundingRect(width: self.maxLabelWidth(), height: maxLabelSize.height, font: textFont).height
        let messageWidth = message.sizeOfBoundingRect(width: self.maxLabelWidth(), height: maxLabelSize.height, font: textFont).width
        
        let messageWidthWithPadding = chatPadding  + messageWidth
        var messageHeightWithPadding = (photoID?.count)! > 0 ? stringHeight + 10 : stringHeight + chatBubbleHeightPadding
        messageHeightWithPadding = messageHeightWithPadding < 36 ? 36 : messageHeightWithPadding
        
        var xPoint = xWidth == chatPadding ? xWidth : xWidth - messageWidth
        
        if (photoID?.count)! > 0 {
            xPoint = xWidth - messageWidth - chatPadding
        }
        
        let bgFrame = CGRect(x: xPoint, y: CGFloat(yOffset), width: messageWidthWithPadding + 5, height: messageHeightWithPadding + 5)
        let bgView = UIView(frame: bgFrame)
        bgView.backgroundColor = NRColorUtility.userChatBackgroundColor()
        
        messageLabel.frame = CGRect(x:0,y:2,width:messageWidthWithPadding,height:messageHeightWithPadding)
        
        let maskLayer = CAShapeLayer()
        maskLayer.path = userChatMask(view: bgView).cgPath
        bgView.layer.mask = maskLayer
        bgView.addSubview(messageLabel)
        nourishChatView.addSubview(bgView)
        return nourishChatView
    }
    
    class func userChatMask(view : UIView) -> UIBezierPath {
        let xPosition = view.bounds.origin.x
        let yPosition = view.bounds.origin.y
        
        let firstPoint = CGPoint(x: xPosition, y: yPosition)
        let secondPoint = CGPoint(x:firstPoint.x,y:yPosition + view.bounds.size.height)
        let thirdPoint = CGPoint(x: xPosition + view.bounds.width - 10, y: secondPoint.y)
        let fourthPoint = CGPoint(x: thirdPoint.x, y: yPosition + 10)
        let fifthPoint = CGPoint(x: xPosition + view.bounds.width , y: yPosition)
        
        let maskPath =  UIBezierPath()
        maskPath.move(to: firstPoint)
        maskPath.addLine(to: secondPoint)
        maskPath.addLine(to: thirdPoint)
        maskPath.addLine(to: fourthPoint)
        maskPath.addLine(to: fifthPoint)
        maskPath.addLine(to: firstPoint)
        maskPath.close()
        return maskPath
    }
    
    class func dailyReportChatWith(message : ChatDataBaseItemViewModel) -> UIView {
        let nourishChatView = UIView()
        let textFont = UIFont.systemFont(ofSize: 14)
        //chat avatar
        let avatarImageView = UIImageView(image: UIImage(named: kAppChatAvatarName)?.withRenderingMode(.alwaysOriginal))
        let paragraphStyle = NSMutableParagraphStyle()
        paragraphStyle.alignment = NSTextAlignment.justified
        paragraphStyle.firstLineHeadIndent = 10.0
        paragraphStyle.headIndent = 10.0
        let attributedString = NSMutableAttributedString(string: message.content,
                                                  attributes: [
                                                    NSParagraphStyleAttributeName: paragraphStyle])
       
        let range = message.content.range(of: "/")
        let index: Int = message.content.distance(from: message.content.startIndex, to: (range!.lowerBound))
        attributedString.addAttributes([NSForegroundColorAttributeName : NRColorUtility.chatProgressColor()], range: NSMakeRange(0, index))

        let containerView = UIView()
        containerView.backgroundColor = NRColorUtility.appChatBackgroundColor()

        //Message view
        let messageLabel = UILabel()
        messageLabel.textColor = NRColorUtility.appChatTextColor()
        messageLabel.font = textFont
        messageLabel.lineBreakMode = NSLineBreakMode.byWordWrapping
        messageLabel.attributedText = attributedString
        messageLabel.numberOfLines = 0
        messageLabel.textAlignment = .left
        messageLabel.shadowColor = UIColor.clear
        messageLabel.layer.borderColor = UIColor.clear.cgColor
        let stringHeight = message.content.sizeOfBoundingRect(width: self.maxLabelWidth(), height: maxLabelSize.height, font: textFont).height
        let messageWidth = message.content.sizeOfBoundingRect(width: self.maxLabelWidth(), height: maxLabelSize.height, font: textFont).width
        
        let messageWidthWithPadding = chatAvatarWidth + chatPadding + messageWidth + 5
        let messageHeightWithPaddding = stringHeight + chatBubbleHeightPadding < minAppChatHeight ? minAppChatHeight : stringHeight + chatBubbleHeightPadding
        
        avatarImageView.frame = CGRect(x: 0, y: 0, width: chatAvatarWidth , height: chatAvatarWidth)
        nourishChatView.addSubview(avatarImageView)
        
        let bubbleFrame = CGRect(x:chatAvatarWidth + 5,y:0,width:messageWidthWithPadding + 10,height:messageHeightWithPaddding + 5)
        containerView.frame = bubbleFrame
        
        messageLabel.frame = CGRect(x:10,y:0, width:messageWidthWithPadding,height:messageHeightWithPaddding)
        containerView.addSubview(messageLabel)

        let maskLayer = CAShapeLayer()
        maskLayer.path = appChatMask(view: containerView).cgPath
        containerView.layer.mask = maskLayer
        nourishChatView.addSubview(containerView)
        return nourishChatView
    }
    
    class func loggedFoodChatMessage(message : String,date:Date) -> UIView {
        let realmRepo = ScoreFeedbackRepository()
        let scoreCahce =  realmRepo.getScoreFeedback(date:date)
        let showInfoIcon : Bool = scoreCahce.nutrientList.count >= 3 ? true : false
        let nourishChatView = UIView()
        let textFont = UIFont.systemFont(ofSize: 14)
        let scoreIconWidth : CGFloat = 16.0
        //chat avatar
        let avatarImageView = UIImageView(image: UIImage(named: kAppChatAvatarName)?.withRenderingMode(.alwaysOriginal))
        let paragraphStyle = NSMutableParagraphStyle()
        paragraphStyle.alignment = NSTextAlignment.justified
        paragraphStyle.firstLineHeadIndent = 10.0
        paragraphStyle.headIndent = 10.0
        let attributedString = NSAttributedString(string: message,
                                                  attributes: [
                                                    NSParagraphStyleAttributeName: paragraphStyle])
        
        //Message View
        let messageView = UIView()
        messageView.backgroundColor = NRColorUtility.appChatBackgroundColor()
        
        //Message Label
        let messageLabel = UILabel()
        messageLabel.backgroundColor = UIColor.clear
        messageLabel.textColor = NRColorUtility.appChatTextColor()
        messageLabel.font = textFont
        messageLabel.lineBreakMode = NSLineBreakMode.byWordWrapping
        messageLabel.attributedText = attributedString
        messageLabel.numberOfLines = 0
        messageLabel.textAlignment = .left
        messageLabel.shadowColor = UIColor.clear
        messageLabel.layer.borderColor = UIColor.clear.cgColor
        
        let stringHeight = message.sizeOfBoundingRect(width: self.maxLabelWidth() - scoreIconWidth - 30, height: maxLabelSize.height, font: textFont).height
        let messageWidth = message.sizeOfBoundingRect(width: self.maxLabelWidth() - scoreIconWidth - 30, height: maxLabelSize.height, font: textFont).width
        let messageHeightWithPaddding = stringHeight + chatBubbleHeightPadding < minAppChatHeight ? minAppChatHeight : stringHeight + chatBubbleHeightPadding
        
        avatarImageView.frame = CGRect(x: 0, y: 10, width: chatAvatarWidth , height: chatAvatarWidth)
        nourishChatView.addSubview(avatarImageView)
        messageLabel.frame = CGRect(x:8,y:0,width:chatAvatarWidth + chatPadding + messageWidth + 10.0,height:messageHeightWithPaddding)
        messageView.frame = CGRect(x:chatAvatarWidth + 5,y:0,width:messageLabel.frame.origin.x + messageLabel.frame.size.width + 20,height:messageHeightWithPaddding)

        let maskLayer = CAShapeLayer()
        maskLayer.path = appChatMask(view: messageView).cgPath
        messageView.layer.mask = maskLayer
        messageView.addSubview(messageLabel)

        // Add score feedback icon
        if showInfoIcon {
            let scFeedbackIcon = UIImageView(image: UIImage(named: kScoreFeedbackIcon)?.withRenderingMode(.alwaysOriginal))
            scFeedbackIcon.frame = CGRect(x: messageView.frame.size.width - 30 , y: messageLabel.center.y + 5, width: 16 , height: 16)
            messageView.addSubview(scFeedbackIcon)
        }
        nourishChatView.addSubview(messageView)
        return nourishChatView
    }
    
    private class func viewWith(image : UIImage, message : String) -> UIView {
        let view = UIView()
        let textFont = UIFont.systemFont(ofSize: 14)

        let paragraphStyle = NSMutableParagraphStyle()
        paragraphStyle.alignment = NSTextAlignment.justified
        paragraphStyle.firstLineHeadIndent = 10.0
        paragraphStyle.headIndent = 10.0
        let attributedString = NSAttributedString(string: message,
                                                  attributes: [
                                                    NSParagraphStyleAttributeName: paragraphStyle])
        let imageView = UIImageView(image: image)
        imageView.frame = CGRect(x:10, y: 0, width : image.size.width, height: image.size.height)
        view.backgroundColor = UIColor.clear
        let messageLabel = UILabel()
        messageLabel.backgroundColor = UIColor.clear
        messageLabel.textColor = NRColorUtility.appChatTextColor()
        messageLabel.font = textFont
        messageLabel.lineBreakMode = NSLineBreakMode.byWordWrapping
        messageLabel.attributedText = attributedString
        messageLabel.numberOfLines = 0
        messageLabel.textAlignment = .left
        messageLabel.shadowColor = UIColor.clear
        messageLabel.layer.borderColor = UIColor.clear.cgColor
        
        let messageWidth = message.sizeOfBoundingRect(width: self.maxLabelWidth() - 30, height: maxLabelSize.height, font: textFont).width
        
        messageLabel.frame = CGRect(x:imageView.frame.size.width + 10.0,y:0,width:messageWidth + 10.0,height:imageView.frame.size.height + 2)
        view.frame = CGRect(x:0,y:0,width:messageLabel.frame.origin.x + messageLabel.frame.size.width + imageView.frame.size.width + 30,height:imageView.frame.size.height)

        view.addSubview(imageView)
        view.addSubview(messageLabel)
        
        return view
    }
    
    
    class func chatInfoCell () -> UIView {
        let nourishChatView = UIView()
        let arrayOfPictures = [#imageLiteral(resourceName: "chatbot_home"), #imageLiteral(resourceName: "chatbot_camera"),#imageLiteral(resourceName: "chatbot_fooddiary"), #imageLiteral(resourceName: "chatbot_voice"), #imageLiteral(resourceName: "chatbot_activity"), #imageLiteral(resourceName: "chatbot_nutrition"),#imageLiteral(resourceName: "chatbot_mealplan")]
        let arrayOfLabels = ["Home", "Use camera", "Food diary", "Use voice", "Activity progress","Nutrition progress","Meal Planner"]
        let avatarImageView = UIImageView(image: UIImage(named: kAppChatAvatarName)?.withRenderingMode(.alwaysOriginal))
        let messageView = UIView()
        messageView.backgroundColor = NRColorUtility.appChatBackgroundColor()
        avatarImageView.frame = CGRect(x: 0, y: 0, width: chatAvatarWidth , height: chatAvatarWidth)
        var maxWidth : CGFloat = 0.0
        var yPosition : CGFloat = 10.0
        for index in 0...arrayOfLabels.count-1  {
            let view = self.viewWith(image: arrayOfPictures[index], message: arrayOfLabels[index])
            view.frame = CGRect(x:10.0, y : yPosition, width : view.frame.size.width, height : view.frame.size.height + 2)
            yPosition += view.frame.size.height + 10
            if view.frame.size.width > maxWidth {
                maxWidth = view.frame.size.width
            }
            messageView.addSubview(view)
        }
        messageView.frame = CGRect(x:chatAvatarWidth + 5,y:0,width:maxWidth + 20,height:yPosition)

        let maskLayer = CAShapeLayer()
        maskLayer.path = appChatMask(view: messageView).cgPath
        messageView.layer.mask = maskLayer

        nourishChatView.addSubview(messageView)
        nourishChatView.addSubview(avatarImageView)
        return nourishChatView
    }
    
    
    //User Response Button
    class func responseButtonWithMessage(message : ChatDataBaseItemViewModel) -> ResponseButton {
        
        let paragraphStyle = NSMutableParagraphStyle()
        paragraphStyle.alignment = NSTextAlignment.justified
        let textFont = UIFont.init(name: "Campton-Bold", size: 14)
       // _ = (4.0 + 7.0 + 5.0)/2.0
        
        //Message view
        let responseButton = ResponseButton()
        responseButton.message = message
        responseButton.setTitle(message.content, for: UIControlState.normal)
        responseButton.titleLabel?.font = textFont
        responseButton.titleLabel?.textAlignment = .center
        responseButton.tintColor = UIColor.white
        responseButton.backgroundColor = NRColorUtility.appBackgroundColor()
        return responseButton
    }
    
    class func getSizeOfString(message : String) -> CGSize {
        let textFont = UIFont.systemFont(ofSize: 14)
        let stringHeight = message.sizeOfBoundingRect(width: maxLabelWidth(), height: maxLabelSize.height, font: textFont).height
        let stringWidth = message.sizeOfBoundingRect(width: maxLabelWidth(), height: maxLabelSize.height, font: textFont).width
        
        let stringSize = CGSize(width: stringWidth + chatPadding + chatAvatarWidth, height: stringHeight + chatBubbleHeightPadding)
        return stringSize
    }
    
    class func horizontalPickerView() -> NRPickerView {
        let horizontalPickerView = NRPickerView()
        //Add horizontal Picker View
        horizontalPickerView.frame = CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: 72)
        horizontalPickerView.textColor = UIColor.white
        horizontalPickerView.highlightedTextColor = UIColor.white
        horizontalPickerView.font = UIFont.systemFont(ofSize: 24, weight: UIFontWeightRegular)
        horizontalPickerView.highlightedFont = UIFont.systemFont(ofSize: 24, weight: UIFontWeightBold)

        return horizontalPickerView
    }
    
    class func chatBubbleAnimation () -> CATransition {
        let transition = CATransition()
        transition.duration = 0.5
        transition.type = kCATransitionMoveIn
        transition.subtype = kCATransitionFromTop
        transition.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionEaseInEaseOut)
        return transition
    }

    class func maxLabelWidth() -> CGFloat {
        return UIScreen.main.bounds.width - 150
    }
}

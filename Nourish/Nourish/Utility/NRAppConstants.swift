// MARK:
// MARK:  NRAppConstants.swift
// MARK:  Nourish
// MARK:
// MARK:  Created by Vino (Vinodha) Sundaramoorthy on 10/31/16.
// MARK:  Copyright © 2016 SSIC. All rights reserved.
// MARK:

import UIKit

// MARK:Profile Cells
let kProfileNameCell = "kProfileNameCell"
let kProfilePickerCell = "kProfilePickerCell"
let kProfileListsTableViewCell = "kProfileListsTableViewCell"
let kListTableCell = "kListTableCell"


// MARK:Image Names
let kAppChatAvatarName = "chatavatar"
let kCloseIcon = "close_icon"
let kCloseWhiteIcon = "cansel_icon"
let kBackgroudNumberPicker = "picker_background"
let kHamburgerIcon = "hamburger_icon"
let kHeaderLogo = "header_logo"
let kFitnessIcon = "fitness_icon"
let kLifeStyleIcon = "lifestyle_icon"
let kFoodIcon = "food_icon"
let kScoreFeedbackIcon = "glyph_information"


// MARK:Pie Chat Legend
let kFitnessDesc = "Fitness"
let kLifeStyleDesc = "Lifestyle"
let kNutritionDesc = "Nutrition"

// MARK:Placeholder Text
let kFeedbackPlaceholderString = "Write your feedback or ask questions for help here."

// MARK:ViewController Title
let kFeedbackViewTitle = "Feedback And Help"
let kProfileSettingsViewTitle = "Profile Settings"

#if OPTIFASTVERSION
    let kManageGoalsViewTitle = "My Goal"
#else
    let kManageGoalsViewTitle = "Manage Goal"
#endif

let kRecommendationViewTitle = "Recommendations"
let kProgressViewTitle = "Progress"
let kNutritionStatsTitle = "Nutrition"
let kActivityStatsTitle = "Activity"
let kWeightStatsTitle = "Weight"
let kStressStatsTitle = "Stress"

// MARK:Onboarding questions
let kWelcomeMessage = "Hey there! \n\nI'm Nouri, your personal nutrition coach."
let kUserGoalMessage = "Let's get to know each other.\n\nWhat is the health goal that you would like to achieve?"
let kProfileSetupMessage = "What is your gender?"
let kAgeMessage = "What is your age?"
let kMetricsPreferenceMessage = "Select units of measure"
let kHeightMessage = "How tall are you?"
let kWeightMessage = "What is your weight?"
let kGoalPreferenceMessage = "Adjust your goal\npreference."
let kDietPreferenceMessage = "What is your dietary preference?"
let kActivityLevelMessage = "Select your activity level\n\nSedentary - low intensity\nactivities and leisure activities.\n\nModerate - 30 to 60 mins of\nactivity such as walking, 3-4\ntimes per week\n\nActive - 60+ mins of intense,\nphysical activity, 3-5 times per\nweek\n\nVery Active - 90+ mins of\nheavy and intense physical\nactivity, 5-6 times per week."
let kWelcomeBackMessage = "Welcome!"
let kTargetWeightMessage = "What is your target\nweight?"
let kAppleHealthPermissionsMessage = "Connect with Apple Health Kit?\n\nWe would like to sync your nutirition and fitness data with Apple Health Kit."
let kWeightLossRecommendationMessage = "Got it!"
let kWeightLossRecommendationMessage2 = "We suggest a stready approach.Lose %0.2f %@ per a week"
let kWatchQuestionMessage = "Would you like to track your activity and heart rate with an  Apple Watch or FitBit?"
let kMaintainWeightRecommendationMessage = "We will help you maintain your current weight by providing relevant food\nand activity recommendations"

// MARK:Chat Response
let kWelcomeResponseMessage = "Hello"
let kGoalResponseMessage1 = "Eat Right"
let kGoalResponseMessage2 = "Lose Weight"
let kGoalResponseMessage3 = "Maintain Weight"
let kSexResponseMessage1 = "Male"
let kSexResponseMessage2 = "Female"
let kMetricsResponseMessage1 = "Metric"
let kMetricsResponseMessage2 = "Imperial"
let kHeightResponseMessage1 = "I'm"
let kWeightResponseMessage1 = "I'm"
let kGoalConfirmationResponseMessage = "Let's Go"
let kDietResponseMessage1 = "No Restriction"
let kDietResponseMessage3 = "Vegetarian"
let kActiveLevelResponseMessage1 = "Sedentary"
let kActivityLevelResponseMessage2 = "Moderate"
let kActiveLevelResponseMessage3 = "Active"
let kActiveLevelResponseMessage4 = "Very Active"
let kSkipResponseMessage1 = "Skip"
let kAppleHealthResponseMessage = "Not Now"
let kAppleHealthResponseMessage2 = "Yes, Please"
let kWeightLossRecommendationResponseMessage = "Got it!"
let kWatchQuestionResponceMessage = "Apple watch"
let kWatchQuestionResponceMessage2 = "FitBit"
let kWatchQuestionResponceMessage3 = "Remind me later"

// MARK:Other 
let kProfilePicturePath = "nouriqProfilePic"
let kFirstTimeAppLaunch = "firstAppLaunch"
let kMetricsPreference = "metricsPreference"
let kCaloriesConsumed = "Calories Consumed"
let kRecommendationRecipesPicturePath = "optifastrecipes"
let kOptifastShopURL = URL(string: "http://www.optifast.nestlenutritionstore.com/s.nl/sc.1/.f?noopt=T&newcust=T&reset=T&login=T&utm_source=Medullan&utm_medium=app")!

// MARK:Food Diary
let kFootDiaryTitle = "Food Diary"
let kAddTitle = "ADD"


//MARK : Sentiance
let kSentianceAPIKey = "58106a0a36f0aad431000002"
let kSentianceSecretKey = "1a3362a0c4eb7c7e4954543c1e27c8f5f048db42ab00db047fb6e5ba783be13c4442c844f60fe62d69500d476ef64a8d6e09db41a711557340a75e40ef667341"


let kMinWeightMetric = 3
let kMaxWeightMetric = 200
let kMaxWeightImperial = 440
let kMaxAge = 120
let kMinHeight = 50
let kMaxHeight = 250
let kFavReload = "favoritesReloaded"

let kMailSubject = "OPTIforLife log"

#if !OPTIFASTVERSION
let kMailSupportAdress = "nestle.support@folium.cloud"
#else
let kMailSupportAdress = "optiforlifesupport@medullan.com"
#endif

let kForceCleanCache = "kForceCleanCache"

//
//  NRColorUtility.swift
//  Nourish
//
//  Created by Vino (Vinodha) Sundaramoorthy on 10/31/16.
//  Copyright © 2016 SSIC. All rights reserved.
//

import UIKit

class NRColorUtility: NSObject {
    
    //Hex to UIColor convertor
    class func hexStringToUIColor (hex:String) -> UIColor {
        var cString:String = hex.trimmingCharacters(in: .whitespacesAndNewlines).uppercased()
        
        if (cString.hasPrefix("#")) {
            cString.remove(at: cString.startIndex)
        }
        
        if ((cString.count) != 6) {
            return UIColor.gray
        }
        
        var rgbValue:UInt32 = 0
        Scanner(string: cString).scanHexInt32(&rgbValue)
        
        return UIColor(
            red: CGFloat((rgbValue & 0xFF0000) >> 16) / 255.0,
            green: CGFloat((rgbValue & 0x00FF00) >> 8) / 255.0,
            blue: CGFloat(rgbValue & 0x0000FF) / 255.0,
            alpha: CGFloat(1.0)
        )
    }
    
    //Home screen
    class func nourishHomeColor () -> UIColor {
        return NRColorUtility.hexStringToUIColor(hex: "#efefef")
    }
    //Main navigation bar
    class func nourishNavigationColor() -> UIColor {
        return NRColorUtility.hexStringToUIColor(hex: "#8b939f")
    }
    class func symplifiedNavigationColor () -> UIColor {
        return NRColorUtility.hexStringToUIColor(hex: "#f7f7f7")
    }

    class func aboutAppNavigationColor () -> UIColor {
        return NRColorUtility.hexStringToUIColor(hex: "#268c90")
    }
    
    class func feedbackNavigationColor () -> UIColor {
        return NRColorUtility.hexStringToUIColor(hex: "#8b949f")
    }
    
    class func appChatTextColor () -> UIColor {
        return NRColorUtility.hexStringToUIColor(hex: "#4a4a4a")
    }
    
    class func appChatBackgroundColor () -> UIColor {
        return  NRColorUtility.hexStringToUIColor(hex:"#ffffff")
    }
    
    class func userChatBackgroundColor () -> UIColor {
        return NRColorUtility.hexStringToUIColor(hex: "#70c397")
    }
    
    class func userChatTextColor () -> UIColor {
        return NRColorUtility.hexStringToUIColor(hex: "#ffffff")
    }
    
    class func responseButtonBackgroundColor () -> UIColor {
        return NRColorUtility.hexStringToUIColor(hex: "#41b6e6")
    }
    
    class func actionMenuTextColor () -> UIColor {
        return NRColorUtility.hexStringToUIColor(hex: "#616161")
    }
    class func buttonBoarderColor () -> UIColor {
        return NRColorUtility.hexStringToUIColor(hex: "#005170")
    }
    class func addButtonTextColor () -> UIColor {
        return NRColorUtility.hexStringToUIColor(hex: "#005170")
    }
    
    class func timeStampTextColor () -> UIColor {
        return NRColorUtility.hexStringToUIColor(hex: "#9b9b9b")
    }
    
    class func mineralsColor () -> UIColor {
        return NRColorUtility.hexStringToUIColor(hex: "#635da9")
    }
    
    class func vitaminsColor () -> UIColor {
        return NRColorUtility.hexStringToUIColor(hex: "#7ed321")
    }
    
    class func electrolytesColor () -> UIColor {
        return NRColorUtility.hexStringToUIColor(hex: "#f5a623")
    }
    
    class func macronutrientsColor () -> UIColor {
        return NRColorUtility.hexStringToUIColor(hex: "#4a90e2")
    }
    
    class func clockGrayColor () -> UIColor {
        return NRColorUtility.hexStringToUIColor(hex: "#c4c4c5")
    }
    
    class func activityColorBlue () -> UIColor {
        return NRColorUtility.hexStringToUIColor(hex: "#2768b4")
    }
    
    class func activityColorGreen () -> UIColor {
        return NRColorUtility.hexStringToUIColor(hex: "#a5f152")
    }
    
    class func activityColorLightBlue () -> UIColor {
        return NRColorUtility.hexStringToUIColor(hex: "#76a2d6")
    }
    
    class func fitnessPieColor () -> UIColor {
        return NRColorUtility.hexStringToUIColor(hex: "#892464")
    }
    
    class func lifeStylePieColor () -> UIColor {
        return NRColorUtility.hexStringToUIColor(hex: "#247789")
    }
    
    class func nutritionPieColor () -> UIColor {
        return NRColorUtility.hexStringToUIColor(hex: "#7cb816")
    }
    
    
    class func defaultHealthProgressColor () -> UIColor {
        return NRColorUtility.hexStringToUIColor(hex: "#d8d8d8")
    }
    
    class func healthGreenColor() -> UIColor {
        return NRColorUtility.hexStringToUIColor(hex: "#7ed321")
    }
    
    class func healthRedColor() -> UIColor {
        return NRColorUtility.hexStringToUIColor(hex: "#d0011b")
    }
    
    class func healthOrangeColor() -> UIColor {
        return NRColorUtility.hexStringToUIColor(hex: "#f6a623")
    }
    
    class func healthRingPlaceholderColor() -> UIColor {
        let color = NRColorUtility.hexStringToUIColor(hex: "#d8d8d8")
        return color
    }
    
    class func healthLabelPlaceholderColor() -> UIColor {
        let color = NRColorUtility.hexStringToUIColor(hex: "#ffffff")
        return color
    }
    
    class func StressGreenColor() -> UIColor {
        let color = NRColorUtility.hexStringToUIColor(hex: "#c5e68d")
        return color
    }
    
    class func progressLabelColor() -> UIColor {
        let color = NRColorUtility.hexStringToUIColor(hex: "#4a4a4a")
        return color
    }
    
    class func legendTextColor () -> UIColor {
        return NRColorUtility.hexStringToUIColor(hex: "#9b9b9b")
    }
    
    class func undeterminedStateColor () -> UIColor {
        return NRColorUtility.hexStringToUIColor(hex: "#f3f3f3")
    }
    
    class func searchBarBgColor () -> UIColor {
        return NRColorUtility.hexStringToUIColor(hex: "#3f4348")
    }
    
    class func foodLabelColor () -> UIColor {
        return NRColorUtility.hexStringToUIColor(hex: "#9b9b9b")
    }
    
    class func recommendationsHeaderColor () -> UIColor {
        return NRColorUtility.hexStringToUIColor(hex: "#efeff4")
    }
    class func activityBarChartFillColor () -> UIColor {
        return NRColorUtility.hexStringToUIColor(hex: "#7dd320")
    }
    
    class func activityBarChartAxisColor () -> UIColor {
        return NRColorUtility.hexStringToUIColor(hex: "#979797")
    }
    
    class func activityBarStepsColor() -> UIColor {
        let color = NRColorUtility.hexStringToUIColor(hex: "#4a4a4a")
        return color
    }
    
    class func weightLeftAxisColor () -> UIColor {
        let color = NRColorUtility.hexStringToUIColor(hex: "#417505")
        return color
    }
    
    class func weightRightAxisColor () -> UIColor {
        let color = NRColorUtility.hexStringToUIColor(hex: "#4a90e2")
        return color
    }
    
    class func subtitleColor () -> UIColor {
        let color = NRColorUtility.hexStringToUIColor(hex: "#e7e4e4")
        return color
    }
    
    class func dailyReportColor () -> UIColor {
        let color = NRColorUtility.hexStringToUIColor(hex: "#eeeeee")
        return color
    }
    
    class func comsumedExceedColor () -> UIColor {
        let color = NRColorUtility.hexStringToUIColor(hex: "#f5420a")
        return color
    }
    
    class func activityExceedColor () -> UIColor {
        let color = NRColorUtility.hexStringToUIColor(hex: "#005fa3")
        return color
    }
    
    class func comsumedColor () -> UIColor {
        let color = NRColorUtility.hexStringToUIColor(hex: "#f6a624")
        return color
    }
    
    class func activityColor () -> UIColor {
        let color = NRColorUtility.hexStringToUIColor(hex: "#0a9ccc")
        return color
    }
    
    class func reportLabelColor () -> UIColor {
        let color = NRColorUtility.hexStringToUIColor(hex: "#747474")
        return color
    }
    
    class func bucketBorderColor() -> UIColor {
        let color = NRColorUtility.hexStringToUIColor(hex: "#dddddd")
        return color
    }
    
    class func overconsumedColor() -> UIColor {
        let color = NRColorUtility.hexStringToUIColor(hex: "#f4420a")
        return color
    }
    
    class func reportActivityColor() -> UIColor {
        let color = NRColorUtility.hexStringToUIColor(hex: "#b3b3b3")
        return color
    }
    
    class func stressedTableViewStressCellBackgroundColor () -> UIColor {
        let color = NRColorUtility.hexStringToUIColor(hex: "#f5d9d9")
        return color
    }
    
    class func normalTableViewStressCellBackgroundColor () -> UIColor {
        let color = NRColorUtility.hexStringToUIColor(hex: "#bee890")
        return color
    }
    
    class func normalStressedTableViewStressCellFontColor () -> UIColor {
        let color = NRColorUtility.hexStringToUIColor(hex: "#009247")
        return color
    }
    
    class func stressedTableViewStressCellFontColor () -> UIColor {
        let color = NRColorUtility.hexStringToUIColor(hex: "#EB1C28")
        return color
    }
    
    class func undeterminedTableViewStressCellFontColor () -> UIColor {
        let color = NRColorUtility.hexStringToUIColor(hex: "#4a4a4a")
        return color
    }
    
    class func normalTableViewStressCellFontColor () -> UIColor {
        let color = NRColorUtility.hexStringToUIColor(hex: "#417505")
        return color
    }
    
    class func normalStressGraphColor () -> UIColor {
        let color = NRColorUtility.hexStringToUIColor(hex: "#7dd320")
        return color.withAlphaComponent(0.5)
    }
    
    class func calorieMeterInfoLabelColor () -> UIColor {
        let color = NRColorUtility.hexStringToUIColor(hex: "#808080")
        return color
    }
    
    class func calorieMeterActivityColor () -> UIColor {
        let color = NRColorUtility.hexStringToUIColor(hex: "#009dcb")
        return color
    }
    
    class func calorieMeterBudgetColor () -> UIColor {
        let color = NRColorUtility.hexStringToUIColor(hex: "#9e9e9e")
        return color
    }
    
    class func calorieMeterCloseButtonColor () -> UIColor {
        let color = NRColorUtility.hexStringToUIColor(hex: "#468b90")
        return color
    }
    
    class func goalHeaderColor () -> UIColor {
        let color = NRColorUtility.hexStringToUIColor(hex: "#6d6d72")
        return color
    }
    
    class func goalLabelColor () -> UIColor {
        let color = NRColorUtility.hexStringToUIColor(hex: "#8e8e93")
        return color
    }
    
    class func sourcesLabelColor () -> UIColor {
        let color = NRColorUtility.hexStringToUIColor(hex: "#417505")
        return color
    }
    class func chatIconColor () -> UIColor {
        let color = NRColorUtility.hexStringToUIColor(hex: "#88b600")
        return color
    }
    
    class func dotColor ()-> UIColor {
        let color = NRColorUtility.hexStringToUIColor(hex: "#9b9b9b")
        return color
    }
    
    class func deleteItemColor ()-> UIColor {
        let color = NRColorUtility.hexStringToUIColor(hex: "#d0011b")
        return color
    }
    
    class func favItemColor ()-> UIColor {
        let color = NRColorUtility.hexStringToUIColor(hex: "#7ed321")
        return color
    }
    
    class func nextRecommendationColor ()-> UIColor {
        let color = NRColorUtility.hexStringToUIColor(hex: "#4990e2")
        return color
    }
    
    class func chatProgressColor () -> UIColor {
        let color = NRColorUtility.hexStringToUIColor(hex: "#72c940")
        return color
    }
    
    class func chatProgressBackgroundColor () -> UIColor {
        let color = NRColorUtility.hexStringToUIColor(hex: "#999999")
        return color
    }
    
    class func taupeGrayColor () -> UIColor {
        let color = NRColorUtility.hexStringToUIColor(hex: "#898989")
        return color
    }
    class func calorieBudgetColor ()-> UIColor {
        let color = NRColorUtility.hexStringToUIColor(hex: "#393939")
        return color
    }
    
    class func nourishLightGray ()-> UIColor {
        let color = NRColorUtility.hexStringToUIColor(hex: "#d3d5da")
        return color
    }
    class func nourishNavBarFontColor () -> UIColor {
        let color = NRColorUtility.hexStringToUIColor(hex: "#4a4a4a")
        return color
    }
    
    class func newSearchBarBackgroundColor () -> UIColor {
       // let color = NRColorUtility.hexStringToUIColor(hex: "#f7f7f7")
        return UIColor.white
    }
    
    class func selectedTabColor () -> UIColor {
        let color = NRColorUtility.hexStringToUIColor(hex: "#4a4a4a")
        return color
    }
    
    class func voiceSearchPulseColor () -> UIColor {
        #if OPTIFASTVERSION
            return NRColorUtility.hexStringToUIColor(hex: "#b3e2f5")
        #else
            return NRColorUtility.hexStringToUIColor(hex: "#b7e1cb")
        #endif
    }
    
    class func coolGrey () -> UIColor {
        return NRColorUtility.hexStringToUIColor(hex: "#8b949f")
    }
    
    // Progress
    
    class func progressRightArrowColor () -> UIColor {
        let color = NRColorUtility.hexStringToUIColor(hex: "#BFBFBF")
        return color
    }

    class func progressDropColor () -> UIColor {
        let color = NRColorUtility.hexStringToUIColor(hex: "#1172B9")
        return color
    }

    class func progressScaleBackgroundColor() -> UIColor {
        let color = NRColorUtility.hexStringToUIColor(hex: "#70C397")
        return color
    }

    class func progressRecomendationIdea() -> UIColor {
        return NRColorUtility.hexStringToUIColor(hex: "#1172b9")
    }
    
    class func progressRecomendationRunningMan() -> UIColor {
        return NRColorUtility.hexStringToUIColor(hex: "#70c397")
    }
    
    class func progressTableBlackColor() -> UIColor {
        return NRColorUtility.hexStringToUIColor(hex: "#303030")
    }
    
    class func progressTableOverColor() -> UIColor {
        return NRColorUtility.hexStringToUIColor(hex: "#EC1C28")
    }
    
    class func progressChartNutritionColor() -> UIColor {
        return NRColorUtility.hexStringToUIColor(hex: "#FFFFFF")
    }
    
    class func progressNutritionFooterColor() -> UIColor {
        return NRColorUtility.hexStringToUIColor(hex: "#f2cc01")
    }
    
    class func progressChartNutritionOverColor() -> UIColor {
        return NRColorUtility.hexStringToUIColor(hex: "#EC1C28")
    }
    
    class func progressNutritionMinColor() -> UIColor {
        return NRColorUtility.hexStringToUIColor(hex: "#BFBFBF")
    }
    
    class func progressTableGrayColor() -> UIColor {
        return NRColorUtility.hexStringToUIColor(hex: "#808080")
    }
    
    class func orangeButtonColor() -> UIColor {
        return NRColorUtility.hexStringToUIColor(hex: "#41b6e6")
    }
    
    class func navigationTitleColor() -> UIColor {
        return NRColorUtility.hexStringToUIColor(hex: "#242424")
    }

    class func navigationTintColor() -> UIColor {
        return NRColorUtility.hexStringToUIColor(hex: "#808080")
    }
    
    class func nourishTextColor() -> UIColor {
        return NRColorUtility.hexStringToUIColor(hex: "#303030")
    }
    
    class func tooMuchColor() -> UIColor {
        return NRColorUtility.hexStringToUIColor(hex: "#ec1c28")
    }
    
    class func tooLittleColor() -> UIColor {
        return NRColorUtility.hexStringToUIColor(hex: "#ec1c28")
    }
    
    class func justRightColor() -> UIColor {
        return NRColorUtility.hexStringToUIColor(hex: "#70c397")
    }
    
    class func calorieMeterInfo() -> UIColor {
        return NRColorUtility.hexStringToUIColor(hex: "#303030")
    }
    
    class func mealPlannerColor() -> UIColor {
        return NRColorUtility.hexStringToUIColor(hex: "BFBFBF")
    }
    
    class func orangeColor() -> UIColor {
        return NRColorUtility.hexStringToUIColor(hex: "41b6e6")
    }
    
    class func backgroundScore() -> UIColor {
        return NRColorUtility.hexStringToUIColor(hex: "e2f3ea")
    }
    
    class func overConsumedColor() -> UIColor {
        return NRColorUtility.hexStringToUIColor(hex: "#ec1c28")
    }
    
    private class func nouriqBackgroundColor() -> UIColor {
        return NRColorUtility.hexStringToUIColor(hex: "#f16527")
    }
    
    private class func optifastBlueColor() -> UIColor {
        return NRColorUtility.hexStringToUIColor(hex: "#41B6E6")
    }
    
    
    private class func optifastCaloriesGraphColor () -> UIColor {
        return NRColorUtility.hexStringToUIColor(hex: "#B3E2F5")
    }
    
    private class func nouriqCaloriesGraphColor() -> UIColor {
        return NRColorUtility.hexStringToUIColor(hex: "#f8b293")
    }

    private class func optifastConsumedCaloriesDailyWeeklyColor() -> UIColor {
        return NRColorUtility.hexStringToUIColor(hex: "#b3e2f5")
    }
    
    private class func optifastOverConsumedCaloriesDailyWeeklyColor() -> UIColor {
        return NRColorUtility.hexStringToUIColor(hex: "#2291be")
    }
    
    private class func nouriqConsumedCaloriesDailyWeeklyColor () -> UIColor {
        return NRColorUtility.hexStringToUIColor(hex: "#f8b293")
    }
    
    private class func nouriqOverConsumedCaloriesDailyWeeklyColor() -> UIColor {
        return NRColorUtility.hexStringToUIColor(hex: "#B94C1D")
    }
    
    class func lifestyleBackgroundColor() -> UIColor {
        return self.appBackgroundColor().withAlphaComponent(0.2)
    }
    
    
    private class func nouriqHomeScreenIconsColor () -> UIColor {
        return NRColorUtility.hexStringToUIColor(hex: "#f4a281")
    }
    
    class func homeScreenIconsColor () -> UIColor {
        #if OPTIFASTVERSION
            return NRColorUtility.hexStringToUIColor(hex: "#B4E2F4")
        #else
            return self.nouriqHomeScreenIconsColor()
        #endif
    }
    
    class func backgroundLoginColor () -> UIColor {
        #if OPTIFASTVERSION
            return NRColorUtility.hexStringToUIColor(hex : "#E6F6FC")
        #else
            return self.nouriqBackgroundColor()
        #endif
    }
    
    
    class func consumedCaloriesDailyWeeklyColor () -> UIColor {
        #if OPTIFASTVERSION
            return self.optifastConsumedCaloriesDailyWeeklyColor()
        #else
            return self.nouriqCaloriesGraphColor()
        #endif
    }
    
    class func overConsumedCaloriesDailyWeeklyColor () -> UIColor {
        #if OPTIFASTVERSION
            return self.optifastOverConsumedCaloriesDailyWeeklyColor()
        #else
            return self.nouriqOverConsumedCaloriesDailyWeeklyColor()
        #endif
    }
    
    class func caloriesGraphBackgroundColor() -> UIColor {
        #if OPTIFASTVERSION
            return self.optifastCaloriesGraphColor()
        #else
            return self.nouriqCaloriesGraphColor()
        #endif
    }
    
    class func appBackgroundColor() -> UIColor {
        #if OPTIFASTVERSION
            return self.optifastBlueColor()
        #else
            return self.nouriqBackgroundColor()
        #endif
    }
    
    class func optifastProductFontColor() -> UIColor {
        return NRColorUtility.hexStringToUIColor(hex: "#303030")
    }
}



extension UIImageView {
    func imageNamedWithTint(named name: String, tintColor color:UIColor) {
        self.image = UIImage(named: name)?.withRenderingMode(.alwaysTemplate)
        self.tintColor = color
        self.backgroundColor = UIColor.clear
    }
}


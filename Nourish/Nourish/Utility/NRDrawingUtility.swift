//
//  NRDrawingUtility.swift
//  Nourish
//
//  Created by Vino (Vinodha) Sundaramoorthy on 12/6/16.
//  Copyright © 2016 SSIC. All rights reserved.
//

import UIKit

let defaultLegendRadius : CGFloat = 8.0
let legendCircleAndRadiusPadding : CGFloat = 8.0
let legendTextWidth : CGFloat = 70.0
let legendTextHeight : CGFloat = 20.0

typealias legends = (description : String, color:UIColor)

class NRDrawingUtility: NSObject {
    
    class func drawTextLayer(frame: CGRect, text : String , color: CGColor, bgColor:CGColor ,font:UIFont) -> CATextLayer {
        let textLayer = CATextLayer()
        textLayer.contentsScale = UIScreen.main.scale
        textLayer.alignmentMode = "center"
        textLayer.foregroundColor = color
        textLayer.backgroundColor = bgColor
        textLayer.string = text
        textLayer.font = font
        textLayer.frame = frame
        textLayer.fontSize = font.pointSize
        return textLayer
    }
    
    class func drawCircularLegend(center:CGPoint,fillColor:CGColor) -> CAShapeLayer{
        //Draw legend circle
        let legendCirclePath = UIBezierPath(arcCenter: CGPoint(x:center.x,y:center.y), radius: defaultLegendRadius, startAngle: 0, endAngle:CGFloat(Double.pi * 2), clockwise: true)
        let legendCircleLayer = CAShapeLayer()
        legendCircleLayer.contentsScale = UIScreen.main.scale
        legendCircleLayer.fillColor = fillColor
        legendCircleLayer.path = legendCirclePath.cgPath
        legendCirclePath.close()
        return legendCircleLayer
    }
    
    class func drawImageLayer(bounds:CGRect, center:CGPoint , bgColor:CGColor, imageName:String) -> CALayer {
        let imageLayer = CALayer()
        imageLayer.position = center
        imageLayer.bounds = bounds
        imageLayer.backgroundColor = bgColor
        imageLayer.contents = UIImage(named:imageName)?.cgImage
        imageLayer.contentsScale = UIScreen.main.scale
        return imageLayer
    }
    
    class func verticalDelimterLine(frame : CGRect) -> UIView{
        let verticalLine = UIView.init(frame: frame)
        verticalLine.backgroundColor = NRColorUtility.healthRingPlaceholderColor()
        return verticalLine
    }

}

extension UIView {
    func rotate(_ toValue: CGFloat, duration: CFTimeInterval = 0.2) {
        let animation = CABasicAnimation(keyPath: "transform.rotation")
        
        animation.toValue = toValue
        animation.duration = duration
        animation.isRemovedOnCompletion = false
        animation.fillMode = kCAFillModeForwards
        
        self.layer.add(animation, forKey: nil)
    }
}

//
//  BaseViewProtocol.swift
//  Nourish
//
//  Created by Nova on 11/16/16.
//  Copyright © 2016 SSIC. All rights reserved.
//

import Foundation

protocol BaseViewProtocol {
    
    func viewWillAppear(_ animated : Bool)
    func viewDidAppear(_ animated : Bool)
    func viewWillDisappear(_ animated : Bool)
    func viewDidDisappear(_ animated : Bool)
    func viewDidLoad()
    func viewDidLayoutSubviews()
    func parseError(error : Error, completion:((Bool) -> Void)?)
    func startActivityAnimation()
    func stopActivityAnimation()
    func showBanner(message : String, showingError : Bool, completion:((Bool) -> Void)?) -> UIView
}

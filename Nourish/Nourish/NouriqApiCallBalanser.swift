//
//  NouriqApiCallBalanser.swift
//  Nourish
//
//  Created by Nova on 10/6/17.
//  Copyright © 2017 SSIC. All rights reserved.
//

import UIKit
import RxSwift

class progressApiEmul : NSObject {
    var start : Date?
    var end : Date?
    var returnHistory : Bool?
    var progressReq = PublishSubject<Progress>()
    
    
    func customCompare(_ object: progressApiEmul?) -> Bool {
        if self.start == object?.start && self.end == object?.end && self.returnHistory == object?.returnHistory {
            return true
        }
        return false
    }
}

class NouriqApiCallBalanser: NSObject {
    let nestleRepo = NestleProgressRepository.shared
    static let shared = NouriqApiCallBalanser()
    private var arrayInProgress : [progressApiEmul] = []
    
    private func isProgressScreenExist(start : Date, end : Date, returnHistory : Bool) -> PublishSubject<Progress>? {
        let proxy = progressApiEmul()
        proxy.start = start
        proxy.end = end
        proxy.returnHistory = returnHistory
        
        var obj : PublishSubject<Progress>?
        for object in self.arrayInProgress {
            if object.customCompare(proxy) {
                obj = object.progressReq
            }
        }
        
        return obj
    }
    
    private func addProgress(start : Date, end : Date, returnHistory : Bool) -> PublishSubject<Progress> {
        let proxy = progressApiEmul()
        proxy.start = start
        proxy.end = end
        proxy.returnHistory = returnHistory
        proxy.progressReq = self.nestleRepo.getReloadedProgress(startDate: start, endDate: end, returnHistory: returnHistory)
        
        self.arrayInProgress.append(proxy)
        return proxy.progressReq
    }
    
    func removeProgress(start : Date, end : Date, returnHistory : Bool) {
        let proxy = progressApiEmul()
        proxy.start = start
        proxy.end = end
        proxy.returnHistory = returnHistory
        
        var indexToDelete : Int?
        if self.arrayInProgress.count > 0 {
            for index in 0...self.arrayInProgress.count - 1 {
                let obj = self.arrayInProgress[index]
                if obj.customCompare(proxy) == true {
                    indexToDelete = index
                }
            }
        }
        
        if indexToDelete != nil {
            self.arrayInProgress.remove(at: indexToDelete!)
        }
    }
    
    func getProgressFor(start : Date, end : Date, returnHistory : Bool) -> PublishSubject<Progress> {
        if let subj = self.isProgressScreenExist(start: start, end: end, returnHistory: returnHistory) {
            return subj
        } else {
            return self.addProgress(start:start,end:end,returnHistory:returnHistory)
        }
    }
    
    
}






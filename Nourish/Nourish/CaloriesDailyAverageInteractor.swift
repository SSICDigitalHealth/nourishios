//
//  CaloriesDailyAverageInteractor.swift
//  Nourish
//
//  Created by Vlad Birukov on 20.06.17.
//  Copyright © 2017 SSIC. All rights reserved.
//

import Foundation
import RxSwift

class CaloriesDailyAverageInteractor {
    private let calorieRepo = CaloricRepository()
    
    func execute (startDate: Date, endDate: Date) -> Observable <CaloriesDailyAverageModel> {
        return self.calorieRepo.getAverageCalorieModelFor(startDate:startDate, endDate:endDate)
//        return Observable<CaloriesDailyAverageModel>.create{(observer) -> Disposable in
//            let pause = Int(arc4random_uniform(4))
//            let dispat = DispatchTime.now() + .seconds(pause)
//            DispatchQueue.main.asyncAfter(deadline: dispat, execute: {
//                let caloriesDailyAverage = CaloriesDailyAverageModel()
//                caloriesDailyAverage.caloriesDailyAverage = Double(arc4random_uniform(9000))
//                caloriesDailyAverage.caloriesOverTarget = Double(arc4random_uniform(50))
//
//                
//                observer.onNext(caloriesDailyAverage)
//                observer.onCompleted()
//            })
//            
//            return Disposables.create()
//        }
    }

}

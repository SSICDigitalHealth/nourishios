//
//  UserRepository.swift
//  Nourish
//
//  Created by Nova on 11/23/16.
//  Copyright © 2016 SSIC. All rights reserved.
//

import UIKit
import RxSwift
import RealmSwift

let kTargetSteps = "target_steps_count"
let kDefaultStepsTarget = 6000.0

final class UserRepository: NSObject {
    
    var realmRepo = RealmUserRepository()
    
    private var cache : UserProfileCacheRealm {
        get {
            let repo = RealmUserRepository()
            return repo.getDefaultUser()
        }
    }
    
    static let shared = UserRepository()
    
    let artik = ArtikCloudConnectionManager()
    let mapperCacheToObject = UserProfileCacheRealm_to_UserProfile()
    let mapperObjectToCache = UserProfile_to_UserProfileCacheRealm()
    //let mapperToJson = UserProfileCache_to_Json()
    //let deviceRepo = UserDevicesRepository()
    let nourishDS = NourishDataStore.shared
    
    func getCurrentUser(policy : FetchingPolicy) -> Observable<UserProfile> {

        switch policy {
        case .Cached:
            let object = self.fetchUserDataCached().flatMap({userProfileCache -> Observable<UserProfile> in
                return Observable.just(self.mapperCacheToObject.transform(userCache: userProfileCache))
            })
            return object
        case .ForceReload:
            let object = self.fetchUserDataForceReloaded().flatMap({userProfileCache -> Observable<UserProfile> in
                return self.calculateCalorieBudgetAndReturnProfile(userProfileCache: userProfileCache)
            })
            return object
        case .DefaultPolicy:
            if self.is5minutesPastLastSync() == true {
                let object = self.fetchUserDataCached().flatMap({userProfileCache -> Observable<UserProfile> in
                    return Observable.just(self.mapperCacheToObject.transform(userCache: userProfileCache))
                })
                return object
            } else {
                let object = self.fetchUserDataForceReloaded().flatMap({userProfileCache -> Observable<UserProfile> in
                    return self.calculateCalorieBudgetAndReturnProfile(userProfileCache: userProfileCache)
                })
                return object
            }
        }
    }
    
    private func calculateCalorieBudgetAndReturnProfile(userProfileCache : UserProfileCacheRealm) -> Observable<UserProfile> {
        let userProfile = self.mapperCacheToObject.transform(userCache: userProfileCache)
        let _ = userProfile.calculateAndStoreCaloriePlan()
        return Observable.just(userProfile)
    }
    
    func getCurrentUserID() -> Observable<String> {
        return self.fetchUserDataForceReloaded().flatMap { user -> Observable<String> in
            var result = ""
            if user.userID != nil {
                result = user.userID!
            }
            
            return Observable.just(result)
        }
    }
    
    func getUserName() -> Observable<String> {
        return self.fetchUserDataCached().flatMap { user -> Observable<String> in
            if user.userFirstName != nil {
                if user.userLastName != nil {
                    return Observable.just("\(user.userFirstName!) \(user.userLastName!)")
                } else {
                    return Observable.just("\(user.userFirstName!)")
                }
            } else {
                return Observable.just("")
            }
            
        }
    }
    
    func getUserJoinedString() -> Observable<String> {
        return self.fetchUserDataCached().flatMap {user -> Observable<String> in
            let formatter = DateFormatter()
            formatter.dateFormat = "MMMM yyyy"
            formatter.locale = Locale(identifier: "en_US")
            var stringToReturn = ""
            
            if user.activeSinceTimeStamp != nil {
                let interval = (user.activeSinceTimeStamp?.timeIntervalSince1970)! / 1000.0
                let correctDate = Date(timeIntervalSince1970 : interval)
                let dateString = formatter.string(from: correctDate)
                stringToReturn = "Joined \(dateString)"
            }
            
           return Observable.just(stringToReturn)
        }
    }

    func getUserRecommendedSteps() -> Observable<Double> {
        return Observable.just(kDefaultStepsTarget)
    }
    
    func is5minutesPastLastSync() -> Bool {
        let dateLastSynced = self.cache.lastTransferredToArtik
        if dateLastSynced != nil {
            let calendar = Calendar.current
            let date = calendar.date(byAdding: .day, value: -1, to: Date())
            return dateLastSynced?.compare(date!) == .orderedDescending ? true : false
        }
        return false
    }
    
    
    func storeCurrentUserInBackground(user : UserProfile, writeToHealth : Bool) {
        let backgroundQueue = DispatchQueue(label: "com.uploadbackgrounduserque", qos: .background)
        let _ = self.storeCurrentUser(user: user, writeToHealth: writeToHealth, needToRefreshToken: true).observeOn(ConcurrentDispatchQueueScheduler(queue : backgroundQueue)).subscribe(onNext: { artikError in
            if artikError == nil {
                LogUtility.logToFile("Completed background user uploading to Artik")
            } else {
                LogUtility.logToFile("Error uploading user to Artik in background")
            }
        }, onError: { error in
            LogUtility.logToFile("Error uploading user to Artik in background",error)
        }, onCompleted: {
            LogUtility.logToFile("Completed background user uploading to Artik")
        }, onDisposed: {})
    }
    
    func storeCurrentUser(user : UserProfile, writeToHealth : Bool, needToRefreshToken : Bool) -> Observable<ArtikError?> {
        realmRepo.storeDefault(userCache: self.mapperObjectToCache.transform(userProfile: user))
       // self.cache.realm?.beginWrite()
//        self.cache.lastTransferredToArtik = Date()
//        try! cache.realm?.commitWrite()
//        realmRepo.storeDefault(userCache: self.cache)
        if writeToHealth{
            self.storeToHealthKit(user: self.cache)
        }
        let storeProps = self.artik.storeUserProperties(user: self.cache, method: "PUT")
        if needToRefreshToken == true {
            return storeProps.flatMap({error -> Observable<ArtikError?> in
                if error != nil {
                    return Observable.just(error)
                } else {
                    if NRUserSession.sharedInstance.refreshToken != nil && NRUserSession.sharedInstance.accessToken != nil {
                        return self.artik.refreshToken(refreshToken: NRUserSession.sharedInstance.refreshToken!, oldAccessToken: NRUserSession.sharedInstance.accessToken!).flatMap({token -> Observable<ArtikError?> in
                            if token.count > 0 {
                                LogUtility.logToFile("Refreshed token after user saving ", token)
                                return Observable.just(nil)
                            } else {
                                return Observable.just(self.generateArtikErrorWith(description: "Empty acces token from artik"))
                            }
                        })
                    } else {
                        return Observable.just(self.generateArtikErrorWith(description: "No refresh or acces token"))
                    }
                }
            })
        } else {
            return storeProps
        }
    }
    
    
    private func generateArtikErrorWith(description : String) -> ArtikError {
        let error = ArtikError()
        error.errorDescription = description
        return error
    }
    
    private func storeToHealthKit(user : UserProfileCacheRealm) {
        if user.height.value != nil {
            ActivityUtility.storeHeight(height: user.height.value!)
        }
        if user.weight.value != nil {
            ActivityUtility.storeWeight(weight: user.weight.value!, date:Date())
        }
    }
    
    private func fetchUserDataForceReloaded() -> Observable<UserProfileCacheRealm> {
        return Observable.create { observer in
            
            let profile = self.artik.receiveCurrentUser()
            let properties = self.artik.receiveCurrentUserProperties()
                let _ = profile.subscribe(onNext: { [weak self] user in
                    if let instance = self {
                        instance.storeUserEntityProfile(dict: user)
                    }
                }, onError: { error in
                    observer.onError(error)
                }, onCompleted: {
                    let _ = properties.subscribe(onNext: { [weak self] properties in
                        if let instance = self {
                            instance.storeUserProperties(dict: properties)
                        }
                    }, onError: { [weak self] error in
                        let errorArtik = error as! ArtikError
                        LogUtility.logToFile(error, error.localizedDescription)
                        if errorArtik.code != nil && errorArtik.code! == 1702 {
                            if let instance = self {
                                let _ = instance.artik.storeUserProperties(user: instance.cache, method: "POST").subscribe(onNext: { error in
                                    }, onError: {error in}, onCompleted: { [weak self] in
                                        if let instance = self {
                                            NRUserSession.sharedInstance.userID = instance.cache.userID
                                            observer.onNext(instance.cache)
                                        }
                                        observer.onCompleted()
                                }, onDisposed: {})
                            }
                        }
                    }, onCompleted: { [weak self] in
                        if let instance = self {
                            observer.onNext(instance.cache)

                        }
                        observer.onCompleted()
                    }, onDisposed: {})
                }, onDisposed: {
                })
            
            return Disposables.create()
        }
    }    
    
    private func fetchUserDataCached() -> Observable<UserProfileCacheRealm> {
         return Observable.create { observer in
            print("returning cached user")
            let realm = try! Realm()
            if self.cache.userID == nil {
                try! realm.write {
                    self.cache.userID = NRUserSession.sharedInstance.userID ?? ""
                }
            }
            
            observer.onNext(self.cache)
            
            observer.onCompleted()
            return Disposables.create()
        }
    }
    
    private func storeUserEntityProfile(dict : Dictionary<String, Any>) {
        if dict["id"] != nil {
            
            cache.realm?.beginWrite()
            let proxyCache = cache
            proxyCache.userID = dict["id"] as! String?
            NRUserSession.sharedInstance.userID = proxyCache.userID
            proxyCache.activeSinceTimeStamp = Date.init(timeIntervalSince1970: dict["createdOn"] as! TimeInterval)
            let fullName = dict["fullName"] as! String
            var fullNameArr = fullName.split{$0 == " "}.map(String.init)
            proxyCache.email = dict["email"] as? String
            if fullNameArr.count > 1 {
                proxyCache.userFirstName = fullNameArr[0]
                proxyCache.userLastName = fullNameArr[1]
            } else {
                proxyCache.userFirstName = fullNameArr[0]
            }
            try! cache.realm?.commitWrite()

        } else {
            DispatchQueue.main.async {
                NRUserSession.sharedInstance.logout(keepCurrent: false)
                NavigationUtility.changeRootViewController(userProfile: nil, awakedFromBackground: false)
            }
            
        }
        
    }
    
    private func storeUserProperties(dict : Dictionary<String, Any>) {
        if dict["error"] == nil {
            cache.realm?.beginWrite()
            let proxyCache = cache
            proxyCache.lastTransferredToArtik = Date()
            proxyCache.height.value = dict["height"] as? Double
            proxyCache.weight.value = dict["weight"] as? Double
            proxyCache.age.value = dict["age"] as? Int
            proxyCache.activityLevel = dict["activity"] as? String
            proxyCache.dietaryPreference = dict["dietrestriction"] as? String
            proxyCache.biologicalSex = dict["gender"] as? String
            
            if dict["isMetric"] != nil {
                if dict["isMetric"] as? String == "Metrics" {
                    let prefs : MetricPreference = .Metric
                    proxyCache.metricPreference = prefs.description
                } else if dict["isMetric"] as? String == "Imperial" {
                    let prefs : MetricPreference = .Imperial
                    proxyCache.metricPreference = prefs.description
                } else {
                    let metricPrefs = MetricPreference.enumFromBool(isMetric: dict["isMetric"] as! Bool)
                    proxyCache.metricPreference = metricPrefs?.description
                }
            }
           
            proxyCache.goalPrefsActivity.value = dict["goalPrefsActivity"] as? Float
            proxyCache.goalPrefsLifestyle.value = dict["goalPrefsLifestyle"] as? Float
            proxyCache.goalPrefsFood.value = dict["goalPrefsFood"] as? Float
            proxyCache.userGoal = dict["goal"] as? String
            //
            
            
            if dict["goal"] != nil && UserGoal.enumFromString(string: dict["goal"] as! String) == .LooseWeight  {
                proxyCache.startWeightTest.value = dict["startWeight"] as? Double
                proxyCache.goalTest.value = dict["goalValue"] as? Double
                
                if dict["weightLossStartDate"] != nil {
                    proxyCache.weightLossStartDate = self.dateFromUnixStamp(time: dict["weightLossStartDate"] as! Double)
                } else {
                    proxyCache.weightLossStartDate = Date()
                }
                
            }
            
            if dict["goal"] != nil && UserGoal.enumFromString(string: dict["goal"] as! String) == .MaintainWeight {
                    if dict["maintain_weight"] != nil {
                        proxyCache.weightToMaintain.value = dict["maintain_weight"] as? Double
                    } else {
                        proxyCache.weightToMaintain.value = proxyCache.weight.value
                    }
                }
            
            #if OPTIFASTVERSION
                if dict["goal"] == nil {
                    proxyCache.userGoal = UserGoal.MaintainWeight.description
                }
            #endif
            
            proxyCache.listOfOptifastFoodIds.removeAll()
            if dict["optifast_products"] != nil {
                let arrayOfIDs = dict["optifast_products"] as! [String]
                if arrayOfIDs.count > 0 {
                    let _ = arrayOfIDs.map({
                        let realmString = RealmString()
                        realmString.stringValue = $0
                        proxyCache.listOfOptifastFoodIds.append(realmString)
                    })
                }
            }
            
            proxyCache.listOfOptifastOcasions.removeAll()
            if dict["optifast_occasions"] != nil {
                let arrayOfOcasionsString = dict["optifast_occasions"] as! [String]
                if arrayOfOcasionsString.count > 0 {
                    let _ = arrayOfOcasionsString.map({
                       let realmInt = RealmInt()
                      realmInt.value = Ocasion.enumFromMealPlanner(string: $0).rawValue
                        proxyCache.listOfOptifastOcasions.append(realmInt)
                    })
                }
            }
            
            

            try! cache.realm?.commitWrite()
        }
    }
    
    func storeUserFeedBack(message : String, userID : String, token : String, date : TimeInterval, log : String) -> Observable<Bool> {
        return self.nourishDS.storeUserFeedback(feedback: message, userID: userID, token: token, timeStamp: date, log: log).flatMap { data -> Observable<Bool> in
            Observable.just(true)
        }
    }
    
    
    private func dateFromUnixStamp(time : Double) -> Date {
        let interval = time / 1000
        let date = Date(timeIntervalSince1970 : interval)
        return date
    }

}

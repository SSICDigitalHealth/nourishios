//
//  ProgressReportScoreDotView.swift
//  Nourish
//
//  Created by Andrey Zmushko on 7/19/17.
//  Copyright © 2017 SSIC. All rights reserved.
//

import UIKit

class ProgressReportScoreDotView: UIView {

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        backgroundColor = UIColor.white
    }

    override func draw(_ rect: CGRect) {
        let path = UIBezierPath()
        path.move(to: CGPoint(x: 0, y: 0))
        path.addLine(to: CGPoint(x: rect.width, y: 0))
        path.addLine(to: CGPoint(x: rect.width / 2, y: rect.height))
        NRColorUtility.appBackgroundColor().setFill()
        path.fill()
    }
}

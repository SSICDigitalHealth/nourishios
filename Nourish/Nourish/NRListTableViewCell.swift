//
//  NRListTableViewCell.swift
//  Nourish
//
//  Created by Vino (Vinodha) Sundaramoorthy on 11/9/16.
//  Copyright © 2016 SSIC. All rights reserved.
//

import UIKit

class NRListTableViewCell: UITableViewCell {
    
    @IBOutlet weak var listName : UILabel!
    @IBOutlet weak var slectedImage : UIImageView!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}

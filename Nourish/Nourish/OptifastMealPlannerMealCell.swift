//
//  OptifastMealPlannerMealCell.swift
//  Optifast
//
//  Created by Gena Mironchyk on 1/9/18.
//  Copyright © 2018 SSIC. All rights reserved.
//

import UIKit

class RecipeButton : UIButton {
    var section = 0
    var index = 0
}


class OptifastMealPlannerMealCell: UITableViewCell {

    @IBOutlet weak var foodNameLabel : UILabel!
    @IBOutlet weak var foodButton : RecipeButton!
    @IBOutlet weak var buyButton : UIButton!
    override func awakeFromNib() {
        super.awakeFromNib()
        
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func setupWith(food : (meal : foodSearchModel, mealReciepe : RecipiesViewModel?)) {
        self.foodNameLabel.text = food.meal.foodTitle
        print(food.meal.foodId)
    
        self.foodButton.isHidden = true
        self.foodButton.isEnabled = false

        self.buyButton.isHidden = true
        self.buyButton.isEnabled = false
        
        self.buyButton.setImage(UIImage(named: "shoping_cart"), for: .normal)
        self.foodButton.setImage(UIImage(named: "recipe"), for: .normal)

        self.selectionStyle = .none
        
        self.foodNameLabel.textColor = NRColorUtility.hexStringToUIColor(hex: "#808080")
        if food.mealReciepe != nil {
            self.foodButton.isHidden = false
            self.foodButton.isEnabled = true
            self.buyButton.isHidden = true
            self.buyButton.isEnabled = false
            
        } else if food.meal.foodId.range(of: "[opti]") != nil  {
            self.foodNameLabel.textColor = NRColorUtility.optifastProductFontColor()
            self.buyButton.isHidden = false
            self.buyButton.isEnabled = true
            self.foodButton.isHidden = true
            self.foodButton.isEnabled = false
            self.selectionStyle = .default

        }
        
    }
    
}

//
//  OptifastProductDetailsView.swift
//  Nourish
//
//  Created by Gena Mironchyk on 10/20/17.
//  Copyright © 2017 SSIC. All rights reserved.
//

import UIKit
let optifastDetailsPictureCellID = "pictureCellID"
let optifastDetailsNutrientsCellID = "kNutrientCell"
let optSecCellID = "optifastSectionsDetailsCellID"
protocol OptifastDetailsViewProtocol : BaseViewProtocol {
    func reloadData()
     func reloadSectionAt(index : Int)
}

class OptifastProductDetailsView: BaseView, OptifastDetailsViewProtocol {
    
    @IBOutlet weak var optifastDetailsTableView : UITableView!
    @IBOutlet weak var presenter : OptifastProductDetailsPresenter!

    override func viewDidLoad() {
        super.viewDidLoad()
        self.registerCells()
        self.basePresenter = self.presenter
        self.presenter.detailsView = self
        self.optifastDetailsTableView.estimatedRowHeight = 44.0
        self.optifastDetailsTableView.rowHeight = UITableViewAutomaticDimension
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.stopActivityAnimation()
    }
    
    func reloadData() {
         self.optifastDetailsTableView.reloadData()
    }
    func reloadSectionAt(index : Int){
        self.optifastDetailsTableView.reloadSections(NSIndexSet(index : index) as IndexSet, with: .automatic)
        
    }
    
    
    private func registerCells () {
        let nib = UINib.init(nibName: "OptifastDetailsSectionCell", bundle: nil)
        self.optifastDetailsTableView.register(nib, forHeaderFooterViewReuseIdentifier: optSecCellID)
        self.optifastDetailsTableView.register(UINib.init(nibName: "OptifastProductPictureCell", bundle: nil), forCellReuseIdentifier: optifastDetailsPictureCellID)
        self.optifastDetailsTableView.register(UINib.init(nibName: "NRFoodNutrientCell", bundle: nil), forCellReuseIdentifier: optifastDetailsNutrientsCellID)
    }
    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */

}

//
//  NutritionalBalanceInteractor.swift
//  Nourish
//
//  Created by Vlad Birukov on 07.06.17.
//  Copyright © 2017 SSIC. All rights reserved.
//

import Foundation
import RxSwift

class NutritionalBalanceInteractor {
    
    private let repository = NestleProgressRepository.shared
    
    func execute(startDate: Date, endDate: Date) -> Observable<NutritionalBalanceModel> {
        return self.repository.fetchNutritionalBalanceModelFor(startDate:startDate, endDate:endDate)
//        return repository.getProgressFor(startDate: startDate, endDate: endDate).flatMap({observer -> Observable<NutritionalBalanceModel> in
//            return Observable.just(self.transform(model: observer.userMicroelement) )
//        })
    }
    
    func transform(model: [MicroelementInformation]) -> NutritionalBalanceModel {
        let nutritionalBalance = NutritionalBalanceModel()
        var proxyArr: [(NutritionType, UserNutrition)] = [(.fruits, UserNutrition()), (.vegs, UserNutrition()), (.grains, UserNutrition()), (.protein, UserNutrition()), (.dairy, UserNutrition())]
        
        for i in 0..<proxyArr.count {
            let curentNutrition = model.filter({NutritionType.stringToType(string: $0.idNameMicroelement) == proxyArr[i].0})
            if curentNutrition.count > 0 {
                proxyArr[i].1.consumedNutritional = curentNutrition[0].consumedMicroelement
                if curentNutrition[0].upperLimit != nil {
                    if curentNutrition[0].consumedMicroelement > curentNutrition[0].lowerLimit! {
                        proxyArr[i].1.targetNutritional = curentNutrition[0].upperLimit!
                    } else {
                        proxyArr[i].1.targetNutritional = curentNutrition[0].lowerLimit!
                    }
                } else {
                    proxyArr[i].1.targetNutritional = curentNutrition[0].lowerLimit!
                }
                proxyArr[i].1.unit = curentNutrition[0].unit.replacingOccurrences(of: "_eq", with: "")
            }
        }
        
        nutritionalBalance.data = proxyArr
        
        return nutritionalBalance
    }

}

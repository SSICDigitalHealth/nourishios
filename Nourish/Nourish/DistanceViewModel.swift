//
//  DistanceViewModel.swift
//  Nourish
//
//  Created by Vlad Birukov on 29.08.17.
//  Copyright © 2017 SSIC. All rights reserved.
//

import Foundation

class DistanceViewModel {
    var distance: Double = 0.0
    var avgDistance: Double?
    var isMetric: Bool = false
}

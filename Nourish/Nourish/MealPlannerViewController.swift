//
//  MealPlannerViewController.swift
//  Nourish
//
//  Created by Vlad Birukov on 21.08.17.
//  Copyright © 2017 SSIC. All rights reserved.
//

import UIKit

class MealPlannerViewController: BasePresentationViewController {
    @IBOutlet weak var mealPlannerView: MealPlannerView!
    @IBOutlet weak var mealPlannerNavigationBarView: MealPlannerNavigationBarView!

    override func viewDidLoad() {
        super.viewDidLoad()
        self.baseViews = [mealPlannerView, mealPlannerNavigationBarView]
        for v in self.baseViews! {
            v.viewDidLoad()
        }
        self.mealPlannerView.presenter.mealPlannerViewController = self
        
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        NavigationUtility.showTabBar(animated: false)
        self.automaticallyAdjustsScrollViewInsets = false
        self.setUpNavigationBar()
    }
 
    func setUpNavigationBar() {
        if self.navigationController != nil {
            self.navigationController?.setNavigationBarHidden(true, animated: false)
            self.mealPlannerNavigationBarView.resetButton.addTarget(self, action: #selector(showResetView), for: .touchUpInside)
        }
    }
    
    func showResetView(sender:UIButton!) {
        self.mealPlannerView.presenter.showResetView()
    }
}

//
//  MealDiaryDataStore.swift
//  Nourish
//
//  Created by Nova on 6/22/17.
//  Copyright © 2017 SSIC. All rights reserved.
//

import UIKit
import RxSwift
import RealmSwift

let kEmptyObjectID = "emptyObjectID"

class MealDiaryDataStore: NSObject {
    
    func getFoodFor(date : Date) -> Observable<[MealRecordCache]> {
        return Observable.create { observer in
            let realm = try! Realm()
            
            let predicate = self.datePredicate(date: date)
            let hasChangesPredicate = NSPredicate(format : "hasChanges == false")
            let results = realm.objects(MealRecordCache.self).filter(predicate).filter(hasChangesPredicate)
            let array = Array(results)
            
            observer.onNext(array)
            return Disposables.create()
        }
    }
    
    func storeFoodFor(date : Date, array : [MealRecordCache]) {
        if array.count > 0 {
            let realm = try! Realm()
            
            let predicate = self.datePredicate(date: date)
            let predicateHasChanges = NSPredicate(format : "hasChanges == false")
            let results = realm.objects(MealRecordCache.self).filter(predicate).filter(predicateHasChanges)
            
            var result = [MealRecordCache]()
            try! realm.write {
                realm.delete(results, cascading: true)
                for obj in array {
                    obj.date = date
                    obj.hasChanges = false
                    result.append(obj)
                }
                realm.add(result)
            }
        } else {
            self.storeEmptyRecordFor(date: date)
        }
        
    }
    
    func fetchObjects(primaryKeys : [String]) -> [MealRecordCache] {
        let realm = try! Realm()
        var array = [MealRecordCache]()
        
        for object in primaryKeys {
            let specificMeal = realm.object(ofType: MealRecordCache.self, forPrimaryKey: object)
            if specificMeal != nil {
                array.append(specificMeal!)
            }
        }
        
        return array
    }
    
    func getUnSyncedRecordsFor(date : Date) -> Observable<[MealRecordCache]> {
        let realm = try! Realm()
        
        let predicate = self.datePredicate(date: date)
        let predicateHasChanges = NSPredicate(format : "hasChanges == true")
        let results = realm.objects(MealRecordCache.self).filter(predicate).filter(predicateHasChanges)
        
        let arrayValues = Array(results)
        return Observable.just(arrayValues)
    }
    
    
    func addFoodForDate(date : Date, meal : MealRecordCache, occasion : Ocasion) -> String {
        let realm = try! Realm()
        try! realm.write {
            meal.hasChanges = true
            meal.isCreated = true
            meal.date = date
            realm.add(meal)
        }
        
        return meal.localCacheID
    }
    
    func getFoodObjectFor(timeStamp : Date, name : String) -> MealRecordCache? {
        let realm = try! Realm()
        
        let predicate = NSPredicate(format : "date == %@",timeStamp as NSDate)
        let uidPredicate = NSPredicate(format : "name == %@", name)
        
        let result = realm.objects(MealRecordCache.self).filter(predicate).filter(uidPredicate).first
        
        return result
    }
    
    func getFoodObjectFor(foodID : String) -> MealRecordCache? {
        let realm = try! Realm()
        
        var predicate = NSPredicate(format : "userFoodId == %@",foodID)
        
        if foodID.hasPrefix("local") == true {
            predicate = NSPredicate(format : "localCacheID == %@", foodID)
        }
        
        let result = realm.objects(MealRecordCache.self).filter(predicate).first
        
        return result
    }
    
    func deleteObjectFor(foodID : String) {
        let realm = try! Realm()
        
        let predicate = NSPredicate(format : "userFoodId == %@",foodID)
        
        let result = realm.objects(MealRecordCache.self).filter(predicate).first
        
        if result != nil {
            try! realm.write {
                realm.delete(result!, cascading: true)
            }
        }
        
    }
    func storeEmptyRecordFor(date : Date) {
        let realm = try! Realm()
        
        let predicate = self.datePredicate(date: date)
        let results = realm.objects(MealRecordCache.self).filter(predicate)
        
        try! realm.write {
            realm.delete(results, cascading: true)
        }
        
        
        
        try! realm.write {
            let emptyObj = MealRecordCache()
            emptyObj.date = date
            
            emptyObj.foodUid = kEmptyObjectID
            realm.add(emptyObj)
        }
        
    }
    
    private func datePredicate(date : Date) -> NSPredicate {
        let calendar = Calendar.current
        let startOfDay = calendar.startOfDay(for: date)
        
        var comps = DateComponents()
        comps.day = 1
        comps.second = -1
        
        let endDate = calendar.date(byAdding: comps, to: startOfDay)
        let predicate = NSPredicate(format : "date > %@ && date < %@",startOfDay as NSDate, endDate! as NSDate)
        return predicate

    }
    
    func addMeal(meal : MealRecordCache, date : Date) {
        let realm = try! Realm()
        
        meal.date = date
        
        try! realm.write {
            realm.add(meal)
        }
    }
    
    func fetchFor(idArray : [String]) -> [MealRecordCache] {
        let realm = try! Realm()
        
        var resultArray = [MealRecordCache]()
        
        for object in idArray {
            let result = realm.objects(MealRecordCache.self).filter(NSPredicate(format : "localCacheID == %@",object))
            if result.count == 1 {
                resultArray.append(result.first!)
            }
        }
        
        return resultArray
    }

}

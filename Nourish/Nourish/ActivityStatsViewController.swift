//
//  ActivityStatsViewController.swift
//  Nourish
//
//  Created by Vino (Vinodha) Sundaramoorthy on 12/23/16.
//  Copyright © 2016 SSIC. All rights reserved.
//

import UIKit

class ActivityStatsViewController: BasePresentationViewController  {
    
    @IBOutlet weak var activityStatsView : ActivityStatsView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupNavigationBar()
        self.baseViews = [activityStatsView]
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        self.navigationController?.navigationBar.topItem?.title = "Activity"
    }
    
    func setupNavigationBar() {
        self.title = "Activity"
        self.navigationController?.navigationBar.backItem?.title = ""
        self.navigationController?.navigationBar.setBackgroundImage(NRColorUtility.symplifiedNavigationColor().as1ptImage(), for: .default)
        self.navigationController?.navigationBar.shadowImage = NRColorUtility.symplifiedNavigationColor().as1ptImage()
    }
    
}

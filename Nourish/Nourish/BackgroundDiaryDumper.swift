//
//  BackgroundDiaryDumper.swift
//  NourIQ
//
//  Created by Igor on 12/22/17.
//  Copyright © 2017 SSIC. All rights reserved.
//

import UIKit
import RxSwift

let kDiaryType = "diary"
let kDataType = "diary_user"

typealias datesSE = (startDate : Date, endDate : Date)
typealias datedIDs = (date : Date, records : [MealRecordCache])
typealias datedCards = (dateValues : datedIDs ,cards : [NewFoodCard])
typealias gramsWithID = (grams : Double, ident : String)
typealias nutrientsWithID = (ident : String, nutrs : [NestleNutrs])
typealias comboObject = (dateID : datedIDs, nutrs : [nutrientsWithID])

let epohDate = Date(timeIntervalSince1970: 0)
class BackgroundDiaryDumperRepository: NSObject {

    let calendar = Calendar.current
    let store = BackgroundActivityDumperDataStore()
    let mealCacheStore = MealDiaryDataStore()
    let mealRepo = MealHistoryRepository.shared
    let foodCardRepo = NewFoodCardRepository()
    let toJson = Combo_to_json()
    
    func startDump() {
        if let token = NRUserSession.sharedInstance.accessToken {
            if let userID = NRUserSession.sharedInstance.userID {
                var finalDate : Date?
                var startDate = Date()
                let obj = self.startDateForMealDiary(token: token).flatMap { date -> Observable<datedIDs> in
                    let dates = self.datesFromStart(date: date)
                    finalDate = dates.endDate
                    startDate = dates.startDate
                    
                    return self.datedIDsFrom(dates: dates, userID: userID, token: token)
                    }.flatMap { someObj -> Observable<comboObject> in
                        return self.mapDateIDsIntoCards(dateID: someObj)
                    }
                 let scheduler = ConcurrentDispatchQueueScheduler(qos: .background)
                
                let _ = obj.toArray().observeOn(scheduler).subscribe(onNext: {combos in
                    let filt = combos.filter {$0.dateID.date != epohDate}
                    if filt.count > 0 {
                        var json = [String : Any]()
                        json["diaryData"] = self.toJson.transform(array: filt)
                        let interval = finalDate?.timeIntervalSince1970 ?? startDate.timeIntervalSince1970
                        let rootDict = ["data" : json, "ts" : Int(interval)] as [String : Any]
                        
                        let _ = self.store.uploadDiaryData(dict: rootDict, token: token).subscribe(onNext: {flag in
                            if flag == true {
                                NRUserSession.sharedInstance.diaryDumpDate = finalDate
                            }
                    
                        }, onError: {error in }, onCompleted: {}, onDisposed: {})
                    } else {
                        if finalDate != nil {
                            NRUserSession.sharedInstance.diaryDumpDate = finalDate
                        }
                    }
                }, onError: {error in
                    
                }, onCompleted: {}, onDisposed: {})
            }
        }
    }
    
    private func startDateForMealDiary(token : String) -> Observable<Date> {
        return self.getRestStartDateForMealDiary(token: token).map { date -> Date in
            let localDate = self.getLocalStartDateForMealDiary()
            if date == nil {
                return localDate
            } else {
                return localDate >= date! ? localDate : date!
            }
        }
    }
    
    private func getLocalStartDateForMealDiary() -> Date {
        if let date = NRUserSession.sharedInstance.diaryDumpDate {
            return date
        } else {
            var cmp = DateComponents() //date of removing functionality of adding food from Noom DB
            cmp.year = 2017
            cmp.month = 9
            cmp.day = 6
            
            let date = calendar.date(from: cmp)
            let startDate = calendar.startOfDay(for: date!)
            
            return startDate
        }
        
    }
    
    private func getRestStartDateForMealDiary(token : String) -> Observable<Date?> {
        return self.store.getLastDateFor(dataType: kDataType, dataSource: kDiaryType, token: token)
    }
    
    private func datesFromStart(date : Date) -> datesSE {
        var endDate = calendar.date(byAdding: .day, value: 10, to: date)
        let yesterday = calendar.date(byAdding: .day, value: -1, to: Date())
        let yesterdayMidnight = calendar.startOfDay(for: yesterday ?? Date())
        
        endDate = endDate! < yesterdayMidnight ? endDate : yesterdayMidnight
        
        return datesSE(startDate: date, endDate : endDate!)
    }
    
    private func datedIDSFromRecord(array : [MealRecordCache], date : Date) -> datedIDs {
        let tuple = datedIDs(date : date, records : array)
        return tuple
    }
    
    private func datedIDsFrom(dates : datesSE, userID : String, token : String) -> Observable<datedIDs> {
        var startDate = dates.startDate
        
        var historyArray = [Observable<[MealRecordCache]>]()
        while startDate <= dates.endDate {
            let history = self.mealRepo.getRestMealDiaryFor(date: startDate, userID: userID, token: token)
            historyArray.append(history)
            startDate = calendar.date(byAdding: .day, value: 1, to: startDate)!
        }
        
        let obj = Observable.from(historyArray).concat().map { records -> datedIDs in
            let datedArray = records.filter {$0.date != nil}
            if datedArray.count > 0 {
                let date = datedArray.first!.date!
                return self.datedIDSFromRecord(array: records, date: date)
            } else {
                return datedIDs(epohDate, [])
            }
        }
        
        return obj
    }
    
    private func mapDateIDsIntoCards(dateID : datedIDs) -> Observable<comboObject> {
        if dateID.records.count > 0 {
            let grams = self.mapDateIDsToDict(dateID: dateID)
            let nutrs = self.countedNutrientsFrom(gramsWiID: grams).map { nutrients -> comboObject in
                let object = comboObject(dateID : dateID, nutrs : nutrients)
                return object
            }
            return nutrs
        } else {
            return Observable.just(comboObject(dateID: dateID, nutrs : []))
        }
    }
    
    private func mapDateIDsToDict(dateID : datedIDs) -> [gramsWithID] {
        var result = [gramsWithID]()
        
        let records = dateID.records.filter {$0.idString != nil && $0.idString != ""}
        
        let newDateID = datedIDs(date: dateID.date, records : records)
        
        for object in newDateID.records {
            let tuple = gramsWithID(grams: object.grams, ident : object.idString ?? "")
            result.append(tuple)
        }
        return result
    }
    
    private func countedNutrientsFrom(gramsWiID : [gramsWithID]) -> Observable<[nutrientsWithID]> {
        var foodCards = [Observable<NewFoodCard>]()
        
        for object in gramsWiID {
            foodCards.append(self.mapStringIDs(idents: object.ident))
        }
        
        return Observable.from(foodCards).merge().toArray().map { foodCards -> [nutrientsWithID] in
            var nutrsResulted = [nutrientsWithID]()
            
            for foodCard in foodCards {
                let identifier : String = foodCard.defaultFoodSearchModel!.foodId
                let filtered = gramsWiID.filter {$0.ident == identifier}
                let obj = filtered.first!
                let weight = obj.grams
                let nutr = nutrientsWithID(ident: identifier, nutrs : self.recountNutrs(nutrients: foodCard.nutrs, weight: weight))
                nutrsResulted.append(nutr)
            }
            
            return nutrsResulted
        }
    }
    
    private func mapStringIDs(idents : String) -> Observable<NewFoodCard> {
        return self.foodCardRepo.fetchFoodCardFor(id:idents)
    }
    
    private func recountNutrs(nutrients : [NestleNutrs], weight : Double) -> [NestleNutrs] {
        var nutrs = [NestleNutrs]()
        
        for object in nutrients {
            let nutrient = NestleNutrs()
            nutrient.name = object.name
            nutrient.unit = object.unit
            nutrient.densityPerGram = object.densityPerGram * weight
            
            nutrs.append(nutrient)
        }
        
        return nutrs
    }
    
    //food uploader
    
}

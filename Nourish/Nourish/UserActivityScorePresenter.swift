//
//  UserActivityScorePresenter.swift
//  Nourish
//
//  Created by Vlad Birukov on 06.06.17.
//  Copyright © 2017 SSIC. All rights reserved.
//

import Foundation
import RxSwift

class UserActivityScorePresenter: BasePresenter {
    
    let objectToModelMapper = UserActivityScoreModel_to_UserActivityScoreViewModel()
    let interactor = UserActivityScoreInteractor()
    var userActivityScoreView : UserActivityScoreProtocol?
    var userActivityScoreModel = UserActivityScoreViewModel()
    var date: (Date, Date)?
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.getActivityUser()
    }

    private func getActivityUser(){
        if self.userActivityScoreView?.config() != nil {
            self.date = self.userActivityScoreView?.config()?.getDate()
        }
        if let view = self.userActivityScoreView {
            if let config = view.config() {
                print(#file, #line, "::", config.date)
            }
        }
        
        if self.date != nil {
            let proxyDate = userActivityScoreView?.config()?.date

            self.subscribtions.append(self.interactor.execute(startDate: (self.date?.0)!, endDate: (self.date?.1)!).observeOn(MainScheduler.instance).subscribe(onNext: { [weak self] score in
                if let instance = self {
                    instance.userActivityScoreModel = instance.objectToModelMapper.transform(score: score)
                    
                    if proxyDate == instance.userActivityScoreView?.config()?.date {
                        instance.userActivityScoreView?.setupWithModel(model: instance.userActivityScoreModel)
                    }
                    
                    instance.userActivityScoreView?.stopActivityAnimation()
                }
               
            }, onError: {[weak self] error in
                if let instance = self {
                    instance.userActivityScoreView?.stopActivityAnimation()
                    instance.userActivityScoreView?.parseError(error: error,completion: nil)
                }
            }, onCompleted: {
            }, onDisposed: { [weak self] in
                if let instance = self {
                    instance.userActivityScoreView?.stopActivityAnimation()
                }
            }))
        }
    }
}

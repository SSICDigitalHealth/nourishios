//
//  DailyStressCell.swift
//  Nourish
//
//  Created by Gena Mironchyk on 1/24/17.
//  Copyright © 2017 SSIC. All rights reserved.
//

import UIKit
class DailyStressCell : UITableViewCell {
    @IBOutlet weak var bpmValueLabel : UILabel?
    @IBOutlet weak var timeLabel : UILabel?
    @IBOutlet weak var bpmLabel : UILabel?
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }

    func setupWithModel(model : DailyStressModel){
        self.bpmValueLabel?.text = String(format : "%0.f",model.bpmValue!)
        self.timeLabel?.text = self.format(date: model.dateToShow!)
        self.bpmValueLabel?.textColor = stressLevel.textColorForStress(stress: model.stressValue!)
        self.backgroundColor = stressLevel.backgroundTableViewCellColorForStress(stress: model.stressValue!)
    }
    
    private func format(date:Date) ->String {
        let df = DateFormatter()
        df.dateFormat = "hh:mm a"
        return df.string(from: date)
    }
    
    
}

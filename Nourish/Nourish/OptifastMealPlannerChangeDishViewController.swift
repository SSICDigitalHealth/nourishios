//
//  OptifastMealPlannerChangeDishViewController.swift
//  Optifast
//
//  Created by Gena Mironchyk on 1/11/18.
//  Copyright © 2018 SSIC. All rights reserved.
//

import UIKit

class OptifastMealPlannerChangeDishViewController: BasePresentationViewController {
    var model : OptifastMealPlannerViewModel!
    var ocasion : Ocasion!
    @IBOutlet weak var changeDishView : OptifastMealPlannerChangeDishView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.baseViews = [self.changeDishView]
        self.changeDishView.viewDidLoad()
        self.changeDishView.presenter.model = self.model
        self.changeDishView.presenter.ocasion = self.ocasion

        // Do any additional setup after loading the view.
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.changeDishView.presenter.controller = self
        self.automaticallyAdjustsScrollViewInsets = false
        self.title = "Change dish"
        self.navigationController?.setNavigationBarHidden(false, animated: false)
        self.setupNavBar()


    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    override func willMove(toParentViewController parent: UIViewController?) {
        super.willMove(toParentViewController: parent)
        if parent == nil {
            self.changeDishView.presenter.processSave()
        }
    }
    
    private func setupNavBar() {
        let titleAttribute = self.navigationBarTitleAttribute()
        
        self.navigationController?.navigationBar.barTintColor = UIColor.white
        self.navigationController?.navigationBar.tintColor = UIColor.gray
        self.navigationController?.navigationBar.titleTextAttributes = titleAttribute
    }
   
}

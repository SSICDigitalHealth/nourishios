//
//  CameraSnapViewController.swift
//  Nourish
//
//  Created by Nova on 4/18/17.
//  Copyright © 2017 SSIC. All rights reserved.
//

import UIKit
import AVFoundation
import Clarifai
import RxSwift
protocol CameraSnapViewControllerProtocol {
    func present(vc : UIViewController, needDismission : Bool)
}

class CameraSnapViewController: BasePresentationViewController, CameraSnapViewControllerProtocol{
    
    @IBOutlet weak var cameraSnapView : CameraSnapView!
    var ocasion : Ocasion?
    
    var date : Date?
    var recognitionDelegate : RecognitionViewContollerDelegate?
    override func viewDidLoad() {
        
        super.viewDidLoad()
        self.baseViews = [cameraSnapView]
        self.cameraSnapView.viewDidLoad()
        self.cameraSnapView.presenter.cameraController = self
        self.cameraSnapView.presenter.recognitionDelegate = self.recognitionDelegate
        self.cameraSnapView.presenter.date = self.date
        self.cameraSnapView.presenter.ocasion = self.ocasion
        // Do any additional setup after loading the view.
    }
    
    func present(vc: UIViewController, needDismission: Bool) {
        if needDismission == true {
            self.dismiss(animated: false, completion: {
                self.present(vc, animated: true, completion: nil)
            })
        } else {
            self.present(vc, animated: true, completion: nil)
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }
    
    override var supportedInterfaceOrientations: UIInterfaceOrientationMask {
        return UIInterfaceOrientationMask.all
    }
    
    override var shouldAutorotate: Bool {
        return true
    }
    
    @IBAction func dissmisCamera() {
        self.dismiss(animated: true, completion: nil)
    }
    
    
        /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

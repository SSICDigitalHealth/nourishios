//
//  RecomendationPresenter.swift
//  Nourish
//
//  Created by Vlad Birukov on 30.08.17.
//  Copyright © 2017 SSIC. All rights reserved.
//

import Foundation
import RxSwift

class RecomendationPresenter: BasePresenter {
    let objectToModelMapper = RecomendationModel_to_RecommendationViewModel()
    let interactor = RecomendationInteractor()
    var recomendationView: RecomendationProtocol!
    var date: (Date, Date)?
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.getRecomendation()
    }
    
    private func getRecomendation() {
        if self.recomendationView.config() != nil {
            self.date = self.recomendationView.config()?.getDate()
        }
        
        if self.date != nil {
            let proxyDate = recomendationView?.config()?.date

            self.subscribtions.append(self.interactor.execute(startDate: (self.date?.0)!, endDate: (self.date?.1)!).observeOn(MainScheduler.instance).subscribe(onNext: { [unowned self] recomendationDetail in
                if proxyDate == self.recomendationView?.config()?.date {
                    self.recomendationView.setupWithModel(model: self.objectToModelMapper.transform(model: recomendationDetail))
                }
                self.recomendationView.stopActivityAnimation()
            }, onError: {error in
                self.recomendationView.stopActivityAnimation()
                self.recomendationView.parseError(error : error, completion: nil)
            }, onCompleted: {
            }, onDisposed: {
                self.recomendationView.stopActivityAnimation()
            }))
        }
    }
}

//
//  NRManageGoalsPresenter.swift
//  Nourish
//
//  Created by Vino (Vinodha) Sundaramoorthy on 11/23/16.
//  Copyright © 2016 SSIC. All rights reserved.
//

import UIKit
import RxSwift

let kDefaultNutritionValue : Float = 25
let kMaxNutritionValue : CGFloat = 70
let kMinNutritionValue : CGFloat = 40
let kMaxFitnessValue : CGFloat = 40
let kMinFitnessValue : CGFloat = 10

class NRManageGoalsPresenter: BasePresenter ,UITextFieldDelegate {
    
    var goalsView : NRManageGoalsViewProtocol!
    var userGoal : UserGoal = UserGoal.EatBetter
    var goalConfiguration :  [goalCellConfig] = goals.goalConfiguration(goal: UserGoal.EatBetter)
    var userWeight : [(lable:String,value:Double)] = [(lable:"Start",value:0),(lable:"Goal",value:0)]
    
    let interactor = UserProfileInteractor()
    let mapperObject = UserProfile_to_UserProfileModel()
    let mapperModel = UserProfileModel_to_UserProfile()
    var userModel : UserProfileModel = UserProfileModel()
    
    // MARK: Base Presenter Protocol
    override func viewWillAppear(_ animated : Bool) {
        self.subscribtions.append(self.currentUser().observeOn(MainScheduler.instance).subscribe(onNext: { [unowned self] user in
            self.userModel = user
        }, onError: {error in
            self.goalsView.parseError(error: error, completion: nil)
            LogUtility.logToFile("error is ",error)
        }, onCompleted: {
            if self.goalsView != nil {
                self.goalsView.setUserProfile(model: self.userModel)
            }
        }, onDisposed: {}))
    }
    
    func currentUser() -> Observable<UserProfileModel> {
        let object = self.interactor.getRawUser().flatMap({ userProfile -> Observable<UserProfileModel> in
            self.userModel = self.mapperObject.transform(user: userProfile)
            return Observable.just(self.mapperObject.transform(user: userProfile))
        })
        return object
    }
    
    func saveCurrentUser() {
        let some = interactor.storeUser(user: mapperModel.transform(userModel: userModel), needToRefreshToken: true)
        subscribtions.append(some.observeOn(MainScheduler.instance).subscribe(onNext: {artikError in
            if artikError != nil {
                LogUtility.logToFile("error is \(String(describing: artikError?.code)) \(String(describing: artikError?.errorDescription))")
            }
        }, onError: {error in
            self.goalsView.parseError(error: error, completion: nil)
            LogUtility.logToFile(error)
        }, onCompleted: {
            DeleteRecommendationsCacheInteractor().execute()
            LogUtility.logToFile("save to Artik completed")
            self.goalsView.setUserProfile(model: self.userModel)
            self.goalsView.showToast(message: "Goal Updated")
        }, onDisposed: {}))
    }

}

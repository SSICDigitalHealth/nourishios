//
//  MicroelementBalanceView.swift
//  Nourish
//
//  Created by Vlad Birukov on 08.06.17.
//  Copyright © 2017 SSIC. All rights reserved.
//

import UIKit

protocol MicroelementBalanceProtocol: ProgressViewProtocol {
    func setupWith(Model model:AnyObject)
    func config() -> ProgressConfig?
}

class MicroelementBalanceView: ProgressView, MicroelementBalanceProtocol{

    var presenter: MicroelementBalancePresenter!
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.presenter.userMicroelementBalanceView = nil
        self.presenter = nil
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func renderView(_ config: ProgressConfig) {
        super.renderView(config)
        self.progressContent = self.loadContentNibWith(Name: config.period.watchOut.nibName)
        self.loadContent(contentView: self.progressContent!)
        self.updateCardHeightHook?(config.period.watchOut.height, .watchOut)
        self.loadWhiteThemeWith(header: config.period.watchOut.header)
    }
    
    override func reloadData() {
        if let model = self.model {
            self.progressContent.model = model as? MicroelementViewModel
            if let config = self.progressConfig {
                self.progressContent?.reloadDataWith(config: config)
            }
        }
    }
    
    override func setupWith(Model model: AnyObject) {
        super.setupWith(Model: model)
        self.reloadData()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.renderView(self.progressConfig!)
        self.presenter = MicroelementBalancePresenter()
        self.basePresenter = self.presenter
        self.presenter.userMicroelementBalanceView = self
        super.viewWillAppear(animated)
        self.removeActivityIndicator()
    }
    
    override func gestureTapAction() {
        if let hook = self.tapHook {
            hook(.watchOut)
        }
    }
}

//
//  NRFoodDetailsProtocol.swift
//  Nourish
//
//  Created by Vino (Vinodha) Sundaramoorthy on 11/21/16.
//  Copyright © 2016 SSIC. All rights reserved.
//

import UIKit

protocol NRFoodDetailsProtocol : BaseViewProtocol {
    func setupFoodDetailsView ()
    func setUpFoodDetailsModel(detailsModel : foodSearchModel)
    func refreshMeasureWithServingType(index : Int)
    func refreshServingsWithIndex(index : Int)
    func reloadNutrientDetails(servingType:String ,measure:Double)
    func addToDiaryForOcassion(ocasion : Ocasion)
    func finishSaveToDiary()
    func disableNavigationBar()
    func hideFoodImageView()
    func setFoodImage(image : UIImage)
    
}

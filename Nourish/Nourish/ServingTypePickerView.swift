//
//  ServingTypePickerView.swift
//  Nourish
//
//  Created by Vino (Vinodha) Sundaramoorthy on 11/22/16.
//  Copyright © 2016 SSIC. All rights reserved.
//

import UIKit

class ServingTypePickerView: UIView , NRPickerViewDelegate , NRPickerViewDataSource {

    var picker : NRPickerView?
    var viewController : MealDiaryViewController?
    var servingTypesArray : [String]?
    var foodSearchModel : foodSearchModel?
    /*
    var servingTypeArray : [String]? {
        get {
            return self.prepareServingTypeArray()
        }
    }
    */
    
    func setupPicker(servingTypes : [String]) {
        picker = NRChatUtility.horizontalPickerView()
        self.addSubview(picker!)
        servingTypesArray = servingTypes
        picker?.delegate = self
        picker?.dataSource = self
        picker?.maskDisabled = false
        picker?.pickerViewStyle = .flat
        picker?.font = UIFont(name: "HelveticaNeue-Light", size: 12)!
        picker?.highlightedFont = UIFont(name: "HelveticaNeue-Light", size: 12)!
        picker?.textColor = UIColor.black
        picker?.highlightedTextColor = UIColor.black
        picker?.reloadData()
    }
    
    func prepareServingTypeArray() -> [String] {
        var array = [String]()
        for type in ServingType.allValues {
            array.append(ServingType.description(type: ServingType(rawValue: type.rawValue)!))
        }
        return array
    }
    
    // MARK: Picker Delegates and data source
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
    }
    
    func numberOfItemsInPickerView(_ pickerView: NRPickerView) -> Int {
        return (servingTypesArray?.count)!
    }
    
    func pickerView(_ pickerView: NRPickerView, titleForItem item: Int) -> String {
        return servingTypesArray![item]
    }
    
    func pickerView(_ pickerView: NRPickerView, didSelectItem item: Int) {
        //let date = self.servingTypeArray?[item]
        foodSearchModel?.unit = self.servingTypesArray![item]
    }
    
}

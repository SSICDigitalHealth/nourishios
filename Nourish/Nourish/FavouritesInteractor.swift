//
//  FavouritesInteractor.swift
//  Nourish
//
//  Created by Nova on 3/20/17.
//  Copyright © 2017 SSIC. All rights reserved.
//

import UIKit
import RxSwift


class FavouritesInteractor: NSObject {
    
    var toSearchModelMapper = MealRecord_to_foodSearchModel()
    var repo = FavoritesRepository()
    
    func addToFavourites(models : [MealRecordModel], name : String) -> Observable<Bool> {
        let models = toSearchModelMapper.transform(records: models)
        return self.repo.addtoFavourites(models: models, name : name)
    }

    func addToFav(model : foodSearchModel) -> Observable<Bool> {
        if model.isFromNoom == true && model.foodId.rangeOfCharacter(from: .whitespaces) == nil && model.foodId.count > 0 {
           // if model.foodBackendID != nil && model.foodBackendID != "" {
                return self.repo.addtoFavourites(models: [model], name: UUID().uuidString)
           /* } else {
                let int = NRFoodDetailsInteractor()
                let obj = int.fetchFoodRecordMeal(foodSearchModel: model, occasion: nil, date: Date()).flatMap {record -> Observable<MealRecordCache> in
                    let mapper = MealRecord_to_MealRecordCache()
                    let cache = mapper.transform(record: record)
                    return Observable.just(cache)
                }.flatMap { cache -> Observable<Bool> in
                    self.repo.refreshCacheFavorite()
                    return self.repo.addtoFavorites(cache: cache, name: UUID().uuidString).map {idString -> Bool in
                        return idString != ""
                    }
                }
                return obj
            }*/
            
        } else {
            return self.addRecToFavourites(searchModel: model, name: UUID().uuidString)
        }
    }
    
    func checkName(name : String) -> String {
        return self.repo.checkGroupName(name: name)
    }
    
    func addRecToFavourites(searchModel : foodSearchModel, name : String) -> Observable<Bool> {
        return self.repo.addRecToFavorite(model: searchModel, name: name)
    }
    
    func fetchFavorites() -> Observable<[MealRecordModel]> {
        return self.repo.fetchFavoritesWithCache()
    }
    
    func deleteFavouriteGroup(groupID : String) -> Observable<Bool> {
        return self.repo.deleteFavouriteGroup(groupID : groupID)
    }
    
    func addToDiary(foodGroupd : String, groupName:String , occasion : Ocasion, date : Date, models : [foodSearchModel]) -> Observable<Bool> {
        self.storeChatMessage(name: groupName, date: date)
        return self.repo.addToDiary(foodGroupd: foodGroupd, groupName: groupName ,occasion: occasion, date: date, models: models)
    }
    
    func addGroupToFav(foodGroupID : String, name : String, userPhotoID : String?) -> Observable<Bool> {
        return self.repo.addGroupToFav(groupdID: foodGroupID, name: name, userPhotoID: userPhotoID)
    }
    
    func addToFavourites(models : [foodSearchModel], name : String) -> Observable<Bool> {
        return self.repo.addGroupToFavorites(models: models, name : name)
    }
    
    func refreshCacheFavorite() {
        self.repo.refreshCacheFavorite()
    }
    
    func storeChatMessage(name : String, date : Date) {
        let eatenChatMessage = ChatDataBaseItem()
        eatenChatMessage.chatBoxType = .typeDish
        eatenChatMessage.date = date
        eatenChatMessage.content = name
        eatenChatMessage.isFromUser = true
        eatenChatMessage.messageID = ""
        eatenChatMessage.userID = NRUserSession.sharedInstance.userID ?? ""
        
        let chatRepo = ChatBoxRepository()
        chatRepo.storeRecordsArray(array: [eatenChatMessage])
    }
}

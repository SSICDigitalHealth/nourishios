  //
//  NRUserProfileInteractor.swift
//  Nourish
//
//  Created by Nova on 11/15/16.
//  Copyright © 2016 SSIC. All rights reserved.
//

import UIKit
import RxSwift
class UserProfileInteractor: NSObject {
    let repository = UserRepository.shared
    let realmRepo = RealmUserRepository()
    
    
    func getCurrentUser() -> Observable<UserProfile> {
        return self.userProfileFullyFilled()
    }
    
    func getCurrentUserWith(fetchingPolicy: FetchingPolicy) -> Observable<UserProfile> {
        return self.repository.getCurrentUser(policy: fetchingPolicy)
    }
    
    func getRawUser() -> Observable<UserProfile> {
        return self.repository.getCurrentUser(policy: .DefaultPolicy).flatMap({userProfile -> Observable<UserProfile> in
        
            if userProfile.startWeight == nil {
                userProfile.startWeight = userProfile.weight
            }
            
            if userProfile.goalValue == nil {
                userProfile.goalValue = userProfile.weight
            }
            
            if userProfile.userGoal == .MaintainWeight {
                if userProfile.weightToMaintain != nil && userProfile.weight! - userProfile.weightToMaintain! > 2.2 {
                    userProfile.userGoal = .LooseWeight
                    userProfile.goalValue = userProfile.weightToMaintain
                    userProfile.startWeight = userProfile.weight
                    userProfile.needToInformUser = true
                    let _ = self.storeUser(user: userProfile, needToRefreshToken: true).subscribe(onNext: {error in }, onError: {error in }, onCompleted: {
                        print("Completed")
                    }, onDisposed: {})
                }
            }
            if userProfile.weightToMaintain != nil && userProfile.userGoal == .LooseWeight && userProfile.weightToMaintain != 0.0 {
                if userProfile.weight! - userProfile.weightToMaintain! <= 0.0 {
                    userProfile.userGoal = .MaintainWeight
                    userProfile.needToInformUser = true
                    let _ = self.storeUser(user: userProfile, needToRefreshToken: true).subscribe(onNext: {error in }, onError: {error in }, onCompleted: {
                        print("Completed")
                    }, onDisposed: {})
                    
                }
            }
            return Observable.just(userProfile)
        })
    }
    
    func userProfileFullyFilled() -> Observable<UserProfile> {
        return self.repository.getCurrentUser(policy: .DefaultPolicy).flatMap{userProfile -> Observable<UserProfile> in
            return self.requiredPropertiesFor(userProfile: userProfile).flatMap{attributes -> Observable<UserProfile> in
                for property in attributes {
                    switch property {
                    case .height:
                        userProfile.height = 1.65
                    case .weight:
                        userProfile.weight = 65
                    case .age:
                        userProfile.age = 25
                    case .activityLevel:
                        userProfile.activityLevel = .Sedentary
                    case .dietaryPreference:
                        userProfile.dietaryPreference = .none
                    case .biologicalSex:
                        userProfile.biologicalSex = .Male
                    case .goalRounder:
                        userProfile.goalPrefsActivity = 25.0
                        userProfile.goalPrefsLifestyle = 20.0
                        userProfile.goalPrefsFood = 55.0
                    case .userGoal:
                        userProfile.userGoal = .EatBetter
                        userProfile.startWeight = userProfile.weight
                        userProfile.goalValue = userProfile.weight
                    default:
                        break
                    }
                }
                if userProfile.startWeight == nil {
                    userProfile.startWeight = userProfile.weight
                }
                
                if userProfile.goalValue == nil {
                    userProfile.goalValue = userProfile.weight
                }
                
                if userProfile.userGoal == .MaintainWeight {
                    if userProfile.weightToMaintain != nil && userProfile.weight! - userProfile.weightToMaintain! > 1.0 {
                        userProfile.userGoal = .LooseWeight
                        userProfile.goalValue = userProfile.weightToMaintain
                        userProfile.needToInformUser = true
                        let _ = self.storeUser(user: userProfile, needToRefreshToken: true).subscribe(onNext: {error in }, onError: {error in }, onCompleted: {
                            print("Completed")
                        }, onDisposed: {})
                    }
                }
                if userProfile.userGoal == .LooseWeight && userProfile.weightToMaintain != 0.0 {
                    if userProfile.weightToMaintain != nil && userProfile.weight! - userProfile.weightToMaintain! <= 0.0 {
                        userProfile.userGoal = .MaintainWeight
                        userProfile.needToInformUser = true
                        let _ = self.storeUser(user: userProfile, needToRefreshToken: true).subscribe(onNext: {error in }, onError: {error in }, onCompleted: {
                            print("Completed")
                        }, onDisposed: {})
                    }
                }
                return Observable.just(userProfile)
            }
        }
        
        
//        let zip = Observable.zip(user, missingAttr, resultSelector : { userProfile, attributes -> UserProfile in
//
//
//            return userProfile
//        })
//
//        return zip
    }
    
    private func generateRandomUser() -> UserProfile{
        let user = UserProfile()
        user.userID = NSUUID().uuidString
        user.userFirstName = "TestFirstName"
        user.userLastName = "TestLastName"
        user.activeSinceTimeStamp = Date()
        user.height = 1.7
        user.weight = 70
        user.age = 25
        user.activityLevel = .Sedentary
        user.dietaryPreference = .Common
        user.metricPreference = .Metric
        user.biologicalSex = .Male
        return user
    }
    
    func storeUser(user : UserProfile, needToRefreshToken : Bool) -> Observable<ArtikError?> {
        return repository.storeCurrentUser(user: user, writeToHealth: true, needToRefreshToken: needToRefreshToken)
    }
    
    func storeUserFeedBack(message : String, log: String) -> Observable<Bool> {
        let object = self.getCurrentUserWith(fetchingPolicy: .DefaultPolicy).flatMap {userProfile -> Observable<String> in
            Observable.just(userProfile.userID!)
            }.flatMap { userID -> Observable<Bool> in
            let token = NRUserSession.sharedInstance.accessToken
            let interval = Date().timeIntervalSince1970
            return self.repository.storeUserFeedBack(message: message, userID: userID, token: token!, date: interval, log: log)
        }
        return object
    }
    
    func requiredPropertiesFor(userProfile : UserProfile) -> Observable<[userPropertyField]> {
        return Observable.create({observer in
            var array : [userPropertyField] = []
            
            if userProfile.height == nil || userProfile.height == 0.0 {
                array.append(.height)
            }
            
            if userProfile.weight == nil || userProfile.weight == 0.0 {
                array.append(.weight)
            }
            
            if userProfile.age == nil || userProfile.age == 0 {
                array.append(.age)
            }
            
            if userProfile.activityLevel == nil {
                array.append(.activityLevel)
            }
            
            if userProfile.dietaryPreference == nil {
                array.append(.dietaryPreference)
            }
            
            if userProfile.metricPreference == nil {
                array.append(.metricPreference)
            }
            
            if userProfile.biologicalSex == nil {
                array.append(.biologicalSex)
            }
            
            /*if userProfile.goalPrefsActivity == nil || userProfile.goalPrefsLifestyle == nil || userProfile.goalPrefsFood == nil {
             array.append(.goalRounder)
             } else {
             let sum : Double = Double(userProfile.goalPrefsFood!) + Double(userProfile.goalPrefsLifestyle!) + Double(userProfile.goalPrefsActivity!)
             if sum < 99 || sum > 101 {
             array.append(.goalRounder)
             }
             }*/
            
            
            if userProfile.userGoal == nil {
                array.append(.userGoal)
            }
            observer.onNext(array)
            observer.onCompleted()
            
            return Disposables.create()
        })
        
        
//        return self.getRawUser().flatMap { userProfile -> Observable<[userPropertyField]> in
//
//
//            return Observable.just(array)
//        }
    }
}

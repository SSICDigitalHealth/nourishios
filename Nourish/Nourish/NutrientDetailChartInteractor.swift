//
//  NutrientDetailChartInteractor.swift
//  Nourish
//
//  Created by Vlad Birukov on 11.08.17.
//  Copyright © 2017 SSIC. All rights reserved.
//

import Foundation
import RxSwift

class NutrientDetailChartInteractor {
    func execute(startDate: Date, endDate: Date) -> Observable <NutrientModel> {
        return Observable<NutrientModel>.create{(observer) -> Disposable in
            let pause = Int(arc4random_uniform(4))
            let dispat = DispatchTime.now() + .seconds(pause)
            DispatchQueue.main.asyncAfter(deadline: dispat, execute: {
                let nutrienModel = NutrientModel()
                nutrienModel.nameNutritional = "Calcium"
                nutrienModel.consumedNutritional = 850
                nutrienModel.targetNutritional = 1000
                nutrienModel.unit = "mg"
                nutrienModel.message = "20% more than last month"
                nutrienModel.flagImage = true
                
                            
                observer.onNext(nutrienModel)
                observer.onCompleted()
            })
                        
            return Disposables.create()
        }

    }
}

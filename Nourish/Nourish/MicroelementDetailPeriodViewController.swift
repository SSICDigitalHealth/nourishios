//
//  MicroelementDetailPeriodViewController.swift
//  Nourish
//
//  Created by Andrey Zmushko on 8/16/17.
//  Copyright © 2017 SSIC. All rights reserved.
//

import UIKit

class MicroelementDetailPeriodViewController: BaseCardViewController {
    @IBOutlet weak var contentView: BaseView!
    @IBOutlet var presenter: MicroelementDetailForPeriodPresenter!
    @IBOutlet weak var cardNavigationBar: NewCardNavigationBarView!

    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.baseViews = [cardNavigationBar, contentView]
        for v in self.baseViews! {
            v.viewDidLoad()
        }
    }

    override init(_ config: ProgressConfig) {
        super.init(nibName: "MicroelementDetailPeriodViewController", bundle: nil)
        self.progressConfig = config
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewWillAppear(_ animated: Bool) {
        setUpNavigationBar(title: "Watch out for", style: .other, navigationBar: cardNavigationBar)
        self.contentView.basePresenter = self.presenter
        self.presenter.config = self.progressConfig
        super.viewWillAppear(animated)
    }

}

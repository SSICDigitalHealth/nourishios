//
//  NRStressModel_to_NRStress.swift
//  Nourish
//
//  Created by Vino (Vinodha) Sundaramoorthy on 12/5/16.
//  Copyright © 2016 SSIC. All rights reserved.
//

import UIKit

class NRStressModel_to_NRStress: NSObject {
    func transform(model : NRStressModel) -> NRStress {
        let object = NRStress()
        object.stressScore = model.stressScore
        object.hourlyStressArray = model.hourlyStressArray
        return object
    }
    
    func transform(modelArray : [NRStressModel]) -> [NRStress] {
        var arrayToReturn : [NRStress] = []
        for model in modelArray {
            arrayToReturn.append(self.transform(model: model))
        }
        return arrayToReturn
    }
}


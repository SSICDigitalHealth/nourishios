//
//  FitBitActivityRepository.swift
//  Nourish
//
//  Created by Vino (Vinodha) Sundaramoorthy on 5/17/17.
//  Copyright © 2017 SSIC. All rights reserved.
//

import UIKit
import RxSwift
import SwiftKeychainWrapper

class FitBitActivityRepository: NSObject {
    
    func synchronizeFitBitActivities () {
        let fitBitInteractor = FitBitInteractor()
        let _ = fitBitInteractor.syncFitBitMessages().subscribe(onNext: { [unowned self] synced in
            DispatchQueue.main.async {
                if synced == true {
                    if  defaults.value(forKey: kFitBitNextToken) == nil {
                        LogUtility.logToFile("FitBit - Sync Complete")
                        defaults.set(Date(), forKey: kLastSyncFitBitDateKey)
                        KeychainWrapper.standard.set(DateFormatter().string(from: Date())
                            , forKey: "kLastSyncFitBit")
                        BackgroundFitBitCacher.shared.isCaching = false
                    } else {
                        self.synchronizeFitBitActivities()
                    }
                }
            }
        }, onError: {error in}, onCompleted: {}, onDisposed: {})
    }
}

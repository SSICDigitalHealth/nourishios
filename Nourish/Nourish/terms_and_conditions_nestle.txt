END USER LICENSE AGREEMENT

Samsung Semiconductor, Inc. (“Samsung”) and Nestec Ltd. (“Nestle”) (collectively “we,” “us,” “our”) provide the Project Nourish App, which is a fitness and wellness mobile application, connected applications and associated accessories that is currently in the “beta” testing stage (“Project Nourish App”).  The Project Nourish App is intended to help mobile application end users (“Users”, “you” or “your”) manage their overall fitness and wellness by offering an application that captures or tracks Users’ nutritional intake, fitness and/or wellness information and metrics and provides access to articles and similar materials that may interest users (the “Purpose”).  The Project Nourish App may also encourage Users to eat and exercise effectively, and offer tools to assist Users, such as personalized activity and nutritional suggestions to meet Users’ individual goals as further described herein.

This End User License Agreement (this “Agreement”) sets forth the legally binding terms for your use of and access to The Project Nourish App. By clicking the “I AGREE” button or installing or using The Project Nourish App, you agree to be bound by this Agreement as of such date (the “Effective Date”). PLEASE READ THIS AGREEMENT BEFORE USING NOURISH. IF YOU DO NOT AGREE WITH THE TERMS, YOU SHOULD NOT USE THE PROJECT NOURISH APP, OR SHOULD DISCONTINUE USE THEREOF AND UNINSTALL THE PROJECT NOURISH APP IMMEDIATELY.

License Grant

Subject to the terms of this Agreement, we hereby grant you a limited, personal, revocable, non-exclusive and nontransferable license, without the right to sublicense, to (a) access and use the Project Nourish App solely for the Purpose as further described in the “Use of the Project Nourish App” section below, and (b) use the articles, text, software, music, sound, photographs, graphics, video, page layouts, design and other material available on or through the Project Nourish App (“Nourish Content”).

License Restrictions

You may not: (i) make copies, translations, or modifications of or to the Project Nourish App or the Nourish Content, (ii) alter, obscure, or remove the copyright notice on the Project Nourish App or the Nourish Content, (iii) assign, sell, distribute, lease, rent, sublicense, or transfer the Project Nourish App, the Nourish Content or this license or disclose the Project Nourish App or the Nourish Content to any other person, (iv) reverse-engineer, disassemble, or decompile the Project Nourish App Nourish or otherwise attempt to discover the source code or structural framework of the Project Nourish App or (v) use the Project Nourish App or the Nourish Content for any commercial purpose or any other purpose other than the Purpose.  The Project Nourish App and the Nourish Content is licensed, not sold, to you. We reserve all rights in and to the Project Nourish App and the Nourish Content not expressly granted to you under this Agreement

Use of the Project Nourish App

The Project Nourish App is intended for wellness and fitness purposes only and is not intended for use in the diagnosis of disease or other conditions, or in the cure, mitigation, treatment, or prevention of disease.  The Project Nourish App and any information and/or services the Project Nourish App provides are not intended for use in the detection, diagnosis, monitoring, management, or treatment of any medical condition, disease or vital physiological processes or for the transmission of time-sensitive health information.  This includes, for example only and without limitation, allergies, diabetes, or other medical conditions that are specific to individual Users.  Users who have such medical conditions should not use the Project Nourish App without first checking with a physician.  Any health-related information found herein and in any materials or information acquired and/or accessed through the Project Nourish App is available only for your convenience and should not be treated as medical advice.  Users should seek any medical advice from a physician, in particular before embarking on any new lifestyle or regimen and users should not self-diagnose any ailment.  Unless otherwise expressly stated or legally required, the Project Nourish App is provided with no warranties.  Any information that you obtain from use of the Project Nourish App may not be suitable, accurate, complete or reliable. The Project Nourish App and any applications provided by The Project Nourish App cannot not be used in connection with life support systems, critical care applications, or any other applications where application failure could lead to injury to persons or loss of life.

All Nourish Content and any other information provided by or through the Project Nourish App is for general recreational purpose only, and we do not accept any responsibility for its accuracy, completeness or relevancy.  You acknowledge and agree that the available functions, features and addable applications for the Project Nourish App may vary from country to country, including due to different local laws and regulations.  You should check the features and applications available in your specific region before using Nourish.  You can also use the Project Nourish App and manage your data by logging in to your Samsung account.

We are not liable for any injuries, damages, losses and/or costs suffered by Users, which are associated with the Project Nourish App, the Nourish Content and any related services and/or information, including recommendations, coaching, tips and/or guidelines, or for the accuracy, completeness or relevancy of any information provided or acquired by or accessed through the Project Nourish App.  In particular the Project Nourish App does not take into account any medical conditions that Users may have in any way, including, but not limited to, food allergies.  Users with medical conditions (for example, and without limitation, food allergies) should not use the Nourish App.  Moreover, while the Project Nourish App allows Users to choose a dietery preference of Vegan or Vegetarian, the Project Nourish App does not guarantee that the recommendations or other Nourish Conduct will reflect those chosen preferences.  We are not responsible for the accuracy of information contained in any articles or materials appearing on external third party websites or reference materials, or for the security or safety of using them.   Reference to information contained on external third party websites or reference materials should be taken neither as an endorsement of the accuracy of that information nor as an endorsement of the contents of that site or reference generally.  The terms of the End User License Agreement accompanying your device, including but not limited to its arbitration provisions, apply to your use of the Project Nourish App.

You acknowledge and agree that Nourish and any related services can be changed or discontinued without prior notice.

The Project Nourish App Is Still Under Development.

We want to be clear:  the Project Nourish App is in the “beta” stage.  Because this product is still under development, you should understand that the Nourish Content and/or any information provided by the Project Nourish App may not be accurate, reliable, or complete.  For example, and without limitation, the personalized nutritional and/or activity recommendations provided by the Project Nourish App are the result of, among other things, an algorithm that is still under development.  Thus, while we are allowing Users to see such recommendations and other Nourish Content, we advise Users to use caution and their own judgement in following the recommendations based on their individual health and dietary needs and preferences.

Your Nourish Account

To use the Project Nourish App, you will need to register and open a Samsung account, if you do not already have one.  Once you register, you will be automatically registered for all Samsung services, and you can sign on and sign off from all Samsung services and through all devices at once. You can access and change your password or registration data at any time by either accessing the relevant Samsung account application on your device.

You are entirely responsible for maintaining the confidentiality of your Nourish Account, including your user name and password, and for any and all activity that occurs under your Nourish Account.  You shall not disclose your user name or password(s) or grant any third party access to your Nourish Account.  You will keep your Nourish Account safe and secure at all times, including by avoiding obvious user names or passwords, and by changing your password regularly.  We may treat any instructions as coming from you if they are received from or issued by a third party using your Nourish Account, including through your user name and password or registration data.  You agree not to use the account of any other user without permission of the user or person holding the respective account.  You will let us know immediately if you learn of any unauthorized use of any of your Nourish Account or registration data or any other breach of security.  You may close your Nourish Account at any time provided that you immediately (a) cease using the Project Nourish App and providing or accessing any data or information by or through the Project Nourish App, and (b) destroy all copies, modifications, and merged portions of the Project Nourish App in any form, including any copy on any computer or mobile device.  We may also disable your Nourish Account at any time and without notice for any reason.

Connecting Third Party Devices and Programs

Your use of third party devices and third party software that connect to the Project Nourish App is solely between you and the applicable third party.  You should read and examine such third party’s terms of use and privacy policies before you use the Project Nourish App in connection with such third party devices and software.

Proprietary Rights and Licenses

The Project Nourish App, the Nourish Content and any other services, software or information provided by us in connection therewith, and all intellectual property rights therein (collectively “Nourish IPR”) belong to Samsung, Nestle, and/or our third party licensors.  Except as expressly provided herein, you are not entitled to use and shall have no interest in the Nourish IPR, or any data generated by other users of the Project Nourish App.  You acknowledge that you have only a revocable and limited right to use the Project Nourish App subject to this Agreement.

You are not permitted, except where expressly authorized in writing to do so, to change, adapt, copy, store, publish, rent license, sub-license, sell, hire, lend or distribute in any way any of the Nourish IPR, or to do any other act or omit to do any other act which may harm or otherwise prejudice any of the Nourish IPR.

User Submitted Content

You may be permitted to upload or submit content to the Project Nourish App in various forms (collectively, “User Content”).  By providing any User Content, you agree that it will not: (a) infringe any copyright, trademark, patent, trade secret, or other proprietary right of any party; (b) be profane, obscene, indecent or violate any law or regulation; (c) defame, abuse, harass, threaten or otherwise violate the legal rights (such as rights of privacy and publicity) of others; (d) incite discrimination, hate or violence towards one person or a group because of their belonging to a race, a religion or a nation; or (e) restrict or inhibit any other user from using the Project Nourish App.  You represent and warrant that the information that you provide to us about you in connection with Nourish will be current, true, accurate, supportable and complete. You further represent and warrant that you have all necessary rights, licenses, consents and approvals to provide us with any User-related information and data in connection with the Project Nourish App.

We do not claim ownership over any User Content uploaded or submitted to the Project Nourish App.  However, by uploading or submitting User Content to the Project Nourish App, or otherwise providing User Content to us, you hereby grant us a non-exclusive, royalty-free, world-wide, perpetual, transferable license to store, reproduce, adapt, distribute, display and use, for any lawful purpose, any and all User Content uploaded or submitted to the Project Nourish App or that is otherwise provided to us and any and all other data and information collected or generated by the Project Nourish App in connection with such User Content.

Any ideas, advice, recommendations, suggestions, enhancement requests, feedback or proposals provided by you to us (“Feedback”), by their receipt grant us with a royalty free, worldwide, transferable, sub-licensable, irrevocable, perpetual license to commercialize, use and incorporate such Feedback into our software, services or systems or use as we otherwise deems necessary or desirable in our business.

Privacy and Protection of Personal Data

Our Privacy Policy, which is available to all Users when they sign up for the Project Nourish App, as we may update from time to time in accordance with its terms, explains how we handle personal data and information and protects your privacy when you use the Project Nourish App.  By downloading or using the Project Nourish App, you specifically acknowledge and consent that any personal information that you provide to us is subject to our Privacy Policy, which is incorporated by reference into this Agreement, including your agreement to the transfer of your personal information to the United States or Switzerland for processing by Samsung, Nestle, and/or our third party providers to enable us to provide the Project Nourish App to you and otherwise to comply with our legal or regulatory requirements or to further our legitimate business interests.

Term and Termination

The term of this Agreement commences on the Effective Date and continues until terminated by us or you. You may terminate this Agreement by ceasing your use of and uninstalling the Project Nourish App. We may terminate this Agreement at any time in our sole discretion. This Agreement and the license granted hereunder automatically terminate if you fail to comply with any provision of this Agreement.  You agree upon termination to: (a) cease using the Project Nourish App and providing or accessing any data or information by or through the Project Nourish App, and (b) destroy all copies, modifications, and merged portions of the Project Nourish App in any form, including any copy on any computer or mobile device. The provisions of the following Sections shall survive the termination of this Agreement:  License Restrictions, Use of the Project Nourish App, Your Nourish Account, Proprietary Rights and Licenses, User Submitted Content, Term and Termination, Disclaimer of Warranty, Limitations of Liability, Indemnification, Export and Other Restrictions and General Provisions.

Disclaimer of Warranty

THE PROJECT NOURISH APP, NOURISH CONTENT AND ANY OTHER SERVICES AND/OR INFORMATION PROVIDED BY US ARE PROVIDED “AS IS” AND ON AN “AS AVAILABLE” BASIS, WITHOUT WARRANTIES OF ANY KIND FROM SAMSUNG, NESTLE, OR OUR THIRD PARTY LICENSORS, EITHER EXPRESS OR IMPLIED.  TO THE FULLEST EXTENT POSSIBLE PURSUANT TO APPLICABLE LAW, SAMSUNG AND NESTLE DISCLAIM ALL WARRANTIES EXPRESS, IMPLIED OR STATUTORY, INCLUDING, BUT NOT LIMITED TO, IMPLIED WARRANTIES OF MERCHANTABILITY, SATISFACTORY QUALITY OR FITNESS FOR A PARTICULAR PURPOSE, RELIABILITY OR AVAILABILITY, ACCURACY, LACK OF VIRUSES, NON INFRINGEMENT OF THIRD PARTY RIGHTS OR OTHER VIOLATION OF RIGHTS.  NO ADVICE OR INFORMATION, WHETHER ORAL OR WRITTEN, OBTAINED BY YOU FROM SAMSUNG, NESTLE OR OUR AFFILIATES SHALL BE DEEMED TO ALTER THIS DISCLAIMER, OR TO CREATE ANY WARRANTY OR ANY SORT FROM SAMSUNG OR NESTLE.

Limitations of Liability

TO THE FULLEST EXTENT ALLOWED AND PERMITTED BY APPLICABLE LAWS AND REGULATIONS, NEITHER SAMSUNG OR NESTLE SHALL, UNDER ANY CIRCUMSTANCES, BE LIABLE TO YOU OR ANY THIRD PARTY FOR PERSONAL INJURY OR ANY CONSEQUENTIAL, EXEMPLARY, INCIDENTAL, INDIRECT, PUNITIVE OR SPECIAL DAMAGES WHATSOEVER, INCLUDING, WITHOUT LIMITATION, DAMAGES FOR DELAY OF DELIVERY, LOSS OF PROFITS, OR LOSS OF DATA, ARISING OUT OF OR IN RELATION TO THIS AGREEMENT, YOUR USE OF THE PROJECT NOURISH APP OR THE NOURISH CONTENT, ANY THIRD PARTY PRODUCTS OR OTHER PRODUCTS OR SERVICES PROVIDED BY OR THROUGH SAMSUNG AND/OR NESTLE UNDER THIS AGREEMENT, WHETHER BASED ON BREACH OF CONTRACT, BREACH OF WARRANTY, TORT (INCLUDING NEGLIGENCE), PRODUCT LIABILITY OR OTHERWISE, EVEN IF SAMSUNG AND/OR NESTLE HAS BEEN ADVISED OF OR IS AWARE OF THE POSSIBILITY OF SUCH DAMAGES AND NOTWITHSTANDING THE FAILURE OF ESSENTIAL PURPOSE OF ANY REMEDY. YOUR UNDERSTANDING, ACKNOWLEDGEMENT AND ACCEPTANCE OF THESE TERMS OF SERVICE ARE THE LEGAL BASIS AND CONSIDERATION FOR YOUR ACCESS TO THE APPLICATIONS AND SERVICES OFFERED BY SAMSUNG AND/OR NESTLE IN CONNECTION WITH THE PROJECT NOURISH APP.  IN NO EVENT SHALL SAMSUNG'S AND/OR NESTLE’S TOTAL LIABILITY TO YOU UNDER THIS AGREEMENT FOR ALL DAMAGES (OTHER THAN AS MAY BE REQUIRED BY APPLICABLE LAW IN CASES INVOLVING PERSONAL INJURY) EXCEED THE GREATER OF ONE THOUSAND DOLLARS ($1,000).

Indemnification

You agree to defend, indemnify and hold harmless Samsung and/or Nestle from and against damages, liabilities and reasonable costs and expenses, including reasonable legal fees arising out of or relating to: (a) your use of the Project Nourish App in violation of the terms of this Agreement and (b) User Content or any other data or content included in or omitted from content and data input into the Project Nourish App by you or any other third party using your account or device.

Export and Other Restrictions

You agree to comply fully with all relevant export laws and regulations of the United States and any other applicable jurisdiction ("Export Laws") to assure that neither the Project Nourish App nor any direct product thereof is (a) exported, directly or indirectly, in violation of Export Laws; or (b) are used for any purposes prohibited by the Export Laws, including, without limitation, nuclear, chemical, or biological weapons proliferation.  The Project Nourish App and the related documentation are “Commercial Items,” as that term is defined at 48 C.F.R. §2.101, consisting of “Commercial Computer Software” and “Commercial Computer Software Documentation,” as such terms are used in 48 C.F.R. §12.212 or 48 C.F.R. §227.7202, as applicable. The Commercial Computer Software and Commercial Computer Software Documentation are being licensed to any U.S. Government end users (a) only as Commercial Items, and (b) with only those rights as are granted to all other end users pursuant to the terms and conditions herein.

General Provisions

The parties will rely only on the terms of this Agreement. Any representations, statements or agreements made or entered into elsewhere, whether directly or indirectly, written or oral or in advertising are not binding on Samsung or Nestle unless expressly confirmed in writing by Samsung or Nestle to you.  Any amendments to and waivers under this Agreement shall only be valid if in writing and signed by an executive of both Samsung and Nestle, or in the case of you, accepted via a click-to accept mechanism or by your continued use of the Project Nourish App after an amended version of this Agreement has been made available to you, which you agree constitutes your acceptance of such amendments.

We may provide you with a translation of the English version of this Agreement.  Such translation is provided for your convenience only.  If there is any conflict between the English language version and the translation version of this Agreement, to the extent permissible by applicable law, the English language version takes precedence.

We may provide you with notices (including notices relating to changes to the Agreement or termination of the Services or parts thereof) by email, ordinary mail, or postings on or via the Project Nourish App.

Failure by Samsung or Nestle to exercise or enforce any legal right or remedy under this Agreement or which Samsung or Nestle has the benefit of under any applicable law will not constitute or be construed as a waiver of Samsung's or Nestle’s rights or remedies.

If any provision of this Agreement is held to be invalid, illegal or unenforceable (in whole or in part) as determined by a court of competent jurisdiction, the validity, legality and enforceability of the remaining provisions shall not in any way be affected or impaired.

You may not transfer, assign or otherwise dispose of this Agreement or any of your rights or obligations arising under this Agreement without the prior written consent of Samsung and Nestle.

This Agreement shall be governed by and construed in accordance with the laws of the State of California, USA, excepting California rules regarding conflict-of-laws rules and the California Arbitration Act (Cal. Civ. Proc. Code §§ 1280-1294.2).   All disputes, controversies or claims between the parties arising out of or in connection with this Agreement (including its existence, validity or termination) will be finally resolved by arbitration to be held in Santa Clara, California and conducted in English under the Rules of Arbitration of the International Chamber of Commerce. The arbitral award shall be final and binding on the parties. Except to the extent entry of judgment and any subsequent enforcement may require disclosure, all matters relating to the arbitration, including the award, shall be held in confidence. Regardless of arbitration requirement above, you, Samsung, and Nestle may enforce your and our respective intellectual property rights in any court of competent jurisdiction, including but not limited to equitable relief.

YOU EXPRESSLY ACKNOWLEDGE AND AGREE TO THE TERMS AND CONDITIONS STATED HEREIN.

Copyrights

Copyright © 2016 Samsung Semiconductor, Inc.  All rights reserved.

This documentation and the corresponding software and mobile applications are the property of Samsung and are licensed to you under the terms of this End User License Agreement. Unauthorized use or copying of the software or mobile applications, documentation, or any other associated materials is a violation of state and federal laws. These materials must be returned to Samsung if so demanded.

Last Revised December 15, 2016

PRIVACY POLICY

Version:  1

Your access to, and use of, the Nourish application, connected applications and associated accessories (“Project Nourish App”) is subject to Samsung Semiconductor, Inc.’s (“Samsung”) and Nestec Ltd. (“Nestle”) (collectively, “we,” “us,” “our”) End User License Agreement.  We have created this Privacy Policy to explain what information we gather from you when you access the Project Nourish App, how we may use this information, and the security approaches we use to protect your information.   This Privacy Policy is incorporated and made part of the Nourish End User License Agreement.

By using the Project Nourish App, you consent to the collection and use of your information by Samsung and Nestle in accordance with this Privacy Policy.  If we decide to change our privacy policy, we will post those changes on this page. Policy changes will apply only to information collected after the date of the change.

1. WHAT INFORMATION IS SHARED WITH US

We may collect information from you when you register for the Project Nourish App, respond to communication such as e-mail, or participate in another application feature.

•	When ordering or registering, we may ask you for your name, e-mail address, mailing address, phone number, credit card information or other information. You may, however, visit the Project Nourish App without providing such information.
•	We use IP addresses and session identifiers to analyze trends, to administer the site, to track user activities, to infer user interests, and to otherwise induce, deduce, and gather information about individual users and market segments.

Further, we collect and store the following information that you provide directly or generate through your use of the Project Nourish App:
-	gender
-	height, weight and activity levels
-	device information including identifiers
-	firmware version of device
-	application version and major setting values for the Nourish application
-	in-app usage information such as menus and setting clicked, features used and any errors displayed
-	connected accessories make, model, identifiers
-	third-party application identifier
-	profile and activity-related information such as user name, date of birth, postings
-	fitness and nutrition-related information such as types of exercises, food, caffeine and water intake
-	meal preferences
-	health condition
-	fitness
-	steps
-	heartrate
-	location information (where you are using location-based features and location settings have been activated on your device)accelerometer data, gyroscope data and real-time location (based on information provided by your device OS).
-	travel and routine behavior, such as driving, walking, biking and public transport (train, metro and bus) travel periods and distances.

Further, we also collects non-personal information that does not directly or in combination with other information identify you as an individual, such as your type of device, operating system (‘OS’), type of mobile browser, your use of Nourish, your real-time location (based on information provided by your device OS), accelerometer data and gyroscope data.  Further, depending on your device make and type and your permissions, Nourish also collects the following data supplied by the device’s OS: activity information, step detection and count, Bluetooth information and battery information.

Because there is not yet a common understanding of how to interpret web browser-based “Do Not Track” (“DNT”) signals other than cookies, we do not currently respond to undefined “DNT” signals to the Project Nourish App.

2. WHAT WE DO WITH THE INFORMATION WE COLLECT

We use this information to provide services or features that you request, including data back-up and sync services for Samsung account users, to offer customized content and recommendations, and to provide optimized service combined to compatible Samsung or Nestle services and devices.  We may conduct data analysis and statistics to improve our services.

We may use the information we collect from you when you register, respond to a survey or marketing communication, surf the application, or use certain other Project Nourish App features in the following ways:
•	To personalize your Project Nourish App experience and to allow us to deliver the type of content and product offerings in which you are most interested.
•	To allow us to better service you in responding to your customer service requests.
•	To quickly process your transactions.
•	To administer a promotion, survey or other Project Nourish App feature.

Additionally, we may also use non-personal information (including location data) to build pseudonymous profiles to that may allow us to detect where you live and work, when and by what means you are travelling and to make an estimated guess of your age, gender and routine behavior.

When you use the Project Nourish App with third party services or applications, you may choose to share or synchronize your information with such services and applications.  We will grant access to such other services and third party applications only when you authorize us to do so.

With your consent, we may send promotional push notifications to your mobile device (you can change your notification settings at any time in the settings menu of the Project Nourish App).

3. DO WE USE “COOKIES”?

Yes. Cookies are small files that a site or its service provider transfers to your mobile device’s hard drive through your Web browser (if you allow) that enables the site's or service provider's systems to recognize your browser and capture and remember certain information. For instance, we use cookies to help us remember and process the devices you are using in connection with the Project Nourish App. Cookies are also used to help us understand your preferences based on previous or current site activity, which enables us to provide you with improved services. We also use cookies to help us compile aggregate data about site traffic and site interaction so that we can offer better site experiences and tools in the future.

We may contract with third-party service providers to assist us in better understanding how our users are using the Project Nourish App.  These service providers are not permitted to use the information collected on our behalf except to help us conduct and improve our business.

You can choose to have your mobile device warn you each time a cookie is being sent, or you can choose to turn off all cookies. You do this through your browser (like Mozilla Firefox or Internet Explorer) settings. Each browser is a little different, so look at your browser Help menu to learn the correct way to modify your cookies. If you turn cookies off, you won't have access to many features that make your experience using the Project Nourish App more efficient and some of our services will not function properly.

4. TO WHOM WE DISCLOSE YOUR INFORMATION

We generally do not sell, trade, or otherwise transfer to outside parties your personally identifiable information unless we provide you with advance notice, except as described herein. The term "outside parties" does not include our affiliates and website hosting partners and other parties who assist us in operating, testing, or improving the Project Nourish App, conducting our business, or servicing you, so long as those parties agree to keep this information confidential. In the event that either of us sells our respective companies, or otherwise transfers any assets of our respective companies, we may provide your information to the purchaser so that you may continue the relationship or business with us and our products.  We may also release your information when we believe release is appropriate to comply with the law, enforce Nourish or other Samsung or Nestle policies, or protect ours or others' rights, property, or safety.

However, non-personally identifiable visitor information may be provided to other parties for marketing, advertising, or other uses.

5. CONSENT TO INFORMATION ABOUT INTERNATIONAL TRANSFERS

It is important to note that Nourish is operated in the United States and Switzerland. If you are located outside of the United States or Switzerland, please be aware that any information you provide to us will be transferred to the United States and/or Switzerland. By using the Project Nourish App and/or providing us with any information, you consent to the collection, transfer, storage and processing of your information collected by the Project Nourish App outside your country of residence (including, in particular, South Korea), consistent with and for the purposes described in this Privacy Policy.

6. SAFEGUARDING YOUR PERSONAL INFORMATION

We follow generally accepted industry security standards to safeguard and help prevent unauthorized access, maintain data security and correctly use your personally identifiable information. However, no commercial method of information transfer over the Internet or electronic data storage is known to be 100% secure. As a result, we cannot guarantee the absolute security of that information during its transmission or its storage in our systems.

7. PRIVACY AND THIRD PARTY LINKS

This Privacy Policy applies solely to information collected by the Project Nourish App.  In an attempt to provide you with increased value, we may include third party links on or through the Project Nourish App. These linked sites have separate and independent privacy policies. We therefore have no responsibility or liability for the content and activities of these linked sites. Nonetheless, we seek to protect the integrity of the Project Nourish App and welcome any feedback about these linked sites (including if a specific link does not work).

8. MOBILE AND ONLINE POLICY ONLY

This Privacy Policy applies only to information collected through the Project Nourish App and not to information collected offline.

10. HOW TO DELETE YOUR INFORMATION

You may delete your account and uninstall the Nourish Beta App at any time to immediately stop any collection of data.  When you delete the Project Nourish App, the information saved in your device will be deleted but any information collected and stored by us will be still remain in our server.  If you want to delete the information collected through the Project Nourish App from both the device and the Project Nourish App server, you can do so by going to Settings and selecting Reset Data.  However, any third party applications you have authorized us to share or synchronize your information with may continue to store your information subject to their own privacy policies and practices. You may also opt-out of the from collection of your real-time location data through the OS settings of your device. If you do not want to delete your account but want to temporarily stop any collection of information, you can do so by uninstalling the Nourish application.

11. QUESTIONS AND FEEDBACK

We welcome your questions, comments, and concerns about the Project Nourish App. Please send us any and all feedback or questions pertaining to the Project Nourish App to Nourishsupport@ssi.samsung.com with "WEBSITE PRIVACY POLICY" in the subject line.

12. YOUR CALIFORNIA PRIVACY RIGHTS

Under California’s "Shine the Light" law, California residents who provide personal information in obtaining products or services for personal, family or household use are entitled to request and obtain from us once a calendar year information about the customer information we shared, if any, with other businesses for their own direct marketing uses. If applicable, this information would include the categories of customer information and the names and addresses of those businesses with which we shared customer information for the immediately prior calendar year (e.g. requests made in 2015 will receive information regarding 2014 sharing activities).

To obtain this information from us please send an email message to Nourishsupport@ssi.samsung.com with "Request for California Privacy Information" on the subject line and in the body of your message. We will provide the requested information to you at your e-mail address in response.  Not all information sharing is covered by the "Shine the Light" requirements and only information on covered sharing will be included in our response.

13. CHILDREN’S PRIVACY

We do not knowingly intend to collect personally identifiable information from users under 18 years of age. If a user under 18 has provided us with personally identifiable information, the user or a parent or guardian of the user may send an email message to Nourishsupport@ssi.samsung.com with “Request for Child Information Removal” on the subject line and the name and age of the user in the body of the message. We will then make reasonable efforts to delete the user’s information from the database that stores information for the Project Nourish App.

ACKNOWLEDGMENT

By selecting “I agree,” you confirm that you have carefully read and understood all terms and conditions of this Privacy Policy, and you hereby give your consent to the collection, use, storage, sharing, disclosure and other processing of your personal information, including your sensitive personal information, in accordance with the terms set forth in this Privacy Policy.

Copyrights

Copyright © 2016 Samsung Semiconductor, Inc.  All rights reserved.

This documentation and the corresponding software and mobile applications are the property of Samsung and are licensed to you under the terms of this End User License Agreement. Unauthorized use or copying of the software or mobile applications, documentation, or any other associated materials is a violation of state and federal laws. These materials must be returned to Samsung if so demanded.

Last Revised December 15, 2016

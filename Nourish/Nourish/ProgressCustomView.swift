//
//  ProgressCustomView.swift
//  Nourish
//
//  Created by Vlad Birukov on 14.06.17.
//  Copyright © 2017 SSIC. All rights reserved.
//

import UIKit


class ProgressCustomView: UIView {

    let consumedColor = NRColorUtility.hexStringToUIColor(hex: "1172B9")
    let activityColor = NRColorUtility.hexStringToUIColor(hex: "70C397")
    let overColor = UIColor.red
    
    var target: CGFloat = 0.0
    var consumed: CGFloat = 0.0
    var activity: CGFloat = 0.0
    
    func setupWith(consumedCalories: Double, targetCalories: Double, activityCalories: Double) {
        
        self.target = CGFloat(targetCalories)
        self.consumed = CGFloat(consumedCalories)
        self.activity = CGFloat(activityCalories)
        
        self.setNeedsDisplay()
    }

    override func draw(_ rect: CGRect) {
        let heigth = frame.height
        let width = frame.width
        
        if self.consumed > (self.target + self.activity) {
            self.drawRectangle(x: 0, y: 0, width: width, heigth: heigth, color: UIColor.red)
            let activityWidth = width * self.activity / self.consumed
            let targetWidth = width * self.target / self.consumed
            self.drawRectangle(x: targetWidth, y: 0, width: activityWidth, heigth: heigth, color: UIColor.black.withAlphaComponent(0.26))
        }
        else {
            let total = self.target + self.activity
            let activityWidth = width * self.activity / total
            let consumedWidth = width * self.consumed / total
            self.drawRectangle(x: width - activityWidth, y: 0, width: activityWidth, heigth: heigth, color: self.activityColor)
            self.drawRectangle(x: 0, y: 0, width: consumedWidth, heigth: heigth, color: self.consumedColor)
            if consumed + activity > total {
                self.drawRectangle(x: width - activityWidth, y: 0, width:activityWidth + consumedWidth - width, heigth: heigth, color: UIColor.black.withAlphaComponent(0.26))
            }
        }
    }
}

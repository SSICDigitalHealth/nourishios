//
//  GenerateMealPlanInteractor.swift
//  Nourish
//
//  Created by Gena Mironchyk on 9/13/17.
//  Copyright © 2017 SSIC. All rights reserved.
//

import UIKit
import RxSwift
class GenerateMealPlanInteractor: NSObject {
    private let mealPlanRepository = MealPlannerRepository()
    
    func executeFor(date : Date) -> Observable<Bool> {
        return self.mealPlanRepository.generateWeeklyPlanFor(date: date)
    }
}

//
//  ActivityConfig.swift
//  Nourish
//
//  Created by Andrey Zmushko on 8/23/17.
//  Copyright © 2017 SSIC. All rights reserved.
//

import UIKit

enum ActivityCells {
    case datePicker
    case stayActive
    case caloricProgress
    case caloricProgressBar
    case caloricProgressNotification
    case caloriesBurned
    case caloricBurned
    case caloricConsumed
    case caloriesBurnedChart
    case steps
    case distance
    case sleep
    case exercise
    case stress
    case weight
    case defaultCell

    var attributes: (nibName: String, identifier: String, delegate: ActivityBaseCellDelegate) {
        switch self {
        case .datePicker:
            return ("DatePickerTableViewCell", "cellDatePicker", ActivityBaseCellDelegate())
        case .stayActive:
            return ("ActivityBaseTableViewCell", "cellStayActive", ActivityStayActiveCellDelegate())
        case .caloricProgress:
            return ("ActivityBaseTableViewCell", "cellCaloricProgress", ActivityCaloricProgressCellDelegate())
        case .caloricProgressBar:
            return ("ActivityBaseTableViewCell", "cellCaloricProgressBar", ActivityBaseCellDelegate())
        case .caloricProgressNotification:
            return ("ActivityBaseTableViewCell", "cellCaloricProgressNotification", ActivityBaseCellDelegate())
        case .caloriesBurned:
            return ("ActivityBaseTableViewCell", "cellCaloriesBurned", ActivityCaloriesBurnedCellDelegate())
        case .caloricBurned:
            return ("ActivityBaseTableViewCell", "cellCaloriesConsumed", ActivityCaloricBurnedCellDelegate())
        case .caloricConsumed:
            return ("ActivityBaseTableViewCell", "cellCaloriesConsumed", ActivityCaloricConsumedCellDelegate())
        case .caloriesBurnedChart:
            return ("ActivityBaseTableViewCell", "cellCaloriesBurnedChart", ActivityBaseCellDelegate())
        case .steps:
            return ("ActivityBaseTableViewCell", "cellSteps", ActivityStepsCellDelegate())
        case .distance:
            return ("ActivityBaseTableViewCell", "cellDistance", ActivityDistanceCellDelegate())
        case .sleep:
            return ("ActivityBaseTableViewCell", "cellSleep", ActivitySleepCellDelegate())
        case .exercise:
            return ("ActivityBaseTableViewCell", "cellExercise", ActivityExerciseCellDelegate())
        case .stress:
            return ("ActivityBaseTableViewCell", "cellStress", ActivityStressCellDelegate())
        case .weight:
            return ("ActivityBaseTableViewCell", "cellWeight", ActivityWeightCellDelegate())
        case .defaultCell:
            return ("ActivityBaseTableViewCell", "cellActivityDefault", ActivityBaseCellDelegate())
        }
    }

}

class ActivityConfig: NSObject {

    enum Period {
        case daily
        case weekly
        case monthly
        
        func order(state: State) -> [ActivityCells] {
            switch self {
            case .daily:
                switch state {
                case .progress:
                    return [.datePicker, .stayActive, .caloricProgress, .caloriesBurned, .steps, .distance, .sleep, .exercise, .stress, .weight]
                case .report:
                    return [.datePicker, .caloricProgress, .caloriesBurned, .steps, .distance, .sleep, .exercise, .stress, .weight]
                }
                
            case .weekly:
                switch state {
                case .progress:
                    return [.datePicker, .caloricProgress, .caloricBurned, .caloricConsumed, .steps]
                case .report:
                    return [.datePicker]
                }
            case .monthly:
                return []
            }
        }
    }
    
    enum State {
        case progress
        case report
    }
    
    enum Present {
        case dashboard
        case card
    }
    
    enum Card {
        case stress
    }

    var date = Date()
    var period: Period = .daily
    var state: State = .progress
    var present: Present = .dashboard
    var card: Card = .stress
    
    init(date: Date, period: Period, state: State, present: Present, card: Card) {
        self.date = date
        self.card = card
        self.state = state
        self.present = present
        self.period = period
    }
    
    static func != (left: ActivityConfig, right: ActivityConfig) -> Bool {
        return !(
            left.card == right.card &&
            left.period == right.period &&
            left.state == right.state &&
            left.present == right.present)
    }
    
    
}


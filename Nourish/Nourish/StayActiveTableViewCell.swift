//
//  StayActiveTableViewCell.swift
//  Nourish
//
//  Created by Vlad Birukov on 31.08.17.
//  Copyright © 2017 SSIC. All rights reserved.
//

import UIKit

class StayActiveTableViewCell: ActivityBaseCell {
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var captionImageView: UIImageView!
    @IBOutlet weak var baseView: BaseView!
    @IBOutlet weak var topCaption: NSLayoutConstraint!
    @IBOutlet weak var bottonCaption: NSLayoutConstraint!
    @IBOutlet weak var bottonMessage: NSLayoutConstraint!
    @IBOutlet weak var holderTop: NSLayoutConstraint!
    @IBOutlet weak var holderHeight: NSLayoutConstraint!
    @IBOutlet weak var imageTop: NSLayoutConstraint!
    @IBOutlet weak var imageBotton: NSLayoutConstraint!
    @IBOutlet weak var separatorLine: UIView!



    let startText = "To achieve your goal, you will need to walk"
    let endText = "steps before the end of the day."
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.captionImageView.imageNamedWithTint(named: "idea", tintColor: UIColor.white)

        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    func setupWith(model: StayActiveViewModel) {
        if model.currentStepsToGoal != nil {
            if model.currentStepsToGoal! <= 0.0 {
                self.hideCell()
            } else {
                self.descriptionLabel.text = String(format: "%@ %.0f %@", self.startText, model.currentStepsToGoal!, self.endText)
            }
        }
    }
    
    private func hideCell() {
        self.descriptionLabel.text = ""
        self.titleLabel.text = ""
        
        separatorLine.backgroundColor = UIColor.clear
        topCaption.constant = 0
        bottonCaption.constant = 0
        bottonMessage.constant = 0
        holderTop.constant = 0
        holderHeight.constant = 0
        imageTop.constant = 0
        imageBotton.constant  = 0
    }
}

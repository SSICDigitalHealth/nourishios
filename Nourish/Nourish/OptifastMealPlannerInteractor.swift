//
//  OptifastMealPlannerInteractor.swift
//  NourIQ
//
//  Created by Gena Mironchyk on 1/10/18.
//  Copyright © 2018 SSIC. All rights reserved.
//

import UIKit
import RxSwift
class OptifastMealPlannerInteractor: NSObject {
    
    private let mealPlannerRepository = MealPlannerRepository()
    private let favRepo = FavoritesRepository()
    
    
    func execute(date: Date) -> Observable <OptifastMealPlannerModel?> {
        let mealPlannerFood = self.mealPlannerRepository.getOptifastPlanFor(date: date)
        let favoriteMeal = self.favRepo.fetchFavoritesWithCache()
        
        let zip = Observable.combineLatest(favoriteMeal, mealPlannerFood, resultSelector: {favMeal, mealFood -> OptifastMealPlannerModel? in
            if mealFood != nil {
                let mealPlannerModel = mealFood
                if let foods = mealPlannerModel?.foods {
                    for foodModel in foods {
                        if favMeal.count > 0 {
                            for favorite in favMeal {
                                foodModel.ocasionFoods = self.isFavorite(favorite: favorite, foodsArray: foodModel.ocasionFoods)
                            }
                        }
                    }
                }
                return mealPlannerModel
            }
            
            return nil
        })
        
        return zip
    }
    
    
    private func isFavorite(favorite: MealRecordModel, foodsArray: [(meal : foodSearchModel,mealReciepe : RecipiesModel?)]) -> [(meal : foodSearchModel, mealReciepe : RecipiesModel?)]{
        var mealsModel = [(meal : foodSearchModel,mealReciepe : RecipiesModel?)]()
        let favoriteTitle = favorite.groupedMealArray.filter({$0.foodTitle != nil}).map({return $0.foodTitle!})
        let mealFoodTitle = foodsArray.map({return $0.meal.foodTitle})
        
        if (favoriteTitle.count == mealFoodTitle.count) && mealFoodTitle.sorted() == favoriteTitle.sorted() {
            for food in foodsArray {
                var foodsearchModel = foodSearchModel()
                foodsearchModel = food.meal
                foodsearchModel.groupID = favorite.groupID
                foodsearchModel.isFavorite = true
                
                mealsModel.append((foodsearchModel, self.favoriteRecipies(food: food, favoriteTitle: favoriteTitle, favorite: favorite)))
            }
            
            return mealsModel
        } else {
            for food in foodsArray {
                mealsModel.append((food.meal, self.favoriteRecipies(food: food, favoriteTitle: favoriteTitle, favorite: favorite)))
            }
            return mealsModel
        }
    }
    
    private func favoriteRecipies(food: (meal : foodSearchModel, mealReciepe : RecipiesModel?), favoriteTitle: [String], favorite: MealRecordModel) -> RecipiesModel? {
        
        var recipiesModel: RecipiesModel?
        if food.mealReciepe != nil {
            recipiesModel = RecipiesModel()
            recipiesModel = food.mealReciepe
            
            if favoriteTitle.count == 1 && favoriteTitle.sorted() == [recipiesModel!.nameFood] {
                recipiesModel?.isFavorite = true
                recipiesModel?.groupId = favorite.groupID
            }
        }
        
        return recipiesModel
    }
    
}

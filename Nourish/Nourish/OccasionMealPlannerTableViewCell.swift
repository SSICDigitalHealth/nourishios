//
//  OccasionMealPlannerTableViewCell.swift
//  Nourish
//
//  Created by Vlad Birukov on 22.08.17.
//  Copyright © 2017 SSIC. All rights reserved.
//

import UIKit

class OccasionMealPlannerTableViewCell: UITableViewCell {
    @IBOutlet weak var nameFood: UILabel!
    @IBOutlet weak var amount: UILabel!


    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func setUpWithModel(model: foodSearchModel) {
        self.nameFood.text = String(format: "%@", model.foodTitle)
        self.nameFood.setLineSpacing(spacing: 4)
        self.amount.text = String(format: "%@ %@", NRFormatterUtility.numberToStringMealPlanner(number: model.amount), model.unit)
        self.amount.setLineSpacing(spacing: 4, alignment: .right)

    }
    
}

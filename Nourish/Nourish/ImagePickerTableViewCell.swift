//
//  ImagePickerTableViewCell.swift
//  Nourish
//
//  Created by Nova on 5/16/17.
//  Copyright © 2017 SSIC. All rights reserved.
//

import UIKit

class ImagePickerTableViewCell: UITableViewCell {
    
    @IBOutlet weak var nameLabel : UILabel?
    @IBOutlet weak var unitLabel : UILabel?
    @IBOutlet weak var calorieLabel : UILabel?
    @IBOutlet weak var checkMarkButton : UIButton?

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func drawWithModel(model : foodSearchModel, selected : Bool) {
        self.nameLabel?.text = model.foodTitle
        self.unitLabel?.text = String(format : "%.1f %@",model.amount, model.unit)
        let calories = Double(model.calories)
        self.calorieLabel?.text = String(format : "%.1f Cal",calories!)
        
        let checkMarkedImage = UIImage(named: "circle_selected")
        let uncheckMarked =  UIImage(named: "circle_select")
        
        let imageToDraw = selected ? checkMarkedImage : uncheckMarked
        
        self.checkMarkButton?.setImage(imageToDraw, for: .normal)
    }
    
}

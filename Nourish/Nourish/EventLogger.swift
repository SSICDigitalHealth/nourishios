//
//  EventLogger.swift
//  Nourish
//
//  Created by Gena Mironchyk on 5/11/17.
//  Copyright © 2017 SSIC. All rights reserved.
//

import UIKit
import RxSwift


private let EVENT_SAVE_FOOD = "Logged_food";
private let EVENT_SAVE_FOOD_GROUP = "Logged_food_group";
private let EVENT_SAVE_FOOD_TO_BREAKFAST_USING_CAMERA = "Logged_food_to_breakfast_using_camera";
private let EVENT_SAVE_FOOD_TO_LUNCH_USING_CAMERA = "Logged_food_to_lunch_using_camera";
private let EVENT_SAVE_FOOD_TO_DINNER_USING_CAMERA = "Logged_food_to_dinner_using_camera";
private let EVENT_SAVE_FOOD_TO_SNACK_USING_CAMERA = "Logged_food_to_snack_using_camera";
private let EVENT_SAVE_FOOD_TO_BREAKFAST = "Logged_food_to_breakfast";
private let EVENT_SAVE_FOOD_TO_LUNCH = "Logged_food_to_lunch";
private let EVENT_SAVE_FOOD_TO_DINNER = "Logged_food_to_dinner";
private let EVENT_SAVE_FOOD_TO_SNACK = "Logged_food_to_snack";
private let EVENT_SAVE_FOOD_TO_BREAKFAST_PREVIOUS_DAY = "Logged_food_to_breakfast_for_previous_days";
private let EVENT_SAVE_FOOD_TO_LUNCH_PREVIOUS_DAY = "Logged_food_to_lunch_for_previous_days";
private let EVENT_SAVE_FOOD_TO_DINNER_PREVIOUS_DAY = "Logged_food_to_dinner_for_previous_days";
private let EVENT_SAVE_FOOD_TO_SNACK_PREVIOUS_DAY = "Logged_food_to_snack_for_previous_days";
private let EVENT_CLICK_ON_SEARCH_BUTTON = "Clicks_on_search_button";
private let EVENT_OPEN_FOOD_CARD = "Food_card_was_opened";
private let EVENT_MODIFIED_PORTION_SIZE = "Portion_size_was_modified";
private let EVENT_CLICK_ON_LOG_FOOD = "Clicked_on_Log_food";
private let EVENT_SAVE_FOOD_FAVORITE_TO_BREAKFAST = "Logged_from_favorite_to_breakfast";
private let EVENT_SAVE_FOOD_FAVORITE_TO_LUNCH = "Logged_from_favorite_to_lunch";
private let EVENT_SAVE_FOOD_FAVORITE_TO_DINNER = "Logged_from_favorite_to_dinner";
private let EVENT_SAVE_FOOD_FAVORITE_TO_SNACK = "Logged_from_favorite_to_snack";
private let EVENT_BARCODE_SCAN = "Barcode_scans";
private let EVENT_BARCODE_SCAN_SUCCESSFUL = "Barcode_scans_successful";
private let EVENT_SAVE_FOOD_FROM_RECOMMENDATION = "Logged_from_recommendation";
private let EVENT_SAVE_FOOD_TO_BREAKFAST_FROM_RECOMMENDATION = "Logged_from_recommendation_to_breakfast";
private let EVENT_SAVE_FOOD_TO_LUNCH_FROM_RECOMMENDATION = "Logged_from_recommendation_to_lunch";
private let EVENT_SAVE_FOOD_TO_DINNER_FROM_RECOMMENDATION = "Logged_from_recommendation_to_dinner";
private let EVENT_SAVE_FOOD_TO_SNACK_FROM_RECOMMENDATION = "Logged_from_recommendation_to_snack";
private let EVENT_SWIPE_FOOD_TO_BREAKFAST_FROM_RECOMMENDATION = "Swiped_from_recommendation_to_breakfast";
private let EVENT_SWIPE_FOOD_TO_LUNCH_FROM_RECOMMENDATION = "Swiped_from_recommendation_to_lunch";
private let EVENT_SWIPE_FOOD_TO_DINNER_FROM_RECOMMENDATION = "Swiped_from_recommendation_to_dinner";
private let EVENT_SWIPE_FOOD_TO_SNACK_FROM_RECOMMENDATION = "Swiped_from_recommendation_to_snack";
private let EVENT_CLICK_ON_ADD_BREAKFAST = "Clicked_on_Add_breakfast";
private let EVENT_CLICK_ON_ADD_LUNCH = "Clicked_on_Add_lunch";
private let EVENT_CLICK_ON_ADD_DINNER = "Clicked_on_Add_dinner";
private let EVENT_CLICK_ON_ADD_SNACK = "Clicked_on_Add_snack";
private let EVENT_VISITING_RECOMMENDATION_SCREEN = "Visiting_recommendation_screen";
private let EVENT_CLICK_ON_WEIGHT_SECTION = "Clicked_on_weight_section";
private let EVENT_CLICK_ON_WEEKLY_WEIGHT_SCREEN = "Clicked_on_weekly_weight_screen";
private let EVENT_CLICK_ON_MONTHLY_WEIGHT_SCREEN = "Clicked_on_monthly_weight_screen";
private let EVENT_CLICK_ON_NUTRITION_SECTION = "Clicked_on_nutrition_section";
private let EVENT_CLICK_ON_TODAY_NUTRITION_SCREEN = "Clicked_on_today_nutrition_screen";
private let EVENT_CLICK_ON_WEEKLY_NUTRITION_SCREEN = "Clicked_on_weekly_nutrition_screen";
private let EVENT_CLICK_ON_MONTHLY_NUTRITION_SCREEN = "Clicked_on_monthly_nutrition_screen";
private let EVENT_CLICK_ON_ACTIVITY_SECTION = "Clicked_on_activity_section";
private let EVENT_CLICK_ON_TODAY_ACTIVITY_SCREEN = "Clicked_on_today_activity_screen";
private let EVENT_CLICK_ON_WEEKLY_ACTIVITY_SCREEN = "Clicked_on_weekly_activity_screen";
private let EVENT_CLICK_ON_MONTHLY_ACTIVITY_SCREEN = "Clicked_on_monthly_activity_screen";
private let EVENT_CLICK_ON_REPORTS = "Clicked_on_Reports";
private let EVENT_CLICK_ON_PROFILE_SETTINGS = "Clicked_on_Profile_settings";
private let EVENT_CLICK_ON_MANAGE_GOAL = "Clicked_on_Manage_goal";
private let EVENT_CLICK_ON_CAMERA = "Clicked_on_Camera_icon";
private let EVENT_VISITING_HOME_SCREEN = "Visiting_home_screen";
private let EVENT_SEND_FEEDBACK = "Sending_feedback";
private let EVENT_LOGS_WEIGHT = "User_entered_weight";
private let EVENT_LOGS_WEIGHT_USING_WITHINGS = "User_entered_weight_using_withings";
private let EVENT_ESTORE_OPENED = "eStore_opened"
private let EVENT_ESTORE_OPENED_MENU = "eStore_opened_menu"
private let EVENT_LOGGED_FOOD_USING_VOICE_SEARCH = "Logged_food_to_using_voice_search"
private let EVENT_VOICE_SEARCH = "Voice_search"
private let EVENT_VOICE_SEARCH_RETURN_RESULT = "Voice_search_return_result"
private let EVENT_CLICKED_ON_CHAT = "Clicked_on_chat"
private let EVENT_CLICKED_ON_MEAL_PLANNER = "Clicked_on_meal_planner"

private let EVENT_CLICKED_ON_MEAL_RECOMMENDATION = "Clicked_on_meal_recomendation"
private let EVENT_CLICKED_ON_HYDRATION = "Clicked_on_hydration"
private let EVENT_CLICKED_ON_NUTRITIONAL_BALANCE = "Clicked_on_nutritional_balance"
private let EVENT_CLICKED_ON_VOICE_ICON = "Clicked_on_voice_icon"
private let EVENT_CLICKED_ON_FOOD_DIARY_ICON = "Clicked_on_food_diary_icon"
private let EVENT_CLICKED_ON_GENERATE_PLAN = "Clicked_on_generate_plan"
private let EVENT_CLICKED_ON_NUTRITION_SCORE = "Clicked_on_nutrition_score"

private let CLICKED_ON_WHAT_TO_EAT_NEXT_FOR_TODAY = "Clicked_on_what_to_eat_next_for_today"
private let CLICKED_ON_WHAT_TO_EAT_NEXT_FOR_WEEKLY = "Clicked_on_what_to_eat_next_for_weekly"
private let CLICKED_ON_WHAT_TO_EAT_NEXT_FOR_MONTHLY = "Clicked_on_what_to_eat_next_for_monthly"
private let CLICKED_ON_NUTRITIONAL_TIPS = "Clicked_on_nutritional_tips"
private let CLICKED_ON_RECIPE = "Clicked_on_recipe"

private let PROPERTY_USER_ID = "user_id";
private let PROPERTY_FOOD_ID = "food_id";
private let PROPERTY_FOOD_GROUP_ID = "food_group_id";
private let PROPERTY_SEARCH_FOOD = "search_food";
private let PROPERTY_BARCODE_SCAN = "barcode_scan";

let kUersWithEnabledDevModeKey = "Users with enabled dev mode"

private let analyticsSystemsArray : [AnalyticsProtocol] = [FirebaseAnalytics(),FlurryAnalytics()]

class EventLogger: NSObject {
    
    class func initAnalyticsSystems () -> Observable<Bool> {
        if NRUserSession.sharedInstance.accessToken != nil {
            
            let userInDevMode = self.userIsInDeveloperMode()
            let userRepo = UserRepository.shared
            let currentUser = userRepo.getCurrentUser(policy: .Cached)
            
            return Observable.zip(userInDevMode,currentUser, resultSelector : {devModeEnabled, userProfile -> Bool in 
                if devModeEnabled == false {
                    var userMeta = [String : Any]()
                    userMeta["email"] = userProfile.email ?? ""
                    userMeta["artikUserId"] = userProfile.userID ?? ""
                    userMeta["Version"] = Bundle.main.infoDictionary?["CFBundleShortVersionString"] as? String ?? ""
                    for system in analyticsSystemsArray {
                        system.initializeWith(userMeta: userMeta)
                    }
                    return true
                } else {
                    return false
                }
            })
            
        } else {
            return Observable.just(false)
        }
    }
    
    class func logEventWith(name : String) {
        if NRUserSession.sharedInstance.accessToken != nil {
            let _ = self.userIsInDeveloperMode().subscribe(onNext: {devModeEnabled in
                if devModeEnabled == false{
                        for system in analyticsSystemsArray {
                        system.logEventWith(name: name)
                    }
                }
            }, onError: {error in}, onCompleted: {}, onDisposed: {})
            
        }
    }
    
    class func userIsInDeveloperMode () -> Observable<Bool> {
        let arrayOfEnabledUsers = defaults.array(forKey: kUersWithEnabledDevModeKey) as? [String]
        let userRepo = UserRepository.shared
        let result = userRepo.getCurrentUser(policy: .Cached).flatMap({ userProfile -> Observable<Bool> in
            if arrayOfEnabledUsers != nil {
                let userID = userProfile.userID ?? ""
                return Observable.just(arrayOfEnabledUsers!.contains(userID))
            } else {
                return Observable.just(false)
            }
        })
        return result
    }
    
    
    class func logSaveFoodFor(date : Date, ocasion : Ocasion, isFavourite : Bool, isGroup : Bool) {
        if isGroup == true {
            self.logEventWith(name: EVENT_SAVE_FOOD_GROUP)
        } else {
            self.logEventWith(name: EVENT_SAVE_FOOD)
        }
        switch ocasion {
        case .breakfast:
            self.logEventWith(name: EVENT_SAVE_FOOD_TO_BREAKFAST)
            if isFavourite == true {
                self.logEventWith(name: EVENT_SAVE_FOOD_FAVORITE_TO_BREAKFAST)
            }
            if Calendar.current.startOfDay(for: date) != Calendar.current.startOfDay(for: Date()) {
                self.logEventWith(name: EVENT_SAVE_FOOD_TO_BREAKFAST_PREVIOUS_DAY)
            }
            break
        case .dinner:
            self.logEventWith(name: EVENT_SAVE_FOOD_TO_DINNER)
            if isFavourite == true {
                self.logEventWith(name: EVENT_SAVE_FOOD_FAVORITE_TO_DINNER)
            }
            if Calendar.current.startOfDay(for: date) != Calendar.current.startOfDay(for: Date()) {
                self.logEventWith(name: EVENT_SAVE_FOOD_TO_DINNER_PREVIOUS_DAY)
            }

            break
        case .lunch:
            self.logEventWith(name: EVENT_SAVE_FOOD_TO_LUNCH)
            if isFavourite == true {
                self.logEventWith(name: EVENT_SAVE_FOOD_FAVORITE_TO_LUNCH)
            }
            if Calendar.current.startOfDay(for: date) != Calendar.current.startOfDay(for: Date()) {
                self.logEventWith(name: EVENT_SAVE_FOOD_TO_LUNCH_PREVIOUS_DAY)
            }
            break
        case .snacks:
            self.logEventWith(name: EVENT_SAVE_FOOD_TO_SNACK)
            if isFavourite == true {
                self.logEventWith(name: EVENT_SAVE_FOOD_FAVORITE_TO_SNACK)
            }
            if Calendar.current.startOfDay(for: date) != Calendar.current.startOfDay(for: Date()) {
                self.logEventWith(name: EVENT_SAVE_FOOD_TO_SNACK_PREVIOUS_DAY)
            }
            break
        }
    }
    
    class func logSaveFoodUsingCameraFor(ocasion : Ocasion) {
        switch ocasion {
        case .breakfast:
            self.logEventWith(name: EVENT_SAVE_FOOD_TO_BREAKFAST_USING_CAMERA)
            break
        case .dinner:
            self.logEventWith(name: EVENT_SAVE_FOOD_TO_DINNER_USING_CAMERA)
            break
        case .lunch:
            self.logEventWith(name: EVENT_SAVE_FOOD_TO_LUNCH_USING_CAMERA)
            break
        case .snacks:
            self.logEventWith(name: EVENT_SAVE_FOOD_TO_SNACK_USING_CAMERA)
            break
        }
    }
    
    class func logOnSearchButtonClick (){
        self.logEventWith(name: EVENT_CLICK_ON_SEARCH_BUTTON)
    }
    
    class func logOpenFoodCardClick (){
        self.logEventWith(name: EVENT_OPEN_FOOD_CARD)
    }
    
    class func logPortionSizeChanged () {
        self.logEventWith(name: EVENT_MODIFIED_PORTION_SIZE)
    }
    
    class func logOnLogFoodClick () {
        self.logEventWith(name: EVENT_CLICK_ON_LOG_FOOD)
    }
    
    class func logBarcodeScan () {
        self.logEventWith(name: EVENT_BARCODE_SCAN)
    }
    
    class func logBarcodeScanSuccessfull () {
        self.logEventWith(name: EVENT_BARCODE_SCAN_SUCCESSFUL)
    }
    
    class func logSaveFoodFromReccomendation (ocasion : Ocasion) {
        switch ocasion {
        case .breakfast:
            self.logEventWith(name: EVENT_SAVE_FOOD_TO_BREAKFAST_FROM_RECOMMENDATION)
            break
        case .dinner:
            self.logEventWith(name: EVENT_SAVE_FOOD_TO_DINNER_FROM_RECOMMENDATION)
            break
        case .lunch:
            self.logEventWith(name: EVENT_SAVE_FOOD_TO_LUNCH_FROM_RECOMMENDATION)
            break
        case .snacks:
            self.logEventWith(name: EVENT_SAVE_FOOD_TO_SNACK_FROM_RECOMMENDATION)
            break
        }
    }
    
    class func logSwipeFromRecommendation (ocasion : Ocasion) {
        switch ocasion {
        case .breakfast:
            self.logEventWith(name: EVENT_SWIPE_FOOD_TO_BREAKFAST_FROM_RECOMMENDATION)
            break
        case .dinner:
            self.logEventWith(name: EVENT_SWIPE_FOOD_TO_DINNER_FROM_RECOMMENDATION)
            break
        case .lunch:
            self.logEventWith(name: EVENT_SWIPE_FOOD_TO_LUNCH_FROM_RECOMMENDATION)
            break
        case .snacks:
            self.logEventWith(name: EVENT_SWIPE_FOOD_TO_SNACK_FROM_RECOMMENDATION)
            break
        }
    }
    
    class func logOnAddFoodClickFor(ocasion : Ocasion) {
        switch ocasion {
        case .breakfast:
            self.logEventWith(name: EVENT_CLICK_ON_ADD_BREAKFAST)
            break
        case .dinner:
            self.logEventWith(name: EVENT_CLICK_ON_ADD_DINNER)
            break
        case .lunch:
            self.logEventWith(name: EVENT_CLICK_ON_ADD_LUNCH)
            break
        case .snacks:
            self.logEventWith(name: EVENT_CLICK_ON_ADD_SNACK)
            break
        }
    }

    class func logVisitRecommendationsScreen () {
        self.logEventWith(name: EVENT_VISITING_RECOMMENDATION_SCREEN)
    }
    
    class func logOnWeightScreenClick() {
        self.logEventWith(name: EVENT_CLICK_ON_WEIGHT_SECTION)
    }
    
    class func logOnWeightScreenClickForPerion (period : period) {
        switch period {
        case .monthly:
            self.logEventWith(name: EVENT_CLICK_ON_MONTHLY_WEIGHT_SCREEN)
            break
        case .weekly:
            self.logEventWith(name: EVENT_CLICK_ON_WEEKLY_WEIGHT_SCREEN)
            break
        default:
            break
        }
    }
    
    class func logOnNutritionScreenClick () {
        self.logEventWith(name: EVENT_CLICK_ON_NUTRITION_SECTION)
    }
    
    class func logOnNutritionScreenClickFor (period : ProgressPeriod) {
        switch period {
        case .monthly:
            self.logEventWith(name: EVENT_CLICK_ON_MONTHLY_NUTRITION_SCREEN)
            break
        case .weekly:
            self.logEventWith(name: EVENT_CLICK_ON_WEEKLY_NUTRITION_SCREEN)
            break
        case .daily:
            self.logEventWith(name: EVENT_CLICK_ON_TODAY_NUTRITION_SCREEN)
            break
        }
    }
    
    class func logOnActivityScreenClick () {
        self.logEventWith(name: EVENT_CLICK_ON_ACTIVITY_SECTION)
    }
    
    class func logOnActivityTScreenClickFor(period : ActivityNavigationPresenter.Period) {
        switch period {
        case .monthly:
            self.logEventWith(name: EVENT_CLICK_ON_MONTHLY_ACTIVITY_SCREEN)
            break
        case .weekly:
            self.logEventWith(name: EVENT_CLICK_ON_WEEKLY_ACTIVITY_SCREEN)
            break
        case .daily:
            self.logEventWith(name: EVENT_CLICK_ON_TODAY_ACTIVITY_SCREEN)
            break
        }
    }
    
    class func logOnReportsClick () {
        self.logEventWith(name: EVENT_CLICK_ON_REPORTS)
    }
    
    class func logOnProfileSettingsClick () {
        self.logEventWith(name: EVENT_CLICK_ON_PROFILE_SETTINGS)
    }
    
    class func logOnManageGoalClick () {
        self.logEventWith(name: EVENT_CLICK_ON_MANAGE_GOAL)
    }
    
    class func logOnCameraClick () {
        self.logEventWith(name: EVENT_CLICK_ON_CAMERA)
    }
    
    class func logVisitingHomeScreen () {
        self.logEventWith(name: EVENT_VISITING_HOME_SCREEN)
    }
    
    class func logOnSendFeedback () {
        self.logEventWith(name: EVENT_SEND_FEEDBACK)
    }
    
    class func logLogsWeight () {
        self.logEventWith(name: EVENT_LOGS_WEIGHT)
    }
    
    class func logLogsWeightWithWithings () {
        self.logEventWith(name: EVENT_LOGS_WEIGHT_USING_WITHINGS)
    }
    
    class func logEstoreOpened() {
        self.logEventWith(name: EVENT_ESTORE_OPENED)
    }
    
    class func logEstoreOpenedMenu() {
        self.logEventWith(name: EVENT_ESTORE_OPENED_MENU)
    }
    
    class func logSaveFoodWithVoiceSearch() {
        self.logEventWith(name: EVENT_LOGGED_FOOD_USING_VOICE_SEARCH)
    }

    class func logVoiceSearch() {
        self.logEventWith(name: EVENT_VOICE_SEARCH)
    }
    
    class func logVoiceSearchReturnResults () {
        self.logEventWith(name: EVENT_VOICE_SEARCH_RETURN_RESULT)
    }
    
    class func logClickedOnChat () {
        self.logEventWith(name: EVENT_CLICKED_ON_CHAT)
    }
    
    class func logClickedOnMealPlanner() {
        self.logEventWith(name: EVENT_CLICKED_ON_MEAL_PLANNER)
    }
    
    class func logClickedOnRecommendations() {
        self.logEventWith(name: EVENT_CLICKED_ON_MEAL_RECOMMENDATION)
    }
    
    class func logClickedOnRecommendationWith(period : ProgressPeriod) {
        self.logEventWith(name: self.recommendationSringfrom(period: period))
    }
    
    class func recommendationSringfrom(period : ProgressPeriod) -> String {
        switch period {
        case .daily:
            return CLICKED_ON_WHAT_TO_EAT_NEXT_FOR_TODAY
        case .weekly:
            return CLICKED_ON_WHAT_TO_EAT_NEXT_FOR_WEEKLY
        case .monthly:
            return CLICKED_ON_WHAT_TO_EAT_NEXT_FOR_MONTHLY
        }
    }
    
    class func logNutritionTipsClicked() {
        self.logEventWith(name: CLICKED_ON_NUTRITIONAL_TIPS)
    }
    
    class func logReceiptClicked() {
        self.logEventWith(name: CLICKED_ON_RECIPE)
    }

    class func logClickedOnHydration() {
        self.logEventWith(name: EVENT_CLICKED_ON_HYDRATION)
    }
    
    class func logClickedOnNutritionalBalance() {
        self.logEventWith(name: EVENT_CLICKED_ON_NUTRITIONAL_BALANCE)
    }
    
    class func logClickedOnVoiceIcon () {
        self.logEventWith(name: EVENT_CLICKED_ON_VOICE_ICON)
    }
    
    class func logClickedOnFoodDiaryIcon() {
        self.logEventWith(name: EVENT_CLICKED_ON_FOOD_DIARY_ICON)
    }
    
    class func logClickedOnGenerateMealPlan() {
        self.logEventWith(name: EVENT_CLICKED_ON_GENERATE_PLAN)
    }
    
    class func logClickedOnNutritionScore() {
        self.logEventWith(name: EVENT_CLICKED_ON_NUTRITION_SCORE)
    }
}

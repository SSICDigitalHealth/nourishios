//
//  CalorificFoodInteractor.swift
//  Nourish
//
//  Created by Vlad Birukov on 20.06.17.
//  Copyright © 2017 SSIC. All rights reserved.
//

import Foundation
import RxSwift

class CalorificFoodInteractor {
    let repository = NestleProgressRepository.shared
    
    func execute(startDate: Date, endDate: Date) -> Observable <CalorificFoodModel> {
        return repository.getProgressFor(startDate: startDate, endDate: endDate, returnHistory: true).flatMap({obserbable -> Observable<CalorificFoodModel> in
            
            return Observable.just(self.transform(model: obserbable.userOcasion))
        })
    }
    
    private func transform(model: [Element]) -> CalorificFoodModel {
        let calorificFoodModel = CalorificFoodModel()
        
        if model.count > 0 {
            calorificFoodModel.arrCaloriesFood = [ElementModel]()
            let groupModel = self.groupElements(array: model)
            let calorificElements = groupModel.sorted(by: ({(s1: Element, s2: Element) -> Bool in
                return s1.consumCalories > s2.consumCalories
            }))
            
            let sumCalories =  calorificElements.reduce(0, {$0 + $1.consumCalories})
            var othersSum = sumCalories
            
            for i in 0..<calorificElements.count {
                if i == 0 || i == 1 || i == 2 {
                    if calorificElements[i].consumCalories / sumCalories > 0.15 {
                        calorificFoodModel.arrCaloriesFood?.append(self.convertElementToElementModel(model: calorificElements[i]))
                        othersSum -= calorificElements[i].consumCalories
                    }
                }
                
            }
            
            if othersSum > 0.5 {
                let elementModel = ElementModel()
                elementModel.nameElement = "Others"
                elementModel.consumedState = othersSum
                elementModel.unit = "kcals"
                    
                calorificFoodModel.arrCaloriesFood?.append(elementModel)
            }
        }
        return calorificFoodModel
    }
    
    private func convertElementToElementModel(model: Element) -> ElementModel {
        let elementModel = ElementModel()
        elementModel.nameElement = model.nameFood
        elementModel.consumedState = model.consumCalories
        elementModel.amountElement = model.amountFood
        elementModel.unit = "kcals"
        
        return elementModel
    }
    
    private func groupElements(array: [Element]) -> [Element] {
        var resultDict = [String: Double]()
        
        for item in array {
            if item.nameFood.isEmpty {
                continue
            } else if let curentValue = resultDict[item.nameFood] {
                resultDict[item.nameFood] = curentValue + item.consumCalories
            } else {
                resultDict[item.nameFood] = item.consumCalories
            }
        }
        var resultArray = resultDict.map{Element(nameFood: $0, consumedCalories: $1)}
        resultArray.sort{$0.consumCalories > $1.consumCalories}
        return resultArray
    }

//    func execute() -> Observable <CalorificFoodModel> {
//        
//        return Observable<CalorificFoodModel>.create{(observer) -> Disposable in
//            let pause = Int(arc4random_uniform(4))
//            let dispat = DispatchTime.now() + .seconds(pause)
//            DispatchQueue.main.asyncAfter(deadline: dispat, execute: {
//                let calorificFoodOfWeek = CalorificFoodModel()
//                let massName = ["Croissant", "Tesco chocolate", "Pepperoni pizza", "Big Mac"]
//                
//                var proxyArr = [ElementModel]()
//
//                for i in 0..<massName.count {
//                    let elementModel = ElementModel()
//                    elementModel.nameElement = massName[i]
//                    elementModel.unit = "cal"
//                    elementModel.consumedState = Double(arc4random_uniform(5000))
//                    elementModel.amountElement = Double(arc4random_uniform(50))
//                    
//                    proxyArr.append(elementModel)
//                }
//                
//                calorificFoodOfWeek.arrCaloriesFood = proxyArr
//                observer.onNext(calorificFoodOfWeek)
//                observer.onCompleted()
//            })
//            
//            return Disposables.create()
//        }
//    }
}

//
//  ScoreChartView.swift
//  Nourish
//
//  Created by Andrey Zmushko on 7/27/17.
//  Copyright © 2017 SSIC. All rights reserved.
//

import UIKit

class ScoreChartView: UIView {

    typealias History = [Int]
    var history = History()
    
    func setupWith(History history: History) {
        self.history = history
        self.setNeedsDisplay()
    }
    
    override func draw(_ rect: CGRect) {
        let minHeight: CGFloat   = 20.0
        let maxHeight: CGFloat   = 68.0
        
        let path = UIBezierPath()
        path.move(to: CGPoint(x: 0, y: (rect.height - minHeight)))
        path.addLine(to: CGPoint(x: rect.width, y: rect.height - maxHeight - minHeight))
        path.addLine(to: CGPoint(x: rect.width, y: rect.height))
        path.addLine(to: CGPoint(x: 0, y: rect.height))
        path.close()
        NRColorUtility.backgroundScore().setFill()
        path.fill()
        
        func startPoint(_ scale: CGFloat) -> CGPoint {
            return CGPoint(x: (rect.width * scale),
                           y: rect.height)
        }

        func endPoint(_ scale: CGFloat) -> CGPoint {
            return CGPoint(x: (rect.width * scale),
                           y: (rect.height - ((rect.width * scale) * maxHeight) / rect.width - minHeight))
        }

        self.drawDashedLine(from: startPoint(0.8), till: endPoint(0.8), gap: 1.8)
        self.drawDashedLine(from: startPoint(0.6), till: endPoint(0.6), gap: 1.9)
        self.drawDashedLine(from: startPoint(0.4), till: endPoint(0.4), gap: 2.1)

        let stepAlpha: CGFloat! = 0.7 / (CGFloat(self.history.count) + 1)
        var alpha: CGFloat! = 1
        for i in self.history {
            let radius: CGFloat = 1.8
            var point = endPoint(CGFloat(i) * 0.01)
            point.y = point.y - radius * 0.8
            self.drawSector(fillColor: NRColorUtility.appBackgroundColor().withAlphaComponent(alpha),
                                        center: point,
                                        radius: radius,
                                        startAngle: 0,
                                        endAngle: 360,
                                        clockwise: true)
            alpha = alpha - stepAlpha
        }
    }

    private func drawDashedLine(from: CGPoint, till: CGPoint, gap: CGFloat) {
        let path = UIBezierPath()
        path.move(to: from)
        path.addLine(to: till)
        path.lineWidth = 1
        path.lineJoinStyle = .miter
        path.setLineDash([2.0, gap], count: 2, phase: 0)
        NRColorUtility.progressScaleBackgroundColor().setStroke()
        path.stroke()
    }

}



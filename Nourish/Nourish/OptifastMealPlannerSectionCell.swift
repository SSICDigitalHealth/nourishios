//
//  OptifastMealPlannerSectionCell.swift
//  Optifast
//
//  Created by Gena Mironchyk on 1/9/18.
//  Copyright © 2018 SSIC. All rights reserved.
//

import UIKit

class OptifastMealPlannerSectionCell: UITableViewCell {
    
    @IBOutlet weak var ocasionImage : UIImageView!
    @IBOutlet weak var ocasionNameLabel : UILabel!
    @IBOutlet weak var caloriesLabel : UILabel!
    @IBOutlet weak var actionButton : UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func setUpWith(model: OptifastMealViewModel, index : Int){
        self.ocasionImage.image = UIImage(named: Ocasion.stringDescription(servingType: model.occasion).lowercased())
        self.ocasionNameLabel.text = String(format: "%@", Ocasion.stringDescription(servingType: model.occasion))
        self.caloriesLabel.text = String(format: "%.0f kcal", model.totalCalories)
    }
    
}

//
//  ResetMealPlannerView.swift
//  Nourish
//
//  Created by Vlad Birukov on 23.08.17.
//  Copyright © 2017 SSIC. All rights reserved.
//

import UIKit

class ResetMealPlannerView: BaseView {

    @IBOutlet var presenter: ResetMealPlannerPresenter!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.basePresenter = self.presenter
        self.presenter.resetMealPlannerView = self
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.stopActivityAnimation()
    }

}

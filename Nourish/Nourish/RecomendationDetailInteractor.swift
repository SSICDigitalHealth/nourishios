//
//  RecomendationDetailInteractor.swift
//  Nourish
//
//  Created by Vlad Birukov on 30.08.17.
//  Copyright © 2017 SSIC. All rights reserved.
//

import Foundation
import RxSwift

class RecomendationDetailInteractor {
    private let recsRepo = NestleRecommendationsRepository()
    private let favRepo = FavoritesRepository()
    
    func execute(startDate: Date, endDate: Date) -> Observable <RecomendationDetailModel> {
        let favoriteMeal = favRepo.fetchFavoritesWithCache()
        let recommendationMeal = self.recsRepo.getRecommendationsFor(startDate: startDate, endDate: endDate)
        
        let combineObject = Observable.combineLatest(favoriteMeal, recommendationMeal, resultSelector: {favMeal, recomMeal -> RecomendationDetailModel in
            let recommendation = RecomendationDetailModel()
            var data = [foodSearchModel]()
            let favoritMeal = favMeal.filter({$0.groupedMealArray.count == 1})
            
            
            if recomMeal.dataRecipies != nil {
                recommendation.dataRecipies = [RecipiesModel]()
                for recipe in recomMeal.dataRecipies! {
                    let recipeModel = recipe
                    var favorite = false
                    
                    if favoritMeal.count > 0 {
                        for i in 0..<favoritMeal.count {
                            if favoritMeal[i].groupedMealArray[0].foodTitle == recipe.nameFood {
                                favorite = true
                                recipeModel.groupId = favoritMeal[i].groupID
                            }
                        }
                    }
                    recipeModel.isFavorite = favorite
                    recommendation.dataRecipies?.append(recipeModel)
                }
            }

            
            for meal in recomMeal.meals {
                var foodSearchModel = meal
                var favorite = false
                
                if favoritMeal.count > 0 {
                    for i in 0..<favoritMeal.count {
                        if favoritMeal[i].groupedMealArray[0].idString == meal.foodId {
                            favorite = true
                            foodSearchModel.groupID = favoritMeal[i].groupID
                        }
                    }
                }
                foodSearchModel.isFavorite = favorite
                data.append(foodSearchModel)
            }
            
            if let caption = recomMeal.caption {
                recommendation.caption = caption
            }
            recommendation.data = data
            
            if recomMeal.tips.count > 0 {
                if let messages = recomMeal.tips[0].messages {
                    recommendation.descriptionText = messages[0]
                }
                
                if let title = recomMeal.tips[0].tipTitle {
                    recommendation.titleText = title
                }
            }
            
            return recommendation
        })
        
        return combineObject
    }
    
    func refreshCacheFavorite() {
        self.favRepo.refreshCacheFavorite()
    }

}

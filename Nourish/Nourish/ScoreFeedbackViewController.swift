//
//  ScoreFeedbackViewController.swift
//  Nourish
//
//  Created by Vino (Vinodha) Sundaramoorthy on 4/18/17.
//  Copyright © 2017 SSIC. All rights reserved.
//

import UIKit

class ScoreFeedbackViewController: BasePresentationViewController {
    @IBOutlet weak var scoreView : ScoreView!
    @IBOutlet weak var textLabel : UILabel!
    @IBOutlet weak var subTitle : UILabel!
    @IBOutlet weak var spotlightTitle : UILabel!
    @IBOutlet weak var closeButton : UIButton!

    var date : Date = Date()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.scoreView.controller = self
        self.baseViews = [scoreView]
        
        self.textLabel.textColor = NRColorUtility.nourishTextColor()
        self.subTitle.textColor = NRColorUtility.hexStringToUIColor(hex: "808080")
        spotlightTitle.textColor = NRColorUtility.nourishTextColor()
        
        let blueColor = NRColorUtility.hexStringToUIColor(hex: "#1172B9")
        closeButton.setTitleColor(blueColor, for: .normal)
    }
    
    @IBAction func close(sender : UIButton) {
       self.closeAction(sender)
    }

}

//
//  BackgroundActivityDumperDataStore.swift
//  NourIQ
//
//  Created by Igor on 12/12/17.
//  Copyright © 2017 SSIC. All rights reserved.
//

import UIKit
import RxSwift

let kBaseURL = "https://api-general.dh.ssic.io/"
let kBaseSubstring = "dhdp/sdh/v1/"
let kGetDateURL = "artik/data/"

typealias chunkFlag = (flag : Bool, type : String, date : Date)
class BackgroundActivityDumperDataStore: BaseNestleDataStore {

    func getLastDateFor(dataType : String, dataSource : String, token : String) -> Observable<Date?> {
        return Observable.create { observer in
            let url = self.getDateUrl(dataType: dataType, dataSource: dataSource)
            var request = URLRequest(url : url)
            request.setValue("artik \(token)", forHTTPHeaderField: kOAUTHAuthorizationHeader)
            request.setValue("application/json", forHTTPHeaderField: "Content-Type")
            request.httpMethod = "GET"
            
            let session = URLSession.shared.dataTask(with: request) {
                data, responce, error in
                if error == nil && data != nil {
                    do {
                        let dict = try JSONSerialization.jsonObject(with: data!, options: []) as? [String : Any]
                        if dict != nil {
                            if let _ = dict!["ts"] as? NSNull {
                                observer.onNext(nil)
                            } else {
                                if let interval = dict!["ts"] as? TimeInterval {
                                    observer.onNext(Date(timeIntervalSince1970: interval))
                                } else {
                                    observer.onNext(nil)
                                }
                            }
                        }
                    } catch let error {
                        observer.onError(error)
                    }
                    observer.onCompleted()
                } else {
                    observer.onError(error!)
                }
            }
            session.resume()
            return Disposables.create {
                session.cancel()
            }
        }
    }
    
    func uploadDiaryData(dict : [String : Any], token : String) -> Observable<Bool> {
        return Observable.create { observer in
            let url = self.getUploadUrl(dataType: kDataType, dataSource: kDiaryType)
            var request = URLRequest(url : url)
            request.setValue("artik \(token)", forHTTPHeaderField: kOAUTHAuthorizationHeader)
            request.setValue("application/json", forHTTPHeaderField: "Content-Type")
            request.httpMethod = "POST"
            
            var data : Data? = nil
            
            do {
                data = try JSONSerialization.data(withJSONObject: dict, options: .prettyPrinted)
            } catch let error as NSError {
                LogUtility.logToFile(error)
            }
            request.httpBody = data
            
            let session = URLSession.shared.dataTask(with: request) {
                data, response, error in
                if error == nil && data != nil {
                    if let httpResponse = response as? HTTPURLResponse {
                        let flag = httpResponse.statusCode == 200
                        observer.onNext(flag)
                    }
                    observer.onCompleted()
                } else {
                    observer.onError(error!)
                }
            }
            session.resume()
            
            return Disposables.create()
        }
    }
    
    func uploadChunkData(chunk : mappedChunk, token : String) -> Observable<chunkFlag> {
        return Observable.create { observer in
            let url = self.getUploadUrl(dataType: chunk.deviceSamples.type.description, dataSource: String(chunk.deviceSamples.device.bundleIdentifier.hashValue))
            var request = URLRequest(url : url)
            request.setValue("artik \(token)", forHTTPHeaderField: kOAUTHAuthorizationHeader)
            request.setValue("application/json", forHTTPHeaderField: "Content-Type")
            request.httpMethod = "POST"
            
            var dict = [String : Any]()
            let unixTime = chunk.date.timeIntervalSince1970
            dict["ts"] = Int(unixTime)
            dict["data"] = chunk.mappedSamples
            
            var data : Data? = nil
            
            do {
                data = try JSONSerialization.data(withJSONObject: dict, options: .prettyPrinted)
            } catch let error as NSError {
                LogUtility.logToFile(error)
            }
            
            request.httpBody = data
            
            let session = URLSession.shared.dataTask(with: request) {
                data, response, error in
                if error == nil && data != nil {
                    if let httpResponse = response as? HTTPURLResponse {
                        let chf = chunkFlag(httpResponse.statusCode == 200, String(describing : chunk.deviceSamples.type), chunk.date)
                        observer.onNext(chf)
                    }
                    observer.onCompleted()
                } else {
                    observer.onError(error!)
                }
            }
            session.resume()
            return Disposables.create {
                session.cancel()
            }
            
        }
    }
    
    private func dateFromUnixStamp(time : Double) -> Date {
        let interval = time / 1000
        let date = Date(timeIntervalSince1970 : interval)
        return date
    }
    
    private func getDateUrl(dataType : String, dataSource : String) -> URL {
        let urlString = kBaseURL + kGetDateURL + dataType + "/" + dataSource + "/ts"
        return URL(string: urlString)!
    }
    
    private func getUploadUrl(dataType : String, dataSource : String) -> URL {
        let urlString = kBaseURL + kGetDateURL + dataType + "/" + dataSource
        return URL(string : urlString)!
    }
    
    func getStoreDictUrl(userID : String, type : DHDataType, scheme : DHDataSchema, dataSet : String, token : String) -> Observable<URL> {
        return Observable.create { observer in
            let url = self.getStoreFoodURL(userID: userID, type: type, scheme: scheme, dataSet: dataSet)
            var request = URLRequest(url : url)
            request.setValue("artik \(token)", forHTTPHeaderField: kOAUTHAuthorizationHeader)
            //request.setValue("application/json", forHTTPHeaderField: "Content-Type")
            request.httpMethod = "GET"
            
            let session = URLSession.shared.dataTask(with: request) {
                data, response, error in
                if error == nil && data != nil {
                    if let httpResponse = response as? HTTPURLResponse {
                        do {
                            let dict = try JSONSerialization.jsonObject(with: data!, options: []) as? [String : Any]
                            if dict != nil {
                                if let url = dict!["url"] as? String {
                                    observer.onNext(URL(string : url)!)
                                }
                            }
                        } catch let error {
                            observer.onError(error)
                        }
                        observer.onCompleted()
                    } else {
                        observer.onError(error!)
                    }
                }
            }
            
            session.resume()
            return Disposables.create()
        }
        
    }
    
    func storeDict(dict : [String : Any],userID : String, type : DHDataType, scheme : DHDataSchema, dataSet : String, token : String) -> Observable<Bool> {
        return self.getStoreDictUrl(userID: userID, type: type, scheme: scheme, dataSet: dataSet, token: token).flatMap {url -> Observable<Bool> in
            return self.uploadDataToDH(dict: dict, token: token, url: url)
        }
    }
    
    func storeGroupModel(dict : [String : Any],userID : String, type : DHDataType, scheme : DHDataSchema, dataSet : String, token : String) -> Observable<Bool> {
        return self.getStoreDictUrl(userID: userID, type: type, scheme: scheme, dataSet: dataSet, token: token).flatMap {url -> Observable<Bool> in
            return self.uploadDataToDH(dict: dict, token: token, url: url)
        }
    }
    
    private func uploadDataToDH(dict : [String : Any], token : String, url : URL) -> Observable<Bool> {
        return Observable.create { observer in
            var request = URLRequest(url : url)
            request.setValue("artik \(token)", forHTTPHeaderField: kOAUTHAuthorizationHeader)
            request.setValue("application/json", forHTTPHeaderField: "Content-Type")
            request.httpMethod = "POST"
            
            var data : Data? = nil
            
            do {
                data = try JSONSerialization.data(withJSONObject: dict, options: .prettyPrinted)
            } catch let error as NSError {
                LogUtility.logToFile(error)
            }
            
            request.httpBody = data
            
            let session = URLSession.shared.dataTask(with: request) {
                data, response, error in
                if error == nil && data != nil {
                    observer.onNext(true)
                } else {
                    observer.onNext(false)
                }
            }
            session.resume()
            return Disposables.create()
        }
    }
    
    private func getStoreFoodURL(userID : String, type : DHDataType, scheme : DHDataSchema, dataSet : String) -> URL {
        var urlString = kBaseURL + kBaseSubstring + userID + "/" + scheme.rawValue + "/"
        urlString = urlString + type.rawValue + "/" + kDHSourceString + "/data/url?type=stream&dataSetId=\(dataSet)"
        return URL(string : urlString)!
    }
    
}

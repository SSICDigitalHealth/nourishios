//
//  FitBitLoginViewController.swift
//  Nourish
//
//  Created by Vino (Vinodha) Sundaramoorthy on 5/16/17.
//  Copyright © 2017 SSIC. All rights reserved.
//

import UIKit

class FitBitLoginViewController: UIViewController , AuthenticationProtocol {

    var authenticationController: FitBitAuthenticationController?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        authenticationController = FitBitAuthenticationController(delegate: self)
        //authenticationController?.login(fromParentViewController: self)
    }
    
    // MARK: Actions
    func login() {
        authenticationController?.login(fromParentViewController: self)
    }
    
    func logout(_ sender: AnyObject) {
        //FitBitAuthenticationController.logout()
    }

    // MARK: AuthenticationProtocol
    func authorizationDidFinish(_ success: Bool) {
        FitBitConnectionManager.sharedInstance.authorize(with: "")
        //NavigationUtility.changeRootViewController()
        self.dismiss(animated: true, completion: nil)
    }
}

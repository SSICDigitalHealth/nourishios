//
//  OptifastMealPlannerChangeDishStoreCacheInteractor.swift
//  Optifast
//
//  Created by Gena Mironchyk on 1/11/18.
//  Copyright © 2018 SSIC. All rights reserved.
//

import UIKit
import RxSwift
class OptifastMealPlannerChangeDishStoreCacheInteractor: NSObject {
    
    private let mealPlannerCacheDataStore = MealPlannerCacheDataStore()
    private let mapper = OptifastMealPlannerModel_to_MealPlannerCache()
    
    func execute(model : OptifastMealPlannerModel) -> Observable<Bool> {
        return Observable.create { observer in
            self.mealPlannerCacheDataStore.store(cache: self.mapper.transform(model: model))
            observer.onNext(true)
            observer.onCompleted()
            return Disposables.create()
        }
    }
    
}

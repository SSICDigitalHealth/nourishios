//
//  CaloricProgressViewModel.swift
//  Nourish
//
//  Created by Vlad Birukov on 29.08.17.
//  Copyright © 2017 SSIC. All rights reserved.
//

import Foundation

class CaloricProgressViewModel {
    var userGoal: UserGoal?
    var weeklyPlan: Double?
    var activityLevel: ActivityLevel?
    var caloricState : [caloricProgress]?
    var isMetric: Bool = false
}

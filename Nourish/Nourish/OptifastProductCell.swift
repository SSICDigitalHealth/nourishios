//
//  OptifastProductCell.swift
//  Nourish
//
//  Created by Gena Mironchyk on 10/19/17.
//  Copyright © 2017 SSIC. All rights reserved.
//

import UIKit

class OptifastProductCell: UITableViewCell {
    
    @IBOutlet weak var checkmarkButton : UIButton!
    @IBOutlet weak var optifastProductTitle : UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func setupWith(optifastTitle : String, isSelected : Bool) {
        self.optifastProductTitle.text = optifastTitle
        self.checkmarkButton.isSelected = isSelected
    }
    
}

//
//  WeightStatsView.swift
//  Nourish
//
//  Created by Vino (Vinodha) Sundaramoorthy on 1/9/17.
//  Copyright © 2017 SSIC. All rights reserved.
//

import UIKit

class WeightStatsView: BaseView , WeightStatsProtocol,IAxisValueFormatter{
    @IBOutlet weak var weightsTableView : UITableView!
    @IBOutlet weak var periodSegmentControl : NRSegmentedControl!
    @IBOutlet weak var chartView : CombinedChartView!
    @IBOutlet weak var scrollView : UIScrollView!
    @IBOutlet weak var avgWeightLabel : UILabel!
    @IBOutlet weak var avgCalorieLabel : UILabel!

    var period : period!
    var currentWeight : Double = 0
    var maxWeight : Double = 0
    var weightModel : weightModel!
    var controller = WeightStatsViewController()
    var presenter = WeightStatsPresenter()
    var isMetric : Bool = false
    
    override func viewWillAppear(_ animated: Bool) {
        self.backgroundColor = NRColorUtility.nourishHomeColor()
        self.basePresenter = presenter
        presenter.weightsView = self
        period = presenter.weightPeriod
        self.setUpScrollView()
        self.setUpSegmentControl()
        self.setupTableView()
        super.viewWillAppear(animated)
    }
    
    func setupTableView() {
        weightsTableView?.register(UINib(nibName: "WeightsStatsCellTableViewCell", bundle: nil), forCellReuseIdentifier: "kWeightStatsCell")
        weightsTableView.delegate = presenter
        weightsTableView.dataSource = presenter
        weightsTableView.reloadData()
    }
    
    func setUpSegmentControl(){
        let segmentColor = NRColorUtility.nourishNavigationColor()
        let textFont = UIFont.systemFont(ofSize: 15)
        let textColor = UIColor.white
            
        periodSegmentControl = NRSegmentedControl(frame: CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: 45), titles: ["WEEKLY","MONTHLY"],selectedtitles:[], action: {
                control, index in
            })
        periodSegmentControl.appearance = segmentAppearance(backgroundColor: segmentColor, selectedBackgroundColor:segmentColor, textColor: textColor, font: textFont, selectedTextColor: textColor, selectedFont: textFont, bottomLineColor: segmentColor, selectorColor: UIColor.white, bottomLineHeight: 5, selectorHeight: 5, labelTopPadding: 0, hasImage: false, imageSize: nil)
        periodSegmentControl.delegate = presenter
            self.addSubview(periodSegmentControl)
    }
    
    func startActivityIndicator() {
        self.prepareLoadView()
    }
    
    func setUpScrollView() {
        scrollView.layer.cornerRadius = 2
        scrollView.contentSize = CGSize(width: scrollView.frame.width, height: 2000)
    }
    
    // Chart Helper methods
    func setUpChartViewForPeriod(period : period) {
        self.setChartViewSettings()
    }
    
    func setChartViewSettings() {
        //Chart View
        chartView.drawBarShadowEnabled = false
        chartView.drawValueAboveBarEnabled = false
        chartView.legendRenderer.legend = nil
        chartView.drawGridBackgroundEnabled = false
        chartView.drawRoundedBarEnabled = true
      //  chartView.drawOrder = [DrawOrder.bar.rawValue,DrawOrder.line.rawValue]
        chartView.isUserInteractionEnabled = false
        
        let combinedChartData = CombinedChartData()
        combinedChartData.lineData = self.setLineChartData()
        combinedChartData.barData = self.setBarChatData()
        
        //X - Axis
        let xAxis = chartView.xAxis
        xAxis.labelPosition = .bottom
        xAxis.granularity = 1
        xAxis.axisMinimum = 0.5
        xAxis.drawGridLinesEnabled = false
        xAxis.valueFormatter = self
        xAxis.axisLineDashPhase = 0
        xAxis.axisLineDashLengths = [2,2]
        
        //Right Axis
        let rightAxis = chartView.rightAxis
        rightAxis.drawGridLinesEnabled = false
        rightAxis.axisMinimum = 0.0
        rightAxis.axisMaximum = combinedChartData.getYMax(axis: .right)
        rightAxis.granularity = combinedChartData.getYMax(axis: .right) / 2
        rightAxis.axisLineColor = UIColor.clear
        rightAxis.labelTextColor = NRColorUtility.weightLeftAxisColor()
        rightAxis.granularityEnabled = true
        
        //Left Axis
        let leftAxis = chartView.leftAxis
        leftAxis.axisLineColor = UIColor.clear
        leftAxis.gridLineDashPhase = 0
        leftAxis.gridLineDashLengths = [2,2]
        leftAxis.labelTextColor = NRColorUtility.weightRightAxisColor()
        leftAxis.granularityEnabled = true
        leftAxis.granularity = 5
        
        chartView.data = combinedChartData
        chartView.animate(yAxisDuration: 1)
    }
    
    func setBarChatData() -> BarChartData {
        var barChartDataEntry : [BarChartDataEntry] = []
        
        for i in 0...weightModel.caloriesArray.count-1 {
            let data = weightModel.caloriesArray[i]
                barChartDataEntry.append(BarChartDataEntry.init(x: Double(i+1), y: data.calorie))
        }
        
        let set = BarChartDataSet.init(values: barChartDataEntry, label: "")
        
        //Bar Data Settings
        set.axisDependency = .right
        set.drawValuesEnabled = false
        set.barRoundingCorners = [.topLeft , .topRight]
        set.setColor(NRColorUtility.healthGreenColor())
        
        let data = BarChartData.init(dataSets: [set])
        data.barWidth = 0.45
        return data
    }
    
    func setLineChartData() -> LineChartData {
        var lineChartDataEntry : [ChartDataEntry] = []
        var projectLineDataEntry : [ChartDataEntry] = []
       
        maxWeight = weightModel.weightsArray[0].weight
        currentWeight = weightModel.weightsArray[0].weight
        for i in 0...weightModel.weightsArray.count-1 {
            let data = weightModel.weightsArray[i]
            let weight = self.isMetric ? data.weight : NRConversionUtility.kiloToPounds(valueInKilo: data.weight)
            if weight > 0 {
                lineChartDataEntry.append(ChartDataEntry.init(x: Double(i+1), y: weight))
            }
            
            if weight > Double(maxWeight) {
                maxWeight = weight
            }
            
            if weight < Double(currentWeight) {
                currentWeight = weight
            }
        }
        
        if self.weightModel.weeklyPlanArray.count > 0 {
            for i in 0...self.weightModel.weeklyPlanArray.count-1  {
                let weight = self.isMetric ? weightModel.weeklyPlanArray[i] : NRConversionUtility.kiloToPounds(valueInKilo: weightModel.weeklyPlanArray[i])
                if weight > 0 {
                    projectLineDataEntry.append(ChartDataEntry.init(x: Double(i+1), y: weight))
                }
            }
        }
        chartView.leftAxis.axisMinimum = currentWeight - 2
        chartView.leftAxis.axisMaximum = maxWeight + 2
        chartView.xAxis.axisMaximum = Double(weightModel.weightsArray.count) + 0.5
        
        let set1 = LineChartDataSet(values: lineChartDataEntry, label: "")
    
        //user weight line settings
        set1.setColor(NRColorUtility.weightRightAxisColor())
        set1.lineWidth = 2.5;
        set1.setCircleColor(NRColorUtility.weightRightAxisColor())
        set1.circleRadius = 7;
        set1.circleHoleRadius = 0;
        set1.fillColor = NRColorUtility.weightRightAxisColor()
        set1.mode = .linear
        set1.valueFont = UIFont.systemFont(ofSize: 10)
        set1.valueTextColor = NRColorUtility.weightRightAxisColor()
        set1.axisDependency = .left
        set1.drawValuesEnabled = false
        
        let set2 = LineChartDataSet(values: projectLineDataEntry, label: "")
        // Projected Line Settings
        set2.axisDependency = .left
        set2.drawValuesEnabled = false
        set2.setColor(UIColor.lightGray)
        set2.lineWidth = 2
        set2.mode = .linear
        set2.drawCirclesEnabled = false

        let lineData = LineChartData.init(dataSets: [set1,set2])
        return lineData
    }
    
    // MARK: WeightStatsProtocol Implementation
    func fetchActivityStats(period: period) {
        DispatchQueue.main.async {
            self.setChartViewSettings()
            self.weightsTableView.reloadData()
        }
    }
    
    func fetchWeight() {
        DispatchQueue.main.async {
            self.weightsTableView.reloadData()
        }
    }
    
    func setWeightModel(model: weightModel) {
        weightModel = model
        isMetric = (self.presenter.userModel.metricPreference?.isMetric)!
        self.setChartViewSettings()
        let weight = isMetric ? weightModel.averageWeight : NRConversionUtility.kiloToPounds(valueInKilo: weightModel.averageWeight)
        let wtAttribute = [NSForegroundColorAttributeName: NRColorUtility.progressLabelColor() , NSFontAttributeName : UIFont.systemFont(ofSize: 20, weight: UIFontWeightMedium)]
        let wtAttrString = NSMutableAttributedString(string: String(format:"%2.f",weight), attributes: wtAttribute)
        var labelString = String(format:"%@", isMetric ? " Avg. kg" : " Avg. lbs")
        let labelAttributes = [ NSForegroundColorAttributeName: NRColorUtility.progressLabelColor(), NSFontAttributeName : UIFont.systemFont(ofSize: 14)]
        wtAttrString.append(NSAttributedString(string: labelString, attributes: labelAttributes))
        
        let calAttribute = [NSForegroundColorAttributeName: NRColorUtility.progressLabelColor() , NSFontAttributeName : UIFont.systemFont(ofSize: 20, weight: UIFontWeightMedium)]
        let calAttrString = NSMutableAttributedString(string: String(format:"%2.f",weightModel.averageCalorie), attributes: calAttribute)
        labelString = " Avg. Cal"
        calAttrString.append(NSAttributedString(string: labelString, attributes: labelAttributes))
        self.stopActivityAnimation()

        self.avgWeightLabel.attributedText  = wtAttrString
        self.avgCalorieLabel.attributedText = calAttrString
        self.weightsTableView.reloadData()
    }
    
    // MARK: IAxisValueFormatter Implementation
    func stringForValue(_ value: Double, axis: AxisBase?) -> String {
        let index = Int(value) - 1
        var rangeString = weightModel.weightsArray[index].weightPeriodRange
        let periodRange = rangeString.components(separatedBy: "-")
        
        if periodRange.count > 1 {
            rangeString = String(format:"%@-\n%@",periodRange[0],periodRange[1])
        }
        return rangeString
    }

}

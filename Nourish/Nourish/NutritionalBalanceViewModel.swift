//
//  NutritionalBalanceViewModel.swift
//  Nourish
//
//  Created by Vlad Birukov on 29.08.17.
//  Copyright © 2017 SSIC. All rights reserved.
//

import Foundation

typealias nutritionalViewList = (type: NutritionType, nutrition: UserNutritionViewModel)
class NutritionalBalanceViewModel: NSObject {
    var data: [nutritionalViewList]?
}

class UserNutritionViewModel {
    var consumedNutritional: Double = 0.0
    var targetNutritional: Double = 0.0
    var unit: String = ""
    
    init() {
    }
    
    init(consumed: Double, target: Double, unit: String) {
        self.consumedNutritional = consumed
        self.targetNutritional = target
        self.unit = unit
    }
    
}

//
//  NRLoginSplashViewController.swift
//  Nourish
//
//  Created by Vino (Vinodha) Sundaramoorthy on 10/31/16.
//  Copyright © 2016 SSIC. All rights reserved.
//

import UIKit
import RxSwift

let acceptedLicenseAgreementIDArrayKey = "Accepted users"
class NRLoginSplashViewController: BasePresentationViewController {
    
    @IBOutlet weak var loginButton : UIButton!
    @IBOutlet weak var logoImage : UIImageView!
    @IBOutlet weak var loginButtonHeightConstraint : NSLayoutConstraint!
    @IBOutlet weak var bottomImageView : UIImageView!
    var awakedFromBackground = false 
    
    var awakedWithNotification = false
    var dateForFoodDiary : Date? = nil
    
    private let withingsInteratcor = WithingsInteractor()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupView()
       // UIApplication.shared.statusBarStyle = .default

        // Do any additional setup after loading the view.
    }
    
    
    private func setupView () {
        self.view.backgroundColor = NRColorUtility.backgroundLoginColor()

        #if OPTIFASTVERSION
            self.loginButton?.layer.borderColor = NRColorUtility.hexStringToUIColor(hex: "3DB5E6").cgColor
        #else
            self.view.backgroundColor = UIColor.white
            self.bottomImageView.removeFromSuperview()
            self.loginButton?.layer.borderColor =  NRColorUtility.backgroundLoginColor().cgColor
            self.loginButton?.setTitleColor(NRColorUtility.backgroundLoginColor(), for: .normal)
            self.loginButtonHeightConstraint.constant = 45.0
            self.logoImage.image = #imageLiteral(resourceName: "Logo")
            NSLayoutConstraint(item: self.view, attribute: .bottom, relatedBy: .equal, toItem: self.loginButton, attribute: .bottom, multiplier: 1, constant: 100).isActive = true
            self.view.layoutIfNeeded()
        #endif
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        #if !OPTIFASTVERSION
            self.setupStatusBar()
        #endif
       
        if NRUserSession.sharedInstance.accessToken != nil {
            self.loginButton?.isHidden = true
            let userInteractor = UserProfileInteractor()
            if defaults.bool(forKey: onBoardingKey) != true {
                let _ = NRUserSession.sharedInstance.isEmailLocked().subscribe(onNext: { [unowned self] locked in
                    if locked == false {
                        let bckUser = userInteractor.getCurrentUserWith(fetchingPolicy: .ForceReload)
                        let cche = userInteractor.getCurrentUserWith(fetchingPolicy: .Cached)
                        let _ = Observable.zip(bckUser, cche, resultSelector: {frUser,cUser -> UserProfile in
                        
                            var array = defaults.array(forKey: acceptedLicenseAgreementIDArrayKey) as? [String]
                        
                            let same = frUser.userID == cUser.userID
                            let noNillUser = cUser.userID != nil && cUser.userID != ""
                        
                        
                            if same == true && noNillUser == true {
                                if array == nil {
                                    array = []
                                }
                                array?.append(cUser.userID!)
                                defaults.setValue(array, forKey: acceptedLicenseAgreementIDArrayKey)
                            }
                            return frUser
                        
                        }).subscribe(onNext: { [unowned self] userProfile in
                            defaults.set(Date(), forKey: kUserCacheRefreshDateKey)
                            if defaults.bool(forKey: onBoardingKey) == true {
                                let _ = self.withingsInteratcor.syncWithingsMessages().subscribe(onNext: {synced in }, onError: {error in
                                    DispatchQueue.main.async {
                                        self.parseError(error: error,completion : {completed in
                                            if error.code == 429 {
                                                self.loginButton.isHidden = false
                                            }
                                     })
                                    }
                                }, onCompleted: {
                                    self.processLoginFlow(userProfile : userProfile)
                                }, onDisposed: {})
                            } else {
                                self.processLoginFlow(userProfile : userProfile)
                            }
                        }, onError: {error in
                            DispatchQueue.main.async {
                                self.parseError(error: error,completion : {completed in
                                    if error.code == 429 {
                                        self.loginButton.isHidden = false
                                    }
                                })
                            }
                        }, onCompleted: {}, onDisposed: {})
                    } else {
                        DispatchQueue.main.async {
                            NRUserSession.sharedInstance.logout(keepCurrent: false)
                            self.showBlockedEmailAlert()
                        }
                    }
                
                }, onError: {error in }, onCompleted: {}, onDisposed: {})
            } else {
                let _ = userInteractor.getCurrentUserWith(fetchingPolicy: .Cached).subscribe(onNext: { [unowned self] cachedUser in
                    self.processLoginFlow(userProfile: cachedUser)
                }, onError: {error in }, onCompleted: {}, onDisposed: {})
            }
        }
    }
    
    private func showBlockedEmailAlert () {
        let alertController = UIAlertController(title: "Error", message: "This email is locked - please contact support", preferredStyle: .alert)
        alertController.addAction(UIAlertAction(title: "Dismiss", style: UIAlertActionStyle.default, handler: nil))
        self.present(alertController, animated: true, completion: {
            self.loginButton.isHidden = false
        })
    }
    
    private func processLoginFlow (userProfile : UserProfile) {
        DispatchQueue.main.async {
            self.awakedFromBackground = false
            let array = defaults.array(forKey: acceptedLicenseAgreementIDArrayKey) as? [String]
            if userProfile.userID != nil && array != nil && array!.contains(where: {$0 == userProfile.userID!}) {
                self.processPermissionScreenWith(profile : userProfile)
            } else {
                self.processTermsAndConditionsWith(profile: userProfile)
            }
        }
    }
    
    
    private func setupStatusBar() {
        let statusBar: UIView = UIApplication.shared.value(forKey: "statusBar") as! UIView
        if statusBar.responds(to:#selector(setter: UIView.backgroundColor)) {
            statusBar.backgroundColor = UIColor.white
            UIApplication.shared.statusBarStyle = .default
        }
        
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        let statusBar: UIView = UIApplication.shared.value(forKey: "statusBar") as! UIView
        if statusBar.responds(to:#selector(setter: UIView.backgroundColor)) {
            statusBar.backgroundColor = NRColorUtility.appBackgroundColor()
            UIApplication.shared.statusBarStyle = .lightContent

        }
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
//    private func setupStatusBar() {
//        let statusBar: UIView = UIApplication.shared.value(forKey: "statusBar") as! UIView
//        if statusBar.responds(to:#selector(setter: UIView.backgroundColor)) {
//            statusBar.backgroundColor = NRColorUtility.hexStringToUIColor(hex: "e6f6fc")
//        }
//    }
    
    @IBAction func showLoginWebView () {
        self.showWebView()
    }

    
    private func showWebView () {
        let accountsWebView = NRLoginWebViewController(nibName: "NRLoginWebViewController", bundle: nil)
        self.present(accountsWebView, animated: false)
    }
    
    private func showUnsupportedAlert () {
        let alertController = UIAlertController(title: "Alert", message: "Please update your iOS to version 10.3 or later", preferredStyle: .alert)
        alertController.addAction(UIAlertAction(title: "Dismiss", style: UIAlertActionStyle.default, handler: nil))
        self.present(alertController, animated: true, completion: nil)
    }
    
    
    private func supportedIosVersion () -> Bool {
        var supported = true
        let version = UIDevice.current.iOS
        if version != nil && version! < 10.3 {
            supported = false
        }
        return supported
    }
        
    

    private func processPermissionScreenWith(profile : UserProfile) {
        DispatchQueue.main.async {
            if PermissionsUtility.shared.permissionsWasAsked() == false {
                let permissionsViewController = PermissionRequestViewController(nibName: "PermissionRequestViewController", bundle: nil)
                permissionsViewController.userProfile = profile
                self.present(permissionsViewController, animated: true, completion: nil)
            } else {
                NavigationUtility.changeRootViewController(userProfile:profile, awakedFromBackground: self.awakedFromBackground)
                if self.awakedWithNotification == true && self.dateForFoodDiary != nil {
                    NavigationUtility.navigateToFoodDiaryFor(date:self.dateForFoodDiary!)
                    self.awakedWithNotification = false
                }
            }
        }
    }

    private func processTermsAndConditionsWith(profile : UserProfile) {
        DispatchQueue.main.async {
            let termsVC =  NRTermsConditionViewController()
            termsVC.userProfile = profile
            let navigationController = BaseNavigationController.init(rootViewController: termsVC)
            self.present(navigationController, animated: true, completion: nil)
        }
    }
}


extension UIDevice {
    var iOS: Double? {
        if UIDevice.current.systemName == "iOS" {
            if let iosVersion = UIDevice.current.systemVersion.toDouble {
                return iosVersion
            }
        }
        return nil
    }
}

extension String {
    
    var toDouble: Double? {
        
        if let iosVersion = Double(self) {
            return iosVersion
        }
        
        var arr = components(separatedBy: ".")
        if arr.count >= 1 {
            var integerPart = 0
            var floatPart = 0
            
            if let _integerPart = Int(arr[0]), !arr[0].isEmpty{
                integerPart = _integerPart
            }
            
            if let _floatPart = Int(arr[1]), !arr[1].isEmpty{
                floatPart = _floatPart
            }
            
            return Double("\(integerPart).\(floatPart)")
        }
        return nil
    }
}

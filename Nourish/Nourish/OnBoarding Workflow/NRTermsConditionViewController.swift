//
//  NRTermsConditionViewController.swift
//  Nourish
//
//  Created by Vino (Vinodha) Sundaramoorthy on 11/2/16.
//  Copyright © 2016 SSIC. All rights reserved.
//

import UIKit

class NRTermsConditionViewController: BasePresentationViewController {
    
    @IBOutlet weak var termsTextView : UITextView?
    @IBOutlet weak var submitButton : UIButton?
    @IBOutlet weak var userLicenseButton : UIButton?
    @IBOutlet weak var policyButton : UIButton?
    @IBOutlet weak var nourishAgreementButton : UIButton?
    @IBOutlet weak var userAgreementLabel : UILabel?
    @IBOutlet weak var userAgreementButtonView : UIView?
    @IBOutlet weak var agreementViewConstraint : NSLayoutConstraint!
    
    var userProfile : UserProfile?
    
    var licenseAgreement : Bool = false
    var policyAgreement : Bool = false
    var nourishAgreement : Bool = false
    var showAgreementButton : Bool = true

    override func viewDidLoad() {
        super.viewDidLoad()
        self.setUpNavigationBarButtons()
        self.submitButton?.backgroundColor = NRColorUtility.appBackgroundColor()
        self.userLicenseButton?.addTarget(self, action:#selector(NRTermsConditionViewController.stateChanged(mySwitch:)), for: UIControlEvents.touchUpInside)
        self.policyButton?.addTarget(self, action:#selector(NRTermsConditionViewController.stateChanged(mySwitch:)), for: UIControlEvents.touchUpInside)
        self.nourishAgreementButton?.addTarget(self, action:#selector(NRTermsConditionViewController.stateChanged(mySwitch:)), for: UIControlEvents.touchUpInside)

        self.submitButton?.isEnabled = false
        self.termsTextView?.addShadow()
        var pathTh = "terms_and_conditions"
        self.userAgreementLabel?.text = "I have read \"Notice to Samsung Employees Regarding the Nourish Beta Program\""
#if NESTLEVERSION 
        pathTh = "terms_and_conditions_nestle"
    self.userAgreementLabel?.text = "I have read \"Notice to Samsung Employees Regarding the Nourish Beta Program\""

#endif
        if let path = Bundle.main.path(forResource: pathTh, ofType: "txt"){
            do {
             let content = try String(contentsOfFile: path, encoding: .utf8)
                self.termsTextView?.text = content
            }
            catch {
            }
        }
        
        if !self.showAgreementButton {
            self.agreementViewConstraint.constant = 0
        }
        
        self.userAgreementButtonView?.isHidden = !self.showAgreementButton
        self.submitButton?.isHidden = !self.showAgreementButton
    }
    
    override func viewDidLayoutSubviews() {
        self.termsTextView?.setContentOffset(CGPoint.zero , animated: false)
    }
    
    func setUpNavigationBarButtons () {
        let titleAttribute = self.navigationBarTitleAttribute()
        navigationItem.title = "Terms and Condition"
        self.title = "Terms and Condition"
        self.navigationController?.navigationBar.barTintColor = UIColor.white
        self.navigationController?.navigationBar.titleTextAttributes = titleAttribute
        
        if !self.showAgreementButton {
            let closeButton : UIBarButtonItem =  UIBarButtonItem(image: UIImage(named:"close_icon"), style: .plain, target: self, action: #selector(self.closeAction(_:)))
            self.navigationController?.navigationBar.tintColor = NRColorUtility.hexStringToUIColor(hex: "#808080")
            self.navigationController?.navigationItem.leftBarButtonItem = closeButton
            navigationItem.leftBarButtonItem = closeButton
        }
    }

    @IBAction func stateChanged(mySwitch : UIButton) {
        if mySwitch == self.policyButton {
            self.policyAgreement = !self.policyAgreement
            let imageName : String = self.policyAgreement ? "circle_selected" : "circle_select"
                self.policyButton?.setImage(UIImage(named:imageName), for: .normal)
        } else if mySwitch == self.nourishAgreementButton {
            self.nourishAgreement = !self.nourishAgreement
            let imageName : String = self.nourishAgreement ? "circle_selected" : "circle_select"
            self.nourishAgreementButton?.setImage(UIImage(named:imageName), for: .normal)

        } else if mySwitch == self.userLicenseButton {
            self.licenseAgreement = !self.licenseAgreement
            let imageName : String = self.licenseAgreement ? "circle_selected" : "circle_select"
            self.userLicenseButton?.setImage(UIImage(named:imageName), for: .normal)
        }
        self.enableSubmitButton()
    }
    
    
    func enableSubmitButton (){
        if (self.licenseAgreement && self.policyAgreement && self.nourishAgreement) {
            self.submitButton?.isEnabled = true
        } else {
            self.submitButton?.isEnabled = false
        }
    }
    
    @IBAction func submitButtonClicked (sender:UIButton) {
        var arrayOfAgreements = defaults.array(forKey: acceptedLicenseAgreementIDArrayKey)
        if arrayOfAgreements == nil {
            arrayOfAgreements = [String]()
        }
        if self.userProfile?.userID != nil {
            arrayOfAgreements!.append(self.userProfile!.userID!)
        }
        defaults.setValue(arrayOfAgreements, forKey: acceptedLicenseAgreementIDArrayKey)
        if PermissionsUtility.shared.permissionsWasAsked() == false {
            let permissionsViewController = PermissionRequestViewController(nibName: "PermissionRequestViewController", bundle: nil)
            permissionsViewController.userProfile = self.userProfile
            self.present(permissionsViewController, animated: true, completion: nil)
        } else {
            NavigationUtility.changeRootViewController(userProfile: self.userProfile,awakedFromBackground: false)
        }
    }
    
}

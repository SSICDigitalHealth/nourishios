//
//  NRLoginWebViewController.swift
//  Nourish
//
//  Created by Vino (Vinodha) Sundaramoorthy on 10/31/16.
//  Copyright © 2016 SSIC. All rights reserved.
//

import UIKit
import WebKit
import RxSwift
import SafariServices

let kTimeOffsetSeconds = 30.0
let kCloseSafariViewControllerNotification = "kCloseSafariViewControllerNotification"

public extension DispatchQueue {
    
    private static var _onceTracker = [String]()
    
    /**
     Executes a block of code, associated with a unique token, only once.  The code is thread safe and will
     only execute the code once even in the presence of multithreaded calls.
     
     - parameter token: A unique reverse DNS style name such as com.vectorform.<name> or a GUID
     - parameter block: Block to execute once
     */
    public class func once(token: String, block:()->Void) {
        objc_sync_enter(self); defer { objc_sync_exit(self) }
        
        if _onceTracker.contains(token) {
            return
        }
        
        _onceTracker.append(token)
        block()
    }
}


class NRLoginWebViewController: BasePresentationViewController,UIWebViewDelegate, SFSafariViewControllerDelegate {

    @IBOutlet weak var loginWebView : UIWebView?
    var safariVC : SFSafariViewController!
    var isFromCtrl = false
    
    private let onceToken = Bundle.main.bundleIdentifier
    override func viewDidLoad() {
        super.viewDidLoad()
        NotificationCenter.default.addObserver(self, selector: #selector(safariLogin(notification:)), name: NSNotification.Name(rawValue: kCloseSafariViewControllerNotification), object: nil)
        //NotificationCenter.default.addObserver(self, selector: Selector("safariLogin:"), name: NSNotification.Name(rawValue: kCloseSafariViewControllerNotification), object: nil)
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if isFromCtrl == false {
            self.safariLoadRequest()
        }
        
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.deleteCookies()
    }
    
    func safariViewControllerDidFinish(_ controller: SFSafariViewController) {
        self.isFromCtrl = true
        self.dismiss(animated: true, completion: nil)
    }
    
    func safariLogin(notification: NSNotification) {
        
        if let url = notification.object as? NSURL {
            var acessTokenUrl = String(describing : url)
            if (acessTokenUrl .contains("#access_token")) {
                let replace = kSAMIAuthBaseUrl + kSAMIAuthPath
                acessTokenUrl = acessTokenUrl.replacingOccurrences(of: replace, with: "")
                acessTokenUrl = acessTokenUrl.replacingOccurrences(of: "#", with: "&")
                let tokenStringArray = acessTokenUrl.components(separatedBy: "&")
                
                for (_,strValue) in tokenStringArray.enumerated() {
                    let responseData = strValue.components(separatedBy: "=")
                    if responseData.count > 1 {
                        let keyValue = String(responseData[1])
                        
                        if (responseData[0] == "access_token") {
                            NRUserSession.sharedInstance.accessToken = keyValue
                            NRUserSession.sharedInstance.tokenRefreshDate = Date()
                        } else if (responseData[0] == "refresh_token") {
                            NRUserSession.sharedInstance.refreshToken = keyValue
                        } else if (responseData[0] == "token_type") {
                            NRUserSession.sharedInstance.tokenType = keyValue
                        } else if (responseData[0] == "expires_in") {
                            let stringRepresentation = keyValue
                            let date = Date().addingTimeInterval(Double(stringRepresentation!)! - kTimeOffsetSeconds)
                            NRUserSession.sharedInstance.tokenExpirationDate = date
                        }
                    }
                }
                
                if defaults.bool(forKey: kNewAppIDLogout) == true {
                    let repo = RealmUserRepository()
                    let user = repo.getDefaultUser()
                    let artikStore = ArtikCloudConnectionManager()
                    let _ = artikStore.storeUserProperties(user: user, method: "PUT").subscribe(onNext: {error in }, onError: {error in }, onCompleted: {
                        defaults.set(false, forKey: kNewAppIDLogout)
                        defaults.synchronize()
                    }, onDisposed: {})
                }
                
                
                self.deleteCookies()
                DispatchQueue.main.async {
                    self.isFromCtrl = true
                    self.safariVC!.dismiss(animated: true, completion: {
                        self.dismiss(animated: true, completion: nil)
                    })
                }
                //Show the terms and condition
            } else if acessTokenUrl.contains("status=login_request") && NRUserSession.sharedInstance.accessToken == nil {
                DispatchQueue.main.async {
                    self.isFromCtrl = true
                    self.safariVC!.dismiss(animated: true, completion: {
                        self.dismiss(animated: false, completion: {
                            NavigationUtility.changeRootViewController(userProfile: nil,awakedFromBackground: false)
                        })
                    })
                }
            }
        }
        
        
    }
    
    func safariLoadRequest() {
        let authRequestString = String(format: "%@%@&client=mobile&response_type=token&client_id=%@", kSAMIAuthBaseUrl,kSAMIAuthPath,kSamiClientID)
        
        if let url = URL(string : authRequestString) {
            self.safariVC = SFSafariViewController(url : url)
            self.safariVC.delegate = self
            self.present(self.safariVC, animated: true, completion: nil)
        }
    }
    
    
    func webViewLoadRequest() {
        self.deleteCookies()
        self.loginWebView?.delegate = self
        let dictionary = NSDictionary(object: "Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/41.0.2228.0 Safari/537.36", forKey: "UserAgent" as NSCopying)
        UserDefaults.standard.register(defaults: dictionary as! [String : Any])
        
        let authRequestString = String(format: "%@%@?client=mobile&response_type=token&client_id=%@", kSAMIAuthBaseUrl,kSAMIAuthPath,kSamiClientID)
        LogUtility.logToFile(authRequestString)
        let url = URL(string: authRequestString)
        var requestObj = URLRequest(url : url!)
        requestObj.cachePolicy = NSURLRequest.CachePolicy.reloadIgnoringLocalAndRemoteCacheData
        requestObj.httpShouldHandleCookies = false
        self.loginWebView?.loadRequest(requestObj)
    }
    
    private func deleteCookies() {
        URLCache.shared.removeAllCachedResponses()
        URLCache.shared.diskCapacity = 0
        URLCache.shared.memoryCapacity = 0
        
        if let cookies = HTTPCookieStorage.shared.cookies {
            for cookie in cookies {
                HTTPCookieStorage.shared.deleteCookie(cookie)
            }
        }
    }
    
    public func webView(_ webView: UIWebView, shouldStartLoadWith request: URLRequest, navigationType: UIWebViewNavigationType) -> Bool {
        var acessTokenUrl = String(describing: request)
        print("access token should \(acessTokenUrl)")
        if (acessTokenUrl .contains("#access_token")) {
            let replace = kSAMIAuthBaseUrl + kSAMIAuthPath
            acessTokenUrl = acessTokenUrl.replacingOccurrences(of: replace, with: "")
            acessTokenUrl = acessTokenUrl.replacingOccurrences(of: "#", with: "&")
            let tokenStringArray = acessTokenUrl.components(separatedBy: "&")
            
            for (_,strValue) in tokenStringArray.enumerated() {
                let responseData = strValue.components(separatedBy: "=")
                if responseData.count > 1 {
                    let keyValue = String(responseData[1])
                    
                    if (responseData[0] == "access_token") {
                        NRUserSession.sharedInstance.accessToken = keyValue
                    } else if (responseData[0] == "refresh_token") {
                        NRUserSession.sharedInstance.refreshToken = keyValue
                    } else if (responseData[0] == "token_type") {
                        NRUserSession.sharedInstance.tokenType = keyValue
                    } else if (responseData[0] == "expires_in") {
                        let stringRepresentation = keyValue
                        let date = Date().addingTimeInterval(Double(stringRepresentation!)! - kTimeOffsetSeconds)
                        NRUserSession.sharedInstance.tokenExpirationDate = date
                    }
                }
            }
            
            if defaults.bool(forKey: kNewAppIDLogout) == true {
                let repo = RealmUserRepository()
                let user = repo.getDefaultUser()
                let artikStore = ArtikCloudConnectionManager()
                let _ = artikStore.storeUserProperties(user: user, method: "PUT").subscribe(onNext: {error in }, onError: {error in }, onCompleted: {
                defaults.set(false, forKey: kNewAppIDLogout)
                    defaults.synchronize()
                }, onDisposed: {})
            }
            
            
            self.deleteCookies()
            DispatchQueue.main.async {
                self.dismiss(animated: true, completion: nil)
            }
           //Show the terms and condition
        } else if acessTokenUrl.contains("status=login_request") && NRUserSession.sharedInstance.accessToken == nil {
            DispatchQueue.main.async {
                self.deleteCookies()
                NavigationUtility.changeRootViewController(userProfile: nil,awakedFromBackground: false)
            }
        }
        return true
    }
}

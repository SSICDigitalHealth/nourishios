//
//  PortionSizeComponentCell.swift
//  Optifast
//
//  Created by Gena Mironchyk on 10/12/17.
//  Copyright © 2017 SSIC. All rights reserved.
//

import UIKit

protocol PortionSizeHeaderDelegate {
    func toggleSection(header: PortionSizeComponentCell, section: Int)
}

class PortionSizeComponentCell: UITableViewHeaderFooterView {
    @IBOutlet weak var componentIcon : UIImageView!
    @IBOutlet weak var componentTitle : UILabel!
    @IBOutlet weak var arrowPicture : UIImageView!
    @IBOutlet weak var separator : UIView!
    var delegate: PortionSizeHeaderDelegate?
    var section : Int = 0
    
    override func awakeFromNib() {
   
        super.awakeFromNib()
        self.componentTitle.translatesAutoresizingMaskIntoConstraints = false
        self.componentIcon.translatesAutoresizingMaskIntoConstraints = false
        //        self.separator.backgroundColor = NRColorUtility.hexStringToUIColor(hex : "C3C2C3")
        self.translatesAutoresizingMaskIntoConstraints = false
        let gestureRecognizer = UITapGestureRecognizer.init(target: self, action: #selector(PortionSizeComponentCell.tapHeader(gestureRecognizer:)))
        self.addGestureRecognizer(gestureRecognizer)
        // Initialization code
    }
    
    func tapHeader(gestureRecognizer: UITapGestureRecognizer) {
        guard let cell = gestureRecognizer.view as? PortionSizeComponentCell else {
            return
        }
        delegate?.toggleSection(header: self, section: cell.section)
    }
    func setCollapsed(collapsed: Bool) {
        self.arrowPicture.isHidden = false
        if collapsed {
            self.arrowPicture.rotate(CGFloat(180.degreesToRadians))
            self.separator.backgroundColor = UIColor.clear
        } else {
            self.arrowPicture.rotate(CGFloat(0.degreesToRadians))
            self.separator.backgroundColor = NRColorUtility.hexStringToUIColor(hex : "C3C2C3")
        }
    }
    
    func setupWith(sectionData : Section) {
        self.componentIcon.image = sectionData.sectionImage.withRenderingMode(.alwaysTemplate)
        self.componentIcon.layer.cornerRadius = self.componentIcon.frame.size.height/2.0
        self.componentIcon.backgroundColor = self.tintColorFrom(name: sectionData.nameSection)
        self.componentIcon.tintColor = UIColor.white
        self.componentTitle.text = sectionData.nameSection
    }
    
    
    private func tintColorFrom(name : String) -> UIColor {

        switch name {
        case "Vegetables":
            return NRColorUtility.hexStringToUIColor(hex: "70C397")
        case "Fruits":
            return NRColorUtility.hexStringToUIColor(hex: "f6321d")
        case "Protein":
            return NRColorUtility.hexStringToUIColor(hex: "A64DB6")
        case "Dairy":
            return NRColorUtility.hexStringToUIColor(hex: "1172B9")
        default:
            return NRColorUtility.hexStringToUIColor(hex: "F0C517")
        }
    }
    

}

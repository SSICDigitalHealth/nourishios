//
//  OcasionTableViewCell.swift
//  Nourish
//
//  Created by Vlad Birukov on 09.06.17.
//  Copyright © 2017 SSIC. All rights reserved.
//

import UIKit

class OcasionTableViewCell: UITableViewCell {

    @IBOutlet weak var nameOcasion: UILabel!
    @IBOutlet weak var stateOcasion: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func setupWithModel(model: (Ocasion, Double)) {
        nameOcasion.text = Ocasion.stringDescription(servingType: model.0)
        if model.1 != 0.0 {
            stateOcasion.text = String(format: "%0.f kcal", model.1)
        } else {
            stateOcasion.text = ""
        }
    }

}

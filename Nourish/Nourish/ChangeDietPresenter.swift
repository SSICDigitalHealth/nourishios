//
//  ChangeDietPresenter.swift
//  Nourish
//
//  Created by Vlad Birukov on 22.08.17.
//  Copyright © 2017 SSIC. All rights reserved.
//

import Foundation
import RxSwift


class ChangeDietPresener: BasePresenter, UITableViewDataSource,UITableViewDelegate {
    let interactor = ChangeDietInteractor()
    let mapperObject = UserProfile_to_UserProfileModel()
    let mapperModel = UserProfileModel_to_UserProfile()
    var userModel : UserProfileModel = UserProfileModel()
    var changeDietViewController: ChangeDietViewController?
    var changeDietView: ChangeDietProtocol?
    var isChange: Bool = false

    var dietPreference : [(nameDiet:String,isSelected:Bool)] = [(nameDiet:"US Healthy",isSelected:false),(nameDiet:"Vegetarian",isSelected:false)]
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.getUserInformation()
    }
    
    private func getUserInformation() {
        self.subscribtions.append(self.interactor.execute().observeOn(MainScheduler.instance).subscribe(onNext: { [unowned self] userProfile in
            self.userModel = self.mapperObject.transform(user: userProfile)
            self.setCurrentDiet()
        }, onError: {error in
            self.changeDietView?.stopActivityAnimation()
            self.changeDietView?.parseError(error: error, completion: nil)
        }, onCompleted: {
        }, onDisposed: {
            self.changeDietView?.stopActivityAnimation()
        }))
    }

    private func setCurrentDiet() {
        for i in 0..<self.dietPreference.count {
            if self.dietPreference[i].nameDiet == self.userModel.dietaryPreference?.descriptionUI {
                self.dietPreference[i].isSelected = true
            }
        }
        self.changeDietView?.stopActivityAnimation()
        self.changeDietView?.reloadData()
    }

    private func changeDietPreference(dietaryPreference : DietaryPreference) {
        self.userModel.dietaryPreference = dietaryPreference
        self.isChange = true
    }
    
    @IBAction func update(_ sender: Any) {
        if self.isChange == true {
            self.interactor.saveChangeDiet(userProfile: self.mapperModel.transform(userModel: self.userModel), writeToHealth: false)
            saveDietAndBack()
        }
    }
    
    func saveDietAndBack() {
        if self.changeDietViewController != nil {
            self.changeDietViewController?.navigationController?.popViewController(animated: true)
            NavigationUtility.showTabBar(animated: false)
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.dietPreference.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cellDietChange", for: indexPath) as! DietChangeTableViewCell
        
        cell.setupWith(diet: self.dietPreference[indexPath.row])
        
        return cell
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let selectDiet = self.dietPreference[indexPath.row]
        let selectNewDietryPreference = DietaryPreference.enumFromString(string: selectDiet.nameDiet.lowercased())!
        
        if selectNewDietryPreference != self.userModel.dietaryPreference {
            self.changeDietPreference(dietaryPreference: selectNewDietryPreference)
            for i in 0...dietPreference.count - 1 {
                self.dietPreference[i].isSelected = false
            }
            self.dietPreference[indexPath.row].isSelected = true
            self.changeDietView?.reloadData()
        }
    }
}

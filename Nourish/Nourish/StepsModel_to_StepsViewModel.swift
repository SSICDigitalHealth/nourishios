//
//  StepsModel_to_StepsViewModel.swift
//  Nourish
//
//  Created by Vlad Birukov on 29.08.17.
//  Copyright © 2017 SSIC. All rights reserved.
//

import Foundation

class StepsModel_to_StepsViewModel {
    func transform(model: StepsModel) -> StepsViewModel {
        let stepsViewModel = StepsViewModel()
        if model.numberSteps != nil {
            stepsViewModel.numberSteps = model.numberSteps
        }
        
        if model.avgStepsDay != nil {
            stepsViewModel.avgStepsDay = model.avgStepsDay
        }
        return stepsViewModel
    }
}

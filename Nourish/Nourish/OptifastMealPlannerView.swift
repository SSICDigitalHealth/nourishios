//
//  OptifastMealPlannerView.swift
//  Optifast
//
//  Created by Gena Mironchyk on 1/9/18.
//  Copyright © 2018 SSIC. All rights reserved.
//

import UIKit
protocol OptifastMealPlannerProtocol : BaseViewProtocol {
    func reloadData()
}


class OptifastMealPlannerView: BaseView, OptifastMealPlannerProtocol {

    @IBOutlet weak var mealPlannerTable: UITableView!
    @IBOutlet var presenter: OptifastMealPlannerPresenter!
    
    override func viewDidLoad() {
        self.xibAutoLayouting(nibName: "OptifastMealPlannerView")
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.basePresenter = presenter
        self.presenter.mealPlannerView = self
        self.registerCells()
        super.viewWillAppear(animated)
    }
    
    func reloadData() {
         self.mealPlannerTable.reloadData()
    }
    
    private func registerCells() {
        self.mealPlannerTable.register(UINib(nibName: "OptifastMealPlannerSectionCell", bundle: nil), forCellReuseIdentifier: "optifastSectionCell")
        self.mealPlannerTable.register(UINib(nibName: "OptifastMealPlannerMealCell", bundle: nil), forCellReuseIdentifier: "optifastMealCell")
        self.mealPlannerTable.register(UINib(nibName: "OptifastHeaderMealPlannerTableViewCell", bundle: nil), forCellReuseIdentifier: "oprifastHeaderCell")

    }
    
   
    
}

//
//  OptifastMealPlannerChangeDishView.swift
//  Optifast
//
//  Created by Gena Mironchyk on 1/11/18.
//  Copyright © 2018 SSIC. All rights reserved.
//

import UIKit
protocol OptifastMealPlannerChangeDishProtocol : BaseViewProtocol {
    func reloadData()
}

class OptifastMealPlannerChangeDishView: BaseView, OptifastMealPlannerChangeDishProtocol {
    
    @IBOutlet weak var optifastDishesTableView : UITableView!
    @IBOutlet weak var presenter : OptifastMealPlannerChangeDishPresenter!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.xibAutoLayouting(nibName: "OptifastMealPlannerChangeDishView")
        self.basePresenter = presenter
        self.presenter.changeDishView = self
        self.registerCell()

    }

    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    func reloadData() {
        self.optifastDishesTableView.reloadData()
    }
    
    private func registerCell () {
        self.optifastDishesTableView.register(UINib(nibName: "OptifastMealPlannerChangeDishCell", bundle: nil), forCellReuseIdentifier: "changeDishCell")

    }
}

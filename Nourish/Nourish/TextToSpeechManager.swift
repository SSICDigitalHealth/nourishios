//
//  TextToSpeechManager.swift
//  NourIQ
//
//  Created by Vino (Vinodha) Sundaramoorthy on 11/1/17.
//  Copyright © 2017 SSIC. All rights reserved.
//

import UIKit
import AVFoundation

class TextToSpeechManager: NSObject {
    static let shared = TextToSpeechManager()
    var synthesizer : AVSpeechSynthesizer!
    
    func speak(str:String) {
        if self.synthesizer != nil {
            self.synthesizer.stopSpeaking(at: AVSpeechBoundary.immediate)
        }
    
        let utterance = AVSpeechUtterance(string: str)
        utterance.voice = AVSpeechSynthesisVoice(language: "en-US")
    
        self.synthesizer = AVSpeechSynthesizer()
        synthesizer.speak(utterance)
        LogUtility.logToFile("Speech Manager Speaking :\(str)")
    }

    func stop() {
        if self.synthesizer != nil {
            LogUtility.logToFile("Speech Manager Stop Speaking")
            self.synthesizer.stopSpeaking(at: AVSpeechBoundary.immediate)
        }
    }
}

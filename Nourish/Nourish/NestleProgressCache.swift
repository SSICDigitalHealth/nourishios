//
//  ProgressCacheRealm.swift
//  Nourish
//
//  Created by Vlad Birukov on 26.06.17.
//  Copyright © 2017 SSIC. All rights reserved.
//

import Realm
import RealmSwift

class NestleProgressCache: Object {
    dynamic var userScore: Double = 0.0
    dynamic var userCalories : Calories?
    let caloriesForDay = List<CaloriesForDay>()
    let microelementList = List<Microelement>()
    let watchoutForList =  List<RealmString>()
    let scoreHistoryList = List<ScoreHistory>()
    let calsHistory = List<CaloriesHistory>()
    let nutrientsListToShow = List<RealmString>()
    let componentsHistory = List<MicroelementHistoryObject>()
    
    dynamic var startDate : Date? = nil
    dynamic var endDate : Date? = nil
    dynamic var numDays : Double = 0.0
    
    var returnHistory = true
}

class ScoreHistory : Object {
    dynamic var startDate : Date? = nil
    dynamic var endDate : Date? = nil
    dynamic var scoreValue : Double = 0.0
}

class Calories: Object {
    dynamic var consumed: Double = 0.0
    dynamic var target: Double = 0.0
    dynamic var unit = ""
}

class MicroelementHistoryObject : Object {
    dynamic var date : Date?
    dynamic var microelementId : String?
    dynamic var consumedAmount : Double = 0.0
    
}

class CaloriesForDay: Object {
    dynamic var consumedCalories: Double = 0.0
    dynamic var nameFood = ""
    dynamic var amountFood: Double = 0.0
    dynamic var unit: String? = nil
    dynamic var nameFoodIntake = ""
}

class CaloriesHistory : Object {
    dynamic var consumed : Double = 0.0
    dynamic var maxConsumed : Double = 0.0
    dynamic var date : Date? = nil
}

class Microelement: Object {
    let consumedMicroelement = RealmOptional<Double>()
    dynamic var idNameMicroelement = ""
    let lowerLimit = RealmOptional<Double>()
    let upperLimit = RealmOptional<Double>()
    dynamic var unit = ""
    dynamic var message = ""
    dynamic var nutritionIndicator : Int = 2
    let contributors = List<FoodElement>()
}


class FoodElement: Object {
    dynamic var nameFood = ""
    dynamic var consumed: Double = 0.0
    dynamic var quality: Double = 0.0
    dynamic var unit = ""
}




//
//  NRWeightProgressInteractor.swift
//  Nourish
//
//  Created by Vino (Vinodha) Sundaramoorthy on 12/6/16.
//  Copyright © 2016 SSIC. All rights reserved.
//

import UIKit
import RxSwift

class NRWeightProgressInteractor: NSObject {
    
    let userRepo = UserRepository.shared
    let activityRepo = UserActivityRepository()
    
    
    func weightProgress() -> Observable<NRWeightProgress> {
        return self.getWeightProgress()
    }
    
    private func getWeightProgress() -> Observable<NRWeightProgress> {
        let userBool = self.getUserWeightBool()
        let model = self.getOldWeightModel()
        
        let zip = Observable.zip(model, userBool, resultSelector : { weightModelValue, boolValue -> NRWeightProgress in
            let proxyModel = weightModelValue
            proxyModel.dailyWeightLogged = boolValue
            return proxyModel
        })
        
        return zip
        
    }
    
    private func generateFakeWeightProgress() -> NRWeightProgress {
        let object = NRWeightProgress()
        object.progressPercentage = Double(self.generateRandomBetween(max: 100, min: 0))
        object.metrics = "lbs"
        object.weightProgress = Double(self.generateRandomBetween(max: 10, min: 0))
        object.currentWeight = Double(self.generateRandomBetween(max: 180, min: 120))
        return object
    }
    
    func generateRandomBetween(max : Int, min : Int) -> Int {
        return min + Int(arc4random_uniform(UInt32(max - min + 1)))
    }
    
    private func getUserWeightBool() -> Observable<Bool> {
        return userRepo.getCurrentUser(policy: .DefaultPolicy).flatMap {user -> Observable<Bool> in
            let chatRepository = ChatBoxRepository()
            return chatRepository.recordedDailyWeightFor(date: Date(), userID: user.userID!, userGoal: user.userGoal)
        }
    }
    
    private func getOldWeightModel() -> Observable<NRWeightProgress> {
        let userValue = self.userRepo.getCurrentUser(policy: .DefaultPolicy)
        let weight = self.activityRepo.getUserWeight()
        
        let zip = Observable.zip(userValue, weight, resultSelector: { user, weightValue -> NRWeightProgress in
            let model = NRWeightProgress()
            if user.userGoal == .LooseWeight {
                
                let currentWeight = weightValue ?? user.weight
                let targetWeight = user.goalValue ?? currentWeight! - 2
                var progressPercentage = (user.startWeight! - currentWeight!)/(user.startWeight! - targetWeight) * 100
                
                if progressPercentage.isNaN || progressPercentage < 0 {
                    progressPercentage = 0.0
                }
                
                if progressPercentage > 100 {
                    progressPercentage = 100
                }
                
                model.weightProgress = currentWeight! - user.startWeight!
                model.currentWeight = currentWeight!
                model.metrics = (user.metricPreference?.isMetric)! ? "kg" : "lbs"
                model.progressPercentage = progressPercentage
                model.userId = user.userID
                return model
            } else if user.userGoal == .MaintainWeight{
                let currentWeight = weightValue ?? user.weight
                let weightToMaiantain = user.weightToMaintain
                var progressPercentage = (weightToMaiantain!/currentWeight!) * 100
                if progressPercentage.isNaN || progressPercentage < 0 {
                    progressPercentage = 0.0
                }
                
                if progressPercentage > 100 {
                    progressPercentage = 100
                }
                model.weightProgress = currentWeight! - weightToMaiantain!
                model.currentWeight = currentWeight!
                model.metrics = (user.metricPreference?.isMetric)! ? "kg" : "lbs"
                model.progressPercentage = progressPercentage
                model.userId = user.userID
                return model
            } else {
                return self.generateFakeWeightProgress()
            }

        })
        
        return zip
        
    }

}

//
//  UserProfileCacheRealm_to_UserProfile.swift
//  Nourish
//
//  Created by Nova on 1/17/17.
//  Copyright © 2017 SSIC. All rights reserved.
//

import UIKit

class UserProfileCacheRealm_to_UserProfile: NSObject {
    func transform(userCache : UserProfileCacheRealm) -> UserProfile {
        let user = UserProfile()
        user.userID = userCache.userID?.description
        user.userFirstName = userCache.userFirstName?.description
        user.userLastName = userCache.userLastName?.description
        user.activeSinceTimeStamp = userCache.activeSinceTimeStamp
        
        if userCache.activityLevel != nil {
            user.activityLevel = ActivityLevel.enumFromString(string: (userCache.activityLevel?.description)!)
        }
        
        user.height = userCache.height.value
        user.weight = userCache.weight.value
        
        if userCache.biologicalSex != nil {
            user.biologicalSex = BiologicalSex.enumFromString(string: (userCache.biologicalSex?.description)!)
        }
        
        if userCache.dietaryPreference != nil {
            user.dietaryPreference = DietaryPreference.enumFromString(string: (userCache.dietaryPreference?.description)!)
        }
        
        if userCache.metricPreference != nil {
            user.metricPreference = MetricPreference.enumFromString(string: (userCache.metricPreference?.description)!)
        }
        
        
        user.goalPrefsFood = userCache.goalPrefsFood.value
        user.goalPrefsLifestyle = userCache.goalPrefsLifestyle.value
        user.goalPrefsActivity = userCache.goalPrefsActivity.value
        
        if userCache.userGoal != nil {
            user.userGoal = UserGoal.enumFromString(string: (userCache.userGoal?.description)!)
        }
        
        user.age = userCache.age.value
        user.startWeight = userCache.startWeightTest.value
        user.goalValue = userCache.goalTest.value
        user.weightLossStartDate = userCache.weightLossStartDate
        user.email = userCache.email?.description
        user.weightToMaintain = userCache.weightToMaintain.value
        user.needToInformUser = userCache.needToInformUser
        
        if userCache.listOfOptifastFoodIds.count > 0 {
            user.optifastFoodIds = userCache.listOfOptifastFoodIds.map({return $0.stringValue})
        }
        
        if userCache.listOfOptifastOcasions.count > 0 {
            user.optifastOcasionsList = userCache.listOfOptifastOcasions.map({return Ocasion(rawValue : $0.value)!})
        }
        
        let _ = user.calculateAndStoreCaloriePlan()
        
        return user
    }
}

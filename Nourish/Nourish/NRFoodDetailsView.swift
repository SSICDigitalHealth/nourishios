//
//  NRFoodDetailsView.swift
//  Nourish
//
//  Created by Vino (Vinodha) Sundaramoorthy on 11/21/16.
//  Copyright © 2016 SSIC. All rights reserved.
//

import UIKit
import RxSwift

let kNutrientRowHeight : Int = 30
class NRFoodDetailsView: BaseView , NRFoodDetailsProtocol {
    
    @IBOutlet weak var nutrientDetailsTableView : UITableView!
    @IBOutlet weak var servingTypePicker : NRPickerView!
    @IBOutlet weak var measurePicker : NRPickerView!
    @IBOutlet weak var addToDiaryButton : UIButton!
    @IBOutlet weak var favButton : UIButton!
    @IBOutlet weak var foodImage : UIImageView!
    @IBOutlet weak var nutrientDetailsHeight : NSLayoutConstraint!
    @IBOutlet weak var imageHeightConstraint : NSLayoutConstraint!
    @IBOutlet weak var closeButton : UIButton!
    @IBOutlet weak var titleLabel : UILabel!
    @IBOutlet weak var modalNavBarHeightConstraint : NSLayoutConstraint!
    @IBOutlet weak var navBarFavButton : UIBarButtonItem?
    @IBOutlet weak var titleLineView : UIView!
    @IBOutlet var presenter : NRFoodDetailsPresenter?
    var presentedModally : Bool = false
    var updatingAmount : Bool = false
    
    var isMealPrediction : Bool = false
    var showFavButton : Bool = false

    var isFromGroup : Bool = false
    var viewController : NRFoodDetailsViewController?
    var foodDetailsModel : foodSearchModel!
    
    override func viewWillAppear(_ animated: Bool) {
        self.basePresenter = presenter
        self.foodImage.translatesAutoresizingMaskIntoConstraints = false
        servingTypePicker.font = UIFont.systemFont(ofSize: 15)
        servingTypePicker.textColor = NRColorUtility.hexStringToUIColor(hex: "#808080")
        servingTypePicker.highlightedFont = UIFont.systemFont(ofSize: 14, weight: UIFontWeightBold)
        servingTypePicker.highlightedTextColor = NRColorUtility.hexStringToUIColor(hex: "#303030")
        servingTypePicker.maskDisabled = true
        measurePicker.textColor = UIColor.white
        measurePicker.highlightedTextColor = UIColor.white
        measurePicker.highlightedFont = UIFont.systemFont(ofSize: 24, weight: UIFontWeightBold)
        measurePicker.font = UIFont.systemFont(ofSize: 24, weight: UIFontWeightRegular)
        measurePicker.maskDisabled = true
        presenter?.foodView = self
        presenter?.date = viewController?.date
        viewController?.foodDetailsView = self
        self.nutrientDetailsTableView.delegate = self.presenter
        self.nutrientDetailsTableView.dataSource = self.presenter
        self.servingTypePicker.dataSource = self.presenter
        self.servingTypePicker.delegate = self.presenter
        self.measurePicker.delegate = self.presenter
        self.measurePicker.dataSource = self.presenter
        nutrientDetailsTableView?.register(UINib(nibName: "NRFoodNutrientCell", bundle: nil), forCellReuseIdentifier: "kNutrientCell")
        presenter?.occasion = viewController?.ocassion
        self.foodDetailsModel = presenter?.foodSearchModel
        presenter?.foodSearchModel = viewController?.foodSearchModel
        if self.presentedModally == true {
            self.setupViewForModalPresentation()
        }
        if self.updatingAmount == true {
            self.addToDiaryButton.setTitle("UPDATE AMOUNT", for:.normal)
        }
        self.favButton.addTarget(self, action: #selector(toggleFavourite), for: .touchUpInside)
        self.addToDiaryButton.backgroundColor = NRColorUtility.appBackgroundColor()
        super.viewWillAppear(animated)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
    }
    
    
    func deinitAll () {
        self.viewController = nil
        self.presenter?.foodView = nil
        self.presenter?.foodDetailsViewControler = nil
        self.presenter = nil
    }
    
    private func setupViewForModalPresentation () {
        self.modalNavBarHeightConstraint.constant = 44.0
        self.closeButton.isHidden = false
        self.titleLabel.isHidden = false
        self.titleLineView.isHidden = false
        self.layoutIfNeeded()
    }
    
    @IBAction private func dismiss () {
        self.viewController?.dismiss(animated: true, completion: {
            self.deinitAll()
        })
    }
    
    func setFoodImage(image : UIImage) {
        self.foodImage.image = image
        let _ = self.foodImage.addGradientWithHeight(height: 56)
        self.imageHeightConstraint.constant = 200
    }
    
    func hideFoodImageView() {
        self.imageHeightConstraint.constant = 0.0
    }

    // MARK: NRFoodDetailsProtocol
    func stopActivityIndicator() {
        self.stopActivityAnimation()
    }
    
    func disableNavigationBar() {
        self.viewController?.diableNavigationBar()
    }
    
    func startActivityIndicator() {
        self.prepareLoadView()
    }
    
    func setupFoodDetailsView() {
        EventLogger.logOpenFoodCardClick()
        presenter?.foodSearchModel = viewController?.foodSearchModel
        presenter?.occasion = viewController?.ocassion
        if self.navBarFavButton != nil {
            self.navBarFavButton?.target = self
            self.navBarFavButton?.action = #selector(toggleFavourite)
        }
        self.foodDetailsModel = self.presenter?.foodSearchModel
        self.presenter?.subscribtions.append((self.presenter?.fetchAllData().observeOn(MainScheduler.instance).subscribe(onNext: {completed in
            if completed {
            }
            LogUtility.logToFile("Food Fetching Completed")
        },onError: {error in
            print(error)
            LogUtility.logToFile("error")
        }, onCompleted: {
            self.setupFavButton ()
            self.loadNutrientDetails()
            self.measurePicker.reloadData()
            self.servingTypePicker.reloadData()
            self.stopActivityAnimation()
        }, onDisposed: {}))!)
        
    }
    

    func setupFavButton () {
        if self.isFromGroup == false {
            if self.presentedModally == true {
                self.favButton.isHidden = self.showFavButton
                if self.foodDetailsModel.isFavorite! {
                    self.favButton.setImage(UIImage(named : "fav_green"), for: .normal)
                } else {
                    self.favButton.setImage(UIImage(named : "fav_icon_00-1"), for: .normal)
                }
            } else {
                if self.foodDetailsModel.isFavorite! {
                    self.navBarFavButton?.image = UIImage(named : "fav_green")
                    self.navBarFavButton?.tintColor = NRColorUtility.hexStringToUIColor(hex: "#70c397")
                } else {
                    self.navBarFavButton?.image = UIImage(named : "fav_icon_00-1")
                }
                if self.isMealPrediction {
                    self.viewController?.removeRightItem()
                }
            }
        } else {
            self.viewController?.removeRightItem()
            self.favButton.isHidden = true
        }
        
    }
    
    func toggleFavourite () {
        self.startActivityIndicator()
        self.navBarFavButton?.isEnabled = false
        if self.foodDetailsModel.isFavorite! {
            self.presenter?.subscribtions.append((self.presenter?.deleteFromFavorite().observeOn(MainScheduler.instance).subscribe(onNext: { [unowned self] success in
                if success {
                    self.foodDetailsModel.isFavorite = false
                    self.setupFavButton()
                }
                }, onError: {error in
                    print(error)
            }, onCompleted: {
                self.navBarFavButton?.isEnabled = true
                self.stopActivityIndicator()
            }, onDisposed: {}))!)

        } else {
            let _ = self.presenter?.addToFavourites().observeOn(MainScheduler.instance).subscribe(onNext: { [unowned self] success in
                if success {
                    self.foodDetailsModel.isFavorite = true
                    self.setupFavButton()
                }
                
            }, onError: {error in
                print(error)
            }, onCompleted: {
                self.navBarFavButton?.isEnabled = true
                self.stopActivityIndicator()
            }, onDisposed: {})
        }
    }
    
    
    
    func loadNutrientDetails() {
        let noOfRows = self.presenter?.foodNutrientsArray.count ?? 0
        self.nutrientDetailsHeight.constant = CGFloat(kNutrientRowHeight * noOfRows)
        
        //Reload nutrient table
        self.nutrientDetailsTableView.reloadData()
    }
    
    func finishSaveToDiary() {
        self.viewController?.finishSaveToDiary()
    }
    
    func setUpFoodDetailsModel(detailsModel : foodSearchModel) {
        self.foodDetailsModel = detailsModel
    }
    
    func refreshMeasureWithServingType(index : Int) {
        DispatchQueue.main.async(){
            self.measurePicker.reloadData()
            self.measurePicker.selectItem(index, animated: false, notifySelection: true)
        }
    }
 
    func reloadNutrientDetails(servingType:String ,measure:Double) {
        self.foodDetailsModel.unit = servingType
        self.foodDetailsModel.amount = measure
        self.loadNutrientDetails()
    }
    
    func refreshServingsWithIndex(index : Int) {
        DispatchQueue.main.async {
            self.servingTypePicker.reloadData()
            self.servingTypePicker.selectItem(index, animated: true, notifySelection: true)
           }
    }
    
    func performDelegateAction() {
        self.presenter?.performDelegateAction()
    }
    
    func addToDiaryForOcassion(ocasion: Ocasion) {
        self.presenter?.addToDiary(ocasion: ocasion, date : self.presenter!.date)
    }
    
    func changeFood() {
        self.presenter?.changeFoodAmount()
    }
}

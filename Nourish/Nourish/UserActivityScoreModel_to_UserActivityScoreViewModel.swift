//
//  UserActivityScoreModel_to_UserActivityScoreViewModel.swift
//  Nourish
//
//  Created by Vlad Birukov on 06.06.17.
//  Copyright © 2017 SSIC. All rights reserved.
//

import Foundation

class UserActivityScoreModel_to_UserActivityScoreViewModel {
    
    func transform(score: UserActivityScoreModel) -> UserActivityScoreViewModel {
        let userActivityScoreViewModel = UserActivityScoreViewModel()
        userActivityScoreViewModel.scoreActivity = score.scoreActivity
        userActivityScoreViewModel.isDairyEmpty = score.isDairyEmpty
        if score.historyScore.count != 0 {
            for object in score.historyScore {
                userActivityScoreViewModel.scoreHistory.append(object.value)
            }
        }
        
        return userActivityScoreViewModel
    }
}

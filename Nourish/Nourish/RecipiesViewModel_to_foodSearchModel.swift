//
//  RecipiesViewModel_to_foodSearchModel.swift
//  Nourish
//
//  Created by Vlad Birukov on 16.10.2017.
//  Copyright © 2017 SSIC. All rights reserved.
//

import Foundation

class RecipiesViewModel_to_foodSearchModel {
    func transform(model: RecipiesViewModel) -> foodSearchModel {
        var foodsearchModel = foodSearchModel()
        
        foodsearchModel.amount = model.amount
        foodsearchModel.foodId = model.foodId
        foodsearchModel.unit = model.unit
        foodsearchModel.grams = model.grams
        foodsearchModel.mealIdString = model.foodId
        foodsearchModel.foodTitle = model.nameFood
        foodsearchModel.calories = String(model.numberCal)
        
        return foodsearchModel
    }
}

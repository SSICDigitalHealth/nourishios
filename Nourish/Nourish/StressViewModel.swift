//
//  StressViewModel.swift
//  Nourish
//
//  Created by Vlad Birukov on 29.08.17.
//  Copyright © 2017 SSIC. All rights reserved.
//

import Foundation

class StressViewModel {
    var avgBPM: Double?
    var maxBPM: Double?
    var minBPM: Double?
    var currentBPM: Double?
}

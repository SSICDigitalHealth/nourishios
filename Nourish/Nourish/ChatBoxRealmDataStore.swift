//
//  ChatBoxRealmDataStore.swift
//  Nourish
//
//  Created by Nova on 2/6/17.
//  Copyright © 2017 SSIC. All rights reserved.
//

import UIKit
import RealmSwift
import RxSwift
class ChatBoxRealmDataStore: NSObject {
    
    
    func getRecordsFor(date : Date,userID : String, userGoal : UserGoal?) -> Observable<[ChatDataBaseItem]> {
        let realm = try! Realm()
        let calendar = Calendar.current
        let startOfDay = calendar.startOfDay(for: date)
        
        var comps = DateComponents()
        comps.day = 1
        comps.second = -1
        
        let endDate = calendar.date(byAdding: comps, to: startOfDay)
        let predicate = NSPredicate(format : "date > %@ && date < %@",startOfDay as NSDate, endDate! as NSDate)
        let userPredicate = NSPredicate(format : "userID == %@",userID)
        
        
        let deletePredicate = NSPredicate(format: "date < %@", startOfDay as NSDate)
        let messagesToDelete = realm.objects(ChatDataBaseItem.self).filter(deletePredicate)
        
        if messagesToDelete.count > 0 {
            try! realm.write {
                realm.delete(messagesToDelete)
            }
        }
        
        let messages = realm.objects(ChatDataBaseItem.self).filter(predicate).sorted(byKeyPath: "date", ascending : true).filter(userPredicate)
        
       // let arrayToReturn = self.addLogWeightMessageIfNeddedTo(arrayOfMessages: Array(messages), userID: userID, userGoal: userGoal, realm: realm)
            
            //Array(messages)
        
        return Observable.just(Array(messages))
    }
    
    
    
    
    
    func addLogWeightMessageIfNeddedTo(arrayOfMessages : [ChatDataBaseItem], userID:String, userGoal: UserGoal?) -> Observable<[ChatDataBaseItem]> {
        let realm = try! Realm()
        var contains = false
        var arrayToReturn : [ChatDataBaseItem] = []
        let calendar = Calendar.current
        let date = defaults.value(forKey: kOnBoardingKeyDate) as? Date
        if date != nil {
            if calendar.startOfDay(for: date!) != calendar.startOfDay(for: Date()) {
                if userGoal != nil {
                   if userGoal == .MaintainWeight ||  userGoal == .LooseWeight {
                    for message in arrayOfMessages {
                        if message.chatBoxType == .typeLogWeight {
                            contains = true
                        }
                    }
                    for message in arrayOfMessages {
                        if message.chatBoxType == .typeWeightRecord {
                            contains = true
                        }
                    }
                    
                    if contains == false {
                        let logWeightMessage = ChatDataBaseItem()
                        logWeightMessage.isFromUser = false
                        logWeightMessage.chatBoxType = .typeLogWeight
                        logWeightMessage.content = "Please update your weight today."
                        logWeightMessage.date = Date()
                        logWeightMessage.userID = userID
                        try! realm.write {
                            realm.add(logWeightMessage)
                        }
                        arrayToReturn.append(logWeightMessage)
                        }
                    
                    }
                }
            
            }
        }
        return Observable.just(arrayToReturn)

    }
    
    
    func storeRecordsArray(array : [ChatDataBaseItem]) {
        let realm = try! Realm()
        for object in array {
            try! realm.write {
                realm.add(object)
            }
        }
    }
    
    func deleteMessagesFor(idString : String) {
        DispatchQueue.main.async {
            let realm = try! Realm()
            
            let results = realm.objects(ChatDataBaseItem.self).filter("messageID == %@",idString)
            
            try! realm.write {
                realm.delete(results)
            }
        }
    }
    
    func getAllUserMessagesCount(userID : String) -> Int {
        let userPredicate = NSPredicate(format : "userID == %@",userID)
        let results = try! Realm().objects(ChatDataBaseItem.self).filter(userPredicate)
        return results.count
    }
    
    func getMessageWithContent(content : String) -> ChatDataBaseItem? {
        let contentPredicate = NSPredicate(format : "content == %@",content)
        
        let calendar = Calendar.current
        let startOfDay = calendar.startOfDay(for: Date())
        
        var comps = DateComponents()
        comps.day = 1
        comps.second = -1
        let endDate = calendar.date(byAdding: comps, to: startOfDay)
        
        let predicate = NSPredicate(format : "date > %@ && date < %@",startOfDay as NSDate, endDate! as NSDate)
        let results = try! Realm().objects(ChatDataBaseItem.self).filter(contentPredicate).filter(predicate)
        var array = Array(results)
        array = array.sorted(by: {$0.date > $1.date})
        return array.last
    }
    
}

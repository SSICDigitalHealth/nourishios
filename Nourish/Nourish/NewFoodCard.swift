//
//  NewFoodCard.swift
//  Nourish
//
//  Created by Nova on 10/3/17.
//  Copyright © 2017 SSIC. All rights reserved.
//

import UIKit
import RealmSwift

typealias rulerTuple = (min : Double, max : Double, step : Double)

class NewFoodCard {
    var defaultFoodSearchModel : foodSearchModel? = nil
    var nutrs : [NestleNutrs] = []
    var servings : [NestleServings] = []
}

class NestleNutrs {
    var densityPerGram : Double = 0.0
    var unit : String = ""
    var name : String = ""
    
    func dictionaryRepresentationfor(grams : Double) -> [String : Any] {
        var dict = [String : Any]()
        
        dict["name"] = self.name
        dict["unit"] = self.unit
        dict["quantity"] = self.densityPerGram * grams
        
        return dict
    }
}

class NestleServings {
    var ruler : rulerTuple? = nil
    var defaultAmount : Double = 0.0
    var defaultGrams : Double = 0.0
    var unit : String = ""
    
    func dictionaryRepresentation() -> [String : Any]? {

        if let rulerT = self.ruler {
            var dict = [String : Any]()
            dict["max"] = rulerT.max
            dict["min"] = rulerT.min
            dict["step"] = rulerT.step
            return dict
        }
        
        return nil
        
    }
}

class NewFoodCardCache : Object {
    dynamic var date : Date!
    dynamic var data : Data!
    dynamic var ident : String!
}

//class NewFoodCard_to_json {
//    func transform(array : [NewFoodCard]) -> [[String : Any]] {
//        return array.map {return self.transform(card: $0)}
//    }
//
//    func transform(card : NewFoodCard) -> [String : Any] {
//        var dict = [String : Any]()
//        if let model = card.defaultFoodSearchModel {
//            dict["amount"] = model.amount
//            dict["calories"] = model.calories
//
//        }
//
//    }
//}

class json_to_NewFoodCard {
    func transform(data : Data) -> NewFoodCard? {
        
        var json: [Any]?
        do {
            json = try JSONSerialization.jsonObject(with: data, options: []) as? [Any]
        } catch {
            print(error)
        }
        
        guard let meal = json?.first as? [String : Any] else {
            return nil
        }
        
        var model = foodSearchModel()
        let kcalPerGram = meal["kcal_per_gram"] as? Double ?? 0.0
        model.amount = meal["msre_amount"] as? Double ?? 1.0
        model.unit = meal["msre_desc"] as? String ?? ""
        let grams = meal["msre_grams"] as? Double ?? 0.0
        let calories = kcalPerGram * grams
        model.grams = grams
        model.calories = String(format : "%.1f",calories)
        model.caloriesPerGram = kcalPerGram
        
        let nutrsJson = meal["nutrients"] as? [[String : Any]]
        let servingsJson = meal["servings"] as? [[String : Any]]
        
        let servingObjs = self.transform(servings: servingsJson)
        let nutrsObj = self.transform(nutrs: nutrsJson)
        
        let cardModel = NewFoodCard()
        cardModel.defaultFoodSearchModel = model
        cardModel.nutrs = nutrsObj
        cardModel.servings = servingObjs
        
        return cardModel
        
    }
    
    private func transform(nutrs : [[String : Any]]?) -> [NestleNutrs] {
        var result = [NestleNutrs]()
        
        if nutrs != nil && (nutrs?.count)! > 0 {
            for object in nutrs! {
                let densityPerGram = object["density_per_gram"] as? Double ?? 0.0
                let name = object["nutrient"] as? String ?? ""
                let unit = object["unit"] as? String ?? ""
                
                if densityPerGram > 0 { //check for non 0.0 values
                    let nN = NestleNutrs()
                    nN.densityPerGram = densityPerGram
                    nN.name = name
                    nN.unit = unit
                    result.append(nN)
                }
            }
        }
        
        
        return result
    }
    
    private func transform(servings : [[String : Any]]?) -> [NestleServings] {
        var result = [NestleServings]()
        
        if servings != nil && (servings?.count)! > 0 {
            for serving in servings! {
                let model = NestleServings()
                
                let amount = serving["amount"] as? Double ?? 0.0
                let grams = serving["grams"] as? Double ?? 0.0
                let unit = serving["unit"] as? String ?? ""
                
                if let ruler = serving["ruler"] as? [String : Double] {
                    let maxVal = ruler["max"] ?? 0.0
                    let minVal = ruler["min"] ?? 0.0
                    let stepValue = ruler["step"] ?? 0.0
                    
                    let tuple = rulerTuple(min: minVal, max: maxVal, step : stepValue)
                    model.ruler = tuple
                }
                
                model.defaultAmount = amount
                model.defaultGrams = grams
                model.unit = unit
                result.append(model)
            }
        }
        
        return result
    }
}

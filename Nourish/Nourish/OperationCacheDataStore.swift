//
//  OperationCacheDataStore.swift
//  Nourish
//
//  Created by Nova on 8/30/17.
//  Copyright © 2017 SSIC. All rights reserved.
//

import UIKit
import RealmSwift
import RxSwift


class OperationCacheDataStore: NSObject {
    
    func fetchAllRecordsFor(date : Date) -> [OperationCache] {
        let realm = try! Realm()
        
        let calendar = Calendar.current
        let startOfDay = calendar.startOfDay(for: date)
        
        var comps = DateComponents()
        comps.day = 1
        comps.second = -1
        
        let endDate = calendar.date(byAdding: comps, to: startOfDay)
        
        let predicate = NSPredicate(format : "timeStamp > %@ && timeStamp < %@",startOfDay as NSDate, endDate! as NSDate)
        
        let counterPredicate = NSPredicate(format : "counter < %d", 4)
        
        let operations = realm.objects(OperationCache.self).filter(predicate).filter(counterPredicate).sorted(byKeyPath: "timeStamp", ascending : true)
        
        return Array(operations)
    }
    
    func deleteWithIDent(string : String) {
        let realm = try! Realm()
        let predicate = NSPredicate(format : "localCacheID == %@", string)
        let result = realm.objects(OperationCache.self).filter(predicate)
        
        if result.count > 0 {
            try! realm.write {
                realm.delete(result, cascading: true)
                LogUtility.logToFile(String(format : "Operation with id %@ has been deleted", string))
            }
        }
    }
    
    func increaseCounterFor(stringID : String) {
        let realm = try! Realm()
        let predicate = NSPredicate(format : "localCacheID == %@", stringID)
        
        if let result = realm.objects(OperationCache.self).filter(predicate).first {
            try! realm.write {
                let value = result.counter
                result.counter = value + 1
            }
        }
    }
    
    func fetchFirstRecordFor(date : Date) -> OperationCache? {
        let realm = try! Realm()
        
        let calendar = Calendar.current
        let startOfDay = calendar.startOfDay(for: date)
        
        var comps = DateComponents()
        comps.day = 1
        comps.second = -1
        
        let endDate = calendar.date(byAdding: comps, to: startOfDay)
        
        let predicate = NSPredicate(format : "timeStamp > %@ && timeStamp < %@",startOfDay as NSDate, endDate! as NSDate)
        
        let counterPredicate = NSPredicate(format : "counter < %d", 4)
        
        let operations = realm.objects(OperationCache.self).filter(predicate).filter(counterPredicate).sorted(byKeyPath: "timeStamp", ascending : true)
        
        LogUtility.logToFile("Operations query : ",operations)
        LogUtility.logToFile("Operations count : ", operations.count)
        
        return Array(operations).first
    }
    
    func fetchAllRecords() -> [OperationCache] {
        let realm = try! Realm()
        
        let operations = realm.objects(OperationCache.self)
        return Array(operations)
    }
    
    func store(operations : [OperationCache]) {
        let realm = try! Realm()
        for object in operations {
            try! realm.write {
                realm.add(object)
            }
        }
    }
    
    func fetchUpdateOperationsFor(date : Date, affectedID : String) -> [OperationCache] {
        let realm = try! Realm()
        var result : [OperationCache] = []
        let calendar = Calendar.current
        
        let startOfDay = calendar.startOfDay(for: date)
        
        var comps = DateComponents()
        comps.day = 1
        comps.second = -1
        
        let endDate = calendar.date(byAdding: comps, to: startOfDay)
        
        let predicate = NSPredicate(format : "timeStamp > %@ && timeStamp < %@",startOfDay as NSDate, endDate! as NSDate)
        
        let operations = realm.objects(OperationCache.self).filter(predicate).filter {$0.operationTypeEnum != .create && $0.operationTypeEnum != .createVoice}
        
        for operation in operations {
            if operation.affectedIDs.contains(affectedID) {
                result.append(operation)
            }
        }
        
        return result
    }
    
    func fetchForGroupID(ident : String) -> OperationCache? {
        let realm = try! Realm()
        let predicate = NSPredicate(format : "foodGroupID == %@",ident)
        let operationsArray = Array(realm.objects(OperationCache.self).filter(predicate))
        let result = operationsArray.filter {$0.operationTypeEnum == .addPhoto}
        return result.first
    }
    
    func reFetchCreateGroupOperations(date : Date, affectedID : String, localID : String) {
        let realm = try! Realm()
        let calendar = Calendar.current
        
        let startOfDay = calendar.startOfDay(for: date)
        
        var comps = DateComponents()
        comps.day = 1
        comps.second = -1
        
        let endDate = calendar.date(byAdding: comps, to: startOfDay)
        
        let predicate = NSPredicate(format : "timeStamp > %@ && timeStamp < %@",startOfDay as NSDate, endDate! as NSDate)
        
        let operations = realm.objects(OperationCache.self).filter(predicate).filter {$0.operationTypeEnum == .merge}
        
        for operation in operations {
            if let cacheIDS = operation.dictionary[kCacheIDKey] as? [String] {
                if cacheIDS.count > 0 {
                    if cacheIDS.contains(affectedID)  {
                        
                        let cacheProxy = cacheIDS.filter {$0 != affectedID}
                        var bckIDs = (operation.dictionary[kBackendIDKey] as? [String]) ?? [String]()
                        bckIDs.append(localID)
                        
                        let newDict = [kBackendIDKey : bckIDs, kCacheIDKey : cacheProxy, kGroupNameKey : operation.dictionary[kGroupNameKey] ?? ""]
                        try! realm.write {
                            operation.dictionary = newDict
                        }
                    }
                }
            }
            
        }
    }
}

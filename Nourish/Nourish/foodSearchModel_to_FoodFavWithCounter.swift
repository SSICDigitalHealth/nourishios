//
//  foodSearchModel_to_FoodFavWithCounter.swift
//  Nourish
//
//  Created by Nova on 1/23/17.
//  Copyright © 2017 SSIC. All rights reserved.
//

import UIKit
let favoriteRepo = FavoritesRepository()

class foodSearchModel_to_FoodFavWithCounter: NSObject {
    func transform(model : foodSearchModel) -> FoodFavWithCounter {
        let cache = FoodFavWithCounter()
        cache.foodId = model.foodId != "" ? model.foodId : model.mealIdString
        cache.calories = model.calories
        cache.amount = model.amount
        cache.foodTitle = model.foodTitle
        cache.measure = model.measure
        cache.unit = model.unit
        cache.foodOccasion.value = model.occasion?.rawValue
        cache.photoID = model.userPhotoId
        cache.foodGroupID = model.groupID
        cache.grams = model.grams ?? 0.0
        
        return cache
    }
}

class FoodFavWithCounter_to_foodSearchModel : NSObject {
    
    
    func transform(cache : FoodFavWithCounter) -> foodSearchModel {
        var model = foodSearchModel()
        
        model.foodId = cache.foodId!
        model.calories = cache.calories!
        model.amount = cache.amount
        model.foodTitle = cache.foodTitle!
        model.measure = cache.measure!
        model.unit = cache.unit!
        model.userPhotoId = cache.photoID
        model.grams = cache.grams
        
        if cache.foodOccasion.value != nil {
            model.occasion = Ocasion(rawValue : (cache.foodOccasion.value)!)
        }
        
        model.counted = cache.counter
        model.groupID = cache.foodGroupID
        //model.isFavorite = favoriteRepo.isFoodGroupSingleFavorite(groupID: cache.foodGroupID)
        return model
    }
    
    func transform(caches : [FoodFavWithCounter]) -> [foodSearchModel] {
        var array : [foodSearchModel] = []
        for cache in caches {
            array.append(transform(cache: cache))
        }
        return array
    }

}

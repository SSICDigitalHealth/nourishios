//
//  CurrentStateFoodStuffs_to_CurrentStateFoodStuffsViewModel.swift
//  Nourish
//
//  Created by Vlad Birukov on 19.06.17.
//  Copyright © 2017 SSIC. All rights reserved.
//

import Foundation

class CurrentStateFoodStuffs_to_CurrentStateFoodStuffsViewModel {
    
    func transform(model: CurrentStateFoodStuffs) -> CurrentStateFoodStuffsViewModel {
        let currentStateFoodStuffsViewModel = CurrentStateFoodStuffsViewModel()
        if model.arrCurentStateFood.count != 0 {
            for state in model.arrCurentStateFood {
                currentStateFoodStuffsViewModel.arrCurentStateFood.append(self.transform(model: state))
            }
        }
        return currentStateFoodStuffsViewModel
    }
    
    private func transform(model: StateFoodStuffsModel) -> StateFoodStuffsViewModel {
        let stateFoodStuffsViewModel = StateFoodStuffsViewModel()
        if model.type != nil {
            stateFoodStuffsViewModel.type = model.type
        }
        
        if model.arrFoodElement != nil {
            stateFoodStuffsViewModel.arrFoodElement = [FoodElementViewModel]()
            for element in model.arrFoodElement! {
                stateFoodStuffsViewModel.arrFoodElement?.append(self.transform(model: element))
            }
        }
        
        return stateFoodStuffsViewModel
    }
    
    private func transform(model: FoodElementModel) -> FoodElementViewModel {
        let foodElementViewModel = FoodElementViewModel()
        foodElementViewModel.nameFood = model.nameFood
        if model.amountFood != nil {
            foodElementViewModel.amountFood = model.amountFood
        }
        
        if model.unit != nil {
            foodElementViewModel.unit = model.unit
        }
        
        return foodElementViewModel
    }
}

//
//  NutritionsUtility.swift
//  Nourish
//
//  Created by Gena Mironchyk on 3/31/17.
//  Copyright © 2017 SSIC. All rights reserved.
//

import UIKit

class NutritionsUtility: NSObject {
    class func formatNutrientsName(nutritions : [nutrients]) {
        for nutrient in nutritions {
            nutrient.name = nutrient.name.replacingOccurrences(of: "_", with: " ")
            if nutrient.name.contains("Fat") || nutrient.name.contains("Water") {
                let array = nutrient.name.components(separatedBy: " ")
                if array.count == 2 {
                    let string = String(format:"%@ %@",array.last!,array.first!).capitalized
                    nutrient.name = string
                }
            }
        }
    }
    
    class func formatNutrientName(nutritions : [Nutrient]) {
        for nutrient in nutritions {
            nutrient.name = nutrient.name.replacingOccurrences(of: "_", with: " ")
            if nutrient.name.contains("Fat")
                || nutrient.name.contains("Water")
                || nutrient.name.contains("Folate")
                || nutrient.name.contains("Sugars") {
                let array = nutrient.name.components(separatedBy: " ")
                if array.count == 2 {
                    let string = String(format:"%@ %@",array.last!,array.first!).capitalized
                    nutrient.name = string
                }
            }
        }
    }
    
    class func formatNutrientInfo(nutritionName : String) -> String {
        var nutrientName : String = ""
        nutrientName = nutritionName.replacingOccurrences(of: "_", with: " ")
        if nutrientName.contains("Fat") || nutrientName.contains("Water") {
            let array = nutrientName.components(separatedBy: " ")
            if array.count == 2 {
                let string = String(format:"%@ %@",array.last!,array.first!).capitalized
                nutrientName = string
            }
        }
        return nutrientName
    }

}

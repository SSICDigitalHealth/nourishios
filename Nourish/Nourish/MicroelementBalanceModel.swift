//
//  MicroelementBalanceModel.swift
//  Nourish
//
//  Created by Vlad Birukov on 14.06.17.
//  Copyright © 2017 SSIC. All rights reserved.
//

import Foundation

class MicroelementBalanceModel: NSObject {
    var arrMicroelementName : [MicroelementNameModel]?
}

class MicroelementNameModel {
    var type: String = ""
    var target:Double = 0.0
    var arrMicroelement : [ElementModel]?
}

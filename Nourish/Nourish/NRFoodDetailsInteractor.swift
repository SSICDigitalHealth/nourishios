//
//  NRFoodDetailsInteractor.swift
//  Nourish
//
//  Created by Vino (Vinodha) Sundaramoorthy on 11/22/16.
//  Copyright © 2016 SSIC. All rights reserved.
//

import UIKit
import RxSwift

class NRFoodDetailsInteractor: NSObject {
    let noomRepo = NoomRepository.shared
    let routine = NMFood_to_MealRecordRoutine()
    let userProfileRepo = UserRepository.shared
    let mapper = MealRecord_to_MealRecordCache()
    let mealRepo = MealHistoryRepository.shared
    let realmRepo = FoodRecordRepository.shared
    let foodCardCacheRepository = FoodCardCacheRepository()
    
    func fetchFoodRecordDetails() -> MealRecord {
        let mealRecord = MealRecord()
        mealRecord.name = "Chicken Salad"
        mealRecord.unit = "Cup"
        mealRecord.amount = 1
        return mealRecord
    }
    
    
    
/*    func fetchFoodCardDetails(foodSearchModel : foodSearchModel) -> Observable<FoodCardDetails> {
        let cache = self.foodCardCacheRepository.fetchCacheFor(foodId: foodSearchModel.foodId)
        if cache != nil {
            return Observable.just(cache!)
        } else {
            let nutr = self.fetchNutrientDetailsForModel(foodModel: foodSearchModel)
            let servingType = self.servingTypesFrom(model: foodSearchModel)
            let zipValue = Observable.zip(nutr, servingType, resultSelector : { nutrArray, servingArray -> FoodCardDetails in
                let details = FoodCardDetails(nutrients : nutrArray, servingTypes : servingArray)
                self.foodCardCacheRepository.storeCache(foodId: foodSearchModel.foodId, cache: details)
                return details
            })
            return zipValue
        }
    }
    
    
    func fetchFoodRecordMeal(foodSearchModel : foodSearchModel, occasion : Ocasion?, date : Date) -> Observable<MealRecord> {
        return self.noomRepo.foodFrom(uid: foodSearchModel.foodId).flatMap { food -> Observable<MealRecord> in
            let meal = self.routine.noomFoodToMeal(noomFood: food, model: foodSearchModel, occasion: occasion, date: date)
            return Observable.just(meal)
        }
    }
    
    func servingTypesFrom(model : foodSearchModel) -> Observable<[String]> {
        return noomRepo.foodFrom(uid: model.foodId).flatMap({food -> Observable<[String]> in
            var array : [String] = []
            let units = food.preciseUnits
            for unit in units! {
                array.append(unit.name)
            }
            return Observable.just(array)
        })
    }*/
    
    
    /*func fetchNutrientDetailsForModel(foodModel : foodSearchModel) -> Observable<[Nutrient]> {
        //Fetch nutrient details based on the meal record
        let nutr = routine.foodUnitFrom(model: foodModel).flatMap { foodUnits -> Observable<[Nutrient]> in
            for unit in foodUnits {
                if unit.name == foodModel.unit {
                    return Observable.just(self.routine.noomNutrienstsToNutrients(foodUnit: unit, amount: foodModel.amount, calories : Double(foodModel.calories)!))
                }
            }; return Observable.just([])
        }
        return nutr
    }*/
    
    func recountNutritionDetailsForModel(foodModel : foodSearchModel, oldAmount : Double, nutrArray : [Nutrient]) -> Observable<[Nutrient]> {
        let koef = foodModel.amount / oldAmount
        var arrayToReturn : [Nutrient] = []
        
        for nutr in nutrArray {
            nutr.representationValue = nutr.representationValue * koef
            arrayToReturn.append(nutr)
        }
        
        return Observable.just(arrayToReturn)
    }
    
    func recountCaloriesGramsFor(foodModel : foodSearchModel, oldAmount : Double) -> Observable<foodSearchModel> {
        let koef = foodModel.amount / oldAmount
        
        var model = foodSearchModel()
        model = foodModel
        
        let calories = Double(model.calories)! * koef
        model.calories = String(format : "%.2f",calories)
        
        if model.grams != nil {
            model.grams = model.grams! * koef
        }
        return Observable.just(model)
        
    }
    
//    func fetchNutrientDetailsForRecomFood(foodModel : foodSearchModel) -> Observable<[Nutrient]> {
//        //self.mealRepo.
//    }
    
    
 /*   func fetchCaloriesForModel(foodModel : foodSearchModel) -> Observable<Double> {
        let unit = routine.foodUnitFrom(model: foodModel).flatMap {foodUnits -> Observable<Double> in
            for unit in foodUnits {
                if unit.name == foodModel.unit {
                    var value = 0.0
                    
                    let number = foodModel.amount as NSNumber
                    let calories = unit.calories(forAmount:number)
                    if calories != nil {
                        value = Double(calories!)
                    }
                    return Observable.just(value)
                }
            }; return Observable.just(0.0)
        }
        return unit
    }
    
    func fetchGramsForModel(foodmodel : foodSearchModel) -> Observable<foodSearchModel> {
        let seq = self.fetchFoodRecordMeal(foodSearchModel: foodmodel, occasion: nil, date: Date()).flatMap { record -> Observable<foodSearchModel> in
                let grams = record.grams
                var model = foodmodel
                model.grams = grams
                return Observable.just(model)
            }
        
        return seq
    }
    
    func addToDiary(model : foodSearchModel, occasion : Ocasion, date : Date) -> Observable<String> {
        let obs = self.fetchFoodRecordMeal(foodSearchModel: model, occasion: occasion, date: date).flatMap({ mealRecord in
            return self.mealRepo.uploadMealAndGetFoodID(mealRecord: mealRecord)
        })
        return obs
    }
    
    func addToDiaryAndGetFoodID(model : foodSearchModel, occasion : Ocasion, date : Date) -> Observable<String> {
        let obs = self.fetchFoodRecordMeal(foodSearchModel: model, occasion: occasion, date: date).flatMap { mealrecord in
            return self.mealRepo.uploadMealAndGetFoodID(mealRecord: mealrecord)
        }
        return obs
    }*/
    
    func addToDiaryFromRecommendation(model : foodSearchModel, date : Date,occasion : Ocasion?) -> Observable<Double> {
        
        let obj = self.routine.recomendationFoodToMeal(model: model, date: date ,occasion : occasion)
        let obs = self.mealRepo.uploadMeal(mealRecord: obj)
        return obs
    }
    
    func addFnddsToDiaryAndGetFoodID(model : foodSearchModel, occasion : Ocasion, date : Date) -> Observable<String> {
        let obj = self.routine.recomendationFoodToMeal(model: model, date: date ,occasion : occasion)
        return self.mealRepo.oldUploadMeal(mealRecord: obj)
    }
    
    func changeFood(model : foodSearchModel) -> Observable<Double> {
        let obs = self.mealRepo.changeMealRecord(foodsearcMeal: model)
        return obs
    }
    
    func getisFavourite(model : foodSearchModel) -> Bool {
        return self.realmRepo.isFavourite(foodItem: model)
    }
    
    func toggleFavourite(model : foodSearchModel) -> Observable<Bool> {
        return self.realmRepo.toggleFavourite(foodItem: model)
    }
    
    func addToCounted(model : foodSearchModel) {
        self.realmRepo.addToCounted(model: model)
    }

}

//
//  ActivityCaloricProgressCellPresenter.swift
//  Nourish
//
//  Created by Andrey Zmushko on 8/26/17.
//  Copyright © 2017 SSIC. All rights reserved.
//

import UIKit
import RxSwift

protocol ActivityCaloricProgressCellDelegate {
    func didPressButton()
}

class ActivityCaloricProgressCellPresenter: ActivityBaseCellPresenter, ActivityCaloricProgressCellDelegate {
    let interactor = CaloricProgressInteractor()
    let mapperObjectCaloricProgress = CaloricProgressModel_to_CaloricProgressViewModel()
    var caloricProgressViewModel = CaloricProgressViewModel()
    var activityLevel: String = ""
    var maxconsumedCal: Double = 0.0
    var maxBurnedCal: Double = 0.0
    
    let eatColor = NRColorUtility.hexStringToUIColor(hex: "70c397")
    let activityColor = NRColorUtility.appBackgroundColor()

    
    private func getInformation(cell: CaloricProgressTableViewCell ,startDate: Date, endDate: Date) {
        cell.baseView.viewWillAppear(true)
        self.subscribtions.append(self.interactor.execute(startDate: startDate, endDate: endDate).observeOn(MainScheduler.instance).subscribe(onNext:{ [unowned self] caloricProgress in
            self.caloricProgressViewModel = self.mapperObjectCaloricProgress.transform(model: caloricProgress)
        }, onError: {error in
            DispatchQueue.main.async {
                cell.baseView.parseError(error: error, completion: nil)
            }
        }, onCompleted: {
            self.setupViewForData(cell: cell)
            self.reloadTable(cell: cell)
            cell.baseView.stopActivityAnimation()

        }, onDisposed: {
        }))
    }
    
    func didPressButton() {
        if let delegate = self.delegate {
            delegate.actionTap(sender: self)
        }
    }
    
    
    override func fetchModel(For basecell: ActivityBaseCell) {
        super.fetchModel(For: basecell)
        
        let cell = basecell as! CaloricProgressTableViewCell
        cell.delegate = self
        if let delegate = self.delegate {
            let date = delegate.fetchEpoch()
            self.getInformation(cell: cell, startDate: date.startDate, endDate: date.endDate)
        }
        
    }
    
    private func setupViewForData (cell: CaloricProgressTableViewCell) {
        if self.caloricProgressViewModel.weeklyPlan != nil && self.caloricProgressViewModel.userGoal != nil  {
            if self.caloricProgressViewModel.userGoal == .LooseWeight {
                
                let weeklyPlan = self.caloricProgressViewModel.isMetric ? self.caloricProgressViewModel.weeklyPlan : NRConversionUtility.kiloToPounds(valueInKilo: self.caloricProgressViewModel.weeklyPlan!)
                let weeklyUnit = self.caloricProgressViewModel.isMetric ? "kg" : "lbs"
                
                cell.caloricProgressMessage.text = String(format : "You are on a %.2f %@/week weight loss plan",  weeklyPlan!, weeklyUnit)
            } else {
                cell.caloricProgressMessage.text = ""
            }
        }
    
        if let caloricData = caloricProgressViewModel.caloricState?[0] {
            if let activityLevel = self.caloricProgressViewModel.activityLevel {
                self.activityLevel = activityLevel.description
                self.maxBurnedCal = caloricData.cal.maxBurnedCal
                self.maxconsumedCal = caloricData.cal.maxConsumedCal
            }
            
            let overConsumption = self.overConsumed()
            
            cell.titleStatus.text = overConsumption ? "Needs Improvement" : "Keep it up!"
        
            cell.detailStatusMessage.text = overConsumption ? String(format:"Your daily budget was %0.f kcal\nYou exceeded by %0.f kcal", caloricData.cal.maxConsumedCal,caloricData.cal.userConsumedCal - caloricData.cal.maxConsumedCal)  : "You are doing great!"
            
            cell.activityCaloriesChart.setupWith(consumedCalories: caloricData.cal.userBurnedCal, targetCalories: caloricData.cal.maxBurnedCal, consumedColor: self.activityColor)
            cell.eatCaloriesChart.setupWith(consumedCalories: caloricData.cal.userConsumedCal, targetCalories: caloricData.cal.maxConsumedCal, consumedColor: self.eatColor)
            
            cell.consumedEatCalories.text = String(format: "%0.f kcal", caloricData.cal.userConsumedCal)
            cell.consumedActivityCalories.text = String(format: "%0.f kcal", caloricData.cal.userBurnedCal)
            cell.targetActivityCalories.text = String(format: "%0.f kcal", caloricData.cal.maxBurnedCal)
            cell.targetEatCalories.text = String(format: "%0.f kcal", caloricData.cal.maxConsumedCal)
        }
    }

    private func overConsumed() -> Bool {
        if self.caloricProgressViewModel.caloricState![0].cal.userConsumedCal > self.caloricProgressViewModel.caloricState![0].cal.maxConsumedCal {
            return true
        } else {
            return false
        }
    }
}

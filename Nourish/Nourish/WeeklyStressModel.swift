//
//  WeeklyStressModel.swift
//  Nourish
//
//  Created by Gena Mironchyk on 1/19/17.
//  Copyright © 2017 SSIC. All rights reserved.
//

import UIKit

class WeeklyStressModel: NSObject{
    var avgBPM : Double?
    var maxBPM : Double?
    var minBPM : Double?
    var weeklyArray : [weeklyStruct]?
}

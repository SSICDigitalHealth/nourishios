//
//  RecepiesTableViewCell.swift
//  Nourish
//
//  Created by Vlad Birukov on 10.10.17.
//  Copyright © 2017 SSIC. All rights reserved.
//

import UIKit

class RecepiesTableViewCell: UITableViewCell {
    @IBOutlet weak var indexInstruction: UILabel!
    @IBOutlet weak var intstruction: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func setupWith(instructionElement: String, index: Int) {
        self.intstruction.text = String(format: "%@", instructionElement)
        self.indexInstruction.text = String(format: "%d.", index)
        
        self.intstruction.setLineSpacing(spacing: 2)
    }
}

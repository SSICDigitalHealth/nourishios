//
//  UserActivityScoreView.swift
//  Nourish
//
//  Created by Vlad Birukov on 06.06.17.
//  Copyright © 2017 SSIC. All rights reserved.
//

import UIKit

protocol UserActivityScoreProtocol: ProgressViewProtocol {
    func setupWithModel(model: UserActivityScoreViewModel)
    func config() -> ProgressConfig?
}

class UserActivityScoreView: ProgressView, UserActivityScoreProtocol {

    @IBOutlet weak var activityScoreView: UIView!
    @IBOutlet weak var userActiviteScore: UILabel!
    @IBOutlet weak var userActivityScorePt: UILabel!
    @IBOutlet weak var messageLabel: UILabel!
    @IBOutlet weak var infoIcon: UIImageView!
    @IBOutlet weak var infoIconSpace: NSLayoutConstraint!
    @IBOutlet weak var scoreDot: ProgressReportScoreDotView!
    @IBOutlet weak var dotCenterXConstraint: NSLayoutConstraint!
    @IBOutlet weak var dotBottomConstraint: NSLayoutConstraint!
    @IBOutlet weak var dotLabelCenterXConstraint: NSLayoutConstraint!
    @IBOutlet weak var starLabel: UILabel!
    @IBOutlet weak var scoreChartView: ScoreChartView!
    var presenter : UserActivityScorePresenter!
    
    var scoreActivity: Int?
    var scoreHistory = [Int]()
    var needChart = false
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.presenter.userActivityScoreView = nil
        self.presenter = nil
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }

    func loadScoreNib(nibName: String) {
        activityScoreView =  UINib(nibName: nibName, bundle: nil).instantiate(withOwner: self, options: nil)[0] as! UIView
        loadContent(contentView: activityScoreView)
    }
    
    func drawChart() {
        self.starLabel.text = "\u{2605}"
        self.scoreChartView.setupWith(History: self.scoreHistory)
    }
    
    func moveScoreArrow() {
        let minHeight: CGFloat   = 20.0
        let maxHeight: CGFloat   = 68.0
        let width: CGFloat       = activityScoreView.frame.size.width
        let height: CGFloat      = activityScoreView.frame.size.height
        
        if scoreActivity == nil {
            scoreActivity = 0
        }
        
        if var score = scoreActivity {
            let minScore = 0
            let maxScore = 100
            if score < minScore {
                score = minScore
            }
            else if score > maxScore {
                score = maxScore
            }
            let step = (width) / 100
            dotCenterXConstraint.constant = step * CGFloat(score - 50)
            
            userActiviteScore.sizeToFit()
            let minGap = userActiviteScore.frame.size.width / 2 + 2
            if width / 2 - abs(dotCenterXConstraint.constant) < minGap {
                if dotCenterXConstraint.constant < 0 {
                    dotLabelCenterXConstraint.constant = minGap - width / 2
                }
                else {
                    dotLabelCenterXConstraint.constant = width / 2 - minGap
                }
            }
            else {
                dotLabelCenterXConstraint.constant = dotCenterXConstraint.constant
            }
            let scale = CGFloat(score) * 0.01
            dotBottomConstraint.constant = height - (height - ((width * scale) * maxHeight)/width - minHeight)
        }
    }
    
    private func formatedFrom(_ date: Date, format: String) -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.setLocalizedDateFormatFromTemplate(format)
        
        return dateFormatter.string(from: date)
    }
    
    func loadReportStyle() {
        if let config = self.progressConfig {
            loadScoreNib(nibName: "UserActivityScoreReportView")

            if self.isToday() {
                config.type = .progress
            }
            else {
                config.type = .report
            }
            

            self.loadThemeWithHeader()

            activityScoreView.layoutIfNeeded()
            needChart = true
            updateScore()
            moveScoreArrow()

            setNeedsDisplay()
        }
    }

    func loadProgressStyle() {
        if !self.isToday() {
            self.loadReportStyle()
        }
        needChart = false
        
        loadScoreNib(nibName: "UserActivityScoreView")
//        loadGreenThemeWith(header: "Today",
//                           leftArrow: true,
//                           rightArrow: !self.isToday(),
//                           leftHook: nil,
//                           rightHook: nil)
        self.loadThemeNoHeaderProgress()
     
        updateScore()
        
        if let score = scoreActivity {
            if score > 0 && (infoIcon != nil) {
                infoIcon.imageNamedWithTint(named: "info", tintColor: UIColor.white.withAlphaComponent(0.35))
                if score < 10 {
                    infoIconSpace.constant = -19.0;
                }
                else if score < 100 {
                    infoIconSpace.constant = -5.0;
                }
                else {
                    infoIconSpace.constant = 5.0;
                }
            }
        }
    }
    
    override func renderView(_ config: ProgressConfig) {
        super.renderView(config)
        var newHeight:CGFloat = 150.0 - 24.0
        if self.progressConfig?.period == .daily && self.progressConfig?.type == .progress && self.isToday() {
            loadProgressStyle()
            newHeight = 108.0
        }
        else {
            loadReportStyle()
        }
        if let hook = self.updateCardHeightHook {
            hook(newHeight, .score)
        }
    }

    func loadCardStyle(_ period: ProgressPeriod) {
        if period == .daily {
            loadScoreNib(nibName: "UserActivityScoreView")
            dailyDetailScore()
        }
        else {
            loadScoreNib(nibName: "UserActivityScoreReportView")
            loadThemeNoHeader()
            activityScoreView.layoutIfNeeded()
            needChart = true
            updateScore()
            moveScoreArrow()
            setNeedsDisplay()
        }
    }

    override func viewWillAppear(_ animated: Bool) {
        self.presenter = UserActivityScorePresenter()
        self.basePresenter = self.presenter
        self.presenter.userActivityScoreView = self
        self.userActiviteScore.text = ""
        super.viewWillAppear(animated)
        self.removeActivityIndicator()
    }

    func updateScore() {
        if let config = self.progressConfig {
            if config.period == .daily && self.isToday() {
                self.userActiviteScore?.isHidden = false
                self.userActivityScorePt?.isHidden = false
                self.messageLabel?.isHidden = true
            }
        }
        if let score = scoreActivity {
            if let label = userActiviteScore {
                label.text = String(format: "%d", score)
            }
        }
        if let chart = self.scoreChartView {
            chart.setupWith(History: self.scoreHistory)
        }
    }
    
    private func showMessage() {
        if let config = self.progressConfig {
            if config.period == .daily && self.isToday() {
                self.userActiviteScore?.isHidden = true
                self.userActivityScorePt?.isHidden = true
                self.messageLabel?.isHidden = false
            }
        }
    }
    
    func reloadData(model: UserActivityScoreViewModel) {
        self.scoreActivity = Int(model.scoreActivity.roundToPlaces(places: 0))
        self.scoreHistory = model.scoreHistory.map{ (history: Double) -> Int in
            return Int(history)
        }
        if self.scoreActivity == 0 && model.isDairyEmpty == true {
            self.showMessage()
        }
        else {
            self.updateScore()
        }
        
        if needChart {
            self.moveScoreArrow()
        }
    }
    
    func setupWithModel(model: UserActivityScoreViewModel) {
        self.reloadData(model: model)
    }
    
    override func gestureTapAction() {
        if let hook = tapHook {
            EventLogger.logClickedOnNutritionScore()
            hook(.score)
        }
    }
    
    override func draw(_ rect: CGRect) {
        super.draw(rect)
        if needChart {
            drawChart()
        }
    }
}

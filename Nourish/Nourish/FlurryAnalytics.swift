//
//  FlurryAnalytics.swift
//  Nourish
//
//  Created by Gena Mironchyk on 5/11/17.
//  Copyright © 2017 SSIC. All rights reserved.
//

import UIKit
import Flurry_iOS_SDK
#if !OPTIFASTVERSION
let FLURRY_API_KEY = "MS2W2GB4W4QKHY53PXM4"
#else
let FLURRY_API_KEY = "56TCW72Y8YSFHMBYBG3X"
#endif
class FlurryAnalytics: NSObject,AnalyticsProtocol {
    private var userDict = [String : Any]()
    
    func initializeWith(userMeta : [String : Any]) {
        Flurry.startSession(FLURRY_API_KEY, with: FlurrySessionBuilder
            .init()
            .withCrashReporting(true)
            .withLogLevel(FlurryLogLevelCriticalOnly))
        self.userDict = userMeta
    }
    
    func logEventWith(name: String) {
        Flurry.logEvent(name, withParameters: self.userDict)
    }

}

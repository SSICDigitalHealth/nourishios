//
//  MIcroelement.swift
//  Nourish
//
//  Created by Vlad Birukov on 28.06.17.
//  Copyright © 2017 SSIC. All rights reserved.
//

import Foundation
enum NutritionIntdicator : Int {
    case good = 1
    case deficiency
    static func intFrom(string : String) -> Int {
        switch string {
        case "deficiency" : return self.deficiency.rawValue
        case "good" : return self.good.rawValue
        default : return self.deficiency.rawValue
        
        }
    }
}


class MicroelementInformation: NSObject {
    var numDays: Double = 0.0
    var consumedMicroelement: Double = 0.0
    var idNameMicroelement = ""
    var lowerLimit: Double?
    var upperLimit: Double?
    var unit = ""
    var message = ""
    var contributors = [FoodMicroelemenInformation]()
    var nutritionIndicator : NutritionIntdicator = .deficiency
    var historyDate : Date? = nil
    var max : Double {
        get {
            if self.lowerLimit != nil && self.upperLimit != nil {
                if self.consumedMicroelement < self.lowerLimit! {
                    return self.lowerLimit!
                } else {
                    return self.upperLimit!
                }
            }else if self.lowerLimit == nil && self.upperLimit == nil {
                return self.consumedMicroelement
            }else if self.lowerLimit == nil {
                return self.upperLimit!
            } else {
                if self.consumedMicroelement < self.lowerLimit! {
                    return self.lowerLimit!
                } else {
                    return self.consumedMicroelement
                }
            }
            
        }
    }
    
    var graphRepresentation : Double {
        get {
            return self.consumedMicroelement / self.max
        }
    }

    
}

class FoodMicroelemenInformation: NSObject {
    var nameFood = ""
    var consumed: Double = 0.0
    var quality: Double = 0.0
    var unit = ""

}

//
//  UserActivityScoreInteractor.swift
//  Nourish
//
//  Created by Vlad Birukov on 06.06.17.
//  Copyright © 2017 SSIC. All rights reserved.
//

import Foundation
import RxSwift

class UserActivityScoreInteractor {
    
    let repository = NestleProgressRepository.shared
    let mealRepo = MealHistoryRepository.shared
    
    func execute(startDate: Date, endDate: Date) -> Observable <UserActivityScoreModel> {
        if Calendar.current.isDateInToday(startDate) {
            let history = self.mealRepo.getFoodFor(date: startDate, onlyCache: true)
            let progress = self.repository.getProgressFor(startDate: startDate, endDate: endDate, returnHistory: true)
            
            let zip = Observable.zip(history, progress, resultSelector: {historyFetched , progressFetched -> UserActivityScoreModel in
                let isDiaryEmpty = historyFetched.count == 0
                let score = progressFetched.userScore
                score.isDairyEmpty = isDiaryEmpty
                return score
            })
            return zip
        } else {
            return self.repository.getProgressFor(startDate: startDate, endDate: endDate, returnHistory: true).flatMap({progress -> Observable<UserActivityScoreModel> in
                return Observable.just(progress.userScore)
            })
        }
    }

}

//
//  WeeklyStressProtocol.swift
//  Nourish
//
//  Created by Gena Mironchyk on 1/23/17.
//  Copyright © 2017 SSIC. All rights reserved.
//

protocol WeeklyStressProtocol {
    func setChartData(data : ScatterChartData, xTitles : [String],maxBpm : Double, minBpm : Double, avgBpm : Double)
    func updateTableData()
}

//
//  NRRecommendation.swift
//  Nourish
//
//  Created by Nova on 1/11/17.
//  Copyright © 2017 SSIC. All rights reserved.
//

import Foundation
import UIKit

typealias recommendationListStruct = (type : recommendationType, recommendation:[recommendationStruct])

enum recommendationType {
    case tips
    case activity
    case lunch
    case breakfast
    case dinner
    case snack
    
    static func stringFromEnum(type : recommendationType) -> String {
        switch type {
        case .tips:
            return "Tip"
        case .activity:
            return "Activity"
        case .lunch:
            return "Lunch"
        case .breakfast:
            return "Breakfast"
        case .dinner:
            return "Dinner"
        case .snack:
            return "Snacks"
        }
    }
    
    static func imageFromEnum(type : recommendationType) -> UIImage {
        switch type {
        case .tips:
            return UIImage(named: "rec_icon_tip")!
        case .activity:
            return UIImage(named: "rec_icon_activity")!
        case .lunch ,.snack,.dinner,.breakfast:
            return UIImage(named: "rec_icon_food")!
        }
    }
    
    static func ocasionFromType(type : recommendationType) -> Ocasion {
        switch type {
        case .tips:
            return .breakfast
        case .activity:
            return .breakfast
        case .lunch:
            return .lunch
        case .breakfast:
            return .breakfast
        case .dinner:
            return .dinner
        case .snack:
            return .snacks
        }
    }
}

struct recommendationStruct {
    var description : String
    var title : String = ""
    var subTitle : String = ""
    var type : recommendationType
    var foodModel : foodSearchModel? = nil
}

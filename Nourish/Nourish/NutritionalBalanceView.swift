//
//  NutritionalBalanceView.swift
//  Nourish
//
//  Created by Vlad Birukov on 07.06.17.
//  Copyright © 2017 SSIC. All rights reserved.
//

import UIKit

protocol NutritionalBalanceProtocol : BaseViewProtocol {
    func setupWith(Model model: AnyObject)
    func config() -> ProgressConfig?
}

class NutritionalBalanceView: ProgressView, NutritionalBalanceProtocol {
    
    @IBOutlet weak var fruitsNutritional: UILabel!
    @IBOutlet weak var vegsNutritional: UILabel!
    @IBOutlet weak var grainsNutritional: UILabel!
    @IBOutlet weak var proteinNutritional: UILabel!
    @IBOutlet weak var dairyNutritional: UILabel!
    @IBOutlet var nutritionBalanceView: UIView!
    @IBOutlet weak var nutritionChartView: NutritionalChartView!
    @IBOutlet weak var chartWidth: NSLayoutConstraint!
    @IBOutlet weak var chartHeight: NSLayoutConstraint!
    
    var presenter: NutritionalBalancePresenter!

    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.presenter.userNutritionBalanceView = nil
        self.presenter = nil
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.presenter = NutritionalBalancePresenter()
        if let config = self.progressConfig {
            self.renderView(config)
        }
        self.basePresenter = self.presenter
        self.presenter.userNutritionBalanceView = self
        super.viewWillAppear(animated)
        self.removeActivityIndicator()
    }
    
    override func renderView(_ config: ProgressConfig) {
        super.renderView(config)
        self.nutritionBalanceView =  UINib(nibName: "NutritionalBalanceView", bundle: nil).instantiate(withOwner: self, options: nil)[0] as! UIView
        self.loadContent(contentView: nutritionBalanceView)
        self.loadWhiteThemeWith(header: "Nutritional balance")
        self.nutritionChartView.layer.cornerRadius = 92.0
        self.reloadData()
    }
    
    override func reloadData() {
        if self.nutritionBalanceView == nil {
            return
        }
        var maxChartRadius: CGFloat = 92.0
        func calcMaxChartSize(rect: CGRect) -> CGFloat {
            let newChartRadius = UIScreen.main.bounds.width / 2 - rect.size.width - 16.0
            return newChartRadius < maxChartRadius ? newChartRadius : maxChartRadius
        }
        var chartData: NutritionalChartView.Data? = nil
        if let model = self.model as? NutritionalBalanceViewModel {
            if let data = model.data {
                chartData = NutritionalChartView.Data()
                for element in data {
                    let consumed = element.nutrition.consumedNutritional
                    let target = element.nutrition.targetNutritional
                    var label = ""
                    if element.nutrition.unit == "cup" {
                     label = String(format: "%@/%@ %@", NRFormatterUtility.doubleToString(num: NRFormatterUtility.roundCupElements(number: consumed)), NRFormatterUtility.doubleToString(num: NRFormatterUtility.roundCupElements(number: target)), element.nutrition.unit)
                    } else {
                         label = String(format: "%@/%@ %@", NRFormatterUtility.numberToStringNutrition(number: consumed) , NRFormatterUtility.numberToStringNutrition(number: target), element.nutrition.unit)
                    }
                    switch element.type {
                    case .dairy:
                        self.dairyNutritional.text = label
                        chartData?.append((.dairy, consumed, target))
                    case .protein:
                        self.proteinNutritional.text = label
                        chartData?.append((.protein, consumed, target))
                        self.proteinNutritional.sizeToFit()
                        maxChartRadius = calcMaxChartSize(rect: self.proteinNutritional.frame)
                    case .grains:
                        self.grainsNutritional.text = label
                        chartData?.append((.grains, consumed, target))
                        self.grainsNutritional.sizeToFit()
                        maxChartRadius = calcMaxChartSize(rect: self.grainsNutritional.frame)
                    case .vegs:
                        self.vegsNutritional.text = label
                        chartData?.append((.vegs, consumed, target))
                        self.vegsNutritional.sizeToFit()
                        maxChartRadius = calcMaxChartSize(rect: self.vegsNutritional.frame)
                    case .fruits:
                        self.fruitsNutritional.text = label
                        chartData?.append((.fruits, consumed, target))
                        self.fruitsNutritional.sizeToFit()
                        maxChartRadius = calcMaxChartSize(rect: self.fruitsNutritional.frame)
                    }
                }
                self.chartWidth.constant = maxChartRadius * 2
                self.chartHeight.constant = self.chartWidth.constant
                self.nutritionChartView.layer.cornerRadius = maxChartRadius
                self.nutritionChartView.radiusTarget = maxChartRadius * (1 - 20.0 / 92.0)
                self.nutritionChartView.setupWith(Data: chartData!)
            }
        }
    }
    
    override func setupWith(Model model: AnyObject) {
        super.setupWith(Model: model)
        self.reloadData()
    }
    
    override func gestureTapAction() {
        if let hook = tapHook {
            EventLogger.logClickedOnNutritionalBalance()
            hook(.nutrition)
        }
    }
}

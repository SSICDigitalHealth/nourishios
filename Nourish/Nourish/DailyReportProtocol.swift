//
//  DailyReportProtocol.swift
//  Nourish
//
//  Created by Vino (Vinodha) Sundaramoorthy on 2/8/17.
//  Copyright © 2017 SSIC. All rights reserved.
//

import UIKit

protocol  DailyReportProtocol {
    func setReport(report:DailyReportModel)
    func getReportDate() -> Date
}

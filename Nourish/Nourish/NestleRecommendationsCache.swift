//
//  NestleRecommendationsCache.swift
//  Nourish
//
//  Created by Gena Mironchyk on 9/20/17.
//  Copyright © 2017 SSIC. All rights reserved.
//

import UIKit
import RealmSwift
class NestleRecommendationsCache: Object {
    dynamic var startDate : Date?
    dynamic var endDate : Date?
    dynamic var caption : String? = nil
    let meals = List<FoodSearchCacheModel>()
    let tips = List<NestleCacheTipsModel>()
    let dataRecipies = List<RecipiesCacheModel>()
}

class FoodSearchCacheModel: Object {
    dynamic var amount: Double = 1.0
    dynamic var displayUnit = ""
    dynamic var unit = ""
    dynamic var foodId = ""
    dynamic var mealIdString = ""
    dynamic var foodTitle = ""
    let grams = RealmOptional<Double>()
    dynamic var caloriesPerGram: Double = 0.0
    dynamic var calories = ""
}

class NestleCacheTipsModel: Object {
    dynamic var componentId: String? = nil
    dynamic var indicator: String? = nil
    dynamic var tipTitle: String? = nil
    dynamic var tipType: String? = nil
    let messages = List<RealmString>()
}

class RecipiesCacheModel: Object {
    dynamic var foodId = ""
    dynamic var amount: Double = 0.0
    dynamic var nameFood = ""
    dynamic var groupId: String? = nil
    dynamic var pathImage = ""
    dynamic var numberCal: Double = 0.0
    dynamic var grams: Double = 0.0
    dynamic var unit = ""
    let ingredient = List<RealmString>()
    let instruction = List<RealmString>()
    dynamic var isFavorite = false
}

//
//  CaloricProgressTableViewCell.swift
//  Nourish
//
//  Created by Vlad Birukov on 31.08.17.
//  Copyright © 2017 SSIC. All rights reserved.
//

import UIKit

class CaloricProgressTableViewCell: ActivityBaseCell {
    @IBOutlet weak var caloricProgressMessage: UILabel!
    @IBOutlet weak var consumedEatCalories: UILabel!
    @IBOutlet weak var targetEatCalories: UILabel!
    @IBOutlet weak var eatCaloriesChart: CaloricChartView!
    @IBOutlet weak var consumedActivityCalories: UILabel!
    @IBOutlet weak var targetActivityCalories: UILabel!
    @IBOutlet weak var activityCaloriesChart: CaloricChartView!
    @IBOutlet weak var titleStatus: UILabel!
    @IBOutlet weak var detailStatusMessage: UILabel!
    @IBOutlet weak var buttonInformation: UIButton!
    
    @IBOutlet weak var baseView: BaseView!
    
    var delegate: ActivityCaloricProgressCellDelegate?

    override func awakeFromNib() {
        super.awakeFromNib()
        self.activityCaloriesChart.backgroundColor = NRColorUtility.caloriesGraphBackgroundColor()
        self.consumedActivityCalories.textColor = NRColorUtility.appBackgroundColor()
    }
    
    @IBAction func pressButton(_ sender: Any) {
        self.delegate?.didPressButton()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}

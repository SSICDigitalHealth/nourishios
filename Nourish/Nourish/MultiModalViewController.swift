//
//  MultiModalViewController.swift
//  Nourish
//
//  Created by Gena Mironchyk on 5/7/17.
//  Copyright © 2017 SSIC. All rights reserved.
//

import UIKit
import AVFoundation

protocol MultiModalViewControllerProtocol {
    func dismissMultiModalView()
    func dismissMultiModalViewAndShowMealDiary ()
    func dismissMultiModalViewAndShowPhotoSearch()
    func dismissMutliModalViewAndShowVoiceSearch()
}

class MultiModalViewController: BasePresentationViewController, MultiModalViewControllerProtocol {
    @IBOutlet weak var multimodalView : MultiModalView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.baseViews = [self.multimodalView]
        self.multimodalView.viewDidLoad()
        self.multimodalView.presenter.multiModalViewController = self
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func dismissMultiModalView() {
        self.dismiss(animated: false, completion: nil)
    }
    
    func dismissMultiModalViewAndShowMealDiary() {
        NavigationUtility.navigateToFoodDiaryFor(date: Date())
    }
    
    func dismissMultiModalViewAndShowPhotoSearch() {
        if AVCaptureDevice.authorizationStatus(forMediaType: AVMediaTypeVideo) ==  AVAuthorizationStatus.authorized
        {
            self.showCameraSnapController()
        }
        else
        {
            AVCaptureDevice.requestAccess(forMediaType: AVMediaTypeVideo, completionHandler: { (granted :Bool) -> Void in
                if granted == true
                {
                    self.showCameraSnapController()
                }
                else
                {
                    self.showCameraErrorAlert()
                }
            });
        }
        
        
    }
    
    private func showMicrophoneErrorAlert() {
        let alertMessage = "You can re-enable microphone access for Nourish in Settings"
        let alertTitle = "Microphone Access Disabled"
        
        let alert = UIAlertController.init(title: alertTitle, message: alertMessage, preferredStyle: UIAlertControllerStyle.alert)
        let settingsAction = UIAlertAction(title: "Settings", style: UIAlertActionStyle.default, handler: { action in
            UIApplication.shared.openURL(URL(string: UIApplicationOpenSettingsURLString)!)
        })
        
        let dismissAction = UIAlertAction(title: "Cancel", style: UIAlertActionStyle.default, handler: { action in
            self.dismissMultiModalView()
        })
        alert.addAction(dismissAction)
        alert.addAction(settingsAction)
        self.present(alert, animated: true, completion: nil)
    }
    
    private func showCameraErrorAlert() {

        let alertMessage = "You can re-enable camera access for Nourish in Settings"
        let alertTitle = "Camera Access Disabled"
        
        let alert = UIAlertController.init(title: alertTitle, message: alertMessage, preferredStyle: UIAlertControllerStyle.alert)
        let settingsAction = UIAlertAction(title: "Settings", style: UIAlertActionStyle.default, handler: { action in
            UIApplication.shared.openURL(URL(string: UIApplicationOpenSettingsURLString)!)
        })
        
        let dismissAction = UIAlertAction(title: "Cancel", style: UIAlertActionStyle.default, handler: { action in
            self.dismissMultiModalView()
        })
        alert.addAction(dismissAction)
        alert.addAction(settingsAction)
        self.present(alert, animated: true, completion: nil)
        
    }

    
    private func showCameraSnapController() {
        let vc = CameraSnapViewController(nibName : "CameraSnapViewController", bundle : nil)
        vc.date = Date()
        vc.recognitionDelegate = NavigationUtility.chatViewControllerInstance()
        NavigationUtility.presentVC(vc: vc, needDismission: true)
    }
    
    func dismissMutliModalViewAndShowVoiceSearch() {
        
        let mediaType = AVMediaTypeAudio
        let authStatus = AVCaptureDevice.authorizationStatus(forMediaType: mediaType)
        switch authStatus {
        case .authorized:
            self.showSoundHoundViewController()
            break
        case .denied:
            self.showMicrophoneErrorAlert()
            break
            
        case .restricted:
            self.showSoundHoundViewController()
            break
            
        case .notDetermined:
            AVCaptureDevice.requestAccess(forMediaType : mediaType, completionHandler: { granted in
                if granted{
                    self.showSoundHoundViewController()
                } else {
                    self.showMicrophoneErrorAlert()
                }
            })
            
            break
        }
    }

    func showSoundHoundViewController() {
        let vc = VoiceSearchListenerViewController(nibName : "VoiceSearchListenerViewController", bundle : nil)
        vc.date = Date()
        vc.delegateVC = NavigationUtility.chatViewControllerInstance()
        NavigationUtility.presentVC(vc: vc, needDismission: true)
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

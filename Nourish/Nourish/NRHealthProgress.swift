//
//  NRHealthProgress.swift
//  Nourish
//
//  Created by Vino (Vinodha) Sundaramoorthy on 12/1/16.
//  Copyright © 2016 SSIC. All rights reserved.
//

import UIKit

class NRHealthProgress: NSObject {
    var stress : stressLevel = .Undetermined
    var sleep : Double = 0.0
    var activity : Double = 0.0
    var nutritionScore : Double = 0.0
    var calories : Double = 0.0
    var steps : Double = 0.0
    var bmr : Double = 0.0
}

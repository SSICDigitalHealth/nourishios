//
//  OptifastMealPlannerViewModel_to_OptifastMealPlannerModel.swift
//  Optifast
//
//  Created by Gena Mironchyk on 1/11/18.
//  Copyright © 2018 SSIC. All rights reserved.
//

import UIKit

class OptifastMealPlannerViewModel_to_OptifastMealPlannerModel: NSObject {
    func transform(viewModel : OptifastMealPlannerViewModel) -> OptifastMealPlannerModel {
        let model = OptifastMealPlannerModel()
        model.optifastFoods = viewModel.optifastFoods
        model.score = viewModel.score
        model.targetCalories = viewModel.targetCalories
        model.foods = self.modelFrom(viewModels: viewModel.foods)
        model.date = viewModel.date
        if model.foods != nil {
            model.foods!.sort(by: { $0.occasion.rawValue < $1.occasion.rawValue})
        }
        return model
    }
    
    
    private func modelFrom(viewModels : [OptifastMealViewModel]?) -> [OptifastMealModel]? {
        if viewModels != nil {
            var arrayToReturn = [OptifastMealModel]()
            for viewModel in viewModels! {
                let model = OptifastMealModel()
                model.occasion = viewModel.occasion
                model.totalCalories = viewModel.totalCalories
                for ocasionFood in viewModel.ocasionFoods {
                    model.ocasionFoods.append((meal: ocasionFood.meal, mealReciepe : self.recipeModelFrom(viewModel: ocasionFood.mealReciepe)))
                }
                arrayToReturn.append(model)
            }
            return arrayToReturn
        } else {
            return nil
        }
    }
    
    
    private func recipeModelFrom(viewModel : RecipiesViewModel?) -> RecipiesModel? {
        if viewModel != nil {
            let model = RecipiesModel()
            model.nameFood = viewModel!.nameFood
            
            if viewModel?.image != nil {
                model.image = viewModel!.image
            }
            
            if viewModel?.groupId != nil {
                model.groupId = viewModel!.groupId
            }
            model.pathForImage = viewModel!.pathForImage
            model.foodId = viewModel!.foodId
            model.numberCal = viewModel!.numberCal
            model.unit = viewModel!.unit
            model.grams = viewModel!.grams
            model.amount = viewModel!.amount
            model.ingredient = viewModel!.ingredient
            model.instructions = viewModel!.instructions
            model.isFavorite = viewModel!.isFavorite
            
            return model
        }
        return nil
    }
}

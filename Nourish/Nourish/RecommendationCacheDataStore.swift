//
//  RecommendationCacheDataStore.swift
//  Nourish
//
//  Created by Nova on 7/18/17.
//  Copyright © 2017 SSIC. All rights reserved.
//

import UIKit
import RealmSwift

class RecommendationCacheDataStore: NSObject {
    
    func store(data : Data, date : Date, type : RecommendationType) {
        let realm = try! Realm()
        
        let proxyArray = self.fetchRecsFor(type: type, date: date)
        
        if proxyArray.count > 0 {
            try! realm.write {
                realm.delete(proxyArray)
            }
        }
        
        let obj = RecommendationCache()
        obj.date = date
        obj.data = data
        obj.type = type
        
        
        try! realm.write {
            realm.add(obj)
        }
    }
    
    func getTipsFor(date : Date) -> RecommendationCache? {
        
        //let obj = self.fetchRecsFor(type: .recommendationTip, date: date).last
        //print("tips \(obj?.data)")
        return self.fetchRecsFor(type: .recommendationTip, date: date).last
    }
    
    func getRecsFor(date : Date) -> RecommendationCache? {
        //let obj = self.fetchRecsFor(type: .recommendationMeal, date: date).last
         //print("meals \(obj?.data)")
        return self.fetchRecsFor(type: .recommendationMeal, date: date).last
    }
    
    private func fetchRecsFor(type : RecommendationType, date : Date) -> [RecommendationCache] {
        let realm = try! Realm()
        
        let datePredicate = self.datePredicate(date: date)
        let typePredicate = NSPredicate(format : "privateType == %d",type.rawValue)
        
        let results = realm.objects(RecommendationCache.self).filter(datePredicate).filter(typePredicate)
        let array = Array(results)
        print(array, type)
        
        return array
    }
    
    
    private func datePredicate(date : Date) -> NSPredicate {
        let calendar = Calendar.current
        let startOfDay = calendar.startOfDay(for: date)
        
        var comps = DateComponents()
        comps.day = 1
        comps.second = -1
        
        let endDate = calendar.date(byAdding: comps, to: startOfDay)
        let predicate = NSPredicate(format : "date > %@ && date < %@",startOfDay as NSDate, endDate! as NSDate)
        return predicate
        
    }

}

//
//  ProgressViewController.swift
//  Nourish
//
//  Created by Nova on 11/21/16.
//  Copyright © 2016 SSIC. All rights reserved.
//

import UIKit
import RxSwift

class ProgressViewController: BasePresentationViewController {
    @IBOutlet weak var nutritionView : NutritionView!
    @IBOutlet weak var activityView : UserActivityView!
    @IBOutlet weak var weightView : NRWeightProgressView!
    @IBOutlet weak var stressView : NRStressView!
    @IBOutlet weak var healthProgressView : NRHealthProgressView!
    @IBOutlet weak var calorieMeterView : WeightLossMeterView!
    @IBOutlet weak var weightHeightConstraint : NSLayoutConstraint!
    @IBOutlet weak var calorieMeterHeightConstraint : NSLayoutConstraint!

    let userPresenter = UserProfilePresenter()
    let userRepo = UserRepository.shared
    var userProfile : UserProfile!
    
    override func viewWillAppear(_ animated: Bool) {
        
        self.title = kProgressViewTitle
        self.setUpNavigationBarButtons()
        NavigationUtility.showTabBar(animated: false)
        ActivityUtility.getHealthKitPermission(completion: {granted, error in
            if granted == true {
                let _ = self.userRepo.getCurrentUser(policy: .DefaultPolicy).observeOn(MainScheduler.instance).subscribe(onNext:{ [unowned self] userProfile in
                    self.userProfile = userProfile
                    self.testMethods()
                }, onError: {error in
                    LogUtility.logToFile(error)
                }, onCompleted: {
                    
                    if self.userProfile.userGoal == UserGoal.LooseWeight || self.userProfile.userGoal == .MaintainWeight {
                        self.calorieMeterHeightConstraint.constant = 320
                        self.weightHeightConstraint.constant = 400
                        self.weightView.controller = self
                        self.calorieMeterView.controller = self
                        self.baseViews = [self.calorieMeterView,self.healthProgressView,self.weightView,self.nutritionView,self.activityView,self.stressView]
                    } else {
                        self.calorieMeterHeightConstraint.constant = 320
                        self.weightHeightConstraint.constant = 0.0
                        self.baseViews = [self.calorieMeterView,self.healthProgressView,self.nutritionView,self.activityView,self.stressView]
                    }
                    self.nutritionView.controller = self
                    self.activityView.controller = self
                    self.stressView.controller = self
                    super.viewWillAppear(animated)
                        
                }, onDisposed: {})
            }
        })
    
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        NotificationCenter.default.addObserver(self, selector: #selector(updateWeightView), name: NSNotification.Name("Withings weight synchronized"), object: nil)
    }
    
    func updateWeightView(){
        self.weightView.viewWillAppear(true)
    }
        
    func testMethods() {
        let localRoutine = LocalNotificationsRoutine()
        localRoutine.logNotificationDates()
    }
    
    func save() {
        userPresenter.saveCurrentUser()
    }

    func setUpNavigationBarButtons () {
        let titleAttribute = self.simplyfiedBarTitileAttributes()
        navigationItem.title = self.title
        self.navigationController?.navigationBar.isTranslucent = false
        self.navigationController?.navigationBar.titleTextAttributes = titleAttribute
        self.navigationController?.navigationBar.barTintColor = NRColorUtility.symplifiedNavigationColor()
        self.navigationController?.navigationBar.tintColor = NRColorUtility.nourishNavBarFontColor()
    }
    
    func openDailyStressController(){
        let vc = StressViewController(nibName: "StressViewController", bundle: nil)
        self.navigationController?.pushViewController(vc, animated: true)
        NavigationUtility.hideTabBar(animated: false)
    }
    
    func openVCNutritionStats() {
        let vc = NRNutritionViewController(nibName: "NRNutritionViewController", bundle: nil)
        self.navigationController?.pushViewController(vc, animated: false)
        NavigationUtility.hideTabBar(animated: false)
    }
    
    func openVCActivityStats() {
        let vc = ActivityStatsViewController(nibName: "ActivityStatsViewController", bundle: nil)
        self.navigationController?.pushViewController(vc, animated: false)
        NavigationUtility.hideTabBar(animated: false)
    }
    
    func openVCWeightStats() {
        let vc = WeightStatsViewController(nibName: "WeightStatsViewController", bundle: nil)
        self.navigationController?.pushViewController(vc, animated: false)
        NavigationUtility.hideTabBar(animated: false)
    }
    
    func showCalorieMeterInfo(activity:String , budgetCal:Double , budgetActivity:Double) {
        let viewController = WeightLossMeterInfoViewController(nibName: "WeightLossMeterInfoViewController", bundle: nil)
        viewController.activityBudget = budgetActivity
        viewController.activityLevel = activity
        viewController.consumedBudget = budgetCal
        viewController.providesPresentationContextTransitionStyle = true;
        viewController.definesPresentationContext = true;
        viewController.modalPresentationStyle=UIModalPresentationStyle.overCurrentContext
        viewController.view.backgroundColor = UIColor.black.withAlphaComponent(0.3)
        self.present(viewController, animated: true, completion: nil)
    }

}

//
//  NestleProgressRepository.swift
//  Nourish
//
//  Created by Vlad Birukov on 26.06.17.
//  Copyright © 2017 SSIC. All rights reserved.
//

import Foundation
import RxSwift

class NestleProgressRepository: NSObject {
    
    let mapperCacheToObject = ProgressCacheRealmToObject()
    let progressDataStore = NestleProgressDataStore()
    let userRepository = UserRepository.shared
    let jsonToCache = json_to_NestleProgressCache()
    let nestleProgressCacheDataStore = NestleCacheDataStore()
    let fooodCompsRepo = NestleFoodComponentsRepository()
    let calendar = Calendar.current
    
    static let shared = NestleProgressRepository()
    
    func getProgressFor(startDate : Date, endDate: Date, returnHistory : Bool) -> Observable<Progress> {
        return Observable.create{observer in
            let cachedProgress = self.nestleProgressCacheDataStore.getCacheFor(startDate: startDate, endDate: endDate, returnHistory: true)
            if cachedProgress != nil {
                observer.onNext(self.mapperCacheToObject.transform(progressCache: cachedProgress!))
            }
            let balancer = NouriqApiCallBalanser.shared
            let progressAsObs = balancer.getProgressFor(start: startDate, end: endDate, returnHistory: returnHistory)
            let _ = progressAsObs.asObservable().subscribe(onNext: {progress in
                observer.onNext(progress)
                observer.onCompleted()
            }, onError: {error in
                observer.onError(error)
                observer.onCompleted()
            }, onCompleted: {observer.onCompleted()}, onDisposed: {})
            return Disposables.create()
        }
    }
    
    func getReloadedProgress(startDate : Date, endDate : Date, returnHistory : Bool) -> PublishSubject<Progress> {
        let subj = PublishSubject<Progress>()
        
        let _ = self.userRepository.getCurrentUser(policy: .Cached).flatMap { [weak self] userProfile -> Observable<Progress> in
            if let token = NRUserSession.sharedInstance.accessToken {
            if let instance = self {
                let daysBetween = instance.daysBetweenDates(startDate: startDate, endDate: endDate)
                
                return instance.progressDataStore.fetchProgress(token: token, userID: userProfile.userID!, startDate: startDate, endDate: endDate, includingRange: true, rangeDays: daysBetween, returningBreakDown: returnHistory, returningHistory: returnHistory).flatMap{ [weak self]  dictionary -> Observable<Progress> in
                    if let instance = self {
                        let cachedProgress = instance.storeProgressToCacheFor(startDate: startDate, endDate: endDate, progressDictionary: dictionary, returnHistory: returnHistory)
                        return Observable.just(instance.mapperCacheToObject.transform(progressCache: cachedProgress))
                    } else {
                        return Observable.just(Progress())
                    }
                    }
            } else {
                return Observable.just(Progress())
                }
            } else {
                return Observable.just(Progress())
            }
    
            }.subscribe(onNext: {object in
                print(object.userCallories)
                subj.onNext(object)
                subj.onCompleted()
            }, onError: {error in }, onCompleted: {
                let balancer = NouriqApiCallBalanser.shared
                balancer.removeProgress(start: startDate, end: endDate, returnHistory: returnHistory)
            }, onDisposed: {})
        
        return subj
    }
    
    func refetchProgressFor(startDate : Date, endDate : Date, returningHistory : Bool) -> Observable<Progress> {
        return self.userRepository.getCurrentUser(policy: .DefaultPolicy).flatMap{userProfile -> Observable<Progress> in
            let token = NRUserSession.sharedInstance.accessToken
            self.nestleProgressCacheDataStore.deletCacheFor(startDate: startDate, endDate: endDate, returnHistory: returningHistory)
            let daysBetween = self.daysBetweenDates(startDate: Date(), endDate: Date())
            return self.progressDataStore.fetchProgress(token: token!, userID: userProfile.userID!, startDate: startDate, endDate: endDate, includingRange: true, rangeDays: daysBetween, returningBreakDown: returningHistory, returningHistory: returningHistory).flatMap{ dictionary -> Observable<Progress> in
                let cachedProgress = self.storeProgressToCacheFor(startDate: startDate, endDate: endDate, progressDictionary: dictionary, returnHistory: true)
                return Observable.just(self.mapperCacheToObject.transform(progressCache: cachedProgress))
                
            }
            
        }
    }
    
    
    func fetchConsumedCaloriesFor(starDate : Date, endDate : Date) -> Observable<Double> {
        return self.getProgressFor(startDate: starDate, endDate: endDate, returnHistory: true).flatMap{progress -> Observable<Double> in
            let consumedCals = progress.userCallories.consumedCalories
            return Observable.just(consumedCals)
        }
    }
    
    
    func getDetailedWatchoutListFor(startDate : Date, endDate : Date) -> Observable<MicroelementBalanceModel> {
        return self.getProgressFor(startDate: startDate, endDate: endDate, returnHistory: true).flatMap{userProgress -> Observable<MicroelementBalanceModel> in
            var result = [[MicroelementInformation]]()
            if self.calendar.startOfDay(for: startDate) == self.calendar.startOfDay(for: endDate){
                let filters = ["Added_sugars", "Fat_saturated","Sodium"]
                let filtered = userProgress.userMicroelement.filter({filters.contains($0.idNameMicroelement)})
                for obj in filtered {
                    result.append(self.arrayFromId(foodId: obj.idNameMicroelement, elements: [obj]))
                }
            } else {
                result = userProgress.watchoutForToShow
            }
            
            return self.watchoutListFrom(nutrs: result)
        }
    }
    
    
    private func watchoutListFrom(nutrs : [[MicroelementInformation]]) -> Observable<MicroelementBalanceModel> {
        let model = MicroelementBalanceModel()
        model.arrMicroelementName = [MicroelementNameModel]()
        for element in nutrs {
            model.arrMicroelementName?.append(self.watchoutMicroelementNameModelFrom(elements: element))
        }
        
        return Observable.just(model)
    }
    
    private func watchoutMicroelementNameModelFrom(elements : [MicroelementInformation]) -> MicroelementNameModel {
        let microelmentNameModel = MicroelementNameModel()
        
        if let object = elements.first {
            microelmentNameModel.target = object.upperLimit ?? object.lowerLimit ?? 0.0
            let type = MicroelementType.typeFrom(foodId: object.idNameMicroelement)
            microelmentNameModel.type = MicroelementType.description(type: type)
            microelmentNameModel.arrMicroelement = [ElementModel]()
            
            for element in elements {
                for contributor in element.contributors {
                    let elementModel = ElementModel()
                    elementModel.amountElement = contributor.quality
                    elementModel.consumedState = contributor.consumed
                    elementModel.nameElement = contributor.nameFood
                    elementModel.unit = contributor.unit
                    microelmentNameModel.arrMicroelement?.append(elementModel)
                }
            }
        }
        
        return microelmentNameModel
    }
    
    
    
    func getWatchoutListFor(startDate : Date, endDate : Date) -> Observable<MicroelementModel> {
        return self.getProgressFor(startDate: startDate, endDate: endDate, returnHistory: true).flatMap{userProgress -> Observable<MicroelementModel> in
            var result = [[MicroelementInformation]]()
            if self.calendar.startOfDay(for: startDate) == self.calendar.startOfDay(for: endDate){
                let filters = ["Added_sugars", "Fat_saturated","Sodium"]
                let filtered = userProgress.userMicroelement.filter({filters.contains($0.idNameMicroelement)})
                for obj in filtered {
                    result.append(self.arrayFromId(foodId: obj.idNameMicroelement, elements: [obj]))
                }
            } else {
                result = userProgress.watchoutForToShow
            }
            
            return self.watchoutListFrom(nutrs: result, startDate : startDate , endDate : endDate)
            
        }
    }
    
    
    func fetchNutritionalDetailedBalanceModelFor(startDate : Date, endDate : Date) -> Observable<NutritionBalanceModel> {
        return self.getProgressFor(startDate: startDate, endDate: endDate, returnHistory: true).flatMap{ userProgress -> Observable<NutritionBalanceModel> in
            let filters = ["Grain_total", "Fruit_total", "Veg_total", "Prot_total", "Dairy_total"]
            let result : [MicroelementInformation]
            if self.calendar.startOfDay(for: startDate) == self.calendar.startOfDay(for: endDate){
                result =  userProgress.userMicroelement.filter({filters.contains($0.idNameMicroelement)})
            } else {
                if userProgress.componentsHistory.count > 0 {
                    result = userProgress.componentsHistory.filter({filters.contains($0.idNameMicroelement)})
                } else {
                    result =  userProgress.userMicroelement.filter({filters.contains($0.idNameMicroelement)})
                }
                
            }
            return self.nutritionalDetailedBalanceModelFrom(nutrs: result,wholeGrainsHistory: userProgress.wholeGrainsHistory, wholeGrains: userProgress.wholeGrains, refinedGrainsHistory: userProgress.refinedGrainsHistory, startDate: startDate, endDate: endDate)
        }
    }
    
    func fetchNutritionalBalanceModelFor(startDate:Date, endDate : Date) -> Observable<NutritionalBalanceModel> {
        return self.getProgressFor(startDate: startDate, endDate: endDate, returnHistory: true).flatMap{userProgress -> Observable<NutritionalBalanceModel> in
            let filters = ["Grain_total","Fruit_total", "Veg_total", "Prot_total", "Dairy_total"]
            let result : [MicroelementInformation]
            if self.calendar.startOfDay(for: startDate) == self.calendar.startOfDay(for: endDate){
                result =  userProgress.userMicroelement.filter({filters.contains($0.idNameMicroelement)})
            } else {
                if userProgress.componentsHistory.count > 0 {
                    result = userProgress.componentsHistory.filter({filters.contains($0.idNameMicroelement)})
                } else {
                    result =  userProgress.userMicroelement.filter({filters.contains($0.idNameMicroelement)})
                }
            }
            return self.nutritionalBalanceModelFrom(nutrs: result, startDate: startDate, endDate: endDate)
        }
    }
    
    private func nutritionalBalanceModelFrom(nutrs : [MicroelementInformation],startDate : Date, endDate : Date) -> Observable<NutritionalBalanceModel> {
        let model = NutritionalBalanceModel()
        model.data = [nutritionalList] ()
        let grainArray = self.arrayFromId(foodId: "Grain_total", elements: nutrs)
        let fruit = self.arrayFromId(foodId: "Fruit_total", elements: nutrs)
        let veg = self.arrayFromId(foodId: "Veg_total", elements: nutrs)
        let protArray = self.arrayFromId(foodId: "Prot_total", elements: nutrs)
        let diary = self.arrayFromId(foodId: "Dairy_total", elements: nutrs)
        model.data?.append(self.nutrListFrom(array: grainArray, startDate: startDate, endDate: endDate))
        model.data?.append(self.nutrListFrom(array: fruit, startDate: startDate, endDate: endDate))
        model.data?.append(self.nutrListFrom(array: veg, startDate: startDate, endDate: endDate))
        model.data?.append(self.nutrListFrom(array: protArray, startDate: startDate, endDate: endDate))
        model.data?.append(self.nutrListFrom(array: diary, startDate: startDate, endDate: endDate))
        
        return Observable.just(model)
        
    }
    
    private func nutrListFrom(array : [MicroelementInformation], startDate: Date, endDate : Date) -> nutritionalList {
        let userNutrition = UserNutrition()
        var type = NutritionType(rawValue: 0)
        
        if let object = array.first {
            userNutrition.unit = object.unit
            userNutrition.targetNutritional = (object.upperLimit ?? object.lowerLimit ?? 0.0) / object.numDays
            type = NutritionType.stringToType(string: object.idNameMicroelement)
        }
        
        for day in 0...self.daysBetweenDates(startDate: startDate, endDate: endDate){
            let date = self.addDay(startDate: startDate, number: day)
            var objectToUse : MicroelementInformation?
            objectToUse = array.first(where: {$0.historyDate != nil && $0.historyDate! == calendar.startOfDay(for: date)})
            if objectToUse != nil {
                userNutrition.consumedNutritional += objectToUse!.consumedMicroelement / (objectToUse?.numDays)!
            } else if calendar.startOfDay(for: startDate) == calendar.startOfDay(for: endDate) {
                if let object = array.first {
                    userNutrition.consumedNutritional += object.consumedMicroelement
                }
            }
        }
        
        return (type: type!, nutrition: userNutrition)
    }
    
    private func arrayFromId(foodId : String, elements : [MicroelementInformation] ) -> [MicroelementInformation] {
        let arrayToReturn = elements.filter({$0.idNameMicroelement == foodId})
        return arrayToReturn
    }
    
    private func nutritionalDetailedBalanceModelFrom(nutrs : [MicroelementInformation], wholeGrainsHistory : [MicroelementInformation], wholeGrains: [MicroelementInformation], refinedGrainsHistory: [MicroelementInformation], startDate:Date, endDate : Date) -> Observable<NutritionBalanceModel> {
        let model = NutritionBalanceModel()
        let grainArray = self.arrayFromId(foodId: "Grain_total", elements: nutrs)
        let fruit = self.arrayFromId(foodId: "Fruit_total", elements: nutrs)
        let veg = self.arrayFromId(foodId: "Veg_total", elements: nutrs)
        let protArray = self.arrayFromId(foodId: "Prot_total", elements: nutrs)
        let diary = self.arrayFromId(foodId: "Dairy_total", elements: nutrs)
        
        
        let grainObject = self.nutritionalElementFrom(array: grainArray, startDate: startDate, endDate: endDate)
        
        
        
        
        model.arrNutritionElement.append(self.mergeGrainsWith(history: wholeGrainsHistory, wholeGrains: wholeGrains, refinedGrains: refinedGrainsHistory, grainsObject: grainObject))
        model.arrNutritionElement.append(self.nutritionalElementFrom(array: fruit, startDate: startDate, endDate: endDate))
        model.arrNutritionElement.append(self.nutritionalElementFrom(array: veg, startDate: startDate, endDate: endDate))
        model.arrNutritionElement.append(self.nutritionalElementFrom(array: protArray, startDate: startDate, endDate: endDate))
        model.arrNutritionElement.append(self.nutritionalElementFrom(array: diary, startDate: startDate, endDate: endDate))
        
        return Observable.just(model)
    }
    
    private func mergeGrainsWith(history : [MicroelementInformation], wholeGrains: [MicroelementInformation], refinedGrains: [MicroelementInformation], grainsObject : NutritionElementModel) -> NutritionElementModel{
        let mergedModel = NutritionElementModel()
        mergedModel.dataCharts = [NutrientInformationChartModel]()
        mergedModel.type = grainsObject.type
        mergedModel.consumedAmount = grainsObject.consumedAmount
        mergedModel.target = grainsObject.target
        mergedModel.unit = grainsObject.unit
        mergedModel.message = grainsObject.message
        for index in 0...grainsObject.dataCharts!.count - 1 {
            let dataChart = grainsObject.dataCharts![index]
            var totalWholeGrains = 0.0
            let dateToCompare = self.calendar.startOfDay(for : dataChart.label!)
            let historyObject = history.first(where: {$0.historyDate != nil && self.calendar.startOfDay(for : $0.historyDate!) == dateToCompare})
            
            if historyObject != nil{
                totalWholeGrains = historyObject!.consumedMicroelement
            }
            dataChart.wholeGrainsConsumed = totalWholeGrains
            mergedModel.dataCharts?.append(dataChart)
        }
        
        let wholeGrainsArray = self.grainsArray(array: wholeGrains)
        let refinedGrains = self.grainsArray(array: refinedGrains, wholeGrains: false)
        let grainsArray = wholeGrainsArray + refinedGrains
        mergedModel.arrNutrition = [ElementModel]()
        mergedModel.richNutritionFood = [ElementModel]()
        mergedModel.arrNutrition = grainsArray
        mergedModel.richNutritionFood? = grainsArray
        
        return mergedModel
    }
    
    private func grainsArray (array: [MicroelementInformation], wholeGrains: Bool = true) -> [ElementModel] {
        var nutritionArray = [ElementModel]()
        var objectToUse : MicroelementInformation?
        let elementModel = ElementModel()

        if wholeGrains == true {
            elementModel.nameElement = "Whole grains"
        } else {
            elementModel.nameElement = "Refined grains"
        }
        nutritionArray.append(elementModel)
        objectToUse = array.first
        
        if let object = objectToUse {
            nutritionArray += self.formationTopFood(elementModel: self.groupElements(array: self.FoodMicroelemenInformation_to_ElementModel(array: object.contributors)))
        }

        return nutritionArray
    }
    
    private func nutritionalElementFrom(array : [MicroelementInformation], startDate : Date, endDate : Date) -> NutritionElementModel {
        let nutritionElement = NutritionElementModel()
        nutritionElement.dataCharts = [NutrientInformationChartModel]()
        nutritionElement.arrNutrition = [ElementModel]()
        for day in 0...self.daysBetweenDates(startDate: startDate, endDate: endDate) {
            let date = self.addDay(startDate: startDate, number: day)
            let data = NutrientInformationChartModel()
            data.label = date
            var objectToUse : MicroelementInformation?
            objectToUse = array.first(where: {$0.historyDate != nil && $0.historyDate! == calendar.startOfDay(for: date)})
            if objectToUse != nil {
                data.consumed = objectToUse!.consumedMicroelement
                for contr in objectToUse!.contributors {
                    let elementModel = ElementModel()
                    elementModel.amountElement = contr.quality
                    elementModel.nameElement = contr.nameFood
                    elementModel.unit = contr.unit
                    elementModel.consumedState = contr.consumed
                    nutritionElement.arrNutrition?.append(elementModel)
                }
            }else if calendar.startOfDay(for: startDate) == calendar.startOfDay(for: endDate) {
                if array.first != nil {
                    data.consumed = array.first!.consumedMicroelement
                    objectToUse = array.first!
                    for contr in array.first!.contributors {
                        let elementModel = ElementModel()
                        elementModel.amountElement = contr.quality
                        elementModel.nameElement = contr.nameFood
                        elementModel.unit = contr.unit
                        elementModel.consumedState = contr.consumed
                        nutritionElement.arrNutrition?.append(elementModel)
                    }
                }
                
            } else {
                if let object = array.first {
                    objectToUse = object
                }
            }
            
            if nutritionElement.arrNutrition != nil {
                nutritionElement.arrNutrition = self.formationTopFood(elementModel: self.groupElements(array: nutritionElement.arrNutrition!))
            }
            
            nutritionElement.dataCharts?.append(data)
            
            if let object = objectToUse {
                nutritionElement.richNutritionFood = self.formationTopFood(elementModel: self.groupElements(array: self.FoodMicroelemenInformation_to_ElementModel(array: object.contributors)))
                nutritionElement.type = NutritionType.stringToType(string: object.idNameMicroelement)
                nutritionElement.consumedAmount = object.consumedMicroelement
                nutritionElement.target = (object.upperLimit ?? object.lowerLimit ?? 0.0) / object.numDays
                nutritionElement.unit = object.unit
                nutritionElement.message = object.message
            }
        }
        return nutritionElement
    }
    
    private func elementModelWith(title : String) -> ElementModel {
        let elementModel = ElementModel()
        elementModel.nameElement = title
        return elementModel
    }
    
    
    private func watchoutListFrom(nutrs : [[MicroelementInformation]], startDate : Date , endDate : Date) -> Observable<MicroelementModel> {
        let model = MicroelementModel()
        model.data = [microelementList]()
        model.detailInformation = [DetailInformationModel]()
        for day in 0...self.daysBetweenDates(startDate: startDate, endDate: endDate) {
            let date = self.addDay(startDate: startDate, number: day)
            model.data?.append(self.watchoutDataFrom(elements: nutrs,date : date, startDate: startDate, endDate: endDate))
        }
        for elements in nutrs {
            if elements.first != nil {
                model.detailInformation?.append(self.watchoutDetailInformationModelFrom(element : elements.first!))
            }
        }
        
        return Observable.just(model)
    }
    
    private func watchoutDataFrom(elements : [[MicroelementInformation]], date :Date, startDate : Date, endDate : Date) -> microelementList {
        var arrayOfLists = [(type: MicroelementType, element: UserMicroelement)]()
        var type : MicroelementType
        for elementsArray in elements {
            let userMicroelement = UserMicroelement()
        
            type = MicroelementType.typeFrom(foodId: (elementsArray.first!.idNameMicroelement))
            var objectToUse : MicroelementInformation?
            objectToUse = elementsArray.first(where: {$0.historyDate != nil && $0.historyDate! == calendar.startOfDay(for: date)})
            if objectToUse != nil {
                userMicroelement.consumedMicroelement += objectToUse!.consumedMicroelement / (objectToUse?.numDays)!
                
            }else if calendar.startOfDay(for: startDate) == calendar.startOfDay(for: endDate) {
                
                if elementsArray.first != nil {
                    objectToUse = elementsArray.first!
                }
                
                userMicroelement.consumedMicroelement += objectToUse!.consumedMicroelement
                
            }else {
                if elementsArray.first != nil {
                    objectToUse = elementsArray.first!
                }
            }
            
            if let object = objectToUse {
                userMicroelement.targetMicroelement = (object.upperLimit ?? object.lowerLimit ?? 0.0) / object.numDays
                userMicroelement.unit = object.unit
                arrayOfLists.append((type: type, element: userMicroelement))
            }
        }
        
        return (date: date, elements: arrayOfLists)
    }
    
    
    
    private func watchoutDetailInformationModelFrom(element : MicroelementInformation) -> DetailInformationModel{
        let detailInformationModel = DetailInformationModel()
        let type = MicroelementType.typeFrom(foodId: element.idNameMicroelement)
        
        detailInformationModel.message = element.message
        detailInformationModel.topFood = self.formationTopFood(elementModel: self.groupElements(array: self.transform(model: element.contributors))).map({return $0.nameElement})
        
        switch type {
        case .sugars:
            detailInformationModel.title = "Your top added sugars sources:"
            break
        case .salt:
            detailInformationModel.title = "Your top sodium sources:"
            break
        case .saturatedFat:
            detailInformationModel.title = "Your top saturated fat sources:"
            break
        }
        return detailInformationModel
    }
    
    
    
    
    func fetchNutrientsListFor(startDate : Date, endDate : Date) -> Observable<NutrientsModel> {
        let progress = self.getProgressFor(startDate: startDate, endDate: endDate, returnHistory: true)
        let foodCompsCache = self.fooodCompsRepo.getStoredFoodComponents()
        return Observable.zip(progress,foodCompsCache, resultSelector: {userProgress, foodComps -> NutrientsModel in
            let model = self.nutrientsModelFrom(nutts: userProgress.nutrientsLisToShow, startDate: startDate, endDate: endDate,foodCompsCache: foodComps)
            return model
            
        })
    }
    
    private func nutrientsModelFrom(nutts : [[MicroelementInformation]], startDate : Date, endDate : Date, foodCompsCache : [[String : Any]]) -> NutrientsModel {
        let model = NutrientsModel()
        for elementsArray in nutts {
            model.nutrientDetail.append(self.detailNutrientModelFrom(array: elementsArray, startDate: startDate, endDate: endDate, foodCompsFromCache: foodCompsCache))
            model.nutrientChartData.append(self.nutrientChatDataFrom(elements: elementsArray, foodCompsCache: foodCompsCache))
        }
        return model
    }
    
    
    private func nutrientChatDataFrom(elements : [MicroelementInformation], foodCompsCache : [[String : Any]]) -> NutrientModel {
        let nutrientModel = NutrientModel()
        let compsToWorkWith = foodCompsCache.first(where: {($0["id"] as? String ?? "") == elements.first!.idNameMicroelement })!
        if let object = elements.first {
            nutrientModel.targetNutritional = (object.upperLimit ?? object.lowerLimit ?? 0.0) / object.numDays
            nutrientModel.unit = object.unit

        }
        nutrientModel.nameNutritional = NutritionsUtility.formatNutrientInfo(nutritionName: compsToWorkWith["display_name"] as? String ?? "")
        var totalConsumed = 0.0
        for element in elements {
            totalConsumed += element.consumedMicroelement
        }
        
        if let object = elements.first {
            nutrientModel.consumedNutritional = totalConsumed / object.numDays
        }
  
        return nutrientModel
        
    }
    
    private func detailNutrientModelFrom(array : [MicroelementInformation], startDate : Date, endDate : Date, foodCompsFromCache : [[String : Any]]) -> NutrientDetailInformationModel{
        let detailModel = NutrientDetailInformationModel()
        detailModel.message = [String]()
        let compsToWorkWith = foodCompsFromCache.first(where: {($0["id"] as? String ?? "") == array.first!.idNameMicroelement })!
        
        if array.first != nil {
            if array.first!.contributors.count > 0 {
                detailModel.nutrientComeFrom.nameFood = [self.formationTopFood(elementModel: self.groupElements(array: (self.transform(model: (array.first?.contributors)!)))).map({return $0.nameElement}), [compsToWorkWith["sources_txt"] as? String ?? ""]]
            } else {
                detailModel.nutrientComeFrom.nameFood = [[""], [compsToWorkWith["sources_txt"] as? String ?? ""]]
            }
            
        }

        if compsToWorkWith["description"] != nil {
            detailModel.message?.append(compsToWorkWith["description"] as? String ?? "")

        }
        
        detailModel.nutrientComeFrom.caption = [String]()
        
        detailModel.nutrientComeFrom.caption?.append(String(format:"Your top %@ sources",(compsToWorkWith["display_name"] as? String ?? "").capitalized))
        detailModel.nutrientComeFrom.caption?.append(String(format:"Foods rich in %@:", (compsToWorkWith["display_name"] as? String ?? "").capitalized))
        
        let nutrientChartModel = NutrientChartModel()
        nutrientChartModel.dataCharts = [NutrientInformationChartModel]()

        if let object = array.first {
            nutrientChartModel.unit = object.unit
            nutrientChartModel.upperLimit = (object.upperLimit ?? 0.0) / object.numDays
            nutrientChartModel.lowerLimit = (object.lowerLimit ?? 0.0) / object.numDays
            detailModel.dataChart = nutrientChartModel
        }
        

        
        
        for day in 0...self.daysBetweenDates(startDate: startDate, endDate: endDate) {
            let date = self.addDay(startDate: startDate, number: day)
            nutrientChartModel.dataCharts?.append(self.informationModelForNutrients(date: date, array: array))
        }
        
        return detailModel
    }
    
    private func informationModelForNutrients(date : Date, array : [MicroelementInformation] ) -> NutrientInformationChartModel {
        let informationChartModel = NutrientInformationChartModel()
        informationChartModel.label = date
        var objectToUse : MicroelementInformation?
        objectToUse = array.first(where: {$0.historyDate != nil && $0.historyDate! == calendar.startOfDay(for: date)})
        if objectToUse != nil {
            informationChartModel.consumed += objectToUse!.consumedMicroelement
        } else {
            if array.first != nil {
                objectToUse = array.first!
            }
        }
        return informationChartModel
    }
    
    
    private func addDay(startDate: Date, number: Int) -> Date {
        return calendar.date(byAdding: .day, value: number, to: startDate)!
    }
    
    private func daysBetweenDates(startDate: Date, endDate: Date) -> Int
    {
        let components = calendar.dateComponents([.day], from: startDate, to: endDate)
        let dayBetween = components.day
        return  dayBetween ?? 0
    }
    
    
    private func storeProgressToCacheFor(startDate : Date, endDate : Date, progressDictionary : [String : Any], returnHistory : Bool) -> NestleProgressCache {
        let cacheFromDict = self.jsonToCache.transform(dictionary: progressDictionary)
        cacheFromDict.realm?.beginWrite()
        cacheFromDict.startDate = calendar.startOfDay(for: startDate)
        cacheFromDict.endDate = calendar.startOfDay(for: endDate)
        cacheFromDict.returnHistory = returnHistory
        try! cacheFromDict.realm?.commitWrite()
        self.nestleProgressCacheDataStore.store(cache : cacheFromDict)
        return cacheFromDict
    }
    

    private func FoodMicroelemenInformation_to_ElementModel(array: [FoodMicroelemenInformation]) -> [ElementModel] {
        var resultArray = [ElementModel]()
        if array.count > 0 {
            for item in array {
                let elementModel = ElementModel()
                elementModel.nameElement = item.nameFood
                elementModel.consumedState = item.consumed
                elementModel.unit = item.unit
                
                resultArray.append(elementModel)
            }
        }
        return resultArray
    }
    
    func groupElements(array: [ElementModel]) -> [ElementModel] {
        var resultDict = [String: Double]()
        
        for item in array {
            if item.nameElement.isEmpty {
                continue
            } else if let curentValue = resultDict[item.nameElement] {
                resultDict[item.nameElement] = curentValue + item.consumedState!
            } else {
                resultDict[item.nameElement] = item.consumedState
            }
        }
        var resultArray = resultDict.map{ElementModel(nameElement: $0, consumedCalories: $1)}
        resultArray.sort{$0.consumedState! > $1.consumedState!}
        return resultArray
    }
    
    func formationTopFood(elementModel: [ElementModel], round: Bool = false) -> [ElementModel] {
        var resultArray = [ElementModel]()
        if elementModel.count > 0 {
            let groupElement = self.groupElements(array: elementModel)
            let calorificElements = groupElement.sorted(by: ({(s1: ElementModel, s2: ElementModel) -> Bool in
                return s1.consumedState! > s2.consumedState!
            }))
            
            let sumCalories = calorificElements.reduce(0, {$0 + $1.consumedState!})
            var otherSum  = sumCalories
            
            for i in 0..<calorificElements.count {
                if i == 0 || i == 1 || i == 2 {
                    if calorificElements[i].consumedState! / sumCalories > 0.15 {
                        if round == true {
                            resultArray.append(ElementModel(nameElement: calorificElements[i].nameElement, consumedCalories: NRFormatterUtility.roundCupElements(number: calorificElements[i].consumedState!)))
                        } else {
                            resultArray.append(ElementModel(nameElement: calorificElements[i].nameElement, consumedCalories:  calorificElements[i].consumedState!))
                        }
                        
                        otherSum -= calorificElements[i].consumedState!
                    }
                }
            }
            
            if round == true {
                if otherSum >= 0.125 {
                    resultArray.append(ElementModel(nameElement: "Others", consumedCalories: NRFormatterUtility.roundCupElements(number: otherSum)))
                }
            } else {
                if otherSum >= 0.05 {
                    resultArray.append(ElementModel(nameElement: "Others", consumedCalories:  otherSum))
                }
            }
            
        }
        
        return resultArray
    }
    
    private func transform(model : [FoodMicroelemenInformation]) -> [ElementModel] {
        var resultArray = [ElementModel]()
        if model.count > 0 {
            for i in 0..<model.count {
                let elementModel = ElementModel()
                elementModel.nameElement = model[i].nameFood
                elementModel.consumedState = model[i].consumed
                
                resultArray.append(elementModel)
            }
        }
        return resultArray
    }
    
}


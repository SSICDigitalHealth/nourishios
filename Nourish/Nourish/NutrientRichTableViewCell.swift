//
//  NutrientRichTableViewCell.swift
//  Nourish
//
//  Created by Vlad Birukov on 25.09.17.
//  Copyright © 2017 SSIC. All rights reserved.
//

import UIKit

class NutrientRichTableViewCell: UITableViewCell {
    @IBOutlet weak var message: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func setupWith(message: String) {
        self.message.text = message        
    }
    
}

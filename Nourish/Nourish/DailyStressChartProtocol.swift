//
//  DailyStressChartProtocol.swift
//  Nourish
//
//  Created by Gena Mironchyk on 1/24/17.
//  Copyright © 2017 SSIC. All rights reserved.
//

import Foundation
protocol DailyStressChartProtocol {
    func setChartData(data : [DailyStressModel])
}

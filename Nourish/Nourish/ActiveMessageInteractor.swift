 //
//  ActiveMessageInteractor.swift
//  Nourish
//
//  Created by Gena Mironchyk on 3/24/17.
//  Copyright © 2017 SSIC. All rights reserved.
//

import UIKit
import RxSwift
import HealthKit
let backgroundDateKey = "droppedToBackgroundKey"
class ActiveMessageInteractor: StatBaseInteractor {
    let messageRepo = ChatBoxRepository()
    let activityRepo = UserActivityRepository()
    let userRepo = UserRepository.shared
    
/*    func getActiveMessage () -> Observable<ChatDataBaseItem> {
        let user = self.userRepo.getCurrentUser(policy: .ForceReload)
        let startDate =  defaults.object(forKey: backgroundDateKey) as! Date
        let calories = self.activityRepo.getCalories(startDate:startDate , endDate: Date())
        let zip = Observable.zip(user, calories, resultSelector: {userProfile, caloriesArray -> ChatDataBaseItem in
            let cals = self.burnedCalories(samples: caloriesArray)
            let activeMessage = ChatDataBaseItem()
            activeMessage.isFromUser = false
            activeMessage.chatBoxType = .typeCalorieReport
            activeMessage.content = String(format:"Great! You have burned %0.0f calories", cals)
            activeMessage.date = Date()
            activeMessage.userID = userProfile.userID!
            return activeMessage
        })
        return zip
    }*/
    
    func addActiveEnergyBurnedMessage() -> Observable<Bool> {
        let startDate =  defaults.object(forKey: backgroundDateKey) as? Date
        if startDate != nil  && defaults.object(forKey: kOnBoardingKeyDate) != nil{
            let user = self.userRepo.getCurrentUser(policy: .ForceReload)
            let calories = self.activityRepo.getCalories(startDate:startDate! , endDate: Date())
            let zip = Observable.zip(user,calories, resultSelector: {userProfile, caloriesArray -> Bool in
                if userProfile.userGoal == .LooseWeight {
                    let cals = self.burnedCalories(samples: caloriesArray)
                    if cals > 10 {
                        let activeMessage = ChatDataBaseItem()
                        activeMessage.isFromUser = false
                        activeMessage.chatBoxType = .typeCalorieReport
                        activeMessage.content = String(format:"Great! You have burned %0.0f calories", cals)
                        activeMessage.date = Date()
                        activeMessage.userID = userProfile.userID!
                        self.messageRepo.storeRecordsArray(array: [activeMessage])
                        defaults.removeObject(forKey: backgroundDateKey)
                        defaults.synchronize()
                        return true
                    }
                }
                return false
            })
            return zip
        } else {
            return Observable.just(false)
        }
    }
    
    
    func burnedCalories(samples : [HKSample]) -> Double {
        var summary = 0.0
        for sample in samples {
            let key = sample as! HKQuantitySample
            summary = summary + key.quantity.doubleValue(for: HKUnit.kilocalorie())
        }
        return summary
    }
}

//
//  NRProfileSettingsView.swift
//  Nourish
//
//  Created by Vino (Vinodha) Sundaramoorthy on 11/29/16.
//  Copyright © 2016 SSIC. All rights reserved.
//

import UIKit

class NRProfileSettingsView: BaseView , UserProfileViewProtocol ,UIImagePickerControllerDelegate,UINavigationControllerDelegate{
    
    @IBOutlet weak var profileTableView : UITableView!
    var agePicker : NRPickerView!
    var heightPicker : NRPickerView!
    var weightPicker : NRPickerView!
    var controller = NRProfileSettingsViewController()
    var presenter = UserProfilePresenter()
    var imagePicker = UIImagePickerController()

    override func viewWillAppear(_ animated: Bool) {
        //Register cell classes
       self.registerTableViewCells()
       self.basePresenter = presenter
        profileTableView.dataSource = presenter
        profileTableView.delegate = presenter
        
        let footerView = UIView(frame: CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: 72))
        let saveButton = UIButton(frame: CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: 72))
        saveButton.backgroundColor = NRColorUtility.appBackgroundColor()
        saveButton.setTitle("SAVE PROFILE", for: .normal)
        saveButton.titleLabel?.font = UIFont.init(name: "Campton-Bold", size: 15)
        saveButton.titleLabel?.textColor = UIColor.white
        saveButton.addTarget(self, action: #selector(saveUserModel), for: .touchUpInside)
        footerView.addSubview(saveButton)
        profileTableView.tableFooterView = footerView
        
        presenter.userModelView = self
        imagePicker.delegate = self
        imagePicker.allowsEditing = false

        super.viewWillAppear(animated)
    }
    
    func registerTableViewCells() {
        profileTableView.register(UINib(nibName:"NRProfileImageTableViewCell" ,bundle:nil) , forCellReuseIdentifier: "kProfileImageCell")
   profileTableView.register(UINib(nibName:"NRProfilePickerTableViewCell" ,bundle:nil) , forCellReuseIdentifier: "kProfilePickerCell")
        profileTableView.register(UINib(nibName:"NRListTableViewCell" ,bundle:nil) , forCellReuseIdentifier: "kListTableCell")
        profileTableView.register(UINib(nibName:"NRNameTableViewCell" ,bundle:nil) , forCellReuseIdentifier: "kProfileNameCell")
        profileTableView.register(UINib(nibName:"NRUnitsTableViewCell" ,bundle:nil) , forCellReuseIdentifier: "kUnitsCell")
        profileTableView.register(UINib(nibName:"NRProfileGenderCellTableViewCell" ,bundle:nil) , forCellReuseIdentifier: "kProfileGender")
    }
    
    // MARK: UserProfileViewProtocol
    func setFirstName(firstName : String) {
        
    }
    func setLastName(lastName : String) {
        
    }
    func setHeight(height : Double) {
        
    }
    func setWeight(height : Double) {
        
    }
    func setAge(age : Int) {
        
    }
    func setActivityLevel(actLevel : ActivityLevel) {
        
    }
    func setDietaryPreference(dietaryPreference : DietaryPreference) {
        
    }
    func setMetricPreference(metricPreference : MetricPreference) {
        
    }
    func setBiologicalSex(biologicalSex : BiologicalSex) {
        
    }
    
    func stopActivity() {
        self.controller.enableNavigationBar()
        self.stopActivityAnimation()
    }
    
    func startActivity() {
        self.controller.disableNavigationBar()
        self.prepareLoadView()
    }
    
    func showToast(message:String) {
        DispatchQueue.main.async {
            self.controller.showToast(message: message, showingError: false, completion: nil)
        }
    }

    
    func validateSave() -> Bool {
        if self.presenter.userModel.userFirstName == "" || self.presenter.userModel.userLastName == "" || self.presenter.userModel.email == "" {
            DispatchQueue.main.async {
                //Show alert
                let alert = UIAlertController(title: "", message: "Please enter all the details before save.", preferredStyle: UIAlertControllerStyle.alert)
                
                // add an action (button)
                alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
                // show the alert
                self.controller.present(alert, animated: true, completion: nil)
            }
            return false
        }
        return true
    }
    
    func saveUserModel() {
        if self.validateSave() {
            self.presenter.saveCurrentUser()
        }
    }
    
    func refreshUserSettings() {
        self.profileTableView.reloadData()
        self.stopActivity()
    }
    
    func reloadMetricPreference() {
        DispatchQueue.main.async {
            let indexes: IndexSet = [3,4,5]
            self.profileTableView.reloadSections(indexes, with: .none)
        }
    }
    
    func reloadCellSection(sections: IndexSet) {
        DispatchQueue.main.async {
            self.profileTableView.reloadSections(sections, with: .none)
        }
    }
    
    func chooseImageFromGallery() {
        if UIImagePickerController.isSourceTypeAvailable(.savedPhotosAlbum){
            imagePicker.sourceType = .savedPhotosAlbum;
            imagePicker.allowsEditing = false
            imagePicker.delegate = self
            self.controller.present(imagePicker, animated: true, completion: nil)
        }
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
         let image = info[UIImagePickerControllerOriginalImage] as? UIImage
        self.controller.dismiss(animated: false, completion: nil)
        NRImageUtility.saveImageAsProfilePic(image: image!,userId: self.presenter.userModel.userID!)
        self.reloadCellSection(sections: IndexSet(integer: 0))
    }

}

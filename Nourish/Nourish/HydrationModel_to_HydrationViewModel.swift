//
//  HydrationModel_to_HydrationViewModel.swift
//  Nourish
//
//  Created by Vlad Birukov on 29.08.17.
//  Copyright © 2017 SSIC. All rights reserved.
//

import Foundation

class HydrationModel_to_HydrationViewModel {
    func transform(model: HydrationModel) -> HydrationViewModel {
        let hydrationViewModel = HydrationViewModel()
        hydrationViewModel.consumedHydration = model.consumedHydration
        hydrationViewModel.targetHydration = model.targetHydration
        hydrationViewModel.unit = model.unit
        
        return hydrationViewModel
    }
}

//
//  StepsModel.swift
//  Nourish
//
//  Created by Vlad Birukov on 28.08.17.
//  Copyright © 2017 SSIC. All rights reserved.
//

import Foundation

class StepsModel {
    var numberSteps: Double?
    var avgStepsDay: Double?
}

//
//  MealPlannerTableViewCell.swift
//  Nourish
//
//  Created by Vlad Birukov on 21.08.17.
//  Copyright © 2017 SSIC. All rights reserved.
//

import UIKit

class MealPlannerTableViewCell: MGSwipeTableCell {
    @IBOutlet weak var imageOccasion: UIImageView!
    @IBOutlet weak var nameOccasion: UILabel!
    @IBOutlet weak var caloriesLabel: UILabel!
    @IBOutlet weak var line: UIView!
    var views = [FoodMealPlannerView]()
    var viewToProceed: FoodMealPlannerView = FoodMealPlannerView()
    
    override func awakeFromNib() {
        super.awakeFromNib()
//        self.contentView.autoresizingMask = UIViewAutoresizing.flexibleHeight
//        self.contentView.translatesAutoresizingMaskIntoConstraints = false
//        self.viewToProceed.translatesAutoresizingMaskIntoConstraints = false
        // Initialization code
    }

    func setUpWith(model: MealViewModel, index: Int, presenter: MealPlannerPresenter, indexPath: IndexPath) {
        self.viewToProceed = FoodMealPlannerView()
        for item in self.views {
            item.removeFromSuperview()
        }
        
        self.views.removeAll()
        
        self.imageOccasion.image = UIImage(named: Ocasion.stringDescription(servingType: model.nameOccasion).lowercased())
        self.nameOccasion.text = String(format: "%@", Ocasion.stringDescription(servingType: model.nameOccasion))
        self.caloriesLabel.text = String(format: "%.0f kcal", model.calories[index])
        
        
        if model.nameFood[index].count > 0 {
            let numberFood = (model.nameFood[index].count)
            
            for i in 0..<numberFood {
                let foodView = FoodMealPlannerView()
                
                foodView.viewDidLoad()
                
                
                self.contentView.addSubview(foodView)
                foodView.setupWith(nameFood: model.nameFood[index][i], model: model.detail[index].occasion![i], presenter: presenter, i: i, indexPath: indexPath, index: index)
                self.views.append(foodView)
                
                if numberFood == 1 {
                    self.autoLayouting(item: foodView, toTopItem: self.imageOccasion, toBottomItem: self.line, top: 13, bottom: -23)
                } else if i == 0 {
                    self.autoLayouting(item: foodView, toTopItem: self.imageOccasion, toBottomItem: nil, top: 13, bottom: nil)
                } else if i == numberFood - 1 {
                    self.autoLayouting(item: foodView, toTopItem: self.viewToProceed, toBottomItem: self.line, top: 16, bottom: -23)
                } else {
                    self.autoLayouting(item: foodView, toTopItem: self.viewToProceed, toBottomItem: nil, top: 16 , bottom: nil)
                }
                
                self.layoutIfNeeded()
                
                self.viewToProceed = foodView
            }
        }
        self.layoutIfNeeded()
    }
    
    func autoLayouting(item: Any, toTopItem: Any?, toBottomItem: Any?, top: CGFloat?, bottom: CGFloat?) {
        
        (item as! UIView).translatesAutoresizingMaskIntoConstraints = false


        NSLayoutConstraint(item: item, attribute: .leading, relatedBy: .equal, toItem: self, attribute: .leading, multiplier: 1, constant: 0).isActive = true
        NSLayoutConstraint(item: item, attribute: .trailing, relatedBy: .equal, toItem: self, attribute: .trailing, multiplier: 1, constant: 0).isActive = true
        
        
        
        if toTopItem != nil && top != nil {
            let topView = toTopItem as! UIView
            let attr : NSLayoutAttribute = topView == self.contentView ? .top : .bottom
            let constraint = NSLayoutConstraint(item: item, attribute: .top, relatedBy: .equal, toItem: toTopItem!, attribute: attr, multiplier: 1, constant: top!)
            
            constraint.priority = 999
            constraint.isActive = true
        }
        
        if toBottomItem != nil && bottom != nil {
            let topView = toBottomItem as! UIView
            let attr : NSLayoutAttribute = topView == self.contentView ? .bottom : .top
            let constraint = NSLayoutConstraint(item: item, attribute: .bottom, relatedBy: .equal, toItem: toBottomItem!, attribute: attr, multiplier: 1, constant: bottom!)
            
            constraint.priority = 999
            constraint.isActive = true
        }
    }
}

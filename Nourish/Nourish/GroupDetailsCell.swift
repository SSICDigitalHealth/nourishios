//
//  GroupDetailsCell.swift
//  Nourish
//
//  Created by Vino (Vinodha) Sundaramoorthy on 3/8/17.
//  Copyright © 2017 SSIC. All rights reserved.
//

import UIKit

class GroupDetailsCell: MGSwipeTableCell {

    @IBOutlet var titleLabel : UILabel?
    @IBOutlet var servingCountLabel : UILabel?
    @IBOutlet var caloriesLabel : UILabel?
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func setupWithModel(record : MealRecordModel) {
        titleLabel?.text = record.foodTitle
        let serving = record.servingType ?? ""
        let scoreFormatter    = NumberFormatter()
        scoreFormatter.minimumFractionDigits = 0
        scoreFormatter.maximumFractionDigits = 2
        let servingAmount = scoreFormatter.string(from: NSNumber(value: record.servingAmmount!))
        servingCountLabel?.text = String(format:"%@ %@",servingAmount!,serving)
        caloriesLabel?.text = String(format : "%.0f cal",record.calories!)
    }

}

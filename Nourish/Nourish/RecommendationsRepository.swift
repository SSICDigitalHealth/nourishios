//
//  RecommendationsRepository.swift
//  Nourish
//
//  Created by Nova on 1/11/17.
//  Copyright © 2017 SSIC. All rights reserved.
//

import UIKit
import RxSwift
import RealmSwift

let kDisplayText = "display_txt"
let kCalorieText = "kcal"
let kNameText = "name"


final class RecommendationsRepository: NSObject {
    let store = NourishDataStore.shared
    static let shared = RecommendationsRepository()
    let cacheStore = RecommendationCacheDataStore()
    
    func getRecommendationsFor(date : Date, userID : String, token : String, count : Int) -> Observable<[recommendationStruct]> {
        return Observable.create { observer in
            let cache = self.cacheStore.getRecsFor(date: date)
            if cache != nil && cache?.data != nil {
                let datastring = String(data: cache!.data!, encoding: .utf8)
                let array : [recommendationStruct] = self.convertToRecs(text: datastring!)!
                observer.onNext(array)
                observer.onCompleted()
            }
            let _ = self.store.getRecommendations(date: date, userID: userID, token: token, count: count).flatMap({ data -> Observable<[recommendationStruct]> in
                self.cacheStore.store(data: data, date: date, type: .recommendationMeal)
                let datastring = String(data: data, encoding: .utf8)
                let array : [recommendationStruct] = self.convertToRecs(text: datastring!)!
                
                observer.onNext(array)
                observer.onCompleted()
                return Observable.just(array)
            }).subscribe()
            
            return Disposables.create()
        }
    }
    
    func getRecTipsFor(date : Date, userID : String, token : String) -> Observable<[recommendationStruct]> {
        return Observable.create { observer in
            
            let cache = self.cacheStore.getTipsFor(date: date)
            if cache != nil && cache?.data != nil {
                let dataString = String(data : (cache?.data)!, encoding : .utf8)
                let recs = self.convertFromTips(text: dataString!)
                if recs != nil && (recs?.count)! > 0 {
                    let recomendation = recs!.first
                    let array = Array([recomendation!])
                    observer.onNext(array)
                    observer.onCompleted()
                }
            }
            
            let _ = self.store.getTipsFor(userID: userID, token: token, date: date).flatMap({ data -> Observable<[recommendationStruct]> in
                self.cacheStore.store(data: data, date: date, type: .recommendationTip)
                let dataString = String(data : data, encoding : .utf8)
                let recs = self.convertFromTips(text: dataString!)
                if recs != nil && (recs?.count)! > 0 {
                    let recomendation = recs!.first
                    let array = Array([recomendation!])
                    observer.onNext([recomendation!])
                    observer.onCompleted()
                    return Observable.just(array)
                } else {
                    return Observable.just([])
                }
                
            }).subscribe()
            return Disposables.create()
        }
    }
    
    func convertFromTips(text : String) -> [recommendationStruct]? {
        var array : [String] = []
        if let data = text.data(using: .utf8) {
            do {
                array = try JSONSerialization.jsonObject(with: data, options: []) as! [String]
            } catch {
                LogUtility.logToFile(error)
            }
        }
        var recs : [recommendationStruct] = []
        for string in array {
            let rec : recommendationStruct = recommendationStruct(description: string, title: "", subTitle: "", type: .tips, foodModel: nil)

            recs.append(rec)
        }
        
        return recs
    }
    
    func convertToRecs(text: String) -> [recommendationStruct]? {
        var dict : [[String : Any]] = []
        if let data = text.data(using: .utf8) {
            do {
                let responseArray = try JSONSerialization.jsonObject(with: data, options: []) as! [String:[[String:Any]]]
                
                for (value,_) in responseArray {
                    let obj = responseArray[value]
                    for item in obj! {
                        dict.append(item)
                    }
                }
            } catch {
                LogUtility.logToFile(error)
            }
        }
        var array : [recommendationStruct] = []
        for object in dict {
            let rec = self.recommendationFrom(dict: object )
            array.append(rec)
        }
        return array
    }
    
    func recommendationFrom(dict : [String : Any]) -> recommendationStruct {
        let titleString : String = dict[kNameText] as! String
        var subTitleString : String = ""
        
        if ((dict[kCalorieText]) != nil) {
            subTitleString = String(format: " | %.2f kCal",dict[kCalorieText] as! Double)
        }
        
        let unit : String = dict["unit"] == nil ? "A serving" :  String(format:"A %@",dict["unit"] as! String)
        
        let score : String = dict["delta_day"] is NSNull || dict["delta_day"] == nil ? "" : String(format:"%.0f",dict["delta_day"] as! Double * 100)
        
        let nut : String = dict["target_nut"] is NSNull || dict["target_nut"]  == nil ? String(format:" will improve your score by %@ points.",score) : String(format:" will improve your %@ intake and improve your score by %@ points.",dict["target_nut"] as! String,score)
        
        let desc : String = unit + nut
        
        var proxyModel : foodSearchModel?
        if dict["grams"] != nil {
            
            var model = foodSearchModel()
            model.amount = dict["amount"] as! Double
            model.grams = dict["grams"] as? Double
            model.foodTitle = dict["name"] as! String
            model.isFromNoom = false
            //model.foodBackendID = dict["id"] as? String
            model.mealIdString = dict["id"] as? String

            let calories = dict["kcal"] as! Double
            model.calories = String(format : "%.0f", calories)
            
            let occasionString = dict["occasion_name"] as! String
            model.occasion = Ocasion.enumFrom(stringRep: occasionString)
            
            model.unit = dict["unit"] as! String
            model.caloriePerPortion = calories / model.amount
            proxyModel = model
        }
        
        // TO DO : Hardcoded to .lunch for testing UI
        return recommendationStruct(description: desc, title: titleString, subTitle: subTitleString, type : self.recommendationTypeFrom(model: proxyModel), foodModel : proxyModel)
    }
    
    func recommendationTypeFrom(model : foodSearchModel?) -> recommendationType {
        let modelOcasion = (model?.occasion)! as Ocasion
        if model == nil {
            return .tips
        } else {
            switch modelOcasion {
            case .breakfast:
                return .breakfast
            case .dinner:
                return .dinner
            case .snacks:
                return .snack
            case .lunch:
                return .lunch
            }
        }
    }
}

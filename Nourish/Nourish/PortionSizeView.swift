//
//  PortionSizeView.swift
//  Optifast
//
//  Created by Gena Mironchyk on 10/12/17.
//  Copyright © 2017 SSIC. All rights reserved.
//

import UIKit
let portionSizeComponentsCellIdentifier = "componentCell"
let portionDetailsCellIdentifier = "detailsCell"

protocol PortionSizeViewProtocol {
    func viewForSection(section : Int) -> UIView?
    func reloadSectionAt(index : Int)
}

class PortionSizeView: BaseView, PortionSizeViewProtocol {
    @IBOutlet weak var presenter : PortionSizePresenter!
    @IBOutlet weak var tableView : UITableView!

    override func viewDidLoad() {
        super.viewDidLoad()
        self.registerCells()
        self.basePresenter = self.presenter
        self.presenter.detailedTable = self
        self.tableView.estimatedRowHeight = 255.0
        self.tableView.rowHeight = UITableViewAutomaticDimension
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.stopActivityAnimation()

    }
    
    private func registerCells () {
        self.tableView.register(UINib.init(nibName: "PortionSizeComponentCell", bundle: nil), forHeaderFooterViewReuseIdentifier: portionSizeComponentsCellIdentifier)
        self.tableView.register(UINib.init(nibName: "PortionSizeDetailsCell", bundle: nil), forCellReuseIdentifier: portionDetailsCellIdentifier)
        
    }
    
    func viewForSection(section : Int) -> UIView?
    {
        return self.tableView.headerView(forSection:section)
    }
    func reloadSectionAt(index : Int){
        self.tableView.reloadSections(NSIndexSet(index : index) as IndexSet, with: .automatic)

    }
  
    
    
    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */

}

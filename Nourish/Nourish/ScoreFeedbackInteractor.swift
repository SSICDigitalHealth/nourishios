//
//  ScoreFeedbackInteractor.swift
//  Nourish
//
//  Created by Vino (Vinodha) Sundaramoorthy on 4/19/17.
//  Copyright © 2017 SSIC. All rights reserved.
//

import UIKit
import RxSwift
import RealmSwift

class ScoreFeedbackInteractor: NSObject {
    let userRepository = UserRepository.shared
    let realmRepo = ScoreFeedbackRepository()
    let cacheToModelMapper = ScoreCacheRealm_to_ScoreModel()

    func getScoreModel(date:Date) -> Observable<ScoreModel> {
        let scoreCahce =  realmRepo.getScoreFeedback(date:date)
        
        let topNutrients = List<NutrientInfoRealm>()
        let arrayToFilter = scoreCahce.scoreDelta >= 0 ? scoreCahce.bestDelta : scoreCahce.worstDelta
        var predicateString = "name == "
        
        if arrayToFilter.count > 0{
            for nut in arrayToFilter {
                predicateString.append(String(format:"\'%@\'",nut.stringValue))
            
                if nut.stringValue != arrayToFilter.last?.stringValue {
                    predicateString.append(" || name == ")
                }
            }
            let predicate = NSPredicate(format : predicateString)
            let searchResult = scoreCahce.nutrientList.filter(predicate)

            for nut in searchResult {
                topNutrients.append(nut)
            }
        }
        
        scoreCahce.nutrientList = topNutrients
        let scoreModel =  cacheToModelMapper.transform(cache: scoreCahce)
        return Observable.just(scoreModel)
    }
    
}

//
//  OptifastDetailsSectionCell.swift
//  Nourish
//
//  Created by Gena Mironchyk on 10/23/17.
//  Copyright © 2017 SSIC. All rights reserved.
//

import UIKit
protocol OptifastDetailsSectionCellDelegate {
    func toggleSection(header: OptifastDetailsSectionCell, section: Int)
}
class OptifastDetailsSectionCell: UITableViewHeaderFooterView {

    @IBOutlet weak var sectionTitleLabel : UILabel!
    @IBOutlet weak var expandedImage : UIImageView!
    @IBOutlet weak var footerSeparator : UIView!
    
    var delegate: OptifastDetailsSectionCellDelegate?
    var section : Int = 0
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        let gestureRecognizer = UITapGestureRecognizer.init(target: self, action: #selector(OptifastDetailsSectionCell.tapHeader(gestureRecognizer:)))
        self.addGestureRecognizer(gestureRecognizer)
        // Initialization code
    }

    func setCollapsed(collapsed: Bool) {
        if collapsed {
           // self.expandedImage.transform = self.expandedImage.transform.rotated(by: CGFloat(180.degreesToRadians))
            self.expandedImage.rotate(CGFloat(180.degreesToRadians))
            self.footerSeparator.backgroundColor = UIColor.clear
        } else {
            //self.expandedImage.transform = self.expandedImage.transform.rotated(by: CGFloat(0.degreesToRadians))
            self.expandedImage.rotate(CGFloat(0.0.degreesToRadians))
            self.footerSeparator.backgroundColor = NRColorUtility.hexStringToUIColor(hex : "C3C2C3")
        }
    }
    
    func tapHeader(gestureRecognizer: UITapGestureRecognizer) {
        guard let cell = gestureRecognizer.view as? OptifastDetailsSectionCell else {
            return
        }
        delegate?.toggleSection(header: self, section: cell.section)
    }
    
    
    
}

//
//  MicroelementCustomCollectionViewCell.swift
//  Nourish
//
//  Created by Vlad Birukov on 08.06.17.
//  Copyright © 2017 SSIC. All rights reserved.
//

import UIKit

class MicroelementCustomCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var nameMicroelement: UILabel!
    @IBOutlet weak var currentStateMicroelement: UILabel!
    @IBOutlet weak var progressMicroelementView: ProgressMicroelementView!
    @IBOutlet weak var percentMicroelement: UILabel!
    
    func setupWithModel(model: (String, UserMicroelement)) {
//        nameMicroelement.text = model.0
//        currentStateMicroelement.text = String(format: "%.2f/%.2f%@", model.1.consumedMicroelement, model.1.targetMicroelement, model.1.unit)
//        progressMicroelementView.setupWith(microelementConsumed: model.1.consumedMicroelement, microelementTarget: model.1.targetMicroelement)
//        
//        percentMicroelement.text = percentCalculation(userMicroelement: model.1)
    
    }
    
    private func percentCalculation(userMicroelement: UserMicroelement) -> String {
        var percentMicroelemt: String = "0%"
        
        if userMicroelement.targetMicroelement != 0.0{
            percentMicroelemt = String(format: "%d%@",Int(userMicroelement.consumedMicroelement/userMicroelement.targetMicroelement * 100.0), "%")
        } else if userMicroelement.consumedMicroelement != 0.0{
            percentMicroelemt = String(format: "%d%@",Int(userMicroelement.consumedMicroelement * 10000), "%")
        }
        
        return percentMicroelemt
    }
}

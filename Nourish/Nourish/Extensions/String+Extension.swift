//
//  String+Extension.swift
//  Nourish
//
//  Created by Vino (Vinodha) Sundaramoorthy on 11/18/16.
//  Copyright © 2016 SSIC. All rights reserved.
//

import UIKit

extension String {
    func sizeOfBoundingRect(width: CGFloat ,height: CGFloat , font:UIFont) -> CGSize {
        let constraintRect = CGSize(width: width, height: height)
        let paragraphStyle = NSMutableParagraphStyle()
        paragraphStyle.alignment = NSTextAlignment.justified
        let boundingBox = self.boundingRect(with: constraintRect, options: .usesLineFragmentOrigin, attributes: [NSFontAttributeName: font,NSParagraphStyleAttributeName:paragraphStyle], context: nil)
        let boundingSize = CGSize(width: ceil(boundingBox.size.width), height: ceil(boundingBox.size.height))
        return boundingSize
    }
    
    func fromBase64() -> String? {
        guard let data = Data(base64Encoded: self) else {
            return nil
        }
            
        return String(data: data, encoding: .utf8)
    }
        
    func toBase64() -> String {
        return Data(self.utf8).base64EncodedString()
    }
    
    func slice(from: String, to: String) -> String? {
        
        return (range(of: from)?.upperBound).flatMap { substringFrom in
            (range(of: to, range: substringFrom..<endIndex)?.lowerBound).map { substringTo in
                substring(with: substringFrom..<substringTo)
            }
        }
    }
    

}

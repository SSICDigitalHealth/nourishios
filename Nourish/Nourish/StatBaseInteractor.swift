//
//  StatBaseInteractor.swift
//  Nourish
//
//  Created by Nova on 2/22/17.
//  Copyright © 2017 SSIC. All rights reserved.
//

import UIKit
import RxSwift
import HealthKit

class StatBaseInteractor: NSObject {
    
    let calendar = Calendar.current
    
    func fillWithEmpties(dict : [Date : [HKSample]], period : period) -> [Date : [HKSample]] {
        switch period {
        case .today:
            return dict
        case .weekly:
            if dict.keys.count == 7 {
                return dict
            } else {
                var dictToReturn = dict
                let necessary = self.necessaryKeys(period: period)
                for date in necessary {
                    if dictToReturn[date] == nil {
                        dictToReturn[date] = []
                    }
                }
                return dictToReturn
            }
        case .monthly:
            if dict.keys.count == 4 {
                return dict
            } else {
                var dictToReturn = dict
                let necessary = self.necessaryKeys(period: period)
                for date in necessary {
                    if dictToReturn[date] == nil {
                        dictToReturn[date] = []
                    }
                }
                return dictToReturn
            }
            
        }
    }
    
    func necessaryKeys(period : period) -> [Date] {
        let date = Date()
        switch period {
        case .today:
            return [date]
        case .weekly:
            var array : [Date] = []
            let startOfDay = calendar.startOfDay(for: date)
            array.append(startOfDay)
            
            for index in 1..<7 {
                let newDate = calendar.date(byAdding: Calendar.Component.day, value: -index, to: startOfDay)
                array.append(newDate!)
            }
            return array
        case .monthly:
            var array : [Date] = []
            let startOfWeek = self.startOfWeekFrom(date: date)
            
            array.append(startOfWeek)
            
            for index in 1..<4 {
                let newDate = calendar.date(byAdding: Calendar.Component.weekdayOrdinal, value: -index, to: startOfWeek)
                array.append(newDate!)
            }
            return array
        }
    }
    
    func transforDict(weightDict :  [Date : [HKSample]], calorieDict :  [Date : [HKSample]],  period : period, weeklyPlan : Double, startWeight : Double) -> WeightStatsTotal {
        
        let stat = WeightStatsTotal()
        stat.caloriesArray = self.calorieStatsSample(dict: calorieDict, period: period)
        stat.weightsArray = self.weightStatsSample(dict: weightDict, period: period)
        stat.averageCalorie = self.avgValueFromCalories(samples: stat.caloriesArray)
        stat.averageWeight = self.avgValueFromWeight(samples: stat.weightsArray)
        var periodArray : [Double] = []
        
        
        let weightsArray = stat.weightsArray.sorted(by: {$0.startDate<$1.startDate})
        if weightsArray.first(where: {$0.weight > 0}) != nil {
            let currentWeightObject = weightsArray.first(where: {$0.weight > 0})!
        
            var currentWeight = currentWeightObject.weight
            let index = weightsArray.index(of: currentWeightObject)
            if index! > 0 {
                for _ in 0...Int(index!)-1 {
                    periodArray.append(0.0)
                }
            }
        
            periodArray.append(currentWeight)

            var coeff = 0.0
            if period == .weekly {
                coeff = weeklyPlan/7.0
                while periodArray.count != 7 {
                    currentWeight -= coeff
                    periodArray.append(currentWeight)
                }
            } else {
                while periodArray.count != 4 {
                    currentWeight -= weeklyPlan
                    periodArray.append(currentWeight)
                }
            }
            stat.weeklyPlanArray = periodArray
        }
        return stat
    }
    
    private func avgValueFromCalories(samples : [CalorieStatsSample]) -> Double {
        var count = samples.count
        if count > 0 {
            var sum = 0.0
            
            for sample in samples {
                if sample.calories != 0.0 {
                    sum += sample.calories
                } else {
                    count = count - 1
                }
            }
            if count == 0 {
                count = count + 1
            }
            
            return sum / Double(count)
        } else {
            return 0.0
        }
        
    }
    
    private func avgValueFromWeight(samples : [WeightStatsSample]) -> Double {
        var count = samples.count
        
        if count > 0 {
            var sum = 0.0
            
            for sample in samples {
                if sample.weight != 0.0 {
                    sum += sample.weight
                } else {
                    count = count - 1
                }
            }
            if count == 0 {
                count = count + 1
            }
            
            return sum / Double(count)
        } else {
            return 0.0
        }
        
    }
    
    func weightStatsSample(dict : [Date : [HKSample]], period : period) -> [WeightStatsSample] {
        var array : [WeightStatsSample] = []
        
        for key in dict.keys {
            let sample = WeightStatsSample()
            sample.startDate = key
            sample.endDate = self.endDateFrom(period: period, startDate: key)
            if (dict[key]?.count)! > 0{
                let quantityWeight = dict[key]?.last as! HKQuantitySample
                sample.weight = quantityWeight.quantity.doubleValue(for: HKUnit.gramUnit(with: .kilo))
            } else {
                sample.weight = 0.0
            }
            array.append(sample)
        }
        
        return array
    }
    
    func calorieStatsSample(dict : [Date : [HKSample]], period : period) -> [CalorieStatsSample] {
        var array : [CalorieStatsSample] = []
        let arrayOfKeys = Array(dict.keys.sorted())
        for key in arrayOfKeys {
            let sample = CalorieStatsSample()
            sample.startDate = key
            sample.endDate = self.endDateFrom(period: period, startDate: key)
            var summary = 0.0
            
            let caloriesArray = dict[key]
            
            if (caloriesArray?.count)! > 0 {
                for sampleValue in caloriesArray! {
                    let value = sampleValue as! HKQuantitySample
                    
                    summary = summary + value.quantity.doubleValue(for: HKUnit.kilocalorie())
                }
            }
            
            sample.calories = summary
            
            array.append(sample)
        }
        return array
    }
    
    func endDateFrom(period : period, startDate : Date) -> Date {
        switch period {
        case .today:
            let endDate = calendar.date(byAdding: Calendar.Component.day, value: 1, to: startDate)
            return endDate!
        case .weekly:
            let endDate = calendar.date(byAdding: Calendar.Component.day, value: 1, to: startDate)
            return endDate!
        case .monthly:
            let endDate = calendar.date(byAdding: Calendar.Component.weekdayOrdinal, value: 1, to: startDate)
            return endDate!
        }
    }
    
    func avgValueFrom(samples : [HKSample], isWeight : Bool) -> Double {
        let count = samples.count
        var sum = 0.0
        
        for sample in samples {
            let value = sample as! HKQuantitySample
            
            if isWeight == true {
                sum = sum + value.quantity.doubleValue(for: HKUnit.gramUnit(with: .kilo))
            } else {
                sum = sum + value.quantity.doubleValue(for:HKUnit.kilocalorie())
            }
        }
        
        if sum == 0 {
            return sum
        }
        
        return sum / Double(count)
    }
    func separateSamples(samples : [HKSample], period : period) -> [Date : [HKSample]] {
        
        switch period {
        case .today:
            return [calendar.startOfDay(for: Date()) : samples]
        case .weekly:
            var dict : [Date : [HKSample]] = [:]
            var proxyDate : Date?
            
            for sample in samples {
                let date = sample.startDate
                proxyDate = calendar.startOfDay(for: date)
                
                let keyExists = dict[proxyDate!] != nil
                
                if keyExists == true {
                    var array = dict[proxyDate!]
                    array?.append(sample)
                    dict[proxyDate!] = array
                } else {
                    var array : [HKSample] = []
                    array.append(sample)
                    dict[proxyDate!] = array
                }
            }
            return dict
        case .monthly:
            var dict : [Date : [HKSample]] = [:]
            var proxyWeek : Date?
            
            for sample in samples {
                let date = sample.startDate
                proxyWeek = self.startOfWeekFrom(date: date)
                
                let keyExists = dict[proxyWeek!] != nil
                
                if keyExists == true {
                    var array = dict[proxyWeek!]
                    array?.append(sample)
                    dict[proxyWeek!] = array
                } else {
                    var array : [HKSample] = []
                    array.append(sample)
                    dict[proxyWeek!] = array
                }
                
            }
            return dict
        }
    }
    
    func startOfWeekFrom(date : Date) -> Date {
        return Calendar.current.date(from: Calendar.current.dateComponents([.yearForWeekOfYear, .weekOfYear], from: date))!
    }

}

//
//  json_to_NestleProgressCache.swift
//  Nourish
//
//  Created by Gena Mironchyk on 8/30/17.
//  Copyright © 2017 SSIC. All rights reserved.
//

import UIKit

class json_to_NestleProgressCache: NSObject {
    func transform(dictionary : [String : Any]) -> NestleProgressCache {
        let cache = NestleProgressCache()
        let summary = dictionary["summary"] as? [String : Any]
        
        cache.numDays = dictionary["num_days"] as? Double ?? 0.0
        
        if dictionary["score"] != nil {
            let score = dictionary["score"] as? Double ?? 0.0
            cache.userScore = score * 100.0
        } else {
            cache.userScore = 0.0
        }
        
        if summary != nil {
            let callories = summary?["calorie"] as? [String : Any]
            if callories != nil {
                cache.userCalories = Calories()
                cache.userCalories?.consumed = callories?["consumed_amount"] as? Double ?? 0.0
                cache.userCalories?.target = callories?["recommended_amount"] as? Double ?? 0.0
                cache.userCalories?.unit = callories?["unit"] as? String ?? ""
                
                let calsHistory = callories!["history"] as? [[String : Any]] ?? [[String : Any]]()
                
                cache.calsHistory.append(objectsIn: self.calorieHistoryArrayFrom(history: calsHistory))
                
                if callories?["contributors"] != nil {
                    let contributors = callories?["contributors"] as? [[String : Any]] ?? [[String : Any]]()
                    for food  in contributors {
                        cache.caloriesForDay.append(self.caloriesForDayFrom(dict: food))
                    }
                }
            }
            
            let scoreDictionary = summary?["score"] as? [String : Any]
            if scoreDictionary != nil {
                let scoreHistoryArray = scoreDictionary?["history"] as? [[String : Any]]
                if scoreHistoryArray != nil {
                    for scoreHistoryObject in scoreHistoryArray! {
                        cache.scoreHistoryList.append(self.scoreHistoryFrom(dict: scoreHistoryObject))
                    }
                }
                
            }
            
        }
        if let components = summary?["component"] as? [String : Any] {
            cache.componentsHistory.append(objectsIn: self.componentsHistoryArrayFrom(dict: components))
        }
        
        
        if dictionary["components"] != nil {
            let comps = dictionary["components"] as? [[String : Any]] ?? [[String : Any]]()
            for element in comps{
                cache.microelementList.append(self.microelementFrom(dict: element))
            }
        }
        
        let nutrsList = dictionary["nutrient_list"] as? [String]
        if nutrsList != nil {
            for nutrId in nutrsList! {
                let realmString = RealmString()
                realmString.stringValue = nutrId
                cache.nutrientsListToShow.append(realmString)
            }
        }
        
        
        return cache
    }
    
    
    
    private func dateFrom(dateString : String) -> Date {
        let df = DateFormatter()
        df.dateFormat = "yyyyMMdd"
        return Calendar.current.startOfDay(for: df.date(from: dateString)!)
    }
    
    
    
    private func calorieHistoryArrayFrom(history : [[String : Any]]) -> [CaloriesHistory] {
        var arrayToReturn = [CaloriesHistory]()
        for calorieHistoryDict in history {
            let historyObject = CaloriesHistory()
            let calorieDetails = calorieHistoryDict["calorie"] as? [String : Any] ?? [String : Any]()
            historyObject.consumed = calorieDetails["consumed_amount"] as? Double ?? 0.0
            historyObject.maxConsumed = calorieDetails["recommended_amount"] as? Double ?? 0.0
            let stringDate = calorieHistoryDict["start_day"] as? String
            if stringDate != nil {
                historyObject.date = self.dateFrom(dateString: stringDate!)
            }
            arrayToReturn.append(historyObject)
        }
        return arrayToReturn
    }
    
    
    private func scoreHistoryFrom(dict : [String : Any]) -> ScoreHistory {
        let scoreHistory = ScoreHistory()
        let scoreValue = dict["score"] as? Double ?? 0.0
        scoreHistory.scoreValue = scoreValue * 100.0
        let starDateString = dict["start_day"] as? String
        let endDateString = dict["end_day"] as? String
        if starDateString != nil {
            scoreHistory.startDate = self.dateFrom(dateString: starDateString!)
        }
        if endDateString != nil {
            scoreHistory.endDate = self.dateFrom(dateString: endDateString!)
        }
        return scoreHistory
    }
    
    
    private func microelementFrom(dict: Dictionary<String, Any>) -> Microelement {
        let microelement = Microelement()
        
        microelement.idNameMicroelement = dict["id"] as? String ?? ""
        microelement.nutritionIndicator = NutritionIntdicator.intFrom(string: dict["indicator"] as? String ?? "")
        microelement.consumedMicroelement.value = dict["consumed_amount"] as? Double
        microelement.lowerLimit.value = dict["lower_limit"] as? Double
        microelement.upperLimit.value = dict["upper_limit"] as? Double
        microelement.unit = dict["unit"] as? String ?? ""
        if let message = dict["message"] as? String {
            microelement.message = message
        }
        
        
        if dict["contributors"] != nil {
            let contributors = dict["contributors"] as? [[String : Any]] ?? [[String : Any]]()
            for foodItem in contributors{
                let contributorsElement = foodItem
                let foodElement = FoodElement()
                foodElement.nameFood = contributorsElement["food_name"] as? String ?? ""
                foodElement.consumed = contributorsElement["consumed_amount"] as? Double ?? 0.0
                
                if contributorsElement["portions"] != nil {
                    let portions = contributorsElement["portions"] as? [[String : Any]] ?? [[String : Any]]()
                    for item in portions {
                        let portionDict = item
                        foodElement.quality = foodElement.quality + (portionDict["quantity"] as? Double ?? 0.0)
                        foodElement.unit = portionDict["unit"] as? String ?? ""
                    }
                }
                microelement.contributors.append(foodElement)
                
            }
        }
        
        return microelement
    }

    
    private func componentsHistoryArrayFrom(dict : [String : Any]) -> [MicroelementHistoryObject] {
        var arrayToReturn = [MicroelementHistoryObject]()
        let historyArray = dict["history"] as? [[String : Any]] ?? [[String : Any]]()
        for historyInfo in historyArray {
            let historyComponents = historyInfo["components"] as? [[String : Any]] ?? [[String : Any]]()
            for component in historyComponents {
                let object = MicroelementHistoryObject()
                object.consumedAmount = component["consumed_amount"] as? Double ?? 0.0
                object.microelementId = component["id"] as? String
                let dateString = historyInfo["start_day"] as? String
                if dateString != nil {
                    object.date = self.dateFrom(dateString: dateString!)
                }
                arrayToReturn.append(object)
            }
        }
        return arrayToReturn
    }

    
    
    
    
    private func caloriesForDayFrom(dict: Dictionary<String, Any>) -> CaloriesForDay {
        let caloriesForDay = CaloriesForDay()
        caloriesForDay.nameFood = dict["food_name"] as? String ?? ""
        caloriesForDay.nameFoodIntake = dict["occasion_name"] as? String ?? ""
        caloriesForDay.consumedCalories = dict["consumed_amount"] as? Double ?? 0.0
        
        if dict["portions"] != nil {
            let foodDetail = dict["portions"] as? [[String : Any]] ?? [[String : Any]]()
            for potrion in foodDetail {
                let portionDict = potrion
                caloriesForDay.amountFood = caloriesForDay.amountFood + (portionDict["quantity"] as? Double ?? 0.0)
                caloriesForDay.unit = portionDict["unit"] as? String
            }
        }
        
        return caloriesForDay
    }
    
    
    
}

//
//  MultiModalView.swift
//  Nourish
//
//  Created by Gena Mironchyk on 5/7/17.
//  Copyright © 2017 SSIC. All rights reserved.
//

import UIKit

class MultiModalView: BaseView {
    @IBOutlet weak var micButton : UIButton!
    @IBOutlet weak var photoButton : UIButton!
    @IBOutlet weak var mealDiaryButton : UIButton!
    @IBOutlet weak var cancelButton : UIButton!
    
    @IBOutlet weak var photoButtonBottomConstraint : NSLayoutConstraint!
    @IBOutlet weak var micButtonBottomConstraint : NSLayoutConstraint!
    @IBOutlet weak var micButtonCenterConstraint : NSLayoutConstraint!
    @IBOutlet weak var foodDiaryCenterConstraint : NSLayoutConstraint!
    
    let presenter = MultiModalMenuPresenter ()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.basePresenter = presenter
        self.setupView()
        self.photoButton.layer.cornerRadius = self.photoButton.frame.size.width/2.0
        self.mealDiaryButton.layer.cornerRadius = self.mealDiaryButton.frame.size.width/2.0
        self.micButton.layer.cornerRadius = self.micButton.frame.size.width/2.0
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.stopActivityAnimation()
    }
    
    private func setupView() {
        let view = UINib(nibName: "MutliModalView", bundle: nil).instantiate(withOwner: self, options: nil)[0] as! UIView
        view.backgroundColor = UIColor.black.withAlphaComponent(0.6)
        view.translatesAutoresizingMaskIntoConstraints = false
        self.translatesAutoresizingMaskIntoConstraints = false
        self.addSubview(view)
        NSLayoutConstraint(item: view, attribute: .leading, relatedBy: .equal, toItem: self, attribute: .leading, multiplier: 1, constant: 0).isActive = true
        NSLayoutConstraint(item: view, attribute: .trailing, relatedBy: .equal, toItem: self, attribute: .trailing, multiplier: 1, constant: 0).isActive = true
        NSLayoutConstraint(item: view, attribute: .top, relatedBy: .equal, toItem: self, attribute: .top, multiplier: 1, constant: 0).isActive = true
        NSLayoutConstraint(item: view, attribute: .bottom, relatedBy: .equal, toItem: self, attribute: .bottom, multiplier: 1, constant: 0).isActive = true
        
        self.photoButton.transform = CGAffineTransform(rotationAngle: CGFloat(180.0.degreesToRadians))
        self.micButton.transform = CGAffineTransform(rotationAngle: CGFloat(180.0.degreesToRadians))
        self.mealDiaryButton.transform = CGAffineTransform(rotationAngle: CGFloat(180.0.degreesToRadians))

    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.animatebuttons()
    }
    
    private func animatebuttons () {
        
//        self.photoButtonBottomConstraint.constant = 116.0
//        self.micButtonBottomConstraint.constant = 94.0
//        self.micButtonCenterConstraint.constant -= 64.0
//        self.foodDiaryCenterConstraint.constant += 64.0
        
        self.photoButtonBottomConstraint.constant = 122.0
        self.micButtonBottomConstraint.constant = 100.0
        self.micButtonCenterConstraint.constant -= 70.0
        self.foodDiaryCenterConstraint.constant += 70.0
        self.micButton.isHidden = false 
        self.photoButton.isHidden = false
        self.mealDiaryButton.isHidden = false
     
        UIView.animate(withDuration: 0.2, animations: {
            self.layoutIfNeeded()
            self.photoButton.transform = self.photoButton.transform.rotated(by: CGFloat(-170.degreesToRadians))
            self.photoButton.transform = self.photoButton.transform.rotated(by: CGFloat(-30.degreesToRadians))
            self.micButton.transform = self.micButton.transform.rotated(by: CGFloat(-170.degreesToRadians))
            self.micButton.transform = self.micButton.transform.rotated(by: CGFloat(-30.degreesToRadians))
            self.mealDiaryButton.transform = self.mealDiaryButton.transform.rotated(by: CGFloat(-170.degreesToRadians))
            self.mealDiaryButton.transform = self.mealDiaryButton.transform.rotated(by: CGFloat(-30.degreesToRadians))
        }, completion : {completed in
            self.photoButtonBottomConstraint.constant = 112.0
            self.micButtonBottomConstraint.constant = 90.0
            self.micButtonCenterConstraint.constant += 10.0
            self.foodDiaryCenterConstraint.constant -= 10.0

            UIView.animate(withDuration: 0.1, animations: {
                self.layoutIfNeeded()
                self.photoButton.transform = self.photoButton.transform.rotated(by: CGFloat(30.degreesToRadians))
                self.micButton.transform = self.micButton.transform.rotated(by: CGFloat(30.degreesToRadians))
                self.mealDiaryButton.transform = self.mealDiaryButton.transform.rotated(by: CGFloat(30.degreesToRadians))

            }, completion : {completed in
                self.photoButtonBottomConstraint.constant = 116.0
                self.micButtonBottomConstraint.constant = 94.0
                self.micButtonCenterConstraint.constant -= 4.0
                self.foodDiaryCenterConstraint.constant += 4.0
                UIView.animate(withDuration: 0.1, animations: {
                    self.layoutIfNeeded()
                    self.photoButton.transform = self.photoButton.transform.rotated(by: CGFloat(-10.degreesToRadians))
                    self.micButton.transform = self.micButton.transform.rotated(by: CGFloat(-10.degreesToRadians))
                    self.mealDiaryButton.transform = self.mealDiaryButton.transform.rotated(by: CGFloat(-10.degreesToRadians))

                })
            })
        })
    }
    
    @IBAction func onCancelPressed () {
        self.photoButtonBottomConstraint.constant = 0.0
        self.micButtonBottomConstraint.constant = 0.0
        self.micButtonCenterConstraint.constant = 0.0
        self.foodDiaryCenterConstraint.constant = 0.0
        self.layoutIfNeeded()
        self.micButton.isHidden = false
        self.photoButton.isHidden = false
        self.mealDiaryButton.isHidden = false
        self.presenter.onCancelButtonPressed()

    }
    
    @IBAction func onPhotoSearchPressed () {
        self.presenter.onPhotoSearchButtonPressed()
    }
    
    @IBAction func onMealDiaryPressed () {
        self.presenter.onMealDiaryButtonPressed()
    }
    
    @IBAction func onVoiceSearchPressed () {
        self.presenter.onVoiceSearchButtonPressed()
    }
    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */

}

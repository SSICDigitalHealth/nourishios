//
//  FoodCardCacheRepository.swift
//  Nourish
//
//  Created by Gena Mironchyk on 8/21/17.
//  Copyright © 2017 SSIC. All rights reserved.
//

import UIKit
struct FoodCardDetails {
    let nutrients : [Nutrient]?
    let servingTypes : [String]?
}

class FoodCardCacheRepository: NSObject {
    let foodCardCacheDataStore = FoodCardCacheDataStore ()
    
    func fetchCacheFor(foodId : String) -> FoodCardDetails? {
        return self.foodCardCacheDataStore.fetchCache(foodId: foodId)
    }

    func storeCache(foodId : String, cache: FoodCardDetails) {
        self.foodCardCacheDataStore.storeCache(date: Date(), foodId: foodId, cache: cache)
    }
}

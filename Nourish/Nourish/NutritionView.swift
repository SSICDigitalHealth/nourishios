//
//  NutritionView.swift
//  Nourish
//
//  Created by Nova on 11/21/16.
//  Copyright © 2016 SSIC. All rights reserved.
//

import UIKit

class NutritionView: BaseClockView, NutritionViewProtocol {
    
    var presenter = NutritionPresenter()
    var controller = ProgressViewController()
    var vitaminColor : UIColor = NRColorUtility.vitaminsColor()
    var electolytesColor : UIColor = NRColorUtility.electrolytesColor()
    var macronutrientsColor : UIColor = NRColorUtility.macronutrientsColor()
    var mineralColor : UIColor = NRColorUtility.mineralsColor()
    
    var vitValue : Double = 0.0
    var eleValue : Double = 0.0
    var macrValue : Double = 0.0
    var mineralValue : Double = 0.0
    var nutritionScore : Double = 0.0
    var period : period!
    var scoreLabel : UILabel!
    
    func setupWith(vitValue : Double, eleValue : Double, macroValue : Double, mineralValue : Double, score : Double, period:period) {
            self.vitValue = vitValue
            self.eleValue = eleValue
            self.macrValue = macroValue
            self.mineralValue = mineralValue
            self.backgroundColor = UIColor.white
            self.addShadow()
            self.nutritionScore = score
            self.period = period
            self.stopActivityAnimation()
            
            self.setNeedsDisplay()
            
            CATransaction.flush()
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.basePresenter = presenter
        presenter.nutritionView = self
        presenter.viewWillAppear(false)
    }
    
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        self.setUpLegends()
        let context = UIGraphicsGetCurrentContext()
        context!.clear(rect)
        context!.setFillColor((self.backgroundColor?.cgColor)!)
        context!.fill(rect)
        
        let colors = [vitaminColor.withAlphaComponent(0.2),  macronutrientsColor.withAlphaComponent(0.2),electolytesColor.withAlphaComponent(0.2),mineralColor.withAlphaComponent(0.2)]
        
        self.drawTitle()
        
        self.drawShadeСlockFace(context: context!, colorArray: colors)
        
        self.drawArc(context: context!, startAngle: CGFloat(270).degreesToRadians, endAngle: CGFloat(0).degreesToRadians, color: vitaminColor, value: CGFloat(vitValue))
        
        self.drawArc(context: context!, startAngle: CGFloat(0).degreesToRadians, endAngle: CGFloat(90).degreesToRadians, color: macronutrientsColor, value: CGFloat(macrValue))
        
        self.drawArc(context: context!, startAngle: CGFloat(90).degreesToRadians, endAngle: CGFloat(180).degreesToRadians, color: electolytesColor, value: CGFloat(eleValue))
        
        self.drawArc(context: context!, startAngle: CGFloat(180).degreesToRadians, endAngle: CGFloat(270).degreesToRadians, color: mineralColor, value: CGFloat(mineralValue))
        
        self.drawNutritionScoreWithValue(value: CGFloat(nutritionScore))
        self.drawLegends()
        self.registerGesture()
    }
    
    func drawNutritionScoreWithValue(value : CGFloat) {
        let frame = CGRect(x: self.bounds.origin.x, y: self.bounds.origin.y - defaultLegendOffset, width: self.bounds.size.width, height: self.bounds.size.height)
        let font = UIFont.systemFont(ofSize: 44, weight: UIFontWeightMedium)

        if scoreLabel == nil {
            scoreLabel = UILabel(frame: frame)
            scoreLabel.font = font
            scoreLabel.textColor = NRColorUtility.progressLabelColor()
            scoreLabel.textAlignment = .center
            scoreLabel.numberOfLines = 0
            scoreLabel?.text = String(format:"%0.f",value)
            self.addSubview(scoreLabel)
        } else {
            scoreLabel?.text = String(format:"%0.f",value)
        }
    }
    
    func setUpLegends() {
        legendArray = [(description:"Vitamins",color:vitaminColor),(description:"Macronutrients",color:macronutrientsColor),(description:"Electrolytes",color:electolytesColor),(description:"Minerals",color:mineralColor)]
    }
    
    func drawTitle() {
        let frame  = CGRect(x: 30, y: 10, width: 150, height: 22)
        let font = UIFont.systemFont(ofSize: 16)

        let title = NRDrawingUtility.drawTextLayer(frame: frame, text: "Nutrition", color: NRColorUtility.progressLabelColor().cgColor, bgColor: UIColor.clear.cgColor, font: font)
        title.alignmentMode = "left"
        self.layer.addSublayer(title)
    }
    
    func registerGesture() {
        let gesture = UITapGestureRecognizer(target: self, action: #selector (self.showNutritionDetails (_:)))
        self.addGestureRecognizer(gesture)
    }
    
    func showNutritionDetails(_ sender:UITapGestureRecognizer){
        EventLogger.logOnNutritionScreenClick()
        self.controller.openVCNutritionStats()
    }
}

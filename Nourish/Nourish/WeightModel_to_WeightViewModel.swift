//
//  WeightModel_to_WeightViewModel.swift
//  Nourish
//
//  Created by Vlad Birukov on 29.08.17.
//  Copyright © 2017 SSIC. All rights reserved.
//

import Foundation

class WeightModel_to_WeightViewModel {
    func transform(model: WeightModel) -> WeightViewModel {
        let weightViewModel = WeightViewModel()
        weightViewModel.dailyWeightLogged = model.dailyWeightLogged
        
        if model.userId != nil {
            weightViewModel.userId = model.userId
        }
        
        if model.weight != nil {
            weightViewModel.weight = [weightList]()
            weightViewModel.weight = model.weight
            weightViewModel.weight?.sort(by: {$0.subtitle < $1.subtitle})
            if model.progress != nil {
                weightViewModel.progress = model.progress
            }
            weightViewModel.isMetric = model.isMetric
        }
        return weightViewModel
    }
}

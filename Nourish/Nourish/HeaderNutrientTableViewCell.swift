//
//  HeaderNutrientTableViewCell.swift
//  Nourish
//
//  Created by Vlad Birukov on 11.08.17.
//  Copyright © 2017 SSIC. All rights reserved.
//

import UIKit

class HeaderNutrientTableViewCell: UITableViewCell {
    @IBOutlet weak var captionLabel: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func setupWith(text: String, section: Int, config: ProgressConfig) {
        
        let captionText = text.replacingOccurrences(of: "_", with: " ")
        if section == 1 {
            switch config.period {
            case .daily:
                self.captionLabel.text = String(format: "%@ this day:", captionText)
            case .weekly:
                self.captionLabel.text = String(format: "%@ this week:", captionText)
            case .monthly:
                self.captionLabel.text = String(format: "%@ this month:", captionText)
            }

        } else {
            self.captionLabel.text = String(format: "%@", captionText)
        }
    }
}

//
//  MealDiaryPresenter.swift
//  Nourish
//
//  Created by Nova on 11/10/16.
//  Copyright © 2016 Samsung. All rights reserved.
//

import UIKit
import RxSwift
import RealmSwift

let kMealDiaryAnimationTimeStamp : String = "kMealDiaryAnimationTimeStamp"

class MealDiaryPresenter: BasePresenter, UITableViewDelegate, UITableViewDataSource,MGSwipeTableCellDelegate {
    
    var tabledView : MealDiaryViewProtocol?
    var interactor = MealDiaryInteractor()
    let favouriteInt = FavouritesInteractor()
    let backgroundFavInteractor = RefreshFavoritesInteractor()
    let toModelMapper = MealRecord_to_foodSearchModel()
    var dateToFetch : Date = Date()
    var dictionaryToRepresent : [Ocasion : [MealRecordModel]] = [:]
    var arrayOfOcasions : [Ocasion] = [.breakfast, .lunch, .dinner, .snacks]
    var sections : [Int : [MealRecordModel]] = [:]
    let objectToModelMapper = MealRecord_to_MealRecordModel()
    var showAnimation : Bool = false
    var currentDirection : MGSwipeDirection? = .leftToRight
    var showMealPrediction : Bool = false
    var swipeCell : MGSwipeTableCell!
    var loaded : Bool = false
    
    var isUserInteracted = false
    
    //MARK : BasePresenterProtocol
    
    override func viewWillAppear(_ animated : Bool) {
        super.viewWillAppear(animated)
        self.dictionaryToRepresent = [:]
        let notificationName = Notification.Name(kDiaryReFetched)
        NotificationCenter.default.addObserver(self, selector: #selector(MealDiaryPresenter.reload(notification:)), name: notificationName, object: nil)
        NotificationCenter.default.addObserver(self, selector:#selector(MealDiaryPresenter.reload(notification:)), name: NSNotification.Name(rawValue: kOperationsDidFinised), object: nil)
        self.backgroundFavInteractor.execute()
        self.loaded = true
        
        self.showDataFor(date:self.dateToFetch)
    }
    
    func reload(notification : Notification) {
        print(notification)
        let dispat = DispatchTime.now() + 0.2
        DispatchQueue.main.asyncAfter(deadline: dispat, execute: {
            self.showDataFor(date: self.dateToFetch)
        })
    }
    
    // MARK: Interactor

    func showDataFor(date: Date) {
        for object in self.subscribtions {
            object.dispose()
        }
        let calendar = Calendar.current
        if calendar.startOfDay(for: self.dateToFetch) != calendar.startOfDay(for: date) {
            self.dictionaryToRepresent = [:]
        }
        self.dateToFetch = date
        DispatchQueue.main.async {
            self.tabledView?.startActivityAnimation()
        }
        self.subscribtions.append(self.reloadFoodData(date: self.dateToFetch, onlyCache: false).subscribeOn(MainScheduler.instance).subscribe(onNext: { [weak self] result in
            DispatchQueue.main.async {
                if let instance = self {
                    instance.tabledView?.stopActivityAnimation()
                    if result == true {
                        instance.showAnimation = false
                        instance.currentDirection = .leftToRight
                        instance.tabledView?.reloadMealViews()
                    }
                }
            }
            
        }, onError: { [weak self] error in
            DispatchQueue.main.async {
                if let instance = self {
                    instance.tabledView?.parseError(error:error, completion: nil)
                }
            }
            
        }, onCompleted: {}, onDisposed: {}))
    }
    
    func reloadFoodData(date: Date, onlyCache : Bool) -> Observable<Bool> {
        return self.fetchDictionaryOfFoodEntriesForDate(date: date,onlyCache: onlyCache).flatMap { dict -> Observable<Bool> in

            let needUpdate = !self.compare(dict: dict, inidict: self.dictionaryToRepresent)
            if needUpdate == true {
                self.dictionaryToRepresent = dict
                for ocasion in self.arrayOfOcasions {
                    let array = self.dictionaryToRepresent[ocasion]
                    if array?.isEmpty == false {
                        let filteredArray = array?.filter({$0.foodTitle != nil && $0.foodTitle != ""})
                        self.sections[ocasion.rawValue - 1] = filteredArray
                    } else {
                        self.sections[ocasion.rawValue - 1] = []
                    }
                }
            }
            print("needUpdate ------->\(needUpdate)")
            return Observable.just(needUpdate)
        }
    }
    func compare(dict :  [Ocasion : [MealRecordModel]], inidict :  [Ocasion : [MealRecordModel]]) -> Bool {
        var dictArr : [MealRecordModel] = []
        var indictArr : [MealRecordModel] = []
        
        for (_, value) in dict {
            dictArr.append(contentsOf: value)
        }
        
        for (_, indi) in inidict {
            indictArr.append(contentsOf: indi)
        }
        
        //dictArr = dictArr.sorted(by: {$0.date! > $1.date!})
        //indictArr = indictArr.sorted(by: {$0.date! > $1.date!})
        
        var same = true
        
        if dictArr.count == indictArr.count && dictArr.count > 1 {
            for index in 0...dictArr.count-1 {
                let obj = dictArr[index]
                let sObj = indictArr[index]
                
                if obj.isFromOperaion != sObj.isFromOperaion {
                    return false
                } else {
                    if obj.groupedMealArray.count != sObj.groupedMealArray.count {
                        return false
                    }
                    
                    if obj.groupedMealArray.count > 1 {
                        let flag = obj.isGroupFavourite == sObj.isGroupFavourite
                        if flag == false {
                            return flag
                        }
                    } else {
                        let sFlag = obj.isFavourite == sObj.isFavourite
                        if sFlag == false {
                            return sFlag
                        }
                        if let new = obj.groupedMealArray.first {
                            if new.isFavourite != sObj.groupedMealArray.first?.isFavourite {
                                return false
                            }
                        }
                    }
                }
            }
        } else {
            same = false
        }
        return same
    }
    
    func fetchDictionaryOfFoodEntriesForDate(date : Date, onlyCache : Bool) -> Observable<[Ocasion : [MealRecordModel]]> {
        let arrayOfMeals = self.interactor.arrayOfFetchedMealsForDate(date: date, onlyCache : onlyCache).flatMap({ mealArray -> Observable<[Ocasion : [MealRecordModel]]> in
            
            let arrayToWorkWith = self.objectToModelMapper.transform(recordArray: mealArray)
            let occasionDict = self.separateAllEntriesOnOccasions(records: arrayToWorkWith)
            
            var mapped = [Ocasion : [MealRecordModel]]()
            for (occasion, array) in occasionDict {
                mapped[occasion] = self.objectToModelMapper.prepareForGroups(modelArray: array)
            }
            
            return Observable.just(mapped)
        })
        
        return arrayOfMeals
    }
    
    func separateAllEntriesOnOccasions(records : [MealRecordModel]) -> [Ocasion : [MealRecordModel]] {
        var dictToReturn : [Ocasion : [MealRecordModel]] = [:]
        for record in records {
            let ocasion = record.ocasion
            var array = dictToReturn[ocasion!]
            array = array != nil ? array : []
            array?.append(record)
            dictToReturn[ocasion!] = array
        }
        return dictToReturn
    }
    
    // MARK: UITableViewDataSource
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        var count = 0
        if sections[section] != nil {
            count = self.sections[section]!.count
        }
        count += 1
        return count
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return arrayOfOcasions.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let addCell = "AddCell"
        let ocasionCell = "OcasionCell"
        let foodCell = "FoodCell"
        let arrayAtSection = sections[indexPath.section]
        let cell = UITableViewCell()
        
        if arrayAtSection?.isEmpty == true {
            if indexPath.row == 0 {
                let cell : NoOcasionTableViewCell = tableView.dequeueReusableCell(withIdentifier: addCell)! as! NoOcasionTableViewCell
                let ocasion = arrayOfOcasions[indexPath.section]
                cell.ocasionTitle?.text = Ocasion.mealTypeString(servingType: ocasion).uppercased()
                cell.selectionStyle = UITableViewCellSelectionStyle.none
                return cell
            }
        } else {
            if indexPath.row == 0 {
                let cell : AddFoodTableViewCell = tableView.dequeueReusableCell(withIdentifier: ocasionCell)! as! AddFoodTableViewCell
                let ocasion = arrayOfOcasions[indexPath.section]
                cell.ocasionTitle?.text = Ocasion.mealTypeString(servingType: ocasion).uppercased()
                cell.ocasion = ocasion
                cell.addButton?.addTarget(self, action: #selector(self.addButtonCliked(sender:)), for: .touchUpInside)
                cell.actionButton?.addTarget(self, action: #selector(self.favouriteButtonClicked(sender:)), for: .touchUpInside)
                if sections.count > 0 {
                    let mealArray =  sections[ocasion.rawValue - 1]!
                    //Check how many are marked for fav
                    let groupMeal = mealArray.filter {$0.isMarkedForFav == true}
                    var fav : favouriteAction = .none
                    if (groupMeal.count) > 0 {
                        fav = groupMeal.count == 1 ? favouriteAction.fav : favouriteAction.groupFav
                    }
                    cell.setFavAction(action: fav)
                    }
                cell.selectionStyle = UITableViewCellSelectionStyle.none
                return cell
            } else {
                let cell : MealPreviewTableViewCell = tableView.dequeueReusableCell(withIdentifier: foodCell) as! MealPreviewTableViewCell
                cell.actionButton?.addTarget(self, action: #selector(self.actionButtonCliked(sender:)), for: .touchUpInside)
                if (arrayAtSection?.count)! > (indexPath.row - 1) && indexPath.row > 0 {
                    if let record = arrayAtSection?[indexPath.row - 1] {
                        cell.setupWithModel(record: record)
                    }
                }
                
                //cell.setupWithModel(record: (arrayAtSection?[indexPath.row - 1])!)
                cell.selectionStyle = UITableViewCellSelectionStyle.none
                cell.delegate = self
                return cell
            }
        }
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: false)
        switch indexPath.row {
        case 0:
            let array = self.sections[indexPath.section]
            if  (array?.count)! > 0 {
               return
            } else {
                EventLogger.logOnAddFoodClickFor(ocasion:self.arrayOfOcasions[indexPath.section])
                self.tabledView?.showAddFoodControllerForOcasion(ocasion: self.arrayOfOcasions[indexPath.section], date: dateToFetch,showPredictions: self.shouldShowMealPrediction(ocassion: self.arrayOfOcasions[indexPath.section]))

                return
            }
        default:
            let array = self.sections[indexPath.section]
            let record = array?[indexPath.row - 1]
            
            let ocasion = arrayOfOcasions[indexPath.section]
        
            if ((record?.groupedMealArray.count)! > 1) {
                self.tabledView?.showGroupDetailsControllerForOcasion(ocasion: ocasion, date: dateToFetch, model: record!)
                return
            } else {
                var model = self.toModelMapper.transform(record: record!)
                if record?.groupedMealArray != nil && (record?.groupedMealArray.count)! > 0 {
                    model = self.toModelMapper.transform(record: (record?.groupedMealArray[0])!)
                    if record?.groupPhotoID != nil {
                        model.userPhotoId = record?.groupPhotoID
                    }
                }
                
                self.tabledView?.changeFoodAmountWith(model: model)
            }
            
            break
        }
        
    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        let arrayAtSection = sections[indexPath.section]
        
        if arrayAtSection?.isEmpty == true {
            let cell = self.tableView(tableView, cellForRowAt: indexPath)
            return cell.frame.size.height
        } else {
            
            if indexPath.row == 0 {
                let cell = self.tableView(tableView, cellForRowAt: indexPath)
                return cell.frame.size.height
            }
            var arrayCount = 0
            if let arrCount = arrayAtSection?.count {
                arrayCount = arrCount
            }
            
            if indexPath.row - 1 >= 0 && indexPath.row - 1 < arrayCount   {
                let data = arrayAtSection?[indexPath.row - 1]
                let widthPadding : CGFloat = 76
                let heightPadding : CGFloat = 65
                let titleHeight = data?.descriptionText().sizeOfBoundingRect(width: UIScreen.main.bounds.width - widthPadding, height: maxLabelSize.height, font:UIFont.systemFont(ofSize: 15, weight: UIFontWeightRegular)).height
                return heightPadding + titleHeight!
            } else {
                let cell = self.tableView(tableView, cellForRowAt: indexPath)
                return cell.frame.size.height
            }
        }
    }
    
    // MARK: IBAction implementation
    
    func addButtonCliked(sender : UIButton){
        let cell = sender.superview?.superview?.superview as! AddFoodTableViewCell
        let section = cell.ocasion?.rawValue
        self.tabledView?.showAddFoodControllerForOcasion(ocasion: self.arrayOfOcasions[section! - 1], date: dateToFetch,showPredictions: self.shouldShowMealPrediction(ocassion: cell.ocasion!))
        EventLogger.logOnAddFoodClickFor(ocasion:self.arrayOfOcasions[section! - 1])
    }
    
    
    func favouriteButtonClicked(sender : UIButton){
        let cell = sender.superview?.superview?.superview as! AddFoodTableViewCell
        let section = (cell.ocasion?.rawValue)! - 1

        if sections.count > 0 {
            let mealArray =  sections[section]!
            //Check how many are marked for fav
            let groupMeal = mealArray.filter {$0.isMarkedForFav == true}
            if (groupMeal.count) ==  1 {
                DispatchQueue.main.async {
                 //   self.tabledView?.startActivityIndicator()
                }

                let model = groupMeal.first!
                let nameForItem = model.foodId ?? NSUUID().uuidString
                self.subscribtions.append(favouriteInt.addToFavourites(models: groupMeal, name : nameForItem).subscribe(onNext: { success in
                    model.isMarkedForFav = false
                }, onError: {error in }, onCompleted: {
                    let dispat = DispatchTime.now() + 0.2
                    DispatchQueue.main.asyncAfter(deadline: dispat, execute: {
                        self.showDataFor(date: self.dateToFetch)
                    })
                }, onDisposed: {}))
                self.showDataFor(date: self.dateToFetch)
            } else if groupMeal.count > 1{
                self.tabledView?.showGroupFavouriteVC(mealGroup: groupMeal)
            }
        }
    }

     func actionButtonCliked(sender : UIButton){
        let cell = sender.superview?.superview?.superview as! MealPreviewTableViewCell
        let section = (cell.ocassion?.rawValue)! - 1
        var mealArray : [MealRecordModel] = []
        
        mealArray =  sections[section]!
        if mealArray.count > 0 {
            var currentMeal = mealArray.filter {$0.userFoodID == cell.userMealId || $0.localCacheID == cell.userMealId}
            if currentMeal.count > 0 {
                if currentMeal[0].isFavourite {
                    sender.setImage(UIImage(named:"fav_inactive"), for: .normal)
                } else if currentMeal[0].isGroupFavourite {
                    sender.setImage(UIImage(named:"favgroup"), for: .normal)
                } else if currentMeal[0].isGroupFavourite == false && currentMeal[0].groupedMealArray.count > 1 {
                    sender.setImage(UIImage(named:"fav_grp"), for: .normal)
                }else if currentMeal[0].isMarkedForFav  {
                    sender.setImage(UIImage(named:"circle_select"), for: .normal)
                    currentMeal[0].isMarkedForFav = false
                } else {
                    sender.setImage(UIImage(named:"circle_selected"), for: .normal)
                    currentMeal[0].isMarkedForFav = true
                }
            }
        }
        
        //Reload section header
        self.tabledView?.reloadAddTableCell(section: section)
    }
    
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if cell.isKind(of: MealPreviewTableViewCell.self) {
            let selectedcell = cell as! MealPreviewTableViewCell
            let section = (selectedcell.ocassion?.rawValue ?? 1) - 1
            var mealArray : [MealRecordModel] = []
        
            mealArray =  sections[section]!
            if !self.hasShownSwipeAnimation() {
                if !self.showAnimation && mealArray.count > 0 {
                    self.showAnimation = true
                    let when = DispatchTime.now() + 1
                    DispatchQueue.main.asyncAfter(deadline: when) {
                        if self.swipeCell != nil {
                            self.currentDirection = .leftToRight
                        }
                        self.swipeCell = cell as! MGSwipeTableCell
                        self.showAnimations(cell:self.swipeCell)
                    }
                }
            }

        }
    }
    
    func showAnimations(cell:MGSwipeTableCell) {
        if cell == self.swipeCell && isUserInteracted == false {
            let index =  self.tabledView?.getTableViewIndexPath(cell: cell)
            if !(index?.isEmpty)! {
                cell.showSwipe(currentDirection!, animated: true) { (Bool) in
                    self.currentDirection = self.currentDirection == .leftToRight ? .rightToLeft : nil
                    self.hideAnimation(cell: cell)
                }
            }
        }
    }
    
    func hideAnimation(cell:MGSwipeTableCell) {
        if cell == self.swipeCell {
            let when = DispatchTime.now() + 1
            DispatchQueue.main.asyncAfter(deadline: when) {
                cell.hideSwipe(animated: true, completion: { (Bool) in
                    if self.currentDirection != nil {
                        self.showAnimations(cell: cell)
                    } else {
                        //Animation is complete
                        if !self.hasShownSwipeAnimation() {
                            let dateFormater = DateFormatter()
                            dateFormater.dateStyle = .short
                            let dateString = dateFormater.string(from: Date())
                            UserDefaults.standard.set(dateString, forKey: kMealDiaryAnimationTimeStamp)
                            UserDefaults.standard.synchronize()
                        }
                    }
                })
            }
        }
    }
    
    func hasShownSwipeAnimation() -> Bool {
        var canShowSwipe : Bool = false
        let dateFormater = DateFormatter()
        dateFormater.dateStyle = .short
        
        if UserDefaults.standard.value(forKey: kMealDiaryAnimationTimeStamp) != nil {
            let lastAnimationDate = UserDefaults.standard.value(forKey: kMealDiaryAnimationTimeStamp) as! String
            let animationDate = dateFormater.date(from: lastAnimationDate) ?? Date()
            
            let calendar = Calendar.current
            if calendar.isDateInToday(animationDate) {
                canShowSwipe = true
            }
        }
        
        return canShowSwipe
    }
    
    func swipeTableCell(_ cell: MGSwipeTableCell, canSwipe direction: MGSwipeDirection) -> Bool {
        let index =  self.tabledView?.getTableViewIndexPath(cell: cell)
        if !(index?.isEmpty)! {
            let section = sections[(index?.section)!]
            let indexRow = (index?.row)! - 1
            if section != nil && (section?.count)! >= indexRow - 2 {
                let record = section?[(index?.row)! - 1]
                if direction == MGSwipeDirection.leftToRight {
                    if (record?.isFavourite)!  || (record?.isGroupFavourite)!{
                        return false
                    }
                }
            } else {
                return false
            }
            
        } else {
            return false
        }
        return true
    }
    
    
    func swipeTableCell(_ cell: MGSwipeTableCell, swipeButtonsFor direction: MGSwipeDirection, swipeSettings: MGSwipeSettings, expansionSettings: MGSwipeExpansionSettings) -> [UIView]? {
        
        swipeSettings.transition = MGSwipeTransition.border;
        expansionSettings.buttonIndex = 0;
        let index =  self.tabledView?.getTableViewIndexPath(cell: cell)
        let section = sections[(index?.section)!]
        let record = section?[(index?.row)! - 1]
        
        if direction == MGSwipeDirection.leftToRight {
            expansionSettings.fillOnTrigger = true;
            expansionSettings.threshold = 1;
            
            return [
                MGSwipeButton(title: "Favourite", icon: UIImage(named:"favtab_white_icon"), backgroundColor: NRColorUtility.favItemColor(), callback: { (cell) -> Bool in
                    print("Fav button tapped")
                    self.tabledView?.startActivityAnimation()
                    if (record?.groupedMealArray.count)! > 0 {
                        let name = record?.groupName
                        if name != nil && record?.groupID != nil {
                            self.subscribtions.append(self.favouriteInt.addGroupToFav(foodGroupID: (record?.groupID)!, name: name!, userPhotoID: record?.groupPhotoID).subscribe(onNext: {success in
                                
                            }, onError: {error in
                            LogUtility.logToFile(error)
                                DispatchQueue.main.async {
                                    self.tabledView?.parseError(error: error, completion: nil)
                                }
                            }, onCompleted: {
                                self.showDataFor(date: self.dateToFetch)
                            }, onDisposed: {}))
                        }
                    } else {
                        let nameForItem = record?.foodId ?? NSUUID().uuidString
                        self.subscribtions.append(self.favouriteInt.addToFavourites(models: [record!], name : nameForItem).subscribe(onNext: {success in
                        }, onError: {error in
                            DispatchQueue.main.async {
                                self.tabledView?.parseError(error: error, completion: nil)
                            }
                            LogUtility.logToFile(error)
                        }, onCompleted: {
                            self.showDataFor(date: self.dateToFetch)
                        }, onDisposed: {}))
                    }
                    
                    return true
                })
            ]
        }
        else {
            expansionSettings.fillOnTrigger = false;
            expansionSettings.threshold = 3;
            return [
                MGSwipeButton(title: "Delete", icon: UIImage(named:"deleteitem_icon"), backgroundColor: NRColorUtility.deleteItemColor(), callback: { (cell) -> Bool in
                    let ident = record?.userFoodID ?? record?.localCacheID
                    if record?.groupedMealArray != nil && (record?.groupedMealArray.count)! > 0 {
                        self.subscribtions.append(self.interactor.deleteGroupItems(array: (record?.groupedMealArray)!).subscribe(onNext: {score in
                            print("Current food score \(score)")
                            print("item deleted")
                        }, onError: {error in
                            DispatchQueue.main.async {
                                self.tabledView?.parseError(error: error, completion: nil)
                            }
                        }, onCompleted: {
                        }, onDisposed: {}))
                        self.showDataFor(date: self.dateToFetch)

                    } else {
                        if ident != nil {
                            self.subscribtions.append(self.interactor.deleteFoodItem(foodID: [ident!],date:self.dateToFetch,saveChat:true).subscribe(onNext: {score in
                                print("Current food score \(score)")
                                print("item deleted")
                                
                            }, onError: {error in
                                DispatchQueue.main.async {
                                    self.tabledView?.parseError(error: error, completion: nil)
                                }
                            }, onCompleted: {
                            }, onDisposed: {}))
                            
                            self.showDataFor(date: self.dateToFetch)
                        }
                    }
                    
                    return true
                })
            ]
        }
    }
    
    func shouldShowMealPrediction(ocassion:Ocasion) -> Bool{
        let dateFormat = DateFormatter()
        dateFormat.dateFormat = "MM//DD/YYYY"
        
        //Show predictions only for today
        if dateFormat.string(from:self.dateToFetch) != dateFormat.string(from: Date()) {
            return false
        }
        
        // Check if meal is already logged for the ocassion
        /*let mealArray = sections[ocassion.rawValue - 1]
        if (mealArray?.count)! > 0 {
            return false
        }*/
        
        return true
    }
}


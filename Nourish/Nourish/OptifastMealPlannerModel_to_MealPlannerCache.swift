//
//  OptifastMealModel_to_MealPlannerCache.swift
//  Optifast
//
//  Created by Gena Mironchyk on 1/11/18.
//  Copyright © 2018 SSIC. All rights reserved.
//

import UIKit

class OptifastMealPlannerModel_to_MealPlannerCache: NSObject {
    func transform(model : OptifastMealPlannerModel) -> MealPlannerCache {
        let cache = MealPlannerCache()
        cache.totalCalories = model.targetCalories
        cache.score = model.score
        cache.date = model.date
        var arrayOfOptifoods = [[String : Any]]()
        for optiFood in model.optifastFoods {
            arrayOfOptifoods.append(self.dictionaryFrom(model: optiFood))
        }
        cache.optifastMealsData = self.dataFrom(array: arrayOfOptifoods)
        
        if let foods = model.foods {
            cache.ocasionData.append(contentsOf: self.dataFrom(mealModelArray: foods))
        }
        
        return cache
    }
    
    
    private func dictionaryFrom(model : foodSearchModel) -> [String : Any] {
        var foodSearchDictionary = [String : Any]()
        foodSearchDictionary["amount"] = model.amount
        foodSearchDictionary["unit"] = model.unit
        foodSearchDictionary["food_id"] =  model.foodId
        foodSearchDictionary["food_name"] = model.foodTitle
        foodSearchDictionary["grams"] = model.grams
        foodSearchDictionary["kcal_per_gram"] = model.caloriesPerGram
        return foodSearchDictionary
    }
    
    private func foodSearchModelFrom(dict: [String : Any]) -> foodSearchModel {
        var model = foodSearchModel()
        model.amount = dict["amount"] as? Double ?? 0.0
        model.unit = dict["unit"] as? String ?? ""
        model.foodId = dict["food_id"] as? String ?? ""
        model.mealIdString = dict["food_id"] as? String ?? ""
        model.foodTitle = dict["food_name"] as? String ?? ""
        model.grams = dict["grams"] as? Double
        model.caloriesPerGram = dict["kcal_per_gram"] as? Double ?? 0.0
        model.calories = String((dict["kcal_per_gram"] as? Double ?? 0.0) * (dict["grams"] as? Double ?? 0.0))
        return model
    }
    
    private func dataFrom(mealModelArray : [OptifastMealModel]) -> [OcasionData] {
        var arrayToReturn = [OcasionData]()
        for model in mealModelArray {
            let data = OcasionData()
            data.calsTotal = model.totalCalories
            let realmString = RealmString()
            realmString.stringValue = Ocasion.mealPlannerJsonRepresentation(ocasion: model.occasion)
            data.ocasionName = realmString
            var mealsData = [[String : Any]]()
            for ocasionFood in model.ocasionFoods {
                mealsData.append(self.dictionaryFrom(model: ocasionFood.meal))
                data.recipiesData.append(self.recipeFrom(model: ocasionFood.mealReciepe))
            }
            data.mealsData = self.dataFrom(array: mealsData)
            arrayToReturn.append(data)
        }
        
        
        return arrayToReturn
    }
    
    
    private func recipeFrom(model: RecipiesModel?) -> RecipiesCacheModel {
        let recipesCache = RecipiesCacheModel()
        
        if model != nil {
            recipesCache.foodId = model!.foodId
            recipesCache.amount = model!.amount
            recipesCache.nameFood = model!.nameFood
        
        if model!.ingredient.count > 0 {
            for stringObj in model!.ingredient {
                let realmStr = RealmString()
                realmStr.stringValue = stringObj
                recipesCache.ingredient.append(realmStr)
            }
        }
        if model!.groupId != nil {
            recipesCache.groupId = model!.groupId
        }
        
        if model!.image != nil{
            recipesCache.pathImage = model!.pathForImage
        }
        
        recipesCache.numberCal = model!.numberCal
        recipesCache.grams = model!.grams
        recipesCache.unit = model!.unit
        
        if model!.instructions.count > 0 {
            for stringObj in model!.instructions {
                let realmStr = RealmString()
                realmStr.stringValue = stringObj
                recipesCache.instruction.append(realmStr)
            }
        }
        
        recipesCache.isFavorite = model!.isFavorite
        }
        return recipesCache
    }
    
    
    private func dataFrom(array : [[String : Any]]) -> Data {
        return try! JSONSerialization.data(withJSONObject: array, options: .prettyPrinted)
    }
}

//
//  HomeScreenModel.swift
//  Nourish
//
//  Created by Andrey Zmushko on 9/12/17.
//  Copyright © 2017 SSIC. All rights reserved.
//

import UIKit

class HomeScreenModel: NSObject {

    var avatar: UIImage?
    var score: Double?
    var grains: Double?
    var protein: Double?
    var dairy: Double?
    var vegetable: Double?
    var fruits: Double?
    var consumed: Double?
    var burned: Double?
    var activity: Double?
    var sleep: Double?
    var stress: stressLevel?
    
}

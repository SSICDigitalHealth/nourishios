//
//  DistanceInteractor.swift
//  Nourish
//
//  Created by Vlad Birukov on 31.08.17.
//  Copyright © 2017 SSIC. All rights reserved.
//

import Foundation
import RxSwift

class DistanceInteractor {
    let repo = NewActivityRepository.shared
    
    func execute(startDate: Date, endDate: Date) -> Observable <DistanceModel> {
        return repo.distanceModel(startDate:startDate,endDate:endDate)
    }
    
    private func generateRandomBetween(max : Int, min : Int) -> Int {
        return min + Int(arc4random_uniform(UInt32(max - min + 1)))
    }
}

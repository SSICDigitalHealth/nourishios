//
//  NRHomeViewController.swift
//  Nourish
//
//  Created by Vino (Vinodha) Sundaramoorthy on 10/20/16.
//  Copyright © 2016 SSIC. All rights reserved.
//

import UIKit
import HealthKit
import RxSwift

private let reuseIdentifier = "kPickerCell"
typealias HKCompletionHandleForStep = (Double?, NSError?) -> ()

class NRHomeViewController: BasePresentationViewController,UIGestureRecognizerDelegate , UITableViewDelegate,UITableViewDataSource  {
    
    @IBOutlet weak var menuTableViewHeightConstraint : NSLayoutConstraint?
    @IBOutlet weak var menuTableView : UITableView!
    let nutrientsInteractor = NutritionInteractor()

    @IBOutlet weak var chatView:ChatView!
    var score : Double = 0
    var globalNavigationButton : UIBarButtonItem!
    var scoreReloaded : Bool = false

    var menuViewDataSource : [(label :String,imageName:String)] = [(label:"Log Food",imageName:"ic_chat_action_menu_log_food"),(label:"Today's Update",imageName:"ic_chat_action_menu_todays_update"),(label:"Recommendations",imageName:"ic_chat_action_menu_recomendations")]

    func setupView (){
        self.view.backgroundColor = NRColorUtility.nourishHomeColor()
       // self.setUpNavigationBarButtons()
        //TO DO - Round corners not working as expected in 6plus and 7 plus screens , there is an offset and shadow issue with it
        //self.menuTableView.roundCorners(corners: [.topLeft,.topRight], radius: 12)
        self.menuTableView.isHidden = true
        self.baseViews = [self.chatView!]
        self.chatView!.viewDidLoad()
        //chatView.homeView = self
        
        //Initiate the work flow
        self.menuTableViewHeightConstraint?.constant = 0
    }
    
    func hideMenu(){
        self.menuTableViewHeightConstraint?.constant = 0
        UIView.animate(withDuration: 0.3, animations: {
            self.view.layoutIfNeeded()
        })
    }
    
    func onboardingCompleted (){
        self.menuTableViewHeightConstraint?.constant = 218
        self.setUpActionMenu()
        UIView.animate(withDuration: 0.3, animations: {
            self.view.layoutIfNeeded()
        })
        ActivityUtility.getHealthKitPermission(completion: {granted, error in
        })

    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.score = MealHistoryRepository.shared.scoreForDate(date: Date())
        self.scoreReloaded = false
        self.updateDailyScore()
        self.menuTableView.reloadData()
    }
    
    func updateDailyScore () {
        let _ = self.nutrientsInteractor.getNutritionScoreForToday().observeOn(MainScheduler.instance).subscribe(onNext: { [unowned self] score in
            self.score = score
        }, onError: {error in },
           onCompleted: {
            self.scoreReloaded = true
            self.menuTableView.reloadData()
        },
           onDisposed: {})

    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupView()
    }
    
    // MARK: View Set up
    //TO DO : If the same navigation bar has t appear for other VC then it can be moved to a common super class
    //Home screen navigation
    func setUpNavigationBarButtons () {
        self.navigationController?.navigationBar.barTintColor = NRColorUtility.nourishNavigationColor()
        self.navigationController?.navigationBar.isTranslucent = false;
        
        let logoContainer = UIView(frame: CGRect(x: 50, y: 5, width: 200, height: 30))
        let logoImage = UIImage(named:kHeaderLogo)?.withRenderingMode(.alwaysOriginal)
        let logoButton = UIButton(frame: CGRect(x: 50, y: 5, width: 90, height: 25))
        logoButton.setBackgroundImage(logoImage, for: .normal)
        logoButton.isUserInteractionEnabled = false
        logoButton.tintColor = UIColor.white
        logoContainer.addSubview(logoButton)
        self.navigationItem.titleView = logoContainer
        self.navigationItem.setLeftBarButtonItems([globalNavigationButton], animated: false)
    }
    
    func setUpActionMenu () {
        self.menuTableView.isHidden = false
        self.menuTableView.delegate = self
        self.menuTableView.dataSource = self
        self.menuTableView.reloadData()
    }
    
    
    func openFoodDiaryFor(date : Date) {
        let recVC = MealDiaryViewController(nibName: "MealDiaryViewController", bundle: nil)
        recVC.dateToFetch = date
        let navigationController = BaseNavigationController.init(rootViewController: recVC)
        self.present(navigationController, animated: true, completion: nil)

    }
    
    func showDailyReportFor(date: Date) {
        let recVC = ReportsViewController(nibName: "ReportsViewController", bundle: nil)
        let navigationController = BaseNavigationController.init(rootViewController: recVC)
        self.present(navigationController, animated: true, completion: {
            recVC.showDetailedReport(date: date)
        })
    }
    
    func showScoreFeedback(date : Date) {
        let scoreVC = ScoreFeedbackViewController(nibName: "ScoreFeedbackViewController", bundle: nil)
        scoreVC.providesPresentationContextTransitionStyle = true;
        scoreVC.definesPresentationContext = true;
        scoreVC.modalPresentationStyle=UIModalPresentationStyle.overCurrentContext
        scoreVC.view.backgroundColor = UIColor.black.withAlphaComponent(0.3)
        scoreVC.date = date
        self.present(scoreVC, animated: false, completion: nil)
    }
    
    // MARK: Table View Delgates & Data Source
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        //TODO : Open corresponding View Controller
        if indexPath.row == 0 {
           self.openFoodDiaryFor(date: Date())
            
        } else if (indexPath.row == 2) {
            /*
            let recVC = NRRecommendationViewController(nibName: "NRRecommendationViewController", bundle: nil)
            let navigationController = BaseNavigationController.init(rootViewController: recVC)
            self.present(navigationController, animated: true, completion: nil)
            */
            if #available(iOS 10.0, *) {
                let vc = CameraSnapViewController(nibName : "CameraSnapViewController", bundle : nil)
                //let navCtr = BaseNavigationController(rootViewController : vc)
                self.present(vc, animated: true, completion: nil)
            } else {
                // Fallback on earlier versions
            }
            
            
        } else if (indexPath.row == 1) {
            let recVC = ProgressViewController(nibName: "ProgressViewController", bundle: nil)
            let navigationController = BaseNavigationController.init(rootViewController: recVC)
            self.present(navigationController, animated: true, completion: nil)
        }
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.menuViewDataSource.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
            let cell = UITableViewCell(style: .default, reuseIdentifier: "actionMenuCell")
            cell.textLabel?.text = self.menuViewDataSource[indexPath.row].label
            cell.textLabel?.font = UIFont.systemFont(ofSize: 15)
            cell.textLabel?.textColor = NRColorUtility.actionMenuTextColor()
            let itemSize = CGSize(width: 50, height: 50)
            UIGraphicsBeginImageContextWithOptions(itemSize, false, UIScreen.main.scale);
            let imageRect = CGRect(x: 0, y: 0, width: itemSize.width, height: itemSize.height)
            cell.imageView?.image = UIImage(named:self.menuViewDataSource[indexPath.row].imageName)
            cell.imageView?.image!.draw(in: imageRect)
            cell.imageView?.image! = UIGraphicsGetImageFromCurrentImageContext()!
            cell.layoutMargins = UIEdgeInsets.zero
            cell.preservesSuperviewLayoutMargins = false;
            cell.separatorInset = UIEdgeInsets.zero
            cell.selectionStyle = .none
            UIGraphicsEndImageContext();
        
        if self.menuViewDataSource[indexPath.row].label == "Today's Update" {
            if self.scoreReloaded {
                let score : UILabel = UILabel(frame: CGRect(x: 0, y: 20, width: 85, height: 30))
                let scoreFormatter    = NumberFormatter()
                scoreFormatter.minimumFractionDigits = 0
                scoreFormatter.maximumFractionDigits = 2
                let finalScore = scoreFormatter.string(from: NSNumber(value: self.score))
                score.text = finalScore
                score.textColor = UIColor.white
                score.font = UIFont.systemFont(ofSize: 16, weight: UIFontWeightMedium)
                score.textAlignment = .center
                score.backgroundColor = UIColor.clear
                cell.contentView.addSubview(score)
            } else {
                let loadingImage = UIImageView(frame: CGRect(x: 0, y: 0, width: 45, height: 45))
                let imageArray : [UIImage] = [UIImage(named: "48spinner_00.png")!,UIImage(named: "48spinner_01.png")!,UIImage(named: "48spinner_02.png")!,UIImage(named: "48spinner_03.png")!,UIImage(named: "48spinner_04.png")!,UIImage(named: "48spinner_05.png")!,UIImage(named: "48spinner_06.png")!,UIImage(named: "48spinner_07.png")!]
                loadingImage.center = CGPoint(x: 40, y: cell.bounds.height/2 + 15)
                loadingImage.animationImages = imageArray
                loadingImage.animationDuration = 1
                cell.contentView.addSubview(loadingImage)
                loadingImage.startAnimating()
            }
        }

            return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 73
    }
    
}


//
//  HydrationBalanceModel.swift
//  Nourish
//
//  Created by Vlad Birukov on 14.06.17.
//  Copyright © 2017 SSIC. All rights reserved.
//

import Foundation


class HydrationBalanceModel : NSObject {
    var unit: String = ""
    var total: Double = 0.0
    var arrHydrationElement : [ElementModel]?
    var message : String = ""
}

//
//  VoiceSearchConstants.swift
//  Nourish
//
//  Created by Vino (Vinodha) Sundaramoorthy on 7/10/17.
//  Copyright © 2017 SSIC. All rights reserved.
//

import UIKit

// Sound Hound response attributes
let kVoiceSpokenRespone : String = "SpokenResponseLong"
let kVoiceFoodRespone : String = "InformationNuggets"
let kVoiceSearchResults : String = "AllResults"
let kVoiceSearchExitString : String = "Exiting."
let kVoiceUnrecognizedWords : String = "UnrecognizedWords"
let kVoiceConversationState : String = "ConversationState"
let kVoiceFoodLoggingDomain : String = "SamsungFoodLoggingInputData"


let kSoundHoundSourceDB : String = "SR28"

// VC Title
let kVoiceMealListVCTitle : String = "When did you eat this ?"


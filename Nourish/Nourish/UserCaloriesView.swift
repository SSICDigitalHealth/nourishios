//
//  UserCaloriesView.swift
//  Nourish
//
//  Created by Vlad Birukov on 07.06.17.
//  Copyright © 2017 SSIC. All rights reserved.
//

import UIKit

protocol UserCaloriesProtocol : BaseViewProtocol {
    func setupWithModel(model: CaloriesViewModel)
    func config() -> ProgressConfig?
}

class UserCaloriesView: ProgressView, UserCaloriesProtocol {

    @IBOutlet var userCaloriesView: UIView!
    @IBOutlet weak var consumedEatCalories: UILabel!
    @IBOutlet weak var targetEatCalories: UILabel!
    @IBOutlet weak var eatCaloriesChart: CaloricChartView!
    @IBOutlet weak var consumedActivityCalories: UILabel!
    @IBOutlet weak var targetActivityCalories: UILabel!
    @IBOutlet weak var activityCaloriesChart: CaloricChartView!
    
    var presenter: UserCaloriesPresenter!
    
    let eatColor = NRColorUtility.hexStringToUIColor(hex: "70c397")
    let activityColor = NRColorUtility.appBackgroundColor()

    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.presenter.userCaloriesView = nil
        self.presenter = nil
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        self.presenter = UserCaloriesPresenter()
        if let config = self.progressConfig {
            self.renderView(config)
        }
        self.basePresenter = self.presenter
        self.presenter.userCaloriesView = self

        super.viewWillAppear(animated)
    }
    
    func setupWithModel(model: CaloriesViewModel) {
        if let caloricData = model.caloricState {
            self.activityCaloriesChart.setupWith(consumedCalories: caloricData[0].cal.userBurnedCal, targetCalories: caloricData[0].cal.maxBurnedCal, consumedColor: self.activityColor)
            self.eatCaloriesChart.setupWith(consumedCalories: caloricData[0].cal.userConsumedCal, targetCalories: caloricData[0].cal.maxConsumedCal, consumedColor: self.eatColor)
            
            self.consumedEatCalories.text = String(format: "%0.f kcal", caloricData[0].cal.userConsumedCal)
            self.consumedActivityCalories.text = String(format: "%0.f kcal", caloricData[0].cal.userBurnedCal)
            self.targetActivityCalories.text = String(format: "%0.f kcal", caloricData[0].cal.maxBurnedCal)
            self.targetEatCalories.text = String(format: "%0.f kcal", caloricData[0].cal.maxConsumedCal)
        }
    }
    
    override func renderView(_ config: ProgressConfig) {
        super.renderView(config)
        self.userCaloriesView =  UINib(nibName: "UserCaloriesView", bundle: nil).instantiate(withOwner: self, options: nil)[0] as! UIView
        self.loadContent(contentView: userCaloriesView)
        self.loadWhiteThemeWith(header: "Calories consumed")
        self.activityCaloriesChart.backgroundColor = NRColorUtility.caloriesGraphBackgroundColor()
        self.consumedActivityCalories.textColor = NRColorUtility.appBackgroundColor()
        
    }
    
    override func setupWith(Model model: AnyObject) {
        super.setupWith(Model: model)
    }
    
    override func gestureTapAction() {
        if let hook = tapHook {
            hook(.calories)
        }
    }
    
    func settingButtomSpace() {
        self.contentHolderBottom.constant = 13
    }

}

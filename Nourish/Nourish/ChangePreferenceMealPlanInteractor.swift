//
//  ChangePreferenceMealPlanInteractor.swift
//  NourIQ
//
//  Created by Vlad Birukov on 23.10.2017.
//  Copyright © 2017 SSIC. All rights reserved.
//

import Foundation
import RxSwift

class ChangePreferenceMealPlanInteractor {
    private let mealPlannerRepository = MealPlannerRepository()

    func execute(date: Date, lunch: Int, breakfast: Int, dinner: Int, snaks: Int) -> Observable<Bool> {
        return self.mealPlannerRepository.changePreferenceMealPlan(date: date, lunch: lunch, dinner: dinner, breakfast: breakfast, snacks: snaks)
    }
    
}

//
//  UserCaloriesInteractor.swift
//  Nourish
//
//  Created by Vlad Birukov on 07.06.17.
//  Copyright © 2017 SSIC. All rights reserved.
//

import Foundation
import RxSwift
import HealthKit

class UserCaloriesInteractor {
    
    let repository = NestleProgressRepository.shared
    let activityRepo = UserActivityRepository()
    
    func execute(startDate: Date, endDate: Date) -> Observable<CaloriesModel> {
        let activeCals = activityRepo.getActiveCaloriesFor(period: .today)
        let progressModel = repository.getProgressFor(startDate: Date(), endDate: Date(), returnHistory: true)
        
        let zip = Observable.zip(activeCals, progressModel, resultSelector: {activeCalories, progress -> CaloriesModel in
            progress.userCallories.сaloriesForActivity = self.realActiveValueFromDouble(samples: activeCalories)
            return progress.userCallories
        })

        return zip
    }

    private func realActiveValueFromDouble(samples: [HKSample]) -> Double {
        var summary = 0.0
            
        for sample in samples {
            let key = sample as! HKQuantitySample
            summary = summary + key.quantity.doubleValue(for: HKUnit.kilocalorie())
        }
        return summary
    }
}

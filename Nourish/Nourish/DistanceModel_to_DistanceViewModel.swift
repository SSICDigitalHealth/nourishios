//
//  DistanceModel_to_DistanceViewModel.swift
//  Nourish
//
//  Created by Vlad Birukov on 29.08.17.
//  Copyright © 2017 SSIC. All rights reserved.
//

import Foundation

class DistanceModel_to_DistanceViewModel {
    func transform(model: DistanceModel) -> DistanceViewModel {
        let distanceViewModel = DistanceViewModel()
        distanceViewModel.distance = model.distance
        if model.avgDistance != nil {
            distanceViewModel.avgDistance = model.avgDistance
        }
        distanceViewModel.isMetric = model.isMetric
        
        return distanceViewModel
    }
}

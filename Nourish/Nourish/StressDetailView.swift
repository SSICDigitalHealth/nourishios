//
//  StressDetailView.swift
//  Nourish
//
//  Created by Vlad Birukov on 17.08.17.
//  Copyright © 2017 SSIC. All rights reserved.
//

import UIKit

protocol StressDetailProtocol : BaseViewProtocol {
    func reloadTable()
    func registerTableViewCells()
}

class StressDetailView: BaseView, StressDetailProtocol {
    @IBOutlet weak var detailStressTableView: UITableView!

    override func viewDidLoad() {
        self.xibAutoLayouting(nibName: "StressDetailView")
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.registerTableViewCells()
        super.viewWillAppear(animated)
    }
    
    func reloadTable() {
        self.detailStressTableView.reloadData()
    }
    
    func registerTableViewCells() {
        self.detailStressTableView?.register(UINib(nibName: "HeaderStressTableViewCell", bundle: nil), forCellReuseIdentifier: "cellHeaderStress")
        self.detailStressTableView?.register(UINib(nibName: "StressDetailTableViewCell", bundle: nil), forCellReuseIdentifier: "cellStressDetail")

    }


}

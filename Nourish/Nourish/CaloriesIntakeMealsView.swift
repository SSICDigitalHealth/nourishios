//
//  CaloriesIntakeMealsView.swift
//  Nourish
//
//  Created by Vlad Birukov on 20.06.17.
//  Copyright © 2017 SSIC. All rights reserved.
//

import UIKit

protocol CaloriesIntakeMealsProtocol : BaseViewProtocol {
    func setupWithModel(model: CaloriesIntakeMealsViewModel)
    func config() -> ProgressConfig?
}

class CaloriesIntakeMealsView: BaseView, CaloriesIntakeMealsProtocol {
    @IBOutlet weak var breakfastLabel: UILabel!
    @IBOutlet weak var lunchLabel: UILabel!
    @IBOutlet weak var dinnerLabel: UILabel!
    @IBOutlet weak var snacksDrinksLabel: UILabel!
    @IBOutlet weak var lunchImage: UIImageView!
    @IBOutlet weak var breakfastImage: UIImageView!
    @IBOutlet weak var dinnerImage: UIImageView!
    @IBOutlet weak var snackDrinksImage: UIImageView!
    @IBOutlet var presenter: CaloriesIntakeMealsPresenter!
    @IBOutlet weak var progressCaloriesMealsView: ProgressCaloriesMealsView!
    
    let inactiveColor = NRColorUtility.hexStringToUIColor(hex: "#bfbfbf")
    
    var progressConfig: ProgressConfig?
    
    func config() -> ProgressConfig? {
        return self.progressConfig
    }
    
    override func viewDidLoad() {
        self.xibAutoLayouting(nibName: "CaloriesIntakeMealsView")
        cornerImage()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.basePresenter = presenter
        self.presenter.caloriesIntakeMealsView = self
        
        super.viewWillAppear(animated)
    }
    
  

    func setupWithModel(model: CaloriesIntakeMealsViewModel) {
        var breakfastPercent = 0.0
        var lunchPercent = 0.0
        var dinnerPercent = 0.0
        var snackDrinksPercent = 0.0
        
        for caloriesMeal in model.arrCaloriesMeal {
            switch caloriesMeal.type {
            case .breakfast:
                if caloriesMeal.percent != 0.0 {
                    self.breakfastLabel.text = String(format: "%@ - %.0f%@", Ocasion.stringRepresentation(servingType : caloriesMeal.type), caloriesMeal.percent, "%")
                    breakfastPercent = caloriesMeal.percent
                } else {
                    self.breakfastLabel.text = String(format: "%@", Ocasion.stringRepresentation(servingType : caloriesMeal.type))
                    self.breakfastImage.backgroundColor = self.inactiveColor
                    self.breakfastLabel.textColor = self.inactiveColor
                }
            case .lunch:
                if caloriesMeal.percent != 0.0 {
                    self.lunchLabel.text = String(format: "%@ - %.0f%@", Ocasion.stringRepresentation(servingType : caloriesMeal.type), caloriesMeal.percent, "%")
                    lunchPercent = caloriesMeal.percent
                } else {
                    self.lunchLabel.text = String(format: "%@", Ocasion.stringRepresentation(servingType : caloriesMeal.type))
                    self.lunchImage.backgroundColor = self.inactiveColor
                    self.lunchLabel.textColor = self.inactiveColor
                }
            case .dinner:
                if caloriesMeal.percent != 0.0 {
                    self.dinnerLabel.text = String(format: "%@ - %.0f%@", Ocasion.stringRepresentation(servingType : caloriesMeal.type), caloriesMeal.percent, "%")
                    dinnerPercent = caloriesMeal.percent
                } else {
                    self.dinnerLabel.text = String(format: "%@ ", Ocasion.stringRepresentation(servingType : caloriesMeal.type))
                    self.dinnerImage.backgroundColor = self.inactiveColor
                    self.dinnerLabel.textColor = self.inactiveColor
                }
            case .snacks:
                if caloriesMeal.percent != 0.0 {
                    self.snacksDrinksLabel.text = String(format: "%@ - %.0f%@", Ocasion.stringRepresentation(servingType : caloriesMeal.type), caloriesMeal.percent, "%")
                    snackDrinksPercent = caloriesMeal.percent
                    self.snackDrinksImage.backgroundColor = NRColorUtility.hexStringToUIColor(hex: "#f16527").withAlphaComponent(0.2)
                } else {
                    self.snacksDrinksLabel.text = String(format: "%@", Ocasion.stringRepresentation(servingType : caloriesMeal.type))
                    self.snackDrinksImage.backgroundColor = self.inactiveColor
                    self.snacksDrinksLabel.textColor = self.inactiveColor
                }
            }
        }
        
        self.progressCaloriesMealsView.setupWith(breakfastPercent: breakfastPercent, lunchPercent: lunchPercent, dinnerPercent: dinnerPercent, snackDrinkPercent: snackDrinksPercent)
        
    }

    private func cornerImage() {
        breakfastImage.layer.cornerRadius = breakfastImage.layer.bounds.width/2
        lunchImage.layer.cornerRadius = lunchImage.layer.bounds.width/2
        dinnerImage.layer.cornerRadius = dinnerImage.layer.bounds.width/2
        snackDrinksImage.layer.cornerRadius = dinnerImage.layer.bounds.width/2
    }
}

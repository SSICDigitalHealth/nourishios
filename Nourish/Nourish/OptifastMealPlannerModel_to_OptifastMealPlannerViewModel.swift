//
//  OptifastMealPlannerModel_to_OptifastMealPlannerViewModel.swift
//  Optifast
//
//  Created by Gena Mironchyk on 1/10/18.
//  Copyright © 2018 SSIC. All rights reserved.
//

import UIKit

class OptifastMealPlannerModel_to_OptifastMealPlannerViewModel: NSObject {
    func transform(model : OptifastMealPlannerModel) -> OptifastMealPlannerViewModel {
        let viewModel = OptifastMealPlannerViewModel()
        viewModel.optifastFoods = model.optifastFoods
        viewModel.score = model.score
        viewModel.targetCalories = model.targetCalories
        viewModel.foods = self.viewModelFrom(models: model.foods)
        viewModel.date = model.date
        if viewModel.foods != nil {
             viewModel.foods!.sort(by: { $0.occasion.rawValue < $1.occasion.rawValue})
        }
        return viewModel
    }
    
    
    private func viewModelFrom(models : [OptifastMealModel]?) -> [OptifastMealViewModel]? {
        if models != nil {
            var arrayToReturn = [OptifastMealViewModel]()
            for model in models! {
                let viewModel = OptifastMealViewModel()
                viewModel.occasion = model.occasion
                viewModel.totalCalories = model.totalCalories
                for ocasionFood in model.ocasionFoods {
                    viewModel.ocasionFoods.append((meal: ocasionFood.meal, mealReciepe : self.recipeViewModelFrom(model: ocasionFood.mealReciepe)))
                }
                arrayToReturn.append(viewModel)
            }
            return arrayToReturn
        } else {
            return nil
        }
    }
    
    
    private func recipeViewModelFrom(model : RecipiesModel?) -> RecipiesViewModel? {
        if model != nil {
            let viewModel = RecipiesViewModel()
            viewModel.nameFood = (model?.nameFood)!
            
            if model?.image != nil {
                viewModel.image = model?.image
            }
            
            if model?.groupId != nil {
                viewModel.groupId = model?.groupId
            }
            
            viewModel.pathForImage = model!.pathForImage
            viewModel.foodId = model!.foodId
            viewModel.numberCal = model!.numberCal
            viewModel.unit = model!.unit
            viewModel.grams = model!.grams
            viewModel.amount = model!.amount
            viewModel.ingredient = model!.ingredient
            viewModel.instructions = model!.instructions
            viewModel.isFavorite = model!.isFavorite
            
            return viewModel
        }
        return nil
    }
    
}

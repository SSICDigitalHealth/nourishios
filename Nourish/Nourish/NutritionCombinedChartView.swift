//
//  NutritionCombinedChartView.swift
//  Nourish
//
//  Created by Vino (Vinodha) Sundaramoorthy on 1/10/17.
//  Copyright © 2017 SSIC. All rights reserved.
//

import UIKit

let kLegendHeight : CGFloat = 15.0

final class NutritionCombinedChartView: BaseView ,NutritionCombinedChartProtocol,IAxisValueFormatter{
    var  combinedChartView : CombinedChartView!
    var  calorieLabel : UILabel!
    var  nutritionLabel : UILabel!
    var minAxisValue : Double = 0.0
    var maxAxisValue : Double = 0.0
    var period : period = .today
    var model : nutritionStatsModel? = nil
    var upperLimitScore : Double = 0
    var lowerLimitScore : Double = 0
    var needReloaded = false

    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override func draw(_ rect: CGRect) {
        if needReloaded  {
            self.addShadow()
            for view in self.subviews {
                view.removeFromSuperview()
            }
            if period == .today {
                self.drawTitle()
                self.drawLegends()
                self.drawProgress()
            } else {
                self.drawTitle()
                self.drawCalories()
                self.drawNutrition()
                self.setChartSettings()
            }
        }
    }
    
    func setBarChartData() -> BarChartData {
        var barChartDataEntry : [BarChartDataEntry] = []
        
        for i in 0...(model?.caloriesArray.count)!-1 {
            let data = model?.caloriesArray[i]
            barChartDataEntry.append(BarChartDataEntry.init(x: Double(i+1), y: (data?.calorie)!))
        }
        let set = BarChartDataSet.init(values: barChartDataEntry, label: "")

        //Bar Data Settings
        set.axisDependency = .left
        set.drawValuesEnabled = false
        set.setColor(NRColorUtility.healthGreenColor())
        set.barRoundingCorners = [.topLeft,.topRight,.bottomLeft,.bottomRight]
        
        let data = BarChartData.init(dataSets: [set])
        data.barWidth = 0.45
        return data
    }
    
    func setLineChartData() -> LineChartData {
        let data = LineChartData.init()
        var chartEntry : [ChartDataEntry] = []
        upperLimitScore = (model?.nutritionArray[0].score)!
        lowerLimitScore = (model?.nutritionArray[0].score)!
        
        for i in 0...(model?.nutritionArray.count)!-1 {
            let data = model?.nutritionArray[i]
            chartEntry.append(ChartDataEntry.init(x: Double(i+1), y: (data?.score)!))
            
            if (data?.score)! < lowerLimitScore {
                lowerLimitScore = (data?.score)!
            }
            
            if ((data?.score)! > upperLimitScore) {
                upperLimitScore = (data?.score)!
            }
        }
        
        let set = LineChartDataSet(values: chartEntry, label: "")
        set.setColor(NRColorUtility.healthRedColor())
        set.lineWidth = 2.5
        set.circleRadius = 5
        set.fillColor = NRColorUtility.healthRedColor()
        set.mode = .linear
        set.axisDependency = .right
        set.circleHoleRadius = 0
        set.circleHoleColor = NRColorUtility.healthRedColor()
        set.setCircleColor(NRColorUtility.healthRedColor())
        set.drawValuesEnabled = false
        
        data.addDataSet(set)
        return data
    }
    
    func setChartSettings() {
        combinedChartView = CombinedChartView(frame: CGRect(x: 15, y: 100, width: self.frame.width - 30, height: 230))
        self.addSubview(combinedChartView)
        combinedChartView.chartDescription?.enabled = false
        combinedChartView.drawGridBackgroundEnabled = false
        combinedChartView.drawBarShadowEnabled = false
        combinedChartView.drawRoundedBarEnabled = true
        combinedChartView.isUserInteractionEnabled = false
        combinedChartView.legendRenderer.legend = nil
    
        //Chart Data
        let data = CombinedChartData()
        data.barData = (model?.caloriesArray.count)! > 0 ? self.setBarChartData() : nil
        data.lineData = (model?.nutritionArray.count)! > 0 ? self.setLineChartData() : nil

        //Right Axis
        let rightAxis = combinedChartView.rightAxis
        rightAxis.drawGridLinesEnabled = false
        rightAxis.axisLineColor = UIColor.clear
        rightAxis.zeroLineColor = UIColor.clear
        rightAxis.enabled = false
        rightAxis.granularity = 20
        rightAxis.axisMinimum = 0
        rightAxis.axisMaximum = 100
        rightAxis.granularityEnabled = true
        rightAxis.highlightLimitArea = true
        
        //Left Axis
        let leftAxis = combinedChartView.leftAxis
        if (model?.caloriesArray.count)! > 0 {
            leftAxis.granularity = data.barData.yMax / 3
            leftAxis.axisMaximum = data.barData.yMax
        }
        leftAxis.axisMinimum = 0
        leftAxis.granularityEnabled = true
        leftAxis.zeroLineColor = UIColor.clear
        leftAxis.gridLineDashPhase = 0
        leftAxis.gridLineDashLengths = [2,2]
        leftAxis.labelTextColor = NRColorUtility.legendTextColor()
        leftAxis.drawAxisLineEnabled = false
        
        //X Axis 
        let xAxis = combinedChartView.xAxis
        xAxis.drawGridLinesEnabled = false
        xAxis.labelPosition = .bottom
        xAxis.granularity = 1
        xAxis.granularityEnabled = true
        xAxis.axisLineColor = UIColor.clear
        xAxis.valueFormatter = self
        
        if (model?.nutritionArray.count)! > 0 {
            //Limit Lines
            let upperLimitLine = ChartLimitLine.init(limit: upperLimitScore, label: String(format:"%0.f",upperLimitScore))
            upperLimitLine.valueTextColor = NRColorUtility.healthRedColor()
            upperLimitLine.lineWidth = 0.2
            upperLimitLine.labelPosition = .rightBottom
            upperLimitLine.valueFont = UIFont.systemFont(ofSize: 10)
        
            let lowerLimitLine = ChartLimitLine.init(limit: lowerLimitScore, label: String(format:"%0.f",lowerLimitScore))
            lowerLimitLine.valueTextColor = NRColorUtility.healthRedColor()
            lowerLimitLine.lineWidth = 0.2
            lowerLimitLine.labelPosition = .rightBottom
            lowerLimitLine.valueFont = UIFont.systemFont(ofSize: 10)
        
            rightAxis.removeAllLimitLines()
            rightAxis.addLimitLine(upperLimitLine)
            rightAxis.addLimitLine(lowerLimitLine)
        }
        
        combinedChartView.data = data.barData == nil && data.lineData == nil ? nil : data
        xAxis.axisMinimum = 0.5
        
        if (model?.caloriesArray.count)! > 0 && (model?.nutritionArray.count)! > 0 {
            xAxis.axisMaximum = data.xMax + 0.5
        }
        combinedChartView.animate(yAxisDuration: 1)
        combinedChartView.animate(xAxisDuration: 1)
    }
   
    func drawCalories() {
        // image layer
        let imageFrame = CGRect(x: self.bounds.width/2 - 10, y: 45, width: 25, height: 25)
        let imageView = UIImageView(frame: imageFrame)
        imageView.image = UIImage(named: "intake_sm_icon")
        self.addSubview(imageView)
        
        //Draw Value text
        let valueFrame = CGRect(x:imageFrame.origin.x + imageFrame.width + 10 , y: 45 , width: 150, height: 30)
        let caloriesLabel = UILabel(frame: valueFrame)
        let calAttribute = [NSForegroundColorAttributeName: NRColorUtility.progressLabelColor() , NSFontAttributeName : UIFont.systemFont(ofSize: 20, weight: UIFontWeightMedium)]
        let calAttrString = NSMutableAttributedString(string: String(format:"%2.f",(self.model?.avgCalorie)!), attributes: calAttribute)
        let labelString = " Avg. Cal"
        let labelAttributes = [ NSForegroundColorAttributeName: NRColorUtility.progressLabelColor(), NSFontAttributeName : UIFont.systemFont(ofSize: 14)]
        calAttrString.append(NSAttributedString(string: labelString, attributes: labelAttributes))
        caloriesLabel.attributedText = calAttrString
        self.addSubview(caloriesLabel)
    }
    
    func drawNutrition() {
        // image layer
        let imageFrame = CGRect(x: 20, y: 45, width: 25, height: 25)
        let imageView = UIImageView(frame: imageFrame)
        imageView.image = UIImage(named: "score_sm_icon")
        self.addSubview(imageView)
        
        //Draw Value text
        let valueFrame = CGRect(x:imageFrame.origin.x + imageFrame.width + 10 , y: 45 , width: 100, height: 30)
        let nutritionLabel = UILabel(frame: valueFrame)
        let nutAttribute = [NSForegroundColorAttributeName: NRColorUtility.progressLabelColor() , NSFontAttributeName : UIFont.systemFont(ofSize: 20, weight: UIFontWeightMedium)]
        let nutAttrString = NSMutableAttributedString(string: String(format:"%2.f",(self.model?.nutritionScore)!), attributes: nutAttribute)
        let labelString = " Avg. Cal"
        let labelAttributes = [ NSForegroundColorAttributeName: NRColorUtility.progressLabelColor(), NSFontAttributeName : UIFont.systemFont(ofSize: 14)]
        nutAttrString.append(NSAttributedString(string: labelString, attributes: labelAttributes))
        nutritionLabel.attributedText = nutAttrString
        self.addSubview(nutritionLabel)
    }
    
    func drawTitle(){
        let title = UILabel(frame: CGRect(x: 20, y: 10, width: 150, height: 22))
        title.text = "Calories Consumed"
        title.textColor = NRColorUtility.progressLabelColor()
        title.font = UIFont.systemFont(ofSize: 16)
        self.addSubview(title)
        
        if period == .today {
            //Calorie Comparison
            let subTitleLabel = UILabel(frame: CGRect(x: 20, y: title.frame.origin.y + title.frame.height + 10, width: 200, height: 22))
            
            let titleAttribute = [NSForegroundColorAttributeName: NRColorUtility.progressLabelColor() , NSFontAttributeName : UIFont.systemFont(ofSize: 20, weight: UIFontWeightMedium)]
            let titleAttrString = NSMutableAttributedString(string: String(format:"%0.f",(model?.avgCalorie)!), attributes: titleAttribute)
            var labelString = " Cal / "
            let labelAttributes = [ NSForegroundColorAttributeName: NRColorUtility.progressLabelColor(), NSFontAttributeName : UIFont.systemFont(ofSize: 14)]
            titleAttrString.append(NSAttributedString(string: labelString, attributes: labelAttributes))
            titleAttrString.append(NSMutableAttributedString(string: String(format:"%0.f",(model?.recomCalories)!), attributes: titleAttribute))
            labelString = " Cal"
            titleAttrString.append(NSAttributedString(string: labelString, attributes: labelAttributes))

            subTitleLabel.attributedText = titleAttrString
            subTitleLabel.textAlignment = .left
            self.addSubview(subTitleLabel)
        }
    }
    
    // MARK: NutritionCombinedChartProtocol Implementation
    func fetchCombinedChartStats(period: period) {
    
    }
    
    func setCalorieConsumedStats(model : nutritionStatsModel, forPeriod:period) {
        self.model = model
        self.period = forPeriod
        self.needReloaded = true
        self.setNeedsDisplay()
    }
    
    // MARK:Daily Chart
    func maxCalorieConsumedForDay() -> Double {
        var max : Double = 0.0
        let caloriesArray = model?.caloriesArray
        
        for cal in caloriesArray! {
            if cal.calorie > max {
                max = cal.calorie
            }
        }
        return max
    }
    
    func setBarLegends() {
        if model != nil {
            maxAxisValue = self.maxCalorieConsumedForDay()
        }
    }
    func drawProgress() {
        var i = 0
        var yPosition : CGFloat  = 90
        for calories in (model?.caloriesArray)! {
            //Add Title
            let labelTitle = CGRect(x: 15, y: yPosition, width: 100, height: 15)
            let titleLabel = UILabel(frame: labelTitle)
            titleLabel.text = calories.caloriePeriodRange
            titleLabel.font = UIFont.systemFont(ofSize: 9)
            titleLabel.textColor = UIColor.gray
            self.addSubview(titleLabel)
            
            yPosition = yPosition + labelTitle.height + 5
            
            let progressFrame = CGRect(x: 15, y: yPosition, width: self.caluclateProgressFill(value: calories.calorie), height: 20)
            let progressView = UIView(frame: progressFrame)
            progressView.layer.cornerRadius = calories.calorie > 0 ? 10 : self.caluclateProgressFill(value: calories.calorie) / 2
            progressView.backgroundColor = calories.calorie > 0 ? NRColorUtility.healthGreenColor() : NRColorUtility.defaultHealthProgressColor()
            self.addSubview(progressView)
            
            i = i + 1
            yPosition = yPosition + progressFrame.height + 15
        }
    }
    
    func caluclateProgressFill(value : Double) -> CGFloat {
        var fillValue : CGFloat = 20.0
        let maxWidth = Double(self.frame.size.width - 30)
        
        if value > 0 {
            //convert calories with max percentage of max calorie
            fillValue = CGFloat((value * maxWidth) / maxAxisValue)
        }
        
        return fillValue
    }
    
    func drawLegends() {
        self.setBarLegends()
        var xPosition : CGFloat = 15
        var labelXPosition : CGFloat = 0
        let width = self.bounds.width / 2 - xPosition
        var value : Double = 0
        
        for  _ in 0...2  {
            let frame = CGRect(x:Int(xPosition),y:Int(self.bounds.height - 30) ,width:1,height:10)
            let lineView = NRDrawingUtility.verticalDelimterLine(frame: frame)
            self.addSubview(lineView)
            
            let yPosition = self.bounds.height - 15
            let labelFrame = CGRect(x:Int(labelXPosition),y:Int(yPosition) ,width:30,height:15)
            let label = UILabel.init(frame: labelFrame)
            label.textColor = UIColor.black.withAlphaComponent(0.54)
            label.textAlignment = .center
            label.text = String(format:"%0.f",value)
            label.font = UIFont.systemFont(ofSize: 9)
            self.addSubview(label)
            value = value + maxAxisValue / 2
            xPosition = xPosition + width
            labelXPosition = labelXPosition + width
        }
    }
    
    // MARK: IAxisValueFormatter Implementation
    func stringForValue(_ value: Double, axis: AxisBase?) -> String {
        let index = Int(value) - 1
        var rangeString = (model?.caloriesArray[index].caloriePeriodRange)!
        let periodRange = rangeString.components(separatedBy: "-")
        
        if periodRange.count > 1 {
            rangeString = String(format:"%@-\n%@",periodRange[0],periodRange[1])
        }
        return rangeString
    }
}

//
//  MultiModalMenuPresenter.swift
//  Nourish
//
//  Created by Gena Mironchyk on 5/7/17.
//  Copyright © 2017 SSIC. All rights reserved.
//

import UIKit

class MultiModalMenuPresenter: BasePresenter {
    var multiModalViewController : MultiModalViewControllerProtocol!
    
    func onPhotoSearchButtonPressed () {
        EventLogger.logOnCameraClick()
        self.multiModalViewController.dismissMultiModalViewAndShowPhotoSearch()
    }
    
    func onCancelButtonPressed () {
        self.multiModalViewController.dismissMultiModalView()
    }
    
    func onVoiceSearchButtonPressed () {
        self.multiModalViewController.dismissMutliModalViewAndShowVoiceSearch()
    }
    
    func onMealDiaryButtonPressed () {
        EventLogger.logOnLogFoodClick()
        self.multiModalViewController.dismissMultiModalViewAndShowMealDiary()
    }
}


//
//  WithingsPresenter.swift
//  Nourish
//
//  Created by Gena Mironchyk on 4/18/17.
//  Copyright © 2017 SSIC. All rights reserved.
//

import UIKit
import RxSwift
class WithingsPresenter: BasePresenter, UIWebViewDelegate {
    var withingsView : WithingsWebViewProtocol!
    var withingsViewController : WithingsViewControllerProtocol!
    let interactor = WithingsInteractor ()
    
    override func viewWillAppear(_ animated: Bool) {
        self.deleteCookies()
        self.loadWebView()
    }
    
    private func deleteCookies() {
        URLCache.shared.removeAllCachedResponses()
        URLCache.shared.diskCapacity = 0
        URLCache.shared.memoryCapacity = 0
        
        if let cookies = HTTPCookieStorage.shared.cookies {
            for cookie in cookies {
                HTTPCookieStorage.shared.deleteCookie(cookie)
            }
        }
    }
    
    private func loadWebView () {
        let _ = self.interactor.pairedWithigsDevices().subscribe(onNext: { [unowned self] userDevices in
            if userDevices.count > 0 {
                let withingsDevice = userDevices.first
                let _ = self.interactor.authurizationURLForWithingsDevice(withingsDevice: withingsDevice!).observeOn(MainScheduler.instance).subscribe(onNext: { [unowned self] withingsUrl in
                    let request = self.requestWithToken(token: NRUserSession.sharedInstance.accessToken ?? "", url: withingsUrl)// URLRequest(url: withingsUrl)
                    self.withingsView.loadRequest(request: request)
                }, onError: {error in}, onCompleted: {}, onDisposed: {})
            } else {
                let _ = self.interactor.createWithingsDevice().subscribe(onNext: { [unowned self] devices in
                    if devices.count > 0 {
                        let withingsDevice = devices.first
                        let _ = self.interactor.authurizationURLForWithingsDevice(withingsDevice: withingsDevice!).observeOn(MainScheduler.instance).subscribe(onNext: { [unowned self] withingsUrl in
                            let request = self.requestWithToken(token: NRUserSession.sharedInstance.accessToken ?? "", url: withingsUrl)
                            self.withingsView.loadRequest(request: request)
                        }, onError: {error in}, onCompleted: {}, onDisposed: {})
                    }
                }, onError: {error in }, onCompleted: {}, onDisposed: {})
            }
        }, onError: {error in}, onCompleted: {}, onDisposed: {})
    }
    
    
    func webView(_ webView: UIWebView, shouldStartLoadWith request: URLRequest, navigationType: UIWebViewNavigationType) -> Bool {
        let urlString =  String(describing: request)
        print(urlString)
        if urlString.contains("https://authcomplite") || urlString.contains("https://www.artik.cloud/") {
            webView.stopLoading()
            self.withingsViewController.dismissWithingsView()
        }
        return true
    }
   
    func webViewDidFinishLoad(_ webView: UIWebView) {
        self.withingsView.stopAnimation()
    }
    
    private func requestWithToken(token : String, url : URL) -> URLRequest {
        var request = URLRequest(url : url)
        request.setValue("Bearer \(token)", forHTTPHeaderField: kOAUTHAuthorizationHeader)
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        request.setValue("https://authcomplite", forHTTPHeaderField: "Referer")
        return request
    }
    
}

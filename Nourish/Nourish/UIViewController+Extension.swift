//
//  UIViewController+Extension.swift
//  Nourish
//
//  Created by Vino (Vinodha) Sundaramoorthy on 5/22/17.
//  Copyright © 2017 SSIC. All rights reserved.
//

import UIKit

extension UIViewController {
    
    func showToast(message : String, showingError : Bool, completion:((Bool) -> Void)?) {
        let finalFrame = CGRect(x: self.view.frame.size.width/2 - 150, y: 100, width: 300, height: 35)
        let initialFrame = CGRect(x: self.view.frame.size.width/2 - 150, y: 0, width: 300, height: 35)
        
        let toastLabel = UILabel(frame:initialFrame)
        toastLabel.backgroundColor = UIColor.black.withAlphaComponent(0.6)
        toastLabel.textColor = UIColor.white
        toastLabel.textAlignment = .center;
        toastLabel.font = UIFont.systemFont(ofSize: 16, weight: UIFontWeightRegular)
        toastLabel.text = message
        toastLabel.alpha = 1.0
        toastLabel.layer.cornerRadius = 10;
        toastLabel.clipsToBounds  =  true
        self.view.addSubview(toastLabel)
        UIView.animate(withDuration:1.0, delay: 0.1, options: .curveEaseInOut, animations: {
            toastLabel.frame = finalFrame
        }, completion: {(isCompleted) in
            var delay = 0.0
            if showingError == true {
                delay = 5.0
            }
            self.delayWithSeconds(delay, completion: {
                UIView.animate(withDuration: 1.0, delay: 0.1, options: .curveEaseInOut, animations: {
                    toastLabel.frame = initialFrame
                    toastLabel.alpha = 0.0
                }, completion: {(completed) in
                    toastLabel.removeFromSuperview()
                    if completion != nil {
                        completion!(completed)
                    }
                })
            })
        })
    }
    
    private func delayWithSeconds(_ seconds: Double, completion: @escaping () -> ()) {
        DispatchQueue.main.asyncAfter(deadline: .now() + seconds) {
            completion()
        }
    }
}

extension UINavigationController {
    func popViewControllerWithHandler(completion: @escaping ()->()) {
        CATransaction.begin()
        CATransaction.setCompletionBlock(completion)
        self.popViewController(animated: true)
        CATransaction.commit()
    }
    func pushViewController(viewController: UIViewController, completion: @escaping ()->()) {
        CATransaction.begin()
        CATransaction.setCompletionBlock(completion)
        self.pushViewController(viewController, animated: true)
        CATransaction.commit()
    }
}


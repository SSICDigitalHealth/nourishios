//
//  NRAboutAppPresenter.swift
//  Nourish
//
//  Created by Vino (Vinodha) Sundaramoorthy on 11/29/16.
//  Copyright © 2016 SSIC. All rights reserved.
//

import UIKit

let kCopyrightString = "©2017"

/*
let kVersionString = "Version 1.0"
let kAppDescString = "Lorem ipsum dolor sit er elit lamet, consectetaur cillium adipisicing pecu, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Nam liber te conscient to factor tum poen legum odioque civiuda."
*/
class NRAboutAppPresenter: BasePresenter {
    @IBOutlet weak var aboutAppPresenter : NRAboutAppPresenter!
    var appView : NRAboutAppView!
    
    func setUpAppModel() -> aboutAppModel {
        let dictionary = Bundle.main.infoDictionary!
        let version = dictionary["CFBundleShortVersionString"] as? String ?? ""
        
        let descriptionString = "NourIQ is a wellness application designed around your lifestyle providing timely and personalized recommendations to improve your health. Our easy to use application learns about you and makes recommendations on food and activities that support your personal health goals. Project NourIQ has been designed to integrate with a variety of wearable devices, including Health app, and incorporate the most comprehensive food database available in the market today, making it very easy to track your activity and capture your food intake."
        let aboutAppDataComplex : aboutAppModel = aboutAppModel(appVersion:"Version \(version)" , appName:"Nourish",appDescription:descriptionString ,appCopyrightsString:kCopyrightString)
        return aboutAppDataComplex
    }
    
    
    func enableDeveloperMode () {
        var arrayOfDeveloperUser = defaults.array(forKey: kUersWithEnabledDevModeKey)
        if arrayOfDeveloperUser == nil {
            arrayOfDeveloperUser = []
        }
        let userRepo = UserRepository.shared
        let _ = userRepo.getCurrentUserID().subscribe(onNext: {userId in
            arrayOfDeveloperUser?.append(userId)
        }, onError: {error in}, onCompleted: {
            LogUtility.logToFile("Developer mode enabled")
            defaults.set(arrayOfDeveloperUser, forKey: kUersWithEnabledDevModeKey)
            defaults.synchronize()
        }, onDisposed: {})
    }

}

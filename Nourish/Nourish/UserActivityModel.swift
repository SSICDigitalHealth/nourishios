//
//  UserActivityModel.swift
//  Nourish
//
//  Created by Nova on 11/21/16.
//  Copyright © 2016 SSIC. All rights reserved.
//

import UIKit

enum userActivityType : UInt32 {
    case UserIsSleeping
    case UserInBed
    case UserIsActive
}

class UserActivityModel : NSObject {
    var activityType : userActivityType? = .UserIsActive
    var startDate : Date?
    var endDate : Date?
    
    func duration() -> TimeInterval {
        return endDate!.timeIntervalSince(startDate!)
    }
    
    func colorOfActivity() -> UIColor {
        switch self.activityType! {
        case .UserIsSleeping :
            return NRColorUtility.activityColorBlue()
        case .UserIsActive :
            return NRColorUtility.activityColorGreen()
        case .UserInBed :
            return NRColorUtility.activityColorLightBlue()
        }
    }
    override init() {
        
    }
}

//
//  ConnectDeviceView.swift
//  Nourish
//
//  Created by Vino (Vinodha) Sundaramoorthy on 5/18/17.
//  Copyright © 2017 SSIC. All rights reserved.
//

import UIKit

class ConnectDeviceView: BaseView ,ConnectDeviceProtocol {

    @IBOutlet weak var devicesTableView : UITableView!
    @IBOutlet weak var doneButton : UIButton!
    let presenter = ConnectDevicePresenter()
    var controller : ConnectDevicesViewController?
    

    override func viewWillAppear(_ animated: Bool) {
        self.doneButton.backgroundColor = NRColorUtility.appBackgroundColor()
        self.basePresenter = presenter
        presenter.deviceView = self
        self.backgroundColor = UIColor.white
        devicesTableView.backgroundColor = UIColor.white
        devicesTableView.tableHeaderView =  UIView(frame: CGRect(x: 0, y: 0, width: self.devicesTableView.bounds.width, height: 1))
        devicesTableView?.register(UINib(nibName: "ConnectDeviceTableViewCell", bundle: nil), forCellReuseIdentifier: "kConnectDeviceCell")
        super.viewWillAppear(animated)
    }
    
    // MARK : ConnectDeviceProtocol
    func loadDevices() {
        self.devicesTableView.delegate = presenter
        self.devicesTableView.dataSource = presenter
        self.devicesTableView.reloadData()
        self.stopActivityAnimation()
        
    }
    
    func openWithingsLogin() {
        self.controller?.openWithingsLogin()
    }
    
    func openFitBitLogin() {
        self.controller?.openFitBitLogin()
    }
}

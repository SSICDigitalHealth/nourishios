//
//  UserProfileViewProtocol.swift
//  Nourish
//
//  Created by Nova on 11/16/16.
//  Copyright © 2016 SSIC. All rights reserved.
//

import Foundation

protocol UserProfileViewProtocol : BaseViewProtocol {
    func setFirstName(firstName : String)
    func setLastName(lastName : String)
    func setHeight(height : Double)
    func setWeight(height : Double)
    func setAge(age : Int)
    func setActivityLevel(actLevel : ActivityLevel)
    func setDietaryPreference(dietaryPreference : DietaryPreference)
    func setMetricPreference(metricPreference : MetricPreference)
    func setBiologicalSex(biologicalSex : BiologicalSex)
    func saveUserModel()
    func refreshUserSettings()
    func reloadMetricPreference()
    func reloadCellSection(sections: IndexSet)
    func chooseImageFromGallery()
    func showToast(message:String)
}

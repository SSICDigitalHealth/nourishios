//
//  ActivityViewController.swift
//  Nourish
//
//  Created by Andrey Zmushko on 8/23/17.
//  Copyright © 2017 SSIC. All rights reserved.
//

import UIKit


class ActivityViewController: BasePresentationViewController, ActivityNavigationProtocol {
    @IBOutlet weak var activityDetailView: ActivityDetailView!
    @IBOutlet weak var activityNavigationView: ActivityNavigationBarView!

    @IBOutlet var activityNavigationPresenter: ActivityNavigationPresenter!
    @IBOutlet var activityDetailPresenter: ActivityDetailPresenter!
    
    func activityNavigation(_ activityNavigationPresenter: ActivityNavigationPresenter, startDate: Date, endDate: Date) {
        activityDetailPresenter.getCurrentInformationFor(startDate: startDate, endDate: endDate)
    }
    
    func resetForTodayDailyActivity() {
        DispatchQueue.main.async {
            self.activityNavigationView.segmentedControl.selectItemAtIndex(index: 0, withAnimation: true)
            if let picker = self.activityNavigationPresenter.datePicker {
                picker.setupDate(date: Date())
                picker.setupPeriod(period: .daily)
            }
        }
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.automaticallyAdjustsScrollViewInsets = false
        self.setupNavigationBar()
        NotificationCenter.default.addObserver(self, selector:#selector(resetForTodayDailyActivity), name:.NSCalendarDayChanged, object: nil)
    }

    override func viewWillDisappear(_ animated: Bool) {
        NotificationCenter.default.removeObserver(self, name: .NSCalendarDayChanged, object: nil)
        super.viewWillDisappear(animated)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupViews()
    }
    
    private func setupViews() {
        self.baseViews = [activityNavigationView, activityDetailView]
        activityNavigationView.viewDidLoad()
        activityDetailView.viewDidLoad()
        
        activityNavigationPresenter.delegate = self
        activityNavigationView.segmentedControl.delegate = activityNavigationPresenter
        
        activityDetailView.presenter = activityDetailPresenter
        activityDetailPresenter.activityViewController = self
        activityDetailPresenter.activityDetailDelegate = activityNavigationPresenter
        activityDetailView.activityDetailDelegate = activityNavigationPresenter

    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }
    
    private func setupNavigationBar() {
        self.navigationController?.setNavigationBarHidden(true, animated: false)
        
        let shadowPath = UIBezierPath(rect: CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: self.activityNavigationView.frame.height))
        self.activityNavigationView.layer.shadowColor = UIColor.darkGray.cgColor
        self.activityNavigationView.layer.shadowOffset = CGSize(width: 0.0, height: 1.0)
        self.activityNavigationView.layer.shadowOpacity = 1.0
        self.activityNavigationView.layer.shadowRadius = 2.0
        self.activityNavigationView.layer.masksToBounds =  false
        self.activityNavigationView.layer.shadowPath = shadowPath.cgPath
    }

}

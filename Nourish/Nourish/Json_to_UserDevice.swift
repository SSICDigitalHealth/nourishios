//
//  Json_to_UserDevice.swift
//  Nourish
//
//  Created by Nova on 12/7/16.
//  Copyright © 2016 SSIC. All rights reserved.
//

import UIKit

class Json_to_UserDevice: NSObject {
    func transform(device : [String:Any], userID : String) -> UserDevice {
        let deviceToReturn = UserDevice()
        deviceToReturn.udidString = device["id"] as! String
        deviceToReturn.dtidString = device["dtid"] as! String
        deviceToReturn.deviceName = device["name"] as! String
        if device["cloudAuthorization"] as! String == "AUTHORIZED" {
            deviceToReturn.isAuthorized = true
        }
        deviceToReturn.userID = userID
        return deviceToReturn
    }
    
    func transform(devices : [[String:Any]], userID : String) -> [UserDevice] {
        var arrayToReturn : [UserDevice] = []
        for device in devices {
            arrayToReturn.append(self.transform(device: device, userID: userID))
        }
        return arrayToReturn
    }
}

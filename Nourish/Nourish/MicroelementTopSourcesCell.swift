//
//  MicroelementTopSourcesCell.swift
//  Nourish
//
//  Created by Andrey Zmushko on 8/18/17.
//  Copyright © 2017 SSIC. All rights reserved.
//

import UIKit

class MicroelementTopSourcesCell: UITableViewCell {

    @IBOutlet weak var rowTextLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}

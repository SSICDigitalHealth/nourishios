//
//  ImageRepository.swift
//  Nourish
//
//  Created by Nova on 5/12/17.
//  Copyright © 2017 SSIC. All rights reserved.
//

import UIKit
import RxSwift

class ImageRepository: NSObject {
    let dataStore = NourishDataStore.shared
    let cache = ImageRealmDataStore()
    let userRepo = UserRepository.shared
    
    
    func uploadPhoto(image : UIImage, userfoodID : String?, groupID : String?) -> Observable<String> {
        let seq = self.userRepo.getCurrentUser(policy: .DefaultPolicy).flatMap { user -> Observable<String> in
            return Observable.just(user.userID!)
        }.flatMap { userID -> Observable<String> in
                let token = NRUserSession.sharedInstance.accessToken
                return self.dataStore.uploadPhoto(image: image, token: token!, userID: userID, userfoodID: userfoodID, groupID: groupID)
        }.flatMap {photoID -> Observable<String> in
            self.cache.store(image: image, photoID: photoID)
            return Observable.just(photoID)
        }
        return seq
        
    }
    
    func getPhoto(userPhotoID : String) -> Observable<UIImage> {
        return Observable.create { observer in
            let photo = self.cache.getImage(ident: userPhotoID)
            if photo != nil {
                observer.onNext(photo!)
                observer.onCompleted()
            } else {
                let _ = self.userRepo.getCurrentUser(policy: .DefaultPolicy).flatMap { user -> Observable<String> in
                    return Observable.just(user.userID!)
                }.flatMap { userID -> Observable<UIImage> in
                    let token = NRUserSession.sharedInstance.accessToken
                    
                    return self.dataStore.getFoodImage(token: token!, userID: userID, photoID: userPhotoID)
                    }.subscribe(onNext: { [unowned self] image in
                        self.cache.store(image: image, photoID: userPhotoID)
                        observer.onNext(image)
                        observer.onCompleted()
                    }, onError: {error in}, onCompleted: {}, onDisposed: {})
                
            }
            return Disposables.create()
        }
    }
}

//
//  OptifastProductPictureCell.swift
//  Nourish
//
//  Created by Gena Mironchyk on 10/23/17.
//  Copyright © 2017 SSIC. All rights reserved.
//

import UIKit

class OptifastProductPictureCell: UITableViewCell {
    @IBOutlet weak var optifastTitleLabel : UILabel!
    @IBOutlet weak var optifastImageView : UIImageView!
    @IBOutlet weak var optifastHeaderLabel : UILabel!
    @IBOutlet weak var optifastFooterLabel : UILabel!
    @IBOutlet var optifastHiglightsLabels : [UILabel]!
    @IBOutlet weak var optifastOrderButton : UIButton!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.optifastOrderButton.layer.cornerRadius = 24.0
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    
    func setupWith(model : foodSearchModel) {
        self.optifastTitleLabel.text = model.foodTitle
        self.optifastImageView.image = model.optifastImage
        self.optifastHeaderLabel.text = model.optifastHeader
        self.optifastFooterLabel.text = model.optifastFooter
        for index in 0...self.optifastHiglightsLabels.count - 1 {
            let label = self.optifastHiglightsLabels[index]
            label.text = model.optifastHightlights[index]
        }
    }
    
}

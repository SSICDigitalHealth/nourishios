//
//  HeaderMealPlannerTableViewCell.swift
//  Nourish
//
//  Created by Vlad Birukov on 21.08.17.
//  Copyright © 2017 SSIC. All rights reserved.
//

import UIKit

class HeaderMealPlannerTableViewCell: UITableViewCell, DatePickerProtocol {
    @IBOutlet weak var caloriesLabel: UILabel!
    @IBOutlet weak var scoreLabel: UILabel!
    @IBOutlet weak var datePickerHolder: UIView!
    
    @IBOutlet weak var hightScore: NSLayoutConstraint!
    @IBOutlet weak var topScore: NSLayoutConstraint!
    @IBOutlet weak var topCalLabel: NSLayoutConstraint!
    @IBOutlet weak var bottomCalLabel: NSLayoutConstraint!
    @IBOutlet weak var heightView: NSLayoutConstraint!


    
    var datePickerView: DatePickerView?
    var presenter: MealPlannerPresenter?

    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    func onChange(datePicker: DatePickerView, newDate: Date) {
        if self.presenter != nil {
            self.presenter?.dateRange = self.daysBetweenDates(endDate: newDate)
            self.presenter?.getMealPlannerInformation(date: newDate)
            self.dataReset()
            if let view = presenter?.mealPlannerView {
                view.startActivityAnimation()
            }
        }
    }

    func setUpWithModel(model: MealPlannerViewModel, presenter: MealPlannerPresenter, date: Date, isHideView: Bool = false) {
        self.caloriesLabel.text = String(format: "%.0f kcal", model.targetCalories)
        self.scoreLabel.text = String(format: "%.0f", model.score)
        self.presenter = presenter
        self.datePickerHolder.setupContent(&self.datePickerView, nibName: "DatePickerView")
        
        if self.presenter != nil {
            self.datePickerView?.setupWith(deligate: self, schema: .planner, date: date)
            self.datePickerView?.setupPeriod(period: .future)
        }
    
        if isHideView == true {
            self.hideView()
        }
    }
    
    private func daysBetweenDates(endDate: Date) -> Int {
        let calendar = Calendar.current
        let startDate = calendar.startOfDay(for: Date())
        let newEndDate = calendar.startOfDay(for: endDate)
        let components = calendar.dateComponents([.day], from: startDate, to: newEndDate)
        return components.day!
    }
    
    private func hideView() {
        self.hightScore.constant = 0
        self.topScore.constant = 0
        self.topCalLabel.constant = 0
        self.bottomCalLabel.constant = 0
        self.heightView.constant = 0

        self.layoutIfNeeded()
    }
    
    private func dataReset() {
        if presenter != nil {
            self.presenter?.lunchIndex = 0
            self.presenter?.dinnerIndex = 0
            self.presenter?.breakfastIndex = 0
            self.presenter?.snacksIndex = 0
        }
    }
}

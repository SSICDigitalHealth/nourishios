//
//  Nutrients.swift
//  Nourish
//
//  Created by Vlad Birukov on 11.09.17.
//  Copyright © 2017 SSIC. All rights reserved.
//

import Foundation

class NutrientsModel_to_NutrientsViewModel {
    
    let nutrientDetail = NutrientDetailInformationModel_to_NutrientDetailInformationViewModel()
    
    func transform(model: NutrientsModel) -> NutrientsViewModel {
        let nutrientsViewModel = NutrientsViewModel()
        
        if model.nutrientChartData.count != 0 {
            for item in model.nutrientChartData {
                nutrientsViewModel.nutrientChartData.append(self.transform(model: item))
            }
        }
        
        if model.nutrientDetail.count != 0 {
            for item in model.nutrientDetail {
                nutrientsViewModel.nutrientDetail.append(nutrientDetail.transform(model: item))
            }
        }
        
        return nutrientsViewModel
    }

    private func transform(model: NutrientModel) -> NutrientViewModel {
        let nutrientViewModel = NutrientViewModel()
        nutrientViewModel.nameNutritional = model.nameNutritional.capitalized
        nutrientViewModel.consumedNutritional = model.consumedNutritional
        nutrientViewModel.targetNutritional = model.targetNutritional
        nutrientViewModel.unit = model.unit
        nutrientViewModel.message = model.message
        nutrientViewModel.flagImage = model.flagImage
        
        return nutrientViewModel
    }
}

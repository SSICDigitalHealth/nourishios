//
//  DailyStress_to_DailyStressModel.swift
//  Nourish
//
//  Created by Gena Mironchyk on 1/24/17.
//  Copyright © 2017 SSIC. All rights reserved.
//

import Foundation
class DailyStress_to_DailyStressModel : NSObject{
    func transform(stress : DailyStress) -> DailyStressModel {
        let model = DailyStressModel()
        model.bpmValue = stress.bpmValue
        model.stressValue = stress.stressValue
        model.dateToShow = stress.dateToShow
        return model
    }
    
    func transform(array: [DailyStress]) -> [DailyStressModel] {
        var modelsArray : [DailyStressModel] = []
        for str in array {
            modelsArray.append(self.transform(stress: str))
        }
        return modelsArray
    }
}

//
//  NRUnitsTableViewCell.swift
//  Nourish
//
//  Created by Vino (Vinodha) Sundaramoorthy on 11/14/16.
//  Copyright © 2016 SSIC. All rights reserved.
//

import UIKit

class NRUnitsTableViewCell: UITableViewCell {
    @IBOutlet weak var selectedImageView : UIImageView!
    @IBOutlet weak var unitName : UILabel!
    @IBOutlet weak var unitDesc : UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}

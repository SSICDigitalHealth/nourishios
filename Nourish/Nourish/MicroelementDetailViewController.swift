//
//  MicroelementDetailViewController.swift
//  Nourish
//
//  Created by Vlad Birukov on 16.06.17.
//  Copyright © 2017 SSIC. All rights reserved.
//

import UIKit

class MicroelementDetailViewController: BaseCardViewController {

    @IBOutlet var microelementDetailView: MicroelementDetailView!
    @IBOutlet weak var cardNavigationBar: NewCardNavigationBarView!

    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.baseViews = [cardNavigationBar, microelementDetailView]
        for v in self.baseViews! {
            v.viewDidLoad()
        }
        
        if let config = self.progressConfig {
            self.microelementDetailView.progressConfig = config
        }
    }

    override func viewWillAppear(_ animated: Bool) {
        setUpNavigationBar(title: "Watch out for", style: .other, navigationBar: cardNavigationBar)
        super.viewWillAppear(animated)
    }
}

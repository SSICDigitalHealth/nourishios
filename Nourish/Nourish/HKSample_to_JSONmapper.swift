//
//  HKSample_to_JSONmapper.swift
//  NourIQ
//
//  Created by Igor on 12/21/17.
//  Copyright © 2017 SSIC. All rights reserved.
//

import UIKit
import HealthKit

class HKSample_to_JSONmapper: NSObject {

    let newActivityRepo = NewActivityRepository.shared
    
    func transform(samples : [HKSample]) -> [[String : Any]] {
        var result = [[String : Any]]()
        
        for obj in samples {
            result.append(self.transform(sample: obj))
        }
        
        return result
    }
    
    func transform(sample : HKSample) -> [String : Any] {
        var result = [String : Any]()
        
        if let quantity = sample as? HKQuantitySample {
            result["quantityType"] = String(describing:quantity.quantityType.identifier)
            result["quantity"] = String(describing:quantity.quantity)
            
            var metaDataStr = ""
            
            if let metaDs = quantity.metadata {
                metaDataStr = String(describing:metaDs)
            }
            
            result["metaData"] = metaDataStr
            
        }
        
        if let sleeping = sample as? HKCategorySample {
            result["categoryType"] = String(describing : sleeping.categoryType)
        }
        
        if let workout = sample as? HKWorkout {
            result["workoutActivityType"] = self.newActivityRepo.stringFromEnum(type: workout.workoutActivityType)
            result["duration"] = String(describing:workout.duration)
            
            var tDs = "0"
            if let totalDistance = workout.totalDistance {
                tDs = String(describing : totalDistance)
            }
            result["totalDistance"] = tDs
            
            var tFc = "0"
            if #available(iOS 11.0, *) {
                if let totalClimbed = workout.totalFlightsClimbed {
                    tFc = String(describing :totalClimbed)
                }
            }
            result["totalFlightsClimbed"] = tFc
            
            var tStroked = "0.0"
            if #available(iOS 10.0, *) {
                if let totalStroked = workout.totalSwimmingStrokeCount {
                    tStroked = String(describing :totalStroked)
                }
            }
            result["totalSwimmingStrokeCount"] = tStroked
            
            var tEb = "0.0"
            if let totalEnergy = workout.totalEnergyBurned {
                tEb = String(describing:totalEnergy)
            }
            result["totalEnergyBurned"] = tEb
            
            if workout.workoutEvents != nil && (workout.workoutEvents?.count)! > 0 {
                var dictArr = [[String : Any]]()
                
                for object in workout.workoutEvents! {
                    var dict = [String : Any]()
                    
                    var dateInt = ""
                    if #available(iOS 11.0, *) {
                        dateInt = String(describing: object.dateInterval)
                    }
                    dict["dateInterval"] = dateInt
                    dict["type"] = String(describing:object.type)
                    
                    if #available(iOS 10.0, *) {
                        if let meta = object.metadata {
                            dict["metadata"] = String(describing: meta)
                        }
                    }
                    dictArr.append(dict)
                }
                result["workoutEvents"] = dictArr
            }
        }
        
        result["startDate"] = String(describing:sample.startDate)
        result["endDate"] = String(describing:sample.endDate)
        result["uuid"] = sample.uuid.uuidString
        
        return result
    }
}

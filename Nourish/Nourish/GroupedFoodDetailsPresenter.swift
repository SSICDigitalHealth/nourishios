//
//  GroupedFoodDetailsPresenter.swift
//  Nourish
//
//  Created by Vino (Vinodha) Sundaramoorthy on 3/8/17.
//  Copyright © 2017 SSIC. All rights reserved.
//

import UIKit
import RxSwift


class GroupedFoodDetailsPresenter: BasePresenter , UITableViewDelegate , UITableViewDataSource,MGSwipeTableCellDelegate{
   
    var groupFoodView : GroupFoodViewProtocol?
    var foodNutrientsArray : [Nutrient] = []
    var interactor = NRFoodDetailsInteractor()
    let favInteractor = FavouritesInteractor()
    var groupFoodDetails : groudFoodDetailsModel!
    let toModelMapper = MealRecord_to_foodSearchModel()
    let toModel = MealRecordModel_to_MealRecord()
    let toCacheMapper = MealRecord_to_MealRecordCache()
    var date : Date!
    var fromFav : Bool = false
    let imageRepo = ImageRepository()
    
    
    override func viewWillAppear(_ animated: Bool) {
        // Load nutrient Details 
        groupFoodView?.stopActivityAnimation()
        self.loadNutrients()
        //Load group meal details
        groupFoodView?.loadGroupMealDetails()
        
        if self.groupFoodDetails.photoID != nil {
            self.subscribtions.append(self.imageRepo.getPhoto(userPhotoID: self.groupFoodDetails.photoID!).observeOn(MainScheduler.instance).subscribe(onNext: { [unowned self] image in
                self.groupFoodView?.setFoodImage(image: image)
            }))
        } else {
            self.groupFoodView?.hideFoodImage()
        }

    }
    
    func loadNutrients() {
        let nutrUtility = NutrientUtility()
        self.subscribtions.append(nutrUtility.getCombinedNutrsFrom(mealRecords: self.groupFoodDetails.groupMealArray).observeOn(MainScheduler.instance).subscribe(onNext: { [unowned self] nutr in
            self.foodNutrientsArray = nutr
        }, onError: {error in
            self.groupFoodView?.parseError(error : error, completion: nil)
        }, onCompleted: {
            self.groupFoodView?.loadNutrientDetails()
            
        }, onDisposed: {}))
        
    }
    
    func addToDiary(ocasion : Ocasion, date : Date) {
        self.groupFoodView?.startActivityAnimation()
        var models = [foodSearchModel]()
        
        for object in self.groupFoodDetails.groupMealArray {
            models.append(object.foodSearchRepresentation())
        }
//        var dateFor = date
//        if Calendar.current.isDateInToday(date) {
//            dateFor = Date()
//        }
        
        self.subscribtions.append(self.favInteractor.addToDiary(foodGroupd: self.groupFoodDetails.groupId, groupName:self.groupFoodDetails.groupTitle, occasion: ocasion, date: date, models : models).observeOn(MainScheduler.instance).subscribe(onNext: {success in
            print("food group to diarry = \(success)")
        }, onError: {error in
            self.groupFoodView?.parseError(error : error, completion: nil)
        }, onCompleted: {
            //Stop progress indicator
            EventLogger.logSaveFoodFor(date: date, ocasion: ocasion, isFavourite: self.groupFoodDetails.isFavorite, isGroup: true)
            self.groupFoodView?.stopActivityAnimation()
            //Redirect
            self.groupFoodView?.finishSaveToDiary()
            
            
            
            
            let dump = BackgroundActivityDumperRepository()
            dump.storeGroupModel(model: self.groupFoodDetails)
            
        }, onDisposed: {}))
    }
    
    
    // MARK: UITableViewDelegate
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if tableView.tag == 10 && !self.fromFav{
            let record = self.groupFoodDetails.groupMealArray[indexPath.row]
             let model = self.toModelMapper.transform(record: record)
            
           // if model.isFromNoom == true { ??
                self.groupFoodView?.showFoodDetails(model: model)
           // }
            
           // print("Selected record \(record.foodTitle)")
        }
    }
    
    // MARK: UITableViewDataSource
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tableView.tag == 10 {
            return groupFoodDetails.groupMealArray.count
        } else {
            return foodNutrientsArray.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if tableView.tag == 10 {
            let cell : GroupDetailsCell = tableView.dequeueReusableCell(withIdentifier: "kGroupDetails") as! GroupDetailsCell
            let data = self.groupFoodDetails.groupMealArray[indexPath.row]
            cell.delegate = self
            cell.setupWithModel(record: data)
            cell.selectionStyle = .none
            return cell
        } else {
            let cell : NRFoodNutrientCell = tableView.dequeueReusableCell(withIdentifier: "kNutrientCell") as! NRFoodNutrientCell
            let data = self.foodNutrientsArray[indexPath.row]
            cell.name.text = data.name
            cell.value.text = String(format:"%.2f %@",data.representationValue,data.unit)
            cell.selectionStyle = .none
            return cell
        }
    }
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if tableView.tag == 10 {
            let data = self.groupFoodDetails.groupMealArray[indexPath.row]
            let heightPadding : CGFloat = 74
            let widthPadding : CGFloat = 32
            let titleHeight = data.foodTitle?.sizeOfBoundingRect(width: UIScreen.main.bounds.width - widthPadding, height: maxLabelSize.height, font:UIFont.systemFont(ofSize: 14, weight: UIFontWeightRegular)).height
            return heightPadding + titleHeight!
        } else {
            let cell = self.tableView(tableView, cellForRowAt: indexPath)
            return cell.frame.size.height
        }
    }
    
    // MARK: MGSwipeTableCellDelegate
    
    func swipeTableCell(_ cell: MGSwipeTableCell, canSwipe direction: MGSwipeDirection) -> Bool {
        return !self.fromFav
    }
    
    func swipeTableCell(_ cell: MGSwipeTableCell, swipeButtonsFor direction: MGSwipeDirection, swipeSettings: MGSwipeSettings, expansionSettings: MGSwipeExpansionSettings) -> [UIView]? {
        swipeSettings.transition = MGSwipeTransition.border;
        expansionSettings.buttonIndex = 0;
        let index =  groupFoodView?.getGroupIndexPathForCell(cell: cell)
        let record = self.groupFoodDetails.groupMealArray[(index?.row)!]
        
        if direction == MGSwipeDirection.rightToLeft {
            expansionSettings.fillOnTrigger = false;
            expansionSettings.threshold = 3;
            return [
                MGSwipeButton(title: "Delete Item", icon: UIImage(named:"deleteitem_icon"), backgroundColor: NRColorUtility.deleteItemColor(), callback: { (cell) -> Bool in
                    //print("Record to un fav \(record.foodTitle)")
                    let mealRepo = MealHistoryRepository.shared
                        self.groupFoodView?.startActivityAnimation()
                    self.subscribtions.append(mealRepo.deleteFood(foodID: [record.userFoodID ?? record.localCacheID ?? ""],date:record.date!,saveChat:false).subscribe(onNext: {score in
                        print("deleted from group")
                    }, onError: {error in}, onCompleted: {
                        self.groupFoodView?.stopActivityAnimation()
                        self.groupFoodView?.popToMealDiary()
                    }, onDisposed: {}))
                    return true
                })
            ]
        }
        return nil
    }
    func deleteFromFavorite() -> Observable<Bool> {
        return self.favInteractor.deleteFavouriteGroup(groupID: self.groupFoodDetails.groupId)
    }
    
    
    func addToFavourite() -> Observable<Bool> {
        return self.favInteractor.addGroupToFav(foodGroupID: self.groupFoodDetails.groupId, name: self.groupFoodDetails.groupTitle, userPhotoID: self.groupFoodDetails.photoID)
    }
}

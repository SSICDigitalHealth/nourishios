//
//  FitBitInteractor.swift
//  Nourish
//
//  Created by Vino (Vinodha) Sundaramoorthy on 7/20/17.
//  Copyright © 2017 SSIC. All rights reserved.
//

import UIKit
import RxSwift

let kLastSyncFitBitDateKey = "Last sync fitbit"
let kFitBitNextToken = "NextFitBitToken"

class FitBitInteractor: NSObject {

    let devicesRepo = UserDevicesRepository.shared
    let messagesRepository = ArtikCloudMessagesRepository()
    let userRepository = UserRepository.shared
    let chatBoxRepository = ChatBoxRepository()
    
    private func userDevices() -> Observable<[UserDevice]> {
        return self.devicesRepo.userDevices()
    }
    
    func pairedFitBitDevices() -> Observable<[UserDevice]> {
        return self.userDevices().flatMap({devices -> Observable<[UserDevice]> in
            let pairedDevices = devices.filter({$0.dtidString == kFitBitDeviceTypeId})
            return Observable.just(pairedDevices)
        })
    }

    func createFitBitDevice() -> Observable<[UserDevice]> {
        return self.devicesRepo.createFitBitDevice()
    }
    
    func authurizationURLForFitBitDevice(device : UserDevice) -> Observable<URL> {
        return self.devicesRepo.authorizationURLForDevice(device: device)
    }
    
    
    func unauthorizeFitBitDevice() -> Observable<Bool> {
        return self.pairedFitBitDevices().flatMap{devices -> Observable<Bool> in
            if let device = devices.first {
                if device.isAuthorized {
                    return self.devicesRepo.unauthorizeDevice(device: device)
                }
            }
            return Observable.just(true)
        }
    }
    
    
    func storeConnectionStatus(status : String) {
        let _ = self.userRepository.getCurrentUser(policy: .Cached).subscribe(onNext : {userProfile in
            if userProfile.userID != nil {
                let chatMessage = ChatDataBaseItem()
                chatMessage.content = status
                chatMessage.date = Date()
                chatMessage.isFromUser = false
                chatMessage.chatBoxType = .typeString
                chatMessage.userID = userProfile.userID!
                let realmStore = ChatBoxRealmDataStore()
                realmStore.storeRecordsArray(array: [chatMessage])
            }
        })
    }
    
    func syncFitBitMessages() -> Observable<Bool> {
        var startDate = Calendar.current.startOfDay(for: Date())
        //startDate = startDate.addingTimeInterval(-(24*60*60*7))
      
        if defaults.object(forKey: kLastSyncFitBitDateKey) != nil {
            let proxyDate = defaults.object(forKey: kLastSyncFitBitDateKey) as! Date
            if !Calendar.current.isDateInToday(proxyDate) {
                startDate = proxyDate
            }
        }
        
        return self.pairedFitBitDevices().flatMap({devices -> Observable<Bool> in
            if devices.count > 0 {
                let fitBitDevice = devices.first
                if fitBitDevice?.isAuthorized == true {
                    return self.messagesRepository.getFitBitMessages(device: fitBitDevice!, startDate: startDate, endDate: Date()).flatMap({fitBitMessages -> Observable<Bool> in
                        if fitBitMessages.count > 0{
                            FitBitActivityUtility().syncFitBitMessages(messageArray: fitBitMessages)
                            return Observable.just(true)
                        } else {
                            return Observable.just(true)
                        }
                    })
                } else {
                    return Observable.just(true)
                }
            } else {
                return Observable.just(true)
            }
        })
    }
    
    
    func syncFitBitMessage(startDate:Date ,endDate:Date,device:UserDevice) {
         let _ = self.messagesRepository.getFitBitMessages(device: device, startDate: startDate, endDate: Date()).flatMap({fitBitMessages -> Observable<Bool> in
                if fitBitMessages.count > 0 {
                    FitBitActivityUtility().syncFitBitMessages(messageArray: fitBitMessages)
                    return Observable.just(true)
                }
                else {
                    return Observable.just(true)
                }
            })
    }
    
}

//
//  UserActivityScoreViewModel.swift
//  Nourish
//
//  Created by Vlad Birukov on 29.08.17.
//  Copyright © 2017 SSIC. All rights reserved.
//

import Foundation

class UserActivityScoreViewModel: NSObject {
    var scoreActivity: Double = 0.0
    var scoreHistory = [Double]()
    var isDairyEmpty = Bool(false)
}

//
//  CaloriesDetailViewController.swift
//  Nourish
//
//  Created by Vlad Birukov on 20.06.17.
//  Copyright © 2017 SSIC. All rights reserved.
//

import UIKit

class CaloriesDetailViewController: BaseCardViewController {
    
    @IBOutlet weak var caloriesChartView: CaloriesChartView!
    @IBOutlet weak var caloriesDailyAverageView: CaloriesDailyAverageView!
    @IBOutlet weak var userCaloriesView: UserCaloriesView!
    @IBOutlet weak var caloriesFoodView: CalorificFoodView!
    @IBOutlet weak var caloriesIntakeMealsView: CaloriesIntakeMealsView!
    @IBOutlet weak var cardNavigationBar: NewCardNavigationBarView!

    
    @IBOutlet weak var userCaloriesTopConstraint: NSLayoutConstraint!
    @IBOutlet weak var userChartTopNewConstraint: NSLayoutConstraint!
    @IBOutlet weak var caloriesIntakeMealsTopConstraint: NSLayoutConstraint!
    @IBOutlet weak var userCaloriesBottomConstraint: NSLayoutConstraint!
    @IBOutlet weak var caloriesDailyAverageHeight: NSLayoutConstraint!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if let config = self.progressConfig {
            switch config.period {
            case .daily:
                userCaloriesView.progressConfig = config
                caloriesFoodView.progressConfig = config
                caloriesIntakeMealsView.progressConfig = config
                self.baseViews = [cardNavigationBar, userCaloriesView, caloriesIntakeMealsView, caloriesFoodView]
            default:
                caloriesChartView.progressConfig = config
                caloriesDailyAverageView.progressConfig = config
                caloriesFoodView.progressConfig = config
                caloriesIntakeMealsView.progressConfig = config
                self.baseViews = [cardNavigationBar, caloriesChartView, caloriesDailyAverageView, caloriesIntakeMealsView, caloriesFoodView]
            }
            
        }
        
        for v in self.baseViews! {
            v.viewDidLoad()
        }
        
        arrangementViewController()
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        setUpNavigationBar(title: "Calories consumed", style: .other, navigationBar: cardNavigationBar)
        super.viewWillAppear(animated)
    }
    
    func arrangementViewController() {
        if let config = self.progressConfig {
            switch config.period {
            case .daily:
                userCaloriesBottomConstraint.isActive = true
                caloriesIntakeMealsTopConstraint.isActive = false
                caloriesChartView.isHidden = true
                caloriesDailyAverageView.isHidden = true
                
                userCaloriesView.settingButtomSpace()
            default:
                userChartTopNewConstraint.isActive = true
                userCaloriesTopConstraint.isActive = false
                userCaloriesView.isHidden = true
                
                caloriesChartView.settingButtomSpace()
                caloriesDailyAverageView.deleteHeader(config: config)
                caloriesDailyAverageHeight.constant = 48
            }
        }
    }
}

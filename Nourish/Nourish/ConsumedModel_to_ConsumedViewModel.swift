//
//  ConsumedModel_to_ConsumedViewModel.swift
//  Nourish
//
//  Created by Vlad Birukov on 31.08.17.
//  Copyright © 2017 SSIC. All rights reserved.
//

import Foundation

class ConsumedModel_to_ConsumedViewModel {
    func transform(model: ConsumedModel) -> ConsumedViewModel {
        let consumedViewModel = ConsumedViewModel()
        if model.consumedCalories != nil {
            consumedViewModel.consumedCalories = model.consumedCalories
        }
        
        if model.avgConsumed != nil {
            consumedViewModel.avgConsumed = model.avgConsumed
        }
        
        return consumedViewModel
    }
}

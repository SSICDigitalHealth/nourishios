//
//  HydrationModel.swift
//  Nourish
//
//  Created by Vlad Birukov on 14.06.17.
//  Copyright © 2017 SSIC. All rights reserved.
//

import Foundation

class HydrationModel : NSObject {
    var consumedHydration: Double = 0.0
    var targetHydration: Double = 0.0
    var unit: String = ""
}

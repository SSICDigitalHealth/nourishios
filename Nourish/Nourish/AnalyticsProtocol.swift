//
//  AnalyticsProtocol.swift
//  Nourish
//
//  Created by Gena Mironchyk on 5/11/17.
//  Copyright © 2017 SSIC. All rights reserved.
//

protocol AnalyticsProtocol {
    func initializeWith(userMeta : [String : Any])
    func logEventWith(name : String)
}

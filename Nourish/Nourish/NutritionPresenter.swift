//
//  NutritionPresenter.swift
//  Nourish
//
//  Created by Nova on 11/21/16.
//  Copyright © 2016 SSIC. All rights reserved.
//

import UIKit
import RxSwift

class NutritionPresenter: BasePresenterProtocol {
    internal func viewDidLayoutSubviews() {
    }

    
    let interactor = NutritionInteractor()
    let mapperObject = Nutrition_to_NutritionModel()
    let mapperModel = NutritionModel_to_Nutrition()
    var nutritionView : (NutritionViewProtocol & BaseViewProtocol)?
    var period : period = .today {
        didSet {
            self.viewWillAppear(false)
        }
    }
    
    func viewWillAppear(_ animated : Bool) {
        if nutritionView != nil {
            let _ = interactor.nutritionFor(date: Date()).throttle(0.5, scheduler: MainScheduler.instance).observeOn(MainScheduler.instance).subscribe(onNext: { [unowned self] nutrition in
                let model = self.mapperObject.transform(nutrition: nutrition[0])
                self.nutritionView?.setupWith(vitValue: model.vitamins, eleValue: model.electrolytes, macroValue: model.macronutrients, mineralValue: model.minerals, score: model.nutritionScore,period: self.period)
            }, onError: {error in
                self.nutritionView?.parseError(error: error, completion: nil)
            }, onCompleted: {
                LogUtility.logToFile("nutrition view completed")
            }, onDisposed: {})
        }
    }
    
    func viewDidAppear(_ animated : Bool) {
        
    }
    func viewWillDisappear(_ animated : Bool) {
        
    }
    func viewDidDisappear(_ animated : Bool) {
        
    }
}

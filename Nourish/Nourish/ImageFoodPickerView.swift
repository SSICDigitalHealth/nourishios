//
//  ImageFoodPickerView.swift
//  Nourish
//
//  Created by Nova on 5/11/17.
//  Copyright © 2017 SSIC. All rights reserved.
//

import UIKit

class ImageFoodPickerView: BaseView {

    @IBOutlet var imageView : UIImageView?
    var foodImage : UIImage?
    var foodList : [foodSearchModel]?
    @IBOutlet var tableView : UITableView?
    @IBOutlet var pickerView : NRPickerView?
    
    @IBOutlet var presenter : ImagePickerPresenter?
    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */
    override func viewWillAppear(_ animated: Bool) {
        self.basePresenter = presenter
        presenter?.foodImage = self.foodImage
        presenter?.foodModels = self.foodList
        presenter?.imagePickerView = self
        self.setupPicker()
        self.registerCells()
        super.viewWillAppear(animated)
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
    }
    
    private func registerCells() {
        self.tableView?.register(UINib(nibName:"ImagePickerTableViewCell" ,bundle:nil) , forCellReuseIdentifier: "ImagePickerTableViewCell")
    }
    
    private func setupPicker() {
        self.pickerView?.delegate = presenter
        self.pickerView?.dataSource = presenter
        self.pickerView?.textColor = UIColor.white
        self.pickerView?.highlightedTextColor = UIColor.white
        self.pickerView?.font = UIFont.systemFont(ofSize: 14)
        self.pickerView?.highlightedFont = UIFont.systemFont(ofSize: 14, weight: UIFontWeightBold)
        self.pickerView?.maskDisabled = true
    }
    
    func setupImageViewWith(image : UIImage) {
        self.imageView?.image = image
    }
    
    func reloadTableViewData() {
        self.tableView?.reloadData()
    }
    
    func reloadPickerView(ocasion : Ocasion) {
        self.pickerView?.selectItem(ocasion.rawValue - 1)
        self.pickerView?.reloadData()
    }
    
}

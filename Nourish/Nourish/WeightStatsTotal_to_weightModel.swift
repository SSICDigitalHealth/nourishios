//
//  WeightStatsTotal_to_weightModel.swift
//  Nourish
//
//  Created by Nova on 1/24/17.
//  Copyright © 2017 SSIC. All rights reserved.
//

import UIKit

class WeightStatsTotal_to_weightModel: NSObject {
    func transform(object : WeightStatsTotal, period : period) -> weightModel {
        let model = weightModel()
        model.averageWeight = object.averageWeight
        model.averageCalorie = object.averageCalorie
        let calories = object.caloriesArray.sorted(by: {$0.startDate < $1.startDate})
        let weights = object.weightsArray.sorted(by: {$0.startDate < $1.startDate})
        model.caloriesArray = self.caloriesArray(objs: calories, period: period)
        model.weightsArray = self.weightArray(objs: weights, period: period)
        model.weeklyPlanArray = object.weeklyPlanArray
        return model
    }
    
    private func calorieObj(obj : CalorieStatsSample, period : period) -> caloriesModel {
        let stringRep = self.stringFrom(start: obj.startDate, end: obj.endDate, period: period)
        
        let model = caloriesModel(calorie : obj.calories, caloriePeriodRange : stringRep)
        return model
    }
    
    private func weightObj(obj : WeightStatsSample, period : period) -> weightStatsModel {
        let stringRep = self.stringFrom(start: obj.startDate, end: obj.endDate, period: period)
        let model = weightStatsModel(weight : obj.weight, weightPeriodRange : stringRep)
        return model
    }
     
    private func weightArray(objs : [WeightStatsSample], period : period) -> [weightStatsModel] {
        var array : [weightStatsModel] = []
        for obj in objs {
            array.append(self.weightObj(obj: obj, period: period))
        }
        return array
    }
    
    private func caloriesArray(objs : [CalorieStatsSample], period : period) ->[caloriesModel] {
        var array : [caloriesModel] = []
        for obj in objs {
            array.append(self.calorieObj(obj: obj, period: period))
        }
        return array
    }
    
    private func stringFrom(start : Date, end : Date, period : period) -> String {
        let dateformatter = DateFormatter()
        dateformatter.calendar = Calendar.current
        dateformatter.dateFormat = "dd/MM"
        
        var stringRep = ""
        switch period {
        case .today:
            stringRep = dateformatter.string(from: start)
        case .weekly:
            stringRep = dateformatter.string(from: start)
        case .monthly:
            stringRep = "\(dateformatter.string(from: start)) - \(dateformatter.string(from: end))"
        }
        return stringRep
    }
}


//
//  ActivityWeightChart.swift
//  Nourish
//
//  Created by Andrey Zmushko on 9/7/17.
//  Copyright © 2017 SSIC. All rights reserved.
//

import UIKit

protocol ActivityWeightChartDelegate {
    func numberOfBars(in ActivityWeightChart: ActivityWeightChart) -> Int
    func activityWeightChart(_ ActivityWeightChart: ActivityWeightChart, barAtIndex: Int) -> ActivityWeightChart.Bar?
    
    func topLabel(in ActivityWeightChart: ActivityWeightChart) -> String
    func bottomLabel(in ActivityWeightChart: ActivityWeightChart) -> String
    func middleAxisValue(in ActivityWeightChart: ActivityWeightChart) -> CGFloat
}

class ActivityWeightChart: UIView {
    
    typealias Bar = (value: CGFloat, legenda: String)
    
    let topLabel = UILabel()
    let bottomLabel = UILabel()
    let labelColor = NRColorUtility.hexStringToUIColor(hex: "#747474")
    let labelFont = UIFont.systemFont(ofSize: 12.0)
    let legendaFont = UIFont.systemFont(ofSize: UIScreen.main.bounds.width > 320 ? 11.0 : 9.0)
    var bars: [Bar] = []
    var legenda: [UILabel] = []
    
    var middleAxisPosition = CGFloat()

    override func awakeFromNib() {
        super.awakeFromNib()
        self.addSubview(topLabel)
        self.addSubview(bottomLabel)
        self.topLabel.textColor = self.labelColor
        self.topLabel.font = self.labelFont
        self.bottomLabel.textColor = self.labelColor
        self.bottomLabel.font = self.labelFont
        
        self.positionLabel(label: self.topLabel, y: 14.0)
        self.positionLabel(label: self.bottomLabel, y: 114.0)
    }
   
    func setupWith(dataSource: ActivityWeightChartDelegate) {
        
        self.topLabel.text = dataSource.topLabel(in: self)
        self.bottomLabel.text = dataSource.bottomLabel(in: self)
        self.middleAxisPosition = dataSource.middleAxisValue(in: self)
        
        self.bars.removeAll()
        for label in legenda {
            label.removeFromSuperview()
        }
        self.legenda.removeAll()
        for index in 0 ..< dataSource.numberOfBars(in: self) {
            if let bar = dataSource.activityWeightChart(self, barAtIndex: index) {
                bars.append(bar)
                let label = UILabel()
                label.textColor = self.labelColor
                label.font = self.legendaFont
                self.addSubview(label)
                legenda.append(label)
            }
        }
        
        self.setNeedsDisplay()
    }
    
    private func positionLabel(label: UILabel, y: CGFloat) {
        label.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint(item: label,
                           attribute: .left,
                           relatedBy: .equal,
                           toItem: self,
                           attribute: .left,
                           multiplier: 1.0,
                           constant: 0.0).isActive = true
        NSLayoutConstraint(item: label,
                           attribute: .bottom,
                           relatedBy: .equal,
                           toItem: self,
                           attribute: .top,
                           multiplier: 1.0,
                           constant: y).isActive = true
    }
    
    private func positionLegenda(label: UILabel, x: CGFloat) {
        label.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint(item: label,
                           attribute: .top,
                           relatedBy: .equal,
                           toItem: self,
                           attribute: .top,
                           multiplier: 1.0,
                           constant: 120.0).isActive = true
        NSLayoutConstraint(item: label,
                           attribute: .centerX,
                           relatedBy: .equal,
                           toItem: self,
                           attribute: .left,
                           multiplier: 1.0,
                           constant: x).isActive = true
    }
    
    override func draw(_ rect: CGRect) {
        self.drawAxis(from: CGPoint(x: 0, y: 114.0),
                      till: CGPoint(x: self.bounds.width, y: 114.0),
                      color: NRColorUtility.hexStringToUIColor(hex: "#cccccc"),
                      dashed: false)
        self.drawAxis(from: CGPoint(x: 0, y: 14.0),
                      till: CGPoint(x: self.bounds.width, y: 14.0),
                      color: NRColorUtility.hexStringToUIColor(hex: "#cccccc"),
                      dashed: true)
        
        let avrY = 114.0 - 100 * middleAxisPosition
        self.drawAxis(from: CGPoint(x: 0, y: avrY),
                      till: CGPoint(x: self.bounds.width, y: avrY),
                      color: NRColorUtility.hexStringToUIColor(hex: "#cccccc"),
                      dashed: true)
        
        
        var chartWidth: CGFloat {
            let total = self.bounds.width - 56.0
            return total
        }
        
        func legendaX(_ index: Int) -> CGFloat {
            let gap = 56.0 + (chartWidth / CGFloat(self.bars.count)) * CGFloat(index)
            
            return gap + (chartWidth / CGFloat(self.bars.count)) / 2
        }
        
        func pointAt(index: Int) -> CGPoint {
            var y: CGFloat {
                let apr = self.bars[index].value
                if abs(apr - self.middleAxisPosition) < 0.1 {
                    return self.middleAxisPosition
                }
                return apr
            }
            return CGPoint(x: legendaX(index),
                           y: 114.0 - 100 * y)
        }
        
        func radiusAt(count: Int) -> CGFloat {
            return count > 7 ? 1.5 : 2.5
        }

        func lineWidthAt(count: Int) -> CGFloat {
            return count > 7 ? 1.0 : 2.0
        }
        
        for (index, bar) in self.bars.enumerated() {
            var colorChartDot = NRColorUtility.hexStringToUIColor(hex: "1172b9")
            var colorChartLine = NRColorUtility.hexStringToUIColor(hex: "1172b9")

            legenda[index].text = bar.legenda
            self.positionLegenda(label: legenda[index], x: legendaX(index))
            
            if bar.value == -1 {
                colorChartDot = UIColor.clear
            }
            
            if index <= self.bars.count - 2 {
                if bars[index + 1].value == -1 || (bars[index].value == -1 && bars[index + 1].value != -1) {
                    colorChartLine = UIColor.clear
                }
            }
                
            self.drawDot(center: pointAt(index: index),
                         radius: radiusAt(count: self.bars.count), color: colorChartDot)
            
            if index < self.bars.count - 1 {
                self.drawLine(start: pointAt(index: index),
                              end: pointAt(index: index + 1),
                              width: lineWidthAt(count: self.bars.count), color: colorChartLine)
            }
        }
        
    }
    
    private func drawAxis(from: CGPoint, till: CGPoint, color: UIColor, dashed: Bool) {
        let path = UIBezierPath()
        path.move(to: from)
        path.addLine(to: till)
        path.lineWidth = 1.0
        if dashed == true {
            path.lineJoinStyle = .miter
            path.setLineDash([2.0, 1.0], count: 2, phase: 0)
        }
        color.setStroke()
        path.stroke()
    }
    
    private func drawDot(center: CGPoint, radius: CGFloat, color: UIColor) {
        self.drawSector(fillColor: color,
                        center: center,
                        radius: radius,
                        startAngle: 0,
                        endAngle: 360,
                        clockwise: true)
    }
    
    private func drawLine(start: CGPoint, end: CGPoint, width: CGFloat, color: UIColor) {
        let path = UIBezierPath()
        path.move(to: start)
        path.addLine(to: end)
        path.lineWidth = width
        color.setStroke()
        path.stroke()
    }
}

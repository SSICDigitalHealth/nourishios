//
//  RecomendationDetailViewModel.swift
//  Nourish
//
//  Created by Vlad Birukov on 29.08.17.
//  Copyright © 2017 SSIC. All rights reserved.
//

import Foundation

class RecomendationDetailViewModel {
    var caption: String?
    var titleText = ""
    var descriptionText = ""
    var data : [foodSearchModel]?
    var dataRecipies: [RecipiesViewModel]?
}

class RecipiesViewModel {
    var foodId: String = ""
    var nameFood: String = ""
    var groupId: String?
    var image: UIImage?
    var pathForImage : String = ""
    var numberCal: Double = 0.0
    var unit: String = ""
    var amount: Double = 0.0
    var grams: Double = 0.0
    var ingredient = [String]()
    var instructions = [String]()
    var isFavorite = false
}

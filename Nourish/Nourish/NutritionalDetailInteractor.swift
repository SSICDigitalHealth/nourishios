//
//  NutritionalDetailInteractor.swift
//  Nourish
//
//  Created by Vlad Birukov on 19.06.17.
//  Copyright © 2017 SSIC. All rights reserved.
//

import Foundation
import RxSwift

class NutritionalDetailInteractor {
    
    private let repository = NestleProgressRepository.shared
    
    func execute(startDate: Date, endDate: Date) -> Observable<NutritionBalanceModel> {
        let nutritionDetailBalanceModel = self.repository.fetchNutritionalDetailedBalanceModelFor(startDate: startDate,endDate: endDate)
        let nutritionModel = self.repository.fetchNutritionalBalanceModelFor(startDate:startDate, endDate:endDate)
        
        let combineObject = Observable.combineLatest(nutritionDetailBalanceModel, nutritionModel, resultSelector: {nutritionDetail, nutritModel -> NutritionBalanceModel in
            
            nutritionDetail.nutritionData = nutritModel
            
            return nutritionDetail
        })
        
        return combineObject
        
    }
    
    
//    private func transform(start: Date, end: Date, model: [MicroelementInformation]) -> NutritionBalanceModel {
//        let nameNutrition: [NutritionType] = [.grains, .protein, .dairy, .vegs, .fruits]
//        let nutritionBalance = NutritionBalanceModel()
//        /////
//        nutritionBalance.message = "Your overall balance is the best yet. Well done!"
//        
//        let foodName = [["Orange juice", "Yogurt","Green vegetables"],["Orange juice", "Yogurt","Green vegetables"],["Orange juice", "Yogurt","Green vegetables"],["Orange juice", "Yogurt","Green vegetables"],["Orange juice", "Yogurt","Green vegetables"]]
//        let arr = [2.4, 5.0, 12.2, 23.0, 32.0, 43.5, 32.0, 2.4, 5.0, 12.2, 23.0, 32.0, 43.5, 32.0, 2.4, 5.0, 12.2, 23.0, 32.0, 43.5, 32.0, 2.4, 5.0, 12.2, 23.0, 32.0, 43.5, 32.0, 32.0, 2.4, 5.0, 12.2, 23.0, 32.0, 43.5, 32.0, 2.4, 5.0, 12.2, 23.0, 32.0, 43.5, 32.0]
//        ///
//        for i in 0..<nameNutrition.count {
//            
//            let nutritionElement = NutritionElementModel()
//            ////
//            nutritionElement.dataCharts = [NutrientInformationChartModel]()
//             for j in 0...self.daysBetweenDates(startDate: start, endDate: end) {
//                if i == 0 {
//                    let data = NutrientInformationChartModel()
//                    data.label = self.addDay(startDate: start, number: j)
//                    data.consumed = arr[j]
//                    data.wholeGrainsConsumed = arr[j]
//                    nutritionElement.dataCharts?.append(data)
//                } else {
//                    let data = NutrientInformationChartModel()
//                    data.label = self.addDay(startDate: start, number: j)
//                    data.consumed = arr[j]
//                    nutritionElement.dataCharts?.append(data)
//                }
//                
//            }
//            nutritionElement.richNutritionFood = foodName[i]
//            ////
//            
//            let currentNutrition = model.filter({NutritionType.stringToType(string: $0.idNameMicroelement) == nameNutrition[i]})
//            
//            if currentNutrition.count > 0 {
//                nutritionElement.type = nameNutrition[i]
//                
//                if currentNutrition[0].upperLimit != nil {
//                    if currentNutrition[0].consumedMicroelement > currentNutrition[0].lowerLimit! {
//                        nutritionElement.target = currentNutrition[0].upperLimit!
//                    } else {
//                        nutritionElement.target = currentNutrition[0].lowerLimit!
//                    }
//                } else {
//                    nutritionElement.target = currentNutrition[0].lowerLimit!
//                }
//                nutritionElement.unit = currentNutrition[0].unit.replacingOccurrences(of: "_eq", with: "")
//                nutritionElement.message = "Look for whole grain products such as whole wheat bread or pasta."
//                
//                if currentNutrition[0].contributors.count > 0 {
//                    nutritionElement.arrNutrition = [ElementModel]()
//                    if nutritionElement.type == .grains {
//                        for j in 0..<currentNutrition[0].contributors.count + 2 {
//                            if j == 0 {
//                                let element = ElementModel()
//                                element.nameElement = "Whole grains"
//                                nutritionElement.arrNutrition?.append(element)
//                            } else if j == 1 {
//                                let element = ElementModel()
//                                element.nameElement = "Refined grains"
//                                nutritionElement.arrNutrition?.append(element)
//                            } else {
//                                let element = ElementModel()
//                                element.amountElement = currentNutrition[0].contributors[j - 2].quality
//                                element.consumedState = currentNutrition[0].contributors[j - 2].consumed
//                                element.nameElement = currentNutrition[0].contributors[j - 2].nameFood
//                                element.unit = currentNutrition[0].contributors[j - 2].unit
//                                nutritionElement.arrNutrition?.append(element)
//                            }
//                        }
//                    } else {
//                        for j in 0..<currentNutrition[0].contributors.count {
//                            let element = ElementModel()
//                            element.amountElement = currentNutrition[0].contributors[j].quality
//                            element.consumedState = currentNutrition[0].contributors[j].consumed
//                            element.nameElement = currentNutrition[0].contributors[j].nameFood
//                            element.unit = currentNutrition[0].contributors[j].unit
//                            nutritionElement.arrNutrition?.append(element)
//                        }
//                    }
//                }
//            }
//            
//            nutritionBalance.arrNutritionElement.append(nutritionElement)
//        }
//        
//        return nutritionBalance
//    }

    private func addDay(startDate: Date, number: Int) -> Date {
        return Calendar.current.date(byAdding: .day, value: number, to: startDate)!
    }
    
    private func daysBetweenDates(startDate: Date, endDate: Date) -> Int
    {
        let calendar = Calendar.current
        let components = calendar.dateComponents([.day], from: startDate, to: endDate)
        return components.day!
    }
}

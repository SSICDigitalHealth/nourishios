//
//  HydrationBalanceViewModel.swift
//  Nourish
//
//  Created by Vlad Birukov on 29.08.17.
//  Copyright © 2017 SSIC. All rights reserved.
//

import Foundation

class HydrationBalanceViewModel : NSObject {
    var unit: String = ""
    var total: Double = 0.0
    var arrHydrationElement : [ElementViewModel]?
    var message : String = ""
}

//
//  DiaryRecordModel.swift
//  Nourish
//
//  Created by Nova on 11/10/16.
//  Copyright © 2016 Samsung. All rights reserved.
//

import UIKit

enum ServingType : Int {
    case cup
    case weight
    case plate
    case slice
    case servings
    
    static func random() -> ServingType {
        var maxValue = UInt32(slice.rawValue)
        maxValue += 1
        let rand = arc4random_uniform(maxValue)
        return ServingType(rawValue: Int(rand))!
    }
    
    static let allValues = [cup,weight,plate,slice]

    static func description(type:ServingType) -> String {
        switch (type) {
        case .cup:
            return "Cup"
        case .weight:
            return "weight"
        case .plate:
            return "plate"
        case .slice:
            return "slice"
        case .servings:
            return "servings"
        }
    }
}

enum Ocasion : Int {
    case breakfast = 1
    case lunch
    case dinner
    case snacks
    
    static func random() -> Ocasion {
        var maxValue = UInt32(snacks.rawValue)
        maxValue += 1
        let rand = arc4random_uniform(maxValue)
        return Ocasion(rawValue: Int(rand))!
    }
    
    static func mealPlannerJsonRepresentation(ocasion : Ocasion) -> String{
        switch ocasion {
        case .breakfast:
            return "breakfast"
        case .lunch:
            return "lunch"
        case .dinner:
            return "dinner"
        case .snacks:
            return "snack"
        }
    }
    
    
    static func stringRepresentation(servingType : Ocasion) -> String {
        switch servingType {
        case .breakfast:
            return "Breakfast"
        case .lunch:
            return "Lunch"
        case .dinner:
            return "Dinner"
        case .snacks:
            #if OPTIFASTVERSION
            return "All day in-betweens"
            #else
            return "Snacks"
            #endif
        }
    }
    
    static func mealTypeString(servingType : Ocasion) -> String {
        switch servingType {
        case .breakfast:
            return "Breakfast"
        case .lunch:
            return "Lunch"
        case .dinner:
            return "Dinner"
        case .snacks:
            return "Snacks & Drinks"
        }
    }
    
    static func stringDescription(servingType: Ocasion) -> String {
        switch servingType {
        case  .breakfast:
            return "Breakfast"
        case .lunch:
            return "Lunch"
        case .dinner:
            return "Dinner"
        case .snacks:
            #if OPTIFASTVERSION
                return "All day in-betweens"
            #else
                return "Snacks"
            #endif
        }
    }
    
    static func enumFrom(stringRep : String) -> Ocasion {
        let stringToParse = stringRep.lowercased()
        
        switch stringToParse {
        case "breakfast":
            return .breakfast
        case "lunch":
            return .lunch
        case "dinner":
            return .dinner
        case "snacks","snack", "All day in-betweens", "all day in-betweens":
            return .snacks
        default :
            return .breakfast
        }
        
    }
    
    static func enumFromMealPlanner(string: String) -> Ocasion {
        let stringToParse = string.lowercased()
        
        switch stringToParse {
        case "breakfast":
            return .breakfast
        case "lunch":
            return .lunch
        case "dinner":
            return .dinner
        case "snack","All day in-betweens":
            return .snacks
        default :
            return .breakfast
        }
    }
    
}

class MealRecordModel : NSObject {
    var date : Date?
    var foodTitle : String?
    var idString : String?
    var foodId : String?
    var userFoodID : String?
    var servingAmmount : Double?
    var servingType : String?
    var caloriesPerServing : Double?
    var ocasion : Ocasion?
    var isFavourite : Bool = false
    var calories : Double?
    var grams : Double?
    var isFromNoom : Bool = false
    //groups
    var isFromOperaion = false
    var isGroupFavourite : Bool = false
    var isMarkedForFav : Bool = false
    var groupedMealArray : [MealRecordModel] = []
    var groupName : String? = nil
    var groupID : String? = nil
    var noomID : String? = nil
    var nutrs : [Nutrient]?
    var numberOfItems : Int?
    var userPhotoId : String?
    var groupPhotoID : String?
    
    var sourceFoodDB : String?
    var sourceFoodDBID : String?
    
    var groupFoodUUID : String?
    
    var localCacheID : String?
    
    func descriptionText() -> String {
        var stringToReturn = ""
        
        if self.groupedMealArray.count > 1 {
            if self.groupName != nil {
                stringToReturn = self.groupName!
            }
        } else {
            if self.groupedMealArray.count == 1 {
                let mealTitle = self.groupedMealArray.first?.foodTitle
                if mealTitle != nil {
                    stringToReturn = mealTitle!
                }
                
            } else {
                if self.foodTitle != nil {
                    stringToReturn = self.foodTitle!
                }
            }
        }
        
        let uuid = NSUUID(uuidString : stringToReturn)
        if uuid != nil {
            stringToReturn = ""
        }
        
        return stringToReturn
    }
    
    func groupCalories() -> Double {
        if self.groupedMealArray.count > 1 {
            var sum = 0.0
            groupedMealArray.forEach { sum += $0.calories != nil ? $0.calories! : 0.0}
            return sum
        } else {
            return self.calories != nil ? self.calories! : 0.0
        }
    }
    
    func foodSearchRepresentation() -> foodSearchModel {
        
        var model = foodSearchModel()
        model.foodId = self.foodId ?? self.idString ?? ""
        model.grams = self.grams
        model.calories = String(format : "%.1f", self.calories ?? 0.0)
        model.amount = self.servingAmmount ?? 1
        model.foodTitle = self.foodTitle ?? ""
        model.measure = self.servingType ?? ""
        model.unit = self.servingType ?? ""
        model.occasion = self.ocasion
        model.userPhotoId = self.userPhotoId
        
        return model
    }
    
}

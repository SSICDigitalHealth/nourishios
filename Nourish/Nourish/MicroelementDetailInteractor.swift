//
//  MicroelementDetailInteractor.swift
//  Nourish
//
//  Created by Vlad Birukov on 16.06.17.
//  Copyright © 2017 SSIC. All rights reserved.
//

import Foundation
import RxSwift

class  MicroelementDetailInteractor {
    
    let repository = NestleProgressRepository.shared
    
    func execute(startDate: Date, endDate: Date) -> Observable <MicroelementBalanceModel> {
        return repository.getDetailedWatchoutListFor(startDate: startDate, endDate: endDate).flatMap({microelement -> Observable<MicroelementBalanceModel> in
            return Observable.just(self.transform(model: microelement))
        })

    }
    
    private func transform(model: MicroelementBalanceModel) -> MicroelementBalanceModel {
        if let microelement = model.arrMicroelementName {
            for i in 0..<microelement.count {
                if microelement[i].arrMicroelement != nil {
                    microelement[i].arrMicroelement = repository.formationTopFood(elementModel: self.transformWithRound(array: microelement[i].arrMicroelement!, target: microelement[i].target))
                }
            }
        }
        return model
    }
    
    private func transformWithRound(array: [ElementModel], target: Double) -> [ElementModel] {
        var elementModelArray = [ElementModel]()
        
        for item in array {
            let elementModel = ElementModel()
            elementModel.nameElement = item.nameElement
            if item.consumedState != nil {
                elementModel.consumedState = item.consumedState?.roundMicroelement(target: target)

            }
            elementModel.unit = item.unit
            elementModelArray.append(elementModel)
        }
        return elementModelArray
    }
}

//
//  NRNutritionStatsPresenter.swift
//  Nourish
//
//  Created by Vino (Vinodha) Sundaramoorthy on 12/13/16.
//  Copyright © 2016 SSIC. All rights reserved.
//

import UIKit
import RxSwift


class NRNutritionStatsPresenter: BasePresenter ,NRSegmentedControlDelegate , NutrientTokenViewDelegate{
    
    var statsView : NRNutritionStatsProtocol?
    var tokenView : NutrientTokenView!
    var selectedNutrient : nutrients? = nil
    var nutritionModel : nutritionStatsModel!
    var interactor = NutritionStatsInteractor()
    var nutrientsArray : [nutrients] = []
    var periodStats : period = .today
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.setUpNutritionModelForPeriod(period: .today)
    }
    
    func setUpNutritionModelForPeriod(period : period) {
        self.subscribtions.append(self.interactor.getNutritionStatsFor(period: period).observeOn(MainScheduler.instance).subscribe(onNext: { [unowned self] model in
            self.nutritionModel = model
            NutritionsUtility.formatNutrientsName(nutritions: self.nutritionModel.nutrArray)
        }, onError: {error in }, onCompleted: {
            self.statsView?.setUpNutritionStats(model: self.nutritionModel, period: period)
        }, onDisposed: {}))
    }
    
    // MARK: NutrientTokenViewDelegate
    func setSelectedNutrient(nutrient: nutrients) {
        self.selectedNutrient = nutrient
    }
    
    // MARK:NRSegmentedControlDelegate
    func segmentedControlDidPressedItemAtIndex(segmentedControl: NRSegmentedControl, index: Int) {
        if periodStats != period(rawValue:index)! {
            periodStats = period(rawValue:index)!
            //EventLogger.logOnNutritionScreenClickFor(period : periodStats)
            statsView?.startActivityProgress()
            self.setUpNutritionModelForPeriod(period: periodStats)
        }
    }
}

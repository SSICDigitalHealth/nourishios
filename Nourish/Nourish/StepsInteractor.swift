//
//  StepsInteractor.swift
//  Nourish
//
//  Created by Vlad Birukov on 31.08.17.
//  Copyright © 2017 SSIC. All rights reserved.
//

import Foundation
import RxSwift

class StepsInteractor {
    let activityRepo = NewActivityRepository.shared
    
    func execute(startDate: Date, endDate: Date) -> Observable <StepsModel> {
        return self.activityRepo.stepsModelFrom(startDate:startDate, endDate:endDate)
    }
    
    private func generateRandomBetween(max : Int, min : Int) -> Int {
        return min + Int(arc4random_uniform(UInt32(max - min + 1)))
    }
}

//
//  WeightInteractor.swift
//  Nourish
//
//  Created by Vlad Birukov on 31.08.17.
//  Copyright © 2017 SSIC. All rights reserved.
//

import Foundation
import RxSwift

class WeightInteractor {
    let repository = UserRepository.shared
    let activityRepo = NewActivityRepository.shared
    
    private func weight(startDate: Date, endDate: Date) -> Observable <WeightModel> {
        return Observable<WeightModel>.create{(observer) -> Disposable in
            let pause = Int(arc4random_uniform(4))
            let dispat = DispatchTime.now() + .seconds(pause)
            DispatchQueue.main.asyncAfter(deadline: dispat, execute: {
                let weightModel = WeightModel()
                let metric = [true, false]
                weightModel.isMetric = metric[self.generateRandomBetween(max: 1, min: 0)]
                weightModel.weight = [(subtitle:Date, value:Double?)]()
                if Calendar.current.compare(startDate, to: endDate, toGranularity: .day) == .orderedSame {
                    
                    weightModel.dailyWeightLogged = metric[self.generateRandomBetween(max: 1, min: 0)]
                    weightModel.progress = Double(self.generateRandomBetween(max: 23, min: 1))
                    weightModel.weight?.append((subtitle: startDate, value: Double(self.generateRandomBetween(max: 80, min: 60))))
    
                } else {
                    for i in 0...self.daysBetweenDates(startDate: startDate, endDate: endDate) {
                        weightModel.weight?.append((subtitle: self.addDay(startDate: startDate, number: i) , value: Double(self.generateRandomBetween(max: 80, min: 60))))
                    }
                }
                
                observer.onNext(weightModel)
                observer.onCompleted()
            })
            
            return Disposables.create()
        }
    }
    
    func execute(startDate: Date, endDate: Date) -> Observable <WeightModel> {
        return self.activityRepo.weightModel(startDate:startDate, endDate:endDate)
    }
    /*
    func execute(startDate: Date, endDate: Date) -> Observable <WeightModel> {
        let weightModel = self.weight(startDate: startDate, endDate: endDate)
        let userId = self.repository.getCurrentUserID()
        
        let zip = Observable.zip(weightModel, userId, resultSelector: {weightMod, userID -> WeightModel in
            weightMod.userId = userID
            return weightMod
        })
        
        return zip
    }
    */
    private func addDay(startDate: Date, number: Int) -> Date {
        return Calendar.current.date(byAdding: .day, value: number, to: startDate)!
    }
    
    private func daysBetweenDates(startDate: Date, endDate: Date) -> Int
    {
        let calendar = Calendar.current
        let components = calendar.dateComponents([.day], from: startDate, to: endDate)
        return components.day!
    }
    
    private func generateRandomBetween(max : Int, min : Int) -> Int {
        return min + Int(arc4random_uniform(UInt32(max - min + 1)))
    }
}

//
//  BaseNestleDataStore.swift
//  Nourish
//
//  Created by Nova on 10/10/17.
//  Copyright © 2017 SSIC. All rights reserved.


import UIKit

class BaseNestleDataStore: NSObject {

    func requestWithToken(token : String, url : URL) -> URLRequest {
        var request = URLRequest(url : url)
        request.setValue("Bearer \(token)", forHTTPHeaderField: kOAUTHAuthorizationHeader)
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        request.timeoutInterval = 20.0
        return request
    }

    func log(foodProcessingOrWithings : Bool,url : String?, requestMethod : String? , urlResponse : URLResponse?, requestBody : [String : Any]?, error : Error?, responceBody : Data?) {
        
        if foodProcessingOrWithings == false {
            if error != nil || (urlResponse as? HTTPURLResponse)?.statusCode != 200 && (urlResponse as? HTTPURLResponse)?.statusCode != 201 {
                let logString = String(format: "URL - %@ - Method - %@ - Error - %@ - Status Code - %d", url ?? "", requestMethod ?? "",error?.localizedDescription ?? "",(urlResponse as? HTTPURLResponse)?.statusCode ?? 0)
                LogUtility.logToFile(logString)
            }
        } else {
            var respBody = [String : Any]()
            if responceBody != nil {
                do {
                    respBody = try JSONSerialization.jsonObject(with: responceBody!, options: []) as? [String : Any] ?? [String : Any]()
                } catch let err as NSError  {
                    print("Error response dictionary serialization ", err.localizedDescription)
                }
            }
            let logString = String(format: "URL - %@ - Method - %@ - Status Code - %d - Request Body - %@ - Error - %@ - Response  Body - %@", url ?? "", requestMethod ?? "", (urlResponse as? HTTPURLResponse)?.statusCode ?? 0, requestBody ?? [String : Any](), error?.localizedDescription ?? "", self.prettyPrint(with: respBody))
            LogUtility.logToFile(logString)
        }
        
    }
    
    func printUTF8String(data : Data) {
        print(String(data : data, encoding : .utf8) ?? "")
    }
    
    func prettyPrint(with json: [String:Any]) -> String{
        let data = try! JSONSerialization.data(withJSONObject: json, options: .prettyPrinted)
        let string = NSString(data: data, encoding: String.Encoding.utf8.rawValue)
        return string as! String
    }
}

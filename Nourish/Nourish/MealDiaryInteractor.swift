//
//  MealDiaryInteractor.swift
//  Nourish
//
//  Created by Nova on 11/10/16.
//  Copyright © 2016 Samsung. All rights reserved.
//

import UIKit
import RxSwift

class MealDiaryInteractor: NSObject {
    var fakeGeneratedDate = Date()
    let cacheToObjectMapper = MealRecordCache_to_MealRecord()
    let notificationRoutine = LocalNotificationsRoutine.shared
    let foodCardCacheRepository = FoodCardCacheRepository()
    var mealRepo = MealHistoryRepository.shared
    
    func arrayOfFetchedMealsForDate(date : Date,onlyCache : Bool) -> Observable<[MealRecord]> {
        let object = self.mealRepo.getFoodFor(date: date, onlyCache: onlyCache).flatMap({cacheList -> Observable<[MealRecord]> in
            let mealModels = self.cacheToObjectMapper.transform(cacheList: cacheList)
            
            self.notificationRoutine.setupNotifications(mealRecords: mealModels, date: date)
            
            //let mealsToReturn = self.cacheToObjectMapper
            return Observable.just(mealModels)
        })
        return object
    }
    
    
    func fetchOnlyNutrients(foodSearchModel : foodSearchModel) -> Observable<[Nutrient]> {
        return self.fetchFoodCardDetails(foodSearchModel : foodSearchModel).flatMap({details -> Observable<[Nutrient]> in
            return Observable.just(details.nutrients!)
        })
    }

    func fetchFoodCardDetails(foodSearchModel : foodSearchModel) -> Observable<FoodCardDetails> {
        let foodId = foodSearchModel.mealIdString != "" ? foodSearchModel.mealIdString : foodSearchModel.foodId
        let cache = self.foodCardCacheRepository.fetchCacheFor(foodId: foodId!)
        if cache != nil {
            return Observable.just(cache!)
        } else {
            let nutr = self.fetchNutrientsFor(model: foodSearchModel)
            let servingType = Observable.just([foodSearchModel.unit])
            let zipValue = Observable.zip(nutr, servingType, resultSelector : { nutrArray, servingArray -> FoodCardDetails in
                let details = FoodCardDetails(nutrients : nutrArray, servingTypes : servingArray)
                self.foodCardCacheRepository.storeCache(foodId: foodId!, cache: details)
                return details
            })
            return zipValue
        }
    }
    
    
    func fetchNutrientsFor(model : foodSearchModel) -> Observable<[Nutrient]> {
        return self.mealRepo.fetchNutrientDetailsForRecomFood(foodModel: model)
    }
    
    func deleteFoodItem(foodID : [String],date:Date,saveChat:Bool) -> Observable<Double> {
        return self.mealRepo.deleteFood(foodID: foodID,date:date,saveChat:saveChat)
    }
    
    func deleteGroupItems(array : [MealRecordModel]) -> Observable<Double> {
        var ids : [String] = []
        
        for item in array {
            if let id = item.userFoodID ?? item.localCacheID {
                ids.append(id)
            }
        }
        let date = array.first?.date ?? Date()
        return self.deleteFoodItem(foodID: ids, date: date, saveChat: true)
        /*
        var arrayObs : [Observable<Double>] = []
        for item in array {
            let id = item.userFoodID ?? item.localCacheID
            arrayObs.append(self.deleteFoodItem(foodID: id!, date: item.date!,saveChat:array.last == item ? true : false))
        }
        let singleObs = Observable.from(arrayObs).merge()
        let sequence = singleObs.toArray()
        
        let value = sequence.takeLast(1).flatMap { values in
            return Observable.just(values.last!)
        }
        return value
         */
    }
    
    func generateRandomBetween(max : Int, min : Int) -> Int {
        return min + Int(arc4random_uniform(UInt32(max - min + 1)))
    }
    
    func randomString(length: Int) -> String {
        
        let letters : NSString = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"
        let len = UInt32(letters.length)
        
        var randomString = ""
        
        for _ in 0 ..< length {
            let rand = arc4random_uniform(len)
            var nextChar = letters.character(at: Int(rand))
            randomString += NSString(characters: &nextChar, length: 1) as String
        }
        
        return randomString.lowercased()
    }
}

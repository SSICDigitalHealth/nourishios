//
//  ActivityCaloricProgressChartPresenter.swift
//  Nourish
//
//  Created by Andrey Zmushko on 9/1/17.
//  Copyright © 2017 SSIC. All rights reserved.
//

import UIKit
import RxSwift


class ActivityCaloricProgressChartPresenter: ActivityBaseCellPresenter, ActivityBarChartDelegate {

    let interactor = CaloricProgressInteractor()
    var caloricProgressViewModel = CaloricProgressViewModel()
    let mapperObjectCaloricProgress = CaloricProgressModel_to_CaloricProgressViewModel()
    
    let middleAxisMultiplier = 0.67
    var epoch: (startDate: Date, endDate: Date)?
    
    var maxCal = Double()
    var avgConsumed = Double()
    
    func numberOfBars(in activityBarChart: ActivityBarChart) -> Int {
        if let state = self.caloricProgressViewModel.caloricState {
            return state.count
        }
        else {
            if let epoch = self.epoch {
                return self.numberOfDaysIn(Interval: epoch.startDate, epoch.endDate)
            }
            else {
                return 0
            }
        }
    }
    
    func topAxisValue(in activityBarChart: ActivityBarChart) -> CGFloat {
        return CGFloat(avgConsumed / (maxCal + Double.leastNonzeroMagnitude))
    }

    func activityBarChart(_ activityBarChart: ActivityBarChart, barAtIndex: Int) -> ActivityBarChart.Bar? {
        var burnedLower: CGFloat = 0.0
        var consumedLower: CGFloat = 0.0
        var burnedHigher: CGFloat = 0.0
        var consumedHigher: CGFloat = 0.0
        var legenda: String {
            let formatter = DateFormatter()
            if let state = self.caloricProgressViewModel.caloricState {
                formatter.dateFormat = {
                    if state.count <= 7 {
                        if let epoch = self.epoch {
                            if Calendar.current.isDateInToday(epoch.endDate) == false {
                                return "E"
                            }
                        }
                    }
                    return "MM/dd"
                }()
                if state.count <= 7 || (barAtIndex == 1 || barAtIndex == state.count - 2) {
                    var index: Int {
                        if state.count > 7 && barAtIndex == 1 {
                            return 0
                        }
                        else if state.count > 7 && barAtIndex == state.count - 2 {
                            return state.count - 1
                        }
                        return barAtIndex
                    }
                    return formatter.string(from: state[index].subtitle).uppercased()
                }
            }
            return ""
        }
        if let state = self.caloricProgressViewModel.caloricState {
            burnedLower = CGFloat(state[barAtIndex].cal.bmrCal / (self.maxCal + Double.leastNonzeroMagnitude))
            
            if state[barAtIndex].cal.userConsumedCal > state[barAtIndex].cal.maxConsumedCal {
                consumedLower = CGFloat(state[barAtIndex].cal.maxConsumedCal / (self.maxCal + Double.leastNonzeroMagnitude))
            } else {
                consumedLower = CGFloat(state[barAtIndex].cal.userConsumedCal / (self.maxCal + Double.leastNonzeroMagnitude))
            }
            
            burnedHigher = CGFloat(state[barAtIndex].cal.userBurnedCal / (self.maxCal + Double.leastNonzeroMagnitude)) + burnedLower
            if state[barAtIndex].cal.maxConsumedCal > state[barAtIndex].cal.userConsumedCal {
                consumedHigher = CGFloat(consumedLower)
            } else {
                consumedHigher = CGFloat((state[barAtIndex].cal.userConsumedCal - state[barAtIndex].cal.maxConsumedCal) / (self.maxCal + Double.leastNonzeroMagnitude)) + consumedLower
            }
        }
        return (burned: (burnedLower, burnedHigher), consumed: (consumedLower, consumedHigher), legenda: legenda)
    }
    
    func topLabel(in activityBarChart: ActivityBarChart) -> String {
        return String(format: "%.0f kcal", self.avgConsumed)
    }
    
    func bottomLabel(in activityBarChart: ActivityBarChart) -> String {
        return "0 kcal"
    }

    private func getInformation(cell: ActivityCaloricProgressChartCell, startDate: Date, endDate: Date) {
        cell.baseView.viewWillAppear(true)
        if self.daysBetweenDates(startDate: startDate, endDate: endDate) <= 1 {
            cell.baseView.removeActivityIndicator()
        }
        self.subscribtions.append(self.interactor.execute(startDate: startDate, endDate: endDate).observeOn(MainScheduler.instance).subscribe(onNext: { [unowned self] caloricProgress in
            self.caloricProgressViewModel = self.mapperObjectCaloricProgress.transform(model: caloricProgress)
        }, onError: {error in
            cell.baseView.parseError(error: error, completion: nil)
        }, onCompleted: {
            self.setupModelFor(cell: cell)
            self.reloadTable(cell: cell)
            cell.baseView.stopActivityAnimation()
        }, onDisposed: {
        }))
    }
    
    private func daysBetweenDates(startDate: Date, endDate: Date) -> Int
    {
        let calendar = Calendar.current
        let components = calendar.dateComponents([.day], from: startDate, to: endDate)
        return components.day! + 1
    }
    
    private func calculateMaxCal() {
        if let state = caloricProgressViewModel.caloricState {
            var maxBurned = 0.0
            var maxConsumed = 0.0
            
            let maxBurnedCalories = state.max(by: { (a, b) -> Bool in
                return (a.cal.userBurnedCal + a.cal.bmrCal) < (b.cal.userBurnedCal + b.cal.bmrCal)})
            if let state = maxBurnedCalories {
                maxBurned = state.cal.bmrCal + state.cal.userBurnedCal
            }
            
            let maxConsumedCalories = state.max(by: { (a, b) -> Bool in
                return (a.cal.userConsumedCal + a.cal.maxConsumedCal) < (b.cal.userConsumedCal + b.cal.maxConsumedCal)})
            
            if let state = maxConsumedCalories {
                if state.cal.userConsumedCal >= state.cal.maxConsumedCal {
                    maxConsumed = state.cal.userConsumedCal
                } else {
                    maxConsumed = state.cal.maxConsumedCal
                }
            }
            
            self.maxCal = [maxConsumed, maxBurned].max(by: {(a, b) -> Bool in
                return a < b
            })!
        }
    }
    
    private func calculateAvgConsumed() {
        if let state = caloricProgressViewModel.caloricState {
            let sum = state.reduce(0.0, {$0 + $1.cal.maxConsumedCal})
            self.avgConsumed = sum / Double(state.count) + Double.leastNonzeroMagnitude
        }
    }
    
    private func setupModelFor(cell: ActivityCaloricProgressChartCell) {
        self.calculateMaxCal()
        self.calculateAvgConsumed()
        cell.chartView.reloadWith(DataSource: self)
    }
    
    override func fetchModel(For basecell: ActivityBaseCell) {
        super.fetchModel(For: basecell)
        
        let cell = basecell as! ActivityCaloricProgressChartCell
        cell.title.attributedText = NSMutableAttributedString(string: "Caloric Progress", attributes: cell.boldAttr)
        
        if let delegate = self.delegate {
            self.epoch = delegate.fetchEpoch()
            if let epoch = self.epoch {
                self.getInformation(cell: cell, startDate: epoch.startDate, endDate: epoch.endDate)
            }
        }
        cell.chartView.reloadWith(DataSource: self)
    }
    
}

//
//  CaloricBurnedTableViewCell.swift
//  Nourish
//
//  Created by Vlad Birukov on 31.08.17.
//  Copyright © 2017 SSIC. All rights reserved.
//

import UIKit

class CaloricBurnedTableViewCell: ActivityBaseCell {
    @IBOutlet weak var iconImageView: UIImageView!
    @IBOutlet weak var progressBarChart: ProgressBarChart!
    @IBOutlet weak var valueLabel: UILabel!

    @IBOutlet weak var baseView: BaseView!
    
    @IBOutlet weak var imageBackgroundView : UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        iconImageView.imageNamedWithTint(named: "calburned_icon", tintColor: UIColor.white)
        self.imageBackgroundView.backgroundColor = NRColorUtility.appBackgroundColor()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

    }
    
}

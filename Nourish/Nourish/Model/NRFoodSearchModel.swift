//
//  NRFoodSearchModel.swift
//  Nourish
//
//  Created by Vino (Vinodha) Sundaramoorthy on 11/15/16.
//  Copyright © 2016 SSIC. All rights reserved.
//

import UIKit

struct foodSearchModel {
    var foodTitle : String = ""
    var foodId : String = ""
    var measure : String = ""
    var calories : String = ""
    var amount : Double = 1
    var unit : String = ""
    var foodBackendID : String? = ""
    var originalSearchString : String = ""
    var grams : Double? = 0.0
    var caloriePerPortion : Double = 0.0
    var isFromNoom = false
    var displayUnit : String = ""
    var caloriesPerGram : Double = 0.0
    
    //rec
    var mealIdString : String? = ""
    var occasion : Ocasion?
    var isFavorite : Bool? = false
    var isGroupFavorite : Bool? = false
    var groupID : String?
    var userPhotoId : String?
    
    //countedFav
    var counted : Int = 0
    
    var localCacheID : String?
    
    //foodSource
    var sourceFoodDB : String?
    var sourceFoodDBID : String?
    
    
    //optifast
    var optifastHeader : String = ""
    var optifastHightlights = [String]()
    var optifastFooter : String = ""
    var optifastImage : UIImage? = nil
    var optifastIngredients = [String]()
    
}

struct groudFoodDetailsModel {
    var groupTitle : String = ""
    var groupId : String = ""
    var groupBackendID : String = ""
    var isFromNoom = false
    var isFavorite = false
    var groupMealArray : [MealRecordModel] = []
    var photoID : String?
}

//The order in which the food details are displayed
enum foodDetailConfiguration : String {
    case foodImage = "Image"
    case servingType = "Serving Type"
    case servingAmount = "Measure"
    case recordMeal = "Record Meal"
    case nutrients = "nutrient"
    
    static let allValues = [foodImage,servingType,servingAmount,recordMeal,nutrients]
}

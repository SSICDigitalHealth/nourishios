//
//  ProgressModel.swift
//  Nourish
//
//  Created by Vino (Vinodha) Sundaramoorthy on 12/6/16.
//  Copyright © 2016 SSIC. All rights reserved.
//

import UIKit

enum progressCells {
    case Health
    case Weight
    case Nutrition
    case Activity
    case Stress
    
    static let allValues = [Health,Weight,Nutrition,Activity,Stress]
    static let allHeights = [Health :448.0,Weight :374,Nutrition :399.0,Activity :359.0,Stress :374.0]
}

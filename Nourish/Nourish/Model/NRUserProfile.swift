//
//  NRUserProfile.swift
//  Nourish
//
//  Created by Vino (Vinodha) Sundaramoorthy on 11/5/16.
//  Copyright © 2016 SSIC. All rights reserved.
//

import UIKit

enum dietaryPreference {
    case none
    case vegetarian
}

enum activityLevel {
    case sedentary
    case moderate
    case active
    case veryActive
}

enum metricPreference {
    case imperial
    case Metric
}

class NRUserProfile: NSObject {
    let userID : String = ""
    let userFirstName : String = ""
    let userLastName : String = ""
    let activeSinceTimeStamp : Date? = nil
    let height : Float = 0
    let weight : Float = 0
    let biologicalSex : String = ""
    let activityLevel : activityLevel? = nil
    let dietaryPreference : dietaryPreference? = nil
}

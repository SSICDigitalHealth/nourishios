//
//  NRRecommendationsModel.swift
//  Nourish
//
//  Created by Vino (Vinodha) Sundaramoorthy on 11/14/16.
//  Copyright © 2016 SSIC. All rights reserved.
//

import UIKit

typealias recommendationList = (type : recommendationType, recommendation:[recommendation])

struct recommendation {
    var description : String
    var title : String
    var subTitle : String
    var type : recommendationType
    var foodModel : foodSearchModel? = nil
}


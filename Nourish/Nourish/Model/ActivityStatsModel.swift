//
//  ActivityStatsModel.swift
//  Nourish
//
//  Created by Vino (Vinodha) Sundaramoorthy on 12/23/16.
//  Copyright © 2016 SSIC. All rights reserved.
//

import UIKit

enum activityCellConfig {
    case activityCircle
    case caloriesBurnedChart
    case stepsStats
    case milesStats
    case sleepStats
    case activeTimeStats
}

struct activityTime {
    var activeHr : Double = 0.0
    var periodString : String = ""
}

class activityModel : NSObject {
    var steps : Double = 0.0
    var distance : Double = 0.0
    var activeHours : Double = 0.0
    var sleepHours : Double = 0.0
    var activityList : [(activityName : String , value: Double,type:String)] = []
    var caloriesArray : [caloriesModel] = []
    var activityArray : [activityTime] = []
    var avgCalorie : Double = 0.0
    var avgSteps : Double = 0.0
}

struct caloriesBurnedChartModel {
    var calories : CGFloat = 0.0
    var steps : CGFloat = 0.0
}

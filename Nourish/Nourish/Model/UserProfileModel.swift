//
//  NRUserProfile.swift
//  Nourish
//
//  Created by Vino (Vinodha) Sundaramoorthy on 11/5/16.
//  Copyright © 2016 SSIC. All rights reserved.
//

import UIKit

enum profileCells : String {
    case nameCell = "PROFILE"
    case genderCell = "Gender"
    case ageCell = "Age"
    case metricsCell = "Units of Measurement"
    case heightCell = "Height"
    case weightCell = "Weight"
    case dietCell = "Dietary Preferences"
    case activityCell = "Activity Level"
}

enum userPropertyField {
    case height
    case weight
    case age
    case activityLevel
    case dietaryPreference
    case metricPreference
    case biologicalSex
    case goalRounder
    case userGoal
}

struct units {
    var name : String
    var description : String
    var isSelected : Bool
}

struct profileModel {
    var title : String
    var value : String
    var placeHolder : String
}

enum DietaryPreference : CustomStringConvertible {
    case Common
    case Vegetarian
    
    var description : String {
        switch self {
        case .Common: return "none";
        case .Vegetarian: return "vegetarian";
        }
    }
    
    var profileDescription : String {
        switch self {
        case .Common: return "No Restriction";
        case .Vegetarian: return "Vegetarian";
        }
    }
    
    var descriptionUI : String {
        switch self {
        case .Common: return "US Healthy";
        case .Vegetarian: return "Vegetarian";
        }
    }
    
    var stringForMealPlan : String {
        switch self {
        case .Vegetarian: return "HEP_vegetarian";
        case .Common : return "HEP_US_style";
        }
        
    }
    
    static func enumFromString(string:String) -> DietaryPreference? {
        switch string {
        case "none": return .Common
        case "vegetarian": return .Vegetarian
        case "None" : return .Common
        case "Vegetarian" : return .Vegetarian
        case "No Restriction" : return .Common
        case "us healthy" : return .Common

        default : return nil
        }
    }
}

enum ActivityLevel : CustomStringConvertible {
    case Sedentary
    case Moderate
    case Active
    case VeryActive
    
    var description : String {
        switch self {
        case .Sedentary: return "sedentary"
        case .Moderate: return "moderately active"
        case .Active: return "active"
        case .VeryActive: return "very active"
        }
    }
    
    var dataSourceMappingString : String {
        switch self {
        case .Sedentary: return "sedentary"
        case .Moderate: return "moderate"
        case .Active: return "active"
        case .VeryActive: return "very active"
        }
    }
    
    static func stringToDataSource(string:String) -> ActivityLevel? {
        switch string {
        case "sedentary": return .Sedentary
        case "moderate":  return .Moderate
        case "active" : return .Active
        case "very active": return .VeryActive
        default : return nil
        }
    }
    
    static func enumFromString(string:String) -> ActivityLevel? {
        switch string {
        case "sedentary": return .Sedentary
        case "moderately active": return .Moderate
        case "active" : return .Active
        case "very active": return .VeryActive
        
        case "Sedentary": return .Sedentary
        case "Moderate": return .Moderate
        case "Active": return .Active
        case "Very Active": return .VeryActive
        default : return nil
        }
    }
}

enum MetricPreference : CustomStringConvertible{
    case Metric
    case Imperial
    
        var description: String {
            switch self {
            case .Metric: return "Metrics"
            case .Imperial: return "Imperial"
            }
        }
    
    
        var isMetric : Bool {
            switch self {
            case .Metric:
                return true
            case .Imperial:
                return false
        }
    }
    

    var dataSourceMappingString : String {
        switch self {
        case .Metric: return "metric"
        case .Imperial: return "imperial"
        }
    }
    
    var descriptionForChat : String {
        switch self {
        case .Metric: return "kg"
        case .Imperial: return "lbs"
        }
    }
    
    static func stringToDataSource(string:String) -> MetricPreference? {
        switch string {
        case "metric": return .Metric
        case "imperial":  return .Imperial
        default : return nil
        }
    }
    
     static func enumFromBool(isMetric : Bool) -> MetricPreference? {
        switch isMetric {
        case true:
            return .Metric
        case false:
            return .Imperial
        }
    }
    
    static func enumFromString(string:String) -> MetricPreference? {
        switch string {
        case "Metrics": return .Metric
        case "Imperial": return .Imperial
        case "Centimeter": return .Metric
        case "Inches" : return .Imperial
        case "Metric" : return .Metric
        default : return nil
        }
    }
    
}

enum BiologicalSex : CustomStringConvertible {
    case Male
    case Female
    
    var description : String {
        switch self {
            case .Male: return "male";
            case .Female: return "female";
        }
    }
    var descriptionForChat : String {
        switch self {
        case .Male: return "Male";
        case .Female: return "Female";
        }
    }
    
    
    static func enumFromString(string:String) -> BiologicalSex? {
        switch string {
            case "male": return .Male
            case "female": return .Female
            case "Male": return .Male
            case "Female": return .Female
            default : return nil
        }
    }
}

enum UserGoal : CustomStringConvertible {
    case EatBetter
    case LooseWeight
    case MaintainWeight
    var description : String {
        switch self {
        case .EatBetter: return "EatRight"
        case .LooseWeight: return "WeightLoss"
        case .MaintainWeight: return "MaintainWeight"
        }
    }
    
    var descriptionUI : String {
        switch self {
        case .EatBetter: return "Eat Right"
        case .LooseWeight: return "Lose Weight"
        case .MaintainWeight: return "Maintain Weight"
        }
    }
    
    static func enumFromString(string:String) -> UserGoal? {
        switch string {
        case "nutrition balance": return .EatBetter
        case "weight loss": return .LooseWeight
        case "Eat Right": return .EatBetter
        case "Lose Weight": return .LooseWeight
        case "EatRight" : return .EatBetter
        case "WeightLoss" : return .LooseWeight
        case "MaintainWeight" : return .MaintainWeight
        case "Maintain Weight" : return .MaintainWeight
        default : return nil
        }
    }
}

class UserProfileModel: NSObject {
    var userID : String? = ""
    var userFirstName : String? = ""
    var userLastName : String? = ""
    var activeSinceTimeStamp : Date? = nil
    var height : Double? = 0
    var weight : Double? = 0
    var age : Int? = 0
    var activityLevel : ActivityLevel? = nil
    var dietaryPreference : DietaryPreference? = nil
    var metricPreference : MetricPreference? = nil
    var biologicalSex : BiologicalSex? = nil
    var goalPrefsActivity : Float? = 0
    var goalPrefsFood : Float? = 0
    var goalPrefsLifestyle : Float? = 0
    var userGoal : UserGoal?
    var startWeight : Double?
    var goalValue : Double?
    var weightLossStartDate : Date?
    var email : String?
    var weightToMaintain : Double? = nil
    var needToInfromUser : Bool = false
    var optifastOcasionsList = [Ocasion]()
    var optifastFoodIds = [String]()
}

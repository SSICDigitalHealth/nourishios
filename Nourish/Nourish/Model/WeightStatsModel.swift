//
//  WeightStatsModel.swift
//  Nourish
//
//  Created by Vino (Vinodha) Sundaramoorthy on 1/9/17.
//  Copyright © 2017 SSIC. All rights reserved.
//

import UIKit

struct weightStatsModel {
    var weight : Double = 0.0
    var weightPeriodRange : String = ""
}

struct caloriesModel {
    var calorie : Double = 0.0
    var caloriePeriodRange : String = ""
}

class weightModel {
    var averageWeight : Double = 0.0
    var averageCalorie : Double = 0.0
    var weightsArray : [weightStatsModel] = []
    var caloriesArray : [caloriesModel] = []
    var weeklyPlanArray : [Double] = []
}


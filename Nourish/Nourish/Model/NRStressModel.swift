//
//  NRStressModel.swift
//  Nourish
//
//  Created by Vino (Vinodha) Sundaramoorthy on 12/5/16.
//  Copyright © 2016 SSIC. All rights reserved.
//

import UIKit

enum stressLevel : Int {
    case Normal
    case Stressed
    case Undetermined
    
    static let allValues = [Normal, Stressed, Undetermined]
    
    static func stringFromEnum(level : stressLevel) -> String {
        switch level {
            case .Normal:
                return "Normal"
            case .Stressed:
                return "Stress"
            case .Undetermined:
                return "Undetermined"
        }
    }
    
    static func enumFromString(stringValue : String) -> stressLevel {
        switch stringValue {
        case "Normal":
            return .Normal
        case "Stress":
            return .Stressed
        default :
            return .Undetermined
        }
    }
    
    
    
    static func graphColorForStress(stress : stressLevel) -> UIColor {
        switch stress {
        case .Normal:
            return NRColorUtility.normalStressGraphColor ()
        case .Stressed:
            return NRColorUtility.healthRedColor ()
        case .Undetermined:
            return NRColorUtility.undeterminedStateColor ()
        }
    }
    
    static func backgroundTableViewCellColorForStress(stress : stressLevel) -> UIColor {
        switch stress {
        case .Normal:
            return UIColor.white
        case .Stressed:
            return NRColorUtility.stressedTableViewStressCellBackgroundColor()
        case.Undetermined:
            return UIColor.white
        }
    }
    
    
    static func textColorForStress(stress : stressLevel) -> UIColor {
        switch stress {
        case .Normal:
            return NRColorUtility.normalStressedTableViewStressCellFontColor()
        case .Stressed:
            return NRColorUtility.stressedTableViewStressCellFontColor()
        case.Undetermined:
            return NRColorUtility.undeterminedTableViewStressCellFontColor()
        }
    }
    
    static func colorForStress(stress : stressLevel) -> UIColor {
        switch stress {
        case .Normal:
            return NRColorUtility.StressGreenColor()
        case .Stressed:
            return NRColorUtility.healthRedColor()
        case .Undetermined:
            return NRColorUtility.undeterminedStateColor()
        }
    }

}

class NRStressModel: NSObject {
    var stressScore : Double = 0
    var stressMetricsLabel : String = ""
    var hourlyStressArray : [stressLevel]?
    
}

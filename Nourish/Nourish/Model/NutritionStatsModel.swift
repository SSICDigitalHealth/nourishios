//
//  NutritionStatsModel.swift
//  Nourish
//
//  Created by Vino (Vinodha) Sundaramoorthy on 12/13/16.
//  Copyright © 2016 SSIC. All rights reserved.
//

import UIKit

let kQuantityNormal = "Just Right"
let kQuantityHigh = "Too Much"
let kQuantityLow = "Too Little"

enum period : Int {
    case today = 0
    case weekly = 1
    case monthly = 2
    
    static func stringFromEnum(period : period) -> String {
        switch period {
            case .today:
                return "Today"
            case .weekly:
                return "Weekly"
            case .monthly:
                return "Monthly"
        }
    }
    
    static func startDateFor(period : period) -> Date {
        let calendar = Calendar.current
        let start = calendar.startOfDay(for: Date())
        
        var comps = DateComponents()
        comps.day = 1
        let endOfDay = calendar.date(byAdding: comps, to: start)!
        let startOfWeek = Calendar.current.date(from: Calendar.current.dateComponents([.yearForWeekOfYear, .weekOfYear], from: Date()))!
        
        switch period {
        case .today:
            return start
        case .weekly:
            return calendar.date(byAdding: Calendar.Component.weekdayOrdinal, value: -1, to: endOfDay)!
        case .monthly:
            return calendar.date(byAdding: Calendar.Component.weekdayOrdinal, value: -3, to: startOfWeek)!
        }
    }
    
    static func endDateFrom(period : period, startDate : Date) -> Date {
        let calendar = Calendar.current
        switch period {
        case .today:
            var comps = DateComponents()
            comps.day = 1
            comps.second = -1
            let endDate = calendar.date(byAdding: comps, to: startDate)
            return endDate!
        case .weekly:
            let endDate = calendar.date(byAdding: Calendar.Component.day, value: 1, to: startDate)
            return endDate!
        case .monthly:
            let endDate = calendar.date(byAdding: Calendar.Component.weekdayOrdinal, value: 1, to: startDate)
            return endDate!
        }
    }
}

class nutrients : NSObject {
    var name : String = ""
    var min : Double = 0.0
    //var max : Double = 0.0
    var value : Double = 0.0
    var descriptionString : String = ""
    var deficiency : String = ""
    var excess : String = ""
    var score : Double = 0.0
    var sources : String = ""
    var unit : String = ""
    var flag : String = ""
    
    var uhr : Double?
    var lhr : Double?
    
    var max : Double {
        get {
            if self.lhr != nil && self.uhr != nil {
                if self.value < self.lhr! {
                    return self.lhr!
                } else {
                    return self.uhr!
                }
            } else if self.lhr == nil {
                return self.uhr!
            } else {
                if self.value < self.lhr! {
                    return self.lhr!
                } else {
                    return self.value
                }
            }
        }
    }
    var quantityString : String {
        get {
            if self.lhr != nil && self.uhr != nil {
                let maxValue = self.uhr
                let minValue = self.lhr
                
                if self.value < minValue! {
                    return kQuantityLow
                } else if self.value > maxValue! {
                    return kQuantityHigh
                } else {
                    return kQuantityNormal
                }
                
            } else if self.lhr == nil {
                let maxValue = self.uhr
                
                if self.value == 0 {
                    return kQuantityLow
                } else if value <= maxValue! {
                    return kQuantityNormal
                } else {
                    return kQuantityHigh
                }
            } else { //only minimal
                let maxValue = self.lhr
                
                if self.value < maxValue! {
                    return kQuantityLow
                } else {
                    return kQuantityNormal
                }
            }
        }
    }
    
    var textDescription : String {
        get {
            if self.quantityString == kQuantityNormal {
                return self.descriptionString
            } else if self.quantityString == kQuantityLow {
                return self.deficiency
            } else {
                return self.excess
            }
        }
    }
    
    func tokenColor() -> UIColor {
        if quantityString == kQuantityLow {
            return NRColorUtility.healthOrangeColor()
        } else if quantityString == kQuantityHigh {
            return NRColorUtility.healthRedColor()
        } else {
            return NRColorUtility.healthGreenColor()
        }
    }
    
    func tokenTextColor() -> UIColor {
        if tokenColor() == NRColorUtility.healthGreenColor() {
            return NRColorUtility.progressLabelColor()
        }
        return UIColor.white
    }
    
    func progressColor() -> UIColor {
        if quantityString == kQuantityNormal {
            return NRColorUtility.healthGreenColor()
        } else {
            return NRColorUtility.healthRedColor()
        }
    }
}

struct caloriesConsumedModel {
    var calorie : Double = 0.0
    var caloriePeriodRange : String = ""
}

struct nutritionalScoreModel {
    var score : Double = 0.0
    var scorePeriod : String = ""
}

class nutritionStatsModel : NSObject {
    var caloriesArray : [caloriesConsumedModel] = []
    var nutritionArray : [nutritionalScoreModel] = []
    var avgCalorie : Double = 0.0
    var avgScore : Double = 0.0
    var maxCalories : Double = 0.0
    var nutrArray : [nutrients] = []
    
    var vitamins : Double = 0.0
    var electrolytes : Double = 0.0
    var minerals : Double = 0.0
    var macronutrients : Double = 0.0
    var nutritionScore : Double = 0.0
    
    var recomCalories : Double?
    
}

//
//  ProgressCacheRealmToObject.swift
//  Nourish
//
//  Created by Vlad Birukov on 26.06.17.
//  Copyright © 2017 SSIC. All rights reserved.
//

import Foundation

class ProgressCacheRealmToObject: NSObject {
    func transform(progressCache: NestleProgressCache) -> Progress {
        let progress = Progress()
        let userScore = UserActivityScoreModel()
        let userCallories = CaloriesModel()
        var userOcasion = [Element]()
        var userMicroelement = [MicroelementInformation]()
        
        userScore.scoreActivity = progressCache.userScore
        let array = progressCache.scoreHistoryList.sorted(by: {$0.startDate! < $1.startDate!})

        for object in array {
            userScore.historyScore.append((subtitle: object.startDate!,value: object.scoreValue))
        }
        
        if progressCache.userCalories != nil {
            userCallories.consumedCalories = (progressCache.userCalories?.consumed)!
            userCallories.targetCalories = (progressCache.userCalories?.target)!
            userCallories.unit = (progressCache.userCalories?.unit)!
    
        }
        
        if progressCache.caloriesForDay.count > 0 {
            for i in 0..<progressCache.caloriesForDay.count {
                let element = Element()
                element.consumCalories = progressCache.caloriesForDay[i].consumedCalories
                element.nameFood = progressCache.caloriesForDay[i].nameFood
                element.nameFoodIntake = progressCache.caloriesForDay[i].nameFoodIntake
                element.amountFood = progressCache.caloriesForDay[i].amountFood

                if progressCache.caloriesForDay[i].unit != nil {
                    element.unit = progressCache.caloriesForDay[i].unit

                }
                userOcasion.append(element)
            }
            
        }
        if progressCache.calsHistory.count > 0 {
            var arrayOfCalsHistory = [CalorieHistoryModel]()
            for cached in progressCache.calsHistory {
                let model = CalorieHistoryModel()
                model.maxConsumed = cached.maxConsumed
                model.consumed = cached.consumed
                model.date = cached.date
                arrayOfCalsHistory.append(model)
            }
            progress.calorieHistory = arrayOfCalsHistory
        }
        
    
        
        if progressCache.microelementList.count > 0 {
            for i in 0..<progressCache.microelementList.count {
                let microelement = MicroelementInformation()
                
                if progressCache.numDays == 0.0 {
                    microelement.numDays = 1.0
                } else {
                    microelement.numDays = progressCache.numDays
                }
                
                microelement.idNameMicroelement = progressCache.microelementList[i].idNameMicroelement
                microelement.nutritionIndicator = NutritionIntdicator(rawValue : progressCache.microelementList[i].nutritionIndicator)!
                
                if progressCache.microelementList[i].consumedMicroelement.value != nil {
                    microelement.consumedMicroelement = progressCache.microelementList[i].consumedMicroelement.value!
                }
                
                if progressCache.microelementList[i].lowerLimit.value != nil {
                    microelement.lowerLimit = progressCache.microelementList[i].lowerLimit.value
                }
                if progressCache.microelementList[i].upperLimit.value != nil {
                    microelement.upperLimit = progressCache.microelementList[i].upperLimit.value
                } 
                microelement.unit = progressCache.microelementList[i].unit
                microelement.message = progressCache.microelementList[i].message
                
                if progressCache.microelementList[i].contributors.count > 0 {
                    for j in 0..<progressCache.microelementList[i].contributors.count {
                        let foodMicroelementInformation = FoodMicroelemenInformation()
                        foodMicroelementInformation.nameFood = progressCache.microelementList[i].contributors[j].nameFood
                        foodMicroelementInformation.consumed = progressCache.microelementList[i].contributors[j].consumed
                        foodMicroelementInformation.quality = progressCache.microelementList[i].contributors[j].quality
                        foodMicroelementInformation.unit = progressCache.microelementList[i].contributors[j].unit
                        microelement.contributors.append(foodMicroelementInformation)
                    }
                }
                
                userMicroelement.append(microelement)
            }
        }
        
        var historyArray = [MicroelementInformation]()
        for historyObject in progressCache.componentsHistory {
            let historyMicroelement = MicroelementInformation()
            let obj = userMicroelement.first(where: {$0.idNameMicroelement == historyObject.microelementId})!
            
            historyMicroelement.historyDate = historyObject.date!
            historyMicroelement.idNameMicroelement = historyObject.microelementId!
            historyMicroelement.contributors = obj.contributors
            historyMicroelement.numDays = obj.numDays
            historyMicroelement.lowerLimit = obj.lowerLimit
            historyMicroelement.upperLimit = obj.upperLimit
            historyMicroelement.message = obj.message
            historyMicroelement.unit = obj.unit
            historyMicroelement.nutritionIndicator = obj.nutritionIndicator
            historyMicroelement.consumedMicroelement = historyObject.consumedAmount
            historyArray.append(historyMicroelement)
            
        }
        
        progress.componentsHistory = historyArray

        progress.userScore = userScore
        progress.userCallories = userCallories
        progress.userOcasion = userOcasion
        progress.userMicroelement = userMicroelement
        
        var filters = [String]()
        for realmId in progressCache.nutrientsListToShow {
            filters.append(realmId.stringValue)
        }
        
        var result : [MicroelementInformation]? = []
        
        if progress.componentsHistory.count > 0 {
            result = progress.componentsHistory?.filter({filters.contains($0.idNameMicroelement)})
        } else {
            result = progress.userMicroelement.filter({filters.contains($0.idNameMicroelement)})
        }
        
        
        var filtered = [[MicroelementInformation]]()
        for object in result! {
            if filtered.count != filters.count {
                filtered.append(self.arrayFromId(foodId: object.idNameMicroelement, elements: result!))
            }
        }
        progress.nutrientsLisToShow = filtered
        
        filters = ["Added_sugars", "Fat_saturated","Sodium"]
        result = progress.componentsHistory?.filter({filters.contains($0.idNameMicroelement)})
        
        filtered.removeAll()
        for object in result! {
            if filtered.count != filters.count {
                filtered.append(self.arrayFromId(foodId: object.idNameMicroelement, elements: result!))
            }
        }

        progress.watchoutForToShow = filtered
        
        progress.wholeGrainsHistory = progress.componentsHistory.filter({$0.idNameMicroelement == "Grain_whole"})
        progress.wholeGrains = progress.userMicroelement.filter({$0.idNameMicroelement == "Grain_whole"})
        progress.refinedGrainsHistory = progress.userMicroelement.filter({$0.idNameMicroelement == "Grain_refined"})
        
        return progress
    }
    private func arrayFromId(foodId : String, elements : [MicroelementInformation] ) -> [MicroelementInformation] {
        let arrayToReturn = elements.filter({$0.idNameMicroelement == foodId})
        return arrayToReturn
    }

    
    private func daysBetweenDates(startDate: Date, endDate: Date) -> Int
    {
        let calendar = Calendar.current
        let components = calendar.dateComponents([.day], from: startDate, to: endDate)
        return components.day! + 1
    }
    
}

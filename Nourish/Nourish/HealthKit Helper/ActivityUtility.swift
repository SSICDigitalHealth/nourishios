//
//  ActivityUtility.swift
//  Nourish
//
//  Created by Vino (Vinodha) Sundaramoorthy on 10/31/16.
//  Copyright © 2016 SSIC. All rights reserved.
//

import UIKit
import HealthKit

class ActivityUtility {
    static let healthStore: HKHealthStore? = HKHealthStore()
    static let healthStoreStress : HKHealthStore? = HKHealthStore()
    static let healthStoreSteps : HKHealthStore? = HKHealthStore()
    
    class func getHealthKitPermission(completion:((Bool, Error?) -> Void)!) {
        guard HKHealthStore.isHealthDataAvailable() else {
            
            return
        }
        
        let readDataTypes: Set<HKObjectType> = self.dataTypesToRead()
        let writeDataTypes = self.dataTypesToWrite()
        
        ActivityUtility.healthStore?.requestAuthorization(toShare: writeDataTypes, read: readDataTypes, completion: completion)
        
    }
    
    class func dataTypesToWrite() -> Set<HKSampleType> {
        let weightType = HKSampleType.quantityType(forIdentifier: HKQuantityTypeIdentifier.bodyMass)!
        let heightType = HKSampleType.quantityType(forIdentifier: HKQuantityTypeIdentifier.height)!
        let stepCount = HKSampleType.quantityType(forIdentifier: HKQuantityTypeIdentifier.stepCount)!
        let sleepAnalysis = HKQuantityType.categoryType(forIdentifier: HKCategoryTypeIdentifier.sleepAnalysis)!
        let heartRateType = HKQuantityType.quantityType(forIdentifier: HKQuantityTypeIdentifier.heartRate)!
        let basalEnregyType = HKQuantityType.quantityType(forIdentifier: HKQuantityTypeIdentifier.basalEnergyBurned)!
        let activeEnergyBurnType = HKQuantityType.quantityType(forIdentifier: HKQuantityTypeIdentifier.activeEnergyBurned)!
        let distanceType = HKQuantityType.quantityType(forIdentifier: HKQuantityTypeIdentifier.distanceWalkingRunning)!

        return [weightType, heightType,stepCount,sleepAnalysis,heartRateType,basalEnregyType,activeEnergyBurnType,distanceType]
    }
    
    class func forceRefetchData() {
        DispatchQueue.main.async {
                let dataTypesToRead = self.dataTypesToRead()
                let predicate : NSPredicate = self.mostRecentPredicate()
                for sampleType in dataTypesToRead {
                if sampleType.isKind(of: HKSampleType.self) {
                    let query =  HKSampleQuery(sampleType: sampleType as! HKSampleType, predicate: predicate, limit: 1, sortDescriptors: nil) { query, results, error in
                    }
                    self.healthStore?.execute(query)
                }
            }
        }
    }
    
    class func dataTypesForDumper() -> [HKObjectType] {
        var array = [HKObjectType]()
        
        let activeEnergyBurnType = HKQuantityType.quantityType(forIdentifier: HKQuantityTypeIdentifier.activeEnergyBurned)!
        let heightType = HKQuantityType.quantityType(forIdentifier: HKQuantityTypeIdentifier.height)!
        let weightType = HKQuantityType.quantityType(forIdentifier: HKQuantityTypeIdentifier.bodyMass)!
        let heartRateType = HKQuantityType.quantityType(forIdentifier: HKQuantityTypeIdentifier.heartRate)!
        let stepCountType = HKQuantityType.quantityType(forIdentifier: HKQuantityTypeIdentifier.stepCount)!
        let sleepAnalysis = HKQuantityType.categoryType(forIdentifier: HKCategoryTypeIdentifier.sleepAnalysis)!
        let distanceType = HKQuantityType.quantityType(forIdentifier: HKQuantityTypeIdentifier.distanceWalkingRunning)!
        let workoutTypes = HKObjectType.workoutType()
        
        array = [activeEnergyBurnType, heightType, weightType, heartRateType, stepCountType, sleepAnalysis, distanceType, workoutTypes]
        return array
    }
    
    class func dataTypesToRead() -> Set<HKObjectType> {
        
        let dietaryCalorieEnergyType = HKQuantityType.quantityType(forIdentifier: HKQuantityTypeIdentifier.dietaryEnergyConsumed)!
        let activeEnergyBurnType = HKQuantityType.quantityType(forIdentifier: HKQuantityTypeIdentifier.activeEnergyBurned)!
        let heightType = HKQuantityType.quantityType(forIdentifier: HKQuantityTypeIdentifier.height)!
        let weightType = HKQuantityType.quantityType(forIdentifier: HKQuantityTypeIdentifier.bodyMass)!
        let birthdayType = HKQuantityType.characteristicType(forIdentifier: HKCharacteristicTypeIdentifier.dateOfBirth)!
        let biologicalSexType = HKQuantityType.characteristicType(forIdentifier: HKCharacteristicTypeIdentifier.biologicalSex)!
        let heartRateType = HKQuantityType.quantityType(forIdentifier: HKQuantityTypeIdentifier.heartRate)!
        let stepCountType = HKQuantityType.quantityType(forIdentifier: HKQuantityTypeIdentifier.stepCount)!
        let basalEnregyType = HKQuantityType.quantityType(forIdentifier: HKQuantityTypeIdentifier.basalEnergyBurned)!
        let sleepAnalysis = HKQuantityType.categoryType(forIdentifier: HKCategoryTypeIdentifier.sleepAnalysis)!
        let workoutTypes = HKObjectType.workoutType()
        let distanceType = HKQuantityType.quantityType(forIdentifier: HKQuantityTypeIdentifier.distanceWalkingRunning)!
        
        let readDataTypes: Set<HKObjectType>
        
        if #available(iOS 9.3, *) {
            let activeTimeType = HKQuantityType.quantityType(forIdentifier: HKQuantityTypeIdentifier.appleExerciseTime)!
            readDataTypes = [dietaryCalorieEnergyType, activeEnergyBurnType, heightType, weightType, birthdayType, biologicalSexType,heartRateType,stepCountType,activeTimeType,basalEnregyType, sleepAnalysis, workoutTypes, distanceType]
            
        } else {
            // Fallback on earlier versions
            readDataTypes = [dietaryCalorieEnergyType, activeEnergyBurnType, heightType, weightType, birthdayType, biologicalSexType,heartRateType,stepCountType,basalEnregyType, sleepAnalysis, workoutTypes,distanceType]
        }
        
        return readDataTypes
    }
    
    class func predicateForSamplesToday() -> NSPredicate
    {
        let (starDate, endDate): (Date, Date) = self.datesFromToday()
        
        let predicate: NSPredicate = HKQuery.predicateForSamples(withStart: starDate, end: endDate, options: [])
        
        return predicate
    }
    
    class func predicateSince18() -> NSPredicate {
        let now = Date()
        let calendar = Calendar.current
        
        var components = calendar.dateComponents(
            [Calendar.Component.year,
            Calendar.Component.month,
            Calendar.Component.day,
            Calendar.Component.hour,
            Calendar.Component.minute,
            Calendar.Component.second], from: now)
        
        components.hour = 18
        components.second = 0
        components.minute = 0
        
        var endComps = DateComponents()
        endComps.day = 1
        endComps.second = -1
        
        let endDate = calendar.date(byAdding: endComps, to: calendar.startOfDay(for: now))
        
        let startDate = calendar.date(byAdding: Calendar.Component.day, value: -1, to: endDate!)
        
        return HKQuery.predicateForSamples(withStart: startDate, end: endDate, options: HKQueryOptions.strictEndDate)
        
    }
    
    class func predicateLast10Minutes() -> NSPredicate {
        let now = Date()
        let newDate = Calendar.current.date(byAdding: .minute, value: 10, to: now)
        let predicate : NSPredicate = HKQuery.predicateForSamples(withStart: newDate, end: now, options: HKQueryOptions.strictStartDate)
        return predicate
    }
    
    class func predicateLastTwoDays() -> NSPredicate {
        let nowDate = Date()
        let calendar = Calendar.current
        let endDate: Date = calendar.date(byAdding: Calendar.Component.hour, value: -24, to: nowDate)!
        let predicate = HKQuery.predicateForSamples(withStart: endDate, end: nowDate, options: [])
        return predicate
    }
    
    class func mostRecentPredicate() -> NSPredicate {
        let past = Date.distantPast
        let now   = Date()
        let mostRecentPredicate = HKQuery.predicateForSamples(withStart: past, end:now, options: [])
        return mostRecentPredicate
    }
    
    class func predicateFrom(startDate : Date, endDate : Date) -> NSPredicate {
        return HKQuery.predicateForSamples(withStart: startDate, end: endDate, options: HKQueryOptions.strictEndDate)
    }
    
    
   class func datesFromToday() -> (Date, Date)
    {
        let calendar = Calendar.current
        
        let nowDate = Date()
        
        let starDate: Date = calendar.startOfDay(for: nowDate)
        var dateComps = DateComponents()
        dateComps.day = 1
        dateComps.second = -1
        
        let endDate: Date = calendar.date(byAdding: dateComps, to: starDate)!
        return (starDate, endDate)
    }
    
   class func recentSteps2(completion: @escaping (Double, NSError?) -> () )
    { // this function gives you all of the steps the user has taken since the beginning of the current day.
        
        let type = HKSampleType.quantityType(forIdentifier: HKQuantityTypeIdentifier.stepCount) // The type of data we are requesting
        let todayPredicate: NSPredicate = self.predicateForSamplesToday()
        
        // The actual HealthKit Query which will fetch all of the steps and add them up for us.
        let query = HKSampleQuery(sampleType: type!, predicate: todayPredicate, limit: 0, sortDescriptors: nil) { query, results, error in
            var steps: Double = 0
            
            if results != nil && (results?.count)! > 0
            {
                for result in results as! [HKQuantitySample]
                {
                    //Duplicate in the sample can occur , so it can be restricted to choose any source
                    //if result.sourceRevision.source.name == "Vinodha's Apple Watch" {
                    steps += result.quantity.doubleValue(for: HKUnit.count())
                    //}
                }
            }
            completion(steps, error as NSError?)
        }
        self.healthStore?.execute(query)
    }
    
    class func recent10MinutesSteps(completion: @escaping (Double, NSError?) -> () )
    {
        let type = HKSampleType.quantityType(forIdentifier: HKQuantityTypeIdentifier.stepCount)
        let minutes10Predicate = self.predicateLast10Minutes()
        
        let query = HKSampleQuery(sampleType: type!, predicate: minutes10Predicate, limit: 0, sortDescriptors: nil) { query, results, error in
            var steps: Double = 0
            
            if results != nil && (results?.count)! > 0
            {
                for result in results as! [HKQuantitySample]
                {
                    steps += result.quantity.doubleValue(for: HKUnit.count())
                }
            }
            completion(steps, error as NSError?)
        }
        self.healthStore?.execute(query)
    }
    
    class func recentActivities(completion: @escaping ([HKSample], Error?) ->()) {
        let typeSteps = HKSampleType.quantityType(forIdentifier: HKQuantityTypeIdentifier.stepCount)
        let sortDescriptor = NSSortDescriptor(key: HKSampleSortIdentifierEndDate, ascending: false)
        let todayPredicate: NSPredicate = self.predicateForSamplesToday()
        let querySteps = HKSampleQuery(sampleType : typeSteps!, predicate : todayPredicate, limit: 0, sortDescriptors: [sortDescriptor])
        { query, results, error in
            if results != nil && (results?.count)! > 0 {
                completion(results!, error)
            } else {
                completion([], nil)
            }
        }
        self.healthStore?.execute(querySteps)
    }
    
    class func aggregatedUserStepsForToday(completion : @escaping (Double) -> ()) {
        let stepsCount = HKQuantityType.quantityType(forIdentifier: HKQuantityTypeIdentifier.stepCount)
        
        //   Get the start of the day
        let date = Date()
        let cal = Calendar.current
        let newDate = cal.startOfDay(for: date as Date)
        
        //  Set the Predicates & Interval
        let predicate = HKQuery.predicateForSamples(withStart: newDate as Date, end: Date(), options: .strictStartDate)
        var interval: DateComponents = DateComponents()
        interval.day = 1
        
        //  Perform the Query
        let query = HKStatisticsCollectionQuery(quantityType: stepsCount!, quantitySamplePredicate: predicate, options: [.cumulativeSum], anchorDate: newDate as Date, intervalComponents:interval as DateComponents)
        
        query.initialResultsHandler = { query, results, error in
            
            if error != nil {
                
                //  Something went Wrong
                return
            }
            
            if let myResults = results{
                myResults.enumerateStatistics(from: newDate, to: Date()) {
                    statistics, stop in
                    
                    if let quantity = statistics.sumQuantity() {
                        
                        let steps = quantity.doubleValue(for: HKUnit.count())
                        

                        completion(steps)
                        
                    } else {
                        completion(0.0)
                    }
                }
            }
            
            
        }
        
        self.healthStore?.execute(query)
    }
    
    class func recentSleepingActivity(completion: @escaping ([HKSample]?, Error?) ->()) {
        let typeSleep = HKSampleType.categoryType(forIdentifier: HKCategoryTypeIdentifier.sleepAnalysis)
        let sortDescriptor = NSSortDescriptor(key: HKSampleSortIdentifierEndDate, ascending: false)
        let todayPredicate: NSPredicate = self.predicateForSamplesToday()
        let querySleeping = HKSampleQuery(sampleType : typeSleep!, predicate : todayPredicate, limit: 0, sortDescriptors: [sortDescriptor])
        { query, results, error in
                completion(results, error)
        }
        self.healthStore?.execute(querySleeping)
    }
    
    class func sleepingActivityLastNight(completion: @escaping ([HKSample]?, Error?) ->()) {
        let typeSleep = HKSampleType.categoryType(forIdentifier: HKCategoryTypeIdentifier.sleepAnalysis)
        let sortDescriptor = NSSortDescriptor(key: HKSampleSortIdentifierEndDate, ascending: false)
        let predicate = self.predicateSince18()
        let querySleeping = HKSampleQuery(sampleType : typeSleep!, predicate : predicate, limit: 0, sortDescriptors: [sortDescriptor])
        { query, results, error in
            completion(results, error)
        }
        self.healthStore?.execute(querySleeping)
    }
    
    class func sleepingActivityFor(startDate : Date, endDate : Date, completion : @escaping ([HKSample]?, Error?) ->()) {
        let typeSleep = HKSampleType.categoryType(forIdentifier: HKCategoryTypeIdentifier.sleepAnalysis)
        let sortDescriptor = NSSortDescriptor(key: HKSampleSortIdentifierEndDate, ascending: false)
        let predicate = self.predicateFrom(startDate: startDate, endDate: endDate)
        
        let querySleeping = HKSampleQuery(sampleType : typeSleep!, predicate : predicate, limit: 0, sortDescriptors: [sortDescriptor])
        { query, results, error in
            completion(results, error)
        }
        self.healthStore?.execute(querySleeping)
    }
    
    class func userSampleForSampleType(sampleType : HKSampleType, completion: @escaping (HKSample?, Error?) ->()) {
        let predicate = self.mostRecentPredicate()
        let sortDescriptor = NSSortDescriptor(key: HKSampleSortIdentifierStartDate, ascending: false)
        let sampleQuery = HKSampleQuery(sampleType: sampleType, predicate: predicate, limit: 1, sortDescriptors: [sortDescriptor])
        { (sampleQuery, results, error ) -> Void in
            
            if error != nil {
                completion(nil, error)
                return;
            }
            
            let mostRecentSample = results?.first as? HKQuantitySample
            completion(mostRecentSample,nil)
        }
        self.healthStore?.execute(sampleQuery)
    }
    
    class func lastUserWeight(completion: @escaping (HKSample?, Error?) ->()) {
        
        let sampleType = HKSampleType.quantityType(forIdentifier: HKQuantityTypeIdentifier.bodyMass)
        
        self.userSampleForSampleType(sampleType: sampleType!, completion: completion)
    }
    
    class func lastUserHeight(completion: @escaping (HKSample?, Error?) ->()) {
        let sampleType = HKSampleType.quantityType(forIdentifier: HKQuantityTypeIdentifier.height)
        
        self.userSampleForSampleType(sampleType: sampleType!, completion: completion)
    }
    
    class func userWeight(startDate : Date, endDate : Date, completion: @escaping ([HKSample], Error?) -> ()) {
        
        let sampleType = HKSampleType.quantityType(forIdentifier: HKQuantityTypeIdentifier.bodyMass)
        let sortDescriptor = NSSortDescriptor(key: HKSampleSortIdentifierEndDate, ascending: true)
        let predicate = self.predicateFrom(startDate: startDate, endDate: endDate)
        
        let query = HKSampleQuery(sampleType : sampleType!, predicate : predicate, limit: 0, sortDescriptors: [sortDescriptor])
        { query, results, error in
            if results != nil && (results?.count)! > 0 {
                completion(results!, error)
            } else {
                completion([], error)
            }
        }
        self.healthStore?.execute(query)
    }
    
    class func userHeight(startDate : Date, endDate : Date, completion: @escaping ([HKSample], Error?) -> ()) {
        let sampleType = HKSampleType.quantityType(forIdentifier: HKQuantityTypeIdentifier.height)
        let sortDescriptor = NSSortDescriptor(key: HKSampleSortIdentifierEndDate, ascending: true)
        let predicate = self.predicateFrom(startDate: startDate, endDate: endDate)
        
        let query = HKSampleQuery(sampleType : sampleType!, predicate : predicate, limit: 0, sortDescriptors: [sortDescriptor])
        { query, results, error in
            if results != nil && (results?.count)! > 0 {
                completion(results!, error)
            } else {
                completion([], error)
            }
        }
        self.healthStore?.execute(query)
    }
    class func heartRate10minutes(completion: @escaping ([HKQuantitySample], Error?) -> () ) {
        let sampleType = HKSampleType.quantityType(forIdentifier: HKQuantityTypeIdentifier.heartRate)
        
        let sortDescriptor = NSSortDescriptor(key: HKSampleSortIdentifierEndDate, ascending: false)
        let todayPredicate: NSPredicate = self.predicateLast10Minutes()
        let heartRate = HKSampleQuery(sampleType : sampleType!, predicate : todayPredicate, limit: 0, sortDescriptors: [sortDescriptor])
        { query, results, error in
            if results != nil && (results?.count)! > 0 {
                completion(results! as! [HKQuantitySample], error)
            } else {
                completion([], error)
            }
        }
        self.healthStore?.execute(heartRate)
    }
    
    class func heartRateLastTwoDays(completion: @escaping ([HKQuantitySample], Error?) -> () ) {
        let sampleType = HKSampleType.quantityType(forIdentifier: HKQuantityTypeIdentifier.heartRate)
        
        let sortDescriptor = NSSortDescriptor(key: HKSampleSortIdentifierEndDate, ascending: false)
        let predicate = self.predicateLastTwoDays()
        let queryHeartRate = HKSampleQuery(sampleType : sampleType!, predicate : predicate, limit: 0, sortDescriptors: [sortDescriptor])
        { query, results, error in
            if results != nil && (results?.count)! > 0 {
                completion(results! as! [HKQuantitySample], error)
            } else {
                completion([], error)
            }
        }
        self.healthStore?.execute(queryHeartRate)
    }
    
    class func heartRateFor(startDate : Date, endDate : Date, completion: @escaping ([HKQuantitySample], Error?) -> () ) {
        let sampleType = HKSampleType.quantityType(forIdentifier: HKQuantityTypeIdentifier.heartRate)
        
        let sortDescriptor = NSSortDescriptor(key: HKSampleSortIdentifierEndDate, ascending: false)
        let predicate = self.predicateFrom(startDate: startDate, endDate: endDate)
        
        let queryHeartRate = HKSampleQuery(sampleType : sampleType!, predicate : predicate, limit: 0, sortDescriptors: [sortDescriptor])
        { query, results, error in
            if results != nil && (results?.count)! > 0 {
                completion(results! as! [HKQuantitySample], error)
            } else {
                completion([], error)
            }
        }
        self.healthStore?.execute(queryHeartRate)
    }
    
    class func heartRateForToday(completion: @escaping ([HKQuantitySample], Error?) -> () ) {
        let sampleType = HKSampleType.quantityType(forIdentifier: HKQuantityTypeIdentifier.heartRate)
        
        let sortDescriptor = NSSortDescriptor(key: HKSampleSortIdentifierEndDate, ascending: false)
        let predicate = self.predicateForSamplesToday()
        let queryHeartRate = HKSampleQuery(sampleType : sampleType!, predicate : predicate, limit: 0, sortDescriptors: [sortDescriptor])
        { query, results, error in
            if results != nil && (results?.count)! > 0 {
                completion(results! as! [HKQuantitySample], error)
            } else {
                completion([], error)
            }
        }
        self.healthStore?.execute(queryHeartRate)
    }
    
    class func stepsFor(caching : Bool, startDate : Date, endDate : Date, completion: @escaping ([HKSample], Error?) -> () ) {
        let typeSteps = HKSampleType.quantityType(forIdentifier: HKQuantityTypeIdentifier.stepCount)
        let sortDescriptor = NSSortDescriptor(key: HKSampleSortIdentifierEndDate, ascending: true)
        let todayPredicate: NSPredicate = self.predicateFrom(startDate: startDate, endDate: endDate)
        let querySteps = HKSampleQuery(sampleType : typeSteps!, predicate : todayPredicate, limit: 0, sortDescriptors: [sortDescriptor])
        { query, results, error in
            if results != nil && (results?.count)! > 0 {
                completion(results!, error)
            } else {
                completion([], error)
            }
        }
        if caching == true {
            self.healthStoreStress?.execute(querySteps)
        } else {
            self.healthStore?.execute(querySteps)
        }
    }

    
    class func activeEnergyBurned(completion: @escaping (Double, NSError?) -> () )
    { // this function gives you all of the steps the user has taken since the beginning of the current day.
        
        let type = HKSampleType.quantityType(forIdentifier: HKQuantityTypeIdentifier.activeEnergyBurned) // The type of data we are requesting
        let todayPredicate: NSPredicate = self.predicateForSamplesToday()
        
        // The actual HealthKit Query which will fetch all of the steps and add them up for us.
        let query = HKSampleQuery(sampleType: type!, predicate: todayPredicate, limit: 0, sortDescriptors: nil) { query, results, error in
            var calories : Double = 0
            
            if results != nil && (results?.count)! > 0
            {
                for result in results as! [HKQuantitySample]
                {
                    calories += result.quantity.doubleValue(for:HKUnit.kilocalorie())
                }
            }
            completion(Double(calories), error as NSError?)
        }
        self.healthStore?.execute(query)
    }
    
    class func basalEnergyBurned(completion: @escaping (Double, NSError?) -> () )
    { // this function gives you all of the steps the user has taken since the beginning of the current day.
        
        let type = HKSampleType.quantityType(forIdentifier: HKQuantityTypeIdentifier.basalEnergyBurned) // The type of data we are requesting
        let todayPredicate: NSPredicate = self.predicateForSamplesToday()
        
        // The actual HealthKit Query which will fetch all of the steps and add them up for us.
        let query = HKSampleQuery(sampleType: type!, predicate: todayPredicate, limit: 0, sortDescriptors: nil) { query, results, error in
            var calories : Double = 0
            
            if results != nil && (results?.count)! > 0
            {
                for result in results as! [HKQuantitySample]
                {
                    calories += result.quantity.doubleValue(for:HKUnit.kilocalorie())
                }
            }
            completion(Double(calories), error as NSError?)
        }
        self.healthStore?.execute(query)
    }
    
    class func getWeightFor(startDate : Date, endDate : Date, completion : @escaping ([HKSample], Error?) -> ()) {
        let sampleType = HKSampleType.quantityType(forIdentifier: HKQuantityTypeIdentifier.bodyMass)
        let predicate = self.predicateFrom(startDate: startDate, endDate: endDate)
        let sortDescriptor = NSSortDescriptor(key: HKSampleSortIdentifierEndDate, ascending: true)
        
        let query = HKSampleQuery(sampleType: sampleType!, predicate: predicate, limit: 0, sortDescriptors: [sortDescriptor]) { query, results, error in
            
            if results != nil && (results?.count)! > 0 {
                completion(results!, error)
            } else {
                completion([], error)
            }
            
        }
        self.healthStore?.execute(query)
    }
    
    class func getHeightsFor(startDate : Date, endDate : Date, completion : @escaping ([HKSample], Error?) -> ()) {
        let sampleType = HKSampleType.quantityType(forIdentifier: HKQuantityTypeIdentifier.height)
        let predicate = self.predicateFrom(startDate: startDate, endDate: endDate)
        let sortDescriptor = NSSortDescriptor(key: HKSampleSortIdentifierEndDate, ascending: true)
        
        let query = HKSampleQuery(sampleType: sampleType!, predicate: predicate, limit: 0, sortDescriptors: [sortDescriptor]) { query, results, error in
            if results != nil && (results?.count)! > 0 {
                completion(results!, error)
            } else {
                completion([], error)
            }
            
        }
        self.healthStore?.execute(query)
    }
    
    class func getCaloriesFor(startDate : Date, endDate : Date, completion : @escaping ([HKSample], Error?) -> ()) {
        
        let type = HKSampleType.quantityType(forIdentifier: HKQuantityTypeIdentifier.activeEnergyBurned)
        
        let predicate = self.predicateFrom(startDate: startDate, endDate: endDate)
        let sortDescriptor = NSSortDescriptor(key: HKSampleSortIdentifierEndDate, ascending: false)
        
        let query = HKSampleQuery(sampleType: type!, predicate: predicate, limit: 0, sortDescriptors: [sortDescriptor]) { query, results, error in
            
            if results != nil && (results?.count)! > 0 {
                completion(results!, error)
            } else {
                completion([], error)
            }
            
        }
        self.healthStore?.execute(query)
    }
    
    class func getBasalCaloriesFor(startDate : Date, endDate : Date, completion : @escaping ([HKSample], Error?) -> ()) {
        
        let type = HKSampleType.quantityType(forIdentifier: HKQuantityTypeIdentifier.basalEnergyBurned)
        
        let predicate = self.predicateFrom(startDate: startDate, endDate: endDate)
        let sortDescriptor = NSSortDescriptor(key: HKSampleSortIdentifierEndDate, ascending: false)
        
        let query = HKSampleQuery(sampleType: type!, predicate: predicate, limit: 0, sortDescriptors: [sortDescriptor]) { query, results, error in
            
            if results != nil && (results?.count)! > 0 {
                completion(results!, error)
            } else {
                completion([], error)
            }
            
        }
        self.healthStore?.execute(query)
    }
    
    class func getWorkoutSamples(startDate : Date, endDate : Date, completion : @escaping ([HKWorkout], Error?) ->()) {
        let type = HKObjectType.workoutType()
        let predicate = self.predicateFrom(startDate: startDate, endDate: endDate)
        
        let sortDescriptor = NSSortDescriptor(key : HKSampleSortIdentifierEndDate, ascending : true)
        
        let query = HKSampleQuery(sampleType: type, predicate: predicate, limit: 0, sortDescriptors: [sortDescriptor]) { query, results, error in
            
            if results != nil && (results?.count)! > 0 {
                completion(results! as! [HKWorkout], error)
            } else {
                completion([], error)
            }
            
        }
        self.healthStore?.execute(query)
    }
    
    class func getDistanceSamples(startDate : Date, endDate : Date, completion : @escaping ([HKSample], Error?) -> ()) {
        
        let type = HKSampleType.quantityType(forIdentifier: .distanceWalkingRunning)
        let predicate = self.predicateFrom(startDate: startDate, endDate: endDate)
        let sortDescriptor = NSSortDescriptor(key : HKSampleSortIdentifierEndDate, ascending : true)
        let query = HKSampleQuery(sampleType: type!, predicate: predicate, limit: 0, sortDescriptors: [sortDescriptor]) { query, results, error in
            
            if results != nil && (results?.count)! > 0 {
                completion(results!, error)
            } else {
                completion([], error)
            }
            
        }
        self.healthStore?.execute(query)
    }
    
    class func fetchDevicesForType(type : HKSampleType, completion : @escaping ([HKSource], Error?) -> ()) {
        let sampleType = type
        
        let query = HKSourceQuery(sampleType: sampleType, samplePredicate: nil) {
            query, sources, error in
            
            if error != nil {
                abort()
            } else {
                if sources != nil {
                    completion(Array(sources!), error)
                }
            }
        }
        
        self.healthStore?.execute(query)
    }
    
    class func getSamplesForSource(source : HKSource, quantityType : HKSampleType, startDate : Date, endDate : Date,completion : @escaping ([HKSample], Error?) -> ()) {
        
        if quantityType.isKind(of: HKCategoryType.self) {
            print(quantityType)
        }
        
        let predicate = HKQuery.predicateForSamples(withStart: startDate, end: endDate, options: [])
        let devicePredicate : NSPredicate = HKQuery.predicateForObjects(from: source)
        let cmpPred = NSCompoundPredicate.init(type: .and, subpredicates: [predicate, devicePredicate])
        let sortDescriptor = NSSortDescriptor(key : HKSampleSortIdentifierEndDate, ascending : true)
        
        let query = HKSampleQuery(sampleType: quantityType, predicate: cmpPred, limit: 0, sortDescriptors: [sortDescriptor]) { query, results, error in
            
            if results != nil && (results?.count)! > 0 {
                completion(results!, error)
            } else {
                completion([], error)
            }
            
        }
        
        self.healthStore?.execute(query)
        
    }
    
    class func getStatisticFrom(startDate : Date, endDate : Date, quantityType : HKQuantityType,options : HKStatisticsOptions?, completion : @escaping (HKStatisticsCollection?, Error?) -> ()) {
        let predicate = HKQuery.predicateForSamples(withStart: startDate, end: endDate, options: .strictStartDate)
        var interval = DateComponents()
        interval.day = 1
        
        var opts : HKStatisticsOptions = [.cumulativeSum,.separateBySource]
        
        if options != nil {
            opts = options!
            interval.year = 1
        }
        
        let query = HKStatisticsCollectionQuery.init(quantityType: quantityType, quantitySamplePredicate: predicate, options: opts, anchorDate: startDate, intervalComponents: interval)
        
        query.initialResultsHandler = {query, result, error in
            completion(result, error)
        }
        
        self.healthStore?.execute(query)
    }
    
    class func getHeartRateStatisticsForLastTwoMonths(caching : Bool, completion : @escaping (HKStatisticsCollection?, Error?) -> ()) {
        
        let stepsCount = HKQuantityType.quantityType(forIdentifier: HKQuantityTypeIdentifier.heartRate)
        
        //   Get the start of the day
        let cal = Calendar.current

        let startOfWeek = cal.date(from: cal.dateComponents([.yearForWeekOfYear, .weekOfYear], from: Date()))!
        
        var dateComps = DateComponents()
        dateComps.weekdayOrdinal = -7
        
        let newDate = cal.date(byAdding: dateComps, to: startOfWeek)
        
        //  Set the Predicates & Interval
        let predicate = HKQuery.predicateForSamples(withStart: newDate, end: Date(), options: .strictStartDate)
        var interval: DateComponents = DateComponents()
        interval.day = 7
        
        //  Perform the Query
        let query = HKStatisticsCollectionQuery(quantityType: stepsCount!, quantitySamplePredicate: predicate, options: [.discreteMin,.discreteMax,.discreteAverage], anchorDate: newDate!, intervalComponents:interval)
        
        query.initialResultsHandler = { query, results, error in
            completion(results, error)
        }
        if caching == true {
            self.healthStoreStress?.execute(query)
        } else {
            self.healthStore?.execute(query)
        }
        
    }
    
    class func getStepStatistic(startDate : Date, endDate : Date, period : period, completion : @escaping (HKStatisticsCollection?, Error?) -> ()) {
        
        let stepsCount = HKQuantityType.quantityType(forIdentifier: HKQuantityTypeIdentifier.stepCount)
        let predicate = HKQuery.predicateForSamples(withStart: startDate, end: endDate, options: .strictStartDate)
        
        var interval: DateComponents = DateComponents()
        
        if period == .monthly {
            interval.weekOfYear = 1
        } else if period == .weekly {
            interval.day = 1
        } else {
            interval.hour = 1
        }
        
        let query = HKStatisticsCollectionQuery(quantityType: stepsCount!, quantitySamplePredicate: predicate, options: [.cumulativeSum], anchorDate: startDate, intervalComponents:interval)
        
        query.initialResultsHandler = { query, results, error in
            completion(results, error)
        }
        
        self.healthStore?.execute(query)
        
        
    }
    
    class func getHeartRateSamle(caching : Bool,startDate : Date, endDate : Date, completion : @escaping ([HKQuantitySample], Error?) -> ()) {
        let sampleType = HKSampleType.quantityType(forIdentifier: HKQuantityTypeIdentifier.heartRate)
        
        let sortDescriptor = NSSortDescriptor(key: HKSampleSortIdentifierEndDate, ascending: false)
        let predicate: NSPredicate = self.predicateFrom(startDate: startDate, endDate: endDate)
        let heartRate = HKSampleQuery(sampleType : sampleType!, predicate : predicate, limit: 0, sortDescriptors: [sortDescriptor])
        { query, results, error in
            if results != nil && (results?.count)! > 0 {
                completion(results! as! [HKQuantitySample], error)
            } else {
                completion([], error)
            }
        }
        if caching == true {
            self.healthStoreStress?.execute(heartRate)
        } else {
            self.healthStore?.execute(heartRate)
        }
    }
    
    class func storeWeight(weight : Double, date : Date) {
        let weightUnit = HKUnit.gramUnit(with: HKMetricPrefix.kilo)
        let type = HKQuantityType.quantityType(forIdentifier: HKQuantityTypeIdentifier.bodyMass)
        let quantity = HKQuantity(unit: weightUnit, doubleValue: weight)
        //let now = Date()
        
        let sample = HKQuantitySample.init(type: type!, quantity: quantity, start: date, end: date)
        
        self.healthStore?.save(sample, withCompletion: {success, error in
            if error != nil {
                LogUtility.logToFile("error store weight", error!)
            }
        })
        
    }
    
    class func storeHeight(height : Double) {
        let heightUnit = HKUnit.meter()
        let type = HKQuantityType.quantityType(forIdentifier: HKQuantityTypeIdentifier.height)
        let quantity = HKQuantity(unit: heightUnit, doubleValue: height)
        let now = Date()
        
        let sample = HKQuantitySample.init(type: type!, quantity: quantity, start: now, end: now)
        
        self.healthStore?.save(sample, withCompletion: {success, error in
            if error != nil {
                LogUtility.logToFile("error storing height", error!)
            }
        })
        
    }
    
    class func fetchFirstActivityDateFor(type : HKSampleType, device : HKSource, completion : @escaping (Date?, Error?) ->()) {
        let epohTime = Date(timeIntervalSince1970: 0.0)
        
        var endDate = Calendar.current.startOfDay(for: Date())
        var comps = DateComponents()
        comps.second = -1
        
        endDate = Calendar.current.date(byAdding: comps, to: endDate)!
        
        let predicate: NSPredicate = self.predicateFrom(startDate: epohTime, endDate: endDate)
        let devicePredicate : NSPredicate = HKQuery.predicateForObjects(from: device)
        let cmpPred = NSCompoundPredicate.init(type: .and, subpredicates: [predicate, devicePredicate])
        
        let sortDescriptor = NSSortDescriptor(key: HKSampleSortIdentifierEndDate, ascending: true)
        
        let query = HKSampleQuery(sampleType: type, predicate: cmpPred, limit: 1, sortDescriptors: [sortDescriptor]) { query, results, error in
            
            var date : Date? = nil
            if results != nil && (results?.count)! > 0
            {
                date = results?.first?.startDate
            }
            completion(date, error)
        }
        self.healthStore?.execute(query)
        
    }
    
    class func scanForNextSampleFor(type : HKSampleType, device : HKSource, startDate : Date, completion : @escaping (HKSample?, Error?) -> ()) {
        
        let calendar = Calendar.current
        
        if let yesterday = calendar.date(byAdding: .day, value: -1, to: Date()) {
            let endDate = calendar.startOfDay(for: yesterday)
            let predicate: NSPredicate = self.predicateFrom(startDate: startDate, endDate: endDate)
            let devicePredicate : NSPredicate = HKQuery.predicateForObjects(from: device)
            let cmpPred = NSCompoundPredicate(type: .and, subpredicates: [predicate, devicePredicate])
            
            let sortDescriptor = NSSortDescriptor(key: HKSampleSortIdentifierEndDate, ascending: true)
            
            let query = HKSampleQuery(sampleType: type, predicate: cmpPred, limit: 1, sortDescriptors: [sortDescriptor]) { query, results, error in
            
                 completion(results?.first, error)
            }
            self.healthStore?.execute(query)
            
        } else {
            completion(nil, nil)
        }
        
    }
    
    class func fetchSamplesFor(type : HKQuantityType, device : HKSource, startDate : Date, endDate : Date, completion : @escaping ([HKSample], Error?) ->()) {
        
        let predicate: NSPredicate = self.predicateFrom(startDate: startDate, endDate: endDate)
        let devicePredicate : NSPredicate = HKQuery.predicateForObjects(from: device)
        let cmpPred = NSCompoundPredicate.init(type: .and, subpredicates: [predicate, devicePredicate])
        
        let sortDescriptor = NSSortDescriptor(key: HKSampleSortIdentifierEndDate, ascending: true)
        
        let query = HKSampleQuery(sampleType: type, predicate: cmpPred, limit: 0, sortDescriptors: [sortDescriptor]) { query, results, error in
            
            if results != nil && (results?.count)! > 0 {
                completion(results!, error)
            } else {
                completion([], error)
            }
            
        }
        self.healthStore?.execute(query)
        
    }
    
    class func fetchFirstStepDate(completion : @escaping (Date?, Error?) -> ()) {
        
        let type = HKSampleType.quantityType(forIdentifier: HKQuantityTypeIdentifier.stepCount)
        
        let epohTime = Date(timeIntervalSince1970: 0.0)
        
        var endDate = Calendar.current.startOfDay(for: Date())
        var comps = DateComponents()
        comps.second = -1
        
        endDate = Calendar.current.date(byAdding: comps, to: endDate)!
        let predicate: NSPredicate = self.predicateFrom(startDate: epohTime, endDate: endDate)
        
        let sortDescriptor = NSSortDescriptor(key: HKSampleSortIdentifierEndDate, ascending: true)
        
        
        let query = HKSampleQuery(sampleType: type!, predicate: predicate, limit: 1, sortDescriptors: [sortDescriptor]) { query, results, error in
            
            var date : Date? = nil
            if results != nil && (results?.count)! > 0
            {
                date = results?.first?.startDate
            }
            completion(date, error)
        }
        self.healthStore?.execute(query)
    }
    
    class func permissionsWasAsked() -> HKAuthorizationStatus? {
        let heartRateType = HKQuantityType.quantityType(forIdentifier: HKQuantityTypeIdentifier.heartRate)!
        return self.healthStore?.authorizationStatus(for: heartRateType)
    }
    
}

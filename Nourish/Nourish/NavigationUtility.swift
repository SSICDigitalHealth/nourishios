//
//  NavigationUtility.swift
//  Nourish
//
//  Created by Gena Mironchyk on 2/22/17.
//  Copyright © 2017 SSIC. All rights reserved.
//

import UIKit
let appDelegate = UIApplication.shared.delegate!
var loginSplashVC : NRLoginSplashViewController?
class NavigationUtility: NSObject {
    class func changeRootViewController(userProfile : UserProfile?, awakedFromBackground : Bool) {
        let session = NRUserSession.sharedInstance
        if session.accessToken == nil {
            if loginSplashVC == nil {
                loginSplashVC = NRLoginSplashViewController(nibName: "NRLoginSplashViewController", bundle: nil)
            }
            appDelegate.window?!.rootViewController = loginSplashVC
            appDelegate.window?!.makeKeyAndVisible()
        } else {
            let tabBarController = NourishTabBarViewController()
            tabBarController.chatViewControllerInstance().userProfile = userProfile
            tabBarController.chatViewControllerInstance().awakedFromBackground = awakedFromBackground
            let snapshot : UIView = (appDelegate.window?!.snapshotView(afterScreenUpdates: true))!
            tabBarController.view.addSubview(snapshot)
            appDelegate.window?!.rootViewController = tabBarController
            appDelegate.window?!.makeKeyAndVisible()
            UIView.animate(withDuration: 0.3, animations: {() in
                snapshot.layer.opacity = 0
                snapshot.layer.transform = CATransform3DMakeScale(1.5, 1.5, 1.5)
            }, completion: {
                (value: Bool) in
                snapshot.removeFromSuperview();
                loginSplashVC = nil
            });
        }
    }
    

    class func takeScreenShot ()  -> UIImage?{
        if  let window = appDelegate.window?! {
            UIGraphicsBeginImageContextWithOptions(window.frame.size, window.isOpaque, UIScreen.main.scale)
            window.drawHierarchy(in: window.bounds, afterScreenUpdates: true)
        }
        let image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        return image
       
    }
    
    class func navigateToFoodDiaryFor(date: Date) {
        let rootController = appDelegate.window?!.rootViewController as! NourishTabBarViewController
        rootController.dismiss(animated: false, completion:nil)
        rootController.openFoodDiaryForDate(date: date, showFoodSearch: false, ocasion: nil)
    }
    
    class func presentVC(vc : UIViewController, needDismission : Bool) {
        let rootController = appDelegate.window?!.rootViewController as! NourishTabBarViewController
        rootController.presentModal(vc: vc, animated: true, needDismission: needDismission)
    }
    
//    class func showDetailedReportFor(date : Date) {
//        let rootController = appDelegate.window?!.rootViewController as! NourishTabBarViewController
//        rootController.showDetailedReportFor(date: date)
//    }
    
    class func showChatView () {
        let rootController = appDelegate.window?!.rootViewController as! NourishTabBarViewController
        rootController.showChat()
    }
    
    class func showActivityController () {
        let rootController = appDelegate.window?!.rootViewController as! NourishTabBarViewController
        rootController.showActivity()
    }
    
    class func showNutritionController () {
        let rootController = appDelegate.window?!.rootViewController as! NourishTabBarViewController
        rootController.showNutrition()
    }
    
    class func dismiss(animated : Bool) {
        let rootController = appDelegate.window?!.rootViewController as! NourishTabBarViewController
        rootController.dismiss(animated: animated, completion: nil)
    }
    
    class func hideTabBar(animated : Bool) {
        DispatchQueue.main.async {
            let rootController = appDelegate.window?!.rootViewController as! NourishTabBarViewController
            rootController.hideTabbarView(animated : animated)
        }
    }
    
    class func showTabBar(animated : Bool) {
        let rootController = appDelegate.window?!.rootViewController as! NourishTabBarViewController
        rootController.showTabbarView(animated : animated)
    }
    
    class func openFoodSearchFor(ocasion : Ocasion) {
        self.dismiss(animated: false)
        let rootController = appDelegate.window?!.rootViewController as! NourishTabBarViewController
        rootController.openFoodDiaryForDate(date: Date(), showFoodSearch: true, ocasion: ocasion)
    }
    
    class func showDetailVC(model : foodSearchModel) {
        let vc = NRFoodDetailsViewController(nibName: "NRFoodDetailsViewController", bundle: nil)
        vc.title = model.foodTitle
        vc.foodSearchModel = model
        self.presentVC(vc: vc, needDismission: true)
    }
    
    class func selectedViewControllerIndex() -> Int {
        let rootController = appDelegate.window?!.rootViewController as! NourishTabBarViewController
        return rootController.selectedIndex
    }
    
    class func chatViewControllerInstance () -> ChatViewController {
        let rootController = appDelegate.window?!.rootViewController as! NourishTabBarViewController
        return rootController.chatViewControllerInstance()
    }
}


//
//  BackgroundMealDiarySyncInteractor.swift
//  Nourish
//
//  Created by Nova on 7/6/17.
//  Copyright © 2017 SSIC. All rights reserved.
//

import UIKit
import RxSwift
import RealmSwift

let kEmptyOperations = "kEmptyOperations"
let kOperationsDidFinised = "operationsFinished"

typealias operationResult = (Bool, String)

class BackgroundMealDiarySyncInteractor: NSObject {
    static let shared = BackgroundMealDiarySyncInteractor()
    
    let mealRecordRepository = MealHistoryRepository.shared
    let realmRepo = FoodRecordRepository.shared
    let operationRepo = OperationCacheRepository()
    let foodDataStore = NourishDataStore.shared
    let bgFetcher = BackgroundChangeScoreFetcher.shared
    var isInProgress = false
    let imageStore = ImageRealmDataStore()
    let cacheToObjectMapper = MealRecordCache_to_MealRecord()
    let toSearchModelMapper = MealRecord_to_foodSearchModel()

    let scoreJsonMapper = json_to_ScoreCacheRealm()
    let scoreRepo = ScoreFeedbackRepository()
    let backDS = BackgroundActivityDumperDataStore()
    
    func execute() {
        if NRUserSession.sharedInstance.accessToken != nil {
            let scheduler = ConcurrentDispatchQueueScheduler(qos: .background)
            let _ = mealRecordRepository.getCacheWithChangesFor(date: Date()).observeOn(scheduler).subscribeOn(scheduler).subscribe(onNext: { [unowned self] records in
                let deleted = records.filter {$0.isDeleted == true}
                let created = records.filter {$0.isCreated == true}
                let changed = records.filter {$0.isCreated == false && $0.isDeleted == false}
                
                self.storeCreatedModels(models: created)
                self.deleteModels(models: deleted)
                self.changeAmountFor(models: changed)
            })
        }
    }
    
    private func storeCreatedModels(models : [MealRecordCache]) {
        let routine = NMFood_to_MealRecordRoutine()
        
        for object in models {
            let identity = object.localCacheID
            let model = object.shortModelRepresentation()
            let date = object.date ?? Date()
            
            let obj = routine.recomendationFoodToMeal(model: model, date: date, occasion : object.occasionEnum)
            
            let _ = self.mealRecordRepository.uploadMealAndGetFoodID(mealRecord: obj).subscribe(onNext: { [unowned self] foodID in
                
                let realm = try! Realm()
                let fetched = self.mealRecordRepository.fetchFor(idArray: [identity]).first
                if fetched != nil {
                    try! realm.write {
                        fetched?.userFoodId = foodID
                        fetched?.hasChanges = false
                    }
                }

            }, onError: {error in }, onCompleted: {}, onDisposed: {})
        }
    }
    
    private func increaseCounterForOperationID(idString : String) {
        let operationRepo = OperationCacheRepository()
        operationRepo.increaseCounterForID(stringID: idString)
    }
    
    func deleteOperationWith(idString : String) {
        let operationRepo = OperationCacheRepository()
        operationRepo.deleteOperationWithIDent(string: idString)
    }
    private func deleteModels(models : [MealRecordCache]) {
        
    }
    
    private func changeAmountFor(models : [MealRecordCache]) {
        for model in models {
            let identity = model.localCacheID
            
            let foodSearch = model.shortModelRepresentation()
            let _ = self.mealRecordRepository.changeMealRecord(foodsearcMeal: foodSearch).subscribe(onNext: {newValue in }, onError: {error in }, onCompleted: {
                let realm = try! Realm()
                let fetchedObject = self.mealRecordRepository.fetchCacheObjectFor(ident: identity)
                if fetchedObject != nil {
                    try! realm.write {
                        fetchedObject?.hasChanges = false
                    }
                }
            }, onDisposed: {})
        }
    }
    
    func testFetch() {
        if self.isInProgress == false {
            self.isInProgress = true
            let _ = self.newExecute().observeOn(MainScheduler.instance).subscribe(onNext: {result in
                
                    let logString = String(format : "Operation with id %@ did finish with result %@", result.1, result.0 ? "success" : "failed")
                    LogUtility.logToFile(logString)
                    if result.0 == true && result.1 != "" {
                        self.deleteOperationWith(idString: result.1)
                        self.isInProgress = false
                        self.testFetch()
                    } else {
                        if result.1 == kEmptyOperations {
                            NotificationCenter.default.post(name: Notification.Name(kOperationsDidFinised), object: nil)
                            self.isInProgress = false
                            self.bgFetcher.executeFor(date: Date())
                        } else {
                            self.increaseCounterForOperationID(idString: result.1)
                            self.isInProgress = false
                            self.testFetch()
                        }
                    }
                
            }, onError: {error in
                self.isInProgress = false
                self.testFetch()
            }, onCompleted: {
            }, onDisposed: {
              //  self.isInProgress = false
            })
        } else {
            print("Canceled operation in progress")
        }
    }
    
    func newExecute() -> Observable<operationResult> {
        if NRUserSession.sharedInstance.accessToken != nil {
            var resultFirst = Observable.just(operationResult(false, kEmptyOperations))
            if let operation = self.operationRepo.fetchFirstRecordFor(date: Date()) {
                
                LogUtility.logToFile("Operation did started with id ", operation.localCacheID)
                
                if operation.operationTypeEnum == .create {
                    resultFirst = self.processCreateOperation(operation: operation)
                }
                
                if operation.operationTypeEnum == .createVoice {
                    resultFirst = self.processCreateVoiceOperation(operation: operation)
                }
                
                if operation.operationTypeEnum == .update {
                    resultFirst = self.processChangeAmount(operation: operation)
                }
                
                if operation.operationTypeEnum == .delete {
                    resultFirst = self.processDeleteOperation(operation: operation)
                }
                
                if operation.operationTypeEnum == .merge {
                    resultFirst = self.processCreateGroupOperation(operation: operation)
                }
                
                if operation.operationTypeEnum == .createGroup {
                    resultFirst = self.processAddCreateGroupOperation(operation: operation)
                }
                
                if operation.operationTypeEnum == .addPhoto {
                    resultFirst = self.processAddPhotoOperation(operation: operation)
                }
                
            }
            
            return resultFirst
        }
        return Observable.just(operationResult(false, kEmptyOperations))
    }
    
    
    
    private func getFoodIDFrom(data : Data) -> String {
        var stringValue = ""
            do {
                let dict = try JSONSerialization.jsonObject(with: data, options: []) as? [String : Any]
                var idString = ""
                if dict?["userfoodids"] != nil {
                    let array : [String] = dict?["userfoodids"] as! [String]
                    idString = array[0]
                }
                stringValue = idString
            } catch let error as NSError {
                print(error)
            }
        return stringValue
    }
    
    private func getMealIDFrom(data : Data) -> [String:String]{
        var stringValue : [String:String] = [:]
            do {
                let dict = try JSONSerialization.jsonObject(with: data, options: []) as? [String : Any]
                var idDict : [String : String] = [:]
                if dict?["userfoodids"] != nil {
                    let userFoodIdArray : [String] = dict?["userfoodids"] as! [String]
                    let mealIdArray : [String] = dict?["foodids"] as! [String]
                    
                    idDict["mealIdString"] = mealIdArray[0]
                    idDict["userFoodId"] = userFoodIdArray[0]
                    stringValue = idDict
                }
            } catch let error as NSError {
                print(error)
            }
        return stringValue
    }
    
    private func processAddCreateGroupOperation(operation : OperationCache) -> Observable<operationResult> {
        if NRUserSession.sharedInstance.accessToken != nil {
            let repo = FavoritesRepository()
            let foodGroupID = operation.foodGroupID
            
            var array = [MealRecordCache]()
            for object in operation.affectedIDs {
                if let object = self.mealRecordRepository.fetchCacheObjectFor(ident: object) {
                    array.append(object)
                }
            }
            let operationCache = operation.localCacheID
           // let operationType = operation.operationTypeEnum
            
            let dict = operation.dictionary
            let date = operation.date ?? Date()
            let name = dict["name"] as? String ?? ""
            let ocasionString = dict["ocasion"] as? String ?? "breakfast"
            let ocasion = Ocasion.enumFrom(stringRep: ocasionString)
            
            let some = repo.addToDiaryWithPhoto(models: array, name: name, ocasion: ocasion, date: date, userID: operation.userID!).flatMap {groupID -> Observable<Bool> in
                
                if foodGroupID != nil && (foodGroupID?.hasPrefix("local"))! {
                    let cacheRepo = OperationCacheRepository()
                    
                    if let record = cacheRepo.fetchForGroupID(ident: foodGroupID!) {
                        LogUtility.logToFile("---> groupIDLocal : \(foodGroupID) groupIDB : \(groupID)")
                        let realm = try! Realm()
                        try! realm.write {
                            record.foodGroupID = groupID
                        }
                    }
                }
                
                return repo.addToDiary(foodGroupd: groupID, groupName: name, occasion: ocasion, date: date, models: [])
            }.flatMap { success -> Observable<operationResult> in
                
                
                let operationRR = operationResult(success, operationCache)
                return Observable.just(operationRR)
            }
            return some
        } else {
            return Observable.just(operationResult(false, ""))
        }
        
    }
    
    private func processCreateGroupOperation(operation : OperationCache) -> Observable<operationResult> {
        if let token = NRUserSession.sharedInstance.accessToken {
            let store = self.foodDataStore
            let favoritesRepo = FavoritesRepository()
            //let operationType = operation.operationTypeEnum
            
            let realm = try! Realm()
            try! realm.write {
                operation.counter += 1
            }
            
            let operationID = operation.localCacheID
            
            let ids = operation.dictionary[kBackendIDKey] as? [String]
            var proxy = [foodSearchModel]()
            
            for id in ids! {
                var model = foodSearchModel()
                model.foodBackendID = id
                proxy.append(model)
            }
            let name = operation.dictionary[kGroupNameKey] as? String
            
            let userID = operation.userID ?? NRUserSession.sharedInstance.userID
            
            let value = store.addMealsToGroup(token: token, userID: userID!, foodModels: proxy, name: name!).flatMap({ data -> Observable<operationResult> in
                favoritesRepo.refreshCacheFavorites()
                return Observable.just(operationResult(true, operationID))
            })
            return value
        }
       
        return Observable.just(operationResult(false, ""))
    }
    
    private func processAddPhotoOperation(operation : OperationCache) -> Observable<operationResult> {
        if let token = NRUserSession.sharedInstance.accessToken {
            let store = self.foodDataStore
            
            let operationID = operation.localCacheID
            let photoID = operation.userPhotoID
            let realm = try! Realm()
            try! realm.write {
                operation.counter += 1
            }
            
            if let image = self.imageStore.getImage(ident: photoID!) {
                let userFoodID = operation.userFoodID
                let groupFoodID = operation.foodGroupID
                
                let value = store.uploadPhoto(image: image, token: token, userID: NRUserSession.sharedInstance.userID!, userfoodID: userFoodID, groupID: groupFoodID).flatMap { photoIDent -> Observable<operationResult> in
                    
                    var returnValue = operationResult(false, "")
                    
                    if photoIDent != "" {
                        let photo = self.imageStore.getImageObject(ident: photoID!)
                        let realm = try! Realm()
                        try! realm.write {
                            photo?.userPhotoID = photoIDent
                        }
                        returnValue = operationResult(true, operationID)
                    }
                    return Observable.just(returnValue)
                    
                }
                return value
                
            }
            
        }
        return Observable.just(operationResult(false, ""))
    }
    
    private func processDeleteOperation(operation : OperationCache) -> Observable<operationResult> {
        return Observable.create { observer in
            let token = NRUserSession.sharedInstance.accessToken
            let operationID = operation.localCacheID
            let operationDate = operation.timeStamp ?? Date()
            let operationType = operation.operationTypeEnum
            let dateToRefetch = operation.date ?? Date()
            let affectedIDs = operation.affectedIDs
            if operation.isInvalidated == false {
                let realm = try! Realm()
                try! realm.write {
                    operation.counter += 1
                }
            }
            if token != nil {
                if let mealID = operation.userFoodID ?? operation.affectedIDs.first {
                    if let record = self.mealRecordRepository.fetchObjectFor(stringID: mealID) {
                        if record.userFoodId != nil {
                            let userID = operation.userID ?? NRUserSession.sharedInstance.userID
                            
                            var deletionList = [record.userFoodId!]
                            
                            if operation.affectedIDs.count != 1 {
                                deletionList = affectedIDs
                            }
                            
                            let _ = self.foodDataStore.changeDeleteFood(dictionary: nil, token: token!, userID: userID!, userFoodId: deletionList).subscribe(onNext: { [unowned self] data in
                                
                                let dataToWorkWith = self.getValidJSONData(data: data)
                                
                                if Calendar.current.isDateInToday(dateToRefetch) {
                                     self.storeChatMessagesFor(date: operationDate, data: dataToWorkWith, operationType: operationType)
                                }
                               
                                
                                if let obj = self.mealRecordRepository.fetchObjectFor(stringID: mealID) {
                                    let realmIn = try! Realm()
                                    try! realmIn.write {
                                        realmIn.delete(obj)
                                    }
                                }
                                self.bgFetcher.executeFor(date: dateToRefetch)
                                
                                let result = operationResult(true, operationID)
                                observer.onNext(result)
                                observer.onCompleted()
                            }, onError: {error in
                                observer.onError(error)
                            }, onCompleted: {
                                observer.onCompleted()
                            }, onDisposed: {})
                        } else {
                            observer.onNext((false, ""))
                            observer.onCompleted()
                        }
                    } else {
                        observer.onNext((false, ""))
                        observer.onCompleted()
                    }
                } else {
                    observer.onNext((false, ""))
                    observer.onCompleted()
                }
            }
            
            return Disposables.create()
        }
    }
    
    private func processChangeAmount(operation : OperationCache) -> Observable<operationResult> {
        return Observable.create { observer in
            let token = NRUserSession.sharedInstance.accessToken
            if token != nil {
                let operationID = operation.localCacheID
                let operationDate = operation.timeStamp ?? Date()
                let operationType = operation.operationTypeEnum
                
                let realm = try! Realm()
                try! realm.write {
                    operation.counter += 1
                }
                
                if let mealID = operation.userFoodID {
                    let _ = self.foodDataStore.changeDeleteFood(dictionary: operation.dictionary, token: token!, userID: NRUserSession.sharedInstance.userID!, userFoodId: [mealID]).subscribe(onNext: { [unowned self] data in
                        let dataToWorkWith = self.getValidJSONData(data: data)
                        self.storeChatMessagesFor(date: operationDate, data: dataToWorkWith, operationType: operationType)
                        let result = operationResult(true, operationID)
                        observer.onNext(result)
                        observer.onCompleted()
                    }, onError: {error in
                        observer.onError(error)
                    }, onCompleted: {
                        observer.onCompleted()
                    }, onDisposed: {})
                    
                } else {
                    let result = operationResult(false, "")
                    observer.onNext(result)
                }
                
            }

            return Disposables.create()
        }
    }
    
    private func storeChatMessagesFor(date : Date, data : Data, operationType : operationType) {
        
        do {
            let dict = try JSONSerialization.jsonObject(with: data, options: []) as? [String : Any]
            var showNoChange : Bool = false
            var idString = ""
            if dict?["userfoodids"] != nil {
                let array : [String] = dict?["userfoodids"] as! [String]
                idString = array[0]
            }
            
            var scoreDelta = 0.0
            var totalScore = 0.0
            
            if dict?["score_after"] != nil && dict?["score_before"] != nil {
                let after = dict?["score_after"] as! Double
                let before = dict?["score_before"] as! Double
                
                totalScore = after * 100
                scoreDelta = after - before
                scoreDelta = scoreDelta * 100
                if round(scoreDelta) == 0 || round(scoreDelta) == -0 {
                    showNoChange = true
                }
            }
            
            let responceMessage = ChatDataBaseItem()
            responceMessage.chatBoxType = .typeLoggedFood
            responceMessage.date = date
            
            var scoreDeltaBool = false
            if operationType == .create || operationType == .createGroup {
                responceMessage.content = showNoChange ? "Your score has not changed." :String(format: "You just got %.0f points for the food you logged", round(scoreDelta))
                scoreDeltaBool = true
            } else {
                responceMessage.content = showNoChange ? "Your score has not changed."  : String(format: "Your score has been adjusted. Your new score is %.0f points.", round(totalScore))
                scoreDeltaBool = false
            }
            
            responceMessage.isFromUser = false
            responceMessage.messageID = idString
            responceMessage.userID = NRUserSession.sharedInstance.userID ?? ""
            
            let chatStore = ChatBoxRealmDataStore()
            chatStore.storeRecordsArray(array: [responceMessage])
            let scoreCahce = self.scoreJsonMapper.transform(dictionary: dict!,date:date, isScoreDelta: scoreDeltaBool)
            self.scoreRepo.storeScoreFeedback(scoreModel: scoreCahce)
            
        } catch let error {
            LogUtility.logToFile(error)
        }
    }
    
    private func processCreateOperation(operation : OperationCache) -> Observable<operationResult> {
        return Observable.create { observer in
            
            let dateToRefetch = operation.date
            let token = NRUserSession.sharedInstance.accessToken
            let operationID = operation.localCacheID
            let operationDate = operation.timeStamp
            let operationType = operation.operationTypeEnum
            
            if operation.isInvalidated == false {
                let realm = try! Realm()
                try! realm.write {
                    operation.counter += 1
                }
            }
            
                if token != nil {
                    if let mealID = operation.affectedIDs.first {
                        if let record = self.mealRecordRepository.fetchCacheObjectFor(ident: mealID) {
                            let cacheID = record.localCacheID
                            let recordDate = record.date ?? Date()
                            
                            let metaRepo = MetaDataRepository()
                            let userID = operation.userID ?? ""
                            
                            let _ = self.foodDataStore.uploadMealRecord(mealRecord: record, userID: operation.userID!, token: token!).subscribe(onNext: { [unowned self] data in
                                
                                
                                let dataToWorkWith = self.getValidJSONData(data: data)
                                
                                let string = self.getFoodIDFrom(data: dataToWorkWith)
                                self.storeChatMessagesFor(date: recordDate, data: dataToWorkWith, operationType: operationType)
                                
                                if string != "" {
                                    let repo = OperationCacheRepository()
                                    repo.reFetchCreateGroupOperations(date: operationDate!, affectedID: cacheID, localID: string)
                                    
                                    let realm = try! Realm()
                                    
                                    if let recordChange = self.mealRecordRepository.fetchObjectFor(stringID: cacheID) {
                                        
                                        try! realm.write {
                                            recordChange.userFoodId = string

                                        }
                                    }
                                    let opers = repo.fetchUpdateOperationsFor(date: operationDate!, affectedID: cacheID)
                                    
                                    for object in opers {
                                        try! realm.write {
                                            object.userFoodID = string
                                        }
                                        
                                    }
                                    
                                    let obj = self.mealRecordRepository.fetchCacheObjectFor(ident: mealID)
                                    try! realm.write {
                                        obj?.hasChanges = false
                                        obj?.userFoodId = string
                                    }
                                    let proxyDate = dateToRefetch ?? Date()
                                    
                                    self.bgFetcher.executeFor(date: proxyDate)
                                    
                                    let result = operationResult(true, operationID)
                                    
                                    DispatchQueue.main.async {
                                        if let fetchRecord = self.mealRecordRepository.fetchCacheObjectFor(ident: mealID) {
                                            let meta = metaRepo.getDHMetaDataFor(dhSchema: .jSON, dhType: .food)
                                            let foodData = fetchRecord.dhMealRepresentation()
                                            let _ = Observable.zip(meta, foodData, resultSelector: {metaD, foodD -> [String : Any] in
                                                
                                                var totalDict = [String : Any]()
                                                
                                                totalDict["Meta"] = metaD
                                                
                                                
                                                var subdict = [String : Any]()
                                                subdict["Calories"] = record.calories
                                                subdict["Size"] = String(record.grams)
                                                subdict["Food"] = record.name
                                                subdict["OptionalData"] = self.prettyPrint(with: foodD)
                                                
                                                totalDict["Data"] = subdict
                                                
                                                return totalDict
                                            }).flatMap { dict -> Observable<Bool> in
                                                let uuid = self.randomString()
                                                return self.backDS.storeDict(dict: dict, userID: userID, type: .food, scheme: .jSON, dataSet: uuid, token: token!)
                                                }.subscribe(onNext: {result in
                                                    print("result uploading food")
                                                }, onError: {error in
                                                    print("error uploading DH \(error)")
                                                }, onCompleted: {}, onDisposed: {})
                                        }
                                        
                                    }

                                    observer.onNext(result)
                                    observer.onCompleted()
                                    
                                } else {
                                    observer.onNext((false, ""))
                                    observer.onCompleted()
                                }
                            }, onError: {error in
                                observer.onError(error)
                            }, onCompleted: {
                                observer.onCompleted()
                            }, onDisposed: {})
                        } else {
                            observer.onNext((false, ""))
                            observer.onCompleted()
                        }
                    } else {
                        observer.onNext((false, ""))
                        observer.onCompleted()
                    }
            }
            
            return Disposables.create()
        }
        
    }
    
    private func prettyPrint(with json: [String:Any]) -> String{
        let data = try! JSONSerialization.data(withJSONObject: json, options: .prettyPrinted)
        let string = NSString(data: data, encoding: String.Encoding.utf8.rawValue)
        return string as! String
    }
    
    private func randomString() -> String {
        return UUID().uuidString.replacingOccurrences(of: "-", with: "").lowercased()
    }
    
    private func processCreateVoiceOperation(operation : OperationCache) -> Observable<operationResult> {
        return Observable.create { observer in
            let token = NRUserSession.sharedInstance.accessToken
            let operationID = operation.localCacheID
            let operationDate = operation.timeStamp
            
            let realm = try! Realm()
            try! realm.write {
                operation.counter += 1
            }
            if token != nil {
                if let mealID = operation.affectedIDs.first {
                    if let record = self.mealRecordRepository.fetchCacheObjectFor(ident: mealID) {
                        let cacheID = record.localCacheID
                        let recordDate = record.date ?? Date()
                        
                        let _ = self.foodDataStore.uploadMealRecord(mealRecord: record, userID: operation.userID!, token: token!).subscribe(onNext: { [unowned self] data in
                            let dataToWorkWith = self.getValidJSONData(data: data)
                            let string = self.getFoodIDFrom(data: dataToWorkWith)
                            self.storeChatMessagesFor(date: recordDate, data: dataToWorkWith, operationType: .create)
                            
                            if string != "" {
                                let repo = OperationCacheRepository()
                                repo.reFetchCreateGroupOperations(date: operationDate!, affectedID: cacheID, localID: string)
                                
                                let realm = try! Realm()
                                
                                if let recordChange = self.mealRecordRepository.fetchObjectFor(stringID: cacheID) {
                                    
                                    //Meal Id for food logged via sound hound will be sent in the response after save is complete , mealIdString is needed for the food item to be recorded in frequent meals
                                    let mealRecord = self.cacheToObjectMapper.transform(cache: recordChange)
                                    let mealIdString = self.getMealIDFrom(data: dataToWorkWith)
                                    mealRecord.idString = mealIdString["mealIdString"]
                                    self.realmRepo.addToCounted(model: self.toSearchModelMapper.transform(record: mealRecord))
                                    
                                    try! realm.write {
                                        recordChange.userFoodId = string
                                    }
                                }
                                let opers = repo.fetchUpdateOperationsFor(date: operationDate!, affectedID: cacheID)
                                
                                for object in opers {
                                    try! realm.write {
                                        object.userFoodID = string
                                    }
                                    
                                }
                                
                                let obj = self.mealRecordRepository.fetchCacheObjectFor(ident: mealID)
                                try! realm.write {
                                    obj?.hasChanges = false
                                    obj?.userFoodId = string
                                }
                                
                                let result = operationResult(true, operationID)
                                observer.onNext(result)
                                observer.onCompleted()
                                
                            } else {
                                observer.onNext((false, ""))
                                observer.onCompleted()
                            }
                        }, onError: {error in
                            observer.onError(error)
                        }, onCompleted: {
                            observer.onCompleted()
                        }, onDisposed: {})
                    } else {
                        observer.onNext((false, ""))
                        observer.onCompleted()
                    }
                } else {
                    observer.onNext((false, ""))
                    observer.onCompleted()
                }
            }
            
            return Disposables.create()
        }
        
    }
    
    private func processMerge(operation : OperationCache) {
        let token = NRUserSession.sharedInstance.accessToken
        
        if token != nil {
            if let mealID = operation.affectedLocalCacheIDs.first {
                if let record = self.mealRecordRepository.fetchFor(idArray: [mealID.stringValue]).first {
                    let _ = self.foodDataStore.uploadMealRecord(mealRecord: record, userID: operation.userID!, token: token!).subscribe(onNext: { [unowned self] data in
                        
                        let dataToWorkWith = self.getValidJSONData(data: data)
                        let string = self.getFoodIDFrom(data: dataToWorkWith)
                        
                        if string != "" {
                            record.userFoodId = string
                        }
                    }, onError: {error in }, onCompleted: {
                        self.bgFetcher.executeFor(date: Date())
                    }, onDisposed: {})
                }
            }
        }
    }
    
    
    private func getValidJSONData(data : Data) -> Data{
        var dataToReturn = data
        if !JSONSerialization.isValidJSONObject(data) {
            let jsonStr = String(data : data, encoding : .utf8)
            let correctJson = jsonStr?.replacingOccurrences(of: "NaN", with: "0")
            if let correctData = correctJson?.data(using: .utf8) {
                dataToReturn = correctData
            }
        }
        return dataToReturn
    }
    
    /*
    private func processOperations(operations : [OperationCache]) {
        let token = NRUserSession.sharedInstance.accessToken
        
        if token != nil {
            for obj in operations {
                switch obj.operationTypeEnum {
                case .create:
                    if obj.affectedLocalCacheIDs.count == 1 {
                        let records = self.mealRecordRepository.fetchFor(idArray: obj.affectedLocalCacheIDs)
                        
                        if records.count == 1 {
                            let meal = records.first!
                            let _ = self.foodDataStore.uploadMealRecord(mealRecord: meal, userID: obj.userID!, token: token!).subscribe(onNext: {data in
                                let string = self.getFoodIDFrom(data: data)
                                
                                if string != "" {
                                    meal.userFoodId = string
                                }
                            }, onError: {error in }, onCompleted: {}, onDisposed: {})
                        }
                    }
            }
        }
        
        }
    }
    */
}

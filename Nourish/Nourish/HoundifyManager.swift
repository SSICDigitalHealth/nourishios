//
//  HoundifyManager.swift
//  Nourish
//
//  Created by Vino (Vinodha) Sundaramoorthy on 6/21/17.
//  Copyright © 2017 SSIC. All rights reserved.
//

import UIKit

let kVoiceSearchEndPoint = "https://api.houndify.com/v1/audio"
let kTapMicrophoneMessage : String = "Tap on the microphone to start speaking"

class HoundifyManager: NSObject {
    
    var conversationState : [String : Any]!
    var partialTranscript : String = ""
    var foodIdArray : [NSNumber] = []
    var mealOcassion : Ocasion!
    
    func startVoiceSearch(requestInfo:[String:Any]) {
        HoundVoiceSearch.instance().enableSpeech = true
        HoundVoiceSearch.instance().enableEndOfSpeechDetection = true
        
        
        let url = URL(string:kVoiceSearchEndPoint)
        print("Starting Voice Search")
        HoundVoiceSearch.instance().start(withRequestInfo: requestInfo, endPointURL: url) { (error, responseType, response, dict) in
            
            if (error != nil) {
                print("Error Start Voice Search \(error!)")
            } else {
                if responseType == HoundVoiceSearchResponseType.partialTranscription {
                    let partialTranscription = response as! HoundDataPartialTranscript
                    self.partialTranscript = partialTranscription.partialTranscript
                } else if responseType == HoundVoiceSearchResponseType.houndServer {
                    self.conversationState = dict as! [String : Any]!
                    self.foodIdArray = []
                    
                    if self.conversationState["AllResults"] != nil {
                        let resultsArray = self.conversationState["AllResults"] as! [Any]
                        
                        for result in resultsArray {
                            let resultDict = result as! [String:Any]
                             self.partialTranscript = resultDict["SpokenResponseLong"] as! String
                            if resultDict["InformationNuggets"] != nil {
                                let infoArray = resultDict["InformationNuggets"] as! [Any]
                                 print("info \(infoArray)")
                                for info in infoArray {
                                    let infoDetails = info as! [String:Any]
                                    let foodLogArray = infoDetails["FoodLog"] as! [[String:Any]]
                                
                                    for food in foodLogArray {
                                        let foodDict = food["Food"] as! [String : Any]
                                        print("Food id \(foodDict["ID"]!)")
                                        self.foodIdArray.append(foodDict["ID"] as! NSNumber)
                                        
                                        if food["Meal"] != nil {
                                            print("Meal \(food["Meal"]!)")
                                            self.mealOcassion = Ocasion.enumFrom(stringRep: food["Meal"] as!String)
                                        }
                                    }
                                }
                            }
                        }
                    }
                    
                    if self.foodIdArray.count > 0 && self.mealOcassion != nil {
                        self.stopListening()
                    }
                }
            }
        }
    }
    
    func startListening() {
        HoundVoiceSearch.instance().startListening { (error) in
            if error != nil {
                LogUtility.logToFile("Sound Hound  : Start Listening")
            }
        }
    }
    
    func stopListening() {
        HoundVoiceSearch.instance().stopListening { (error) in
            if (error != nil) {
                LogUtility.logToFile("Sound Hound failed with error \(error!)")
            }
        }
    }
    
    func getVoiceSearchStatus() -> String {
        switch (HoundVoiceSearch.instance().state) {
            case HoundVoiceSearchState.none:
                return "Not Ready"
            case HoundVoiceSearchState.ready:
                return "Ready"
            case HoundVoiceSearchState.recording:
                return "Recording"
            case HoundVoiceSearchState.searching:
                return "Searching"
            case HoundVoiceSearchState.speaking:
                return "Speaking"
        }
    }
}

//
//  WithingsViewController.swift
//  Nourish
//
//  Created by Gena Mironchyk on 4/18/17.
//  Copyright © 2017 SSIC. All rights reserved.
//

import UIKit
protocol WithingsViewControllerProtocol {
    func dismissWithingsView ()
}


class WithingsViewController: BasePresentationViewController, WithingsViewControllerProtocol{

    @IBOutlet weak var withingsView : WithingsView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setUpNavigationBarButtons()
        self.baseViews = [self.withingsView]
        self.withingsView.presenter.withingsViewController = self
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func setUpNavigationBarButtons () {
        let titleAttribute = self.navigationBarTitleAttribute()
        let closeButton : UIBarButtonItem =  UIBarButtonItem(image: UIImage(named:"close_icon"), style: .plain, target: self, action: #selector(self.closeAction(_:)))
        closeButton.setTitleTextAttributes(titleAttribute, for: .normal)
        navigationItem.title = self.title
        self.navigationController?.navigationBar.titleTextAttributes = titleAttribute
        self.navigationController?.navigationBar.barTintColor = NRColorUtility.nourishNavigationColor()
        self.navigationController?.navigationBar.tintColor = UIColor.white
        self.navigationController?.navigationItem.leftBarButtonItem = closeButton
        navigationItem.leftBarButtonItem = closeButton
    }
    
    func dismissWithingsView() {
        self.dismiss(animated: true, completion: nil)
    }

}

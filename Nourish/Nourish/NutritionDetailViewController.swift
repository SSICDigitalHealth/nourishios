//
//  NutritionDetailViewController.swift
//  Nourish
//
//  Created by Vlad Birukov on 11.08.17.
//  Copyright © 2017 SSIC. All rights reserved.
//

import UIKit

class NutritionDetailViewController: BaseCardViewController {
    @IBOutlet weak var nutrienDetailInformationView: NutrientDetailInformationView!
    @IBOutlet weak var cardNavigationBar: NewCardNavigationBarView!
    
    var nutrientViewModel: NutrientViewModel?
    var nutrientDetailInformationViewModel: NutrientDetailInformationViewModel?
    var titleCaption = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.baseViews = [cardNavigationBar, nutrienDetailInformationView]
        
        for v in self.baseViews! {
            v.viewDidLoad()
        }
        
        if nutrientDetailInformationViewModel != nil &&  nutrientViewModel != nil {
            nutrienDetailInformationView.controller = self
            nutrienDetailInformationView.presenter.nutrienModel = nutrientDetailInformationViewModel!
            nutrienDetailInformationView.presenter.nutrienChartModel = nutrientViewModel!
        }

        
        if let config = self.progressConfig {
            nutrienDetailInformationView.progressConfig = config
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        setUpNavigationBar(title: self.titleCaption, style: .other,navigationBar: cardNavigationBar)
        super.viewWillAppear(animated)
    }
}

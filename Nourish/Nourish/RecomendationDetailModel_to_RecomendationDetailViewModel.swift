//
//  RecomendationDetailModel_to_RecomendationDetailViewModel.swift
//  Nourish
//
//  Created by Vlad Birukov on 29.08.17.
//  Copyright © 2017 SSIC. All rights reserved.
//

import Foundation


class RecomendationDetailModel_to_RecomendationDetailViewModel {
    func transform(model: RecomendationDetailModel) -> RecomendationDetailViewModel {
        let recomendationDetailViewModel = RecomendationDetailViewModel()
        
        recomendationDetailViewModel.titleText = model.titleText
        recomendationDetailViewModel.descriptionText = model.descriptionText
        if model.caption != nil {
            recomendationDetailViewModel.caption = model.caption
        }
        
        if model.data != nil {
            recomendationDetailViewModel.data = model.data
        }
        
        if let dataRecipies = model.dataRecipies {
            recomendationDetailViewModel.dataRecipies = [RecipiesViewModel]()
            recomendationDetailViewModel.dataRecipies = self.transform(model: dataRecipies)
        }
        return recomendationDetailViewModel
    }
    
    private func transform(model: [RecipiesModel]) -> [RecipiesViewModel] {
        var result = [RecipiesViewModel]()
        for recipies in model {
            let recipiesModel = RecipiesViewModel()
            recipiesModel.nameFood = recipies.nameFood
            
            if recipies.image != nil {
                recipiesModel.image = recipies.image
            }
            
            if recipies.groupId != nil {
                recipiesModel.groupId = recipies.groupId
            }
            
            recipiesModel.foodId = recipies.foodId
            recipiesModel.numberCal = recipies.numberCal
            recipiesModel.unit = recipies.unit
            recipiesModel.grams = recipies.grams
            recipiesModel.amount = recipies.amount
            recipiesModel.ingredient = recipies.ingredient
            recipiesModel.instructions = recipies.instructions
            recipiesModel.isFavorite = recipies.isFavorite
            result.append(recipiesModel)
        }
        return result
    }
}

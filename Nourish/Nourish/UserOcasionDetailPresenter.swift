//
//  UserOcasionDetailPresenter.swift
//  Nourish
//
//  Created by Vlad Birukov on 19.06.17.
//  Copyright © 2017 SSIC. All rights reserved.
//

import Foundation
import RxSwift


class  UserOcasionDetailPresenter: BasePresenter, UITableViewDataSource, UITableViewDelegate {
    let objectToModelMapper = CurrentStateFoodStuffs_to_CurrentStateFoodStuffsViewModel()
    let interactor = UserOcasionDetailInteractor()
    var userOcasionDetailView : UserOcasionDetailProtocol!
    var currentStateOcasionModel = CurrentStateFoodStuffsViewModel()
    var date: (Date, Date)?
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.getCuurentStateOcasionDetail()
    }
    
    private func getCuurentStateOcasionDetail() {
        if self.userOcasionDetailView.config() != nil {
            self.date = self.userOcasionDetailView.config()?.getDate()
        }
        
        if self.date != nil {
            self.subscribtions.append(self.interactor.execute(startDate: (self.date?.0)!, endDate: (self.date?.1)!).observeOn(MainScheduler.instance).subscribe(onNext: { [unowned self] ocasionDetail in
                self.currentStateOcasionModel = self.objectToModelMapper.transform(model: ocasionDetail)
                self.userOcasionDetailView.reloadTable()
                self.userOcasionDetailView.stopActivityAnimation()
            }, onError: {error in
                self.userOcasionDetailView.stopActivityAnimation()
                self.userOcasionDetailView.parseError(error : error, completion: nil)
            }, onCompleted: {
            }, onDisposed: {
                self.userOcasionDetailView.stopActivityAnimation()
            }))
        }
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return currentStateOcasionModel.arrCurentStateFood.count
    }

    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if section == 0 {
            return 83
        } else if currentStateOcasionModel.arrCurentStateFood[section].arrFoodElement == nil {
            return 50.0
        } else {
            return 40
        }
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let  headerCell = tableView.dequeueReusableCell(withIdentifier: "cellHeaderUserOcasion") as! HeaderUserOcasionTableViewCell
    
        if section == 0 {
            headerCell.topNewConstraint.isActive = true
            headerCell.topOldConstraint.isActive = false
            
            headerCell.layoutIfNeeded()
        }
        
        if currentStateOcasionModel.arrCurentStateFood.count - 1 == section && currentStateOcasionModel.arrCurentStateFood[section].arrFoodElement == nil {
            headerCell.separatorLine.backgroundColor = NRColorUtility.hexStringToUIColor(hex: "#EBEBF1")
        } else {
            headerCell.separatorLine.backgroundColor = UIColor.clear
        }
        
        let currentModel = currentStateOcasionModel.arrCurentStateFood[section]
        
        headerCell.setupWithModel(model: currentModel)
        
        return headerCell
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if  currentStateOcasionModel.arrCurentStateFood[section].arrFoodElement != nil {
            return (currentStateOcasionModel.arrCurentStateFood[section].arrFoodElement?.count)!
        } else {
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cellUserOcasion", for: indexPath) as! UserOcasionTableViewCell
        
        let currentModel = currentStateOcasionModel.arrCurentStateFood[indexPath.section].arrFoodElement?[indexPath.row]
        
        if indexPath.row == (currentStateOcasionModel.arrCurentStateFood[indexPath.section].arrFoodElement?.count)! - 1 {
            cell.bottomConstraint.constant = 30
        }

        
        cell.setupWithModel(model: currentModel!)
        
        return cell
    }
}

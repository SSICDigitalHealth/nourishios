//
//  WeightLossMeter.swift
//  Nourish
//
//  Created by Vino (Vinodha) Sundaramoorthy on 1/26/17.
//  Copyright © 2017 SSIC. All rights reserved.
//

import UIKit

let kPadding : CGFloat = 0.0

class WeightLossMeterView: BaseView,WeightLossMeterProtocol {

    var weightProgress : weightLossMeter!
    var needsReload : Bool = false
    var title : UILabel!
    var subTitle : UILabel!
    var status : UILabel!
    var statusDesc : UILabel!
    var consumedLabel : UILabel!
    var progressLabel : UILabel!
    var infoButton : UIButton!
    var yPosition : CGFloat = 0
    var bucketView : UIView!
    var progressViewArray : [(finalFrame: CGRect ,view:UIView,superView:UIView)] = []
    var overflowViewArray : [(finalFrame: CGRect ,view:UIView,superView:UIView)] = []
    var consumedProgressView : UIView!
    var burnedProgressView : UIView!
    var consumedOverflowView : UIView!
    var burnedOverflowView : UIView!
    var consumePointer : CGFloat = 0.0
    var burnedPointer : CGFloat = 0.0
    var totalConsumedBudget : Double = 0.0
    var totalBurnedBudget : Double = 0.0
    var presenter = WeightLossMeterPresenter()
    var controller : ProgressViewController!
    var bgColor :UIColor = UIColor.clear
    var drawShadow : Bool = true
    var burnedFillHeight : CGFloat = 0
    var consumedFillHeight : CGFloat = 0
    var isMetric : Bool? = false
    var goal : UserGoal!

    override func viewWillAppear(_ animated: Bool) {
        self.basePresenter = presenter
        presenter.weightLossView = self
        super.viewWillAppear(animated)
    }

    func overConsumed() -> Bool {
        if self.weightProgress.userConsumedCal > self.weightProgress.maxConsumedCal {
            return true
        }
        return false
    }
    
    override func draw(_ rect: CGRect) {
        if needsReload {
        yPosition = 10
        //Draw Labels
        self.backgroundColor = bgColor
        if drawShadow {
            self.addShadow()
        }
            
        if title != nil {
            title.removeFromSuperview()
            title = nil
        }
        
        if subTitle != nil {
            subTitle.removeFromSuperview()
            subTitle = nil
        }
        
        if bucketView !=  nil {
            bucketView.removeFromSuperview()
            bucketView = nil
        }
            
        progressViewArray = []
            
        self.totalConsumedBudget = weightProgress.maxConsumedCal
        if weightProgress.userConsumedCal > weightProgress.maxConsumedCal && weightProgress.userBurnedCal > weightProgress.maxBurnedCal {
            let calorieOverflow = weightProgress.userBurnedCal - weightProgress.maxBurnedCal
            let burnedOverflow = weightProgress.userConsumedCal - weightProgress.maxConsumedCal
            let overflow =  calorieOverflow > burnedOverflow ? calorieOverflow - burnedOverflow : 0
            self.totalConsumedBudget = weightProgress.userConsumedCal +  overflow
        } else if weightProgress.userConsumedCal > weightProgress.maxConsumedCal {
                self.totalConsumedBudget = weightProgress.userConsumedCal
        } else if weightProgress.userBurnedCal > weightProgress.maxBurnedCal {
            self.totalConsumedBudget = weightProgress.maxConsumedCal + (weightProgress.userBurnedCal - weightProgress.maxBurnedCal)
        }
        
        self.totalBurnedBudget = weightProgress.maxBurnedCal
        
        if weightProgress.userBurnedCal > weightProgress.maxBurnedCal {
            if weightProgress.userConsumedCal > weightProgress.maxConsumedCal {
                let calorieOverflow = weightProgress.userBurnedCal - weightProgress.maxBurnedCal
                let burnedOverflow = weightProgress.userConsumedCal - weightProgress.maxConsumedCal
                let overflow =  burnedOverflow > calorieOverflow ? burnedOverflow - calorieOverflow : 0
                self.totalBurnedBudget = weightProgress.userBurnedCal + overflow
            } else {
                self.totalBurnedBudget = weightProgress.userBurnedCal
            }
        } else if weightProgress.userConsumedCal > weightProgress.maxConsumedCal {
            self.totalBurnedBudget = weightProgress.maxBurnedCal + (weightProgress.userConsumedCal - weightProgress.maxConsumedCal)
        }
        
        if  title == nil {
            title = UILabel(frame: CGRect(x: 10, y: yPosition, width: 300, height: 22))
            title.text = "Caloric Progress"
            title.font = UIFont.systemFont(ofSize: 15)
            title.textColor = NRColorUtility.progressLabelColor()
            self.addSubview(title)
            yPosition = yPosition + title.frame.height + kPadding
        }
        
        if subTitle == nil && self.goal == .LooseWeight {
            subTitle = UILabel(frame: CGRect(x: 10, y: yPosition , width: 300, height: 15))
            
            let weeklyPlan = isMetric! ? self.weightProgress.weeklyPlan : NRConversionUtility.kiloToPounds(valueInKilo: self.weightProgress.weeklyPlan)
            let weeklyUnit = isMetric! ? "kg" : "lbs"
            subTitle.text = String(format : "You are on a %.2f %@/week weight loss plan",  weeklyPlan,weeklyUnit)
            subTitle.font = UIFont.systemFont(ofSize: 12)
            subTitle.textColor = NRColorUtility.reportLabelColor()
            self.addSubview(subTitle)
            yPosition = yPosition + subTitle.frame.height + 15
        } else {
            yPosition = yPosition + 30
            }
        
        //Draw Bucket View
        if bucketView == nil {
            let bucketWidth = UIScreen.main.bounds.width/2 - 70
            let viewWidth = UIScreen.main.bounds.width - 30
            bucketView = UIView(frame: CGRect(x: 10, y: yPosition , width: viewWidth, height: 200))
            bucketView.backgroundColor = UIColor.white
            bucketView.layer.cornerRadius = 4
            bucketView.center = CGPoint(x: self.frame.width/2, y: self.frame.height/2)
            bucketView.layer.borderColor = NRColorUtility.bucketBorderColor().cgColor
            
            //Add Consumed bucket
            let consumedView = UIView(frame:CGRect(x:15 , y: 20, width: bucketWidth, height: 150))

            consumedView.backgroundColor = UIColor.white
            consumedView.layer.borderColor = NRColorUtility.bucketBorderColor().cgColor
            consumedView.layer.borderWidth = 1
            bucketView.addSubview(consumedView)
            
            let consumedLabel = UILabel(frame:CGRect(x:20 , y: consumedView.frame.origin.y + consumedView.frame.height + 10 , width: 75, height: 22))
            consumedLabel.text = "Consumed"
            consumedLabel.textColor = NRColorUtility.reportLabelColor()
            consumedLabel.font = UIFont.systemFont(ofSize: 12)
            consumedLabel.center = CGPoint(x:consumedView.frame.origin.x + consumedView.frame.width/2, y:consumedView.frame.origin.y + consumedView.frame.height + 10)
            consumedLabel.textAlignment = .center

            bucketView.addSubview(consumedLabel)
            
            //Add BurnedBucket
            let burnedView = UIView(frame:CGRect(x:bucketView.frame.width/2 , y: 20, width: bucketWidth, height: 150))

            burnedView.backgroundColor = UIColor.white
            burnedView.layer.borderWidth = 1
            burnedView.layer.borderColor = NRColorUtility.bucketBorderColor().cgColor
            bucketView.addSubview(burnedView)
            
            let burnedLabel = UILabel(frame:CGRect(x:burnedView.frame.origin.x + 5 , y: burnedView.frame.origin.y + burnedView.frame.height + 5  , width: 50, height: 22))
            burnedLabel.text = "Burned"
            burnedLabel.textAlignment = .center
            burnedLabel.center = CGPoint(x: burnedView.frame.origin.x + burnedView.frame.width/2, y: burnedView.frame.origin.y + burnedView.frame.height + 10)
            burnedLabel.textColor = NRColorUtility.reportLabelColor()
            burnedLabel.font = UIFont.systemFont(ofSize: 12)
            bucketView.addSubview(burnedLabel)
            
            self.addSubview(bucketView)
            yPosition = yPosition + bucketView.frame.height + kPadding

            //Add progress
            if weightProgress != nil {
                
                let consumeFill = self.caluclateProgressFill(value: weightProgress.maxConsumedCal, view: consumedView, maxValue: totalConsumedBudget)

                self.consumePointer = consumedView.frame.origin.y + (consumedView.frame.height - consumeFill)
                let burnedFill = self.caluclateProgressFill(value: weightProgress.maxBurnedCal, view: burnedView, maxValue: totalBurnedBudget)
                self.burnedPointer = burnedView.frame.origin.y + (burnedView.frame.height - burnedFill)

                if weightProgress.userConsumedCal > 0 {
                    //Fill consumed bucket
                    if weightProgress.userConsumedCal <= weightProgress.maxConsumedCal {
                        //Add progress
                        let fill = self.caluclateProgressFill(value: weightProgress.userConsumedCal, view: consumedView, maxValue: totalConsumedBudget)
                        
                        let finalFrame = CGRect(x: 0, y: consumedView.frame.height - fill, width: consumedView.frame.width, height: fill)
                        let initialFrame = CGRect(x: 0, y: consumedView.frame.height, width: consumedView.frame.width, height: 0)
                        self.consumedProgressView = UIView(frame: initialFrame)
                        self.consumedProgressView.backgroundColor = NRColorUtility.comsumedColor()
                        progressViewArray.append((finalFrame: finalFrame , view: self.consumedProgressView,superView:consumedView))
                        self.consumedFillHeight = finalFrame.height
                    } else {
                        let fill = self.caluclateProgressFill(value: weightProgress.maxConsumedCal, view: consumedView, maxValue: totalConsumedBudget)
                        
                        let finalFrame = CGRect(x: 0, y: consumedView.frame.height - fill, width: consumedView.frame.width, height: fill)
                        let initialFrame = CGRect(x: 0, y: consumedView.frame.height, width: consumedView.frame.width, height: 0)
                        self.consumedProgressView = UIView(frame: initialFrame)
                        self.consumedProgressView.backgroundColor = NRColorUtility.comsumedColor()
                        progressViewArray.append((finalFrame: finalFrame , view: self.consumedProgressView,superView:consumedView))
                        
                        //Draw overflow
                        let overflowfill = self.caluclateProgressFill(value: (weightProgress.userConsumedCal - weightProgress.maxConsumedCal), view: consumedView, maxValue: totalConsumedBudget)
                        let overflowfinalFrame = CGRect(x: 0, y: consumedView.frame.height - (overflowfill + fill), width: consumedView.frame.width, height: overflowfill)
                        let overflowInitalFrame = CGRect(x: 0, y: consumedView.frame.height -  fill, width: consumedView.frame.width, height: 0)
                        self.consumedOverflowView = UIView(frame: overflowInitalFrame)
                        self.consumedOverflowView.backgroundColor = NRColorUtility.overconsumedColor()
                        self.overflowViewArray.append((finalFrame: overflowfinalFrame , view: self.consumedOverflowView,superView:consumedView))
                        self.consumedFillHeight = finalFrame.height + overflowfinalFrame.height
                    }
                }
                
                if weightProgress.userBurnedCal > 0 {
                    if weightProgress.userBurnedCal <= weightProgress.maxBurnedCal {
                        let burnedFill = self.caluclateProgressFill(value: weightProgress.userBurnedCal, view: burnedView, maxValue: totalBurnedBudget)
                        let fFrame = CGRect(x: 0, y: burnedView.frame.height - burnedFill, width: burnedView.frame.width, height: burnedFill)
                        let iFrame = CGRect(x: 0, y: burnedView.frame.height, width: consumedView.frame.width, height: 0)
                        self.burnedProgressView = UIView(frame: iFrame)
                        self.burnedProgressView.backgroundColor = NRColorUtility.activityColor()
                        progressViewArray.append((finalFrame: fFrame , view: self.burnedProgressView,superView:burnedView))
                        burnedFillHeight = fFrame.height

                    } else {
                        let burnedFill = self.caluclateProgressFill(value: weightProgress.maxBurnedCal, view: burnedView, maxValue: totalBurnedBudget)
                        let fFrame = CGRect(x: 0, y: burnedView.frame.height - burnedFill, width: burnedView.frame.width, height: burnedFill)
                        let iFrame = CGRect(x: 0, y: burnedView.frame.height, width: consumedView.frame.width, height: 0)
                        self.burnedProgressView = UIView(frame: iFrame)
                        self.burnedProgressView.backgroundColor = NRColorUtility.activityColor()
                        progressViewArray.append((finalFrame: fFrame , view: self.burnedProgressView,superView:burnedView))
                        
                        //Burned overflow
                        let overflowfill = self.caluclateProgressFill(value: (weightProgress.userBurnedCal - weightProgress.maxBurnedCal), view: consumedView, maxValue: totalBurnedBudget)
                        let overflowfinalFrame = CGRect(x: 0, y: consumedView.frame.height - (overflowfill + burnedFill), width: consumedView.frame.width, height: overflowfill)
                        let overflowInitalFrame = CGRect(x: 0, y: consumedView.frame.height -  burnedFill, width: consumedView.frame.width, height: 0)
                        self.burnedOverflowView = UIView(frame: overflowInitalFrame)
                        self.burnedOverflowView.backgroundColor = NRColorUtility.activityExceedColor()
                        self.overflowViewArray.append((finalFrame: overflowfinalFrame , view: self.burnedOverflowView,superView:burnedView))
                        burnedFillHeight = fFrame.height + overflowfinalFrame.height
                    }
                }
                
                //Consumed bucket Image labels
                let consumedImage = UIImageView(frame: CGRect(x: consumedView.frame.width/2 - 10, y: consumedView.frame.height/2, width: 17, height: 20))
                consumedImage.center = CGPoint(x: consumedView.frame.width/2, y: consumedView.frame.height/2)
                consumedImage.image = UIImage(named: "progress_consume_icon")
                consumedView.addSubview(consumedImage)
                
                //Calories Consumed Label
                let consumedCalories = UILabel(frame: CGRect(x: 0, y: consumedImage.frame.origin.y + consumedImage.frame.height + 5, width: consumedView.frame.width, height: 15))

                var intersectionFrame: CGRect = CGRect(x: consumedCalories.frame.origin.x, y: consumedCalories.frame.origin.y, width: consumedCalories.frame.size.width, height: consumedCalories.frame.size.height - 10)
                
                let intersectionFrameForBucket = CGRect(x: 0, y: 0, width: consumedView.frame.size.width, height: self.consumedFillHeight + consumedView.frame.origin.y)
                consumedCalories.textColor = intersectionFrameForBucket.intersects(intersectionFrame) ? UIColor.white : UIColor.gray

                consumedCalories.textAlignment = .center
                consumedCalories.font = UIFont.systemFont(ofSize: 10)
                consumedCalories.text = String(format:"%0.f",weightProgress.userConsumedCal)
                consumedView.addSubview(consumedCalories)
                
                //Burned Buket Image labels
                let burnedImage = UIImageView(frame: CGRect(x: burnedView.frame.width/2 - 10, y: burnedView.frame.height/2, width: 17, height: 20))
                burnedImage.center = CGPoint(x: burnedView.frame.width/2, y: burnedView.frame.height/2)
                burnedImage.image = UIImage(named: "progress_burn_icon")
                burnedView.addSubview(burnedImage)
                
                //Burned Calories Label
                let burnedCalories = UILabel(frame: CGRect(x: 0, y: burnedImage.frame.origin.y + burnedImage.frame.height + 5, width: burnedView.frame.width, height: 15))

                intersectionFrame = CGRect(x: burnedCalories.frame.origin.x, y: burnedCalories.frame.origin.y, width: burnedCalories.frame.size.width, height: burnedCalories.frame.size.height - 10)
                let intersectionFrameForBurned = CGRect(x: 0, y: 0, width: burnedView.frame.size.width, height: self.burnedFillHeight + burnedView.frame.origin.y)

                burnedCalories.textColor =  intersectionFrameForBurned.intersects(intersectionFrame) ? UIColor.white : UIColor.gray
                burnedCalories.textAlignment = .center
                burnedCalories.font = UIFont.systemFont(ofSize: 10)
                burnedCalories.text = String(format:"%0.f",weightProgress.userBurnedCal)
                burnedView.addSubview(burnedCalories)
                
                // Status tile labels
                let overConsumption = self.overConsumed()
                
                if infoButton == nil {
                    infoButton = UIButton(frame: CGRect(x: 10, y: yPosition, width: 20, height: 20))
                    infoButton.setImage(UIImage(named: "info_icon"), for: .normal)
                    infoButton.addTarget(self, action: #selector(self.showBudgetInfo(sender:)), for: .touchUpInside)
                    self.addSubview(infoButton)
                }

                if status == nil {
                    status = UILabel(frame: CGRect(x: infoButton.frame.origin.x + infoButton.frame.width + 10, y: yPosition, width: 300, height: 22))
                    status.text = overConsumption ? "Needs Improvement" : "Keep it up!"
                    status.font = UIFont.systemFont(ofSize: 12)
                    status.textColor = overConsumption ? NRColorUtility.overconsumedColor() : NRColorUtility.healthGreenColor()
                    yPosition  = yPosition  + status.frame.height
                    self.addSubview(status)
                } else {
                    status.text = overConsumption ? "Needs Improvement" : "Keep it up!"
                    status.textColor = overConsumption ? NRColorUtility.overconsumedColor() : NRColorUtility.healthGreenColor()

                }
                
                if statusDesc == nil {
                    statusDesc = UILabel(frame: CGRect(x: infoButton.frame.origin.x + infoButton.frame.width + 10, y: yPosition, width: 300, height: 25))
                    statusDesc.text = overConsumption ? String(format:"Your daily budget was %0.f cal\nYou exceeded by %0.f cal",weightProgress.maxConsumedCal,weightProgress.userConsumedCal - weightProgress.maxConsumedCal)  : "You are doing great!"
                    statusDesc.numberOfLines = 0
                    statusDesc.font = UIFont.systemFont(ofSize: 10)
                    statusDesc.textColor = NRColorUtility.reportLabelColor()
                    self.addSubview(statusDesc)
                } else {
                    statusDesc.text = overConsumption ? String(format:"Your daily budget was %0.f cal\nYou exceeded by %0.f cal",weightProgress.maxConsumedCal,weightProgress.userConsumedCal - weightProgress.maxConsumedCal)  : "You are doing great!"
                }
            }
            
            let finalConsumedFrame = CGRect(x: consumedView.frame.origin.x + consumedView.frame.width - 9, y: self.consumePointer - 9, width: 45, height: 18)

            let initialConsumedFrame = CGRect(x: consumedView.frame.origin.x + consumedView.frame.width - 9, y: consumedView.frame.height + 9, width: 45, height: 18)

            let consumedPointer = UIImageView(frame: initialConsumedFrame)
            consumedPointer.image = UIImage(named: "pointer_bkgd")
            
            let consumedBudgetLabel = UILabel(frame: initialConsumedFrame)
                consumedBudgetLabel.text = String(format:"%0.f",weightProgress.maxConsumedCal)
            consumedBudgetLabel.font = UIFont.systemFont(ofSize: 10)
            consumedBudgetLabel.textColor = NRColorUtility.reportLabelColor()
            consumedBudgetLabel.center = consumedPointer.center
            consumedBudgetLabel.textAlignment = .center
            
            let finalBurnedFrame = CGRect(x: burnedView.frame.origin.x + burnedView.frame.width - 9, y: self.burnedPointer - 9, width: 45, height: 18)
            let initialBurnedFrame = CGRect(x: burnedView.frame.origin.x + burnedView.frame.width - 9, y: burnedView.frame.height + 9, width: 45, height: 18)

            let burnedPointer = UIImageView(frame: initialBurnedFrame)
            burnedPointer.image = UIImage(named: "pointer_bkgd")
                        let burnedBudgetLabel = UILabel(frame: initialBurnedFrame)
            burnedBudgetLabel.text = String(format:"%0.f",weightProgress.maxBurnedCal)
            burnedBudgetLabel.font = UIFont.systemFont(ofSize: 10)
            burnedBudgetLabel.textColor = NRColorUtility.reportLabelColor()
            burnedBudgetLabel.textAlignment = .center
            burnedBudgetLabel.center = burnedPointer.center
            
            UIView.animate(withDuration: 1, delay: 0.2, options: .curveEaseIn, animations: {
               // pointer
                burnedPointer.frame = finalBurnedFrame
                burnedBudgetLabel.frame = finalBurnedFrame
                consumedPointer.frame = finalConsumedFrame
                consumedBudgetLabel.frame = finalConsumedFrame
                
                //consumed view
                for view in self.progressViewArray {
                    view.view.frame = view.finalFrame
                }
            }, completion: nil)
            
            bucketView.addSubview(burnedPointer)
            bucketView.addSubview(burnedBudgetLabel)
            bucketView.addSubview(consumedPointer)
            bucketView.addSubview(consumedBudgetLabel)
            
            for view in self.progressViewArray {
                view.superView.insertSubview(view.view, at: 0)
            }
            
            UIView.animate(withDuration: 1, delay: 1, options: .curveEaseIn, animations: {
                //overflow views
                for view in self.overflowViewArray {
                    view.view.frame = view.finalFrame
                }
            }, completion: nil)
            
            for view in self.overflowViewArray {
                view.superView.insertSubview(view.view, at: 0)
            }
         }
        }
    }

    func caluclateProgressFill(value : Double ,view:UIView , maxValue:Double) -> CGFloat {
        var fillValue : CGFloat = 15.0
        let maxHeight = Double(view.frame.size.height)
        
        if value > 0 {
            fillValue = CGFloat((value * maxHeight) / maxValue)
        }
        
        return fillValue
    }
    
    func showBudgetInfo(sender:UIButton) {
        if controller != nil {
            controller.showCalorieMeterInfo(activity:(controller.userProfile.activityLevel?.description)! , budgetCal:self.weightProgress.maxConsumedCal , budgetActivity:self.weightProgress.maxBurnedCal)
        }
    }
    // MARK: WeightLossMeterProtocol Implementation
    func setMeterModel(model:weightLossMeter,bgColor:UIColor, isMetric : Bool , goal:UserGoal) {
        self.weightProgress =  model
        self.isMetric = isMetric
        self.bgColor = bgColor
        self.needsReload = true
        self.goal = goal
        self.stopActivityAnimation()
        self.setNeedsDisplay()
    }
}

//
//  HydrationDetailInteractor.swift
//  Nourish
//
//  Created by Vlad Birukov on 16.06.17.
//  Copyright © 2017 SSIC. All rights reserved.
//

import Foundation
import RxSwift

class HydrationDetailInteractor {
    
    let repository = NestleProgressRepository.shared
    
    func execute(startDate: Date, endDate: Date) -> Observable<HydrationBalanceModel> {
        return repository.getProgressFor(startDate: startDate, endDate: endDate, returnHistory: true).flatMap({observer -> Observable<HydrationBalanceModel> in
            return Observable.just(self.transform(model: observer.userMicroelement))
        })
    }
    
    private func transform(model: [MicroelementInformation]) -> HydrationBalanceModel {
        let hydrationBalanceModel = HydrationBalanceModel()
        
        let hydrationInformation = model.filter({$0.idNameMicroelement == "Water_total"})
        if hydrationInformation.count > 0 {
            hydrationBalanceModel.unit = "cup"
            hydrationBalanceModel.message = hydrationInformation.first?.message ?? ""
            if hydrationInformation[0].contributors.count > 0 {
                hydrationBalanceModel.arrHydrationElement = [ElementModel]()
                for i in 0..<hydrationInformation[0].contributors.count {
                    let elementModel = ElementModel()
                    elementModel.amountElement = hydrationInformation[0].contributors[i].quality
                    elementModel.consumedState = hydrationInformation[0].contributors[i].consumed /  hydrationInformation[0].numDays
                    elementModel.nameElement = hydrationInformation[0].contributors[i].nameFood
                    elementModel.unit = hydrationInformation[0].contributors[i].unit
                    hydrationBalanceModel.arrHydrationElement?.append(elementModel)
                }
            }
        }
        
        return self.transform(model: hydrationBalanceModel) 
    }
    
    private func transform(model: HydrationBalanceModel) -> HydrationBalanceModel {
        let hydrationBalanceModel = HydrationBalanceModel()
        hydrationBalanceModel.message = model.message
        hydrationBalanceModel.unit = model.unit
        if let hydrationElement =  model.arrHydrationElement {
            hydrationBalanceModel.total = NRFormatterUtility.roundCupElements(number: (hydrationElement.reduce(0, {$0 + $1.consumedState!})))
            
            hydrationBalanceModel.arrHydrationElement = [ElementModel]()
            hydrationBalanceModel.arrHydrationElement = self.repository.formationTopFood(elementModel: self.repository.groupElements(array: hydrationElement), round: true)
            
            let sumConsumedShowCup = (hydrationBalanceModel.arrHydrationElement?.reduce(0, {$0 + $1.consumedState!}))!
            let otherElement = hydrationBalanceModel.arrHydrationElement?.filter({$0.nameElement == "Others"})

            if hydrationBalanceModel.total > sumConsumedShowCup {
                let differenceWithRound = hydrationBalanceModel.total - sumConsumedShowCup
                
                if (otherElement?.count)! > 0 {
                    hydrationBalanceModel.arrHydrationElement?[(hydrationBalanceModel.arrHydrationElement?.count)! - 1].consumedState! += differenceWithRound
                } else {
                    hydrationBalanceModel.arrHydrationElement?[0].consumedState! += differenceWithRound
                }
            } else {
                let differenceWithRound = sumConsumedShowCup - hydrationBalanceModel.total

                if (otherElement?.count)! > 0 {
                    if (otherElement?[0].consumedState!)! > differenceWithRound {
                        hydrationBalanceModel.arrHydrationElement?[(hydrationBalanceModel.arrHydrationElement?.count)! - 1].consumedState! -= differenceWithRound
                    } else {
                        hydrationBalanceModel.arrHydrationElement?[0].consumedState! -= differenceWithRound
                    }
                } else {
                    hydrationBalanceModel.arrHydrationElement?[0].consumedState! -= differenceWithRound
                }
            }
        }
        
        return hydrationBalanceModel
    }
}

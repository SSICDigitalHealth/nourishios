//
//  ProgressMicroelementView.swift
//  Nourish
//
//  Created by Vlad Birukov on 09.06.17.
//  Copyright © 2017 SSIC. All rights reserved.
//

import UIKit
import QuartzCore

protocol ProgressMicroelementProtocol {
    func setupWith(microelementConsumed: Double, microelementTarget: Double)
    func drawArc(value: Double,startAngle: CGFloat,color: UIColor, flagCircle: Bool)
    func drawOverflowArc(center: CGPoint,value: Double, startAngle: CGFloat, color: UIColor)
}

class ProgressMicroelementView: UIView, ProgressMicroelementProtocol {
    
    let circleColor = NRColorUtility.hexStringToUIColor(hex: "F3F3F3")
    let microelementColor = NRColorUtility.hexStringToUIColor(hex: "6BC497")
    let redColor = NRColorUtility.hexStringToUIColor(hex: "F33E3B")
    let arcWidth : CGFloat = 1
    
    var microelementValue: Double = 0.0

    func setupWith(microelementConsumed: Double, microelementTarget: Double) {
        
        if microelementTarget != 0.0 {
            self.microelementValue = microelementConsumed/microelementTarget
        } else  if microelementConsumed != 0.0 {
            self.microelementValue = 2.0 + microelementConsumed
        }
        
        self.setNeedsDisplay()
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
    }

    override func draw(_ rect: CGRect) {
        
        drawArc(value: 0, startAngle: CGFloat(0).degreesToRadians, color: circleColor, flagCircle: true)
        drawArc(value: microelementValue, startAngle: CGFloat(0).degreesToRadians, color: microelementColor, flagCircle: false)
        self.layer.cornerRadius = bounds.width/2
        self.layer.masksToBounds = true
    }

    func drawArc(value: Double = 0,startAngle: CGFloat,color: UIColor, flagCircle: Bool) {
        let center = CGPoint(x: bounds.width/2, y: bounds.height/2)
        let radius: CGFloat = bounds.width/4 - arcWidth
        let newStartAngle = startAngle - CGFloat(90).degreesToRadians
        var endAngle = CGFloat(360 * value).degreesToRadians - CGFloat(90).degreesToRadians

        if flagCircle == true {
            endAngle = CGFloat(360) - CGFloat(90).degreesToRadians
        }
        
        let arc = UIBezierPath(arcCenter: center, radius: radius, startAngle: newStartAngle, endAngle: endAngle, clockwise: true)
        
        arc.lineWidth = radius * 2
        color.setStroke()
        arc.stroke()
        
        if value > 1{
            drawOverflowArc(center: center, value: value, startAngle: newStartAngle, color: redColor)
        }
    }
    
    func drawOverflowArc(center: CGPoint,value: Double, startAngle: CGFloat, color: UIColor) {
        let radius = bounds.width/2 - arcWidth
        var endAngle = CGFloat(360)
        if value > 1 && value < 2{
            endAngle = CGFloat(360 * value.truncatingRemainder(dividingBy: 1)).degreesToRadians - CGFloat(90).degreesToRadians
        }
        let overflowArc = UIBezierPath(arcCenter: center, radius: radius, startAngle: startAngle, endAngle: endAngle, clockwise: true)
        
        overflowArc.lineWidth = arcWidth * 3
        color.setStroke()
        overflowArc.stroke()

    }

}

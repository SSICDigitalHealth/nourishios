//
//  NestleProgressDataStore.swift
//  Nourish
//
//  Created by Vlad Birukov on 26.06.17.
//  Copyright © 2017 SSIC. All rights reserved.
//

import Foundation
import RxSwift

let nestleUrl = kNourishApiAddress + "assessment/progress"

//params
let kNestleToken = "?token="
let kNestleUserID = "&userid="
let kNestleLocalTime = "&local_time="
let kNestleStartDay = "&start_day="
let kNestleEndDay = "&end_day="
let kNestleRangeDays = "&range_days="
let kNestleReturnBreakdown = "&return_breakdown="
let kNestleReturnHistory = "&return_history="
class NestleProgressDataStore: BaseNestleDataStore {
 
    func fetchProgress(token: String, userID: String, startDate: Date, endDate: Date,includingRange : Bool, rangeDays : Int, returningBreakDown : Bool, returningHistory : Bool) -> Observable<Dictionary<String, Any>> {
        return Observable.create { observer in
            
            let url = self.getNestleUrl(token: token, userId: userID, startDate: startDate, endDate: endDate, includingRange: includingRange, rangeDays: rangeDays, returningBreakDown: returningBreakDown, returningHistory: returningHistory)
            var request = self.requestWithToken(token: token, url: url)//URLRequest(url: url)
            request.httpMethod = "GET"
            let session = URLSession.shared.dataTask(with: request) {
                data, response, error in
                
                if error == nil && data != nil {
                    let dataStr = String(data : data!, encoding : .utf8)
                    if let dictionary = self.convertStringToDictionary(text: dataStr!) {
                        observer.onNext(dictionary)
                    }
                } else {
                    if error != nil {
                        observer.onError(error!)
                    }
                }
                
                self.log(foodProcessingOrWithings: false, url: url.absoluteString, requestMethod: request.httpMethod, urlResponse: response, requestBody: nil, error: error, responceBody: nil)
                observer.onCompleted()
            }
            session.resume()
            
            return Disposables.create {
                session.cancel()
            }
        }
    }
    
    private func getNestleUrl(token : String, userId : String, startDate : Date, endDate : Date,includingRange : Bool, rangeDays : Int, returningBreakDown : Bool, returningHistory : Bool) -> URL {
        var stringUrl = nestleUrl + "?userid=" + userId

        //stringUrl = self.addAuthorizationHeader(url: stringUrl, token: token)
        stringUrl = self.addLocalTime(stringUrl: stringUrl, local: kNestleLocalTime)
        stringUrl = self.appendTimeToStringWith(url: stringUrl, startDate: startDate, endDate: endDate, includingRange: includingRange, range: rangeDays, returnBreakdown: returningBreakDown, returnHistory: returningHistory)
        stringUrl = self.addCalorieBudgetToUrl(url: stringUrl)
        return URL(string: stringUrl)!
    }
    
 
    private func appendTimeToStringWith(url : String, startDate : Date, endDate : Date, includingRange : Bool,range: Int, returnBreakdown: Bool, returnHistory : Bool) -> String {
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyyMMdd"
        let startInterval = formatter.string(from: startDate)
        let endInterval = formatter.string(from: endDate)
        //let rangeString = includingRange ? "\(kNestleRangeDays)\(range)" : ""
        let breakDownString = returnBreakdown ? "\(kNestleReturnBreakdown)\("true")" : "\(kNestleReturnBreakdown)\("false")"
        let historyString = returnHistory ? "\(kNestleReturnHistory)\("true")" : "\(kNestleReturnHistory)\("false")"
        let stringToReturn = url.appending(kNestleStartDay + startInterval + kNestleEndDay + endInterval + breakDownString + historyString)
        return stringToReturn
    }
    
    private func addLocalTime(stringUrl : String, local : String) -> String {
        let url = stringUrl + local + self.getLocalTimeString()
        return url
    }
    
    private func getLocalTimeString() -> String {
        let formatter = DateFormatter()
        formatter.dateFormat = "HHmmss"
        let localTime = formatter.string(from: Date())
        return localTime
    }
    
    private func addAuthorizationHeader(url : String, token : String) -> String {
        let authorizedUrl = url.appending(kNestleToken + token)
        return authorizedUrl
    }
    
    
    private func addCalorieBudgetToUrl(url : String) -> String {
        if defaults.object(forKey: kCalorieBudgetKey) != nil {
            let calorieString = String(format: "&calorie_consumption_goal=%0.f", defaults.double(forKey: kCalorieBudgetKey))
            let authorizedUrl = url.appending(calorieString)
            return authorizedUrl
        } else {
            return url
        }
    }
    
    private func convertStringToDictionary(text: String) -> [String : Any]? {
        if let data = text.data(using: String.Encoding.utf8) {
            do {
                return try JSONSerialization.jsonObject(with: data, options: []) as? [String : Any]
            } catch let error as NSError {
                LogUtility.logToFile(error)
            }
        }
        return nil
    }
}

//
//  NutrientTokenProtocol.swift
//  Nourish
//
//  Created by Vino (Vinodha) Sundaramoorthy on 12/18/16.
//  Copyright © 2016 SSIC. All rights reserved.
//

import UIKit

protocol NutrientTokenProtocol {
    func setUpNutrient(tokens : [nutrients], period:period)
}

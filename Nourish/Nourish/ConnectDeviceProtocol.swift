//
//  ConnectDeviceProtocol.swift
//  Nourish
//
//  Created by Vino (Vinodha) Sundaramoorthy on 5/18/17.
//  Copyright © 2017 SSIC. All rights reserved.
//

import UIKit

protocol ConnectDeviceProtocol : BaseViewProtocol {
    func loadDevices()
    func openWithingsLogin()
    func openFitBitLogin()
}

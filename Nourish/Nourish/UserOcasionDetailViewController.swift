//
//  UserOcasionDetailViewController.swift
//  Nourish
//
//  Created by Vlad Birukov on 19.06.17.
//  Copyright © 2017 SSIC. All rights reserved.
//

import UIKit

class UserOcasionDetailViewController: BaseCardViewController {

    @IBOutlet weak var userOcasionDetailView: UserOcasionDetailView!
    @IBOutlet weak var cardNavigationBar: NewCardNavigationBarView!

    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.baseViews = [cardNavigationBar, userOcasionDetailView]
        for v in self.baseViews! {
            v.viewDidLoad()
        }
        
        if let config = self.progressConfig {
            userOcasionDetailView.progressConfig = config
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        setUpNavigationBar(title: "Logged so far", style: .other, navigationBar: cardNavigationBar)
        super.viewWillAppear(animated)
    }
}

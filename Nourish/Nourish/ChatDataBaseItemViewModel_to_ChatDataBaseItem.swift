//
//  ChatDataBaseItemViewModel_to_ChatDataBaseItem.swift
//  Nourish
//
//  Created by Gena Mironchyk on 8/1/17.
//  Copyright © 2017 SSIC. All rights reserved.
//

import UIKit

class ChatDataBaseItemViewModel_to_ChatDataBaseItem: NSObject {
    func transform(viewModel : ChatDataBaseItemViewModel) -> ChatDataBaseItem {
        let item = ChatDataBaseItem()
        item.date = viewModel.date
        item.chatBoxType = viewModel.chatBoxType
        item.content = viewModel.content
        item.isFromUser = viewModel.isFromUser
        item.messageID = viewModel.messageID
        item.userID = viewModel.userID
        item.lostWeightValue = viewModel.lostWeightValue
        item.targetLostWeightValue = viewModel.targetLostWeightValue
        item.photoID = viewModel.photoID
        return item
    }
    
    func transform(viewModels : [ChatDataBaseItemViewModel]) -> [ChatDataBaseItem] {
        var arrayToReturn : [ChatDataBaseItem] = []
        for model in viewModels {
            arrayToReturn.append(self.transform(viewModel: model))
        }
        return arrayToReturn
    }
}

//
//  CameraSnapView.swift
//  Nourish
//
//  Created by Nova on 5/16/17.
//  Copyright © 2017 SSIC. All rights reserved.
//

import UIKit

class CameraSnapView: BaseView {

    var gradLayer : CAGradientLayer?
    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */
    @IBOutlet weak var presenter : CameraSnapPresenter!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.basePresenter = self.presenter
        self.presenter.cameraSnapView = self
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.gradLayer = self.addGradientWithHeight(height: 56)
        self.stopActivityAnimation()
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        self.gradLayer?.frame = CGRect.init(x: 0.0, y: 0.0, width: self.bounds.size.width, height: 56)
        self.presenter.layoutVideoPreviewLayer()
    }

}

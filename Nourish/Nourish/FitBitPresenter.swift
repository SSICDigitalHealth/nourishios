//
//  FitBitPresenter.swift
//  Nourish
//
//  Created by Vino (Vinodha) Sundaramoorthy on 7/20/17.
//  Copyright © 2017 SSIC. All rights reserved.
//

import UIKit
import SwiftKeychainWrapper

class FitBitPresenter: BasePresenter , UIWebViewDelegate {
    var fitBitView : FitBitWebViewProtocol!
    var fitBitViewController : FitBitViewControllerProtocol!
    let interactor = FitBitInteractor()
    var fitBitCacher = BackgroundFitBitCacher.shared

    override func viewWillAppear(_ animated: Bool) {
        self.deleteCookies()
        self.loadWebView()
    }
    
    private func deleteCookies() {
        URLCache.shared.removeAllCachedResponses()
        URLCache.shared.diskCapacity = 0
        URLCache.shared.memoryCapacity = 0
        
        if let cookies = HTTPCookieStorage.shared.cookies {
            for cookie in cookies {
                HTTPCookieStorage.shared.deleteCookie(cookie)
            }
        }
    }
    
    private func loadWebView () {
        let _ = self.interactor.pairedFitBitDevices().subscribe(onNext: { [unowned self] userDevices in
            if userDevices.count > 0 {
                let fitBitDevice = userDevices.first
                let _ = self.interactor.authurizationURLForFitBitDevice(device: fitBitDevice!).subscribe(onNext: { [unowned self] fitBitUrl in
                    let request = self.requestWithToken(token: NRUserSession.sharedInstance.accessToken ?? "", url: fitBitUrl)
                    self.fitBitView.loadRequest(request: request)
                }, onError: {error in}, onCompleted: {

                }, onDisposed: {})
            } else {
                let _ = self.interactor.createFitBitDevice().subscribe(onNext: { [unowned self] devices in
                    if devices.count > 0 {
                        let fitBitDevice = devices.first
                        let _ = self.interactor.authurizationURLForFitBitDevice(device: fitBitDevice!).subscribe(onNext: { [unowned self] fitBitUrl in
                            let request = self.requestWithToken(token: NRUserSession.sharedInstance.accessToken ?? "", url: fitBitUrl)
                            self.fitBitView.loadRequest(request: request)
                        }, onError: {error in}, onCompleted: {}, onDisposed: {})
                    }
                }, onError: {error in }, onCompleted: {
                  

                }, onDisposed: {})
            }
        }, onError: {error in}, onCompleted: {}, onDisposed: {})

    }
    
    func webView(_ webView: UIWebView, shouldStartLoadWith request: URLRequest, navigationType: UIWebViewNavigationType) -> Bool {
        let urlString =  String(describing: request)
        print(urlString)
        if urlString.contains("https://authcomplite") || urlString.contains("https://www.artik.cloud/") {
            webView.stopLoading()
            KeychainWrapper.standard.set("Fit Bit", forKey: kActivitySourceKey)
            self.fitBitCacher.startCaching()
            self.fitBitViewController.dismissFitBitView()
        }
        return true
    }
    
    func webViewDidFinishLoad(_ webView: UIWebView) {
        self.fitBitView.stopAnimation()
    }
    
    
    private func requestWithToken(token : String, url : URL) -> URLRequest {
        var request = URLRequest(url : url)
        request.setValue("Bearer \(token)", forHTTPHeaderField: kOAUTHAuthorizationHeader)
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        request.setValue("https://authcomplite", forHTTPHeaderField: "Referer")
        return request
    }
    
}

//
//  LoggedUserEatStateView.swift
//  Nourish
//
//  Created by Vlad Birukov on 09.06.17.
//  Copyright © 2017 SSIC. All rights reserved.
//

import UIKit

protocol LoggedUserEatStateProtocol : BaseViewProtocol {
    func reloadTable()
    func registerTableViewCells()
    func config() -> ProgressConfig?
}

class LoggedUserEatStateView: ProgressView, LoggedUserEatStateProtocol {

    @IBOutlet var loggedUserEatStateView: UIView!
    @IBOutlet weak var ocasionTableView: UITableView!
    var presenter: LoggedUserEatStatePresenter!
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.presenter.loggedUserEatStateView = nil
        self.presenter = nil
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.loggedUserEatStateView =  UINib(nibName: "LoggedUserEatStateView", bundle: nil).instantiate(withOwner: self, options: nil)[0] as! UIView
        self.loadContent(contentView: loggedUserEatStateView)
        self.loadWhiteThemeWith(header: "Logged so far")
        settingView()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.presenter = LoggedUserEatStatePresenter()
        self.basePresenter = self.presenter
        self.ocasionTableView.delegate = self.presenter
        self.ocasionTableView.dataSource = self.presenter
        self.presenter.loggedUserEatStateView = self
        self.registerTableViewCells()
        super.viewWillAppear(animated)
        self.removeActivityIndicator()
    }

    func reloadTable() {
        self.ocasionTableView.reloadData()
    }
    
    func registerTableViewCells() {
        self.ocasionTableView?.register(UINib(nibName: "OcasionTableViewCell", bundle: nil), forCellReuseIdentifier: "OcasionCell")
    }
    
    private func settingView() {
        self.contentHolderLeading.constant = 0
        self.contentHolderTrailing.constant = 0
    }
}

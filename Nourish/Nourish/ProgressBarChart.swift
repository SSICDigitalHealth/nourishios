//
//  ProgressBarChart.swift
//  Nourish
//
//  Created by Andrey Zmushko on 8/3/17.
//  Copyright © 2017 SSIC. All rights reserved.
//

import UIKit

protocol ProgressBarChartDelegate {

    func numberOfBars(in progressBarChart: ProgressBarChart) -> Int
    func progressBarChart(_ progressBarChart: ProgressBarChart, barAtIndex: Int) -> ProgressBarChart.Bar?

    func numberOfAxis(in progressBarChart: ProgressBarChart) -> Int
    func progressBarChart(_ progressBarChart: ProgressBarChart, axisAtIndex: Int) -> ProgressBarChart.Axis?
}

class ProgressBarChart: UIView {

    typealias Axis = (color: UIColor, type: AxisType, scaleValue: Double, aboveLabel: UIView?, belowLabel: UIView?)
    typealias Bar = (scaleValues: [CGFloat], colors: [UIColor], legenda: UIView?, legendaAlignment: LegendaAlignment)
    
    enum AxisType {
        case solid
        case dashed
    }

    enum LegendaAlignment {
        case left
        case center
    }
    
    var widthBarPattern: (CGFloat, CGFloat) = (0.5, 0)
    var delimiterMultiplier: CGFloat = 0.25
    var lineAxisWidth = CGFloat(0.75)
    var leftGap = CGFloat()
    var topGap = CGFloat()
    var bottomGap = CGFloat()
    var base = CGFloat()
    var axises = [Axis]()
    var bars = [Bar]()

    func reloadData(DataSource delegate: ProgressBarChartDelegate) {
        var maxHeightOfLegends = CGFloat()
        
        self.leftGap = 0
        self.topGap = 0
        self.bottomGap = 0
        self.base = 0
        self.axises.removeAll()
        self.bars.removeAll()
        
        if delegate.numberOfAxis(in: self) <= 0 {
            return
        }

        for view in self.subviews {
            view.removeFromSuperview()
        }
       
        for index in 0...(delegate.numberOfAxis(in: self) - 1) {
            if let axis: Axis = delegate.progressBarChart(self, axisAtIndex: index) {
                self.axises.append(axis)
                if let label = axis.aboveLabel {
                    self.addSubview(label)
                    label.sizeToFit()
                    self.leftGap.equalIfLess(label.bounds.width)
                }
                if let label = axis.belowLabel {
                    self.addSubview(label)
                    label.sizeToFit()
                    self.leftGap.equalIfLess(label.bounds.width)
                }
            }
        }
        
        func calculateTopGap(for axis: Axis, scaleFunc:(_: CGFloat) -> CGFloat ) -> CGFloat {
            var gap = CGFloat()
            if let label = axis.aboveLabel {
                label.sizeToFit()
                gap.equalIfLess(label.bounds.height)
            }
            let labelGap = self.bounds.height * scaleFunc(CGFloat(axis.scaleValue))
            gap.equalIfLess(labelGap)
            
            return gap
        }
        
        let axisesSorted = axises.sorted(by: {$0.scaleValue > $1.scaleValue})
        
        if let axis = axisesSorted.first {
            self.topGap = calculateTopGap(for: axis,
                                  scaleFunc: {1 - $0})
        }

        if let axis = axisesSorted.last {
            self.bottomGap = calculateTopGap(for: axis,
                                     scaleFunc: {$0})
        }
        
        self.bars.removeAll()
        for index in 0 ..< delegate.numberOfBars(in: self) {
            if let bar: Bar = delegate.progressBarChart(self, barAtIndex: index) {
                self.bars.append(bar)
                if let legenda = bar.legenda {
                    self.addSubview(legenda)
                    maxHeightOfLegends.equalIfLess(legenda.bounds.height)
                }
                
            }
        }
        
        self.bottomGap.equalIfLess(maxHeightOfLegends)
        self.setNeedsDisplay()
    }
    
    override func draw(_ rect: CGRect) {
        self.base = rect.size.height - self.bottomGap
        self.leftGap.equalIfLess(rect.size.width * self.delimiterMultiplier)

        func fixingLeftTopCorner(label: UIView, x: CGFloat, y: CGFloat) {
            label.sizeToFit()
            label.translatesAutoresizingMaskIntoConstraints = false
            NSLayoutConstraint(item: label, attribute: .top, relatedBy: .equal, toItem: self, attribute: .top, multiplier: 1.0, constant: y).isActive = true
            NSLayoutConstraint(item: label, attribute: .left, relatedBy: .equal, toItem: self, attribute: .left, multiplier: 1.0, constant: x).isActive = true
            NSLayoutConstraint(item: label, attribute: .width, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1.0, constant: label.bounds.width).isActive = true
            NSLayoutConstraint(item: label, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1.0, constant: label.bounds.height).isActive = true
        }

        for axis in axises {
            let position: CGFloat = {
                let position = (self.topGap + (self.base - self.topGap) * (1.0 - CGFloat(axis.scaleValue))).rounded(.up)
                return position == 0 ? 1 : position
            }()
            if let label = axis.aboveLabel {
                fixingLeftTopCorner(label: label, x: 0.0, y: position - label.bounds.height)
            }
            if let label = axis.belowLabel {
                fixingLeftTopCorner(label: label, x: 0.0, y: position)
            }
            self.drawAxis(from: CGPoint(x: 0,
                                        y: position),
                          till: CGPoint(x: rect.size.width,
                                        y: position),
                          color: axis.color,
                          type: axis.type)
        }
        
        if self.bars.count > 0 {
            let step = (self.bounds.width - self.leftGap)/CGFloat(self.bars.count)
            var index = Int(0)
            let start = { () -> CGFloat in
                if self.widthBarPattern.0 > 0 {
                    return self.leftGap
                }
                else {
                    return self.leftGap + step * (1 - self.widthBarPattern.1)
                }
            }()
            let barWidth = { () -> CGFloat in
                if self.widthBarPattern.0 > 0 {
                    return step * self.widthBarPattern.0
                }
                else {
                    return step * self.widthBarPattern.1
                }
            }()
            for bar in self.bars {
                let xPosition = start + step * CGFloat(index)
                self.drawBar(x: xPosition,
                             scaleValues: bar.scaleValues,
                             colors: bar.colors,
                             width: barWidth)
                
                if let legenda = bar.legenda {
                    fixingLeftTopCorner(label: legenda,
                                        x: bar.legendaAlignment == .center ? xPosition - legenda.bounds.width / 2 + barWidth / 2 : xPosition,
                                        y: self.base)
                }
                index += 1
            }
        }
    }

    private func drawBar(x: CGFloat, scaleValues: [CGFloat], colors: [UIColor], width: CGFloat) {
        let height = self.base - self.topGap
        if colors.count == 2 && scaleValues.count == 2 {
            self.drawRect(rect: CGRect(x: x,
                                       y: self.base - height * scaleValues[0],
                                       width: width,
                                       height: height * scaleValues[0]),
                          color: colors[0])
            self.drawRect(rect: CGRect(x: x,
                                       y: self.base - height * scaleValues[0] - height * scaleValues[1],
                                       width: width,
                                       height: height * scaleValues[1]),
                          color: colors[1])
        }
    }

    private func drawRect(rect: CGRect, color: UIColor) {
        let path = UIBezierPath()
        path.lineWidth = rect.size.width
        color.setStroke()
        let x = rect.origin.x + rect.size.width / 2
        path.move(to: CGPoint(x: x,
                              y: rect.origin.y))
        path.addLine(to: CGPoint(x: x,
                                 y: rect.origin.y + rect.size.height))
        path.stroke()
    }

    private func drawAxis(from: CGPoint, till: CGPoint, color: UIColor, type: AxisType) {
        let path = UIBezierPath()
        path.move(to: from)
        path.addLine(to: till)
        path.lineWidth = self.lineAxisWidth
        if type == .dashed {
            path.lineJoinStyle = .miter
            path.setLineDash([2.0, 2.0], count: 2, phase: 0)
        }
        color.setStroke()
        path.stroke()
    }

}

extension CGFloat {
    mutating func equalIfLess(_ newValue: CGFloat) {
        self = newValue > self ? newValue : self
    }
}

class ProgressBarChartLabel: UIView {
    let label = UILabel()
    
    enum LabelAlignment {
        case top
        case center
        case bottom
    }

    func minHeightWith(height: CGFloat, text: String, numberOfLines: Int, font: UIFont, color: UIColor, verticalAlignment: LabelAlignment) {
        self.addSubview(label)
        label.text = text
        label.numberOfLines = numberOfLines
        label.font = font
        label.textColor = color
        label.sizeToFit()

        self.frame = CGRect(x: 0,
                            y: 0,
                            width: label.bounds.width,
                            height: label.bounds.height + height)

        self.label.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint(item: self.label, attribute: .width, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1.0, constant: label.bounds.width).isActive = true
        NSLayoutConstraint(item: self.label, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1.0, constant: label.bounds.height).isActive = true
        
        NSLayoutConstraint(item: self.label, attribute: .left, relatedBy: .equal, toItem: self, attribute: .left, multiplier: 1.0, constant: 0.0).isActive = true
        switch verticalAlignment {
        case .top:
            NSLayoutConstraint(item: self.label, attribute: .top, relatedBy: .equal, toItem: self, attribute: .top, multiplier: 1.0, constant: 0.0).isActive = true
        case .center:
            NSLayoutConstraint(item: self.label, attribute: .centerY, relatedBy: .equal, toItem: self, attribute: .centerY, multiplier: 1.0, constant: 0.0).isActive = true
        case .bottom:
            NSLayoutConstraint(item: self.label, attribute: .bottom, relatedBy: .equal, toItem: self, attribute: .bottom, multiplier: 1.0, constant: 0.0).isActive = true
        }
    }
}

//
//  PermissionRequestViewController.swift
//  Nourish
//
//  Created by Gena Mironchyk on 6/14/17.
//  Copyright © 2017 SSIC. All rights reserved.
//

import UIKit

protocol PermissionRequestViewControllerProtocol {
    func finishPermissionsQuestions ()
}

class PermissionRequestViewController: BasePresentationViewController,PermissionRequestViewControllerProtocol {
    
    var userProfile : UserProfile?
    
    @IBOutlet weak var permissionsView : PermissionsRequestView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.baseViews = [self.permissionsView]
        self.permissionsView.viewDidLoad()
        self.permissionsView.presenter?.controller = self
    }
    
    func finishPermissionsQuestions() {
        NavigationUtility.changeRootViewController(userProfile: self.userProfile,awakedFromBackground: false)
    }
}

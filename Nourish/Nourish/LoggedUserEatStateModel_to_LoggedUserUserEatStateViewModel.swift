//
//  LoggedUserEatStateModel_to_LoggedUserUserEatStateViewModel.swift
//  Nourish
//
//  Created by Vlad Birukov on 09.06.17.
//  Copyright © 2017 SSIC. All rights reserved.
//

import Foundation

class LoggedUserEatStateModel_to_LoggedUserUserEatStateViewModel {
    
    func transform(model: LoggedUserEatStateModel) -> LoggedUserEatStateViewModel {
        let userCurrentOcasion = LoggedUserEatStateViewModel()
        if model.currentOcasion.count != 0 {
            userCurrentOcasion.currentOcasion = model.currentOcasion
        }
        
        return userCurrentOcasion
    }
}

//
//  ProgressView.swift
//  Nourish
//
//  Created by Andrey Zmushko on 7/10/17.
//  Copyright © 2017 SSIC. All rights reserved.
//

import UIKit

protocol ProgressViewProtocol : BaseViewProtocol {
    func config() -> ProgressConfig?
}

class ProgressView: BaseView {
    
    @IBOutlet var progressView: UIView!
    @IBOutlet weak var captionHolderView: UIView!
    @IBOutlet weak var contentHolderView: UIView!
    
    @IBOutlet weak var contentHolderLeading: NSLayoutConstraint!
    @IBOutlet weak var contentHolderBottom: NSLayoutConstraint!
    @IBOutlet weak var contentHolderTrailing: NSLayoutConstraint!
    @IBOutlet weak var contentHolderTop: NSLayoutConstraint!
    @IBOutlet weak var captionTop: NSLayoutConstraint!
    
    @IBOutlet weak var captionLabel: UILabel!
       @IBOutlet weak var underLine: UIView!
    
    var updateCardHeightHook: ((_: CGFloat, _: ProgressCard) -> Void)? = nil
    
    var model: AnyObject?
    

    
    func isToday() -> Bool {
        if let config = self.progressConfig {
            let calendar = Calendar.current
            let result = calendar.compare(config.date, to: Date(), toGranularity: .day)
            switch result {
            case .orderedSame:
                return true
            default:
                break
            }
        }
        return false
    }

    
    var progressConfig: ProgressConfig?
    
    var tapHook: ((_ cardType: ProgressCard) -> Void)? = nil
    
    var currentContent: UIView?

    var progressContent: ProgressContentView!
    
    var sublayersCollector = [CALayer]()
    
    func config() -> ProgressConfig? {
        return self.progressConfig
    }
    
    func removeSublayers() {
        for layer in sublayersCollector {
            layer.removeFromSuperlayer()
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
       
        progressView =  UINib(nibName: "ProgressView", bundle: nil).instantiate(withOwner: self, options: nil)[0] as! UIView
        translatesAutoresizingMaskIntoConstraints = false
        addSubview(progressView)
        
        progressView.bindEdgeTo(self)
        
        captionHolderView.backgroundColor = UIColor.clear
        contentHolderView.backgroundColor = UIColor.clear
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.registerTapGesture()
    }
    
    func loadContent(contentView content: UIView) {
        currentContent?.removeFromSuperview()
        if currentContent != nil {
            currentContent = nil
        }
        currentContent = content
        contentHolderView.addSubview(content)        
        content.bindEdgeTo(contentHolderView)
        
    }
    
    func loadContentNibWith(Name name: String!) -> ProgressContentView! {
        return UINib(nibName: name, bundle: nil).instantiate(withOwner: self, options: nil).first as! ProgressContentView
    }
    
    func registerTapGesture() {
        let gesture = UITapGestureRecognizer(target: self, action: #selector (gestureTapAction (_:)))
        addGestureRecognizer(gesture)
    }
    
    func gestureTapAction(_ sender:UITapGestureRecognizer){
        gestureTapAction()
    }
    
    func stopAnimation() {
        self.stopActivityAnimation()
    }
    
    func loadWhiteThemeWith(header: String) {
        progressView.backgroundColor = UIColor.white
        self.captionLabel.text = header
    }
    

    func prepareTheme() {
        contentHolderTrailing.constant = 0
        contentHolderLeading.constant = 0
        contentHolderBottom.constant = 0
        
        underLine.isHidden = false
    }
    
    func loadThemeNoHeader() {
        prepareTheme()
        
        captionHolderView.isHidden = true
        contentHolderTop.constant = 0
    }
    
    func dailyDetailScore () {
        underLine.isHidden = true
        captionHolderView.isHidden = true
        contentHolderTop.constant = 26.25
        contentHolderTrailing.constant = 0
        contentHolderLeading.constant = 0
        contentHolderBottom.constant = 0
    }

    func loadThemeNoHeaderProgress() {
        prepareTheme()
        captionHolderView.isHidden = true
        contentHolderTop.constant = 0
        captionTop.constant = 0
        underLine.backgroundColor = NRColorUtility.hexStringToUIColor(hex: "#BFBFBF")
    }
    
    func loadThemeWithHeader() {
        prepareTheme()
        
        captionHolderView.isHidden = false
        contentHolderTop.constant = 56
        captionTop.constant = 8
        
        if let config = self.progressConfig {
            self.captionLabel.text = config.period.scoreCardTitle
        }
    }

    func loadThemeWith(header: String, leftArrow: Bool, rightArrow: Bool, leftHook: (() -> Void)?, rightHook: (() -> Void)?) {
        prepareTheme()
        progressView.backgroundColor = NRColorUtility.progressScaleBackgroundColor()
        captionHolderView.isHidden = false
        contentHolderTop.constant = 24
        captionTop.constant = 0
        captionLabel.textAlignment = NSTextAlignment.center
        captionLabel.textColor = UIColor.white
        captionLabel.text = header
    }
    
    func hideView() {
        contentHolderLeading.constant = 0
        contentHolderBottom.constant = 0
        contentHolderTrailing.constant = 0
        contentHolderTop.constant = 0
        captionTop.constant = 0
    }
    
    func hideLine() {
        underLine.isHidden = true
    }

    
    func showView(caloricAverage: Bool = false) {
        contentHolderLeading.constant = 16
        contentHolderTrailing.constant = 16
        
        if caloricAverage != true {
            contentHolderBottom.constant = 8
            contentHolderTop.constant = 56
            captionTop.constant = 16
        } else {
            contentHolderBottom.constant = 22
            contentHolderTop.constant = 0
            captionTop.constant = 0
        }
    }
    
    func showCaloricChart() {
        contentHolderLeading.constant = 16
        contentHolderTrailing.constant = 16
        contentHolderBottom.constant = 8
        contentHolderTop.constant = 62
        captionTop.constant = 16
    }
    
    func showUserCurrenOcasionView() {
        contentHolderBottom.constant = 8
        contentHolderTop.constant = 56
        captionTop.constant = 16
    }
    
    // virtual mrthods :
    
    func gestureTapAction() {}
    
    func reloadData() {}
    
    func renderView(_ config: ProgressConfig) {
        self.progressConfig = config
        if config.state == .card {
            self.underLine.isHidden = true
            self.captionHolderView.isHidden = true
            self.contentHolderTop.constant = 24
            //self.contentHolderBottom.constant = 24
        }
    }
    
    func setupWith(Model model: AnyObject) {
        self.model = model
    }
}

extension Date {
    func startOfMonth() -> Date? {
        let calendar = Calendar.current
        let currentDateComponents = calendar.dateComponents([.year, .month], from: self)
        let startOfMonth = calendar.date(from: currentDateComponents)
        
        return startOfMonth
    }
    
    func startOfWeek() -> Date? {
        //if start from sunday uncomment current and comment iso
        let calendar = Calendar.current
        //var calendar = Calendar(identifier: .iso8601)
        let currentDateComponents = calendar.dateComponents([.yearForWeekOfYear, .weekOfYear], from: self)
        let startOfWeek = calendar.date(from: currentDateComponents)
        
        return startOfWeek
    }

    func startOfPreviousWeek() -> Date? {
        return Calendar.current.date(byAdding: .day, value: -7, to: self.startOfWeek()!)
    }
}

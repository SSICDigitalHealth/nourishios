//
//  MealRecord_to_MealRecordCache.swift
//  Nourish
//
//  Created by Nova on 12/20/16.
//  Copyright © 2016 SSIC. All rights reserved.
//

import UIKit
import RealmSwift

class MealRecord_to_MealRecordCache: NSObject {
    func transform(record : MealRecord) -> MealRecordCache {
        let meal = MealRecordCache()
        
        meal.occasionEnum = record.ocasion
        meal.grams = record.grams
        meal.amount = record.amount
        meal.idString = record.idString
        meal.name = record.name
        meal.msreDesc = record.msreDesc
        meal.msreGrams = record.msreGrams
        meal.kCal = record.kCal
        meal.portionKCal = record.portionKCal
        meal.brand = record.brand
        meal.score = record.score
        meal.userFoodId = record.userFoodId
        meal.calories = record.calories
        meal.unit = record.unit
        
        meal.isFavourite = record.isFavourite
        meal.searchCount = record.searchCount
        meal.caloriesPerMilliliter = record.caloriesPerMilliliter
        meal.caloriesPerGramm = record.caloriesPerGramm
        meal.barCode = record.barCode
        meal.brandAndName = record.brandAndName
        
        if record.nutrientsFromNoom != nil {
            for nutr in record.nutrientsFromNoom! {
                meal.nutrientsFromNoom.append(nutr)
            }
        }
        meal.originalSearchString = record.originalSearchString
        meal.foodUid = record.foodUid
        meal.date = record.date
        meal.calories = record.calories
        
        meal.groupName = record.groupName
        meal.groupID = record.groupID
        meal.isFavourite = record.isFavourite
        meal.userPhotoId = record.userPhotoId
        meal.groupPhotoID = record.groupPhotoID
        meal.groupFoodUUID = record.groupFoodUUID
        
        meal.sourceFoodDBID = record.sourceFoodDBID
        meal.sourceFoodDB = record.sourceFoodDB
        
        return meal

    }
    
    func transform(recordList : [MealRecord]) -> [MealRecordCache] {
        var array : [MealRecordCache] = []
        for record in recordList {
            array.append(self.transform(record: record))
        }
        return array
    }
    
}

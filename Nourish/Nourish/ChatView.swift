//  ChatView.swift
//  Nourish
//
//  Created by Gena Mironchyk on 2/9/17.
//  Copyright © 2017 SSIC. All rights reserved.
//

import UIKit

class ChatView: BaseView, ChatViewProtocol{
    
    @IBOutlet weak var chatTableView = UITableView()
    @IBOutlet weak var splashScreeView: BaseView!
    @IBOutlet weak var responceScrollView = UIScrollView()
    @IBOutlet weak var responseViewHeight : NSLayoutConstraint?
    var presenter = ChatPresenter()
    var userProfile : UserProfile?
    var homeView : OnboardingHomeViewProtocol!
    
    func setupView() {
        let view = UINib(nibName: "ChatView", bundle: nil).instantiate(withOwner: self, options: nil)[0] as! UIView
        view.translatesAutoresizingMaskIntoConstraints = false
        self.translatesAutoresizingMaskIntoConstraints = false
        self.addSubview(view)
        NSLayoutConstraint(item: view, attribute: .leading, relatedBy: .equal, toItem: self, attribute: .leading, multiplier: 1, constant: 0).isActive = true
        NSLayoutConstraint(item: view, attribute: .trailing, relatedBy: .equal, toItem: self, attribute: .trailing, multiplier: 1, constant: 0).isActive = true
        NSLayoutConstraint(item: view, attribute: .top, relatedBy: .equal, toItem: self, attribute: .top, multiplier: 1, constant: 0).isActive = true
        NSLayoutConstraint(item: view, attribute: .bottom, relatedBy: .equal, toItem: self, attribute: .bottom, multiplier: 1, constant: 0).isActive = true
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.stopActivityAnimation()
    }
    
    override func viewDidLoad() {
        
        self.setupView()
        self.basePresenter = presenter
        presenter.loadPreffiledUser()

        self.chatTableView!.delegate = presenter
        self.chatTableView!.dataSource = presenter
        self.chatTableView!.register(ChatCell.self, forCellReuseIdentifier: "ChatCell")
        presenter.chat = self
        if self.userProfile != nil {
            presenter.loggedUserModel = UserProfile_to_UserProfileModel().transform(user : self.userProfile!)
            self.userProfile = nil
        }
        
        if ((defaults.value(forKey: onBoardingKey)) != nil) {
            self.responseViewHeight?.constant = 0
            self.layoutIfNeeded()
        }
        
}
    
    
    
    func updateChat() {
        self.chatTableView?.isHidden = true
        self.chatTableView!.reloadData()
        self.tableViewScrollToBottom(animated: false)
        self.hideSplashScreen()
        self.chatTableView?.isHidden = false
    }
    
    
    func insertRowAtIndexPath(path : [IndexPath],fromUser : Bool ) {
        self.chatTableView!.beginUpdates()
        if fromUser == true {
            self.chatTableView!.insertRows(at: path, with: .right)
        } else {
            self.chatTableView!.insertRows(at: path, with: .left)
        }
        self.chatTableView!.endUpdates()
        
        self.tableViewScrollToBottom(animated: false)

    }
    
    private func tableViewScrollToBottom(animated : Bool) {
        let numberOfRows = self.chatTableView!.numberOfRows(inSection: 0)
        if numberOfRows > 0 {
            let indexPath = IndexPath(row: numberOfRows - 1, section:0)
            self.chatTableView!.scrollToRow(at: indexPath, at: .bottom, animated: animated)
        
        }
    }
    
    func updateUserResponcesWithChartView(responses : [UIView]) {
        self.responseViewHeight?.constant = 400.0
        for v in (self.responceScrollView!.subviews) {
            v.removeFromSuperview()
        }
        for responceView in responses {
            self.responceScrollView!.addSubview(responceView)
        }
        UIView.animate(withDuration: 0.3, animations: {
            self.layoutIfNeeded()
        }, completion: {_ in
        })
        self.tableViewScrollToBottom(animated: true)
    }
    
    
    private func prepareView(responses: [UIView]) {
        self.clearResponseView(responses:responses)
        for responceView in responses {
            self.responceScrollView!.addSubview(responceView)
        }
        

    }
    func clearResponseView (responses: [UIView]){
        
        var height : CGFloat = 0.0
        
        var showDefaultHeight : Bool = false
        if responses.count < 4 {
            for responseView in responses {
                if responseView.isKind(of: NRPickerView.self) {
                    showDefaultHeight = true
                    break
                }
                height = height + responseView.frame.height + 20
            }
        } else {
            height = 136.0
            showDefaultHeight = true
        }
        
        
        if height > 0 {
            self.responseViewHeight?.constant = showDefaultHeight == true || height < 136.0 ? 136.0 : height

            for v in (self.responceScrollView!.subviews) {
                v.removeFromSuperview()
            }
            UIView.animate(withDuration: 0.3, animations: {
                self.layoutIfNeeded()
            })
        
        } else {
            for v in (self.responceScrollView!.subviews) {
                v.removeFromSuperview()
            }
        }
    }
    
    func updateUserResponces(responces: [UIView]) {
        
        var height : CGFloat = 0
        var showDefaultHeight : Bool = false
        if responces.count < 4 {
            for responseView in responces {
                if responseView.isKind(of: NRPickerView.self) {
                    showDefaultHeight = true
                    break
                }
                height = height + responseView.frame.height + 20
            }
        } else {
            height = 136.0
            showDefaultHeight = true
        }
        
        
        if height > 0 {
            self.responseViewHeight?.constant = showDefaultHeight == true || height < 136.0 ? 136.0 : height
           // self.updateChat()
            UIView.animate(withDuration: 0.3, animations: {
                self.layoutIfNeeded()
            }, completion: {_ in
                self.prepareView(responses: responces)
            })
        } else {
            self.prepareView(responses: responces)
        }
        self.tableViewScrollToBottom(animated: true)

    }
    
    func onboardingFinished () {
        if defaults.value(forKey: onBoardingKey) != nil{
            self.hideResponseView()
            self.homeView.onboardingCompleted()
            self.tableViewScrollToBottom(animated: true)
        }
    }
    
    func hideTable () {
        self.homeView.hideMenu()
    }
    
    func hideResponseView () {
        if (self.responseViewHeight?.constant != 0.0) {
        self.responseViewHeight!.constant = 0.0
        UIView.animate(withDuration: 0.3, animations: {
            self.layoutIfNeeded()
        })
        }
    }
  
    func showScoreFeedback(date : Date) {
        self.homeView.showScoreFeedback(date:date)
    }
    
    func openFitBitLogin() {
        self.homeView.openFitBitLogin()
    }

    func showSplashScreen() {
        if splashScreeView != nil {
            splashScreeView.prepareLoadView()
        }
    }
    
    func hideSplashScreen() {
        if splashScreeView != nil {
            splashScreeView.stopActivityAnimation()
            splashScreeView.removeFromSuperview()
        }
    }
    
    func showGoalChangedInfo() {
        self.homeView.showGoalChangedInfo()
    }
    
}

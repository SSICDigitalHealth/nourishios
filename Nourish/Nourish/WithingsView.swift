//
//  WithingsView.swift
//  Nourish
//
//  Created by Gena Mironchyk on 4/18/17.
//  Copyright © 2017 SSIC. All rights reserved.
//

import UIKit
protocol WithingsWebViewProtocol {
    func loadRequest(request : URLRequest)
    func stopAnimation()
}
class WithingsView: BaseView, WithingsWebViewProtocol {
    
    @IBOutlet weak var withingsWebView = UIWebView()
    
    var presenter = WithingsPresenter()
    
    
    override func viewWillAppear(_ animated: Bool) {
        self.basePresenter = presenter
        self.withingsWebView?.delegate = presenter
        presenter.withingsView = self
        super.viewWillAppear(animated)
    }
    
    func stopAnimation() {
        self.stopActivityAnimation()
    }
    
    func loadRequest(request: URLRequest) {
        self.withingsWebView?.loadRequest(request)
    }
    
    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */

}

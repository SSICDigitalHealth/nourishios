//
//  json_to_ScoreCacheRealm.swift
//  Nourish
//
//  Created by Vino (Vinodha) Sundaramoorthy on 4/19/17.
//  Copyright © 2017 SSIC. All rights reserved.
//

import UIKit

class json_to_ScoreCacheRealm: NSObject {
    
    func transform(dictionary : [String : Any] ,date:Date, isScoreDelta : Bool) -> ScoreCacheRealm {
        let scoreRealm = ScoreCacheRealm()
        let score = dictionary["score_after"] as? Double ?? 0.0
        let scoreDelta = dictionary["score_change"] as? Double ?? 0.0
        scoreRealm.score = score * 100
        scoreRealm.scoreDelta = scoreDelta * 100
        scoreRealm.date = date
        scoreRealm.valueToShow = isScoreDelta ? scoreRealm.scoreDelta : scoreRealm.score
        
        if dictionary["score_delta_info"] != nil {
            let scoreDeltaInfo : [String : Any] = dictionary["score_delta_info"] as! [String : Any]
        
            //Nutrient Info
            if scoreDeltaInfo["nutrient_info"] != nil {
                let nutrientInfoDict : [String : Any] = scoreDeltaInfo["nutrient_info"] as! [String : Any]
                for (keys ,_) in nutrientInfoDict {
                    let nutrientList = NutrientInfoRealm()
                    nutrientList.name = keys
                    
                    if nutrientInfoDict[keys] != nil {
                        let details = nutrientInfoDict[keys] as! [String : Any]
                        nutrientList.max = details["upper"] as? Double ?? 0.0
                        nutrientList.min = details["lower"] as? Double ?? 0.0
                        nutrientList.unit = details["unit"] as? String ?? ""
                        nutrientList.value = details["amount"] as? Double ?? 0.0
                    }
                    
                    scoreRealm.nutrientList.append(nutrientList)
                }
            }
        
            //Best Delta
            if scoreDeltaInfo["nutrients_with_best_delta"] != nil {
                let bestDeltaArray : [String] = scoreDeltaInfo["nutrients_with_best_delta"] as! [String]
                for str in bestDeltaArray {
                    let realStr = RealmString()
                    realStr.stringValue = str
                    scoreRealm.bestDelta.append(RealmString(value: realStr))
                }
            }
        
            //Worst Delta
            if scoreDeltaInfo["nutrients_with_worst_delta"] != nil {
                let worstDeltaArray : [String] = scoreDeltaInfo["nutrients_with_worst_delta"] as! [String]
                for str in worstDeltaArray {
                    let realStr = RealmString()
                    realStr.stringValue = str
                    scoreRealm.worstDelta.append(RealmString(value: realStr))
                }
            }
        
            //Close to upper limit
            if scoreDeltaInfo["nutrients_brought_close_to_upper_limit"] != nil {
                let closeToUpArray : [String] = scoreDeltaInfo["nutrients_brought_close_to_upper_limit"] as! [String]
                for str in closeToUpArray {
                    let realStr = RealmString()
                    realStr.stringValue = str
                    scoreRealm.closeToUpperLimit.append(RealmString(value: realStr))
                }
            }
        
            //Brought above dri
            if scoreDeltaInfo["nutrients_brought_above_dri"] != nil {
                let aboveDriArray : [String] = scoreDeltaInfo["nutrients_brought_above_dri"] as! [String]
                for str in aboveDriArray {
                    let realStr = RealmString()
                    realStr.stringValue = str
                    scoreRealm.aboveDri.append(RealmString(value: realStr))
                }
            }
        
            //Brought into dri
            if scoreDeltaInfo["nutrients_brought_into_dri"] != nil {
                let intoDriArray : [String] = scoreDeltaInfo["nutrients_brought_into_dri"] as! [String]
                for str in intoDriArray {
                    let realStr = RealmString()
                    realStr.stringValue = str
                    scoreRealm.InDri.append(RealmString(value: realStr))
                }
            }
        }
        return scoreRealm
    }

}

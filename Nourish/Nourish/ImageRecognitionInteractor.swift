//
//  ImageRecognitionInteractor.swift
//  Nourish
//
//  Created by Nova on 4/27/17.
//  Copyright © 2017 SSIC. All rights reserved.
//

import UIKit
import RxSwift

class ImageRecognitionInteractor: NSObject {
    
    let repo = ClarifaiRepository()
    let foodSearchInteractor = NRFoodSearchInteractor()
    //let toSearchModelMapper = NMFood_to_foodSearchModel()
    
    func recognizeImage(image : UIImage) -> Observable<[foodSearchModel]> {
        return self.repo.recognizeImage(image: image)
    }
    
   /* func recognizeBarcode(barCode : String) -> Observable<[foodSearchModel]> {
        let obj = self.foodSearchInteractor.searchFoodWith(barcode: barCode, ocasion: .breakfast).flatMap {noomFoods -> Observable<[foodSearchModel]> in
            let models = self.toSearchModelMapper.transform(foods: noomFoods)
            return Observable.just(models)
        }
        return obj
    }*/
}

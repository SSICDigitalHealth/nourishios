//
//  NRGoalOptionsTableViewCell.swift
//  Nourish
//
//  Created by Vino (Vinodha) Sundaramoorthy on 11/28/16.
//  Copyright © 2016 SSIC. All rights reserved.
//

import UIKit

class NRGoalOptionsTableViewCell: UITableViewCell {
    @IBOutlet weak var goalLabel : UILabel!
    @IBOutlet weak var selectButton : UIImageView!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    func setUpGoalWithModel(goal:goalOptionsList) {
        goalLabel.text = goal.goal.descriptionUI
        let imageName = goal.isSelected ? "radio_on_green" : "radio_off_green"
        selectButton.image = UIImage(named: imageName)
    }
}

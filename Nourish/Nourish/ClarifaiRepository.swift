//
//  ClarifaiRepository.swift
//  Nourish
//
//  Created by Nova on 4/17/17.
//  Copyright © 2017 SSIC. All rights reserved.
//

import UIKit
import Clarifai
import RxSwift

let kClarifaiAppID = "uN6hk02XeoJ5Pm7aA4wfDigCLcgVXud-EEd6QDhX"
let kClarifaiSecret = "JKoK-kP490x0BiUH-pxtD8D89xo5PEqY-5qbV8jW"


class ClarifaiRepository: NSObject {
    
    let noomRepo = NoomRepository.shared
    let nDataStore = NourishDataStore.shared
    let userRepo = UserRepository.shared
    let cacheDataStore = ClarifaiCacheDataStore()
    
    private var orderedTags : [String] = []
    
    func recognizeImage(image : UIImage) -> Observable<[foodSearchModel]> {
        let obj = self.imageToTags(image: image).flatMap { tags -> Observable<[String]> in
            return Observable.just(self.filterTags(tags: tags))
        }.flatMap { tagsFiltered -> Observable<[foodSearchModel]> in
            return self.fnddsFoodFromTags(tags: tagsFiltered).flatMap {fnddsMeals -> Observable<[foodSearchModel]> in
                return Observable.just(self.mergeWithCache(tags: tagsFiltered, meals: fnddsMeals))
            }
        }
        return obj
    }
    
    
    private func dictionaryOfMealsWithTags(tags : [String], meals : [foodSearchModel]) -> [String : foodSearchModel] {
        var dictToReturn = [String : foodSearchModel]()
        for tag in tags {
            let indexOfTag = tags.index(of: tag)
            if meals.count > indexOfTag! {
                dictToReturn[tag] = meals[indexOfTag!]
            }
        }
        
        return dictToReturn
    }
    
    private func mergeWithCache(tags : [String], meals : [foodSearchModel]) -> [foodSearchModel] {
        let mealsToMerge = self.dictionaryOfMealsWithTags(tags: tags, meals: meals)
        var cache = self.cacheDataStore.fetchLastCache()
        var counter = 0
        for (key, element) in mealsToMerge {
            cache[Calendar.current.date(byAdding: .second, value: -counter, to: Date())!] = [key : element]
            counter += 1
        }
        self.cacheDataStore.storeCache(cache: cache)
        return mealsArrayFromCache(cache: cache)
    }
    
    private func mealsArrayFromCache(cache : [Date : [String : foodSearchModel]]) -> [foodSearchModel] {
        var arrayToReturn = [foodSearchModel]()
        var dictToWorkWith = [String : foodSearchModel]()
        for (_, element) in cache {
            for (key, foodElement) in element{
                dictToWorkWith[key] = foodElement
            }
        }
        for key in self.orderedTags {
            let model = dictToWorkWith[key]
            if model != nil {
                arrayToReturn.append(model!)
            }
            
        }

        
        let smallest = min(10, arrayToReturn.count)
        return Array(arrayToReturn[0..<smallest])
    }
    
    private func imageToTags(image : UIImage) -> Observable<[String]> {
        return Observable.create { observer in
            let cApp = ClarifaiApp(appID: kClarifaiAppID, appSecret: kClarifaiSecret)
            cApp?.getModelByName("food-items-v1.0", completion: {model, error in
                let cImage = ClarifaiImage(image: image)
                model?.predict(on: [cImage!], completion: {outputs, error in
                    if error != nil {
                        observer.onError(error!)
                    } else {
                        let output = outputs?.first
                        
                        if output != nil {
                            var concepts : [ClarifaiConcept] = []
                            
                            for concept in (output?.concepts)! {
                                concepts.append(concept)
                            }
                            
                            concepts.sort {$0.score > $1.score}
                            concepts = concepts.filter {$0.score >= 0.1}
                            
                            for conc in concepts {
                                print(conc.conceptName,(conc.score * 100))
                            }
                            
                            let strings = concepts.map {$0.conceptName}
                            observer.onNext(strings as! [String])
                            observer.onCompleted()
                        }
                    }
                })
            })
            
            return Disposables.create()
        }
    }
    
    private func filterTags(tags : [String]) -> [String] {
        var arrayOfStrings: [String]?
        
        do {
            if let path = Bundle.main.path(forResource: "exclude", ofType: "txt"){
                let data = try String(contentsOfFile:path, encoding: String.Encoding.utf8)
                arrayOfStrings = data.components(separatedBy: "\n")
            }
        } catch let err as NSError {
            LogUtility.logToFile(err)
        }
        
        var proxyArray : [String] = []
        
        
        let cachedData = self.cacheDataStore.fetchLastCache()
        var cachedTags : [String] = []
        for (_,element) in cachedData {
            cachedTags.append(contentsOf: element.keys)
        }
        
        if arrayOfStrings != nil && (arrayOfStrings?.count)! > 0 {
            for tag in tags {
                if !arrayOfStrings!.contains(tag) {
                    proxyArray.append(tag)
                }
            }
        }
        let smallest = min(10, proxyArray.count)
        var arrayToReturn = Array(proxyArray[0..<smallest])
        self.orderedTags = arrayToReturn
        for tag in arrayToReturn {
            if cachedTags.contains(tag) {
                arrayToReturn.remove(at: arrayToReturn.index(of: tag)!)
            }
        }
        
        return arrayToReturn
    }
    
//    private func foodFromTags(tags : [String]) -> Observable<[foodSearchModel]> {
//        var obsArray : [Observable<NMFood>] = []
//        let mapper = NMFood_to_foodSearchModel()
//
//        for tag in tags {
//            let model = self.noomRepo.loadSingleEntrieFrom(string: tag, ocasion: self.occasionFromDate(date: Date()))
//            obsArray.append(model)
//        }
//
//        let singleObs = Observable.from(obsArray).concat()
//        let seq = singleObs.toArray()
//        let sequenceRow = seq.flatMap { noomFood -> Observable<[foodSearchModel]> in
//            let mappedObjs = mapper.transform(foods: noomFood)
//            return Observable.just(mappedObjs)
//        }
//        return sequenceRow
//    }
//
    private func fnddsFoodFromTags(tags : [String]) -> Observable<[foodSearchModel]> {
        let result = self.userRepo.getCurrentUser(policy: .DefaultPolicy).flatMap { user in
            Observable.just(user.userID)
        }.flatMap { userID -> Observable<[foodSearchModel]> in
            let token = NRUserSession.sharedInstance.accessToken!
            return self.nDataStore.searchFnddsFoodFrom(tags: tags, token: token, userID: userID!)
        }
        return result
    }
    
    private func occasionFromDate(date : Date) -> Ocasion {
        let hour = Double(Calendar.current.component(.hour, from: date)) + Double(Calendar.current.component(.minute, from: date))/60.0
        if hour >= 5 && hour <= 10.5 {
            return .breakfast
        } else if hour >= 11 && hour <= 13.5 {
            return .lunch
        } else if hour >= 17 && hour <= 21.5 {
            return .dinner
        } else {
            return .snacks
        }
        
    }
}

//
//  SleepModel.swift
//  Nourish
//
//  Created by Vlad Birukov on 28.08.17.
//  Copyright © 2017 SSIC. All rights reserved.
//

import Foundation

class SleepModel  {
    var sleepHours: Double?
    var avgSleepDay: Double?
}

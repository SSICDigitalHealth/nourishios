//
//  NutrientWeeklyMonthlyPresenter.swift
//  Nourish
//
//  Created by Vlad Birukov on 26.10.2017.
//  Copyright © 2017 SSIC. All rights reserved.
//

import Foundation

class NutrientWeeklyMonthlyPresenter: BasePresenter, ProgressBarChartDelegate {
    
    var nutrienModel = NutrientDetailInformationViewModel()
    var progressBarChart = ProgressBarChart()
    var proportionWithChart = (0.0, 0.0, 0.0)
    
    let numberAxis = 4
    
    
    func setupWith(model: NutrientDetailInformationViewModel) {
        self.nutrienModel = model
        self.proportionWithChart = self.getProportionWithChartLine()
        self.progressBarChart.widthBarPattern = (0.9, 0)
        self.progressBarChart.reloadData(DataSource: self)
    }
    
    func numberOfBars(in progressBarChart: ProgressBarChart) -> Int {
        return nutrienModel.dataChart.dataCharts != nil ? (nutrienModel.dataChart.dataCharts?.count)! : 0
    }
    
    func numberOfAxis(in progressBarChart: ProgressBarChart) -> Int {
        return self.numberAxis
    }
    
    func progressBarChart(_ progressBarChart: ProgressBarChart, axisAtIndex:Int) -> ProgressBarChart.Axis? {
        let type: ProgressBarChart.AxisType!
        let value: Double!
        var color = UIColor.gray
        var colorText = UIColor.clear
        var label: ProgressBarChartLabel?
        
        switch axisAtIndex {
        case 3:
            type = .solid
            value = 1.0
            color = UIColor.clear
        case 2:
            type = .dashed
            value = self.proportionWithChart.1
            if self.proportionWithChart.1 == 0 {
                color = UIColor.clear
            } else {
                color = UIColor.black.withAlphaComponent(0.2)
                colorText = NRColorUtility.hexStringToUIColor(hex: "808080")
            }
            label = ProgressBarChartLabel()
            label?.minHeightWith(height: 10.0,
                                 text: String(format: "%@ %@", nutrienModel.dataChart.upperLimit.roundToTenIfLessOrMore(target: nutrienModel.dataChart.upperLimit), nutrienModel.dataChart.unit),
                                 numberOfLines: 1,
                                 font: UIFont(name: "Roboto-Regular", size: 12)!,
                                 color: colorText,
                                 verticalAlignment: ProgressBarChartLabel.LabelAlignment.top)
        case 1:
            type = .dashed
            value = self.proportionWithChart.2
            color = UIColor.black.withAlphaComponent(0.2)
            label = ProgressBarChartLabel()
            label?.minHeightWith(height: 10.0,
                                 text: String(format: "%@ %@\n(RDA)", nutrienModel.dataChart.lowerLimit.roundToTenIfLessOrMore(target: nutrienModel.dataChart.lowerLimit), nutrienModel.dataChart.unit),
                                 numberOfLines: 2,
                                 font: UIFont(name: "Roboto-Regular", size: 12)!,
                                 color: NRColorUtility.progressTableGrayColor(),
                                 verticalAlignment: ProgressBarChartLabel.LabelAlignment.top)
        default:
            type = .solid
            value = 0.0
            color = UIColor.clear
        }
        
        return (color, type, value, nil, label) as ProgressBarChart.Axis
    }
    
    func progressBarChart(_ progressBarChart: ProgressBarChart, barAtIndex:Int) -> ProgressBarChart.Bar? {
        var legendaView: ProgressBarChartLabel?
        
        let dataForChart = convertInformationForChart(index: barAtIndex)
        
        if let legenda = nutrienModel.dataChart.dataCharts?[barAtIndex] {
            legendaView = ProgressBarChartLabel()
            legendaView?.label.textAlignment = .center
            legendaView?.minHeightWith(height: 4.0,
                                       text: legenda.label! ,
                                       numberOfLines: 1,
                                       font: UIFont(name: "Roboto-Regular", size: 11)!,
                                       color: NRColorUtility.progressTableGrayColor(),
                                       verticalAlignment: ProgressBarChartLabel.LabelAlignment.bottom)
        }
        
        return (dataForChart.0, dataForChart.1 , legendaView as UIView?, ProgressBarChart.LegendaAlignment.center) as ProgressBarChart.Bar
    }
    
    private func getProportionWithChartLine() -> (Double ,Double, Double) {
        var maxValueWithConsumed = 0.0
        var middleLine = 0.0
        if nutrienModel.dataChart.dataCharts != nil {
            maxValueWithConsumed = (nutrienModel.dataChart.dataCharts?.max(by: { $0.consumed < $1.consumed })?.consumed)!
        }
        
        let hightLine = maxValueWithConsumed > nutrienModel.dataChart.upperLimit ? (nutrienModel.dataChart.upperLimit / maxValueWithConsumed) : 1
        
        if hightLine == 0 {
            middleLine = maxValueWithConsumed > nutrienModel.dataChart.lowerLimit ?
                (nutrienModel.dataChart.lowerLimit / maxValueWithConsumed) : 1
        } else {
            middleLine = maxValueWithConsumed > nutrienModel.dataChart.upperLimit ? (nutrienModel.dataChart.lowerLimit / maxValueWithConsumed) : (nutrienModel.dataChart.lowerLimit / nutrienModel.dataChart.upperLimit)
        }
        
        
        return (max(maxValueWithConsumed, nutrienModel.dataChart.upperLimit, nutrienModel.dataChart.lowerLimit), hightLine, middleLine)
    }
    
    private func convertInformationForChart(index: Int) -> ([CGFloat], [UIColor]) {
        var resultProportion = [CGFloat()]
        var resultColor = [UIColor]()
        var proportion = 0.0
        
        if nutrienModel.dataChart.dataCharts != nil {
            proportion = (nutrienModel.dataChart.dataCharts?[index].consumed)! / self.proportionWithChart.0
        }
        
        if proportion < self.proportionWithChart.2 {
            resultProportion = [CGFloat(proportion), CGFloat(self.proportionWithChart.2 - proportion)]
            resultColor = [NRColorUtility.progressScaleBackgroundColor(), NRColorUtility.progressScaleBackgroundColor().withAlphaComponent(0.2)]
        } else if proportion >= self.proportionWithChart.2 && proportion <= self.proportionWithChart.1 {
            resultProportion = [CGFloat(proportion), 0]
            resultColor = [NRColorUtility.progressScaleBackgroundColor(), NRColorUtility.progressChartNutritionColor()]
        } else if proportion > self.proportionWithChart.1 {
            resultProportion = [CGFloat(self.proportionWithChart.1) ,CGFloat(proportion - self.proportionWithChart.1), ]
            if self.proportionWithChart.1 == 0 {
                resultColor = [NRColorUtility.progressScaleBackgroundColor(), NRColorUtility.progressScaleBackgroundColor()]
            } else {
                resultColor = [NRColorUtility.progressScaleBackgroundColor(), NRColorUtility.progressChartNutritionOverColor()]
            }
        }
        return (resultProportion, resultColor)
    }
}

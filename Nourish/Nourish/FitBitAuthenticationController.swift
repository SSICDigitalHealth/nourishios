//
//  FitBitAuthenticationController.swift
//  Nourish
//
//  Created by Vino (Vinodha) Sundaramoorthy on 5/15/17.
//  Copyright © 2017 SSIC. All rights reserved.
//

import UIKit
import SafariServices
import SwiftKeychainWrapper

class FitBitAuthenticationController: NSObject , SFSafariViewControllerDelegate {
    var authorizationVC: SFSafariViewController?
    var delegate: AuthenticationProtocol?
    var authenticationToken: String?
    let messageRepo = ChatBoxRepository()
    let userRepository = UserRepository.shared
    
    init(delegate: AuthenticationProtocol?) {
        self.delegate = delegate
        super.init()
        NotificationCenter.default.addObserver(forName: NSNotification.Name(rawValue: NotificationConstants.fitBitLaunchNotification), object: nil, queue: nil, using: { [weak self] (notification: Notification) in
            // Parse and extract token
            let success: Bool
            if let token = FitBitAuthenticationController.extractToken(notification) {
                self?.authenticationToken = token
                KeychainWrapper.standard.set("Fit Bit", forKey: kActivitySourceKey, withAccessibility: KeychainItemAccessibility.afterFirstUnlock)
                LogUtility.logToFile("FitBit Successfully authorized")
                self?.storeConnectionStatus(status: "FitBit Connected")
                success = true
            } else {
                LogUtility.logToFile("There was an error extracting the access token from the authentication response.")
                self?.storeConnectionStatus(status: "FitBit connected failed , please try after sometime.Activity source can be changed from Connect Device menu")
                success = false
            }
            
            self?.authorizationVC?.dismiss(animated: true, completion: {
                self?.delegate?.authorizationDidFinish(success)
            })
        })
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
    // MARK: Public API
    
    public func login(fromParentViewController viewController: UIViewController) {
        guard let url = URL(string: "https://www.fitbit.com/oauth2/authorize?response_type=code&client_id="+kFitBitClientId+"&redirect_uri="+kFitBitRedirectUri+"&scope="+kFitBitScope+"&expires_in=604800") else {
            LogUtility.logToFile("FitBit : Unable to create authentication URL")
            return
        }
        
        let authorizationViewController = SFSafariViewController(url: url)
        authorizationViewController.delegate = self
        authorizationVC = authorizationViewController
        viewController.present(authorizationViewController, animated: true, completion: nil)
    }
    
    private static func logout() {
        // TODO
    }
    
    private static func extractToken(_ notification: Notification) -> String? {
        var accessToken : String = ""
        
        guard let url = notification.userInfo?[UIApplicationLaunchOptionsKey.url] as? URL else {
            LogUtility.logToFile("FitBit : notification did not contain launch options key with URL")
            return nil
        }
            
        // Extract the access token from the URL
        let strippedURL = url.absoluteString.replacingOccurrences(of: kFitBitRedirectUri.lowercased(), with: "")
            
        var authCode : String = strippedURL
        authCode = (authCode.replacingOccurrences(of: "?code=", with: ""))
        authCode = authCode.replacingOccurrences(of: "#_=_", with: "")
            
        guard let accessTokenUrl = URL(string: "https://api.fitbit.com/oauth2/token?grant_type=authorization_code&client_id=228HWG&redirect_uri="+kFitBitRedirectUri+"&code="+authCode+"&expires_in=604800") else {
                LogUtility.logToFile("FitBit : Unable to create access token URL")
                return accessToken
            }
            
        let authString = String(format:"%@:%@",kFitBitClientId,kFitBitClientSecret)
        var request = URLRequest(url: accessTokenUrl)
        request.httpMethod = "POST"
        request.setValue("Basic \(authString.toBase64())", forHTTPHeaderField: "Authorization")
        request.setValue("application/x-www-form-urlencoded", forHTTPHeaderField: "Content-Type")
            
        let task = URLSession.shared.dataTask(with: request) { data, response, error in
            guard let data = data, error == nil else {
                return
            }
                
            if let httpStatus = response as? HTTPURLResponse, httpStatus.statusCode != 200 {
            }
                
            guard let responseData = (try? JSONSerialization.jsonObject(with: data, options: .allowFragments)) as? [String: Any] else {
                return
            }
                
            if responseData.count > 0 {
                accessToken = responseData["access_token"] as! String
                
                if responseData["access_token"] != nil {
                    FitBitConnectionManager.sharedInstance.accessToken = responseData["access_token"] as! String?
                }
                
                if responseData["refresh_token"] != nil {
                    FitBitConnectionManager.sharedInstance.refreshToken = responseData["refresh_token"] as! String?
                }
                
                if responseData["expires_in"] != nil {
                    let doubleTime = responseData["expires_in"] as! Double
                    let date = Date().addingTimeInterval(doubleTime - kTimeOffsetSeconds)
                    FitBitConnectionManager.sharedInstance.tokenExpirationDate = date
                }
            }
        }
        task.resume()
        return accessToken
    }
    
    func storeConnectionStatus(status : String) {
        let _ = self.userRepository.getCurrentUser(policy: .Cached).subscribe(onNext : {userProfile in
            if userProfile.userID != nil {
                let chatMessage = ChatDataBaseItem()
                chatMessage.content = status
                chatMessage.date = Date()
                chatMessage.isFromUser = false
                chatMessage.chatBoxType = .typeString
                chatMessage.userID = userProfile.userID!
                let realmStore = ChatBoxRealmDataStore()
                realmStore.storeRecordsArray(array: [chatMessage])
            }
        })
    }
    // MARK: SFSafariViewControllerDelegate
    func safariViewControllerDidFinish(_ controller: SFSafariViewController) {
        LogUtility.logToFile("FitBit : Safari View Controller did finish")
        delegate?.authorizationDidFinish(false)
    }
}

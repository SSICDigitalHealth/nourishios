//
//  WeightstatsInteractor.swift
//  Nourish
//
//  Created by Vino (Vinodha) Sundaramoorthy on 1/9/17.
//  Copyright © 2017 SSIC. All rights reserved.
//

import UIKit
import RxSwift
import HealthKit

class WeightstatsInteractor: StatBaseInteractor {
    let activityRepo = UserActivityRepository()
    var fakeGeneratedDate = Date()

    func getWeightStatsDataFor(period : period) -> Observable<WeightStatsTotal> {
            let weightValue = self.activityRepo.getWeightDataFor(period: period)
            let calorieValue = self.activityRepo.getActiveCaloriesFor(period : period)
            let userRepo = UserRepository.shared
            let user = userRepo.getCurrentUser(policy: .DefaultPolicy)
            let zip = Observable.zip(weightValue,calorieValue,user, resultSelector : {weightSamples, calorieSamples, userProfile -> WeightStatsTotal in
                let weeklyPlan = userProfile.getWeeklyWeightLoss()
                var weightDict = self.separateSamples(samples: weightSamples, period: period)
                var calorieDict = self.separateSamples(samples: calorieSamples, period: period)
                weightDict = self.fillWithEmpties(dict: weightDict, period: period)
                calorieDict = self.fillWithEmpties(dict: calorieDict, period: period)
                
//                let avgWeight = self.avgValueFrom(samples: weightSamples, isWeight: true)
//                let avgCalories = self.avgValueFrom(samples: calorieSamples, isWeight: false)
                var model = WeightStatsTotal()
                if userProfile.userGoal == .MaintainWeight {
                    model = self.transforDict(weightDict: weightDict, calorieDict: calorieDict, period: period, weeklyPlan : weeklyPlan, startWeight : userProfile.weight!)
                } else {
                    model = self.transforDict(weightDict: weightDict, calorieDict: calorieDict, period: period, weeklyPlan : weeklyPlan, startWeight : userProfile.startWeight!)
                }
                
                return model
                
            })
        return zip
            
    }
/*
    private func fillWithEmpties(dict : [Date : [HKSample]], period : period) -> [Date : [HKSample]] {
        switch period {
        case .today:
            return dict
        case .weekly:
            if dict.keys.count == 7 {
                return dict
            } else {
                var dictToReturn = dict
                let necessary = self.necessaryKeys(period: period)
                for date in necessary {
                    if dictToReturn[date] == nil {
                        dictToReturn[date] = []
                    }
                }
                return dictToReturn
            }
        case .monthly:
            if dict.keys.count == 4 {
                return dict
            } else {
                var dictToReturn = dict
                let necessary = self.necessaryKeys(period: period)
                for date in necessary {
                    if dictToReturn[date] == nil {
                        dictToReturn[date] = []
                    }
                }
                return dictToReturn
            }
        
        }
    }
    
    private func necessaryKeys(period : period) -> [Date] {
        let date = Date()
        switch period {
        case .today:
            return [date]
        case .weekly:
            var array : [Date] = []
            let startOfDay = calendar.startOfDay(for: date)
            array.append(startOfDay)
            
            for index in 1..<7 {
                let newDate = calendar.date(byAdding: Calendar.Component.day, value: -index, to: startOfDay)
                array.append(newDate!)
            }
            return array
        case .monthly:
            var array : [Date] = []
            let startOfWeek = self.startOfWeekFrom(date: date)
            
            array.append(startOfWeek)
            
            for index in 1..<4 {
                let newDate = calendar.date(byAdding: Calendar.Component.weekdayOrdinal, value: -index, to: startOfWeek)
                array.append(newDate!)
            }
            return array
        }
    }
    
    private func transforDict(weightDict :  [Date : [HKSample]], calorieDict :  [Date : [HKSample]], avgWeight : Double, avgCalories : Double, period : period) -> WeightStatsTotal {
        
        let stat = WeightStatsTotal()
        
        stat.averageWeight = avgWeight
        stat.averageCalorie = avgCalories
        stat.caloriesArray = self.calorieStatsSample(dict: calorieDict, period: period)
        stat.weightsArray = self.weightStatsSample(dict: weightDict, period: period)
        
        return stat
    }
    
    private func weightStatsSample(dict : [Date : [HKSample]], period : period) -> [WeightStatsSample] {
        var array : [WeightStatsSample] = []
        
        for key in dict.keys {
            let sample = WeightStatsSample()
            sample.startDate = key
            sample.endDate = self.endDateFrom(period: period, startDate: key)
            if (dict[key]?.count)! > 0{
                let quantityWeight = dict[key]?.last as! HKQuantitySample
                sample.weight = quantityWeight.quantity.doubleValue(for: HKUnit.gramUnit(with: .kilo))
            } else {
                sample.weight = 0.0
            }
            array.append(sample)
        }
        
        return array
    }
    
    private func calorieStatsSample(dict : [Date : [HKSample]], period : period) -> [CalorieStatsSample] {
        var array : [CalorieStatsSample] = []
        
        for key in dict.keys {
            let sample = CalorieStatsSample()
            sample.startDate = key
            sample.endDate = self.endDateFrom(period: period, startDate: key)
            var summary = 0.0
            
            let caloriesArray = dict[key]
            
            if (caloriesArray?.count)! > 0 {
                for sampleValue in caloriesArray! {
                    let value = sampleValue as! HKQuantitySample
                    
                    summary = summary + value.quantity.doubleValue(for: HKUnit.kilocalorie())
                }
            }
            
            sample.calories = summary
            
            array.append(sample)
        }
        return array
    }
    
    private func endDateFrom(period : period, startDate : Date) -> Date {
        switch period {
        case .today:
            let endDate = calendar.date(byAdding: Calendar.Component.day, value: 1, to: startDate)
            return endDate!
        case .weekly:
            let endDate = calendar.date(byAdding: Calendar.Component.day, value: 1, to: startDate)
            return endDate!
        case .monthly:
            let endDate = calendar.date(byAdding: Calendar.Component.weekdayOrdinal, value: 1, to: startDate)
            return endDate!
        }
    }
    
    private func avgValueFrom(samples : [HKSample], isWeight : Bool) -> Double {
        let count = samples.count
        var sum = 0.0
        
        for sample in samples {
            let value = sample as! HKQuantitySample
            
            if isWeight == true {
                sum = sum + value.quantity.doubleValue(for: HKUnit.gramUnit(with: .kilo))
            } else {
                sum = sum + value.quantity.doubleValue(for:HKUnit.kilocalorie())
            }
        }
        
        if sum == 0 {
            return sum
        }
        
        return sum / Double(count)
    }
    private func separateSamples(samples : [HKSample], period : period) -> [Date : [HKSample]] {
        
        switch period {
        case .today:
            return [calendar.startOfDay(for: Date()) : samples]
        case .weekly:
            var dict : [Date : [HKSample]] = [:]
            var proxyDate : Date?
            
            for sample in samples {
                let date = sample.startDate
                proxyDate = calendar.startOfDay(for: date)
                
                let keyExists = dict[proxyDate!] != nil
                
                if keyExists == true {
                    var array = dict[proxyDate!]
                    array?.append(sample)
                    dict[proxyDate!] = array
                } else {
                    var array : [HKSample] = []
                    array.append(sample)
                    dict[proxyDate!] = array
                }
            }
            return dict
        case .monthly:
            var dict : [Date : [HKSample]] = [:]
            var proxyWeek : Date?
            
            for sample in samples {
                let date = sample.startDate
                proxyWeek = self.startOfWeekFrom(date: date)
                
                let keyExists = dict[proxyWeek!] != nil
                
                if keyExists == true {
                    var array = dict[proxyWeek!]
                    array?.append(sample)
                    dict[proxyWeek!] = array
                } else {
                    var array : [HKSample] = []
                    array.append(sample)
                    dict[proxyWeek!] = array
                }
                
            }
            return dict
        }
    }
    
    func startOfWeekFrom(date : Date) -> Date {
        return Calendar.current.date(from: Calendar.current.dateComponents([.yearForWeekOfYear, .weekOfYear], from: date))!
    }
/*
    func weekContains(date : Date, week : Date) -> Bool {
        let dateComps = Calendar.current.dateComponents(Calendar.Component.weekOfYear | Calendar.Component.yearForWeekOfYear, from: date)
        let weekComps = Calendar.current.dateComponents(Calendar.Component.weekOfYear | Calendar.Component.yearForWeekOfYear, from: week)
        
        return dateComps == weekComps
    }
*/
    
    func generateWeightGraphDataForPeriod(period : period) {
        
    }
  */
}

//
//  BasePresenterProtocol.swift
//  Nourish
//
//  Created by Nova on 11/16/16.
//  Copyright © 2016 SSIC. All rights reserved.
//

import Foundation
import RxSwift

protocol BasePresenterProtocol {
    func viewWillAppear(_ animated : Bool)
    func viewDidAppear(_ animated : Bool)
    func viewWillDisappear(_ animated : Bool)
    func viewDidDisappear(_ animated : Bool)
    func viewDidLayoutSubviews()
}

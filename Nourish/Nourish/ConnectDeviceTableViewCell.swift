//
//  ConnectDeviceTableViewCell.swift
//  Nourish
//
//  Created by Vino (Vinodha) Sundaramoorthy on 5/18/17.
//  Copyright © 2017 SSIC. All rights reserved.
//

import UIKit

class ConnectDeviceTableViewCell: UITableViewCell {
    @IBOutlet weak var deviceName : UILabel!
    @IBOutlet weak var connectedStatus : UIImageView!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    func setConnectedStatus(status : Bool) {
        let statusImageName : String = status ? "radio_on_green" : "radio_off_green"
        self.connectedStatus.image = UIImage(named: statusImageName)
    }
    
}

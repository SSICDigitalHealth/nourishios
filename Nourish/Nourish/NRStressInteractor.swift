//
//  NRStressInteractor.swift
//  Nourish
//
//  Created by Vino (Vinodha) Sundaramoorthy on 12/5/16.
//  Copyright © 2016 SSIC. All rights reserved.
//

import UIKit
import RxSwift
import HealthKit
let kBPMTreshold = 15.0

class NRStressInteractor: NSObject {
    
    static let shared = NRStressInteractor()
    
    let userActivityRepo = UserActivityRepository()
    let calendar = Calendar.current
    
    var stressLastHour = Variable<stressLevel>(.Undetermined)
    
    func stressFor(date : Date) -> Observable<NRStress> {
        return self.getStressForToday()
    }
    /*
    private func getStressForToday() -> Observable<NRStress> {
        let heartRateSamplesLast2Days = self.userActivityRepo.getUserHeartLastTwoDays()
        let heartRateSampleToday = self.userActivityRepo.getUserHeartForToday()
        let dates = self.datesForLastTwoDay()
        let steps = self.userActivityRepo.getUserSteps(startDate: dates.0, endDate: dates.1)
        let object = Observable.zip(heartRateSamplesLast2Days, heartRateSampleToday, steps ,resultSelector: { lastDays, today, stepsL2day -> NRStress in
            
            var count = 0.0
            var value = 0.0
            var proxyDate : Date? = nil
            
            for sample in lastDays {
                if self.isHighLoadValue(sample: sample, steps: stepsL2day, lastSampleDate: proxyDate) == false {
                    value = value + sample.quantity.doubleValue(for: HKUnit(from: "count/min"))
                    count = count + 1
                }
                proxyDate = sample.endDate
            }
            
            let restingHeartRate = value / count
            
            var stressedSamples : [HKQuantitySample] = []
            var normalSamples : [HKQuantitySample] = []
            var proxySampleDate : Date? = nil
            
            for todaySample in today {
                let proxyValue = todaySample.quantity.doubleValue(for: HKUnit(from: "count/min"))
                if proxyValue > restingHeartRate + kBPMTreshold {
                    if self.isHighLoadValue(sample: todaySample, steps: stepsL2day, lastSampleDate: proxySampleDate) == false {
                        stressedSamples.append(todaySample)
                    } else {
                        normalSamples.append(todaySample)
                    }
                } else {
                    normalSamples.append(todaySample)
                }
                proxySampleDate = todaySample.endDate
                
            }
            let normalIndexes = self.sampleToHourIndexes(samples: normalSamples)
            let stressedIndexes = self.sampleToHourIndexes(samples: stressedSamples)
            
            let model = self.stressModelFrom(normalIndexes: normalIndexes, stressedIndexes: stressedIndexes)
            let score : Double = (today.first?.quantity.doubleValue(for: HKUnit(from: "count/min"))) ?? 0.0
            model.stressScore = score
            self.stressLastHour.value = (model.hourlyStressArray?.last)!
            return model
        })
        return object
    }
 
 */
    func getLastCachedReading() -> Observable<stressLevel> {
        let interactor = StressInteractor()
        let obj = interactor.fetchLastCachedStressSample().flatMap { dailyStress -> Observable<stressLevel> in
            
            var returnValue : stressLevel! = .Undetermined
            if dailyStress.stressValue != nil {
                returnValue = dailyStress.stressValue!
            } else {
                returnValue = .Undetermined
            }
            self.stressLastHour.value = returnValue
            
            return Observable.just(returnValue)
        }
        return obj
    }

    func getStressForToday() -> Observable<NRStress> {
        let interactor = StressInteractor()
        let obj = interactor.dailyStressData(period: .today).flatMap { stressSamples -> Observable<NRStress> in
            
            var normalInd : [DailyStress] = []
            var stressInd : [DailyStress] = []
            
            for sample in stressSamples {
                if sample.stressValue == .Stressed {
                    stressInd.append(sample)
                } else if sample.stressValue == .Normal {
                    normalInd.append(sample)
                }
            }
            let normInd = self.sampleToHourIndexes(samples: normalInd)
            let stressedInd = self.sampleToHourIndexes(samples: stressInd)
            let model = self.stressModelFrom(normalIndexes: normInd, stressedIndexes: stressedInd)
            let lastSample = stressSamples.first
            
            let score = lastSample?.bpmValue
            model.stressScore = score ?? 0.0
            //self.stressLastHour.value = model.hourlyStressArray?.last ?? .Undetermined
            return Observable.just(model)
            
        }
        return obj
    }
    
    
    private func sampleToHourIndexes(samples : [DailyStress]) -> [Int] {
        var indexes : [Int] = []
        for sample in samples {
            indexes.append(self.hourIndexFor(sample: sample))
        }
        return indexes
    }
    
    
    private func hourIndexFor(sample : DailyStress) -> Int {
        let date = sample.dateToShow
        let midnight = calendar.startOfDay(for: date!)
        let difference = calendar.dateComponents([.hour, .minute], from: midnight, to: date!)
        let fullHours = difference.hour
        return fullHours!
    }
    
    private func stressModelFrom(normalIndexes : [Int], stressedIndexes : [Int]) -> NRStress {
        let model = NRStress()
        let fullHours = self.getHourArrayForToday()
        var stressedArray : [stressLevel] = []
        for index in 0...fullHours {
            var stressLevel : stressLevel = .Undetermined
            
            if normalIndexes.contains(index) == true {
                if stressedIndexes.contains(index) {
                    stressLevel = .Stressed
                } else {
                    stressLevel = .Normal
                }
            } else if stressedIndexes.contains(index) {
                stressLevel = .Stressed
            }
            
            stressedArray.append(stressLevel)
            
        }
        model.hourlyStressArray = stressedArray
        return model
    }
    
    private func sampleToHourIndexes(samples : [HKQuantitySample]) -> [Int] {
        var indexes : [Int] = []
        for sample in samples {
            indexes.append(self.hourIndexFor(sample: sample))
        }
        return indexes
    }
    
    
    private func hourIndexFor(sample : HKQuantitySample) -> Int {
        let date = sample.startDate
        let midnight = calendar.startOfDay(for: date)
        let difference = calendar.dateComponents([.hour, .minute], from: midnight, to: date)
        let fullHours = difference.hour
    //    if difference.minute! > 0 {
    //        fullHours = fullHours! + 1
    //    }
        return fullHours!
    }
    
    private func getHourArrayForToday() -> Int {
        let date = Date()
        let midnight = calendar.startOfDay(for: date)
        let difference = calendar.dateComponents([.hour, .minute], from: midnight, to: date)
        let fullHours = difference.hour
    //    if difference.minute! > 0 {
    //        fullHours = fullHours! + 1
    //    }
        return fullHours!
    }
/*
    private func isHighLoadValue(sample : HKQuantitySample, steps : [HKQuantitySample], lastSampleDate : Date?) -> Bool {
        var previousDate : Date? = nil
        
        if lastSampleDate != nil {
            let interval = sample.startDate.timeIntervalSince(lastSampleDate!)
            if interval > 600 {
                previousDate = sample.startDate.addingTimeInterval(-600)
            } else {
                previousDate = lastSampleDate
            }
        } else {
            previousDate = sample.startDate.addingTimeInterval(-600)
        }
        
        var sampleArray : [HKQuantitySample] = []
        
        for step in steps {
            if step.startDate >= previousDate! && step.startDate <= sample.endDate {
                sampleArray.append(step)
            } else if step.endDate >= previousDate! && step.endDate <= sample.endDate {
                sampleArray.append(step)
            } else if step.startDate <= previousDate! && step.endDate >= sample.endDate {
                sampleArray.append(step)
            }
        }
        
        var value = 0.0
        
        for object in sampleArray {
            value += object.quantity.doubleValue(for: HKUnit.count())
        }
        
        if value >= 10 {
            return true
        } else {
            return false
        }
    }
*/    
    private func datesForLastTwoDay() -> (Date , Date) {
        let nowDate = Date()
        let calendar = Calendar.current
        let starDate: Date = calendar.startOfDay(for: nowDate)
        let endDate: Date = calendar.date(byAdding: Calendar.Component.day, value: -1, to: starDate)!
        return (endDate, nowDate)
    }
    
    private func generateRandomStress() -> NRStress {
        let stress = NRStress()
        stress.stressScore = Double(generateRandomBetween(max: 100, min: 0))
        stress.hourlyStressArray = [.Normal,.Stressed,.Normal,.Normal,.Normal,.Normal,.Normal,.Normal,.Normal,.Normal,.Normal,.Stressed,.Normal,.Normal,.Normal,.Normal,.Normal,.Stressed,.Undetermined,.Normal,.Normal,.Normal]
        return stress
    }
    
    private func generateRandomBetween(max : Int, min : Int) -> Int {
        return min + Int(arc4random_uniform(UInt32(max - min + 1)))
    }

}

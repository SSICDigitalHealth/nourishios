//
//  HKSample_to_UserActivity.swift
//  Nourish
//
//  Created by Nova on 11/30/16.
//  Copyright © 2016 SSIC. All rights reserved.
//

import UIKit
import HealthKit

class HKSample_to_UserActivity: NSObject {
    
    func transform(sample : HKSample) -> UserActivity {
        let activity = UserActivity()
        activity.startDate = sample.startDate
        activity.endDate = sample.endDate
        activity.activityType = self.activityTypeFrom(sample: sample)
        return activity
    }

    func transform(array : [HKSample]) -> [UserActivity] {
        var arrayToReturn : [UserActivity] = []
        for sample in array {
            arrayToReturn.append(self.transform(sample: sample))
        }
        return arrayToReturn
    }
    
    
    func activityTypeFrom(sample : HKSample) -> userActivityType? {
        switch sample.sampleType {
        case is HKCategoryType:
            let someObject = sample as? HKCategorySample
            if someObject?.value == HKCategoryValueSleepAnalysis.inBed.rawValue {
                return .UserInBed
            } else {
                return .UserIsSleeping
            }
        case is HKQuantityType:
            return .UserIsActive
        default:
            return nil
        }
    }
    
    
}

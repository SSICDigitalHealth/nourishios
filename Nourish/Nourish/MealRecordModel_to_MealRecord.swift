//
//  MealRecordModel_to_MealRecordObject.swift
//  Nourish
//
//  Created by Nova on 11/15/16.
//  Copyright © 2016 SSIC. All rights reserved.
//

import UIKit

class MealRecordModel_to_MealRecord : NSObject {
    func transform(mealRecord : MealRecordModel) -> MealRecord {
        let mealModel = MealRecord()
        
        //mealModel.caloriesPerServing = mealRecord.portionKCal
        mealModel.ocasion = mealRecord.ocasion ?? .breakfast
        mealModel.date = mealRecord.date
        //mealModel.userFoodId = mealRecord.userFoodId
        mealModel.idString = mealRecord.idString
        mealModel.name = mealRecord.foodTitle
        mealModel.unit = mealRecord.servingType
        mealModel.amount = mealRecord.servingAmmount ?? 1.0
        mealModel.calories = mealRecord.calories ?? 0.0
        mealModel.grams = mealRecord.grams ?? 0.0
        mealModel.isFromNoom = mealRecord.isFromNoom
        mealModel.idString = mealRecord.idString
        
        mealModel.isFavourite = mealRecord.isFavourite
        mealModel.isGroupFavourite = mealRecord.isGroupFavourite
        mealModel.groupedMealArray = mealRecord.groupedMealArray
        mealModel.groupID = mealRecord.groupID
        mealModel.groupName = mealRecord.groupName
        mealModel.noomID = mealRecord.noomID
        mealModel.numberOfItems = mealRecord.numberOfItems
        mealModel.userPhotoId = mealRecord.userPhotoId
        mealModel.groupPhotoID = mealRecord.groupPhotoID
        mealModel.groupFoodUUID = mealRecord.groupFoodUUID
        mealModel.localCacheID = mealRecord.localCacheID
        
        
        return mealModel
    }
    
    func transform(modelArray : [MealRecordModel]) -> [MealRecord] {
        var arrayToReturn : [MealRecord] = []
        for model in modelArray {
            arrayToReturn.append(self.transform(mealRecord: model))
        }
        return arrayToReturn
    }
}

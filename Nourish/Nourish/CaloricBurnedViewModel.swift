//
//  CaloricBurnedViewModel.swift
//  Nourish
//
//  Created by Vlad Birukov on 29.08.17.
//  Copyright © 2017 SSIC. All rights reserved.
//

import Foundation

class CaloricBurnedViewModel {
    var userBurnedCal: Double?
    var avgBurnedCal: Double?
    var avgActiveCal: Double?
    var caloricState: [caloricStateData]?
}

//
//  UIScrollView+Extension.swift
//  Nourish
//
//  Created by Vino (Vinodha) Sundaramoorthy on 2/6/17.
//  Copyright © 2017 SSIC. All rights reserved.
//

import UIKit

extension UIScrollView {
    func scrollToTop() {
        let desiredOffset = CGPoint(x: 0, y: -contentInset.top)
        setContentOffset(desiredOffset, animated: true)
    }
}

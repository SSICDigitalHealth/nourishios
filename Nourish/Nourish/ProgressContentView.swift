//
//  ProgressContentView.swift
//  Nourish
//
//  Created by Andrey Zmushko on 8/2/17.
//  Copyright © 2017 SSIC. All rights reserved.
//

import UIKit

class ProgressContentView: UIView {
    var model: AnyObject?
    var config: ProgressConfig?
    
    func reloadData() {}

    func reloadDataWith(config: ProgressConfig) {
        self.config = config
    }
}

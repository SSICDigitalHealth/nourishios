//
//  MealDiarySenderProtocol.swift
//  Nourish
//
//  Created by Nova on 11/16/16.
//  Copyright © 2016 SSIC. All rights reserved.
//

import Foundation

protocol MealDiarySenderProtocol {
    func showAddFoodControllerForOcasion(ocasion: Ocasion, date: Date,showPredictions:Bool)
    func changeFoodAmountWith(model : foodSearchModel)
    func showGroupFavouriteVC(mealGroup: [MealRecordModel] )
    func showGroupDetailsControllerForOcasion(ocasion:Ocasion,date:Date,model:MealRecordModel)
    func disableNavigationBar()
    func enableNavigationBar()
}

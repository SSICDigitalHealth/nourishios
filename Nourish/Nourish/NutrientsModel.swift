//
//  NutrientsModel.swift
//  Nourish
//
//  Created by Vlad Birukov on 11.09.17.
//  Copyright © 2017 SSIC. All rights reserved.
//

import Foundation

class NutrientsModel: NSObject {
    var nutrientChartData = [NutrientModel]()
    var nutrientDetail = [NutrientDetailInformationModel]()
}

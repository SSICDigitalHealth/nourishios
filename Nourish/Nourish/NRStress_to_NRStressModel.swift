//
//  NRStress_to_NRStressModel.swift
//  Nourish
//
//  Created by Vino (Vinodha) Sundaramoorthy on 12/5/16.
//  Copyright © 2016 SSIC. All rights reserved.
//

import UIKit

class NRStress_to_NRStressModel: NSObject {
    func transform(stress : NRStress) -> NRStressModel {
        let object = NRStressModel()
        object.hourlyStressArray = stress.hourlyStressArray
        object.stressScore = stress.stressScore
        object.stressMetricsLabel = "bpm"
        return object
    }
    
    func transform(modelArray : [NRStress]) -> [NRStressModel] {
        var arrayToReturn : [NRStressModel] = []
        for model in modelArray {
            arrayToReturn.append(self.transform(stress: model))
        }
        return arrayToReturn
    }

}

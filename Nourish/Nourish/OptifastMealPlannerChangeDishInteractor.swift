//
//  OptifastMealPlannerChangeDishInteractor.swift
//  Optifast
//
//  Created by Gena Mironchyk on 1/11/18.
//  Copyright © 2018 SSIC. All rights reserved.
//

import UIKit
import RxSwift
class OptifastMealPlannerChangeDishInteractor: NSObject {
    
    private let userRepo = UserRepository.shared
    
    
    func execute() -> Observable<[String]> {
        return self.userRepo.getCurrentUser(policy: .DefaultPolicy).flatMap{profile -> Observable<[String]> in
            var arrayToReturn = [String]()
            arrayToReturn.append(contentsOf: profile.optifastFoodIds)
            return Observable.just(arrayToReturn)
        }
    }
    
}

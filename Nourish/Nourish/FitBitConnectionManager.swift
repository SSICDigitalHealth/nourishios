//
//  FitBitConnectionManager.swift
//  Nourish
//
//  Created by Vino (Vinodha) Sundaramoorthy on 5/15/17.
//  Copyright © 2017 SSIC. All rights reserved.
//

import UIKit
import SwiftKeychainWrapper

struct NotificationConstants {
    static let fitBitLaunchNotification = "FitBitLaunchotification"
}

protocol AuthenticationProtocol {
    func authorizationDidFinish(_ success :Bool)
}

let kFitBitClientId = "228HWG"
let kFitBitClientSecret = "e6749c8ee0b9689ca50a3066197ad2ad"
let kFitBitBaseUrl = "https://www.fitbit.com/oauth2/authorize"
let kFitBitScope = "sleep+settings+nutrition+activity+social+heartrate+profile+weight+location"
let kFitBitApiUrl = "https://api.fitbit.com/1"
let kFitBitRedirectUri = "com.samsung.nourish://"
let kFitBitRefreshUrl = "https://api.fitbit.com/oauth2/token?"


class FitBitConnectionManager : NSObject {
    static let sharedInstance: FitBitConnectionManager = FitBitConnectionManager()
    static let baseAPIURL = URL(string:kFitBitApiUrl)
    var session: URLSession?
    let userRepository = UserRepository.shared

    //Fit Bit access token
    var accessToken : String? {
        set (newValue) {
            KeychainWrapper.standard.set(newValue!, forKey: kFitBitAccessTokenKey, withAccessibility: KeychainItemAccessibility.afterFirstUnlock)
            UserDefaults.standard.set(newValue, forKey: kFitBitAccessTokenKey)
            UserDefaults.standard.synchronize()
        }
        get {
            let token = UserDefaults.standard.string(forKey: kFitBitAccessTokenKey)
            if token == nil {
                return nil
            } else {
                return UserDefaults.standard.string(forKey: kFitBitAccessTokenKey)
            }
        }
    }
    
    //Refresh Token
    var refreshToken : String? {
        set (newValue) {
            KeychainWrapper.standard.set(newValue!, forKey: kFitBitRefreshTokenKey, withAccessibility: KeychainItemAccessibility.afterFirstUnlock)
        }
        get {
            return KeychainWrapper.standard.string(forKey: kFitBitRefreshTokenKey)
        }
    }
    
    //FitToken Expiration Date
    var tokenExpirationDate : Date? {
        set (newValue) {
            let interval = newValue?.timeIntervalSince1970
            KeychainWrapper.standard.set(interval!, forKey: kFitBitExpirationDateKey, withAccessibility: KeychainItemAccessibility.afterFirstUnlock)
        }
        get {
            let interval = KeychainWrapper.standard.double(forKey: kFitBitExpirationDateKey)
            return Date(timeIntervalSince1970 : interval!)
        }
    }
    
    func authorize(with token: String) {
        let sessionConfiguration = URLSessionConfiguration.default
        var headers = sessionConfiguration.httpAdditionalHeaders ?? [:]
        headers["Authorization"] = "Bearer \(token)"
        sessionConfiguration.httpAdditionalHeaders = headers
        session = URLSession(configuration: sessionConfiguration)
    }
    
    func refreshFitBitToken() {
        LogUtility.logToFile("attempt on fitbit refresh token")
        if self.accessToken != nil && self.refreshToken != nil {
            var dateComps = DateComponents()
            dateComps.weekdayOrdinal = 1
            let endDate = Calendar.current.date(byAdding: dateComps, to: Date())
            if self.tokenExpirationDate! < endDate! {
                self.refreshToken(bearer: self.accessToken!, oldAccessToken: self.refreshToken!, completion: { token in
                    LogUtility.logToFile("New Fit Bit token : \(token)")
                })
            }
        }
    }
    
    func refreshToken(bearer : String, oldAccessToken : String, completion : @escaping (String) -> ()) {
        let stringUrl = kFitBitRefreshUrl + String(format : "grant_type=refresh_token&refresh_token=\(oldAccessToken)")
        let url = URL(string : stringUrl)
        var request = URLRequest(url: url!)
        request.httpMethod = "POST"
        let stringData = "grant_type=refresh_token&refresh_token=\(oldAccessToken)"
        request.httpBody = stringData.data(using: .utf8)
        
        let authString = String(format:"%@:%@",kFitBitClientId,kFitBitClientSecret)
        request.setValue("Basic \(authString.toBase64())", forHTTPHeaderField: "Authorization")
        request.setValue("application/x-www-form-urlencoded", forHTTPHeaderField: "Content-Type")
        
        let session = URLSession.shared.dataTask(with: request) {
            data, response, error in
            guard let responseData = (try? JSONSerialization.jsonObject(with: data!, options: .allowFragments)) as? [String: Any] else {
                return
            }
            
            if responseData["access_token"] != nil {
                FitBitConnectionManager.sharedInstance.accessToken = responseData["access_token"] as? String
            }
            
            if responseData["refresh_token"] != nil {
                FitBitConnectionManager.sharedInstance.refreshToken = responseData["refresh_token"] as? String
            }
            
            if responseData["expires_in"] != nil {
                let doubleTime = responseData["expires_in"] as! Double
                let date = Date().addingTimeInterval(doubleTime - kTimeOffsetSeconds)
                FitBitConnectionManager.sharedInstance.tokenExpirationDate = date
            }
            
            completion(FitBitConnectionManager.sharedInstance.accessToken!)
        }
        session.resume()
    }

}

//
//  CaloricChartView.swift
//  Nourish
//
//  Created by Vlad Birukov on 24.08.17.
//  Copyright © 2017 SSIC. All rights reserved.
//

import UIKit

class CaloricChartView: UIView {

    var overColor = UIColor.black.withAlphaComponent(0.25)
    var consumedColor: UIColor = UIColor.white
    var target: CGFloat = 0.0
    var consumed: CGFloat = 0.0
    
    func setupWith(consumedCalories: Double, targetCalories: Double, consumedColor: UIColor) {
        self.target = CGFloat(targetCalories)
        self.consumed = CGFloat(consumedCalories)
        self.consumedColor = consumedColor
        
        self.setNeedsDisplay()
    }
    
    override func draw(_ rect: CGRect) {
        let heigth = frame.height
        let width = frame.width
        
        if self.consumed > self.target {
            self.drawRectangle(x: 0, y: 0, width: width, heigth: heigth, color: consumedColor)
            let currentWidth = (1 - ((self.consumed - self.target) / self.consumed)) * width
            self.drawRectangle(x: currentWidth, y: 0, width: width - currentWidth, heigth: heigth, color: overColor)
        } else if self.consumed < self.target {
            let currentWidth = width * (self.consumed / self.target)
            self.drawRectangle(x: 0, y: 0, width: currentWidth, heigth: heigth, color: consumedColor)
        }
    }

}

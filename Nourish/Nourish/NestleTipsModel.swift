//
//  NestleTipsModel.swift
//  Nourish
//
//  Created by Gena Mironchyk on 9/11/17.
//  Copyright © 2017 SSIC. All rights reserved.
//

import UIKit

class NestleTipsModel: NSObject {
    var componentId : String?
    var indicator : String?
    var messages : [String]?
    var tipTitle : String?
    var tipType : String?
}

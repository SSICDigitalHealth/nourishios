//
//  StayActiveInteractor.swift
//  Nourish
//
//  Created by Vlad Birukov on 31.08.17.
//  Copyright © 2017 SSIC. All rights reserved.
//

import Foundation
import RxSwift

class StayActiveInteractor {
    let repo = NewActivityRepository.shared
    
    /*
    func execute(startDate: Date, endDate: Date) -> Observable <StayActiveModel> {
        return Observable<StayActiveModel>.create{(observer) -> Disposable in
            let pause = Int(arc4random_uniform(4))
            let dispat = DispatchTime.now() + .seconds(pause)
            DispatchQueue.main.asyncAfter(deadline: dispat, execute: {
                let stayActiveModel = StayActiveModel()
                if Calendar.current.compare(startDate, to: endDate, toGranularity: .day) == .orderedSame {
                    stayActiveModel.currentStepsToGoal = Double(self.generateRandomBetween(max: 8000, min: 2))
                }
                
                observer.onNext(stayActiveModel)
                observer.onCompleted()
            })
            
            return Disposables.create()
        }
    }
    */
    func execute(startDate: Date, endDate: Date) -> Observable <StayActiveModel> {
        return self.repo.stayActiveModel(startDate:startDate, endDate:endDate)
    }
    
    private func generateRandomBetween(max : Int, min : Int) -> Int {
        return min + Int(arc4random_uniform(UInt32(max - min + 1)))
    }

}

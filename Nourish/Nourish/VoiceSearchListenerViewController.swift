//
//  VoiceSearchListenerViewController.swift
//  Nourish
//
//  Created by Vino (Vinodha) Sundaramoorthy on 6/15/17.
//  Copyright © 2017 SSIC. All rights reserved.
//

import UIKit

class VoiceSearchListenerViewController: UIViewController {

    @IBOutlet weak var voiceSearchView : VoiceSearchListenerView!
    var date : Date!
    var delegateVC : UIViewController!

    override func viewDidLoad() {
        super.viewDidLoad()
        voiceSearchView.controller = self
        voiceSearchView.date = self.date
        voiceSearchView.viewDidLoad()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func closeAction(_ sender : UIButton){
        HoundVoiceSearch.instance().stopListening(completionHandler: { (error) in
            if error == nil {
                DispatchQueue.main.async {
                    self.dismiss(animated: true, completion: nil)
                    Hound.clearConversationState()
                }
            }
        })
    }
    
    func showMealListVC(modelArray:[MealRecord],date:Date,ocassion:Ocasion) {
        Hound.clearConversationState()
        HoundVoiceSearch.instance().stopListening(completionHandler: { (error) in
            DispatchQueue.main.async {
                let vc = VoiceMealListViewController(nibName: "VoiceMealListViewController", bundle: nil)
                vc.date = Date()
                vc.mealArray = modelArray
                vc.ocasion = ocassion
                vc.delegate = self.delegateVC as! VoiceMealListDelegate?
                
                self.dismiss(animated: false, completion: {
                    let navigationController = BaseNavigationController.init(rootViewController: vc)
                    self.delegateVC.present(navigationController, animated: false, completion: nil)
                })
            }
        })
    }
    
    func showSearchVC(date:Date,ocassion:Ocasion) {
        DispatchQueue.main.async {
            //Show search screen
            let searchVC = NRFoodSearchViewController(nibName: "NRFoodSearchViewController", bundle: nil)
            searchVC.title = Ocasion.stringDescription(servingType: ocassion).uppercased()
            searchVC.occasion = ocassion
            searchVC.date = date
            searchVC.shouldPredictMeal = false
            searchVC.automaticSearchText = self.voiceSearchView.presenter.unrecognizedWords
            searchVC.showAsModel = true
            
            self.dismiss(animated: false, completion: {
                let navigationController = BaseNavigationController.init(rootViewController: searchVC)
          self.delegateVC.present(navigationController, animated: false, completion: nil)
            })
        }
    }

    
    override var supportedInterfaceOrientations: UIInterfaceOrientationMask {
        return UIInterfaceOrientationMask.portrait
    }
    
    override var shouldAutorotate: Bool {
        return false
    }

}

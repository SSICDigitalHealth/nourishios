//
//  DeleteRecommendationsCacheInteractor.swift
//  Nourish
//
//  Created by Gena Mironchyk on 11/14/17.
//  Copyright © 2017 SSIC. All rights reserved.
//

import UIKit

class DeleteRecommendationsCacheInteractor: NSObject {
    
    private let recommendationsRepository = NestleRecommendationsRepository()
    
    func execute () {
        self.recommendationsRepository.removeAllCache()
    }
}

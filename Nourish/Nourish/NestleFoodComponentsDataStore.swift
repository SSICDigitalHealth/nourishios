//
//  NestleFoodComponentsDataStore.swift
//  Nourish
//
//  Created by Gena Mironchyk on 9/20/17.
//  Copyright © 2017 SSIC. All rights reserved.
//

import UIKit
import RxSwift

private let mainComp = kNourishApiAddress.replacingOccurrences(of: "/api", with: "")
private let foodComponentsUrl = mainComp + "assets/food_components_info.json"


class NestleFoodComponentsDataStore: BaseNestleDataStore {

    func getFoodComponents(userId : String, token : String) -> Observable<[[String : Any]]> {
        return Observable.create{observer in
            let stringURL = foodComponentsUrl + "?userid=" + userId
            let url = URL(string: stringURL)
            var request = self.requestWithToken(token: token, url: url!)
            request.httpMethod = "GET"
            request.setValue("Bearer \(token)", forHTTPHeaderField: "Authorization")
            let task = URLSession.shared.dataTask(with: request) {
                data, response, error in
                
                if error == nil && data != nil {
                    let dataStr = String(data : data!, encoding : .utf8)
                    if let dictionary = self.convertStringToDictionary(text: dataStr!) {
                        observer.onNext(dictionary)
                    }
                } else {
                    if error != nil {
                        observer.onError(error!)
                    }
                }
                self.log(foodProcessingOrWithings: false, url: url?.absoluteString, requestMethod: request.httpMethod, urlResponse: response, requestBody: nil, error: error, responceBody: nil)
                observer.onCompleted()
            }
            task.resume()
            return Disposables.create()
        }
    }
    
    private func convertStringToDictionary(text: String) -> [[String : Any]]? {
        if let data = text.data(using: String.Encoding.utf8) {
            do {
                return try JSONSerialization.jsonObject(with: data, options: []) as? [[String : Any]]
            } catch let error as NSError {
                LogUtility.logToFile(error)
            }
        }
        return nil
    }

    
}

//
//  MealRecordCache_to_MealRecord.swift
//  Nourish
//
//  Created by Nova on 12/14/16.
//  Copyright © 2016 SSIC. All rights reserved.
//

import UIKit
import RealmSwift

class MealRecordCache_to_MealRecord: NSObject {
    
    func transform(cache : MealRecordCache) -> MealRecord {
        let meal = MealRecord()
       
        meal.ocasion = cache.occasionEnum
        meal.grams = cache.grams
        meal.amount = cache.amount
        meal.idString = cache.idString
        meal.name = cache.name
        meal.msreDesc = cache.msreDesc
        meal.msreGrams = cache.msreGrams
        meal.kCal = cache.kCal
        meal.portionKCal = cache.portionKCal
        meal.brand = cache.brand
        meal.score = cache.score
        meal.userFoodId = cache.userFoodId
        meal.calories = cache.calories
        meal.unit = cache.unit
        
        meal.isFavourite = cache.isFavourite
        meal.searchCount = cache.searchCount
        meal.caloriesPerMilliliter = cache.caloriesPerMilliliter
        meal.caloriesPerGramm = cache.caloriesPerGramm
        meal.barCode = cache.barCode
        meal.brandAndName = cache.brandAndName
        //meal.nutrientsFromNoom = self.arrayFrom(list: cache.nutrientsFromNoom)
        meal.foodUid = cache.foodUid
        meal.date = cache.date
        meal.calories = cache.calories
        meal.isFromNoom = cache.isFromNoom
        
        meal.groupName = cache.groupName
        meal.groupID = cache.groupID
        meal.isFavourite = cache.isFavourite
        meal.isGroupFavourite = cache.isGroupFavourite
        meal.numberOfItems = cache.numberOfItems
        meal.userPhotoId = cache.userPhotoId
        meal.groupPhotoID = cache.groupPhotoID
        meal.groupFoodUUID = cache.groupFoodUUID
        meal.localCacheID = cache.localCacheID
        meal.isFromOperation = cache.isFromOperation()
        
        return meal
    }
    
    func transform(cacheList : [MealRecordCache]) -> [MealRecord] {
        var array : [MealRecord] = []
        
        for item in cacheList {
            if item.isInvalidated == false {
                array.append(self.transform(cache: item))
            }
        }
        return array
    }
    
    func arrayFrom(list : List<Nutrient>) -> [Nutrient] {
        var array : [Nutrient] = []
        for object in list {
            array.append(object)
        }
        return array
    }
}

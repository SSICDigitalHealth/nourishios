//
//  MicroelementDetailTableViewCell.swift
//  Nourish
//
//  Created by Vlad Birukov on 16.06.17.
//  Copyright © 2017 SSIC. All rights reserved.
//

import UIKit

class MicroelementDetailTableViewCell: UITableViewCell {

    @IBOutlet weak var nameFood: UILabel!
    @IBOutlet weak var amountMicroelement: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func setupWithModel(model: MicroelementNameViewModel, indexPath: IndexPath, unit: String) {
        if let microelement = model.arrMicroelement {
            
            nameFood.text = String(format: "%@", microelement[indexPath.row].nameElement)
            
            if microelement[indexPath.row].consumedState != nil {
                
                amountMicroelement.text = String(format: "%@ %@", microelement[indexPath.row].consumedState!.roundToTenIfLessOrMore(target: model.target), unit)
            }
            
        }
    }
}

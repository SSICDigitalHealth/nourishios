//
//  LoggedUserEatStatePresenter.swift
//  Nourish
//
//  Created by Vlad Birukov on 09.06.17.
//  Copyright © 2017 SSIC. All rights reserved.
//

import Foundation
import RxSwift


class LoggedUserEatStatePresenter: BasePresenter, UITableViewDataSource,UITableViewDelegate {
    
    let objectToModelMapper = LoggedUserEatStateModel_to_LoggedUserUserEatStateViewModel()
    let interactor = LoggedUserEatStateInteractor()
    var loggedUserEatStateView : LoggedUserEatStateProtocol!
    var sourceCurrentOcasion = LoggedUserEatStateViewModel()
    var date: (Date, Date)?

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.getCurrentOcasion()
    }
    
    private func getCurrentOcasion() {
        if self.loggedUserEatStateView != nil {
            self.date = self.loggedUserEatStateView.config()?.getDate()
        }
        
        if let view = loggedUserEatStateView {
            if self.date != nil {
                let proxyDate = loggedUserEatStateView?.config()?.date

                if Calendar.current.compare((self.date?.0)!, to: (self.date?.1)!, toGranularity: .day) == .orderedSame {
                    self.subscribtions.append(self.interactor.execute(startDate: (self.date?.0)!, endDate: (self.date?.1)!).observeOn(MainScheduler.instance).subscribe(onNext: { [weak self] ocasion in
                        if let instance = self {
                            instance.sourceCurrentOcasion = instance.objectToModelMapper.transform(model: ocasion)
                            if proxyDate == instance.loggedUserEatStateView?.config()?.date {
                                view.reloadTable()
                            }
                        }
                        view.stopActivityAnimation()
                    }, onError: {error in
                        view.stopActivityAnimation()
                        view.parseError(error:error, completion: nil)
                    }, onCompleted: {
                    }, onDisposed: {
                        view.stopActivityAnimation()
                    }))
                }
            }
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return sourceCurrentOcasion.currentOcasion.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "OcasionCell", for: indexPath) as! OcasionTableViewCell
        
        let currentType:Ocasion = {
            switch indexPath.row {
            case 0:
                return .breakfast
            case 1:
                return .lunch
            case 2:
                return .dinner
            default:
                return .snacks
            }
        }()
        
        let currentModel = sourceCurrentOcasion.currentOcasion.filter{ $0.0 == currentType }.first
        
        cell.setupWithModel(model: currentModel!)

        return cell
    }
    
    @IBAction func completeDay(_ sender: Any) {
        print("$a$$$$$$a$$$$$$a$$$$$$a$$$$$$a$$$$$$a$$$$$$a$$$$$$a$$$$$")
    }
}

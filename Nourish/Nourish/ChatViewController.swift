//
//  ChatViewController.swift
//  Nourish
//
//  Created by Gena Mironchyk on 5/4/17.
//  Copyright © 2017 SSIC. All rights reserved.
//

import UIKit

class ChatViewController: BasePresentationViewController, OnboardingHomeViewProtocol, RecognitionViewContollerDelegate , VoiceMealListDelegate {
    
    @IBOutlet weak var chatView: ChatView!
    var globalNavigationButton : UIBarButtonItem!

    var userProfile : UserProfile?
    var awakedFromBackground = false
    
    func recognitionViewControllerDidAddedFood() {
        self.dismiss(animated: true, completion: {
            self.showToast(message: "Item added", showingError: false, completion: nil)
        })
    }
    
    func recognitionViewControllerDidCancel() {
        self.dismiss(animated: true, completion: {
            
        })
    }

    
    // MARK : VoiceMealListDelegate 
    
    func soundHoundViewControllerDidAddedFood() {
        self.dismiss(animated: true, completion: {
            self.showToast(message: "Item added", showingError: false, completion: nil)
        })
    }

    func setUpNavigationBarButtons () {
        self.title = "Chat with Nouri"
        self.navigationController?.navigationBar.barTintColor = NRColorUtility.symplifiedNavigationColor()
        self.navigationController?.navigationBar.backgroundColor = UIColor.white
        self.navigationController?.navigationBar.isTranslucent = false;
        let titleAttribute = self.navigationBarTitleAttribute()
        self.navigationController?.navigationBar.titleTextAttributes = titleAttribute
    }
    
    override func viewDidLoad() {
       // self.view.translatesAutoresizingMaskIntoConstraints = false
        super.viewDidLoad()
        if self.chatView.chatTableView != nil {
            self.chatView.chatTableView!.topAnchor
                .constraint( equalTo: topLayoutGuide.bottomAnchor ).isActive = true
        }
       
        if self.chatView.responceScrollView != nil {
            self.chatView.responceScrollView!.bottomAnchor
                .constraint( equalTo: bottomLayoutGuide.topAnchor ).isActive = true
        }
        
        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(_ animated: Bool) {
        self.baseViews = [self.chatView!]

        if self.userProfile != nil {
            self.chatView.userProfile = self.userProfile!
            self.userProfile = nil
        }
        self.chatView!.viewDidLoad()
        self.chatView.presenter.awakedFromBackgroud = self.awakedFromBackground
        self.awakedFromBackground = false
        self.setUpNavigationBarButtons()
        chatView.homeView = self
        super.viewWillAppear(animated)
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func showScoreFeedback(date : Date) {
        let scoreVC = ScoreFeedbackViewController(nibName: "ScoreFeedbackViewController", bundle: nil)
        scoreVC.providesPresentationContextTransitionStyle = true;
        scoreVC.definesPresentationContext = true;
        scoreVC.modalPresentationStyle=UIModalPresentationStyle.overCurrentContext
        scoreVC.view.backgroundColor = UIColor.black.withAlphaComponent(0.3)
        scoreVC.date = date
        let tabBarController = self.tabBarController! as! NourishTabBarViewController
        tabBarController.present(scoreVC, animated: false, completion: nil)
    }
    
    func openFitBitLogin() {
        let fitBitLoginVC = FitBitViewController()
        let navigationController = UINavigationController.init(rootViewController: fitBitLoginVC)
        self.present(navigationController, animated: false, completion: nil)
    }
    
    func onboardingCompleted () {
        if self.isViewLoaded && (self.view.window != nil) {
           
            NavigationUtility.showTabBar(animated: true)
            UIView.animate(withDuration: 0.3, animations: {
                self.view.layoutIfNeeded()
                let frame : CGRect = (self.chatView.chatTableView?.frame)!
                self.chatView.chatTableView?.frame = CGRect(x: (frame.origin.x), y: (frame.origin.y), width: (frame.size.width), height: (self.chatView.frame.size.height) - CGFloat(kTabBarHeight))
                self.chatView.chatTableView?.layoutIfNeeded()
            })
        }
    }
    
    func hideMenu(){
        if NavigationUtility.selectedViewControllerIndex() == 0 {
            NavigationUtility.hideTabBar(animated: true)
        }
    }
    
    func showGoalChangedInfo() {
        if defaults.value(forKey: kGoalChangedDialogMessage) != nil {
            let alertMessage = defaults.value(forKey: kGoalChangedDialogMessage) as! String
        
            if alertMessage.count > 0 {
                let alertTitle = "Your goal has been adjusted."
                let infoDialogVC = ChatInfoDialogViewController(nibName: "ChatInfoDialogViewController", bundle: nil)
                infoDialogVC.providesPresentationContextTransitionStyle = true;
                infoDialogVC.definesPresentationContext = true;
                infoDialogVC.modalPresentationStyle=UIModalPresentationStyle.overCurrentContext
                infoDialogVC.view.backgroundColor = UIColor.black.withAlphaComponent(0.3)
                infoDialogVC.titleLabel.text = alertTitle
                infoDialogVC.descriptionLabel.text = alertMessage
                self.present(infoDialogVC, animated: false, completion: nil)
                defaults.set("", forKey: kGoalChangedDialogMessage)
            }
        }
    }

}

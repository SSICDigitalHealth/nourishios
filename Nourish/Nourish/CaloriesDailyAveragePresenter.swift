//
//  CaloriesDailyAveragePresenter.swift
//  Nourish
//
//  Created by Vlad Birukov on 20.06.17.
//  Copyright © 2017 SSIC. All rights reserved.
//

import Foundation
import RxSwift


class CaloriesDailyAveragePresenter: BasePresenter {
    let objectToModelMapper = CaloriesDailyAverageModel_to_CaloriesDailyAverageViewModel()
    let interactor =  CaloriesDailyAverageInteractor()
    var caloriesDailyAverageView: CaloriesDailyAverageProtocol!
    var date: (Date, Date)?
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.getCaloriesDailyDetail()
    }
    
    private func getCaloriesDailyDetail() {
        if self.caloriesDailyAverageView.config() != nil {
            self.date = self.caloriesDailyAverageView.config()?.getDate()
        }
        
        if self.date != nil {
            if Calendar.current.compare((self.date?.0)!, to: (self.date?.1)!, toGranularity: .day) != .orderedSame {
                self.subscribtions.append(self.interactor.execute(startDate: (self.date?.0)!, endDate: (self.date?.1)!).observeOn(MainScheduler.instance).subscribe(onNext: { [unowned self] caloriesDetail in
                    self.caloriesDailyAverageView.setInformation(caloriesDailyModel:  self.objectToModelMapper.transform(model: caloriesDetail))
                    self.caloriesDailyAverageView.stopActivityAnimation()
                }, onError: {error in
                    self.caloriesDailyAverageView.stopActivityAnimation()
                    self.caloriesDailyAverageView.parseError(error: error,completion: nil)
                }, onCompleted: {
                }, onDisposed: {
                    self.caloriesDailyAverageView.stopActivityAnimation()
                }))
            }
        }
    }
}

//
//  OccasionMealPlannerViewController.swift
//  Nourish
//
//  Created by Vlad Birukov on 22.08.17.
//  Copyright © 2017 SSIC. All rights reserved.
//

import UIKit

class OccasionMealPlannerViewController: BasePresentationViewController {
    @IBOutlet weak var occasionMealPlannerView: OccasionMealPlannerView!
    @IBOutlet weak var cardMealPlannerNavigationBarView: CardMealPlannerNavigationBarView!
    
    var occasionViewModel: OccasionMealPlannerViewModel?
    
    var titleText: String = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.baseViews = [occasionMealPlannerView, cardMealPlannerNavigationBarView]
        for v in self.baseViews! {
            v.viewDidLoad()
        }
        
        self.occasionMealPlannerView.presenter.occasionMealPlannerViewController = self


        if occasionViewModel != nil {
            self.occasionMealPlannerView.presenter.occasionMealPlannerModel = occasionViewModel
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.automaticallyAdjustsScrollViewInsets = false
        self.setUpNavigationBar()
        super.viewWillAppear(animated)
    }
    
    
    func setUpNavigationBar() {
        if self.navigationController != nil {
            self.cardMealPlannerNavigationBarView.settingNavigation(title: self.titleText)
            self.cardMealPlannerNavigationBarView.favoriteButton.isHidden = false
            self.occasionMealPlannerView.presenter.favoriteButton = self.cardMealPlannerNavigationBarView.favoriteButton
            self.occasionMealPlannerView.presenter.backButton = self.cardMealPlannerNavigationBarView.backButton
        }
    }

}

//
//  StressInteractor.swift
//  Nourish
//
//  Created by Gena Mironchyk on 1/23/17.
//  Copyright © 2017 SSIC. All rights reserved.
//

import UIKit
import RxSwift

class StressInteractor: NSObject {
    
    //func dailyStressData(period: period)
    let activityRepo = UserActivityRepository()
    
    func weeklyStressData(period : period) -> Observable<[WeeklyStress]>{
        return self.fetchWeeklyData()
    }
    
    func fetchLastCachedStressSample() -> Observable<DailyStress> {
        return self.activityRepo.fetchLastCacheSample()
    }
    
    func dailyStressData(period : period) ->Observable<[DailyStress]>{
        return self.fetchBPMValuesForToday()
    }
    
    func fetchWeeklyData() -> Observable<[WeeklyStress]> {
        return self.activityRepo.fetchStressWeekly()
    }
    
    private func fetchBPMValuesForToday() -> Observable<[DailyStress]> {
        return self.activityRepo.fetchStressForToday()
    }
    

    private func generateRandomDailyData()->Observable<[DailyStress]> {
        return Observable.create{observer in
            var dailyStress : [DailyStress] = []
            let count = self.generateRandomBetween(max: 130, min: 100)
            for _ in 1...count{
                dailyStress.append(self.generateDailyStressObject())
            }
            observer.onNext(dailyStress)
            observer.onCompleted()
            return Disposables.create ()
        }

    }
    
    private func generateDailyStressObject() -> DailyStress {
        let stressObj = DailyStress()
        stressObj.bpmValue = Double(self.generateRandomBetween(max: 140, min: 50))
        let indexForStress = self.generateRandomBetween(max: stressLevel.allValues.count - 1, min: 0)
        stressObj.stressValue = stressLevel.allValues[indexForStress]
        let minutes = self.generateRandomBetween(max: 1440, min: 1)
        let day = Calendar.current.startOfDay(for: Date())
        stressObj.dateToShow = Calendar.current.date(byAdding: .minute, value: minutes, to: day)
        return stressObj
    }
    
    private func generateRandomData() -> Observable<[WeeklyStress]> {
        return Observable.create{observer in
            
            var weeklyStress : [WeeklyStress] = []
            let count = self.generateRandomBetween(max: 20, min: 1)
            for _ in 0...count{
                weeklyStress.append(self.generateWeeklyStressObject())
            }
            observer.onNext(weeklyStress)
            observer.onCompleted()
            return Disposables.create ()
        }
    }
    
    private func generateWeeklyStressObject() -> WeeklyStress {
        let stress = WeeklyStress()
        stress.minBPM = Double(self.generateRandomBetween(max: 70, min: 50))
        stress.maxBPM = Double(self.generateRandomBetween(max: 130, min: 100))
        stress.avgBPM = Double(self.generateRandomBetween(max: Int(stress.maxBPM!), min: Int(stress.minBPM!)))
        stress.weeklyArray = self.generateWeeklyArray()
        return stress
    }
    
    private func generateWeeklyArray() -> [weeklyStruct] {
        var array : [weeklyStruct] = []
        let count = 7
        for index in stride(from: count, to: 0, by: -1) {
            array.append(self.generateWeeklyStructForDate(dateToShow: self.dateAppendDaysAgo(daysAgo: index)))
        }

        return array
    }
    
    private func generateWeeklyStructForDate(dateToShow:Date) -> weeklyStruct {
        let weeklyStress = weeklyStruct(dateToShow: dateToShow, hoursArray: self.generateHoursArray())
        return weeklyStress
    }
    
    private func generateHoursArray()-> [hoursStruct] {
        var hoursArray : [hoursStruct] = []
        for index in 0...23 {
            hoursArray.append(self.generateStressForIndex(index: index))
        }
        return hoursArray
    }
    
    private func dateAppendDaysAgo(daysAgo:Int)->Date{
        let today = Date()
        return Calendar.current.date(byAdding: .day, value: -daysAgo, to: today)!
    }
    
    
    private func generateStressForIndex (index: Int) -> hoursStruct {
        
        let indexForStress = self.generateRandomBetween(max: stressLevel.allValues.count - 1, min: 0)
        let hours = hoursStruct(index: index, stressValue:stressLevel.allValues[indexForStress])
        return hours
    }
    
    private func generateRandomBetween(max : Int, min : Int) -> Int {
        return min + Int(arc4random_uniform(UInt32(max - min + 1)))
    }
    
    
    
}

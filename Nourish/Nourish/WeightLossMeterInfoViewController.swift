//
//  WeightLossMeterInfoViewController.swift
//  Nourish
//
//  Created by Vino (Vinodha) Sundaramoorthy on 2/7/17.
//  Copyright © 2017 SSIC. All rights reserved.
//

import UIKit

class WeightLossMeterInfoViewController: BasePresentationViewController {

    @IBOutlet weak var dismissButton : UIButton!
    @IBOutlet weak var tile : UILabel!
    @IBOutlet weak var calorieBudgetLabel : UILabel!
    @IBOutlet weak var activityBudgetLabel : UILabel!
    @IBOutlet weak var calorieDesc : UILabel!
    @IBOutlet weak var activityDesc : UILabel!
    var activityLevel : String!
    var consumedBudget : Double!
    var activityBudget : Double!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tile.textColor = NRColorUtility.nourishTextColor()
        calorieBudgetLabel.text = String(format:"%0.f",consumedBudget)
        activityBudgetLabel.text = String(format:"%0.f",activityBudget)
        self.dismissButton.addTarget(self, action: #selector(self.closeAction(_:)), for: .touchUpInside)
        
        let calDesc = "Your calorie budget/day is calculated based on your height,weight,age,gender,and activity level("
        let calSuffixString = ")."
        let activitySuffixString = ") you selected during on-boarding."
        let actDec = "Your activity recommendation/day is calculated based on the activity level("

        let strAttribute = [ NSForegroundColorAttributeName:NRColorUtility.nourishTextColor() , NSFontAttributeName : UIFont.systemFont(ofSize: 12)]
        let actAttribute = [NSForegroundColorAttributeName:NRColorUtility.nourishTextColor(),NSFontAttributeName: UIFont.systemFont(ofSize: 12)]
       
        let calorie = NSMutableAttributedString(string: calDesc, attributes: strAttribute)
        calorie.append(NSAttributedString(string: activityLevel, attributes: actAttribute))
        calorie.append(NSAttributedString(string: calSuffixString, attributes: strAttribute))
        calorieDesc.attributedText = calorie
        
        
        let activity = NSMutableAttributedString(string: actDec, attributes: strAttribute)
        activity.append(NSAttributedString(string: activityLevel, attributes: actAttribute))
        activity.append(NSAttributedString(string: activitySuffixString, attributes: strAttribute))
        activityDesc.attributedText = activity
    }

}

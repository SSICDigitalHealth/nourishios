//
//  WeightStatsViewController.swift
//  Nourish
//
//  Created by Vino (Vinodha) Sundaramoorthy on 1/9/17.
//  Copyright © 2017 SSIC. All rights reserved.
//

import UIKit

class WeightStatsViewController: BasePresentationViewController {

    @IBOutlet weak var weightStatsView : WeightStatsView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupNavigationBar()
        weightStatsView.controller = self
        self.baseViews = [weightStatsView]
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        self.navigationController?.navigationBar.topItem?.title = "Weight"
    }
    
    func setupNavigationBar() {
        self.title = "Weight"
        self.navigationController?.navigationBar.backItem?.title = ""
        self.navigationController?.navigationBar.setBackgroundImage(NRColorUtility.symplifiedNavigationColor().as1ptImage(), for: .default)
        self.navigationController?.navigationBar.shadowImage = NRColorUtility.symplifiedNavigationColor().as1ptImage()
    }
}

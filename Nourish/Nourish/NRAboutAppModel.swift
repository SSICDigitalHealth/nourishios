//
//  NRAboutAppModel.swift
//  Nourish
//
//  Created by Vino (Vinodha) Sundaramoorthy on 11/29/16.
//  Copyright © 2016 SSIC. All rights reserved.
//

import UIKit

struct aboutAppModel {
    var appVersion : String
    var appName : String
    var appDescription : String
    var appCopyrightsString :String
}

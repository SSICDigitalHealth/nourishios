//
//  UIView+Extension.swift
//  Nourish
//
//  Created by Vino (Vinodha) Sundaramoorthy on 12/15/16.
//  Copyright © 2016 SSIC. All rights reserved.
//

import UIKit

extension UIView {
    func getSnapshotImage() -> UIImage {
        UIGraphicsBeginImageContextWithOptions(bounds.size, isOpaque, 0)
        drawHierarchy(in: bounds, afterScreenUpdates: false)
        let snapshotImage = UIGraphicsGetImageFromCurrentImageContext()!
        UIGraphicsEndImageContext()
        return snapshotImage
    }
    
    func roundCorners(corners:UIRectCorner, radius: CGFloat) {
        let path = UIBezierPath(roundedRect: self.bounds, byRoundingCorners: corners, cornerRadii: CGSize(width: radius, height: radius))
        let mask = CAShapeLayer()
        mask.path = path.cgPath
        self.layer.mask = mask
    }
    
    class func fromNib<T : UIView>() -> T {
        return Bundle.main.loadNibNamed(String(describing: T.self), owner: nil, options: nil)![0] as! T
    }
    
    func addShadow() {
        self.layer.shadowColor = UIColor.lightGray.cgColor
        self.layer.shadowOpacity = 1
        self.layer.shadowOffset = CGSize.zero
        self.layer.shadowRadius = 5
    }
    
    func showBanner(message : String, showingError : Bool, completion:((Bool) -> Void)?) -> UIView {
        let yPosition : CGFloat = self.frame.origin.y == 0 ? 64 : self.bounds.origin.y
        let initialFrame = CGRect(x: 0, y: yPosition, width: UIScreen.main.bounds.size.width, height: 36)
        let bannerView = UIView(frame:initialFrame)
        bannerView.backgroundColor = UIColor.black.withAlphaComponent(0.6)
        
        let labelFrame = CGRect(x:0,y:10,width:UIScreen.main.bounds.size.width - 40,height:25)
        let bannerLabel = UILabel(frame:labelFrame)
        bannerLabel.center = CGPoint(x:UIScreen.main.bounds.size.width/2,y:18)
        bannerLabel.backgroundColor = UIColor.clear
        bannerLabel.textColor = UIColor.white
        bannerLabel.textAlignment = .center;
        bannerLabel.font = UIFont.systemFont(ofSize: 14, weight: UIFontWeightRegular)
        bannerLabel.text = message
        bannerLabel.alpha = 1.0
        bannerView.addSubview(bannerLabel)
        
        let closeButton = UIButton(frame:CGRect(x: bannerLabel.frame.origin.x + bannerLabel.frame.size.width + 2,y:10,width:16,height:16))
        closeButton.setImage(UIImage(named:"searchbar_clear_active"), for: .normal)
        closeButton.addTarget(self, action: #selector(self.dismissBanner(sender:)), for: .touchUpInside)
        bannerView.addSubview(closeButton)
        self.addSubview(bannerView)
        return bannerView
    }
    
    func dismissBanner(sender : UIButton) {
        sender.superview?.removeFromSuperview()
    }
}


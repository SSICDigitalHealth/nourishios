//
//  ReportsInteractor.swift
//  Nourish
//
//  Created by Vino (Vinodha) Sundaramoorthy on 2/8/17.
//  Copyright © 2017 SSIC. All rights reserved.
//

import UIKit
import RxSwift


class ReportsInteractor: NSObject {
    let repo = ReportsRepository()
   /*
    func fetchReports() -> [reportsModel] {
        
        var reportArray : [reportsModel] = []
        
        var date = Date()
        
        for _ in 1...30 {
            
            let active = [activityTime(activeHr: 500, periodString: "12A"),
                          activityTime(activeHr: 100, periodString: "1"),
                          activityTime(activeHr: 50, periodString: "2"),
                          activityTime(activeHr: 0, periodString: "3"),
                          activityTime(activeHr: 50, periodString: "4"),
                          activityTime(activeHr: 100, periodString: "5"),
                          activityTime(activeHr: 10, periodString: "6"),
                          activityTime(activeHr: 100, periodString: "7"),
                          activityTime(activeHr: 20, periodString: "8"),
                          activityTime(activeHr: 50, periodString: "9"),
                          activityTime(activeHr: 100, periodString: "10"),
                          activityTime(activeHr: 90, periodString: "11"),
                          activityTime(activeHr: 70, periodString: "12"),
                          activityTime(activeHr: 100, periodString: "1"),
                          activityTime(activeHr: 18, periodString: "2"),
                          activityTime(activeHr: 50, periodString: "3")]
            
            let meter = weightLossMeter(maxConsumedCal: 1500, maxBurnedCal: 300, userConsumedCal: 50, userBurnedCal: 100, weeklyPlan: 1.8)
            
            var tooMuch : [nutrients] = []
            var justRight : [nutrients] = []
            var tooLittle : [nutrients] = []
            
            let n1 = nutrients()
            n1.name = "Potassium"
            n1.min = 2000
            n1.value = 200
            n1.lhr = 20
            n1.uhr = 3000
            
            let n2 = nutrients()
            n2.name = "Sodium"
            n2.min = 2000
            n2.value = 200
            n2.lhr = 20
            n2.uhr = 3000
            
            let n3 = nutrients()
            n3.name = "Water"
            n3.min = 2000
            n3.value = 200
            n3.lhr = 20
            n3.uhr = 3000
            
            let n4 = nutrients()
            n4.name = "Calories"
            n4.min = 2000
            n4.value = 200
            n4.lhr = 20
            n4.uhr = 3000
            
            let n5 = nutrients()
            n5.name = "Fat Total"
            n5.min = 2000
            n5.value = 200
            n5.lhr = 20
            n5.uhr = 3000
            
            let n6 = nutrients()
            n6.name = "Fat Trans"
            n6.min = 2000
            n6.value = 200
            n6.lhr = 20
            n6.uhr = 3000
            
            let n7 = nutrients()
            n7.name = "Fat Saturated"
            n7.min = 2000
            n7.value = 200
            n7.lhr = 20
            n7.uhr = 3000
            
            let n8 = nutrients()
            n8.name = "Fiber"
            n8.min = 2000
            n8.value = 200
            n8.lhr = 20
            n8.uhr = 3000
            
            let n9 = nutrients()
            n9.name = "Cholesterol"
            n9.min = 2000
            n9.value = 200
            n9.lhr = 20
            n9.uhr = 3000
            
            let n10 = nutrients()
            n10.name = "Omega3"
            n10.min = 2000
            n10.value = 200
            n10.lhr = 20
            n10.uhr = 3000
            
            let n11 = nutrients()
            n11.name = "Omega6"
            n11.min = 2000
            n11.value = 200
            n11.lhr = 20
            n11.uhr = 3000
            
            let n12 = nutrients()
            n12.name = "Fiber"
            n12.min = 2000
            n12.value = 200
            n12.lhr = 20
            n12.uhr = 3000
            
            justRight.append(n1)
            justRight.append(n2)
            justRight.append(n3)
            justRight.append(n4)
            justRight.append(n5)
            justRight.append(n6)
            justRight.append(n7)
            justRight.append(n8)
            justRight.append(n9)
            justRight.append(n10)
            justRight.append(n11)
            justRight.append(n12)
            
            let n13 = nutrients()
            n13.name = "Protein"
            n13.min = 10
            n13.value = 0
            n13.lhr = 0
            n13.uhr = 3000
            
            let n14 = nutrients()
            n14.name = "Magnesium"
            n14.min = 10
            n14.value = 0
            n14.lhr = 0
            n14.uhr = 3000
            
            let n15 = nutrients()
            n15.name = "Calcium"
            n15.min = 10
            n15.value = 0
            n15.lhr = 0
            n15.uhr = 3000
            
            let n16 = nutrients()
            n16.name = "Iron"
            n16.min = 10
            n16.value = 0
            n16.lhr = 0
            n16.uhr = 3000
            
            let n17 = nutrients()
            n17.name = "Iron"
            n17.min = 10
            n17.value = 0
            n17.lhr = 0
            n17.uhr = 3000
            
            let n18 = nutrients()
            n18.name = "Iron"
            n18.min = 10
            n18.value = 0
            n18.lhr = 0
            n18.uhr = 3000
            
            let n19 = nutrients()
            n19.name = "Iron"
            n19.min = 10
            n19.value = 0
            n19.lhr = 0
            n19.uhr = 3000
            
            let n20 = nutrients()
            n20.name = "Iron"
            n20.min = 10
            n20.value = 0
            n20.lhr = 0
            n20.uhr = 3000
            
            let n21 = nutrients()
            n21.name = "Iron"
            n21.min = 10
            n21.value = 0
            n21.lhr = 0
            n21.uhr = 3000
            
            tooLittle.append(n13)
            tooLittle.append(n14)
            tooLittle.append(n15)
            tooLittle.append(n16)
            tooLittle.append(n17)
            tooLittle.append(n18)
            tooLittle.append(n19)
            tooLittle.append(n20)
            tooLittle.append(n21)
            
            let n22 = nutrients()
            n22.name = "Alpha_Carotene"
            n22.min = 0
            n22.value = 200
            n22.lhr = 0
            n22.uhr = 100
            
            let n23 = nutrients()
            n23.name = "Alpha_Carotene"
            n23.min = 0
            n23.value = 200
            n23.lhr = 0
            n23.uhr = 100
            
            let n24 = nutrients()
            n24.name = "Alpha_Carotene"
            n24.min = 0
            n24.value = 200
            n24.lhr = 0
            n24.uhr = 100
            
            let n25 = nutrients()
            n25.name = "Alpha_Carotene"
            n25.min = 0
            n25.value = 200
            n25.lhr = 0
            n25.uhr = 100
            
            let n26 = nutrients()
            n26.name = "Alpha_Carotene"
            n26.min = 0
            n26.value = 200
            n26.lhr = 0
            n26.uhr = 100
            
            let n27 = nutrients()
            n27.name = "Alpha_Carotene"
            n27.min = 0
            n27.value = 200
            n27.lhr = 0
            n27.uhr = 100
            
            tooMuch.append(n22)
            tooMuch.append(n23)
            tooMuch.append(n24)
            tooMuch.append(n25)
            tooMuch.append(n26)
            tooMuch.append(n27)
            
            let dayReport = DailyReportModel(steps: 2356, distance: 1.5, activeHours: 2, sleep: 6, activityArray: active, justRightNutrients: justRight, tooLittleNutrients: tooLittle, tooMuchNutrients: tooMuch, weightProgress: meter)
            
            let report = reportsModel(date: date, report: dayReport)
            date = date.addingTimeInterval(-(24*60*60))
            reportArray.append(report)
        }
        
        return reportArray
    }
    */
    
    func fetchReports() -> Observable<[reportsModel]> {
        return self.repo.fetchReports()
    }
    
    func getReportFor(date : Date) -> Observable<DailyReportModel> {
        return self.repo.getReportFor(date: date)
    }
}

//
//  NutrientChartView.swift
//  Nourish
//
//  Created by Vlad Birukov on 03.08.17.
//  Copyright © 2017 SSIC. All rights reserved.
//

import UIKit

class NutrientChartView: UIView {

    let chartColor = NRColorUtility.hexStringToUIColor(hex: "70C397")
    let overColor = UIColor.red
    
    var upperLimit: CGFloat = 0.0
    var lowerLimit: CGFloat = 0.0
    var consumed: CGFloat = 0.0
    
    func setupWith(consumedNutrient: Double, upperLimit: Double, lowerLimit: Double) {
        self.upperLimit = CGFloat(upperLimit)
        self.lowerLimit = CGFloat(lowerLimit)
        self.consumed = CGFloat(consumedNutrient)
        
        self.setNeedsDisplay()
    }
    
    override func draw(_ rect: CGRect) {
        let heigth = frame.height
        let width = frame.width
        
        if self.consumed > self.upperLimit && self.upperLimit != 0.0 {
            let overflowWidth = width * ((self.consumed - self.upperLimit) / self.upperLimit)
            
            if overflowWidth == 1 {
                self.drawRectangle(x: 0, y: 0, width: width, heigth: heigth, color: UIColor.red)
            
            } else {
                
                self.drawRectangle(x: width - overflowWidth, y: 0, width: overflowWidth, heigth: heigth, color: UIColor.red)
                
                self.drawRectangle(x: 0, y: 0, width: width - overflowWidth, heigth: heigth, color: chartColor)
            }
        } else if self.consumed >= self.lowerLimit && self.consumed <= upperLimit && self.upperLimit != 0.0 {
            
            self.drawRectangle(x: 0, y: 0, width: width, heigth: heigth, color: chartColor)
       
        } else if self.consumed >= self.lowerLimit && self.upperLimit == 0.0 {
        
            self.drawRectangle(x: 0, y: 0, width: width, heigth: heigth, color: chartColor)
        
        } else if self.consumed < self.lowerLimit  {
            
            let currentWidth = width * (self.consumed / self.lowerLimit)
            
            self.drawRectangle(x: 0, y: 0, width: currentWidth, heigth: heigth, color: chartColor)
        }
    }
}

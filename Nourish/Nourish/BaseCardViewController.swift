//
//  BaseCardViewController.swift
//  Nourish
//
//  Created by Andrey Zmushko on 7/20/17.
//  Copyright © 2017 SSIC. All rights reserved.
//

import UIKit

class BaseCardViewController: BasePresentationViewController {
    var progressConfig: ProgressConfig?
    
    
    public enum Style {
        case score
        case other
        
        var background: UIColor {
            switch self {
            default:
                return UIColor.white
            }
        }
        
        var tint: UIColor {
            switch self {
            default:
                return UIColor.black
            }
        }
    }
    
    override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?) {
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
    }
    
    init(_ config: ProgressConfig) {
        super.init(nibName: config.card.nibName, bundle: nil)
        self.progressConfig = config
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    
    func setUpNavigationBar(title: String, style: BaseCardViewController.Style, navigationBar: NewCardNavigationBarView, hideTabBar: Bool = false, controller: UIViewController = BaseViewController()) {
        self.automaticallyAdjustsScrollViewInsets = false
        if navigationController != nil {
            navigationBar.backgroundBar.backgroundColor = style.background
            navigationBar.barTitle.text = title
            navigationBar.barTitle.textColor = style.tint
            
            if hideTabBar == true {
                navigationBar.backButton.addTarget((controller as! RecipesDetailViewController), action: #selector((controller as! RecipesDetailViewController).backActionWithoutShowTabBar), for: .touchUpInside)
            } else {
                navigationBar.backButton.addTarget(self, action: #selector(backAction), for: .touchUpInside)
            }

            navigationBar.layer.borderColor = UIColor.white.cgColor
            navigationBar.layer.borderWidth = 0.1
            navigationBar.layer.shadowColor = UIColor.black.cgColor
            navigationBar.layer.shadowOffset = CGSize(width: 0.0, height: 1.0)
            navigationBar.layer.shadowRadius = 1.5
            navigationBar.layer.shadowOpacity = 0.2
            navigationBar.layer.masksToBounds = false

        }
    }
    
    func backAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
        NavigationUtility.showTabBar(animated: false)
    }
}

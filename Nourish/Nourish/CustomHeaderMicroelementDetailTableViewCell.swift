//
//  CustomHeaderMicroelementDetailTableViewCell.swift
//  Nourish
//
//  Created by Vlad Birukov on 16.06.17.
//  Copyright © 2017 SSIC. All rights reserved.
//

import UIKit

class CustomHeaderMicroelementDetailTableViewCell: UITableViewCell {

    @IBOutlet weak var progressMicroelement: WatchOutChartView!
    @IBOutlet weak var nameMicroelement: UILabel!
    @IBOutlet weak var totalLabel: UILabel!
    @IBOutlet weak var percentCurrentState: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func setupWithModel(model: MicroelementBalanceViewModel, section: Int) {
        let unit = ["tsp", "g", "mg"]
        
        if model.arrMicroelementName != nil {
            nameMicroelement.text = model.arrMicroelementName?[section].type
        }
        
        let(consumed, target) = calculatCurentState(model: model, section: section)
        
        let color = consumed <= target ? UIColor.black : UIColor.white
        
        progressMicroelement.setupWith(Data: (consumed, target))
        
        percentCurrentState.textColor = color
        percentCurrentState.text = percentCalculation(consumedMicroelement: consumed, targetMicroelement: target)
        
        if consumed > target {
            totalLabel.textColor = NRColorUtility.overConsumedColor()
        } else {
            totalLabel.textColor = NRColorUtility.hexStringToUIColor(hex: "303030")
        }
        totalLabel.text = String(format: "%@/%@ %@", consumed.roundToTenIfLessOrMore(target: target), target.roundToTenIfLessOrMore(target: target), unit[section])

    }
    
    private func calculatCurentState(model: MicroelementBalanceViewModel, section: Int) -> (Double, Double) {
        var consumed = 0.0
        var target = 0.0
        
        if model.arrMicroelementName?[section] != nil {
            if model.arrMicroelementName?[section].arrMicroelement != nil {
                consumed = (model.arrMicroelementName?[section].arrMicroelement?.reduce(0, {$0 + $1.consumedState!}))!
            }
            target = (model.arrMicroelementName?[section].target)!
        }
        return (consumed, target)
    }

    private func percentCalculation(consumedMicroelement: Double, targetMicroelement: Double) -> String {
        var percentMicroelemt: String = "0%"
        
        if targetMicroelement != 0.0{
            percentMicroelemt = String(format: "%.0f%@",(consumedMicroelement/targetMicroelement * 100.0), "%")
        }
        
        return percentMicroelemt
    }
}

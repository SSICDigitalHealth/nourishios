//
//  RecomendationDetailsPresenter.swift
//  Nourish
//
//  Created by Andrey Zmushko on 7/31/17.
//  Copyright © 2017 SSIC. All rights reserved.
//

import Foundation
import RxSwift


class RecomendationDetailsPresenter : BasePresenter, UITableViewDataSource, UITableViewDelegate {
    var recomendationDetailModel : RecomendationDetailViewModel?
    var config: ProgressConfig?
    var recomendationDetailViewController: RecomendationViewController?
    
    @IBOutlet weak var detailView: BaseView!
    @IBOutlet weak var detailTableView: UITableView!
    let interactor = RecomendationDetailInteractor()
    let favInteractor = FavouritesInteractor()
    let objectToModelMapper = RecomendationDetailModel_to_RecomendationDetailViewModel()
    var date: (Date, Date)?
    var needReloadData = true
    var selectedCell: IndexPath?
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.registerTableViewCells()
        self.settingTableView()
        self.getRecomendationDetail()
    }
    
    private func getRecomendationDetail() {
        if config != nil {
            self.date = config?.getDate()
        }
        
        if self.date != nil {
            self.subscribtions.append(self.interactor.execute(startDate: (self.date?.0)!, endDate: (self.date?.1)!).observeOn(MainScheduler.instance).subscribe(onNext: { [unowned self] recomendationModel in
                self.recomendationDetailModel = self.objectToModelMapper.transform(model: recomendationModel)
                if self.needReloadData == true {
                    self.detailTableView.reloadData()
                }
                self.detailView.stopActivityAnimation()
            }, onError: {error in
                self.detailView.stopActivityAnimation()
            }, onCompleted: {
                self.refreshInformationFavorite()
                self.needReloadData = true
            }, onDisposed: {
                self.detailView.stopActivityAnimation()
            }))
        }
    }
    
    func registerTableViewCells() {
        self.detailTableView.register(UINib(nibName: "RecomendationHeaderTableViewCell", bundle: nil), forCellReuseIdentifier: "cellHeader")
        self.detailTableView?.register(UINib(nibName: "RecomendationHeaderViewCell", bundle: nil), forCellReuseIdentifier: "Header")
        self.detailTableView?.register(UINib(nibName: "RecomendationCell", bundle: nil), forCellReuseIdentifier: "Cell")
        self.detailTableView.register(UINib(nibName: "RecipiesTableViewCell", bundle: nil), forCellReuseIdentifier: "cellRecipies")
    }
    
    func settingTableView() {
        self.detailTableView.rowHeight = UITableViewAutomaticDimension
        self.detailTableView.estimatedRowHeight = 44
        self.detailTableView.sectionHeaderHeight = UITableViewAutomaticDimension
        self.detailTableView.estimatedSectionHeaderHeight = 44
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        if self.recomendationDetailModel != nil {
            return self.recomendationDetailModel?.dataRecipies != nil ? 3 : 2
        } else {
            return 1
        }
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if self.recomendationDetailModel != nil && section == 1 {
            return (self.recomendationDetailModel?.data?.count)!
        } else if self.recomendationDetailModel?.dataRecipies != nil && section == 2 {
            return (self.recomendationDetailModel?.dataRecipies?.count)!
        } else {
            return 0    
        }
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        if section == 0 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "cellHeader") as! RecomendationHeaderTableViewCell
            if self.recomendationDetailModel != nil {
                cell.setupWith(header: (self.recomendationDetailModel?.titleText)!, description: (self.recomendationDetailModel?.descriptionText)!)
            }
            return cell.contentView
        } else {
            let cell = tableView.dequeueReusableCell(withIdentifier: "Header") as! RecomendationHeaderViewCell
            
            if let model = self.recomendationDetailModel {
                cell.setupWith(model: model, section: section)
            }
            
            return cell.contentView
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if indexPath.section == 1 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as! RecommendationCell

            if let model = self.recomendationDetailModel {
                cell.setupWith(model: model, row: indexPath.row)
            }

            return cell
        } else {
            let recipiesCell = tableView.dequeueReusableCell(withIdentifier: "cellRecipies", for: indexPath) as! RecipiesTableViewCell
            if let data = self.recomendationDetailModel?.dataRecipies?[indexPath.row] {
                recipiesCell.setupWith(model: data, present: self, indexPath: indexPath)
            }
            
            return recipiesCell
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.selectedCell = indexPath

        if indexPath.section == 2 && self.recomendationDetailModel?.dataRecipies != nil {
            if let controller = self.recomendationDetailViewController {
                if let navigation = controller.navigationController {
                    if let data = self.recomendationDetailModel?.dataRecipies?[indexPath.row] {
                        self.needReloadData = false
                        let newController = RecipesDetailViewController()
                        newController.titleNavigationBar = data.nameFood
                        newController.recipiesViewModel = data
                        navigation.pushViewController(newController, animated: true)
                    }
                }
            }
        } else {
            if let model = self.recomendationDetailModel {
                if let data = model.data {
                    let cell = self.detailTableView.cellForRow(at: indexPath) as! RecommendationCell
                    self.detailView.prepareLoadView()

                    if data[indexPath.row].isFavorite == false {
                        var foodSearchModel = data[indexPath.row]
                        foodSearchModel.foodTitle = data[indexPath.row].foodTitle.capitalized
                        self.selectedCell = indexPath
                        self.addFavorite(model: foodSearchModel, cell: cell, cellRecepies: nil)
                        cell.isFavorite(favorite: true)
                    } else {
                        if data[indexPath.row].groupID != nil {
                            self.selectedCell = indexPath
                            self.deleteFavorite(groupId: data[indexPath.row].groupID!, cell: cell, cellRecepies: nil)
                            cell.isFavorite(favorite: false)
                        }
                    }
                }
            }
        }
    }
    
    func transformRecipiesViewModel_to_foodSearchModel(model: RecipiesViewModel) -> foodSearchModel {
        var foodsearchModel = foodSearchModel()
        
        foodsearchModel.amount = model.amount
        foodsearchModel.foodId = model.foodId
        foodsearchModel.unit = model.unit
        foodsearchModel.grams = model.grams
        foodsearchModel.mealIdString = model.foodId
        foodsearchModel.foodTitle = model.nameFood
        foodsearchModel.calories = String(model.numberCal)
        
        return foodsearchModel
    }
    
    func addFavorite(model: foodSearchModel, cell: RecommendationCell?, cellRecepies: RecipiesTableViewCell?) {
        self.subscribtions.append(self.favInteractor.addRecToFavourites(searchModel: model, name: UUID().uuidString).subscribe(onNext: {success in
            if success == false {
                if cell != nil {
                    cell?.isFavorite(favorite: false)
                } else {
                    cellRecepies?.isFavorite(favorite: false)
                }
            }
            
        }, onError: {error in
            DispatchQueue.main.async {
                self.detailView.parseError(error: error, completion: nil)
            }
        }, onCompleted: {
            self.refreshInformationFavorite()
        }, onDisposed: {}))
    }
    
    func deleteFavorite(groupId: String, cell: RecommendationCell?, cellRecepies: RecipiesTableViewCell?) {
        self.subscribtions.append(self.favInteractor.deleteFavouriteGroup(groupID: groupId).subscribe(onNext: {success in
            if success == false {
                if cell != nil {
                    cell?.isFavorite(favorite: true)
                } else {
                    cellRecepies?.isFavorite(favorite: true)
                }
            }
        }, onError: {error in
            DispatchQueue.main.async {
                self.detailView.parseError(error: error, completion: nil)
            }
        }, onCompleted: {
            self.refreshInformationFavorite()
        }, onDisposed: {}))

    }
    
    func refreshInformationFavorite() {
        if config != nil {
            self.date = config?.getDate()
        }
        
        if self.date != nil {
            self.interactor.refreshCacheFavorite()
            self.subscribtions.append(self.interactor.execute(startDate: (self.date?.0)!, endDate: (self.date?.1)!).observeOn(MainScheduler.instance).subscribe(onNext: { [unowned self] recomendationModel in
                    self.recomendationDetailModel = self.objectToModelMapper.transform(model: recomendationModel)
            }, onError: {error in
            }, onCompleted: {
                self.reloadTableViewCellData ()
            }, onDisposed: {
            }))
        }
    }
    
    private func reloadTableViewCellData() {
        if let indexPath = self.selectedCell {
            if indexPath.section == 2 {
                if let model = self.recomendationDetailModel?.dataRecipies?[indexPath.row] {
                    if let cell = self.detailTableView.cellForRow(at: indexPath) as? RecipiesTableViewCell {                            cell.setupWith(model: model, present: self, indexPath: indexPath)
                    }
                }
            } else {
                if let model = self.recomendationDetailModel?.data?[indexPath.row] {
                    if let cell = self.detailTableView.cellForRow(at: indexPath) as? RecommendationCell {
                        if model.isFavorite != nil {
                            cell.isFavorite(favorite: model.isFavorite!)
                        }
                    }
                }
            }
            
            self.detailView.stopActivityAnimation()
            self.selectedCell = nil
        }
    }
}

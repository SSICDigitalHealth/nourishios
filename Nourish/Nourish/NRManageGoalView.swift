//
//  NRManageGoalView.swift
//  Nourish
//
//  Created by Vino (Vinodha) Sundaramoorthy on 11/23/16.
//  Copyright © 2016 SSIC. All rights reserved.
//

import UIKit

class NRManageGoalView: BaseView , NRManageGoalsViewProtocol,UITextFieldDelegate {
    
    @IBOutlet weak var weightLabel : UILabel!
    @IBOutlet weak var startWeight : UITextField!
    @IBOutlet weak var goalWeight : UITextField!
    @IBOutlet weak var goalLabel : UILabel!
    @IBOutlet weak var changeGoalButton : UIButton!
    @IBOutlet weak var placeholderButton : UIButton!
    @IBOutlet weak var goalView : UIView!
    @IBOutlet weak var minGoalValueLabel : UILabel!
    @IBOutlet weak var goalValueLabel : UILabel!
    @IBOutlet weak var startWeightLineView : UIView!
    @IBOutlet weak var goalValueLineView : UIView!
    
    
    @IBOutlet weak var updateGoalButton : UIButton!

    var userModel : UserProfileModel!
    var previousCurrentValue : Float =  0
    var sliderValue : Float = 0
    var startAngle : Float = 0
    var goal : UserGoal! = nil
    var controller : NRManageGoalViewController!
    var presenter = NRManageGoalsPresenter()
    var hasChanges : Bool = false
    var manuallySetMaintainGoal : Bool = false
    var segments : [Segment] = [
        Segment(color: NRColorUtility.fitnessPieColor(), value: 25 , imageName:kFitnessIcon,description :kFitnessDesc),
        Segment(color: NRColorUtility.lifeStylePieColor(), value: 20 , imageName: kLifeStyleIcon,description:kLifeStyleDesc),
        Segment(color: NRColorUtility.nutritionPieColor(), value: 55 , imageName:kFoodIcon,description: kNutritionDesc)
    ]
    
    
    override func viewWillAppear(_ animated: Bool) {
        self.basePresenter = presenter
        self.updateGoalButton.backgroundColor = NRColorUtility.appBackgroundColor()
        self.addDoneButtonOnKeyboard()
        self.changeGoalButton.isHidden = true
        self.updateGoalButton.isHidden = true
        #if !OPTIFASTVERSION
            self.changeGoalButton.addTarget(self, action:#selector(self.showGoalOptionsListWithSelectedModel), for: .touchUpInside)
            self.placeholderButton.addTarget(self, action:#selector(self.showGoalOptionsListWithSelectedModel), for: .touchUpInside)
            self.changeGoalButton.isHidden = false
            self.updateGoalButton.isHidden = false
        #endif
        presenter.goalsView = self
        super.viewWillAppear(animated)
    }
    
    func refreshWithGoal(goal:UserGoal) {
        hasChanges = true
        self.goal = goal
        if goal == .MaintainWeight {
            self.manuallySetMaintainGoal = true
        }
    }
    
    func setUserProfile(model:UserProfileModel) {
        #if !OPTIFASTVERSION
            self.setUserProfileForNourIQ(model: model)
        #else
            self.setUserProfileModelForOptifast(model: model)
        #endif
    }
    
    private func setUserProfileModelForOptifast(model : UserProfileModel) {
        DispatchQueue.main.async {
            self.userModel = model
            if self.userModel.weightToMaintain == 0.0 || self.manuallySetMaintainGoal == true || self.userModel.weightToMaintain == nil {
                self.userModel.weightToMaintain = self.userModel.weight
            }
            self.goalLabel.text = UserGoal.MaintainWeight.descriptionUI
            self.weightLabel.text = "Weight to Maintain:"
            let isMetric = self.userModel.metricPreference?.isMetric
            let metricString = isMetric! ? "Kg" : "lbs"
            if self.userModel.weightToMaintain != nil {
                self.startWeight.text = self.userModel.metricPreference?.isMetric == true ? String(format:"%0.f %@",self.userModel.weightToMaintain!,metricString) : String(format:"%0.f %@",  NRConversionUtility.kiloToPounds(valueInKilo: self.userModel.weightToMaintain!),metricString)
            } else {
                self.startWeight.text = self.userModel.metricPreference?.isMetric == true ? String(format:"%0.f %@",self.userModel.weight!) : String(format:"%0.f %@",  NRConversionUtility.kiloToPounds(valueInKilo: self.userModel.weight!),metricString)
            }
            self.minGoalValueLabel.isHidden = true
            self.goalWeight.isHidden = true
            self.goalValueLabel.isHidden = true
            self.goalValueLineView.isHidden = true
            if model.metricPreference?.isMetric == true {
                self.minGoalValueLabel.text = "Min. 2 kg"
            } else {
                self.minGoalValueLabel.text = "Min. 5 lbs"
            }
            self.stopActivityAnimation()
        }
    }
    
    private func setUserProfileForNourIQ(model:UserProfileModel) {
        DispatchQueue.main.async {
            self.userModel = model
            let goal = self.goal != nil ? self.goal : self.userModel.userGoal
            self.goalLabel.text = goal?.descriptionUI
            self.weightLabel.text = goal == UserGoal.MaintainWeight ? "Weight to Maintain:": "Current Weight:"
            if goal == UserGoal.LooseWeight {
                self.weightLabel.text = "Start Weight:"
            }
            let isMetric = self.userModel.metricPreference?.isMetric
            let metricString = isMetric! ? "Kg" : "lbs"
            
            if goal == UserGoal.LooseWeight { // Lose Weight
                self.minGoalValueLabel.isHidden = false
                self.goalWeight.isHidden = false
                self.goalValueLabel.isHidden = false
                self.goalValueLineView.isHidden = false
                
                self.startWeight.text = self.userModel.metricPreference?.isMetric == true ? String(format:"%0.f %@",self.userModel.startWeight!,metricString) : String(format:"%0.f %@",NRConversionUtility.kiloToPounds(valueInKilo: self.userModel.startWeight!),metricString)
                
                if self.userModel.startWeight == self.userModel.goalValue || self.userModel.startWeight! - self.userModel.goalValue! < 2 {
                    self.userModel.goalValue = self.userModel.startWeight! - 2
                }
                
                self.goalWeight.text = self.userModel.metricPreference?.isMetric == true ? String(format:"%0.f %@",self.userModel.goalValue!,metricString) : String(format:"%0.f %@",NRConversionUtility.kiloToPounds(valueInKilo: self.userModel.goalValue!),metricString)
                
            } else if goal == .MaintainWeight { // Maintain Weight
                if self.userModel.weightToMaintain == 0.0 || self.manuallySetMaintainGoal == true {
                    self.userModel.weightToMaintain = self.userModel.weight
                }
                self.manuallySetMaintainGoal = false
                if self.userModel.weightToMaintain != nil {
                    self.startWeight.text = self.userModel.metricPreference?.isMetric == true ? String(format:"%0.f %@",self.userModel.weightToMaintain!,metricString) : String(format:"%0.f %@",  NRConversionUtility.kiloToPounds(valueInKilo: self.userModel.weightToMaintain!),metricString)
                } else {
                    self.startWeight.text = self.userModel.metricPreference?.isMetric == true ? String(format:"%0.f %@",self.userModel.weight!) : String(format:"%0.f %@",  NRConversionUtility.kiloToPounds(valueInKilo: self.userModel.weight!),metricString)
                }
                self.goalWeight.delegate = self
                self.minGoalValueLabel.isHidden = true
                self.goalWeight.isHidden = true
                self.goalValueLabel.isHidden = true
                self.goalValueLineView.isHidden = true
                
            } else { // Eat Right
                self.startWeight.text = self.userModel.metricPreference?.isMetric == true ? String(format:"%0.f %@",self.userModel.weight!,metricString) : String(format:"%0.f %@",  NRConversionUtility.kiloToPounds(valueInKilo: self.userModel.weight!),metricString)
                self.minGoalValueLabel.isHidden = true
                self.goalWeight.isHidden = true
                self.goalValueLabel.isHidden = true
                self.goalValueLineView.isHidden = true
            }
            
            if model.metricPreference?.isMetric == true {
                self.minGoalValueLabel.text = "Min. 2 kg"
            } else {
                self.minGoalValueLabel.text = "Min. 5 lbs"
            }
            
            self.stopActivityAnimation()
        }
    }
    
    
    func validateForSave() -> Bool {
        self.doneButtonAction()
        let goal = self.getCurrentGoal()
        
        if goal == UserGoal.LooseWeight {
            if self.userModel.startWeight == 0 || self.userModel.goalValue == 0 {
                DispatchQueue.main.async {
                    //Show alert
                    let alert = UIAlertController(title: "", message: "Please enter start weight & goal weight.", preferredStyle: UIAlertControllerStyle.alert)
                    
                    // add an action (button)
                    alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
                    // show the alert
                    self.controller.present(alert, animated: true, completion: nil)
                }
                return false
            }
        }
        return true
    }
    
    func saveGoalPreference() {
        if self.validateForSave() {
            let goal = self.getCurrentGoal()
            self.userModel.userGoal = goal
            if goal != .MaintainWeight {
                self.userModel.weightToMaintain = 0.0
            }
            presenter.userModel = self.userModel
            hasChanges = false
            self.prepareLoadView()
            presenter.saveCurrentUser()
        }
    }
    
    func showGoalOptionsListWithSelectedModel() {
        self.controller.showGoalOptionsListWithSelectedModel(goal: self.userModel.userGoal!)
    }
    
    func getCurrentGoal() -> UserGoal {
        if goal != nil {
            return goal
        } else {
            return self.userModel.userGoal!
        }
    }

    func showToast(message:String) {
        DispatchQueue.main.async {
            self.controller.showToast(message: message, showingError: false, completion: nil)
        }
    }
    
    // MARK: UITextFieldDelegate Implementation
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if textField == self.startWeight {
            self.startWeightLineView.backgroundColor = NRColorUtility.appBackgroundColor()
        } else {
            let currWeight = self.userModel.metricPreference?.isMetric == true ? String(format:"%0.f",self.userModel.goalValue!) : String(format:"%0.f",NRConversionUtility.kiloToPounds(valueInKilo: self.userModel.goalValue!))
            self.goalWeight.text = currWeight
            self.goalValueLineView.backgroundColor = NRColorUtility.appBackgroundColor()
        }
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        if textField.text != nil && textField.text!.count != 0 && Double(textField.text!) == nil {
            self.showInvalidValueError(textField : textField)
        } else {
            if textField == self.goalWeight {
                let text : String = textField.text!
            
                if text.count > 0 {
                    self.userModel.startWeight = self.userModel.weight
                    let isMetric = self.userModel.metricPreference?.isMetric
                    let currWeight = isMetric == true ? String(format:"%0.f",self.userModel.weight!) : String(format:"%0.f",NRConversionUtility.kiloToPounds(valueInKilo: self.userModel.weight!))
                    let metricString = isMetric! ? "Kg" : "lbs"
                    let startWeight : Double = Double(currWeight)!
                        var weight : Double = Double(text)!
                        if self.userModel.metricPreference?.isMetric == true {
                            if startWeight - weight <= 2 {
                                weight = startWeight - 2
                            } else {
                                weight = weight < startWeight ?  weight : startWeight - 2
                            }
                        } else {
                            if startWeight - weight <= 5 {
                                weight = startWeight - 5
                            } else {
                                weight = weight < startWeight ?  weight : startWeight - 5            }
                    }
        
                        let currentWeight : Double = (text.count) > 0 ? (isMetric)! == true ? weight : NRConversionUtility.poundsToKil(valueInPounds: weight) : 0
        
                    //Check for minimum target weight limit
                    let bmiIndex = currentWeight / (self.userModel.height! * self.userModel.height!)
            
                    if bmiIndex < 18.5 {
                        DispatchQueue.main.async {
                            //Show alert
                            let alert = UIAlertController(title: "", message: "The target weight you selected is not healthy. Please select a higher target weight.", preferredStyle: UIAlertControllerStyle.alert)
                    
                            // add an action (button)
                            alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
                        // show the alert
                            self.controller.present(alert, animated: true, completion: nil)
                        }
                    } else {
                        if self.userModel.goalValue != currentWeight {
                           hasChanges = true
                            self.userModel.goalValue = currentWeight
                            textField.text = String(format:"%0.f",weight)
                        }
                    }
                    self.goalWeight.text = isMetric == true ? String(format:"%0.f %@",self.userModel.goalValue!,metricString) : String(format:"%0.f %@",NRConversionUtility.kiloToPounds(valueInKilo: self.userModel.goalValue!),metricString)
                }
                self.goalValueLineView.backgroundColor = NRColorUtility.hexStringToUIColor(hex: "#efefef")

            } else {
                let goal = self.getCurrentGoal()
                let text = textField.text
                let weightValue = Double(text!)
                let isMetric = self.userModel.metricPreference?.isMetric
                let currentWeight : Double = (text?.count)! > 0 ? (isMetric! ? weightValue : NRConversionUtility.poundsToKil(valueInPounds: weightValue!))! : 0
           
                if goal == UserGoal.MaintainWeight {
                    self.userModel.weightToMaintain = currentWeight
                } else {
                    self.userModel.weight = currentWeight
                }
                self.startWeightLineView.backgroundColor = NRColorUtility.hexStringToUIColor(hex: "#efefef")
            }
        }
    }
    
    private func showInvalidValueError(textField : UITextField) {
        let alert = UIAlertController(title: "Error", message: "Please enter a valid value", preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Dismiss", style: .default, handler: {action in
            textField.becomeFirstResponder()
            
        }))
        self.controller.present(alert, animated: true, completion: nil)
    }
    
    func textFieldShouldEndEditing(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    func addDoneButtonOnKeyboard()
    {
        let doneToolbar: UIToolbar = UIToolbar(frame:CGRect(x: 0, y: 0, width: 320, height: 50))
        doneToolbar.barStyle = UIBarStyle.blackTranslucent
        
        let flexSpace = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil)
        let done: UIBarButtonItem = UIBarButtonItem(title: "Done", style: UIBarButtonItemStyle.done, target: self, action:  #selector(doneButtonAction))
        
        var items : [UIBarButtonItem] = []
        items.append(flexSpace)
        items.append(done)

        doneToolbar.items = items
        doneToolbar.sizeToFit()
        
        self.startWeight.inputAccessoryView = doneToolbar
        self.goalWeight.inputAccessoryView = doneToolbar
    }
    
    func doneButtonAction()
    {
        self.startWeight.resignFirstResponder()
        self.goalWeight.resignFirstResponder()
    }
    
}

//
//  GroupFoodViewProtocol.swift
//  Nourish
//
//  Created by Vino (Vinodha) Sundaramoorthy on 3/8/17.
//  Copyright © 2017 SSIC. All rights reserved.
//

import UIKit

protocol GroupFoodViewProtocol : BaseViewProtocol {
    func loadNutrientDetails()
    func loadGroupMealDetails()
    func getGroupIndexPathForCell(cell:UITableViewCell) -> IndexPath?
    func showFoodDetails(model : foodSearchModel)
    func finishSaveToDiary()
    func setFoodImage(image : UIImage)
    func hideFoodImage()
    func popToMealDiary()
}

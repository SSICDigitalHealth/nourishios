//
//  NRManageGoalsModel.swift
//  Nourish
//
//  Created by Vino (Vinodha) Sundaramoorthy on 11/23/16.
//  Copyright © 2016 SSIC. All rights reserved.
//

import UIKit

typealias goalOptionsList = (goal : UserGoal, isSelected:Bool)

enum goalCellConfig : Int {
    case goal
    case weight
    case goalWheel
    
    static func description (goal : goalCellConfig) -> String {
        var desc = ""
        
        if goal == .weight {
            desc = "WEIGHT (Pounds)"
        }
        
        return desc
    }
}

enum goals : String {
    case LoseWeight
    case EatBetter
    case MaintainWeight
    static let allValues = [LoseWeight,EatBetter]
    
    static func description(goal : goals) -> String {
        switch goal {
        case .LoseWeight:
            return "Lose Weight"
        case .EatBetter:
            return "Eat Right"
        case .MaintainWeight:
            return "Maintain Weight"
        }
    }
    
    static func goalConfiguration(goal : UserGoal) -> [goalCellConfig] {
        switch goal {
        case .LooseWeight:
            return [goalCellConfig.goal,goalCellConfig.weight,goalCellConfig.goalWheel]
        case .EatBetter:
            return [goalCellConfig.goal,goalCellConfig.goalWheel]
        case .MaintainWeight:
            return [goalCellConfig.goal, goalCellConfig.weight, goalCellConfig.goalWheel]
        }
    }
}






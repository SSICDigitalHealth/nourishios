//
//  ChatViewProtocol.swift
//  Nourish
//
//  Created by Gena Mironchyk on 2/9/17.
//  Copyright © 2017 SSIC. All rights reserved.
//
import UIKit
protocol ChatViewProtocol : BaseViewProtocol {
    func updateChat()
    func updateUserResponces(responces : [UIView])
    func updateUserResponcesWithChartView(responses : [UIView])
    func onboardingFinished ()
    func hideResponseView ()
    func clearResponseView (responses : [UIView])
    func hideTable ()
    func showScoreFeedback(date : Date)
    func openFitBitLogin()
    func insertRowAtIndexPath(path : [IndexPath],fromUser : Bool )
    func hideSplashScreen()
    func showSplashScreen()
    func showGoalChangedInfo()
}

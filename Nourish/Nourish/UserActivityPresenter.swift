//
//  UserActivityPresenter.swift
//  Nourish
//
//  Created by Nova on 11/21/16.
//  Copyright © 2016 SSIC. All rights reserved.
//

import UIKit
import RxSwift

class UserActivityPresenter: BasePresenterProtocol {
    internal func viewDidLayoutSubviews() {
    }

    
    let interactor = UserActivityInteractor()
    let mapperObject = UserActivity_to_UserActivityModel()
    let mapperModel = UserActivityModel_to_UserActivity()
    var activityView : UserActivityViewProtocol?
    var dailySteps : Double = 0
    var activityModels : [UserActivityModel] = []
    
    func viewWillAppear(_ animated : Bool) {
        if activityView != nil {
            /*
            //Added to test the view rendering , can be removed on integration with repository
            self.activityModels = self.mapperObject.transform(objectArray: self.interactor.userActivity())
            self.activityView?.setupWith(activitiesArray: self.activityModels)
            self.activityView?.setSteps(steps:5520)
            */
            
            let _ = self.interactor.userActivitiesForToday().observeOn(MainScheduler.instance).subscribe(onNext:{ [unowned self] activities in
                self.activityModels = self.mapperObject.transform(objectArray: activities)
            }, onError: {error in
                LogUtility.logToFile(error)
                DispatchQueue.main.async {
                    self.activityView?.parseError(error : error, completion: nil)
                }
            }, onCompleted: {
            self.activityView?.setupWith(activitiesArray: self.activityModels)
                let _ = self.interactor.receiveUserStepsForToday().observeOn(MainScheduler.instance).subscribe(onNext: { [unowned self] steps in
                    self.dailySteps = steps
                }, onError: {error in
                    DispatchQueue.main.async {
                        self.activityView?.parseError(error : error, completion: nil)
                    }
                    LogUtility.logToFile(error)
                }, onCompleted: {
                    self.activityView?.setSteps(steps : self.dailySteps)}, onDisposed: {})
                LogUtility.logToFile("Activity View completed")
            }, onDisposed: {})
            
        }
    }
    
    func viewDidAppear(_ animated : Bool) {
    }
    
    func viewWillDisappear(_ animated : Bool) {
    }
    
    func viewDidDisappear(_ animated : Bool) {
    }
}

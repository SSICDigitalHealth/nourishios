//
//  NRWeightProgress.swift
//  Nourish
//
//  Created by Vino (Vinodha) Sundaramoorthy on 12/6/16.
//  Copyright © 2016 SSIC. All rights reserved.
//

import UIKit

class NRWeightProgress: NSObject {
    var progressPercentage : Double = 0.0
    var currentWeight : Double = 0.0
    var metrics : String = ""
    var weightProgress : Double = 0.0
    var dailyWeightLogged : Bool = false
    var userId : String?

}

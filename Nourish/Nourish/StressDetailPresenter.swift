//
//  StressDetailPresenter.swift
//  Nourish
//
//  Created by Vlad Birukov on 17.08.17.
//  Copyright © 2017 SSIC. All rights reserved.
//

import Foundation
import RxSwift


class StressDetailPresenter: BasePresenter, UITableViewDataSource, UITableViewDelegate {
    let interactor = StressInteractor()
    let mapperObjectDaily = DailyStress_to_DailyStressModel()
    var stressViewController: NewStressViewController?
    
    var dailyData = [DailyStressModel]()
    let heightHeaderCell = 110
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.stressViewController?.stressDetailView.detailStressTableView.dataSource = self
        self.stressViewController?.stressDetailView.detailStressTableView.delegate = self
        self.getCurrentStressState()
    }
    
    private func getCurrentStressState() {
        self.subscribtions.append(self.interactor.dailyStressData(period: .today).observeOn(MainScheduler.instance).subscribe(onNext: { [unowned self] dailyArray in
            let arrayWithData : [DailyStressModel] = self.mapperObjectDaily.transform(array: dailyArray)
            self.stressViewController?.stressCaptionView.setupWith(model: arrayWithData)
            self.dailyData = arrayWithData.sorted(by:{ $0.dateToShow!.compare($1.dateToShow!) == ComparisonResult.orderedDescending })
        }, onError: {error in
            self.stressViewController?.stressDetailView.stopActivityAnimation()
            self.stressViewController?.stressCaptionView.stopActivityAnimation()
            self.stressViewController?.stressDetailView.parseError(error: error,completion: nil)
            self.stressViewController?.stressCaptionView.parseError(error: error, completion: nil)
        }, onCompleted: {
            self.stressViewController?.stressDetailView.reloadTable()
            self.stressViewController?.stressDetailView.stopActivityAnimation()
            self.stressViewController?.stressCaptionView.stopActivityAnimation()
        }, onDisposed: {
            self.stressViewController?.stressDetailView.stopActivityAnimation()
            self.stressViewController?.stressCaptionView.stopActivityAnimation()
        }))
    }
    
    public func numberOfSections(in tableView: UITableView) -> Int {
        return dailyData.count != 0 ? 1 : 0
    }
    
    public func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return CGFloat(self.heightHeaderCell)
    }
    
    public func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let  headerCell = tableView.dequeueReusableCell(withIdentifier: "cellHeaderStress") as! HeaderStressTableViewCell
        if dailyData.count == 1 {
            headerCell.bottomLine.backgroundColor = UIColor.clear
        }
        
        let currentBpm = self.dailyData.first
        headerCell.setupWith(model: currentBpm!)
        
        
        
        return headerCell
    }
    
    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.dailyData.count != 0 ? (self.dailyData.count - 1) : 0
    }
    
    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cellStressDetail", for: indexPath) as! StressDetailTableViewCell
        if indexPath.row == dailyData.count - 2 {
            cell.bottomLine.backgroundColor = UIColor.clear
        } else {
            cell.bottomLine.backgroundColor = UIColor.black 
        }
        
        let currentBpm = self.dailyData[indexPath.row + 1]
        cell.setupWith(model: currentBpm)
        
        return cell
    }
    
    func showActivityView() {
        if self.stressViewController != nil {
            self.stressViewController?.navigationController?.popViewController(animated: true)
            NavigationUtility.showTabBar(animated: false)
        }
    }

}

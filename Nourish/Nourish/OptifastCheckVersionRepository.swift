//
//  OptifastCheckVersionRepository.swift
//  Optifast
//
//  Created by Gena Mironchyk on 2/28/18.
//  Copyright © 2018 SSIC. All rights reserved.
//

import Foundation
import RxSwift
class OptifastCheckVersionRepository {
    
    private let toModelMapper = Json_to_OptifastCheckVerModel()
    private let checkVerDataStore = OptifastCheckVerDataStore()
    
    func checkVersion() -> Observable<OptifastCheckVerModel> {
        return self.checkVerDataStore.checkVersion().flatMap{dict -> Observable<OptifastCheckVerModel> in
            let model = self.toModelMapper.transform(dict: dict)
            return Observable.just(model)
        }
    }
    
}

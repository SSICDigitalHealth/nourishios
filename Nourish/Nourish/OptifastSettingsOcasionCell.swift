//
//  OptifastSettingsOcasionCell.swift
//  Nourish
//
//  Created by Gena Mironchyk on 10/18/17.
//  Copyright © 2017 SSIC. All rights reserved.
//

import UIKit

class OptifastSettingsOcasionCell: UITableViewCell {
    
    @IBOutlet weak var checkMarkButton : UIButton!
    @IBOutlet weak var ocasionTitle : UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func setupWith(ocasionTitle : String, isSelected : Bool) {
        self.ocasionTitle.text = ocasionTitle
        self.checkMarkButton.isSelected = isSelected
    }
    
}

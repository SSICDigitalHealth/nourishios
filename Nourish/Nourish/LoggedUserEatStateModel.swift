//
//  LoggedUserEatStateModel.swift
//  Nourish
//
//  Created by Vlad Birukov on 06.06.17.
//  Copyright © 2017 SSIC. All rights reserved.
//

import Foundation
typealias ocasionList = (Ocasion, Double)
class LoggedUserEatStateModel: NSObject {
    var currentOcasion = [ocasionList]()
}

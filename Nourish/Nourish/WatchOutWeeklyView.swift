//
//  WatchOutWeeklyView.swift
//  Nourish
//
//  Created by Andrey Zmushko on 8/2/17.
//  Copyright © 2017 SSIC. All rights reserved.
//

import UIKit

class WatchOutWeeklyView: ProgressContentView/*, ProgressBarChartDelegate*/ {

    @IBOutlet weak var sugarsView: ProgressBarChart!
    @IBOutlet weak var saltView: ProgressBarChart!
    @IBOutlet weak var satFatView: ProgressBarChart!
    
    let sugarsChartDelegate = ProgressBarChartMicroelementsDelegate()
    let saltChartDelegate = ProgressBarChartMicroelementsDelegate()
    let saturatedFatChartDelegate = ProgressBarChartMicroelementsDelegate()
    
    override func reloadDataWith(config: ProgressConfig) {
        super.reloadDataWith(config: config)
        
        if config.period == .weekly {
            let pattern = (CGFloat(0.33), CGFloat())
            self.sugarsView.widthBarPattern = pattern
            self.saltView.widthBarPattern = pattern
            self.satFatView.widthBarPattern = pattern
        }
        for chart in [self.sugarsView, self.saltView, self.satFatView] {
            chart?.delimiterMultiplier = 0.2
        }
        
        if self.model != nil {
            self.sugarsChartDelegate.setupWith(Model: self.model as! MicroelementViewModel , config: config, type: .sugars)
            self.saturatedFatChartDelegate.setupWith(Model: self.model as! MicroelementViewModel , config: config, type: .saturatedFat)
            self.saltChartDelegate.setupWith(Model: self.model as! MicroelementViewModel , config: config, type: .salt)
        }
        
        sugarsView.reloadData(DataSource: self.sugarsChartDelegate)
        satFatView.reloadData(DataSource: self.saturatedFatChartDelegate)
        saltView.reloadData(DataSource: self.saltChartDelegate)
    }
        
}

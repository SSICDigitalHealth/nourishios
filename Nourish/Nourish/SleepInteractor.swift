
//
//  SleepInteractor.swift
//  Nourish
//
//  Created by Vlad Birukov on 31.08.17.
//  Copyright © 2017 SSIC. All rights reserved.
//

import Foundation
import RxSwift

class SleepInteractor {
    let repo = NewActivityRepository.shared
    
    func execute(startDate: Date, endDate: Date) -> Observable <SleepModel> {
        return self.repo.sleepModel(startDate:startDate, endDate:endDate)
    }
    
    private func generateRandomBetween(max : Int, min : Int) -> Int {
        return min + Int(arc4random_uniform(UInt32(max - min + 1)))
    }
    
}

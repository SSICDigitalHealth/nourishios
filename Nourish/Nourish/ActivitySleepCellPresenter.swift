//
//  ActivitySleepCellPresenter.swift
//  Nourish
//
//  Created by Andrey Zmushko on 8/25/17.
//  Copyright © 2017 SSIC. All rights reserved.
//

import UIKit
import RxSwift


class ActivitySleepCellPresenter: ActivityBaseCellPresenter {
    
    let interactor = SleepInteractor()
    let mapperObjectSleep = SleepModel_to_SleepViewModel()
    var sleepViewModel = SleepViewModel()
    
    private func getInformation(cell: ActivityBaseTableViewCell, startDate: Date, endDate: Date) {
        cell.baseView.viewWillAppear(true)
        cell.baseView.removeActivityIndicator()
        self.subscribtions.append(self.interactor.execute(startDate: startDate, endDate: endDate).observeOn(MainScheduler.instance).subscribe(onNext: { [unowned self] sleepActive in
            self.sleepViewModel = self.mapperObjectSleep.transform(model: sleepActive)
        }, onError: {error in
            cell.baseView.parseError(error: error, completion: nil)
        }, onCompleted: {
            self.setupViewForData(cell: cell)
            cell.baseView.stopActivityAnimation()
        }, onDisposed: {
        }))
    }
    
    override func fetchModel(For basecell: ActivityBaseCell) {
        super.fetchModel(For: basecell)
        
        let cell = basecell as! ActivityBaseTableViewCell
        
        cell.iconImageView.imageNamedWithTint(named: "sleep_icon", tintColor: UIColor.white)
        cell.iconFrame.backgroundColor = NRColorUtility.hexStringToUIColor(hex: "1172B9")
        
        let title = NSMutableAttributedString(string: "Sleep", attributes: cell.boldAttr)
        cell.titleLabel.attributedText = title
        
        
        if let delegate = self.delegate {
            let date = delegate.fetchEpoch()
            self.getInformation(cell: cell, startDate: date.startDate, endDate: date.endDate)
            
            if Calendar.current.compare(date.startDate, to: date.endDate, toGranularity: .day) != .orderedSame {
                cell.descriptionLabel.text = "Avg. sleep/day"
                cell.bottomGap.constant = 0
            }
        }
    }
    
    private func setupViewForData(cell: ActivityBaseTableViewCell) {
        if let sleepHours = self.sleepViewModel.sleepHours {
            cell.valueLabel.attributedText = self.calculateHoursAndMins(numberHours: sleepHours, fitstAtrr: cell.boldAttr, secondAtrr: cell.bookAttr)
        }
        
        if let avgSleep = self.sleepViewModel.avgSleepDay {
            if let delegate = self.delegate {
                let date = delegate.fetchEpoch()
                
                if Calendar.current.compare(date.startDate, to: date.endDate, toGranularity: .day) != .orderedSame {
                    
                    cell.descriptionValueLabel.attributedText = self.calculateHoursAndMins(numberHours: avgSleep, fitstAtrr: cell.discriptionAttr)
                }
            
            }
        }
        
    }
    
    func calculateHoursAndMins(numberHours: Double, fitstAtrr: [String : Any], secondAtrr: [String : Any] = ActivityBaseCell().discriptionAttr) -> NSMutableAttributedString {
        let hours = Int(floor(numberHours))
        let mins = Int((numberHours * 60).truncatingRemainder(dividingBy: 60).roundToPlaces(places: 0))
        let sleepString = NSMutableAttributedString(string:"", attributes: fitstAtrr)
        
        if hours > 0 || mins == 0 {
            sleepString.append(NSAttributedString(string: hours > 0 ? String(format: "%d",hours) : "0", attributes: fitstAtrr))
            let actUnitString = " hr"
            sleepString.append(NSAttributedString(string: actUnitString, attributes: secondAtrr))
        }
        
        if mins > 0 {
            sleepString.append(NSAttributedString(string: String(format: " %d",mins), attributes: fitstAtrr))
            sleepString.append(NSAttributedString(string: " min", attributes: secondAtrr))
        }
        
        return sleepString
    }
}

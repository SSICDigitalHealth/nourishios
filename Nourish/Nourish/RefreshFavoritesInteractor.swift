//
//  RefreshFavoritesInteractor.swift
//  Nourish
//
//  Created by Nova on 7/14/17.
//  Copyright © 2017 SSIC. All rights reserved.
//

import UIKit

class RefreshFavoritesInteractor: NSObject {
    let repo = FavoritesRepository()
    
    func execute() {
        self.repo.refreshCacheFavorites()
    }
}

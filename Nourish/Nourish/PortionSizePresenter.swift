//
//  PortionSizePresenter.swift
//  Nourish
//
//  Created by Gena Mironchyk on 10/12/17.
//  Copyright © 2017 SSIC. All rights reserved.
//

import UIKit
struct Section {
    var nameSection : String
    var details : [ComponentDetail]
    var sectionImage : UIImage
    var collapsed : Bool
    
    init(nameSection: String, details: [ComponentDetail], sectionImage:UIImage, collapsed: Bool = false) {
        self.nameSection = nameSection
        self.details = details
        self.sectionImage = sectionImage
        self.collapsed = collapsed
    }
}

class ComponentDetail : NSObject {
    var foodName : String?
    var foodImage : UIImage?
    var looksLikeImage : UIImage?
    var descriptionText : String?
}

class PortionSizePresenter: BasePresenter, UITableViewDataSource,UITableViewDelegate, PortionSizeHeaderDelegate {
    var detailedTable : PortionSizeViewProtocol!
    var sections = [Section]()
    
    var lastSelected : Int?
    
    override func viewWillAppear(_ animated: Bool) {
    
        self.loadPortionModel()
    }
    
    private func loadPortionModel () {
        let titles = ["Vegetables", "Fruits", "Protein", "Dairy", "Grains"/*, "Sweets"*/]
        let pictures = [UIImage(named: "Veg"),UIImage(named: "Fruit"),UIImage(named: "Protein"),UIImage(named: "Dairy"),UIImage(named: "Grains")/*,UIImage(named: "icon_food_sweets")*/]
        self.sections.removeAll()
        for index in 0...titles.count-1 {
            let sect = Section(nameSection: titles[index], details: self.createDetailsFor(name : titles[index]), sectionImage: pictures[index]!)
            self.sections.append(sect)
        }
    }
    
    
    
    
    private func createDetailsFor(name : String) -> [ComponentDetail] {
        var arrayOfNames : [String]
        var arryOfDescriptions : [String]
        var arrayOfPictures : [UIImage]
        var arrayOfLooksLikePictures : [UIImage]
        var details = [ComponentDetail]()
        switch name {
        case "Vegetables":
            arrayOfNames = ["BROCOLLI", "CARROT","POTATO", "CORN"]
            arryOfDescriptions = ["1 cup of broccoli = 1 baseball", "1 cup of carrot = 1 baseball","1 cup of potato = 1 computer mouse","1 Corn = 1 pencil"]
            arrayOfPictures = [#imageLiteral(resourceName: "portion_broccoli"),#imageLiteral(resourceName: "portion_carrots"),#imageLiteral(resourceName: "portion_potato"),#imageLiteral(resourceName: "portion_corn")]
            arrayOfLooksLikePictures = [#imageLiteral(resourceName: "object_baseball"),#imageLiteral(resourceName: "object_baseball"),#imageLiteral(resourceName: "object_mouse"),#imageLiteral(resourceName: "object_pencil")]
            break
        case "Fruits":
            arrayOfNames = ["BANANA","BERRIES","DRIED FRUIT"]
            arryOfDescriptions = ["1 banana = 1 pencil","1 cup of berries = 1 baseball","¼ cup or 1 oz dried fruit = 1 golf ball"]
            arrayOfPictures = [#imageLiteral(resourceName: "portion_banana"),#imageLiteral(resourceName: "portion_blueberries"),#imageLiteral(resourceName: "portion_dried_apricot")]
            arrayOfLooksLikePictures = [#imageLiteral(resourceName: "object_pencil"),#imageLiteral(resourceName: "object_baseball"),#imageLiteral(resourceName: "object_golf_ball")]
            break
        case "Protein":
            arrayOfNames = ["NUTS", "BEANS","SLICE OF MEAT", "MEAT"/*,"FISH"*/]
            arryOfDescriptions = ["¼ cup of nuts = 1 golf ball", "½ cup of beans = 2 golf balls","1 slice of meat = 1 CD","3 oz of meat = 1 deck of cards"/*, "1 Fish fillet = 1 dollar bill"*/]
            arrayOfPictures = [#imageLiteral(resourceName: "portion_peanuts"),#imageLiteral(resourceName: "portion_beans"),#imageLiteral(resourceName: "portion_steak_alternative"),#imageLiteral(resourceName: "portion_steak_alternative")/*,#imageLiteral(resourceName: "portion_fish")*/]
            arrayOfLooksLikePictures = [#imageLiteral(resourceName: "object_golf_ball"),#imageLiteral(resourceName: "Golf Balls"),#imageLiteral(resourceName: "object_cd"),#imageLiteral(resourceName: "object_cards_deck")/*,#imageLiteral(resourceName: "object_dollar")*/]
            break
        case "Dairy":
            arrayOfNames = ["CHEESE","YOUGURT"]
            arryOfDescriptions = ["1 slice of cheese = 1 slice of sandwich bread","1 cup of yogurt = 1 baseball"]
            arrayOfPictures = [#imageLiteral(resourceName: "portion_cheese"),#imageLiteral(resourceName: "portion_yogurt")]
            arrayOfLooksLikePictures = [#imageLiteral(resourceName: "object_toast"),#imageLiteral(resourceName: "object_baseball")]
            break
        default:
            arrayOfNames = ["CEREAL", "PASTA ON PLATE","WAFFLE / PANCAKE", "BAGEL"]
            arryOfDescriptions = ["1 cup of cereal = 1 baseball", "1 cup of pasta on plate = 1 baseball","1 waffle / pancake = 1 CD","1 bagel = 6 oz can of tuna"]
            arrayOfPictures = [#imageLiteral(resourceName: "portion_cereal_cup"),#imageLiteral(resourceName: "portion_pasta"),#imageLiteral(resourceName: "portion_pancake"),#imageLiteral(resourceName: "portion_bagel")]
            arrayOfLooksLikePictures = [#imageLiteral(resourceName: "object_baseball"),#imageLiteral(resourceName: "object_baseball"),#imageLiteral(resourceName: "object_cd"),#imageLiteral(resourceName: "object_tuna_can")]
            break
        }
        
        for index in 0...arrayOfNames.count - 1 {
            let detail = ComponentDetail()
            detail.foodName = arrayOfNames[index]
            detail.descriptionText = arryOfDescriptions[index]
            detail.foodImage = arrayOfPictures[index]
            detail.looksLikeImage = arrayOfLooksLikePictures[index]
            
            details.append(detail)
        }
        
        return details
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.sections[section].collapsed ? 1 : 0
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return self.sections.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var cell = tableView.dequeueReusableCell(withIdentifier: portionDetailsCellIdentifier) as? PortionSizeDetailsCell
        if cell == nil {
            cell = PortionSizeDetailsCell()
        }
        let comps = self.sections[indexPath.section].details
        cell!.setupWith(details: comps)
        return cell!
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        var portionCell = tableView.dequeueReusableHeaderFooterView(withIdentifier: portionSizeComponentsCellIdentifier) as? PortionSizeComponentCell
        if portionCell == nil {
            portionCell = PortionSizeComponentCell(reuseIdentifier : portionSizeComponentsCellIdentifier)
        }
        let sectionData = self.sections[section]
        portionCell!.setupWith(sectionData : sectionData)
        portionCell!.setCollapsed(collapsed: self.sections[section].collapsed)
        portionCell!.section = section
        portionCell!.delegate = self
        return portionCell
    }
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return self.sections[indexPath.section].collapsed ? 255.0 : 0
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 58.0
    }
    

    
    func toggleSection(header: PortionSizeComponentCell, section: Int) {
        let collapsed = !self.sections[section].collapsed
        

        self.sections[section].collapsed = collapsed
        header.setCollapsed(collapsed: collapsed)
        
        self.detailedTable.reloadSectionAt(index : section)
        
        if  self.lastSelected != nil && self.lastSelected != section {
            self.sections[lastSelected!].collapsed = false
            self.detailedTable.reloadSectionAt(index: self.lastSelected!)
        }
        
        self.lastSelected = section
    }
    
   
}

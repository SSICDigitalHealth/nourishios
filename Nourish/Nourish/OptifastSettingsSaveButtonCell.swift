//
//  OptifastSettingsSaveButtonCell.swift
//  Nourish
//
//  Created by Gena Mironchyk on 10/19/17.
//  Copyright © 2017 SSIC. All rights reserved.
//

import UIKit

class OptifastSettingsSaveButtonCell: UITableViewCell {

    @IBOutlet weak var savePrefsButton : UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}

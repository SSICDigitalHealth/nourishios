//
//  CalorificFoodModel_to_CalorificFoodViewModel.swift
//  Nourish
//
//  Created by Vlad Birukov on 29.08.17.
//  Copyright © 2017 SSIC. All rights reserved.
//

import Foundation

class CalorificFoodModel_to_CalorificFoodViewModel {
    func transform(model: CalorificFoodModel) -> CalorificFoodViewModel {
        let calorificFoodViewModel = CalorificFoodViewModel()
        if model.arrCaloriesFood != nil {
            calorificFoodViewModel.arrCaloriesFood = [ElementViewModel]()
            calorificFoodViewModel.arrCaloriesFood = self.transform(model: model.arrCaloriesFood!)
        }
        return calorificFoodViewModel
    }
    
    func transform(model: [ElementModel]) -> [ElementViewModel] {
        var elements = [ElementViewModel]()
        for element in model {
            let elementViewModel = ElementViewModel()
            elementViewModel.nameElement = element.nameElement

            if element.unit != nil{
                elementViewModel.unit = element.unit
            }
            
            if element.consumedState != nil {
                elementViewModel.consumedState = element.consumedState
            }
            
            if element.amountElement != nil {
                elementViewModel.amountElement = element.amountElement
            }
            elements.append(elementViewModel)
        }
        return elements
    }
}

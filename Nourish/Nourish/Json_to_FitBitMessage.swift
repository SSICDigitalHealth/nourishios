//
//  Json_to_FitBitMessage.swift
//  Nourish
//
//  Created by Vino (Vinodha) Sundaramoorthy on 7/26/17.
//  Copyright © 2017 SSIC. All rights reserved.
//

import UIKit

class Json_to_FitBitMessage: NSObject {
    
    func transform(dict :[String : Any]) -> FitBitMessage {
        let message = FitBitMessage()
    
        let destinationDateFormater = DateFormatter()
        destinationDateFormater.timeZone = TimeZone.current
        destinationDateFormater.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZ"
        destinationDateFormater.dateStyle = .full
        destinationDateFormater.timeStyle = .full

        let sourceFormat = DateFormatter()
        sourceFormat.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZ"
        sourceFormat.timeZone = TimeZone(identifier: "UTC")
        sourceFormat.dateStyle = .full
        sourceFormat.timeStyle = .full
        
        let timestamp = dict["ts"] as! Double
        let date = Date(timeIntervalSince1970 : timestamp/1000.0)
        let dateString = sourceFormat.string(from: date)

        message.date = destinationDateFormater.date(from: dateString)

        let artikTimestamp = dict["cts"] as! Double
        let artikDate = Date(timeIntervalSince1970 : artikTimestamp/1000.0)
        let addedDateString = sourceFormat.string(from: artikDate)
        message.dateAddedToArtik = destinationDateFormater.date(from: addedDateString)
        
        let data = dict["data"] as! [String:Any]
        
        if data["stepCount"] != nil {
            message.messageType = "Activity"
            message.stepCount = data["stepCount"] as! Double
            
            let veryActive : Double = data["veryActiveMinutes"] as! Double
            let lightlyActive : Double = data["lightlyActiveMinutes"] as! Double
            let fairlyActive : Double = data["fairlyActiveMinutes"] as! Double

            message.activeMinutes = veryActive + lightlyActive + fairlyActive
        }
        
        if data["caloriesBMR"] != nil {
            message.caloriesBMR = data["caloriesBMR"] as! Double
        }
        
        if data["activityCalories"] != nil {
            message.activeEnergyBurned = data["activityCalories"] as! Double
        }
        
        if data["distancesTotal"] != nil {
            message.distance = data["distancesTotal"] as! Double
        }
        
        if data["asleepTime"] != nil {
            message.messageType = "Sleep"
            message.sleepTime = data["asleepTime"] as! Double
        }
        
        if data["activitiesHeart"] != nil {
            let heartRateData = data["activitiesHeart"] as! [String:Any]
            message.messageType = "Heart Rate"
            
            if heartRateData["restingHeartRate"] != nil {
                message.restingHeartRate = heartRateData["restingHeartRate"] as! Double
            }
        }
        
        return message
    }
    
    func transform(array : [[String : Any]]) -> [FitBitMessage] {
        var arrayToReturn : [FitBitMessage] = []
        for object in array {
            arrayToReturn.append(self.transform(dict: object))
        }
        return arrayToReturn
    }
}

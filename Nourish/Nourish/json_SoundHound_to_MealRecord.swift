//
//  json_SoundHound_to_MealRecord.swift
//  Nourish
//
//  Created by Vino (Vinodha) Sundaramoorthy on 7/8/17.
//  Copyright © 2017 SSIC. All rights reserved.
//

import UIKit

class json_SoundHound_to_MealRecord: NSObject {
    
    func transform(foodinfo:[Any] , date:Date) -> [String:Any]  {
        var mealRecordArray : [MealRecord] = []
        var mealDict : [String:Any] = [:]
        
        for info in foodinfo {
            let infoDetails = info as! [String:Any]
            
            if infoDetails["FoodLog"] != nil {
                let foodLogArray = infoDetails["FoodLog"] as! [[String:Any]]
            
                for food in foodLogArray {
                    if food["Food"] != nil {
                        let foodDict = food["Food"] as! [String : Any]
                        let mealRecord = MealRecord()
                        let foodId = foodDict["NDB_No"]
                        let amount = foodDict["Quantity"] as! Double
                        let servingTypeIndex = foodDict["SizeIndex"] as! Int
                        mealRecord.name = foodDict["SpokenNoSize"] as! String?
                        mealRecord.sourceFoodDBID = foodId as! String
                
                        let allUnitsArray = foodDict["AllSizes"] as! [Any]
                        let nutrientArray = foodDict["Nutrients"] as! [Any]
                        let calorie = nutrientArray[1] as! [String:Any]
                        var servingTypeUnitArray : [SHFoodUnit] = []
                
                        for unit in allUnitsArray {
                            let unitDict = unit as! [String:Any]
                            servingTypeUnitArray.append(SHFoodUnit(servingType: unitDict["WrittenSing"] as! String, grams: unitDict["Grams"] as! Double))
                        }
                
                        if food["Meal"] != nil {
                            mealRecord.ocasion = Ocasion.enumFrom(stringRep: food["Meal"] as!String)
                            mealDict["ocassion"] = Ocasion.enumFrom(stringRep: food["Meal"] as!String)
                        }
                
                        mealRecord.soundHoundFoodUnit = servingTypeUnitArray
                        mealRecord.sourceFoodDB = kSoundHoundSourceDB
                        mealRecord.unit = servingTypeUnitArray[0].servingType
                        mealRecord.amount = amount
                        mealRecord.caloriesPerGramm = calorie["AmountPer100g"] as!Double / 100
                        mealRecord.calories = mealRecord.caloriesPerGramm * (servingTypeUnitArray[0].grams * mealRecord.amount)
                        mealRecord.grams = (servingTypeUnitArray[0].grams * mealRecord.amount)
                    
                        if food["Timestamp"] != nil {
                            let dateDict = food["Timestamp"] as! [String : Any]
                        
                            if dateDict["DateAndTime"] != nil {
                                let date = dateDict["DateAndTime"] as! [String : Any]
                                let spokenDate = date["Date"] as! [String : Any]
                                let dateFormater = DateFormatter()
                                dateFormater.dateFormat = "MM/dd/yyyy"
                                let dateString = String(format:"%d/%d/%d",spokenDate["Month"] as! Int,spokenDate["DayOfMonth"] as! Int,spokenDate["Year"] as! Int)
                                mealRecord.date = dateFormater.date(from: dateString)
                            } else {
                                mealRecord.date = date
                            }
                        } else {
                            mealRecord.date = date
                        }
                        mealRecord.unit = servingTypeUnitArray[servingTypeIndex].servingType
                        mealRecordArray.append(mealRecord)
                    }
                }
            }
        }
        
        mealDict["MealArray"] = mealRecordArray
        return mealDict
    }

}

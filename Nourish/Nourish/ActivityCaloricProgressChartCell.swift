//
//  ActivityCaloricProgressChartCell.swift
//  Nourish
//
//  Created by Andrey Zmushko on 9/1/17.
//  Copyright © 2017 SSIC. All rights reserved.
//

import UIKit

class ActivityCaloricProgressChartCell: ActivityBaseCell {

    @IBOutlet weak var title: UILabel!
    @IBOutlet weak var chartView: ActivityBarChart!
    
    @IBOutlet weak var baseView: BaseView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}

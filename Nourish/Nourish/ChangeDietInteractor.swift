//
//  ChangeDietInteractor.swift
//  Nourish
//
//  Created by Vlad Birukov on 22.08.17.
//  Copyright © 2017 SSIC. All rights reserved.
//

import Foundation
import RxSwift

class ChangeDietInteractor {
    let repository = UserRepository.shared
    
    func execute() -> Observable<UserProfile> {
        return repository.getCurrentUser(policy: .DefaultPolicy)
    }
    
    func saveChangeDiet(userProfile: UserProfile, writeToHealth: Bool) {
        repository.storeCurrentUserInBackground(user: userProfile, writeToHealth: writeToHealth)
    }
}

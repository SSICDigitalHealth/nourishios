//
//  ActivityWeightCellPresenter.swift
//  Nourish
//
//  Created by Andrey Zmushko on 8/25/17.
//  Copyright © 2017 SSIC. All rights reserved.
//

import UIKit
import RxSwift


class ActivityWeightCellPresenter: ActivityBaseCellPresenter {
    
    let interactor = WeightInteractor()
    let mapperObjectWeight = WeightModel_to_WeightViewModel()
    var weightViewModel = WeightViewModel()
    
    private func getInformation(cell: ActivityBaseTableViewCell, startDate: Date, endDate: Date) {
        cell.baseView.viewWillAppear(true)
        cell.baseView.removeActivityIndicator()
        self.subscribtions.append(self.interactor.execute(startDate: startDate, endDate: endDate).observeOn(MainScheduler.instance).subscribe(onNext: { [unowned self] weightActive in
            self.weightViewModel = self.mapperObjectWeight.transform(model: weightActive)
        }, onError: {error in
            cell.baseView.parseError(error: error, completion: nil)
        }, onCompleted: {
            self.setupViewForData(cell: cell, startDate: startDate, endDate: endDate)
            self.reloadTable(cell: cell)
            cell.baseView.stopActivityAnimation()
        }, onDisposed: {
        }))
    }
     
    override func fetchModel(For basecell: ActivityBaseCell) {
        super.fetchModel(For: basecell)
        
        let cell = basecell as! ActivityBaseTableViewCell
        
        cell.iconImageView.imageNamedWithTint(named: "weight_icon", tintColor: UIColor.white)
        cell.iconFrame.backgroundColor = NRColorUtility.hexStringToUIColor(hex: "f0C517")
        
        cell.titleLabel.attributedText = NSMutableAttributedString(string: "Weight", attributes: cell.boldAttr)
        
        if let delegate = self.delegate {
            let date = delegate.fetchEpoch()
            self.getInformation(cell: cell, startDate: date.startDate, endDate: date.endDate)
        }
        
        cell.bottomGap.constant = 8
    }
    
    private func setupViewForData(cell: ActivityBaseTableViewCell, startDate: Date, endDate: Date) {
        let calendar = Calendar.current
        
        if self.weightViewModel.dailyWeightLogged == false && (calendar.isDateInToday(startDate) && calendar.isDateInToday(endDate)) && self.weightViewModel.userId != nil {
            cell.isUserInteractionEnabled = true
            cell.valueLabel.textColor = NRColorUtility.hexStringToUIColor(hex: "1172B9")
            cell.valueLabel.font =  UIFont(name: "Campton-Bold", size: 15)!
            cell.valueLabel.text = "UPDATE"
            
            if let delegate = self.delegate {
                delegate.transferUserId(userId: self.weightViewModel.userId!)
            }
            
        } else {
        
            let weightUnit = self.weightViewModel.isMetric ? " kg" : " lbs"
            
            if let weightValue = self.weightViewModel.weight {
                let weightStr = NSMutableAttributedString(string: "", attributes: cell.boldAttr)
                
                if weightValue.count > 0 {
                    let weight = self.weightViewModel.isMetric ? weightValue[0].value ?? 0 : NRConversionUtility.kiloToPounds(valueInKilo: weightValue[0].value ?? 0)
                    weightStr.append(NSMutableAttributedString(string: String(format: "%.0f", weight), attributes: cell.boldAttr))

                } else {
                    weightStr.append(NSMutableAttributedString(string: String(format: "--"), attributes: cell.bookAttr))
                }
                
                weightStr.append(NSMutableAttributedString(string: weightUnit, attributes: cell.bookAttr))
                
                cell.valueLabel.attributedText = weightStr
            }
            
            if let progress = self.weightViewModel.progress {
                if progress != 0.0 {
                    var sign = ""
                    let progress = self.weightViewModel.isMetric ?progress : NRConversionUtility.kiloToPounds(valueInKilo: progress)
                    
                    if progress > 0 {
                        sign = "+"
                    }
                    
                    cell.descriptionLabel.text = "Progress:"
                    cell.descriptionValueLabel.text = String(format: "%@%.0f%@", sign, progress, weightUnit)
                }
            }
        }
    }
}

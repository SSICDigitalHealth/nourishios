//
//  FooterOccasionMealPlannerTableViewCell.swift
//  Nourish
//
//  Created by Vlad Birukov on 22.08.17.
//  Copyright © 2017 SSIC. All rights reserved.
//

import UIKit

class FooterOccasionMealPlannerTableViewCell: UITableViewCell {
    @IBOutlet weak var messageImage: UIImageView!
    @IBOutlet weak var messageLabel: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func setUpWith(model: OccasionMealPlannerViewModel) {
        if model.message != nil {
            self.messageLabel.text = String(format: "%@", model.message!)
            self.messageLabel.setLineSpacing(spacing: 6)
            self.messageImage.imageNamedWithTint(named: "info", tintColor: NRColorUtility.hexStringToUIColor(hex: "1172B9"))
        }
    }
    
}

//
//  ConnectDevicesViewController.swift
//  Nourish
//
//  Created by Vino (Vinodha) Sundaramoorthy on 5/18/17.
//  Copyright © 2017 SSIC. All rights reserved.
//

import UIKit

class ConnectDevicesViewController: BasePresentationViewController {
    @IBOutlet weak var connectDeviceView : ConnectDeviceView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Connect Devices"
        self.setUpNavigationBarButtons()
        connectDeviceView.controller = self
        self.baseViews = [connectDeviceView]
    }

    func setUpNavigationBarButtons () {
        let titleAttribute = self.navigationBarTitleAttribute()
        let closeButton : UIBarButtonItem =  UIBarButtonItem(image: UIImage(named:"close_gray_icon"), style: .plain, target: self, action: #selector(self.closeAction(_:)))
        closeButton.setTitleTextAttributes(titleAttribute, for: .normal)
        navigationItem.title = self.title
        self.navigationController?.navigationBar.titleTextAttributes = titleAttribute
        self.navigationController?.navigationBar.barTintColor = UIColor.white
        self.navigationController?.navigationBar.tintColor = NRColorUtility.progressLabelColor()
        self.navigationController?.navigationItem.leftBarButtonItem = closeButton
        navigationItem.leftBarButtonItem = closeButton
    }
    
    func openWithingsLogin() {
        let withingsVC = WithingsViewController()
        let navigationController = UINavigationController.init(rootViewController: withingsVC)
        self.present(navigationController, animated: false, completion: nil)
    }
    
    func openFitBitLogin() {
        let fitBitLoginVC = FitBitViewController()
        let navigationController = UINavigationController.init(rootViewController: fitBitLoginVC)
        self.present(navigationController, animated: false, completion: nil)
    }
    
    
    @IBAction func doneAction(sender:UIButton) {
        self.dismiss(animated: false, completion: nil)
    }
}

//
//  foodSearchModel_to_FoodRecordCache.swift
//  Nourish
//
//  Created by Nova on 1/20/17.
//  Copyright © 2017 SSIC. All rights reserved.
//

import UIKit

class foodSearchModel_to_FoodRecordCache: NSObject {
    func transform(model : foodSearchModel) -> FoodRecordCache {
        let cache = FoodRecordCache()
        
        cache.foodId = model.foodId
        cache.calories = model.calories
        cache.amount = model.amount
        cache.foodTitle = model.foodTitle
        cache.measure = model.measure
        cache.unit = model.unit
        
        return cache
    }
}

class FoodRecordCache_to_foodsearchModel : NSObject {
    func transform(cache : FoodRecordCache) -> foodSearchModel {
        var model = foodSearchModel()
        
        model.foodId = cache.foodId!
        model.calories = cache.calories!
        model.amount = cache.amount
        model.foodTitle = cache.foodTitle!
        model.measure = cache.measure!
        model.unit = cache.unit!
        
        return model
    }
    
    func transform(caches : [FoodRecordCache]) -> [foodSearchModel] {
        var array : [foodSearchModel] = []
        for cache in caches {
            array.append(transform(cache: cache))
        }
        return array
    }
}

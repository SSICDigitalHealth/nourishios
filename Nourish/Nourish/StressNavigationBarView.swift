//
//  StressNavigationBarView.swift
//  Nourish
//
//  Created by Vlad Birukov on 01.09.17.
//  Copyright © 2017 SSIC. All rights reserved.
//

import UIKit

class StressNavigationBarView: BaseView {
    @IBOutlet weak var backButton: UIButton!
    @IBOutlet weak var titleLabel: UILabel!
    
    override func viewDidLoad() {
        self.xibAutoLayouting(nibName: "StressNavigationBarView")
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.stopActivityAnimation()
    }
    
    func settingNavigation(title: String) {
        self.titleLabel.text = title
        self.layer.borderColor = UIColor.white.cgColor
        self.layer.borderWidth = 0.1
        self.layer.shadowColor = UIColor.black.cgColor
        self.layer.shadowOffset = CGSize(width: 0.0, height: 1.0)
        self.layer.shadowRadius = 1.5
        self.layer.shadowOpacity = 0.2
        self.layer.masksToBounds = false
    }
}

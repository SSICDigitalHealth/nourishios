//
//  FitBitViewController.swift
//  Nourish
//
//  Created by Vino (Vinodha) Sundaramoorthy on 7/20/17.
//  Copyright © 2017 SSIC. All rights reserved.
//

import UIKit

protocol FitBitViewControllerProtocol {
    func dismissFitBitView ()
}

class FitBitViewController: BasePresentationViewController ,FitBitViewControllerProtocol{

    @IBOutlet weak var fitBitView : FitBitView!

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        self.setUpNavigationBarButtons()
        self.baseViews = [self.fitBitView]
        self.fitBitView.presenter.fitBitViewController = self

    }

    func setUpNavigationBarButtons () {
        let titleAttribute = self.navigationBarTitleAttribute()
        let closeButton : UIBarButtonItem =  UIBarButtonItem(image: UIImage(named:"close_icon"), style: .plain, target: self, action: #selector(self.closeAction(_:)))
        closeButton.setTitleTextAttributes(titleAttribute, for: .normal)
        navigationItem.title = self.title
        self.navigationController?.navigationBar.titleTextAttributes = titleAttribute
        self.navigationController?.navigationBar.barTintColor = NRColorUtility.nourishNavigationColor()
        self.navigationController?.navigationBar.tintColor = UIColor.white
        self.navigationController?.navigationItem.leftBarButtonItem = closeButton
        navigationItem.leftBarButtonItem = closeButton
    }
    
    func dismissFitBitView () {
        self.dismiss(animated: true, completion: nil)
    }

}

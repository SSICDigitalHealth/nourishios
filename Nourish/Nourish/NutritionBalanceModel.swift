//
//  NutritionBalanceModel.swift
//  Nourish
//
//  Created by Vlad Birukov on 14.06.17.
//  Copyright © 2017 SSIC. All rights reserved.
//

import Foundation

class NutritionBalanceModel : NSObject {
    var message: String?
    var nameImage: String?
    var nutritionData = NutritionalBalanceModel()
    var arrNutritionElement = [NutritionElementModel]()
}

class NutritionElementModel {
    var consumedAmount: Double = 0.0
    var type : NutritionType?
    var target:Double?
    var unit = ""
    var message = ""
    var nameImageMessage: String?
    var arrNutrition : [ElementModel]?
    var dataCharts: [NutrientInformationChartModel]?
    var richNutritionFood: [ElementModel]?
}

class ElementModel {
    var nameElement : String = ""
    var unit: String?
    var consumedState: Double?
    var amountElement: Double?
    
    init(){}
    
    init(nameElement: String, consumedCalories: Double) {
        self.nameElement = nameElement
        self.consumedState = consumedCalories
    }
}

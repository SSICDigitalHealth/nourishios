//
//  ChatCell.swift
//  Nourish
//
//  Created by Gena Mironchyk on 2/7/17.
//  Copyright © 2017 SSIC. All rights reserved.
//

import UIKit

class ChatCell: UITableViewCell {
    var chatView : UIView!
    
    func configureWithModel(chatModel : ChatDataBaseItemViewModel) {
        //var chatView : UIView
        let message = chatModel.content
        var width = NRChatUtility.getSizeOfString(message: message).width
        var height = NRChatUtility.getSizeOfString(message: message).height
        var paddingHeight : CGFloat = 0.0
        
        if height < 36 {
            height = 36.0
        }
        

        if chatModel.photoID.count > 0 {
            width = UIScreen.main.bounds.width - 96
             paddingHeight = 0.05 * UIScreen.main.bounds.height
            height = 0.244 * UIScreen.main.bounds.height + 25 + paddingHeight
        }
        
        
        if chatModel.chatBoxType == .typeInfoMessage {
            height += 178
        }
        self.frame = CGRect(x:self.frame.origin.x, y:self.frame.origin.y,width:self.frame.size.width, height:height+32)
        self.contentView.frame = CGRect(x:self.frame.origin.x, y:self.frame.origin.y,width:self.frame.size.width, height:height+32)

        if (chatModel.isFromUser == false){
            var yPosition = self.frame.height/2.0 - height/2.0
            if chatModel.chatBoxType == .typeDailyReport && chatModel.lostWeightValue > 0 {
                chatView = NRChatUtility.dailyReportChatWith(message:chatModel)
                height += 15
            } else if chatModel.chatBoxType == .typeLoggedFood{
                chatView = NRChatUtility.loggedFoodChatMessage(message: message,date:chatModel.date)
                width += 20
                height = message.sizeOfBoundingRect(width: NRChatUtility.maxLabelWidth() - 46, height: maxLabelSize.height, font: UIFont.systemFont(ofSize: 14)).height + 30
                yPosition = 15
            } else if chatModel.chatBoxType == .typeInfoMessage {
                chatView = NRChatUtility.chatInfoCell()
            } else {
                chatView = NRChatUtility.appChatWithMessage(message: message)
            }
                chatView.frame = CGRect(x: 10, y: yPosition, width:width , height:height)
                chatView.backgroundColor = UIColor.clear
        } else {
            chatView = NRChatUtility.userChatWithMessage(message: message, photoID: chatModel.photoID)
            
            var x = UIScreen.main.bounds.width - width - 20
            if chatModel.photoID.count >  0 {
                x = UIScreen.main.bounds.width - width - 15
                var labelHeight =  NRChatUtility.getSizeOfString(message: message).height
                labelHeight = labelHeight < 36 ? 36 : labelHeight
                height = height + 20 + labelHeight + 20
            } else {
                height = height + 15
            }
            
            chatView.frame = CGRect(x: x , y: self.frame.height/2.0-height/2.0, width: width , height: height)
            if chatModel.chatBoxType == .typeDish {
                
                let label = self.timeStampViewWith(model: chatModel)
                let labelXPosition = UIScreen.main.bounds.width - label.intrinsicContentSize.width - 20
                let labelYPosition = chatModel.photoID.count >  0 ? height + chatView.frame.origin.y - 30 : height + chatView.frame.origin.y - 5
                label.frame = CGRect(x: labelXPosition, y: labelYPosition, width: label.intrinsicContentSize.width, height: label.intrinsicContentSize.height)
                height += label.intrinsicContentSize.height + 8
                self.contentView.addSubview(label)
                self.contentView.sendSubview(toBack: label)
                chatView.frame = CGRect(x: x , y: self.frame.height/2.0-height/2.0, width: width , height: height)
            }
        }
        
        self.contentView.backgroundColor = UIColor.clear
        self.backgroundColor = UIColor.clear
        self.contentView.addSubview(chatView)
        self.contentView.sendSubview(toBack: chatView)
        
        
    }
    
    private func timeStampViewWith(model : ChatDataBaseItemViewModel) -> UILabel {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "MMM d hh:mm a"
        let label = UILabel()
        label.font = UIFont.systemFont(ofSize: 10.0)
        label.textColor = NRColorUtility.dotColor()
        label.text = dateFormatter.string(from: model.date)
        label.textAlignment = .right
        return label
    }
    
    class func neededHeight(text : String) ->CGFloat {
        let height = NRChatUtility.getSizeOfString(message: text).height
        return height + 20.0
    }
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
      
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}

//
//  NourishTabBarViewController.swift
//  Nourish
//
//  Created by Gena Mironchyk on 5/4/17.
//  Copyright © 2017 SSIC. All rights reserved.
//

import UIKit
import Crashlytics
let kTabBarHeight = 65.0

class NourishTabBarViewController: UITabBarController {

    var chatButton : UIButton!
    var nutritionButton : UIButton!
    var multiActionButton : UIButton!
    var activityButton : UIButton!
    var progressButton : UIButton!
    var lastSender : UIButton!
    var recVC : MealDiaryViewController!
    var mealDiaryNavController : BaseNavigationController!
   // let tabBarHeight = 65.0
    private var heightConstraingt : NSLayoutConstraint!
    private var separator = UIView.init(frame: CGRect(x : 0.0, y : 0.0, width : UIScreen.main.bounds.size.width, height : 1.0))

    
    var tabbarView : UIView!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupViewControllers()
        self.setupTabBarView()
       // self.setupStatusBar()
        // Do any additional setup after loading the view.
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        //self.setupViewControllers()
    }
    
    private func setupTabBarView () {
       self.tabbarView = UINib(nibName: "NRTabBar", bundle: nil).instantiate(withOwner: nil, options: nil)[0] as! UIView
        self.view.addSubview(self.tabbarView)
        self.tabbarView.translatesAutoresizingMaskIntoConstraints = false
        self.tabbarView.frame = CGRect(x: 0.0, y: self.view.frame.size.height - self.tabbarView.frame.size.height, width: self.view.frame.size.width, height:self.tabbarView.frame.size.height)
        NSLayoutConstraint(item: self.tabbarView, attribute: .leading, relatedBy: .equal, toItem: self.view, attribute: .leading, multiplier: 1, constant: 0).isActive = true
        NSLayoutConstraint(item: self.tabbarView, attribute: .trailing, relatedBy: .equal, toItem: self.view, attribute: .trailing, multiplier: 1, constant: 0).isActive = true
        NSLayoutConstraint(item: self.tabbarView, attribute: .bottom, relatedBy: .equal, toItem: self.view, attribute: .bottom, multiplier: 1, constant: 0).isActive = true
        self.heightConstraingt = NSLayoutConstraint(item: self.tabbarView, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: CGFloat(kTabBarHeight))
        self.heightConstraingt.isActive = true
        self.tabbarView.addSubview(separator)
        self.separator.backgroundColor = UIColor.black.withAlphaComponent(0.12)

        self.tabBar.frame = CGRect (x: 0.0, y: 0.0, width:0.0, height:0.0)

        
        self.chatButton = self.tabbarView.viewWithTag(1) as! UIButton
        self.nutritionButton = self.tabbarView.viewWithTag(2) as! UIButton
        self.multiActionButton = self.tabbarView.viewWithTag(3) as! UIButton
        self.activityButton = self.tabbarView.viewWithTag(4) as! UIButton
        self.progressButton = self.tabbarView.viewWithTag(5) as! UIButton
        self.chatButton.alignImageAndTitleVertically(padding: 3)
        self.chatButton.titleLabel?.text = "Chat"
        self.nutritionButton.alignImageAndTitleVertically(padding: 3)
        self.activityButton.alignImageAndTitleVertically(padding: 3)
        self.progressButton.alignImageAndTitleVertically(padding: 3)
        
        var selectedAttribute = [String : AnyObject]()
        selectedAttribute[NSForegroundColorAttributeName] = NRColorUtility.hexStringToUIColor(hex: "#6d6d6d")
        selectedAttribute[NSFontAttributeName] = UIFont.systemFont(ofSize: 12, weight: UIFontWeightBold)
        
        var regAttribute = [String : AnyObject]()
        regAttribute[NSForegroundColorAttributeName] = NRColorUtility.hexStringToUIColor(hex: "#6d6d6d")
        regAttribute[NSFontAttributeName] = UIFont.systemFont(ofSize: 12, weight: UIFontWeightRegular)
        
        self.chatButton.setAttributedTitle(NSAttributedString(string: (self.chatButton.titleLabel?.text)!, attributes: regAttribute), for: .normal)
        self.nutritionButton.setAttributedTitle(NSAttributedString(string: (self.nutritionButton.titleLabel?.text)!, attributes: regAttribute), for: .normal)
        self.activityButton.setAttributedTitle(NSAttributedString(string: (self.activityButton.titleLabel?.text)!, attributes: regAttribute), for: .normal)
        self.progressButton.setAttributedTitle(NSAttributedString(string: (self.progressButton.titleLabel?.text)!, attributes: regAttribute), for: .normal)
        
        self.chatButton.setAttributedTitle(NSAttributedString(string: (self.chatButton.titleLabel?.text)!, attributes: selectedAttribute), for: .selected)
        self.nutritionButton.setAttributedTitle(NSAttributedString(string: (self.nutritionButton.titleLabel?.text)!, attributes: selectedAttribute), for: .selected)
        self.activityButton.setAttributedTitle(NSAttributedString(string: (self.activityButton.titleLabel?.text)!, attributes: selectedAttribute), for: .selected)
        self.progressButton.setAttributedTitle(NSAttributedString(string: (self.progressButton.titleLabel?.text)!, attributes: selectedAttribute), for: .selected)

        
        self.chatButton.addTarget(self, action: #selector(processTap(_:)), for: .touchUpInside)
        self.nutritionButton.addTarget(self, action: #selector(processTap(_:)), for: .touchUpInside)
        self.multiActionButton.addTarget(self, action: #selector(processTap(_:)), for: .touchUpInside)
        self.activityButton.addTarget(self, action: #selector(processTap(_:)), for: .touchUpInside)
        self.progressButton.addTarget(self, action: #selector(processTap(_:)), for: .touchUpInside)
        
        self.processTap(self.chatButton)
        self.hideTabbarView(animated: false)

    }
    
    @objc private func processTap(_ sender : UIButton) {
        if sender ==  self.activityButton || sender == self.multiActionButton || sender == self.nutritionButton && PermissionsUtility.shared.checkHealthKitPermissionsWasAsked() == false {
            ActivityUtility.getHealthKitPermission(completion: {completed, error in
                if completed ==  true {
                    DispatchQueue.main.async {
                        self.lastSender = sender
                        self.setSelectedViewController(viewController: self.viewControllers![sender.tag - 1], sender: sender)

                    }
                }
            
            })} else {
            self.lastSender = sender
            self.setSelectedViewController(viewController: self.viewControllers![sender.tag - 1], sender: sender)
        }
        
    }
    
    private func setSelectedViewController(viewController : UIViewController, sender : UIButton) {
        for btn in self.tabbarView.subviews {
            if btn.isKind(of: UIButton.self){
                let button = btn as! UIButton
                if button == self.lastSender{
                    button.isSelected = true
                } else {
                    button.isSelected = false
                }
            }
        }
        
        if sender.tag == self.chatButton.tag {
            EventLogger.logClickedOnChat()
        }
        
        if sender.tag == self.nutritionButton.tag {
            EventLogger.logOnNutritionScreenClick()
                
        }
        
        if sender.tag == self.activityButton.tag {
            EventLogger.logOnActivityScreenClick()
        }
        
        
        if sender == self.progressButton {
            EventLogger.logClickedOnMealPlanner()
            for vc in viewController.childViewControllers {
                vc.navigationController?.popToRootViewController(animated: false)
                #if OPTIFASTVERSION
                    if vc.isKind(of: StartMealPlannerViewController.self) {
                        (vc as! StartMealPlannerViewController).checkCache()
                    }
                #else
                    if vc.isKind(of: NouriqStartMealPlannerViewController.self) {
                        (vc as! NouriqStartMealPlannerViewController).checkCache()
                    }
                #endif
            }

        }
        
        super.selectedViewController = viewController
        
        
    }


    
    func hideTabbarView(animated : Bool) {
        self.chatButton.isHidden = true
        self.nutritionButton.isHidden = true
        self.activityButton.isHidden = true
        self.progressButton.isHidden = true

        self.separator.removeFromSuperview()
        self.heightConstraingt.constant = 0.0
        if animated == true {
            UIView.animate(withDuration: 0.3, delay: 0.0, options: .transitionCurlUp, animations: {
                self.view.layoutIfNeeded()
            }, completion: nil)
        } else {
            self.view.layoutIfNeeded()
        }
        self.tabBar.isHidden = true
    }
    
    func showTabbarView (animated : Bool) {
        self.heightConstraingt.constant = CGFloat(kTabBarHeight)
        self.tabbarView.addSubview(self.separator)
        self.chatButton.isHidden = false
        self.nutritionButton.isHidden = false
        self.activityButton.isHidden = false
        self.progressButton.isHidden = false
        
        if animated == true {
            UIView.animate(withDuration: 0.3, delay: 0.0, options: .transitionCurlUp, animations: {
                self.view.layoutIfNeeded()

            }, completion: nil)
        } else {
            self.view.layoutIfNeeded()
        }
        
    }
    
    
//    func showDetailedReportFor(date : Date) {
//        self.processTap(self.reportsButton)
//        let reportNavController = self.viewControllers![self.reportsButton.tag - 1] as! BaseNavigationController
//        let reportViewController = reportNavController.topViewController as! ReportsViewController
//        reportViewController.showDetailedReport(date: date)
//    }
    
    private func showMultimodalMenu () {
        let multimodalViewController = MultiModalViewController(nibName : "MultiModalViewController", bundle : nil)
        multimodalViewController.modalPresentationStyle=UIModalPresentationStyle.overCurrentContext
        self.present(multimodalViewController, animated: false, completion: nil)
    }
    
    func openFoodDiaryForDate (date : Date, showFoodSearch : Bool, ocasion : Ocasion?) {
        self.recVC.dateToFetch = date
        if showFoodSearch == true {
            self.present(self.mealDiaryNavController, animated: true, completion: {
                if ocasion != nil {
                    self.recVC.showAddFoodControllerForOcasion(ocasion: ocasion!, date: date, showPredictions: true)
                }
            })
        } else {
            self.present(self.mealDiaryNavController, animated: true, completion: nil)
        }
    }
    
    func presentModal(vc : UIViewController, animated : Bool, needDismission : Bool) {
        if needDismission == true {
            self.dismiss(animated: false, completion: {
                self.present(vc, animated: animated, completion: nil)
            })
        } else {
            self.present(vc, animated: animated, completion: nil)
        }
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func showChat() {
        self.processTap(self.chatButton)
    }

    func showActivity() {
        self.processTap(self.activityButton)
    }
    
    func showNutrition() {
        self.processTap(self.nutritionButton)
    }

    private func setupViewControllers() {
        
        self.recVC = MealDiaryViewController(nibName: "MealDiaryViewController", bundle: nil)
        self.mealDiaryNavController = BaseNavigationController.init(rootViewController: self.recVC)
        var viewControllers : [UIViewController] = []
        let homeScreenViewController = HomeScreenViewController(nibName: "HomeScreenViewController", bundle: nil)
        let chatVc = ChatViewController(nibName : "ChatViewController", bundle : nil)
        
        let mealPlannerViewController = NouriqStartMealPlannerViewController(nibName: "NouriqStartMealPlannerViewController", bundle : nil)
        var mealPlannerNavController = BaseNavigationController.init(rootViewController: mealPlannerViewController)

        #if OPTIFASTVERSION
           let mealPlannerVC = StartMealPlannerViewController(nibName: "StartMealPlannerViewController", bundle : nil)
            mealPlannerNavController = BaseNavigationController.init(rootViewController: mealPlannerVC)
        #endif
        
        let nutritionViewController = NewProgressViewController(nibName: "NewProgressViewController", bundle: nil)
        let activityViewController = ActivityViewController(nibName: "ActivityViewController", bundle: nil)
        let homeScreenController = BaseNavigationController.init(rootViewController: homeScreenViewController)
        let homeNavController = BaseNavigationController.init(rootViewController: chatVc)
        let nutritionNavController = BaseNavigationController.init(rootViewController: nutritionViewController)
        let activityNavController = BaseNavigationController.init(rootViewController: activityViewController)
        activityNavController.title = "Activity"
        viewControllers.append(contentsOf: [homeNavController, nutritionNavController, homeScreenController, activityNavController, mealPlannerNavController])
        
        self.viewControllers = viewControllers
        
    }
    
    func chatViewControllerInstance() -> ChatViewController {
        let chatNavController = self.viewControllers![self.chatButton.tag - 1] as! BaseNavigationController
        return chatNavController.topViewController as! ChatViewController
    }
    
    override var supportedInterfaceOrientations: UIInterfaceOrientationMask {
        return UIInterfaceOrientationMask.portrait
    }
    
    override var shouldAutorotate: Bool {
        return false
    }

}

extension UIButton {
    
    func alignImageAndTitleVertically(padding: CGFloat = 6.0) {
        let imageSize = self.imageView!.frame.size
        let titleSize = self.titleLabel!.frame.size
        let totalHeight = imageSize.height + titleSize.height + padding
        
        self.imageEdgeInsets = UIEdgeInsets(
            top: -(totalHeight - imageSize.height),
            //left: (self.frame.size.width - imageSize.width) / 2,
            left: 0,
            bottom: 0,
            right: -titleSize.width
        )
        
        self.titleEdgeInsets = UIEdgeInsets(
            top: 0,
            left: -imageSize.width,
            bottom: -(totalHeight - titleSize.height),
            right: 0
        )
    }
    
}


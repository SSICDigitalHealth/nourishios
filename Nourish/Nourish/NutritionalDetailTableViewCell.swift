//
//  NutritionalDetailTableViewCell.swift
//  Nourish
//
//  Created by Vlad Birukov on 19.06.17.
//  Copyright © 2017 SSIC. All rights reserved.
//

import UIKit

class NutritionalDetailTableViewCell: UITableViewCell {

    @IBOutlet weak var amountFood: UILabel!
    @IBOutlet weak var nameFood: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
        func setupWithModel(model: NutritionElementViewModel, indexPath :IndexPath) {
            
        if model.arrNutrition?[indexPath.row].unit == nil &&  model.arrNutrition?[indexPath.row].consumedState == nil && model.arrNutrition?[indexPath.row].amountElement == nil {
        
            self.nameFood.textColor = NRColorUtility.progressTableBlackColor()
            self.nameFood.font = UIFont(name: "Campton-Bold", size: 15)
            self.nameFood.text = String(format: "%@", (model.arrNutrition?[indexPath.row].nameElement)!)
        } else {
                self.nameFood.textColor = NRColorUtility.progressTableGrayColor()
                self.nameFood.text = String(format: "%@", (model.arrNutrition?[indexPath.row].nameElement)!)
                self.nameFood.setLineSpacing(spacing: 4)
        }
    }
}

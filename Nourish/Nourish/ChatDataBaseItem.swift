//
//  ChatDataBaseItem.swift
//  Nourish
//
//  Created by Nova on 2/6/17.
//  Copyright © 2017 SSIC. All rights reserved.
//

import UIKit
import RealmSwift

@objc enum chatBoxType : Int {
    case typeString
    case typeDish
    case typeWeightRecord
    case typeEatRightReport
    case typeRecommendation
    case typeLogFood
    case typeLogWeight
    case typeCalorieReport
    case typeDailyReport
    case typeLoggedFood
    case typeInfoMessage
    case typeWatchMessage
    case typeWeightlossRecommendationMessage
    case typeHealthKitPermissions
    case typeMaintainWeightReccomendationMessage
}

class ChatDataBaseItem: Object {
    dynamic var date : Date = Date()
    dynamic var chatBoxType : chatBoxType = .typeString
    dynamic var content : String = ""
    dynamic var isFromUser : Bool = false
    dynamic var messageID : String = ""
    dynamic var userID : String = ""
    dynamic var lostWeightValue : Double = 0.0
    dynamic var targetLostWeightValue : Double = 0.0
    dynamic var photoID : String = ""
}

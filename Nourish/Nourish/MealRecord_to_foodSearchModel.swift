//
//  MealRecord_to_foodSearchModel.swift
//  Nourish
//
//  Created by Nova on 2/14/17.
//  Copyright © 2017 SSIC. All rights reserved.
//

import UIKit

class MealRecord_to_foodSearchModel: NSObject {
    func transform(record : MealRecordModel) -> foodSearchModel {
        var model = foodSearchModel()
        model.amount = record.servingAmmount ?? 0
        model.foodBackendID = record.userFoodID
        model.foodTitle = record.foodTitle ?? record.groupName ?? ""
        model.calories = String(format : "%.1f",record.calories ?? 0.0)
        model.foodId = record.foodId != nil ? record.foodId! : ""
        model.unit = record.servingType ?? ""
        model.grams = record.grams
        model.caloriePerPortion = record.caloriesPerServing ?? 0.0
        model.isFromNoom = record.isFromNoom
        model.mealIdString = record.idString
        model.localCacheID = record.localCacheID
        
        model.sourceFoodDB = record.sourceFoodDB
        model.sourceFoodDBID = record.sourceFoodDBID
        
        if record.groupedMealArray.count > 0 {
            model.isGroupFavorite = true
        }
        
        model.groupID = record.groupID
        model.isFavorite = record.isFavourite
        model.userPhotoId = record.userPhotoId
        
        return model
    }
    
    func transform(record:MealRecord) -> foodSearchModel {
        var model = foodSearchModel()
        model.foodTitle = record.name!
        model.localCacheID = record.localCacheID
        model.amount = record.amount
       // model.foodBackendID = record.userFoodId
        model.foodId = record.idString!
        model.mealIdString = record.idString
        model.calories = String(format:"%.1f",record.calories)
        model.grams = record.grams
        model.occasion = record.ocasion
        model.caloriePerPortion = record.caloriesPerGramm
        model.unit = record.unit!
        return model
    }
    
    func transform(records : [MealRecordModel]) -> [foodSearchModel] {
        var array : [foodSearchModel] = []
        
        for record in records {
            let model = self.transform(record: record)
            array.append(model)
        }
        
        return array
    }
}

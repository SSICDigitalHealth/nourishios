//
//  OperactionCache.swift
//  Nourish
//
//  Created by Nova on 8/30/17.
//  Copyright © 2017 SSIC. All rights reserved.
//

import Foundation
import RealmSwift

enum operationType : String {
    case create
    case update
    case delete
    case addPhoto
    case merge
    case createGroup
    case createVoice
}

class OperationCache: Object {
    dynamic var userID : String?
    dynamic var userFoodID : String?
    dynamic var foodGroupID : String?
    dynamic var userPhotoID : String?
    dynamic var operType = operationType.create.rawValue
    dynamic var timeStamp : Date?
    dynamic var date : Date?
    
    dynamic var counter : Int = 0
    
    dynamic var localCacheID = "local" + UUID().uuidString
    
    var affectedLocalCacheIDs = List<RealmString>()
    
    var affectedIDs: [String] {
        get {
            return affectedLocalCacheIDs.map { $0.stringValue }
        }
        set {
            affectedLocalCacheIDs.removeAll()
            affectedLocalCacheIDs.append(objectsIn: newValue.map { RealmString(value: [$0]) })//append(contentsOf: [RealmString(value : newValue)])
        }
    }
    
    var operationTypeEnum : operationType {
        get {
            return operationType(rawValue : operType)!
        }
        
        set {
            operType = newValue.rawValue
        }
    }
    
    private dynamic var dictionaryData: NSData?
    var dictionary: [String: Any] {
        get {
            guard let dictionaryData = dictionaryData else {
                return [String: Any]()
            }
            do {
                let dict = try JSONSerialization.jsonObject(with: dictionaryData as Data, options: []) as? [String: Any]
                return dict!
            } catch {
                return [String: Any]()
            }
        }
        
        set {
            do {
                let data = try JSONSerialization.data(withJSONObject: newValue, options: [])
                dictionaryData = data as NSData
            } catch {
                dictionaryData = nil
            }
        }
    }
    
    override static func ignoredProperties() -> [String] {
        return ["dictionary","affectedIDs"]
    }
    
    
// Specify properties to ignore (Realm won't persist these)
    
//  override static func ignoredProperties() -> [String] {
//    return []
//  }
}

//
//  OptifastSettingsView.swift
//  Nourish
//
//  Created by Gena Mironchyk on 10/17/17.
//  Copyright © 2017 SSIC. All rights reserved.
//

import UIKit

let optifastInfoCellID = "optifastInfoCell"
let optifastSectionCellID = "optifastSectionCell"
let optifastOcasionCellID = "optifastOcasionCell"
let optifasctProductCellID = "optifastProductCell"
let optifastSaveButtonCellID = "optifastSavebuttonCell"

protocol OptifastSettingsViewProtocol : BaseViewProtocol {
    func reloadData ()
}


class OptifastSettingsView: BaseView, OptifastSettingsViewProtocol {

    @IBOutlet weak var optifastSettingsTableView : UITableView!
    @IBOutlet weak var presenter : OptifastSettingsPresenter!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.registerCells()
        self.optifastSettingsTableView.estimatedRowHeight = 44.0
        self.optifastSettingsTableView.rowHeight = UITableViewAutomaticDimension
        self.basePresenter = self.presenter
        self.presenter.settingsView = self
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    func reloadData() {
        self.stopActivityAnimation()
        self.optifastSettingsTableView.reloadData()
    }
    
    private func registerCells () {
        self.optifastSettingsTableView.register(UINib.init(nibName: "OptifastSettingsInfoCell", bundle: nil), forCellReuseIdentifier: optifastInfoCellID)
        self.optifastSettingsTableView.register(UINib.init(nibName: "OptifastSettingsSectionCell", bundle: nil), forCellReuseIdentifier: optifastSectionCellID)
        self.optifastSettingsTableView.register(UINib.init(nibName: "OptifastSettingsOcasionCell", bundle: nil), forCellReuseIdentifier: optifastOcasionCellID)
        self.optifastSettingsTableView.register(UINib.init(nibName: "OptifastProductCell", bundle: nil), forCellReuseIdentifier: optifasctProductCellID)
        self.optifastSettingsTableView.register(UINib.init(nibName: "OptifastSettingsSaveButtonCell", bundle: nil), forCellReuseIdentifier: optifastSaveButtonCellID)
    }
    
}

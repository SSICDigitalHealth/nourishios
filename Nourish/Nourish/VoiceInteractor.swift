//
//  VoiceInteractor.swift
//  Nourish
//
//  Created by Vino (Vinodha) Sundaramoorthy on 7/7/17.
//  Copyright © 2017 SSIC. All rights reserved.
//

import UIKit
import RxSwift

class VoiceInteractor: NSObject {
    
    let mealRepo = MealHistoryRepository.shared
    var toSearchModelMapper = MealRecord_to_foodSearchModel()

    func uploadMeal(meal:MealRecord) -> Observable<Double> {
        let obs = self.mealRepo.uploadMealVoice(mealRecord: meal)
        return obs
    }
    
}

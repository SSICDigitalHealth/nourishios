//
//  StartMealPlannerPresenter.swift
//  Nourish
//
//  Created by Vlad Birukov on 18.08.17.
//  Copyright © 2017 SSIC. All rights reserved.
//

import Foundation
import RxSwift


class StartMealPlannerPresenter: BasePresenter {
    let mapperObjectStartMealPlanner = UserProfile_to_StartMealPlannerViewModel()
    let interactor = StartMealPlannerInteractor()
    let interactorMealPlan = MealPlannerInteractor()
    let generateMealPlanInteractor = GenerateMealPlanInteractor()
    var startMealPlannerViewController: StartMealPlannerViewController?
    var startMealPlannerView: StartMealPlannerProtocol?

    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.getUserInformation()
    }
    
    private func getUserInformation() {
        self.subscribtions.append(self.interactor.execute().observeOn(MainScheduler.instance).subscribe(onNext: { [weak self] userInform in
            if let instance = self {
                instance.startMealPlannerView?.setUpWithModel(model: instance.mapperObjectStartMealPlanner.transform(model: userInform))
            }
        }, onError: { [weak self] error in
            if let instance = self {
                instance.startMealPlannerView?.stopActivityAnimation()
                instance.startMealPlannerView?.parseError(error: error, completion: nil)
            }
        }, onCompleted: { [weak self] in
            if let instance = self {
                instance.startMealPlannerView?.stopActivityAnimation()
            }
        }, onDisposed: { [weak self] in
            if let instance = self {
                instance.startMealPlannerView?.stopActivityAnimation()
            }
        }))
    }
    
    @IBAction func changeDiet(_ sender: Any) {
        if self.startMealPlannerViewController != nil {
            self.startMealPlannerViewController?.navigationController?.pushViewController(ChangeDietViewController(), animated: true)
            NavigationUtility.hideTabBar(animated: false)
        }
    }
    
    @IBAction func generatePlan(_ sender: Any) {
        EventLogger.logClickedOnGenerateMealPlan()
        self.startMealPlannerView?.startActivityAnimation()
        self.subscribtions.append(self.generateMealPlanInteractor.executeFor(date: Date()).observeOn(MainScheduler.instance).subscribe(onNext: { [weak self] generated in
            if let instance = self {
                instance.startMealPlannerView?.stopActivityAnimation()
                if instance.startMealPlannerViewController != nil {
                    #if !OPTIFASTVERSION
                        instance.startMealPlannerViewController?.navigationController?.pushViewController(MealPlannerViewController(), animated: true)
                    #else
                        instance.startMealPlannerViewController?.navigationController?.pushViewController(OptifastMealPlannerViewController(), animated: true)
                    #endif
                }
            }

        }, onError: { [weak self] error in
            if let instance = self {
                instance.startMealPlannerView?.stopActivityAnimation()
                instance.startMealPlannerView?.parseError(error: error,completion: nil)
            }
        }, onCompleted:{}, onDisposed: {}))
        
    }
    
    @IBAction func changeOptifast(_ sender: Any) {
        if self.startMealPlannerViewController != nil {
            let vc = OptifastSettingsViewController.init(nibName: "OptifastSettingsViewController", bundle: nil)
            
            self.startMealPlannerViewController?.navigationController?.pushViewController(vc, animated: true)

        }
    }
    
    func isCache() {
        self.subscribtions.append(self.interactorMealPlan.execute(date: Date()).observeOn(MainScheduler.instance).subscribe(onNext: { [weak self] mealPlannerInform in
            if let instance = self {
                if mealPlannerInform != nil {
                    if instance.startMealPlannerViewController != nil {
                        #if !OPTIFASTVERSION
                            instance.startMealPlannerViewController?.navigationController?.pushViewController(MealPlannerViewController(), animated: false)
                        #else
                            instance.startMealPlannerViewController?.navigationController?.pushViewController(OptifastMealPlannerViewController(), animated: false)
                        #endif
                    }
                }
            }
   
        }, onError: { [weak self] error in
            if let instance = self {
                instance.startMealPlannerView?.parseError(error: error,completion: nil)
            }
        }, onCompleted: {}, onDisposed: {

        }))
    }
}

//
//  NRHealthProgress_to_NRHealthProgressModel.swift
//  Nourish
//
//  Created by Vino (Vinodha) Sundaramoorthy on 12/1/16.
//  Copyright © 2016 SSIC. All rights reserved.
//

import UIKit


class NRHealthProgress_to_NRHealthProgressModel: NSObject {
    
    func transform(healthProgress : NRHealthProgress) -> NRHealthProgressModel {
        let progress = NRHealthProgressModel()
        progress.sleep = healthProgress.sleep
        progress.stress = healthProgress.stress
        progress.activity = healthProgress.activity
        progress.calories = healthProgress.calories
        progress.nutritionScore = healthProgress.nutritionScore
        progress.steps = healthProgress.steps
        progress.bmr = healthProgress.bmr
        return progress
    }
        func transform(modelArray : [NRHealthProgress]) -> [NRHealthProgressModel] {
            var arrayToReturn : [NRHealthProgressModel] = []
            for model in modelArray {
                arrayToReturn.append(self.transform(healthProgress: model))
            }
            return arrayToReturn
        }
}

//
//  WeeklyStressChartView.swift
//  Nourish
//
//  Created by Gena Mironchyk on 1/23/17.
//  Copyright © 2017 SSIC. All rights reserved.
//

import UIKit
import Foundation

class WeeklyStressChartView : BaseView , WeeklyStressProtocol{
    
    @IBOutlet weak var stressLabel : UILabel!
    @IBOutlet weak var maxLabel = UILabel()
    @IBOutlet weak var minLabel = UILabel()
    @IBOutlet weak var avgLabel = UILabel()
    @IBOutlet weak var scatterChart: ScatterChartView!
    @IBOutlet weak var tableView = UITableView()
    var presenter = StressPresenter()
    var labelsAreDrown : Bool = false
    
    func setupChartView() {
        scatterChart.legend.enabled = false

        scatterChart.isUserInteractionEnabled = false
        scatterChart.autoScaleMinMaxEnabled = false
        
        scatterChart.xAxis.axisMinimum = 0.5
        scatterChart.xAxis.axisMaximum = 7.5
        
        scatterChart.xAxis.drawLabelsEnabled = true
        scatterChart.xAxis.labelCount = 7
        scatterChart.xAxis.labelPosition = .bottom
        scatterChart.xAxis.drawGridLinesEnabled = false
        scatterChart.xAxis.drawAxisLineEnabled = false
        
        scatterChart.leftAxis.drawAxisLineEnabled = false
        scatterChart.leftAxis.drawGridLinesEnabled = false
        scatterChart.leftAxis.labelCount = 3
        scatterChart.leftAxis.labelTextColor = UIColor.clear
        scatterChart.leftAxis.drawLabelsEnabled = true
        scatterChart.leftAxis.forceLabelsEnabled = true

        scatterChart.leftAxis.drawZeroLineEnabled = false
        
        
        scatterChart.rightAxis.enabled = false
        scatterChart.animate(yAxisDuration: 1)
        scatterChart.xAxis.labelFont = UIFont.systemFont(ofSize: 12.0)

    }
    
    

    
    func setChartData(data : ScatterChartData, xTitles : [String],maxBpm : Double, minBpm : Double, avgBpm : Double){
        self.maxLabel?.text = String.init(format: "%0.0f", maxBpm)
        self.minLabel?.text = String.init(format: "%0.0f", minBpm)
        self.avgLabel?.text = String.init(format: "%0.0f", avgBpm)

        scatterChart.xAxis.valueFormatter = IndexAxisValueFormatter(values:xTitles)
        scatterChart.data = data
        self.drawLabels()
    }
    
    
    func setupView() {
        let view = UINib(nibName: "WeeklyStressChartView", bundle: nil).instantiate(withOwner: self, options: nil)[0] as! UIView
        view.translatesAutoresizingMaskIntoConstraints = false
        self.translatesAutoresizingMaskIntoConstraints = false
        self.addSubview(view)
        NSLayoutConstraint(item: view, attribute: .leading, relatedBy: .equal, toItem: self, attribute: .leading, multiplier: 1, constant: 0).isActive = true
        NSLayoutConstraint(item: view, attribute: .trailing, relatedBy: .equal, toItem: self, attribute: .trailing, multiplier: 1, constant: 0).isActive = true
        NSLayoutConstraint(item: view, attribute: .top, relatedBy: .equal, toItem: self, attribute: .top, multiplier: 1, constant: 0).isActive = true
        NSLayoutConstraint(item: view, attribute: .bottom, relatedBy: .equal, toItem: self, attribute: .bottom, multiplier: 1, constant: 0).isActive = true
        self.maxLabel?.text = "-"
        self.minLabel?.text = "-"
        self.avgLabel?.text = "-"
    }
    
    
    func drawLabels () {
        if (scatterChart.data != nil && self.labelsAreDrown == false) {
            let viewPortHandler = scatterChart.viewPortHandler
            let color = NRColorUtility.dotColor()
            let topLabel = UILabel()
            let font = UIFont.systemFont(ofSize: 9)
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "h a"
            let startOfDay = Calendar.current.startOfDay(for: Date())
            var text = dateFormatter.string(from: startOfDay)
            topLabel.text = text
            topLabel.font = font
            topLabel.textColor = color
            var size = text.size(attributes: [NSFontAttributeName: font])
            topLabel.frame = CGRect(x: (viewPortHandler?.contentLeft)! - (viewPortHandler?.offsetLeft)!, y: (viewPortHandler?.contentTop)! + (viewPortHandler?.offsetTop)!, width: size.width, height: size.height)
            topLabel.textAlignment = .right
            scatterChart.addSubview(topLabel)
            
            let bottomLabel = UILabel()
            bottomLabel.text = text
            bottomLabel.font = font
            bottomLabel.textColor = color
            bottomLabel.frame = CGRect(x: (viewPortHandler?.contentLeft)! - (viewPortHandler?.offsetLeft)!, y: (viewPortHandler?.contentBottom)! - (viewPortHandler?.offsetBottom)!, width: size.width, height: size.height)
            bottomLabel.textAlignment = .right
            scatterChart.addSubview(bottomLabel)
            
            
            let middleLabel = UILabel()
            text = dateFormatter.string(from: Calendar.current.date(byAdding: .hour, value: 12, to: startOfDay)!)
            middleLabel.text = text
            middleLabel.font = font
            middleLabel.textColor = color
            size = text.size(attributes: [NSFontAttributeName: font])
            middleLabel.frame = CGRect(x: (viewPortHandler?.contentLeft)! - (viewPortHandler?.offsetLeft)!, y: (viewPortHandler?.contentCenter.y)! - size.height/2, width: size.width, height: size.height)
            middleLabel.textAlignment = .right
            scatterChart.addSubview(middleLabel)
            self.labelsAreDrown = true
            
        }
    }
    
    func updateTableData (){
        self.tableView?.reloadData()
        self.stopActivityAnimation()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.setupView()
        self.setupChartView()
        self.basePresenter = presenter
        presenter.weeklyChartView = self
        presenter.currentView = .weeklyStressView
        
        tableView?.delegate = presenter
        tableView?.dataSource = presenter
        
        tableView?.register(UINib(nibName: "WeeklyStressCell", bundle: nil), forCellReuseIdentifier: "WeeklyStressCell")
        super.viewWillAppear(animated)
    }
    
    /*override func viewDidLoad() {
        self.setupView()
        self.setupChartView()
        self.basePresenter = presenter
        presenter.weeklyChartView = self
        presenter.currentView = .weeklyStressView

        tableView?.delegate = presenter
        tableView?.dataSource = presenter
        
        tableView?.register(UINib(nibName: "WeeklyStressCell", bundle: nil), forCellReuseIdentifier: "WeeklyStressCell")
        
        presenter.updateWeeklyStress()
    }*/
}

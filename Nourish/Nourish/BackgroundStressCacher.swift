//
//  BackgroundStressCacher.swift
//  Nourish
//
//  Created by Nova on 4/14/17.
//  Copyright © 2017 SSIC. All rights reserved.
//

import UIKit
import RxSwift

class BackgroundStressCacher: NSObject {
    
    static let shared = BackgroundStressCacher()
    
    var disposeBag : DisposeBag? = DisposeBag()
    let stressRepository = UserActivityRepository()
    var caching : Disposable? = nil
    var queue : DispatchQueue? = nil
    var isCaching : Bool = false
    
    
    func cacheTodayStress(completion : @escaping () -> Void) {
        let _ = self.stressRepository.fetchStressForToday().subscribe(onNext: { models in
            print(models)
        }, onError: {error in
            print(error)
        }, onCompleted: {
            completion()
        }, onDisposed: {}).addDisposableTo(self.disposeBag!)
    }
    
    func startCaching() {
        LogUtility.logToFile("started stress caching")
        if self.isCaching == false {
            self.isCaching = true
            self.queue = DispatchQueue(label: "queuename", attributes: .concurrent)
            self.queue?.async {
                if self.disposeBag != nil {
                    self.cacheTodayStress(completion: {
                        let _ = self.stressRepository.fetchStressWeekly().takeUntil(self.rx.deallocated).subscribe(onNext: { [unowned self] models in
                            self.isCaching = false
                        }, onError: {error in
                            self.isCaching = false
                        }, onCompleted: {
                            LogUtility.logToFile("completed stress caching")
                        }, onDisposed: {}).addDisposableTo(self.disposeBag!)
                    })
                }
            }
        }
        
    }
    
}

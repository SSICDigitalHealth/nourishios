//
//  MealPlannerViewModel.swift
//  Nourish
//
//  Created by Vlad Birukov on 29.08.17.
//  Copyright © 2017 SSIC. All rights reserved.
//

import Foundation

class MealPlannerViewModel: NSObject {
    var targetCalories: Double = 0.0
    var score: Double = 0.0
    var meal: [MealViewModel]?
    var preference = PreferenceMealPlannerViewModel()
}

class MealViewModel: NSObject {
    var nameOccasion: Ocasion = Ocasion.breakfast
    var calories = [Double]()
    var nameFood =  [[String]]()
    var detail = [OccasionMealPlannerViewModel]()
}

class PreferenceMealPlannerViewModel: NSObject {
    var lunch = 0
    var breakfast = 0
    var dinner = 0
    var snacks = 0
}


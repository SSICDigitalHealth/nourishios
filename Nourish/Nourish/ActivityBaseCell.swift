//
//  ActivityBaseCell.swift
//  Nourish
//
//  Created by Andrey Zmushko on 8/31/17.
//  Copyright © 2017 SSIC. All rights reserved.
//

import UIKit

class ActivityBaseCell: UITableViewCell {

    let boldAttr: [String : Any] = [NSFontAttributeName : UIFont(name: "Campton-Bold", size: 18)!, NSForegroundColorAttributeName : NRColorUtility.hexStringToUIColor(hex: "303030")]
    let bookAttr: [String : Any] = [NSFontAttributeName : UIFont(name: "Campton-Book", size: 18)!, NSForegroundColorAttributeName : NRColorUtility.hexStringToUIColor(hex: "303030")]
    let discriptionAttr: [String: Any] = [NSFontAttributeName : UIFont.systemFont(ofSize: 14, weight: UIFontWeightRegular), NSForegroundColorAttributeName : NRColorUtility.hexStringToUIColor(hex: "808080")]
    
    func setupWith(presenter: ActivityBaseCellProtocol) {
        presenter.fetchModel(For: self)
    }
    
    
    
}

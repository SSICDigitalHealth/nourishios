//
//  FoodDiaryViewController.swift
//  Nourish
//
//  Created by Nova on 11/10/16.
//  Copyright © 2016 Samsung. All rights reserved.
//

import UIKit

class MealDiaryViewController: BasePresentationViewController, MealDiarySenderProtocol,FoodDetailsViewControllerDelegate {
    
    @IBOutlet weak var tabledView : MealDiaryView?
    @IBOutlet weak var pickerView : PickerViewContainerView?
    var closeButton : UIBarButtonItem!
    var dateToFetch : Date!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setUpNavigationBar()
        self.tabledView?.viewController = self
        self.baseViews = [self.tabledView!]
        self.automaticallyAdjustsScrollViewInsets = false
        self.pickerView?.viewController = self
        self.pickerView?.setupPicker()
        self.title = kFootDiaryTitle
        self.testUserInetrface()
    }
    func showAddFoodControllerForOcasion(ocasion: Ocasion, date: Date,showPredictions:Bool) {
        LogUtility.logToFile("Controller addMeal for \(ocasion)")
        //Show search screen
        let searchVC = NRFoodSearchViewController(nibName: "NRFoodSearchViewController", bundle: nil)
        searchVC.title = Ocasion.stringDescription(servingType: ocasion).uppercased()
        searchVC.occasion = ocasion
        searchVC.date = date
        searchVC.shouldPredictMeal = showPredictions
        self.navigationController?.pushViewController(searchVC, animated: false)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.pickerView?.selectIndexFor(date: dateToFetch)
        self.title = "Food Diary"
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.title = "Back"
    }
    
    func testUserInetrface() {
        
    }
    
    func setUpNavigationBar () {
        let titleAttribute = self.navigationBarTitleAttribute()
        self.closeButton =  UIBarButtonItem(image: UIImage(named:"close_icon"), style: .plain, target: self, action: #selector(self.closeAction(_:)))
        self.navigationController?.navigationItem.leftBarButtonItem = closeButton
        navigationItem.leftBarButtonItem = self.closeButton
        navigationItem.title = self.title
        self.navigationController?.navigationBar.isTranslucent = false
        self.navigationController?.navigationBar.titleTextAttributes = titleAttribute
        self.navigationController?.navigationBar.tintColor = NRColorUtility.nourishNavBarFontColor()
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
    }
    
    func changeFoodAmountWith(model : foodSearchModel) {
        let vc = NRFoodDetailsViewController(nibName: "NRFoodDetailsViewController", bundle: nil)
        vc.title = model.foodTitle
        vc.foodSearchModel = model
        if model.userPhotoId == nil {
            vc.backgroundImage = NavigationUtility.takeScreenShot()
            vc.delegate = self
            DispatchQueue.main.async {
                self.present(vc, animated : true)
            }
        } else {
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    
    func foodDetailsViewControllerDidFinishWith(foodSearchModel: foodSearchModel, presentedModal: Bool) {
        if presentedModal == true {
            self.dismiss(animated: true, completion: nil)
        }
    }
    
    func showGroupDetailsControllerForOcasion(ocasion:Ocasion,date:Date,model:MealRecordModel) {
        let vc = GroupFoodDetailsViewController(nibName: "GroupFoodDetailsViewController", bundle: nil)
        vc.date = date
        vc.ocasion = ocasion
        vc.mealRecord = model
        vc.title = model.foodTitle
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func showGroupFavouriteVC(mealGroup: [MealRecordModel] ) {
        DispatchQueue.main.async {
            let viewController = SaveGroupFavViewController(nibName: "SaveGroupFavViewController", bundle: nil)
            viewController.groupFavMealArray = mealGroup
            viewController.mealVC = self
            viewController.providesPresentationContextTransitionStyle = true;
            viewController.definesPresentationContext = true;
            viewController.modalPresentationStyle=UIModalPresentationStyle.overCurrentContext
            viewController.view.backgroundColor = UIColor.black.withAlphaComponent(0.3)
            self.present(viewController, animated: true, completion: nil)
        }
    }
    
    override func respondToSwipeGesture(gesture: UISwipeGestureRecognizer) {
        let touchPoint = gesture.location(in: self.view)
        if touchPoint.x < 30 {
            super.respondToSwipeGesture(gesture: gesture)
        }
    }

}

//
//  UserMessage.swift
//  Nourish
//
//  Created by Nova on 12/1/16.
//  Copyright © 2016 SSIC. All rights reserved.
//

import UIKit

class UserMessage: NSObject {
    var weight : Double = 0.0
    var stepsFor10Minutes : Double  = 0
    var height : Double = 0.0
    var dietrestriction : DietaryPreference?
    var age : Int?
    var userId : String?
    var hoursSinceMidnight : Double {
        get {
            let now = Date()
            let midnight = Calendar.current.startOfDay(for: now)
            let secondsBetween = now.timeIntervalSince(midnight) as Double
            return secondsBetween / 3600.0
        }
    }
    var calories : Double = 0.0
    var weightLossGoalRemaining : Double = 0
    var heartRate : Double = 0
    var steps : Double = 0
    var activity : String?
    var averageHeartRateFor10Minutes : Double = 0
    var gender : String?
}

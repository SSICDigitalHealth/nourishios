//
//  NutritionInteractor.swift
//  Nourish
//
//  Created by Nova on 11/21/16.
//  Copyright © 2016 SSIC. All rights reserved.
//

import UIKit
import RxSwift


class NutritionInteractor: NSObject {
    let historyRepo = MealHistoryRepository.shared
    
    func nutritionFor(date : Date) -> Observable<[Nutrition]> {
        return self.historyRepo.getFoodScoreFor(date: date)
    }
    
    private func generateRandomNutrition() -> Nutrition {
        let nutrition = Nutrition()
        nutrition.vitamins = Double(self.generateRandomBetween(max: 100, min: 0)) / 100
        nutrition.minerals = Double(self.generateRandomBetween(max: 100, min: 0)) / 100
        nutrition.macronutrients = Double(self.generateRandomBetween(max: 100, min: 0)) / 100
        nutrition.electrolytes = Double(self.generateRandomBetween(max: 100, min: 0)) / 100
        nutrition.nutritionScore = Double(self.generateRandomBetween(max: 100, min: 0))
        return nutrition
    }
    
    private func generateRandomBetween(max : Int, min : Int) -> Int {
        return min + Int(arc4random_uniform(UInt32(max - min + 1)))
    }

    func nutritionsForPeriod(period : period) -> Observable<[Nutrition]> {
        if period == .today {
            return self.nutritionFor(date: Date())
        } else {
            return self.historyRepo.getFoodScoreForePeriod(period: period)
        }
    }
    
    func getNutritionScoreForToday() -> Observable<Double> {
        let object = self.nutritionFor(date: Date()).flatMap({ nutritions -> Observable<Double> in
            let score = (nutritions.first?.nutritionScore)!
            return Observable.just(score)
        })
        return object
    }
    
    func caloriesForPeriod(period : period) -> Observable<[CaloriesStats]> {
        return self.historyRepo.getCaloriesStatsFor(period: period)
    }
}

//
//  NRGoalOptionViewController.swift
//  Nourish
//
//  Created by Vino (Vinodha) Sundaramoorthy on 11/28/16.
//  Copyright © 2016 SSIC. All rights reserved.
//

import UIKit

class NRGoalOptionViewController: BasePresentationViewController {
    @IBOutlet weak var optionView : NRGoalOptionsView?
    var userGoal : UserGoal!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        optionView?.controller = self
        self.baseViews = [optionView!]
        self.title = "Choose Goal"
    }
    
    func setUserGoal() {
        DispatchQueue.main.async {
            let userGoal = self.optionView?.presenter.userGoal
            let goalVC = self.navigationController?.viewControllers
            _ = self.navigationController?.popViewController(animated: false)
            
            if (goalVC?.count)! > 0 {
                let VC = goalVC?[0] as! NRManageGoalViewController
                VC.manageGoalsView.refreshWithGoal(goal:userGoal!)
            }
        }
    }
    
    func setUpNavigationBarButtons () {
        let titleAttribute = self.navigationBarTitleAttribute()
        navigationItem.title = self.title
        self.navigationController?.navigationBar.titleTextAttributes = titleAttribute
        self.navigationController?.navigationBar.barTintColor = UIColor.white
        self.navigationController?.navigationBar.tintColor = NRColorUtility.navigationTintColor()
    }
}

//
//  GetOptifastSettingsInteractor.swift
//  Nourish
//
//  Created by Gena Mironchyk on 10/18/17.
//  Copyright © 2017 SSIC. All rights reserved.
//

import UIKit
import RxSwift
class GetOptifastSettingsInteractor: NSObject {
    
    private let optifastRepo = OptifastSettingsRepository()
    
    func execute() -> Observable<OptifastSettingsViewModel> {
        return self.optifastRepo.getOptifastSettingsModel().flatMap{model -> Observable<OptifastSettingsViewModel> in
            return Observable.just(self.transform(model: model))
        }
    }
    
    private func transform(model : OptifastSettingsModel) -> OptifastSettingsViewModel {
        let viewModel = OptifastSettingsViewModel()
        viewModel.optifastProductsTotal = model.optifastProductsTotal
        viewModel.optifastUserProducts = model.optifastUserProducts
        viewModel.optifastUserOcasions = model.optifastUserOcasions
        return viewModel
    }
}

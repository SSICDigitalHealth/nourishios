//
//  MealPlannerCache_to_MealPlannerModel.swift
//  Nourish
//
//  Created by Gena Mironchyk on 9/13/17.
//  Copyright © 2017 SSIC. All rights reserved.
//

import UIKit

class MealPlannerCache_to_MealPlannerModel: NSObject {
    
    func transform(cache : MealPlannerCache) -> MealPlannerModel {
        
        let model = MealPlannerModel()
        model.targetCalories = cache.totalCalories
        model.score = cache.score
        model.meal = self.mealModelsFrom(ocasionDataArray: Array(cache.ocasionData))
        
        let preferenceMealPlannerModel = PreferenceMealPlannerModel()
        preferenceMealPlannerModel.breakfast = cache.breakfast
        preferenceMealPlannerModel.dinner = cache.dinner
        preferenceMealPlannerModel.lunch = cache.lunch
        preferenceMealPlannerModel.snacks = cache.snacks
        
        model.preference = preferenceMealPlannerModel
        
        if cache.optifastMealsData != nil {
            var optifastFood = [foodSearchModel]()
            let optifastArray = self.transformToArray(data: cache.optifastMealsData!)
            
            for foodElement in optifastArray {
                optifastFood.append(self.ocasionModelFrom(dict: (foodElement as? [String: Any] ?? [String: Any]())))
            }
        
        
            if model.meal != nil {
                for meal in model.meal! {
                    if meal.nameFood.count > 0 && meal.detail.count > 0 {
                        for optifast in optifastFood {
                            let occasionMealPlannerModel = OccasionMealPlannerModel()
                            occasionMealPlannerModel.occasion = [(foodSearchModel, RecipiesModel?)]()
                            occasionMealPlannerModel.occasion? = [(optifast, nil)]
                            meal.detail.append(occasionMealPlannerModel)
                            meal.calories.append(Double(optifast.calories)!)
                            meal.nameFood.append([optifast.foodTitle])
                        }
                    }
                }
            }
        }
        
        
        
        return model
    }
    
    
    private func mealModelsFrom(ocasionDataArray : [OcasionData]) -> [MealModel] {
        var arrayToReturn = [MealModel]()
        for ocasionData in ocasionDataArray {
            let mealModel = MealModel()
            
            var details = [OccasionMealPlannerModel]()
            var ocasionsModels = [(foodSearchModel, RecipiesModel?)]()
            
            
            mealModel.calories.append(ocasionData.calsTotal)
            mealModel.nameOccasion = Ocasion.enumFromMealPlanner(string: ocasionData.ocasionName?.stringValue ?? "")
            var arrayOfFoodNames = [String]()
            let dictionaryRepresentationsArray = self.transformToDictionary(data: ocasionData.mealsData!)
            
            for i in 0..<dictionaryRepresentationsArray.count {
                arrayOfFoodNames.append(dictionaryRepresentationsArray[i]["food_name"] as! String)
                ocasionsModels.append((self.ocasionModelFrom(dict: dictionaryRepresentationsArray[i]), self.transform(model: ocasionData.recipiesData[i])))
            }
//            for foodDetails in dictionaryRepresentationsArray {
//                arrayOfFoodNames.append(foodDetails["food_name"] as! String)
//                ocasionsModels.append((self.ocasionModelFrom(dict: foodDetails), self.transform(model: ocasionDataArray[0].recipiesData)))
//            }
            
            let occasionMealPlannerModel = OccasionMealPlannerModel()
            occasionMealPlannerModel.occasion = ocasionsModels
            
            details.append(occasionMealPlannerModel)
            mealModel.detail = details
            mealModel.nameFood.append(arrayOfFoodNames)
            arrayToReturn.append(mealModel)
        }
        
        return arrayToReturn
    }
    
    
    private func ocasionModelFrom(dict: [String : Any]) -> foodSearchModel {
        var model = foodSearchModel()
        model.amount = dict["amount"] as? Double ?? 0.0
        model.unit = dict["unit"] as? String ?? ""
        model.foodId = dict["food_id"] as? String ?? ""
        model.mealIdString = dict["food_id"] as? String ?? ""
        model.foodTitle = dict["food_name"] as? String ?? ""
        model.grams = dict["grams"] as? Double
        model.caloriesPerGram = dict["kcal_per_gram"] as? Double ?? 0.0
        model.calories = String((dict["kcal_per_gram"] as? Double ?? 0.0) * (dict["grams"] as? Double ?? 0.0))
        return model
    }
    
    private func transformToDictionary(data : Data) -> [[String : Any]] {
        return try! JSONSerialization.jsonObject(with: data, options: .mutableLeaves) as? [[String : Any]] ?? [[String: Any]]()
    }
    
    private func transformToArray(data: Data) -> [Any] {
        return try! JSONSerialization.jsonObject(with: data, options: .mutableLeaves) as? [Any] ?? [Any]()
    }
    
    private func transform(model: RecipiesCacheModel) -> RecipiesModel? {
        let recipiesModel = RecipiesModel()
        recipiesModel.foodId = model.foodId
        recipiesModel.amount = model.amount
        recipiesModel.nameFood = model.nameFood
        
        if model.ingredient.count > 0 {
            for stringObj in model.ingredient {
                recipiesModel.ingredient.append(stringObj.stringValue)
            }
        } else {
            return nil
        }
        
        
        if model.groupId != nil {
            recipiesModel.groupId = model.groupId
        }
        
        if model.pathImage != "" {
            recipiesModel.image = self.loadImageFromPath(path: model.pathImage)
            recipiesModel.pathForImage = model.pathImage
        }
        
        recipiesModel.numberCal = model.numberCal
        recipiesModel.grams = model.grams
        recipiesModel.unit = model.unit
        
        if model.instruction.count > 0 {
            for stringObj in model.instruction {
                recipiesModel.instructions.append(stringObj.stringValue)
            }
        }
        
        recipiesModel.isFavorite = model.isFavorite
        
        return recipiesModel
    }
    
    private func loadImageFromPath(path: String) -> UIImage? {
        var imageFood: UIImage?
        
        let documentsURL = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first!
        let filePath = documentsURL.appendingPathComponent(path).path
        
        if FileManager.default.fileExists(atPath: filePath) {
            imageFood = UIImage(contentsOfFile: filePath)
        }
        
        return imageFood
    }
}

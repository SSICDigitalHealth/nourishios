//
//  FoodCardCache.swift
//  Nourish
//
//  Created by Gena Mironchyk on 8/21/17.
//  Copyright © 2017 SSIC. All rights reserved.
//

import UIKit
import RealmSwift

class FoodCardCache: Object {
    var nutrients = List<Nutrient>()
    var servingTypes = List<RealmString>()
    dynamic var date : Date?
    dynamic var foodId : String?
}

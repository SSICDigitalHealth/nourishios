//
//  NoomRepository.swift
//  Nourish
//
//  Created by Nova on 12/16/16.
//  Copyright © 2016 SSIC. All rights reserved.
//

import UIKit
import RxSwift

final class NoomRepository: NSObject {
   /* var results : [NMFoodSearchResult] = []
    var dataBaseManager : NMFoodDatabaseManager?*/
    var store = NourishDataStore.shared
    let cacheStore = FNDDSSearchDataStore()
    let foodCardCacheStore = FoodCardCacheDataStore()
    
    static let shared : NoomRepository = {
        let instance = NoomRepository()
        //instance.loadDataBase()
        return instance
    }()
    
    
   /* func loadEntriesFrom(string : String, ocasion : Ocasion) -> Observable<[NMFood]> {
        return Observable.create { observer in
            self.dataBaseManager?.search(byTerm: string, meal: self.foodSearchMealFrom(ocasion: ocasion), existingFoods: nil, withCallback: { foods, error in
                self.foodNoomFrom(searchModels: foods, withCallback: {food, error in
                    if error == nil {
                        observer.onNext(food)
                        observer.onCompleted()
                    } else {
                        observer.onError(error!)
                    }
                    
                })
                
            })
            return Disposables.create()
        }
    }*/
    
    func loadEntriesFromFndds(search : String, token : String, userID : String) -> Observable<[foodSearchModel]> {
        let cache = self.cacheStore.fetchLastCache()
            for (_, element) in cache {
                if element.keys.contains(search) {
                    let cachedSearch = element[search]!
                    return Observable.just(cachedSearch)
                }
        }
        return self.store.searchFnddsFoodWith(search: search, token: token, userID: userID).flatMap({searchedModels -> Observable<[foodSearchModel]> in
                return Observable.just(self.mergeWithCache(searchString: search, mealsToMerge: searchedModels))
            })
    }
    
    private func mergeWithCache(searchString: String, mealsToMerge : [foodSearchModel]) -> [foodSearchModel] {
        var cache = self.cacheStore.fetchLastCache()
        var dictWithCachedMeals = [String : [foodSearchModel]]()
        dictWithCachedMeals[searchString] = mealsToMerge
        cache[Date()] = dictWithCachedMeals
        self.cacheStore.storeCache(cache: cache)
        return self.arrayOfMealsFrom(cache: cache, searchString: searchString)
    }
    
    
    private func arrayOfMealsFrom(cache : [Date : [String : [foodSearchModel]]], searchString : String) -> [foodSearchModel] {
        var arrayToReturn = [foodSearchModel]()
        for (_, element) in cache {
            if element.keys.contains(searchString) {
                arrayToReturn = element[searchString]!
            }
        }
        return arrayToReturn
    }
    
    /*func loadSingleEntrieFrom(string : String, ocasion : Ocasion) -> Observable<NMFood> {
        return Observable.create { observer in
            self.dataBaseManager?.search(byTerm: string, meal: self.foodSearchMealFrom(ocasion: ocasion), existingFoods: nil, withCallback: { foods, error in
                self.foodNoomFrom(searchModels: foods, withCallback: {food, error in
                    if error == nil {
                        if food.first != nil {
                            observer.onNext(food.first!)
                        }
                        observer.onCompleted()
                    } else {
                        observer.onError(error!)
                    }
                    
                })
                
            })
            return Disposables.create()
        }
    }
    
    func loadEntriesFromBarcode(barcode : String, ocasion : Ocasion) -> Observable<[NMFood]> {
        return Observable.create { observer in
            self.dataBaseManager?.search(byBarcode: barcode, withCallback: {food, error in
                if error == nil {
                    if food != nil {
                    let array = [food]
                        observer.onNext(array as! [NMFood])
                    }
                    observer.onCompleted()
                } else {
                    observer.onError(error!)
                }
            })
            return Disposables.create()
        }
    }
    
    func foodSearchMealFrom(ocasion : Ocasion) -> NMFoodSearchMeal {
        switch ocasion {
        case .breakfast:
            return NMFoodSearchMeal.breakfast
        case .dinner:
            return NMFoodSearchMeal.dinner
        case .lunch:
            return NMFoodSearchMeal.lunch
        case .snacks:
            return NMFoodSearchMeal.afternoonSnack
        }
    }
    
    func foodsTo(uuids : [NMFood]) -> [String] {
        var stringArray :[String] = []
        for food in uuids {
            stringArray.append(food.uuid)
        }
        return stringArray
    }
    
    func foodNoomFrom(searchModels : [NMFoodSearchResult],withCallback callback: @escaping FoodDatabaseKit.NMFoodDatabaseManagerFoundFoods) {
        var uids : [String] = []
        for uuidValue in searchModels {
            uids.append(uuidValue.foodUuid)
        }
        self.dataBaseManager?.search(byUuids: uids, withCallback: callback)
    }
    
    func foodFrom(uid : String) -> Observable<NMFood> {
        return Observable.create { observer in
            self.dataBaseManager?.search(byUuid: uid, withCallback: {food, error in
                if error != nil {
                    observer.onError(error!)
                } else {
                    if let fo = food {
                        observer.onNext(fo)
                        observer.onCompleted()
                    }                }
            })
            return Disposables.create()
        }
        
    }
    
    func foodNoomFrom(uid : String, withCallback callback: @escaping FoodDatabaseKit.NMFoodDatabaseManagerFoundFood) {
        self.dataBaseManager?.search(byUuid: uid, withCallback: callback)
    }
    func testInit(completion : @escaping (Bool) -> ()) {
        self.dataBaseManager?.search(byUuid: "", withCallback: {food, error in
            if error != nil {
                completion(true)
            } else {
                completion(false)
            }
        })
    }
    
    func foodSearchInitializedSuccessfully(_ foodSearch: NMFoodDatabaseManager!) {
        LogUtility.logToFile("Noom db initialized successfully")
    }
    
    func foodDatabaseUpgrading(_ foodSearch: NMFoodDatabaseManager!) {
        LogUtility.logToFile("Noom db upgrading")
    }
    
    
    func foodSearchInitializationFailed(_ foodSearch: NMFoodDatabaseManager!, error: Error!) {
        LogUtility.logToFile("Noom db initialization failed \(error)")
    }
    
    func foodDatabaseUpgradedSuccessfully(_ foodSearch: NMFoodDatabaseManager!) {
        LogUtility.logToFile("Noom db upgraded succesfully")
    }
    
    func foodDatabaseUpgradeFailed(_ foodSearch: NMFoodDatabaseManager!, error: Error!) {
        LogUtility.logToFile("Noom db upgrade failed \(error)")
    }*/
    
}

//
//  Json_to_MealRecordCache.swift
//  Nourish
//
//  Created by Nova on 12/13/16.
//  Copyright © 2016 SSIC. All rights reserved.
//

import UIKit

class Json_to_MealRecordCache: NSObject {
    
    func transform(dictionary : [String : Any]) -> MealRecordCache {
        let meal = MealRecordCache()
        meal.amount = dictionary["amount"] as? Double ?? 1.0
        meal.grams = dictionary["grams"] as? Double ?? 0.0
        
        if dictionary["datetime"] != nil {
            meal.date = self.dateFromUnixStamp(time: dictionary["datetime"] as? Double ?? 0.0)
        }
        
        meal.idString = dictionary["id"] as? String ?? ""
        meal.calories = dictionary["kcal"] as? Double ?? (dictionary["por_kcal"] as? Double ?? 0) * meal.amount
        meal.kCal = meal.calories
        meal.msreAmount = dictionary["msre_amount"] as? Double ?? 0.0
        
        if dictionary["msre_desc"] != nil {
            meal.msreDesc = dictionary["msre_desc"] as? String ?? ""
        }
        
        meal.msreGrams = dictionary["msre_grams"] as? Double ?? 0.0
        
        if dictionary["name"] != nil {
            meal.name = dictionary["name"] as? String ?? ""
        }
        
        meal.occasionEnum = Ocasion(rawValue: (dictionary["occasion"] as? Int ?? 0))!
        meal.portionKCal = meal.kCal / meal.amount
        
        if dictionary["unit"] != nil {
            meal.unit = dictionary["unit"] as? String ?? ""
        }
        
        if dictionary["userfoodid"] != nil {
            meal.userFoodId = dictionary["userfoodid"] as? String ?? ""
        }
        
        meal.portionKCal = dictionary["por_kcal"] as? Double ?? 0.0
    
        var stringNoom = dictionary["noom_id"] as? String
        
        if stringNoom != nil {
            stringNoom = self.noomDBStringFrom(stringV: stringNoom!)
        }
        
        meal.foodUid = stringNoom
        meal.isFromNoom = dictionary["is_noom"] as? Bool ?? false
        if dictionary["user_photo_id"] != nil {
            meal.userPhotoId = dictionary["user_photo_id"] as? String
        }
        
        if dictionary["foodgroup_summary"] != nil {
            let subDict = dictionary["foodgroup_summary"] as? [String : Any]
            
            let groupName = subDict?["name"] as? String
            let groupID = subDict?["foodgroup_id"] as? String
            let favorite = subDict?["favorite"] as? Bool
            let _ = subDict?["deleted"] as? Bool
            
            meal.groupName = groupName
            meal.groupID = groupID
            
            if subDict?["number_of_items"] != nil {
                meal.numberOfItems = subDict?["number_of_items"] as! Int
            }
            
            let photoID = subDict?["user_photo_id"] as? String
            
            if photoID != nil {
                meal.groupPhotoID = photoID
            }
            
            
            meal.isFavourite = favorite != nil ? favorite! : false
            meal.isGroupFavourite = meal.isFavourite
            
            if dictionary["foodgroupuuid"] != nil {
                meal.groupFoodUUID = dictionary["foodgroupuuid"] as? String
            }
            
        }
        return meal
    }
    
    func transform(array : [[String : Any]]) -> [MealRecordCache] {
        var arrayToReturn : [MealRecordCache] = []
        for object in array {
            arrayToReturn.append(transform(dictionary: object))
        }
        return arrayToReturn
    }
    
    func dateFromUnixStamp(time : Double) -> Date {
        let interval = time / 1000
        let date = Date(timeIntervalSince1970 : interval)
        return date
    }
    
    func noomDBStringFrom(stringV : String) -> String {
        return stringV.replacingOccurrences(of: "-", with: "").uppercased()
    }
}

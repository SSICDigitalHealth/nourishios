//
//  ReportsViewController.swift
//  Nourish
//
//  Created by Vino (Vinodha) Sundaramoorthy on 2/8/17.
//  Copyright © 2017 SSIC. All rights reserved.
//

import UIKit

class ReportsViewController: BasePresentationViewController {
    @IBOutlet weak var reportView :ReportView!

    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Reports"
        self.setUpNavigationBarButtons()
        reportView.controller = self
        self.baseViews = [reportView]
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        NavigationUtility.showTabBar(animated: false)
    }
    
    func setUpNavigationBarButtons () {
        let titleAttribute = self.simplyfiedBarTitileAttributes()
        navigationItem.title = self.title
        self.navigationController?.navigationBar.titleTextAttributes = titleAttribute
        self.navigationController?.navigationBar.barTintColor = NRColorUtility.symplifiedNavigationColor()
        self.navigationController?.navigationBar.tintColor = NRColorUtility.nourishNavBarFontColor()
    }
    
    func showDetailedReport(date : Date) {
        DispatchQueue.main.async {
            let recVC = DailyReportViewController(nibName: "DailyReportViewController", bundle: nil)
            recVC.reportDate = date
            self.navigationController?.pushViewController(recVC, animated: false)
            NavigationUtility.hideTabBar(animated: false)
        }
    }
}

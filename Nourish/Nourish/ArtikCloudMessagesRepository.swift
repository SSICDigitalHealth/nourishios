//
//  ArtikCloudMessagesRepository.swift
//  Nourish
//
//  Created by Nova on 12/1/16.
//  Copyright © 2016 SSIC. All rights reserved.
//

import UIKit
import RxSwift

class ArtikCloudMessagesRepository: NSObject {
    
    let artikStore = ArtikCloudConnectionManager()
    let toJsonMapper = UserMessage_to_Json()
    let deviceRepo = UserDevicesRepository.shared
    let userRepo = UserRepository.shared
    let toWithingsMessageMapper = Json_to_WithingsMessage()
    let toFitBitMessageMapper = Json_to_FitBitMessage()
    
    func uploadUserMessage(message : UserMessage) -> Observable<Bool> {
        return self.userRepo.getCurrentUserID().flatMap {userID -> Observable<Bool> in
            return self.deviceRepo.currentDevice(userID: userID).flatMap {userDevice -> Observable<Bool> in
                let deviceDtid = userDevice.dtidString
                
            return self.artikStore.uploadUserMessage(userMessage: self.toJsonMapper.transform(message: message), deviceId: deviceDtid)
            }
        }
    }
    
    
    func getWithingsMessages(device:UserDevice, startDate : Date, endDate : Date) -> Observable<[WithingsMessage]> {
        return self.artikStore.getDeviceMessagesFor(device: device, startDate: startDate, endDate: endDate).flatMap({messages -> Observable<[WithingsMessage]> in
            return Observable.just(self.toWithingsMessageMapper.transform(array: messages))
        })
    }
    
    func getFitBitMessages(device:UserDevice, startDate : Date, endDate : Date) -> Observable<[FitBitMessage]> {
        return self.artikStore.getDeviceMessagesFor(device: device, startDate: startDate, endDate: endDate).flatMap({messages -> Observable<[FitBitMessage]> in
            return Observable.just(self.toFitBitMessageMapper.transform(array: messages))
        })
    }

}

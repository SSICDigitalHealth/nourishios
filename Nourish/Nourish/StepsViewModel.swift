//
//  StepsViewModel.swift
//  Nourish
//
//  Created by Vlad Birukov on 29.08.17.
//  Copyright © 2017 SSIC. All rights reserved.
//

import Foundation

class StepsViewModel {
    var numberSteps: Double?
    var avgStepsDay: Double?
}

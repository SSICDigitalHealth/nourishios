//
//  MicroelementBalanceViewModel.swift
//  Nourish
//
//  Created by Vlad Birukov on 29.08.17.
//  Copyright © 2017 SSIC. All rights reserved.
//

import Foundation

class MicroelementBalanceViewModel: NSObject {
    var arrMicroelementName : [MicroelementNameViewModel]?
}

class MicroelementNameViewModel {
    var type: String = ""
    var target:Double = 0.0
    var arrMicroelement : [ElementViewModel]?
}

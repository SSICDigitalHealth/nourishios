//
//  NutrientDetailInformationViewModel.swift
//  Nourish
//
//  Created by Vlad Birukov on 29.08.17.
//  Copyright © 2017 SSIC. All rights reserved.
//

import Foundation

class NutrientDetailInformationViewModel: NSObject {
    var nutrientComeFrom = NutrientComeFromViewModel()
    var dataChart = NutrientChartViewModel()
    var message: [String]?
}

class NutrientChartViewModel: NSObject {
    var upperLimit: Double = 0.0
    var lowerLimit: Double = 0.0
    var unit: String = ""
    var dataCharts: [NutrientInformationChartViewModel]?
}

class NutrientInformationChartViewModel: NSObject {
    var label: String?
    var consumed: Double = 0.0
    var wholeGrainsConsumed: Double?
}

class NutrientComeFromViewModel: NSObject {
    var caption: [String]?
    var nameFood: [[String]]?
}

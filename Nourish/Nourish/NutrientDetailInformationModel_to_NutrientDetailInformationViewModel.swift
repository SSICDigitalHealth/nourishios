//
//  NutrientDetailInformationModel_to_NutrientDetailInformationViewModel.swift
//  Nourish
//
//  Created by Vlad Birukov on 29.08.17.
//  Copyright © 2017 SSIC. All rights reserved.
//

import Foundation

class NutrientDetailInformationModel_to_NutrientDetailInformationViewModel {
    func transform(model: NutrientDetailInformationModel) -> NutrientDetailInformationViewModel {
        let nutrientDetailInformationViewModel = NutrientDetailInformationViewModel()
        nutrientDetailInformationViewModel.nutrientComeFrom = self.transform(model: model.nutrientComeFrom)
        nutrientDetailInformationViewModel.dataChart = self.transform(model: model.dataChart)
        
        if model.message != nil {
            nutrientDetailInformationViewModel.message = [String]()
            nutrientDetailInformationViewModel.message = model.message
        }
        
        return nutrientDetailInformationViewModel
    }
    
    private func transform(model: NutrientComeFromModel) -> NutrientComeFromViewModel {
        let nutrientComeFromViewModel = NutrientComeFromViewModel()
        if model.caption != nil {
            nutrientComeFromViewModel.caption = [String]()
            nutrientComeFromViewModel.caption = model.caption
        }
        
        if model.nameFood != nil {
            nutrientComeFromViewModel.nameFood = [[String]]()
            for i in 0..<model.nameFood!.count {
                var section = [String]()
                if model.nameFood?[i].count != 0 {
                    for j in 0..<model.nameFood![i].count {
                        section.append((model.nameFood?[i][j])!)
                    }
                    nutrientComeFromViewModel.nameFood?.append(section)
                }
            }
        }
        
        return nutrientComeFromViewModel
    }
    
    private func transform(model: NutrientChartModel) -> NutrientChartViewModel {
        let nutrientChartViewModel = NutrientChartViewModel()
        nutrientChartViewModel.upperLimit = model.upperLimit
        nutrientChartViewModel.lowerLimit = model.lowerLimit
        nutrientChartViewModel.unit = model.unit
        
        if model.dataCharts != nil {
            nutrientChartViewModel.dataCharts = self.transform(model: model.dataCharts!)
        }
        
        return nutrientChartViewModel
    }
    
    func transform(model: [NutrientInformationChartModel]) -> [NutrientInformationChartViewModel] {
        var dataCharts = [NutrientInformationChartViewModel]()
        
        for i in 0..<model.count {
            let nutrientInformationChartViewModel = NutrientInformationChartViewModel()
            nutrientInformationChartViewModel.consumed = model[i].consumed
            
            if model.count > 7 {
                if model[i].label != nil {
                    if i == 1 {
                        nutrientInformationChartViewModel.label = self.monthlyDateFormatter(date: model[i - 1].label!)
                    } else if i == model.count - 2 {
                        nutrientInformationChartViewModel.label = self.monthlyDateFormatter(date: model[i + 1].label!)
                    } else {
                        nutrientInformationChartViewModel.label = ""
                    }
                }
            } else {
                if model[i].label != nil {
                    nutrientInformationChartViewModel.label = self.weeklyDateFormatter(date: model[i].label!).uppercased()
                }
            }
            
            if model[i].wholeGrainsConsumed != nil {
                nutrientInformationChartViewModel.consumed = model[i].consumed - model[i].wholeGrainsConsumed!
                nutrientInformationChartViewModel.wholeGrainsConsumed = model[i].wholeGrainsConsumed
            }
            dataCharts.append(nutrientInformationChartViewModel)
        }
        
        return dataCharts
    }
    
    private func weeklyDateFormatter(date: Date) -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "E"
        return dateFormatter.string(from: date)
    }
    
    private func monthlyDateFormatter(date: Date) -> String {
        let dateFormatter = DateFormatter()
        
        dateFormatter.dateFormat = "MM/dd"
        
        return dateFormatter.string(from: date)
    }
}

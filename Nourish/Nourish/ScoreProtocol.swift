//
//  ScoreProtocol.swift
//  Nourish
//
//  Created by Vino (Vinodha) Sundaramoorthy on 4/19/17.
//  Copyright © 2017 SSIC. All rights reserved.
//

import UIKit

protocol ScoreProtocol {
    func refreshScore(model:ScoreModel)
    func setUpNutrientTableView()
    func stopActivity()
}

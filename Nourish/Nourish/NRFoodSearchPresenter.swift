//
//  NRFoodSearchPresenter.swift
//  Nourish
//
//  Created by Vino (Vinodha) Sundaramoorthy on 11/16/16.
//  Copyright © 2016 SSIC. All rights reserved.
//

import UIKit
import AVFoundation
import SpeechKit
import RxSwift


enum SKSState {
    case idle
    case listening
    case processing
}


class NRFoodSearchPresenter: BasePresenter , UITableViewDelegate , UITableViewDataSource , UISearchBarDelegate , AVCaptureMetadataOutputObjectsDelegate , MGSwipeTableCellDelegate,NRSegmentedControlDelegate{
    var interactor = NRFoodSearchInteractor()
    var favInteractor = FavouritesInteractor()
    var originalSearchString : String = ""
    var searchResultList : [foodSearchModel] = []
    var favFoodResultsList : [MealRecordModel] = []
    var frequentMealsList : [MealRecordModel] = []
    var searchView : NRFoodSearchProtocol?
    var captureSession : AVCaptureSession!
    var date : Date?
    //Voice Capture
    var state = SKSState.idle
    var language: String!
    var contextTag: String!
    var endpointer: SKTransactionEndOfSpeechDetection!
    var skSession:SKSession?
    var skTransaction:SKTransaction?
    var favSelected : Bool = false
    var showPredictMeal : Bool = false
    
    //let toSearchModelMapper = NMFood_to_foodSearchModel()
    var occasion : Ocasion? = .breakfast
    
    func loadSearchResults(searchText : String, occasion : Ocasion, isFrequentMeals : Bool) -> Observable<[foodSearchModel]>{
        self.showPredictMeal = isFrequentMeals
        favSelected = false
        if isFrequentMeals == true {
            return interactor.getCountedFavoritesFor(ocasion : occasion)
        } else {
            if searchText != "" {
                return self.interactor.searchFNDDSFood(searchString: searchText)
            } else {
                return Observable.just([])
            }
            
//            return interactor.searchFoodWith(searchString: searchText, ocasion: occasion).flatMap {noomFood -> Observable<[foodSearchModel]> in
//                return Observable.just(self.toSearchModelMapper.transform(foods: noomFood))
//            }
        }
    }
    
    // MARK:UITableViewDelegate , UITableViewDataSource - Table View Delegates
    func numberOfSections(in tableView: UITableView) -> Int {
        if self.favSelected == true {
            if self.frequentMealsList.count > 0 {
                return 2
            } else {
                return 1
            }
        }

        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if self.favSelected == true {
            if section == 0 && self.frequentMealsList.count > 0 {
                return self.frequentMealsList.count
            }
            return self.favFoodResultsList.count
        } else {
            return self.searchResultList.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if self.favSelected == true {
            let section = indexPath.section
            let isFrequentMeal : Bool = self.frequentMealsList.count > 0 && section == 0 ? true : false
            let dataSourceArray : [MealRecordModel] = isFrequentMeal ? self.frequentMealsList : self.favFoodResultsList
            let favModel = dataSourceArray[indexPath.row]
            
            if isFrequentMeal {
                let cell : FrequentMealTableViewCell = tableView.dequeueReusableCell(withIdentifier: "kFrequentMealCell") as! FrequentMealTableViewCell
                cell.setupWithModel(record:favModel)
                cell.selectionStyle = UITableViewCellSelectionStyle.none
                cell.delegate = self
                return cell
            } else {
                let cell : FavMealTableViewCell = tableView.dequeueReusableCell(withIdentifier: "kFavMealCell") as! FavMealTableViewCell
                cell.setupWithModel(record:favModel)
                cell.selectionStyle = UITableViewCellSelectionStyle.none
                cell.delegate = self
                return cell
            }
           
        } else {
//            let cell : NRFoodSearchTableViewCell = tableView.dequeueReusableCell(withIdentifier: "kFoodSearchCell") as! NRFoodSearchTableViewCell
//            let foodModel = self.searchResultList[indexPath.row]
//            cell.setupFoodResultWithModel(foodModel:foodModel)
//            
            let cell : FrequentMealTableViewCell = tableView.dequeueReusableCell(withIdentifier: "kFrequentMealCell") as! FrequentMealTableViewCell
            if indexPath.row < self.searchResultList.count {
                cell.setupWithModel(model: self.searchResultList[indexPath.row])
            }
            cell.selectionStyle = UITableViewCellSelectionStyle.none
            cell.delegate = self
            return cell

        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if !self.favSelected {
            var foodModel = self.searchResultList[indexPath.row]
            foodModel.originalSearchString = originalSearchString
            searchView?.openFoodDetailsWithModel(model: foodModel, isFrequentMeal : self.showPredictMeal)
        } else {
            let section = indexPath.section
            let dataSourceArray : [MealRecordModel] = self.frequentMealsList.count > 0 ? section == 0 ? self.frequentMealsList : self.favFoodResultsList : self.favFoodResultsList
            let favRecord = dataSourceArray[indexPath.row]
            if favRecord.groupedMealArray.count == 1 || (section == 0 && favRecord.groupedMealArray.count == 0) {
                let mapper = MealRecord_to_foodSearchModel()
                let model = (section == 0 && favRecord.groupedMealArray.count == 0) ? favRecord : favRecord.groupedMealArray.first
                model?.isFavourite = favRecord.isFavourite
                var food = mapper.transform(record: model!)
                if self.occasion != nil {
                    food.occasion = self.occasion!
                }
                
                food.groupID = favRecord.groupID
                food.originalSearchString = originalSearchString
                searchView?.openFoodDetailsWithModel(model: food, isFrequentMeal : self.showPredictMeal)
            } else {
                let dateToAdd = date ?? Date()
                
                if self.occasion != nil {
                    favRecord.ocasion = self.occasion!
                }
                searchView?.openGroupDetailswithModel(model: favRecord, date : dateToAdd)
            }
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        let heightPadding : CGFloat = 60
        let widthPadding : CGFloat = 76
        
        if self.favSelected == true {
            //Calculated with the trailing space and image size
            //widthPadding = 100
            let section = indexPath.section
            let isFrequentMeal : Bool = self.frequentMealsList.count > 0 && section == 0 ? true : false
            let dataSourceArray : [MealRecordModel] = isFrequentMeal ? self.frequentMealsList : self.favFoodResultsList
            let favModel = dataSourceArray[indexPath.row]
            let titleHeight = favModel.descriptionText().sizeOfBoundingRect(width: UIScreen.main.bounds.width - widthPadding, height: maxLabelSize.height, font:UIFont.systemFont(ofSize: 15, weight: UIFontWeightRegular)).height
            return heightPadding + titleHeight
        } else {
            let data = self.searchResultList[indexPath.row]
            let titleHeight = data.foodTitle.sizeOfBoundingRect(width: UIScreen.main.bounds.width - widthPadding, height: maxLabelSize.height, font:UIFont.systemFont(ofSize: 14, weight: UIFontWeightRegular)).height
            return heightPadding + titleHeight
        }
    }
    
    // MARK: UISearchBarDelegate - Search Bar Delegates
    func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
        let searchField = searchBar.value(forKey: "_searchField") as! UITextField
        searchField.layer.borderColor = NRColorUtility.hexStringToUIColor(hex: "#bfbfbf").cgColor
        self.searchView?.toggleInActiveSearchImage(canShow:(searchField.text?.count)! > 0 ? true:false )

    }
    
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        let searchField = searchBar.value(forKey: "_searchField") as! UITextField
        searchField.layer.borderColor = NRColorUtility.appBackgroundColor().cgColor
        self.searchView?.deselctAllTabs(dismissKeyboard: false)
        self.searchView?.toggleInActiveSearchImage(canShow:(searchField.text?.count)! > 0 ? true:false )
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        self.searchView?.toggleInActiveSearchImage(canShow:searchText.count > 0 ? true:false )
    }
    
    func loadFavouriteFoods(completion : @escaping (Bool) ->()) {
        favSelected = true
        self.searchView?.startActivityAnimation()
        let favs = favInteractor.fetchFavorites()
        /*
        let frequentMeals : Observable<[MealRecordModel]> = interactor.getFrequentMealsForOcassion(ocassion: self.occasion!,showPrediction: self.showPredictMeal)

        let zipValue = Observable.zip(favs,frequentMeals , resultSelector :  { (favList,frequentMealsList) in
            self.favFoodResultsList = favList
            self.frequentMealsList = frequentMealsList
        })
        */
        self.subscribtions.append(favs.observeOn(MainScheduler.instance).subscribe(onNext: { [unowned self] model in
            self.favFoodResultsList = model
            completion(true)
        }, onError: {error in }, onCompleted: {
        }, onDisposed: {}))
    }
    
    // MARK: AVCaptureMetaDataOutput  - Scanner Delegates
    func captureOutput(_ captureOutput: AVCaptureOutput!, didOutputMetadataObjects metadataObjects: [Any]!, from connection: AVCaptureConnection!) {
        print("Output Captured")
        
        // Get the first object from the metadataObjects array.
        if let barcodeData = metadataObjects.first {
           // let barcode : String!
            // Turn it into machine readable code
            let barcodeReadable = barcodeData as? AVMetadataMachineReadableCodeObject;
            if let readableCode = barcodeReadable {
                print("Bar code detected \(readableCode.stringValue)")
                EventLogger.logBarcodeScanSuccessfull()
               // barcode = self.rightBarcodeFromScanned(barcode: readableCode.stringValue)
                //self.reloadDataWith(barcode: barcode)
            }
            
            // Vibrate the device to give the user some feedback.
            AudioServicesPlaySystemSound(SystemSoundID(kSystemSoundID_Vibrate))
            captureSession.stopRunning()
            //searchView?.finishScanning(barcode: barcode)
        }
    }
    
    /* private func rightBarcodeFromScanned(barcode : String) ->String {
        var rightBarcode = barcode
        if rightBarcode[rightBarcode.startIndex] == "0" && rightBarcode.characters.count == 13 {
            rightBarcode.remove(at: rightBarcode.startIndex)
        }
        return rightBarcode

    }*/
    
    
    // MARK: SKTransactionDelegate - Voice search delegates
    /*func transactionDidBeginRecording(_ transaction: SKTransaction!) {
        print("Begin Recording")
        searchView?.updateVoiceSearchState(state: .listening)
    }
    
    func transactionDidFinishRecording(_ transaction: SKTransaction!) {
        print("Finished Recording")
        searchView?.updateVoiceSearchState(state: .processing)
        
        //Show progress indicator
    }
    
    func transaction(_ transaction: SKTransaction!, didReceive recognition: SKRecognition!) {
        print("Recognied Text \([recognition.text])")
        searchView?.hideVoiceProgressView(searchString: recognition.text)
        //self.loadSearchResults(searchText: recognition.text, occasion: occasion!)
    }
    
    func transaction(_ transaction: SKTransaction!, didReceiveServiceResponse response: [AnyHashable : Any]!) {
        print("Received Service Response \(response)")
    }
    
    func transaction(_ transaction: SKTransaction!, didReceive interpretation: SKInterpretation!) {
        print("Interpretation Result \([interpretation.result])")
        searchView?.updateVoiceSearchState(state: .idle)
        resetTransaction()
    }
    
    func transaction(_ transaction: SKTransaction!, didFinishWithSuggestion suggestion: String) {
        print("Finished With Suggestion")
        searchView?.updateVoiceSearchState(state: .idle)
        searchView?.hideVoiceProgressView(searchString: suggestion)
        resetTransaction()
    }
    
    func transaction(_ transaction: SKTransaction!, didFailWithError error: Error!, suggestion: String!) {
        LogUtility.logToFile(error.localizedDescription)
        
        // Something went wrong. Ensure that your credentials are correct.
        // The user could also be offline, so be sure to handle this case appropriately.
        searchView?.updateVoiceSearchState(state: .idle)
        searchView?.hideVoiceProgressView(searchString: "")
        resetTransaction()
    }
    
    func resetTransaction() {
        //Set the SKTranstion to nil and restore button state
    }*/
    
    /*func reloadDataWith(barcode : String) {
        self.favSelected = false
        let _ = self.interactor.searchFoodWith(barcode: barcode, ocasion: self.occasion!).observeOn(MainScheduler.instance).subscribe(onNext: {models in
            self.searchResultList = self.toSearchModelMapper.transform(foods: models)
        }, onError: {error in
            LogUtility.logToFile(error)
        }, onCompleted: {
            if self.searchResultList.count > 0 {
                self.searchView?.reloadTableView()
            } else {
                if self.searchView?.selectedPickerIndex() == SelectedTab.kFrequentMealsTab {
                    self.searchView?.showEmptyFrequentMealsLabel()
                } else {
                    self.searchView?.showNoResultsLabel()
                }
                
            }
        }, onDisposed: {})

    }*/
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        self.searchBarSearchButtonClicked(searchBar, dismissKeyboard: true)
    }
    
    func searchBarSearchButtonClicked( _ searchBar: UISearchBar, dismissKeyboard : Bool)
    {
        var text = searchBar.text?.lowercased()
        originalSearchString = text!
        if text?.count == 0 {
            text = ""
            DispatchQueue.main.async {
                self.searchView?.showEmptyFrequentMealsLabel()
            }
        } else {
            EventLogger.logOnSearchButtonClick()
            self.searchView?.startActivityAnimation()
            self.subscribtions.append(self.loadSearchResults(searchText: text!, occasion: self.occasion!, isFrequentMeals: false).observeOn(MainScheduler.instance).subscribe(onNext: { [unowned self] models in
                self.searchResultList = models
                self.searchView?.stopActivityAnimation()
            }, onError: {error in
                LogUtility.logToFile(error)
                self.searchView?.stopActivityAnimation()
                self.searchView?.parseError(error : error,completion: nil)
            }, onCompleted: {
                self.searchView?.stopActivityAnimation()
                if self.searchResultList.count > 0 {
                    self.searchView?.reloadTableView()
                } else {
                    if self.searchView?.selectedPickerIndex() == (SelectedTab).kFrequentMealsTab {
                        self.searchView?.showEmptyFrequentMealsLabel()
                    } else {
                        self.searchView?.showNoResultsLabel()
                    }
                    
                }
                if dismissKeyboard == true {
                    searchBar.endEditing(false)
                }

            }, onDisposed: {}))
        }
        
    }
    
    func swipeTableCell(_ cell: MGSwipeTableCell, canSwipe direction: MGSwipeDirection) -> Bool {
        return self.favSelected
    }
    
    func swipeTableCell(_ cell: MGSwipeTableCell, swipeButtonsFor direction: MGSwipeDirection, swipeSettings: MGSwipeSettings, expansionSettings: MGSwipeExpansionSettings) -> [UIView]? {
        swipeSettings.transition = MGSwipeTransition.border;
        expansionSettings.buttonIndex = 0;
        
        let index =  self.searchView?.getGroupIndexPathForCell(cell: cell)
        let record = self.favFoodResultsList[(index?.row)!]
        
        if direction == MGSwipeDirection.rightToLeft {
            expansionSettings.fillOnTrigger = false;
            expansionSettings.threshold = 3;
            return [
                MGSwipeButton(title: "Delete Favourite", icon: UIImage(named:"deleteitem_icon"), backgroundColor: NRColorUtility.deleteItemColor(), callback: { (cell) -> Bool in
                    let fGroup = record.groupID
                    if fGroup != nil {
                        self.searchView?.startActivityAnimation()
                        self.subscribtions.append(self.favInteractor.deleteFavouriteGroup(groupID: fGroup!).observeOn(MainScheduler.instance).subscribe(onNext: { [unowned self] success in
                            print(success)
                                self.loadFavouriteFoods(completion: { finished in
                                    if finished == true {
                                        //onCompleted called
                                            self.searchView?.reloadTableView()
                                            self.searchView?.stopActivityAnimation()
                                    }
                                })
                        }, onError: {error in
                            print(error)
                            self.searchView?.parseError(error : error, completion: nil)
                        }, onCompleted: {}, onDisposed: {}))
                        
                    }
                    return true
                })
            ]
        }
        return nil
    }

    func segmentedControlDidDeselectAll(segmentedControl : NRSegmentedControl, dismissKeyboard : Bool) {
        //self.searchView?.showSearchResult(dismissKeyboard : dismissKeyboard)
    }
    
    func segmentedControlDidPressedItemAtIndex(segmentedControl: NRSegmentedControl, index: Int) {
        self.searchView?.showTableView()
        self.searchView?.setPreviousSelectedSegment(segmentIndex: index)
        self.searchView?.clearSearchBar()
        self.searchView?.hideVoiceProgressView(searchString: "")
        
        if index == 0 {
            self.subscribtions.append(self.loadSearchResults(searchText: "", occasion: occasion!, isFrequentMeals: true).observeOn(MainScheduler.instance).subscribe(onNext: { [unowned self] models in
                self.searchResultList = models
            }, onError: {error in }, onCompleted: {
                if self.searchResultList.count > 0 {
                    self.searchView?.reloadTableView()
                } else {
                    self.searchView?.reloadTableView()
                    self.searchView?.showEmptyFrequentMealsLabel()
                }
            }, onDisposed: {}))
        }
        
        if index == 1{
            self.loadFavouriteFoods(completion: {finished in
                if finished {
                    self.searchView?.reloadTableView()
                }
            })
        }
        if index == 2 {
            self.searchView?.showCameraSnap()
        }
        if index == 3{
            self.searchView?.showVoiceSearch()
            EventLogger.logClickedOnVoiceIcon()
        }
    }
}

//
//  Json_to_OptifastCheckVerModel.swift
//  Optifast
//
//  Created by Gena Mironchyk on 2/28/18.
//  Copyright © 2018 SSIC. All rights reserved.
//

import Foundation
class Json_to_OptifastCheckVerModel {
    
    func transform(dict : [String : Any]) -> OptifastCheckVerModel {
        let model = OptifastCheckVerModel()
        model.messageText = dict["MessageText"] as? String ?? ""
        model.messageTitle = dict["MessageTitle"] as? String ?? ""
        model.buttonText = dict["ButtonText"] as? String ?? ""
        model.detailsUrl = dict["DetailsURL"] as? String ?? ""
        model.version = dict["CurrentVersion"] as? String ?? ""
        return model
    }
    
}

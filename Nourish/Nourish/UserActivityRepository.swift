//
//  UserActivityRepository.swift
//  Nourish
//
//  Created by Nova on 11/30/16.
//  Copyright © 2016 SSIC. All rights reserved.
//

import UIKit
import HealthKit
import RxSwift

typealias HKCompletionHandleActivities = ([HKSample]?, NSError?) -> ()


final class UserActivityRepository: StatBaseInteractor {
    
    struct hwParameters {
        var height : Double = 0.0
        var weight : Double = 0.0
    }
    
    var dailyCachedSteps : [HKQuantitySample] = []
    
    
    let mapperToUserActivity = HKSample_to_UserActivity()
    
    func getCurrentSteps() {
        ActivityUtility.getHealthKitPermission(completion: {granted, error in
            if granted == true {
                ActivityUtility.recentSteps2(completion: {steps, error in
                    LogUtility.logToFile("steps is \(steps)")
                })
            }
        })
    }
    
    func getHealthKitPermissions(completion : @escaping (Bool) -> ()) {
        ActivityUtility.getHealthKitPermission(completion: { granted, error in
            if error == nil {
                completion(granted)
            } else {
                completion(false)
            }
        })
    }
    
    func getUserActivitiesForToday(completion : @escaping ([HKSample]) -> ()) {
        ActivityUtility.getHealthKitPermission(completion: {granted, error in
            if granted == true {
                ActivityUtility.recentActivities(completion: { activities, error in
                    completion(activities)
                })
            }
        })
    }
    
    func getSleepingActivities(completion : @escaping ([HKSample]?) -> ()) {
        ActivityUtility.getHealthKitPermission(completion: {granted, error in
            if granted == true {
                ActivityUtility.sleepingActivityLastNight(completion: { activities, error in
                    completion(activities)
                })
            }
        })
    }
    
    func receiveCurrentUserActivities() -> Observable<[UserActivity]> {
        return Observable.create { observer in
        var array : [HKSample] = []
        
        self.getUserActivitiesForToday(completion: { activities in
            array += activities
            self.getSleepingActivities(completion: { sleepingActivities in
                if (sleepingActivities?.count)! > 0 {
                    array += sleepingActivities!
                }
                
                array = array.sorted(by: { $0.startDate < $1.startDate })
                let userActivities = self.mapperToUserActivity.transform(array: array)
                observer.onNext(userActivities)
                observer.onCompleted()
            })
        })
        return Disposables.create()
        }
    }
    
    func getUserStepsForToday() -> Observable<Double> {
        return Observable.create { observer in
            ActivityUtility.getHealthKitPermission(completion: { granted, errors in
                if  errors != nil {
                    observer.onError(errors!)
                }
                if granted == true {
                    ActivityUtility.aggregatedUserStepsForToday(completion: {steps in
                        observer.onNext(steps)
                        observer.onCompleted()
                    })
                }
            })
           return Disposables.create()
        }
    }
    
    func getStepsStatisticsFor(date : Date) -> Observable<[Double]> {
        return Observable.create { observer in
            
            let startDate = self.calendar.startOfDay(for: date)
            var comps = DateComponents()
            comps.day = 1
            comps.second = -1
            let endDate = self.calendar.date(byAdding: comps, to: startDate)
            
            ActivityUtility.getStepStatistic(startDate: startDate, endDate: endDate!, period: .today, completion: { collectionStats, error in
                if error == nil {
                    let array : [Double] = []
                    if collectionStats != nil {
                        let array = self.statisticsToArray(collection: collectionStats!)
                        observer.onNext(array)
                        observer.onCompleted()
                    } else {
                        observer.onNext(array)
                        observer.onCompleted()
                    }
                    
                } else {
                    observer.onError(error!)
                }
                
            })
            return Disposables.create()
        }
    }
    
    private func statisticsToArray(collection : HKStatisticsCollection) -> [Double] {
        var array : [Double] = []
        let calendar = Calendar.current

        for _ in 0...23 {
            array.append(0)
        }
        
        for stat in collection.statistics() {
            let hourIndex = calendar.component(.hour, from: stat.startDate)
            let value = stat.sumQuantity()?.doubleValue(for: HKUnit.count())
            if value != nil && value! > 0.0 {
                array[hourIndex] = value!
            }
            
        }
        
        return array
    }
    
    
    func getStepsLast10Minutes() -> Observable<Double> {
        return Observable.create { observer in
            ActivityUtility.recent10MinutesSteps(completion: { steps, error in
                observer.onNext(steps)
                observer.onCompleted()
            })
            return Disposables.create()
        }
    }
    
    func getUserHeightsLast3MonthsFrom(date : Date) -> Observable<Double?> {
        return Observable.create { observer in
            var comps = DateComponents()
            comps.month = -3
            let startDate = self.calendar.date(byAdding: comps, to: date)
            ActivityUtility.getHeightsFor(startDate: startDate!, endDate: date, completion: {samples, error in
                if error != nil {
                    observer.onError(error!)
                } else {
                    if samples.count > 0 {
                        let sampleToParse = samples.last
                        let height = sampleToParse as! HKQuantitySample
                        observer.onNext(height.quantity.doubleValue(for: HKUnit.meter()))
                    } else {
                        observer.onNext(nil)
                    }
                    observer.onCompleted()
                }
            })
            return Disposables.create()
        }
    }
    
    func getUserWeightLast3MonthsFrom(date : Date) -> Observable<Double?> {
        return Observable.create { observer in
            var comps = DateComponents()
            comps.month = -3
            let startDate = self.calendar.date(byAdding: comps, to: date)
            ActivityUtility.getWeightFor(startDate: startDate!, endDate: date, completion: {samples, error in
                if error != nil {
                    observer.onError(error!)
                } else {
                    if samples.count > 0 {
                        let sampleToParse = samples.last
                        let weight = sampleToParse as! HKQuantitySample
                        observer.onNext(weight.quantity.doubleValue(for: HKUnit.gramUnit(with: .kilo)))
                    } else {
                        observer.onNext(nil)
                    }
                    observer.onCompleted()
                }
            })
            return Disposables.create()
        }
        
        
    }
    
    
    func getUserWeight() -> Observable<Double?> {
        return Observable.create { observer in
            ActivityUtility.lastUserWeight(completion: {sample, error in
                if error != nil {
                    observer.onError(error!)
                } else {
                    if sample != nil {
                        let weight = sample as! HKQuantitySample
                        observer.onNext(weight.quantity.doubleValue(for: HKUnit.gramUnit(with: .kilo)))
                    } else {
                        observer.onNext(nil)
                    }
                    observer.onCompleted()
                }
            })
            return Disposables.create()
        }
    }
    
    func getUserHeight() -> Observable<Double?> {
        return Observable.create { observer in
            ActivityUtility.lastUserHeight(completion: {sample, error in
                if error != nil {
                    observer.onError(error!)
                } else {
                    if sample != nil {
                        let height = sample as! HKQuantitySample
                        observer.onNext(height.quantity.doubleValue(for: HKUnit.meter()))
                    } else {
                        observer.onNext(nil)
                    }
                    
                    observer.onCompleted()
                    
                }
               
            })
            return Disposables.create()
        }
    }
    
    func getUserCaloriesTotalBurned() -> Observable<Double> {
        return Observable.create { observer in
            var calories = 0.0
            ActivityUtility.activeEnergyBurned(completion: {activeCalories, error in
                calories += activeCalories
                ActivityUtility.basalEnergyBurned(completion: {basalCalories, error in
                    calories += basalCalories
                    observer.onNext(calories)
                    observer.onCompleted()
                })
            })
            return Disposables.create()
        }
    }
    
    func getUserHeartRateLast10Minutes() -> Observable<Double> {
        return Observable.create { observer in
            var heartRate = 0.0
            ActivityUtility.heartRate10minutes(completion: { sampleArray, error in
                let count = sampleArray.count
                if count > 0 {
                    for sample in sampleArray {
                        heartRate += sample.quantity.doubleValue(for: HKUnit(from:"count/min"))
                    }
                    let total = heartRate/Double(count)
                    observer.onNext(total)
                    observer.onCompleted()
                } else {
                    observer.onNext(0.0)
                    observer.onCompleted()
                }
                
            })
            return Disposables.create()
        }
    }
    
    func getUserHeartLastTwoDays() -> Observable<[HKQuantitySample]> {
        return Observable.create { observer in
            ActivityUtility.getHealthKitPermission(completion: {granted, error in
                if error != nil {
                    observer.onError(error!)
                }
                if granted == true {
                    ActivityUtility.heartRateLastTwoDays(completion: {heartRates, error in
                        if error != nil {
                            observer.onError(error!)
                        }
                        if heartRates.count > 0 {
                            observer.onNext(heartRates)
                            observer.onCompleted()
                        } else {
                            observer.onNext([])
                            observer.onCompleted()
                        }
                    })
                }
            })
            return Disposables.create()
        }
    }
    
    private func getBPM(startDate : Date, endDate : Date) -> Observable<[HKQuantitySample]> {
        return Observable.create { observer in
            ActivityUtility.getHealthKitPermission(completion: {granted, error in
                if error != nil {
                    observer.onError(error!)
                }
                if granted == true {
                    ActivityUtility.getHeartRateSamle(caching : false,startDate : startDate, endDate : endDate,completion: {bpms, error in
                        if error != nil {
                            observer.onError(error!)
                        }
                        if bpms.count > 0 {
                            observer.onNext(bpms)
                            observer.onCompleted()
                        } else {
                            observer.onNext([])
                            observer.onCompleted()
                        }
                    })
                }
            })
            return Disposables.create()
        }
    }
    
    func getUserHeartForToday() -> Observable<[HKQuantitySample]> {
        return Observable.create { observer in
            ActivityUtility.getHealthKitPermission(completion: {granted, error in
                if error != nil {
                    observer.onError(error!)
                }
                if granted == true {
                    ActivityUtility.heartRateForToday(completion: {heartRates, error in
                        if error != nil {
                            observer.onError(error!)
                        }
                        if heartRates.count > 0 {
                            observer.onNext(heartRates)
                            observer.onCompleted()
                        } else {
                            observer.onNext([])
                            observer.onCompleted()
                        }
                    })
                }
            })
            return Disposables.create()
        }
    }
    
    func getUserSteps(caching : Bool,startDate : Date, endDate : Date) -> Observable<[HKQuantitySample]> {
        return Observable.create { observer in
            ActivityUtility.getHealthKitPermission(completion: {granted, error in
                if error != nil {
                    observer.onError(error!)
                }
                if granted == true {
                    ActivityUtility.stepsFor(caching : caching,startDate : startDate, endDate : endDate,completion: {steps, error in
                        if error != nil {
                            observer.onError(error!)
                        }
                        if steps.count > 0 {
                            observer.onNext(steps as! [HKQuantitySample])
                            observer.onCompleted()
                        } else {
                            observer.onNext([])
                            observer.onCompleted()
                        }
                    })
                }
            })
            return Disposables.create()
        }

    }
    
    func getLastDaySleeping() -> Observable<Double> {
        return Observable.create { observer in
            ActivityUtility.getHealthKitPermission(completion: {granted, error in
                if granted == true {
                    ActivityUtility.sleepingActivityLastNight(completion: { activities, error in
                        if activities != nil {
                            var totalSeconds = 0.0
                            for activity in activities! {
                                
                                let category = activity as? HKCategorySample
                                if category?.value == HKCategoryValueSleepAnalysis.asleep.rawValue {
                                    totalSeconds = totalSeconds + activity.endDate.timeIntervalSince(activity.startDate)
                                }
                                
                            }
                            let hours = (totalSeconds / 3600)
                            observer.onNext(hours)
                            observer.onCompleted()
                        } else {
                            observer.onNext(0.0)
                            observer.onCompleted()
                        }
                        if error != nil {
                            observer.onError(error!)
                        }
                        
                    })
                } else {
                    if error != nil {
                        observer.onError(error!)
                    }
                }
            })
            return Disposables.create()
        }
    }
    
    func getSleepingFor(date : Date) -> Observable<Double> {
        return Observable.create { observer in
            ActivityUtility.getHealthKitPermission(completion: {granted, error in
                if granted == true {
                    let startDate = self.calendar.startOfDay(for: date)
                    var comps = DateComponents()
                    comps.day = 1
                    comps.second = -1
                    let endDate = self.calendar.date(byAdding: comps, to: startDate)

                    ActivityUtility.sleepingActivityFor(startDate: startDate, endDate: endDate!,completion: { activities, error in
                        if activities != nil {
                            var totalSeconds = 0.0
                            for activity in activities! {
                                let category = activity as? HKCategorySample
                                if category?.value == HKCategoryValueSleepAnalysis.asleep.rawValue {
                                    totalSeconds = totalSeconds + activity.endDate.timeIntervalSince(activity.startDate)
                                }
                            }
                            let hours = (totalSeconds / 3600)
                            observer.onNext(hours)
                            observer.onCompleted()
                        } else {
                            observer.onNext(0.0)
                            observer.onCompleted()
                        }
                        if error != nil {
                            observer.onError(error!)
                        }
                        
                    })
                } else {
                    if error != nil {
                        observer.onError(error!)
                    }
                }
            })
            return Disposables.create()
        }
    }
    
    func getSleepingFor(period : period) -> Observable<Double> {
        return Observable.create { observer in
            ActivityUtility.getHealthKitPermission(completion: {granted, error in
                if granted == true {
                    ActivityUtility.sleepingActivityFor(startDate: self.startDateFor(periods: period, endDate: Date()), endDate: Date(),completion: { activities, error in
                        if activities != nil {
                            var totalSeconds = 0.0
                            for activity in activities! {
                                let category = activity as? HKCategorySample
                                if category?.value == HKCategoryValueSleepAnalysis.asleep.rawValue {
                                    totalSeconds = totalSeconds + activity.endDate.timeIntervalSince(activity.startDate)
                                }
                            }
                            let hours = (totalSeconds / 3600)
                            observer.onNext(hours)
                            observer.onCompleted()
                        } else {
                            observer.onNext(0.0)
                            observer.onCompleted()
                        }
                        if error != nil {
                            observer.onError(error!)
                        }
                        
                    })
                } else {
                    if error != nil {
                        observer.onError(error!)
                    }
                }
            })
            return Disposables.create()
        }
    }
    
    func startDateFor(periods : period, endDate : Date) -> Date {
//        let calendar = Calendar.current
//        switch period {
//        case .today:
//            return calendar.startOfDay(for: endDate)
//        case .weekly:
//            var startDate = calendar.date(byAdding: Calendar.Component.weekdayOrdinal, value: -1, to: endDate)!
//            startDate = calendar.startOfDay(for: startDate)
//            return startDate
//        case .monthly:
//            var startDate = calendar.date(byAdding: Calendar.Component.month, value: -1, to: endDate)!
//            startDate = calendar.startOfDay(for: startDate)
//            return startDate
//        }
        return period.startDateFor(period: periods)
    }
    
    func getWeightDataFor(period : period) -> Observable<[HKSample]> {
        return Observable.create {observer in
            
            let endDate = Date()
            let startDate = self.startDateFor(periods: period, endDate: endDate)
            
            ActivityUtility.getWeightFor(startDate: startDate, endDate: endDate, completion: {samples, error in
                if error == nil {
                    observer.onNext(samples)
                    observer.onCompleted()
                } else {
                    observer.onError(error!)
                }
            })
            
            return Disposables.create()
        }
    }
    
    func getActviveCaloriesFor(date : Date) -> Observable<[HKSample]> {
        return Observable.create { observer in
            let startDate = self.calendar.startOfDay(for: date)
            var comps = DateComponents()
            comps.day = 1
            comps.second = -1
            let endDate = self.calendar.date(byAdding: comps, to: startDate)
            
            ActivityUtility.getCaloriesFor(startDate: startDate, endDate: endDate!, completion: {samples, error in
                if error == nil {
                    observer.onNext(samples)
                    observer.onCompleted()
                } else {
                    observer.onError(error!)
                }
            })
            return Disposables.create()
        }
    }
    
    func getActiveCaloriesFor(period : period) -> Observable<[HKSample]> {
        return Observable.create {observer in
            let endDate = Date()
            let startDate = self.startDateFor(periods: period, endDate: endDate)
            LogUtility.logToFile("calorie\(startDate)")
            ActivityUtility.getCaloriesFor(startDate: startDate, endDate: endDate, completion: {samples, error in
                if error == nil {
                    observer.onNext(samples)
                    observer.onCompleted()
                } else {
                    observer.onError(error!)
                }
            })
            return Disposables.create()
        }
    }
    
    func getCalories(startDate : Date, endDate : Date) -> Observable<[HKSample]> {
        return Observable.create { observer in
            ActivityUtility.getCaloriesFor(startDate: startDate, endDate: endDate, completion: {samples, error in
                if error != nil {
                    observer.onError(error!)
                } else {
                    observer.onNext(samples)
                }
                observer.onCompleted()
            })
            return Disposables.create()
        }
    }
    
    
    func getBasalCaloriesFor(period : period) -> Observable<[HKSample]> {
        return Observable.create {observer in
            let endDate = Date()
            let startDate = self.startDateFor(periods: period, endDate: endDate)
            ActivityUtility.getBasalCaloriesFor(startDate: startDate, endDate: endDate, completion: {samples, error in
                if error == nil {
                    observer.onNext(samples)
                    observer.onCompleted()
                } else {
                    observer.onError(error!)
                }
            })
            return Disposables.create()
        }
    }
    
    func getDistanceFor(date : Date) -> Observable<Double> {
        let startDate = self.calendar.startOfDay(for: date)
        var comps = DateComponents()
        comps.day = 1
        comps.second = -1
        let endDate = self.calendar.date(byAdding: comps, to: startDate)
        return self.getDistance(startDate: startDate, endDate: endDate!)
    }
    
    private func getDistance(startDate : Date, endDate : Date) -> Observable<Double> {
        return Observable.create { observer in
            ActivityUtility.getDistanceSamples(startDate: startDate, endDate: endDate, completion: {samples, error in
                var distanceMeters = 0.0
                if error != nil {
                    observer.onError(error!)
                } else {
                    for sample in samples {
                        let quantityValue = sample as! HKQuantitySample
                        distanceMeters = distanceMeters + quantityValue.quantity.doubleValue(for: HKUnit.meter()) / 1000
                    }
                    observer.onNext(distanceMeters)
                    observer.onCompleted()
                }
            })
            return Disposables.create()
        }
    }
    
    func getWorkoutsTimeFor(date : Date) -> Observable<Double> {
        return Observable.create {observer in
            let startDate = self.calendar.startOfDay(for: date)
            var comps = DateComponents()
            comps.day = 1
            comps.second = -1
            let endDate = self.calendar.date(byAdding: comps, to: startDate)
            ActivityUtility.getWorkoutSamples(startDate: startDate, endDate: endDate!, completion: {workouts, error in
                if error != nil {
                    observer.onError(error!)
                } else {
                    if workouts.count > 0 {
                        var summaryInterval = 0.0
                        for work in workouts {
                            summaryInterval += work.endDate.timeIntervalSince(work.startDate)
                        }
                        let hours = summaryInterval / 3600
                        observer.onNext(hours)
                    } else {
                        observer.onNext(0.0)
                    }
                    observer.onCompleted()
                }
            })
            return Disposables.create()
        }
        
    }
    
    private func getWorkoutsFor(period : period) -> Observable<[HKWorkout]> {
        return Observable.create { observer in
            let startDate = self.startDateFor(periods: period, endDate: Date())
            ActivityUtility.getWorkoutSamples(startDate: startDate, endDate: Date(), completion: {workouts, error in
                if error != nil {
                    observer.onError(error!)
                } else {
                    observer.onNext(workouts)
                    observer.onCompleted()
                }
            })
            return Disposables.create()
        }
    }
    
    private func activeTimeFrom(array : [HKWorkout]) -> Double {
        var totalTime = 0.0
        
        for workout in array {
            totalTime += workout.duration
        }
        
        return totalTime
    }
    
    private func separateWorkoutsIntoDict(array : [HKWorkout]) -> [String : [HKWorkout]] {
        var proxyDict : [String : [HKWorkout]] = [:]
        for workout in array {
            let stringRep = self.stringFromEnum(type: workout.workoutActivityType)
            if proxyDict[stringRep] != nil {
                var arr = proxyDict[stringRep]
                arr?.append(workout)
                proxyDict[stringRep] = arr
            } else {
                proxyDict[stringRep] = [workout]
            }
        }
        return proxyDict
    }
    
    private func getActivityModelForToday() -> Observable<activityModel> {
        let startDate = self.startDateFor(periods: .today, endDate: Date())
        
        let calories = self.getActiveCaloriesFor(period: .today)
        let steps = self.getUserStepsForToday()
        let sleeping = self.getLastDaySleeping()
        let distance = self.getDistance(startDate: startDate, endDate: Date())
        let workouts = self.getWorkoutsFor(period: .today)
        let bmr = self.bmrForFullDay()
        
        let zip = Observable.zip(calories, steps, sleeping, distance,workouts,bmr, resultSelector : {cal, step, sleep, dist,works, bmrValue -> activityModel in
            
            let model = activityModel()
            model.distance = dist
            model.sleepHours = sleep
            model.activeHours = self.activeTimeFrom(array: works) / 60
            
            let calorieDict = self.separateWorkoutsIntoCalories(arr: works)
            model.activityList = self.activityListFrom(dict: calorieDict, unit: "cal")
            
            let doubleHours = self.hoursSinceMidnight(date: Date())
            
            var calorieArray : [caloriesModel] = []
            
            for i in 0...Int(doubleHours) {
                calorieArray.append(caloriesModel(calorie : bmrValue/24*doubleHours, caloriePeriodRange : String(format : "%lu",i)))
            }
            
            let finalCalorieArray = self.caloriesModelIncludingActivities(array: calorieArray, activities: cal)
            model.caloriesArray = finalCalorieArray
            
            model.steps = step
            model.avgCalorie = self.camulativeCalorie(arr: finalCalorieArray) / doubleHours
            
            model.avgSteps = model.steps / doubleHours
            
            return model
        })
        return zip
    }
    /*
    private func getActivityModelWeeklyMonthly(period : period) -> Observable<activityModel> {
        let startDate = self.startDateFor(periods: period, endDate: Date())
        print("steps\(startDate)")
        let steps = self.getUserSteps(startDate: startDate, endDate: Date())
        let distance = self.getDistance(startDate: startDate, endDate: Date())
        let workouts = self.getWorkoutsFor(period: period)
        let sleeping = self.getSleepingFor(period: period)
        let calories = self.getActiveCaloriesFor(period: period)
        let bmr = self.bmrForFullDay()
        
        let zip = Observable.zip(calories, steps, sleeping, distance,workouts,bmr, resultSelector : { cal, step, sleep, dist,works, bmrValue -> activityModel in
            
            let model = activityModel()
            
            model.steps = self.camulativeSteps(arr: step)
            model.distance = dist
            model.sleepHours = sleep
            model.activeHours = self.activeTimeFrom(array: works) / 60
            
            let durationDict = self.separateWorkoutsForHours(arr: works)
            model.activityList = self.activityListFrom(dict: durationDict, unit: "hr")
            
            var dict = self.separateSamples(samples: cal, period: period)
            dict = self.fillWithEmpties(dict: dict, period: period)
            var sample = self.calorieStatsSample(dict: dict, period: period)
            
            sample = self.addBmrFor(samples: sample, bmr: bmrValue, period : period)
            
            let calorieArray = self.calorieArrayFromSample(samples: sample, period: period)
            
            model.caloriesArray = calorieArray
            
            var stepsDict = self.separateSamples(samples: step, period: period)
            stepsDict = self.fillWithEmpties(dict: stepsDict, period: period)
            model.activityArray = self.stepsModelFromDict(dict: stepsDict, period: period)
            
            model.avgCalorie = self.camulativeCalorie(arr: model.caloriesArray) / Double(model.caloriesArray.count)
            model.avgSteps = self.camulativeSteps(arr: step) / Double(model.activityArray.count)
            
            return model
        })
        return zip
    }
    */
    private func getActivityModelWeeklyMonthly(period : period) -> Observable<activityModel> {
        let startDate = self.startDateFor(periods: period, endDate: Date())
        let distance = self.getDistance(startDate: startDate, endDate: Date())
        let workouts = self.getWorkoutsFor(period: period)
        let sleeping = self.getSleepingFor(period: period)
        let bmr = self.bmrForFullDay()
        let steps = self.getStepsStatisticFor(period: period)
        let calories = self.getActiveCaloriesFor(period: period)
        
        let zip = Observable.zip(calories, steps, sleeping, distance, workouts, bmr, resultSelector : {cal, step, sleep, dist, works, bmrValue -> activityModel in
            let model = activityModel()
            
            var stepsSum = 0.0
            var avgSteps = 0.0
            var count = 0
            
            for stat in step.statistics() {
                stepsSum += (stat.sumQuantity()?.doubleValue(for: HKUnit.count()))!
                count = count + 1
            }
            
            if count == 0 {
                count = 1
            }
            
            avgSteps = stepsSum / Double(count)
            model.steps = stepsSum
            model.distance = dist
            model.sleepHours = sleep
            model.activeHours = self.activeTimeFrom(array: works) / 60
            
            let durationDict = self.separateWorkoutsForHours(arr: works)
            model.activityList = self.activityListFrom(dict: durationDict, unit: "hr")
            
            var dict = self.separateSamples(samples: cal, period: period)
            dict = self.fillWithEmpties(dict: dict, period: period)
            var sample = self.calorieStatsSample(dict: dict, period: period)
            
            sample = self.addBmrFor(samples: sample, bmr: bmrValue, period : period)
            
            let calorieArray = self.calorieArrayFromSample(samples: sample, period: period)
            
            model.caloriesArray = calorieArray
            
            var activityArray : [activityTime] = []
            
            for stat in step.statistics() {
                var sample = activityTime()
                sample.activeHr = (stat.sumQuantity()?.doubleValue(for: HKUnit.count()))!
                sample.periodString = self.stringFrom(start: stat.startDate, end: stat.endDate, period: period)
                activityArray.append(sample)
            }
            /*
            var stepsDict = self.separateSamples(samples: step, period: period)
            stepsDict = self.fillWithEmpties(dict: stepsDict, period: period)
            model.activityArray = self.stepsModelFromDict(dict: stepsDict, period: period)
            */
            model.activityArray = activityArray
            model.avgCalorie = self.camulativeCalorie(arr: model.caloriesArray) / Double(model.caloriesArray.count)
            model.avgSteps = avgSteps//self.camulativeSteps(arr: step) / Double(model.activityArray.count)
            
            return model

        
        })
        
        return zip
    }
    private func addBmrFor(samples : [CalorieStatsSample], bmr : Double, period : period) -> [CalorieStatsSample] {
        var array : [CalorieStatsSample] = []
        for sample in samples {
            switch period {
            case .weekly:
                if sample != samples.last {
                    sample.calories += bmr
                } else {
                    sample.calories += bmr / 24.0 * self.hoursSinceMidnight(date: Date())
                }
            case .monthly:
                if sample != samples.last {
                    sample.calories += bmr * 7
                } else {
                    sample.calories += bmr / 24.0 * self.hoursSinceStartOfWeek(date: Date())
                    LogUtility.logToFile(self.hoursSinceStartOfWeek(date: Date()))
                }
            case .today: break
                
            }
            array.append(sample)
        }
        return array
    }
    
    private func stepsModelFromDict(dict : [Date : [HKSample]], period : period) -> [activityTime] {
        var array : [activityTime] = []
        
        for key in dict.keys {
            var sample = activityTime()
            let startDate = key
            let endDate = self.endDateFrom(period: period, startDate: key)
            var summary = 0.0
            
            let stepsArray = dict[key]
            
            if (stepsArray?.count)! > 0 {
                for sampleValue in stepsArray! {
                    let value = sampleValue as! HKQuantitySample
                    summary = summary + value.quantity.doubleValue(for: HKUnit.count())
                }
            }
            
            sample.activeHr = summary
            sample.periodString = self.stringFrom(start: startDate, end: endDate, period: period)
            array.append(sample)
        }
        return array
    }
    
    private func calorieArrayFromSample(samples : [CalorieStatsSample], period : period) -> [caloriesModel] {
        var arrayToReturn : [caloriesModel] = []
        
        for sample in samples {
            let timeString = self.stringFrom(start: sample.startDate, end: sample.endDate, period: period)
            let model = caloriesModel(calorie : sample.calories, caloriePeriodRange : timeString)
            arrayToReturn.append(model)
        }
        
        if period == .weekly {
            if arrayToReturn.count > 7 {
                if calendar.startOfDay(for: (samples.last?.startDate)!) == calendar.startOfDay(for: Date()) {
                    arrayToReturn.remove(at: 0)
                } else {
                    arrayToReturn.removeLast()
                }
                
            }
        }
        return arrayToReturn
    }
    
    private func getStepsStatisticFor(period : period) -> Observable<HKStatisticsCollection> {
        return Observable.create { observer in
            let startDate = self.startDateFor(periods: period, endDate: Date())
            ActivityUtility.getStepStatistic(startDate: startDate, endDate: Date(), period: period, completion: {collectionValues, error in
                
                if error != nil {
                    observer.onError(error!)
                } else {
                    if collectionValues != nil {
                        observer.onNext(collectionValues!)
                    }
                    
                    observer.onCompleted()
                }
            })
            return Disposables.create()
        }
       
    }
    
    private func stringFrom(start : Date, end : Date, period : period) -> String {
        let dateformatter = DateFormatter()
        dateformatter.calendar = Calendar.current
        dateformatter.dateFormat = "dd/MM"
        
        var stringRep = ""
        switch period {
        case .today:
            stringRep = dateformatter.string(from: start)
        case .weekly:
            let startDate = Calendar.current.startOfDay(for: start)
            let myCalendar = NSCalendar(calendarIdentifier: NSCalendar.Identifier.gregorian)!
            let myComponents = myCalendar.components(.weekday, from: startDate)
            let weekDay = myComponents.weekday
            let weekdays = [
                "Su",
                "Mo",
                "Tu",
                "We",
                "Th",
                "Fr",
                "Sa"
            ]
            stringRep = weekdays[weekDay! - 1]
        case .monthly:
            stringRep = "\(dateformatter.string(from: start)) - \(dateformatter.string(from: end))"
        }
        return stringRep
    }
    
    private func caloriesModelIncludingActivities(array : [caloriesModel], activities : [HKSample]) -> [caloriesModel] {
        var caloriesToReturn = array
        
        for activity in activities {
            let sample = activity as! HKQuantitySample
            let indexHour = self.fullHoursSinceMidnight(date: activity.endDate)
            
            let valueToAdd = sample.quantity.doubleValue(for: HKUnit.kilocalorie())
            
            var value = caloriesToReturn[indexHour].calorie
            
            value = value + valueToAdd
            caloriesToReturn[indexHour].calorie = value
        }
        return caloriesToReturn
    }
    
    private func hoursSinceMidnight(date : Date) -> Double {
        let units = Set<Calendar.Component>([.hour, .minute])
        let components = Calendar.current.dateComponents(units, from: date)
        let minutes = 60 * components.hour! + components.minute!
        return Double(minutes) / 60.0
    }
    
    private func fullHoursSinceMidnight(date : Date) -> Int {
        let units = Set<Calendar.Component>([.hour, .minute])
        let components = Calendar.current.dateComponents(units, from: date)
        let returnValue = components.hour != nil ? components.hour! : 0
        return returnValue
    }
    
    private func hoursSinceStartOfWeek(date : Date) -> Double {
        let startOfWeek = Calendar.current.date(from: Calendar.current.dateComponents([.yearForWeekOfYear, .weekOfYear], from: date))!
        let timeInterval = date.timeIntervalSince(startOfWeek)
        return timeInterval / 3600
    }
    
    private func activityListFrom(dict : [String : Double], unit : String) ->  [(activityName : String , value: Double,type:String)] {
        var returnValue : [(activityName : String , value: Double,type:String)] = []
        
        for (name, valueDict) in dict {
            returnValue.append(activityName: name, value : valueDict, type : unit)
        }
        
        return returnValue
    }
    
    private func separateWorkoutsIntoCalories(arr : [HKWorkout]) -> [String : Double] {
        let dict = self.separateWorkoutsIntoDict(array: arr)
        var dictToReturn : [String : Double] = [:]
        
        for (name, works) in dict {
            var value = 0.0
            for work in works {
                if work.totalEnergyBurned != nil {
                    value = value + (work.totalEnergyBurned?.doubleValue(for: HKUnit.kilocalorie()))!
                }
            }
            dictToReturn[name] = value
        }
        
        return dictToReturn
    }
    
    private func separateWorkoutsForHours(arr : [HKWorkout]) -> [String : Double] {
        let dict = self.separateWorkoutsIntoDict(array: arr)
        var dictToReturn : [String : Double] = [:]
        
        for (name, works) in dict {
            var value = 0.0
            for work in works {
                value = value + work.duration
            }
            dictToReturn[name] = value / 3600
        }
        
        return dictToReturn
    }
    
    func getActivityModelFor(period : period) -> Observable<activityModel> {
        if period != .today {
            return self.getActivityModelWeeklyMonthly(period: period)
        } else {
            return self.getActivityModelForToday()
        }
    }
    
    
    private func bmrForFullDay() -> Observable<Double> {
        let user = UserRepository.shared.getCurrentUser(policy: .DefaultPolicy).flatMap { userProfile -> Observable<Double> in
            
            let bmrForDay = userProfile.getBMRCalories(weight: 0.0, height: 0.0, date: Date(), isCurrentDay: false)
            
            return Observable.just(bmrForDay)
        }
        
        return user
    }
    
    
    private func camulativeSteps(arr : [HKQuantitySample]) -> Double {
        var amount = 0.0
        for object in arr {
            amount = amount + object.quantity.doubleValue(for: HKUnit.count())
            //print(object.quantity.doubleValue(for: HKUnit.count()),amount)
        }
        return amount
    }
    
    private func camulativeCalorie(arr : [caloriesModel]) -> Double {
        var amount = 0.0
        for model in arr {
            amount = amount + model.calorie
        }
        return amount
    }
    
    private func stringFromEnum(type : HKWorkoutActivityType) -> String {
        
        switch type {
        case .americanFootball:
            return "American Football"
        case .archery:
            return "Archery"
        case .australianFootball:
            return "Australian Football"
        case .badminton:
            return "Badminton"
        case .baseball:
            return "Baseball"
        case .basketball:
            return "Basketball"
        case .bowling:
            return "Bowling"
        case .boxing:
            return "Boxing"
        case .climbing:
            return "Climbing"
        case .cricket:
            return "Cricket"
        case .crossTraining:
            return "Cross Training"
        case .curling:
            return "Curling"
        case .cycling:
            return "Cycling"
        case .dance:
            return "Dance"
        case .danceInspiredTraining:
            return "Dance Inspired Training"
        case .elliptical:
            return "Elliptical"
        case .equestrianSports:
            return "Equestrian Sports"
        case .fencing:
            return "Fencing"
        case .fishing:
            return "Fishing"
        case .functionalStrengthTraining:
            return "Functional Strength Training"
        case .golf:
            return "Golf"
        case .gymnastics:
            return "Gymnastics"
        case .handball:
            return "Handball"
        case .hiking:
            return "Hiking"
        case .hockey:
            return "Hockey"
        case .hunting:
            return "Hunting"
        case .lacrosse:
            return "Lacrosse"
        case .martialArts:
            return "MartialArts"
        case .mindAndBody:
            return "Mind And Body"
        case .mixedMetabolicCardioTraining:
            return "Mixed Metabolic Cardio Training"
        case .mixedCardio:
            return "Mixed Cardio"
        case .paddleSports:
            return "Paddle Sports"
        case .play:
            return "Play"
        case .preparationAndRecovery:
            return "Preparation And Recovery"
        case .racquetball:
            return "Racquetball"
        case .rowing:
            return "Rowing"
        case .rugby:
            return "Rugby"
        case .running:
            return "Running"
        case .sailing:
            return "Sailing"
        case .skatingSports:
            return "Skating Sports"
        case .snowSports:
            return "Snow Sports"
        case .soccer:
            return "Soccer"
        case .softball:
            return "Softball"
        case .squash:
            return "Squash"
        case .stairClimbing:
            return "Stair Climbing"
        case .surfingSports:
            return "Surfing Sports"
        case .swimming:
            return "Swimming"
        case .tableTennis:
            return "TableTennis"
        case .tennis:
            return "Tennis"
        case .trackAndField:
            return "Track And Field"
        case .traditionalStrengthTraining:
            return "Traditional Strength Training"
        case .volleyball:
            return "Volleyball"
        case .walking:
            return "Walking"
        case .waterFitness:
            return "Water Fitness"
        case .waterPolo:
            return "Water Polo"
        case .waterSports:
            return "Water Sports"
        case .wrestling:
            return "Wrestling"
        case .yoga:
            return "Yoga"
        case .other:
            return "Other"
        case .barre:
            return "Barre"
        case .coreTraining:
            return "Core Training"
        case .crossCountrySkiing:
            return "Cross Country Skiing"
        case .downhillSkiing:
            return "Downhill Skiing"
        case .flexibility:
            return "Flexibility"
        case .highIntensityIntervalTraining:
            return "HighIntensity Interval Training"
        case .jumpRope:
            return "Jump Rope"
        case .kickboxing:
            return "Kickboxing"
        case .pilates:
            return "Pilates"
        case .snowboarding:
            return "Snowboarding"
        case .stairs:
            return "Stairs"
        case .stepTraining:
            return "Step Training"
        case .wheelchairWalkPace:
            return "Wheelchair Walk Pace"
        case .wheelchairRunPace:
            return "Wheelchair Run Pace"
        default :
            return "Training"
        }
    }
    /*
    private func formatString(string : String) -> String {
        var regexp : NSRegularExpression?
        do {
            try regexp = NSRegularExpression(pattern: "([a-z])([A-Z])", options: NSRegularExpression.Options(rawValue: 0))
        } catch  {
            regexp = nil
        }
        var finalString = ""
        if regexp != nil {
            finalString = regexp!.stringByReplacingMatches(in: string, options: NSRegularExpression.MatchingOptions(rawValue: 0), range: NSMakeRange(0, string.characters.count), withTemplate: "$1 $2")
        }
        print(finalString)
        return finalString.capitalized
    }
    */
    // Stress
    
    private func stressSampleForToday() -> Observable<[HKQuantitySample]> {
        return Observable.create {observer in
            ActivityUtility.heartRateForToday(completion: {heartRates, error in
                if error != nil {
                    observer.onError(error!)
                }
                if heartRates.count > 0 {
                    observer.onNext(heartRates)
                    observer.onCompleted()
                } else {
                    observer.onNext([])
                    observer.onCompleted()
                }
            })
            return Disposables.create()
        }
    }
    
    private func restingHeartRateForToday() -> Observable<Double> {
        let calendar = Calendar.current
        let yesterday = calendar.date(byAdding: .day, value: -1, to: Date())
        let start = calendar.startOfDay(for: yesterday!)
        
        let bpm = self.getBPM(startDate: start, endDate: Date())
        let steps = self.getUserSteps(caching : false,startDate: start, endDate: Date())
        
        let zip = Observable.zip(bpm, steps, resultSelector: {heartRates, step -> Double in
            var count = 0.0
            var value = 0.0
            
            self.dailyCachedSteps = step
            
            var previousSampleDate : Date? = nil
            for sample in heartRates {
                if self.isHighLoadValue(sample: sample, steps: step, lastSampleDate: previousSampleDate) == false {
                    value = value + sample.quantity.doubleValue(for: HKUnit(from: "count/min"))
                    count = count + 1
                }
                previousSampleDate = sample.endDate
            }
            var restingHeartRate = 0.0
            
            if value > 0 && count > 0 {
                restingHeartRate = value / count
            }
            
            return restingHeartRate
        })
        return zip
    }
    
    private func stressLevelFrom(sample : HKQuantitySample, obj : DailyStress, bpms : [HKQuantitySample], steps : [HKQuantitySample], prevSampleDate : Date?) -> Observable<DailyStress> {
        
        
        
        let firstSample = sample
        
        let stressStore = StressCacheDataStore.shared
        let stressObject = stressStore.fetchFor(id: firstSample.uuid.uuidString)
        if stressObject != nil {
            let objToReturn = DailyStress()
            objToReturn.bpmValue = stressObject?.bpmValue
            objToReturn.dateToShow = stressObject?.timeStamp
            if stressObject?.stressLevel != nil {
                objToReturn.stressValue = stressLevel.enumFromString(stringValue: (stressObject?.stressLevel)!)
            }
            
            return Observable.just(objToReturn)
        } else {
            let object = obj
            var count = 0.0
            var value = 0.0
            var stressToReturn : stressLevel = .Undetermined
            var previousSampleDate : Date? = nil
            
            for sample in bpms {
                if self.isHighLoadValue(sample: sample, steps: steps, lastSampleDate: previousSampleDate) == false {
                    value = value + sample.quantity.doubleValue(for: HKUnit(from: "count/min"))
                    count = count + 1
                }
                previousSampleDate = sample.endDate
            }
            var restingHeartRate = 0.0
            
            if value > 0 && count > 0 {
                restingHeartRate = value / count
            }
            if stressToReturn == .Undetermined {
                if restingHeartRate != 0.0 {
                    if (firstSample.quantity.doubleValue(for: HKUnit(from: "count/min"))) > (restingHeartRate + kBPMTreshold) {
                        if self.isHighLoadValue(sample: firstSample, steps: steps, lastSampleDate: prevSampleDate) == false {
                    
                            stressToReturn = .Stressed
                        } else {
                            stressToReturn = .Normal
                        }
                    } else {
                        stressToReturn = .Normal
                    }
                } else {
                    stressToReturn = .Normal
                }
            }
            object.bpmValue = firstSample.quantity.doubleValue(for: HKUnit(from: "count/min"))
            object.stressValue = stressToReturn
            self.cacheDailyStress(object: object, id: firstSample.uuid.uuidString)
            return Observable.just(object)
        }
    }
    
    private func cacheDailyStress(object : DailyStress, id : String) {
        let stressStore = StressCacheDataStore.shared
        let cacheObject = StressCacheRealm()
        cacheObject.stressLevel = stressLevel.stringFromEnum(level: object.stressValue!)
        cacheObject.bpmValue = object.bpmValue!
        cacheObject.idString = id
        cacheObject.timeStamp = object.dateToShow
        stressStore.store(object: cacheObject)
    }

    private func isHighLoadValue(sample : HKQuantitySample, steps : [HKQuantitySample], lastSampleDate : Date?) -> Bool {
        var previousDate : Date? = nil
        
        if lastSampleDate != nil {
            let interval = sample.startDate.timeIntervalSince(lastSampleDate!)
            if interval > 600 {
                previousDate = sample.startDate.addingTimeInterval(-600)
            } else {
                previousDate = lastSampleDate
            }
        } else {
            previousDate = sample.startDate.addingTimeInterval(-600)
        }
        
        let finalInterval = sample.startDate.timeIntervalSince(previousDate!)
        
        var sampleArray : [HKQuantitySample] = []
        
        for step in steps {
            if step.startDate >= previousDate! && step.startDate <= sample.endDate {
                sampleArray.append(step)
            } else if step.endDate >= previousDate! && step.endDate <= sample.endDate {
                sampleArray.append(step)
            } else if step.startDate <= previousDate! && step.endDate >= sample.endDate {
                sampleArray.append(step)
            }
        }
        
        var value = 0.0
        
        for object in sampleArray {
            value += object.quantity.doubleValue(for: HKUnit.count())
        }
        
        let correctedKoef = finalInterval / 60 * 10
        
        if value >= correctedKoef {
            return true
        } else {
            return false
        }
    }
    
    func fetchStressForToday() -> Observable<[DailyStress]> {
            return self.stressSampleForToday().flatMap {samples -> Observable<[DailyStress]> in
                
                let firstSample = samples.first
                
                if firstSample != nil {
                    let endDate = firstSample!.startDate
                    let startDate = endDate.addingTimeInterval((-60*60*24))
                    
                    let step = self.getUserSteps(caching : false,startDate: startDate, endDate: endDate)
                    let bpm = self.getBPM(startDate: startDate, endDate: endDate)
                    
                    var obsArray : [Observable<DailyStress>] = []
                    
                    let zip = Observable.zip(step, bpm, resultSelector : {steps, bpms -> Observable<[DailyStress]> in

                        for sample in samples {
                            let proxyValue = sample.quantity.doubleValue(for: HKUnit(from: "count/min"))
                            
                            let obj = DailyStress()
                            let hkSampleValue = sample as HKSample
                            
                            obj.bpmValue = proxyValue
                            obj.dateToShow = hkSampleValue.startDate
                            
                            let filteredSteps = self.filterSamples(samples: steps, date: hkSampleValue.startDate)
                            let filteredBPM = self.filterSamples(samples: bpms, date: hkSampleValue.startDate)
                            obsArray.append(self.stressLevelFrom(sample: sample, obj: obj, bpms: filteredBPM, steps: filteredSteps,prevSampleDate: nil))
                        }
                        let singleObs = Observable.from(obsArray).merge()
                        let sequence = singleObs.toArray()
                        
                        return sequence
                    })
                    return zip.merge()
                } else {
                    return Observable.just([])
                }
        }
    }
    
    private func filterSamples(samples : [HKQuantitySample], date : Date) -> [HKQuantitySample] {
        let sampleArray = samples as [HKSample]
        
        let array = sampleArray.filter {$0.startDate > date.addingTimeInterval(-60*60*24) && $0.startDate < date}
        
        return array as! [HKQuantitySample]
        
    }
    
    private func weeklyStressFrom(collection : HKStatisticsCollection) -> Observable<[WeeklyStress]> {
            //var array : [WeeklyStress] = []
        var obs : [Observable<WeeklyStress>] = []
        
        for value in collection.statistics() {
            let week = self.weeklyModelFrom(value: value)
            obs.append(week)
        }
        
        if obs.count == 0 {
            return Observable.just([])
        }
        
        let merge = Observable.zip(obs, {allValues -> [WeeklyStress] in
            var arrayWeek : [WeeklyStress] = []
            for value in allValues {
                arrayWeek.append(value)
            }
            return arrayWeek.reversed()
        })
        return merge
    }
    
    private func weeklyModelFrom(value : HKStatistics) -> Observable<WeeklyStress> {
        let model = WeeklyStress()
        model.avgBPM = value.averageQuantity()?.doubleValue(for: HKUnit(from:"count/min"))
        model.minBPM = value.minimumQuantity()?.doubleValue(for: HKUnit(from:"count/min"))
        model.maxBPM = value.maximumQuantity()?.doubleValue(for: HKUnit(from:"count/min"))
        
        var startDate = calendar.startOfDay(for: value.startDate)
        startDate = startDate.addingTimeInterval((-60*60*24))
        let endDate = calendar.startOfDay(for: value.endDate)
        
        let bpm = self.getHeartRate(caching : true,startDate: startDate, endDate: endDate)
        let steps = self.getUserSteps(caching : true,startDate: startDate, endDate: endDate)
        
        let zip = Observable.zip(bpm, steps, resultSelector : {heartRates, stepsValue -> Observable<WeeklyStress> in
            let weekLyStructArray = self.weeklyStructFrom(bpm: heartRates, steps: stepsValue, dateToShow: value.startDate).flatMap { models -> Observable<[weeklyStruct]> in
                let result = models.sorted(by: {$0.dateToShow < $1.dateToShow})
                return Observable.just(self.fillWeeklyModelsWithEmpties(models: result))
            }.flatMap { array -> Observable<WeeklyStress> in
                model.weeklyArray = array
                return Observable.just(model)
            }
            return weekLyStructArray
        })
        return zip.merge()
    }
    
    private func fillWeeklyModelsWithEmpties(models : [weeklyStruct]) -> [weeklyStruct] {
        if models.count == 7 {
            return models.sorted(by: {$0.dateToShow < $1.dateToShow})
        } else {
            let firstStruct = models.first
            let startOfWeek = calendar.date(from: calendar.dateComponents([.yearForWeekOfYear, .weekOfYear], from: (firstStruct?.dateToShow)!))!
            var comps = DateComponents()
            
            var hoursArray : [hoursStruct] = []
            for index in 0...23 {
                hoursArray.append(hoursStruct(index : index, stressValue : .Undetermined))
            }
            
            var weeklyArrayEmpty : [weeklyStruct] = []
            
            for index in 0...6 {
                comps.day = index
                var date = calendar.date(byAdding: comps, to: startOfWeek)
                date = calendar.startOfDay(for: date!)
                weeklyArrayEmpty.append(weeklyStruct(dateToShow : date!, hoursArray : hoursArray))
            }
            
            //merge
            var dates : [Date] = []
            
            for model in models {
                dates.append(calendar.startOfDay(for: model.dateToShow))
            }
            
            
            var modelsFinished = models
            
            for emptyRecord in weeklyArrayEmpty {
                if !dates.contains(calendar.startOfDay(for: emptyRecord.dateToShow)) {
                     modelsFinished.append(emptyRecord)
                }
               
            }
         return modelsFinished.sorted(by: { $0.dateToShow < $1.dateToShow })
        }
        
    }
    
    func fetchStressWeekly() -> Observable<[WeeklyStress]> {
        return Observable.create { observer in
            ActivityUtility.getHeartRateStatisticsForLastTwoMonths(caching : true, completion: {statisticCollection, error in
                if error != nil {
                    observer.onError(error!)
                }
                
                if statisticCollection != nil {
                    let weeklyArray = self.weeklyStressFrom(collection: statisticCollection!)
                    
                    let _ = weeklyArray.subscribe(onNext: {weeklyStress in
                        observer.onNext(weeklyStress)
                        observer.onCompleted()
                    }, onError: {error in }, onCompleted: {
                        observer.onCompleted()
                    }, onDisposed: {
                    })
                } else {
                    observer.onCompleted()
                }
                
            })
            return Disposables.create {
                
            }
        }
    }
    
    private func getHeartRate(caching : Bool, startDate : Date, endDate : Date) -> Observable<[HKSample]> {
        return Observable.create { observer in
            ActivityUtility.getHeartRateSamle(caching : caching, startDate: startDate, endDate: endDate, completion: { samples, error in
                if error != nil {
                    observer.onError(error!)
                } else {
                    observer.onNext(samples)
                    observer.onCompleted()
                }
            })
            return Disposables.create()
        }
        
    }
    
    private func separateSamplesWithDays(samples : [HKSample]) -> [Date : [HKSample]] {
        
        var proxyDict : [Date : [HKSample]] = [:]
        
        for sample in samples {
            let key = self.calendar.startOfDay(for: sample.startDate)
            if proxyDict[key] != nil {
                var proxyArray = proxyDict[key]
                proxyArray?.append(sample)
                proxyDict[key] = proxyArray
            } else {
                var array : [HKSample] = []
                array.append(sample)
                proxyDict[key] = array
            }
        }
        
        return proxyDict
        
    }
    
    private func sampleToHourIndexes(samples : [DailyStress]) -> [Int] {
        var indexes : [Int] = []
        for sample in samples {
            indexes.append(self.hourIndexFor(sample: sample))
        }
        return indexes
    }
    
    
    private func hourIndexFor(sample : DailyStress) -> Int {
        let date = sample.dateToShow
        let midnight = calendar.startOfDay(for: date!)
        let difference = calendar.dateComponents([.hour, .minute], from: midnight, to: date!)
        let fullHours = difference.hour
        return fullHours!
    }
    
    private func stressModelFrom(normalIndexes : [Int], stressedIndexes : [Int], date : Date) -> weeklyStruct {
        let fullHours = 23
        var stressedArray : [hoursStruct] = []
        for index in 0...fullHours {
            var stressLevel : hoursStruct = hoursStruct(index : index, stressValue : .Undetermined)
            
            if normalIndexes.contains(index) == true {
                if stressedIndexes.contains(index) {
                    stressLevel.stressValue = .Stressed
                } else {
                    stressLevel.stressValue = .Normal
                }
            } else if stressedIndexes.contains(index) {
                stressLevel.stressValue = .Stressed
            }
            
            stressedArray.append(stressLevel)
        }
        let struc = weeklyStruct(dateToShow : date, hoursArray : stressedArray)
        return struc
    }
    
    func firstActivityDate() -> Observable<Date?> {
        return Observable.create { observer in
            ActivityUtility.fetchFirstStepDate(completion: {date, error in
                if error != nil {
                    observer.onError(error!)
                } else {
                    observer.onNext(date)
                    observer.onCompleted()
                }
            })
            return Disposables.create()
        }
    }
    
    private func weeklyStructFrom(bpm : [HKSample], steps : [HKQuantitySample], dateToShow : Date) -> Observable<[weeklyStruct]> {
        let dict = self.separateSamplesWithDays(samples: bpm)
        
        var weeklyStructs : [Observable<weeklyStruct>] = []
        
        let datesArray = dict.keys.sorted()
        for date in datesArray {
            if date >= dateToShow {
                var stressedSamples : [DailyStress] = []
                var normalSamples : [DailyStress] = []
                
                var obsArray : [Observable<DailyStress>] = []
                
                for todaySample in dict[date]! as! [HKQuantitySample] {
                    let obj = DailyStress()
                    obj.dateToShow = todaySample.startDate
                    
                    let bpmFiltered = self.filterSamples(samples: bpm as! [HKQuantitySample], date: todaySample.startDate)
                    let stepsFilters = self.filterSamples(samples: steps, date: todaySample.startDate)
                    
                    obsArray.append(self.stressLevelFrom(sample: todaySample, obj: obj, bpms: bpmFiltered, steps: stepsFilters, prevSampleDate: nil))
                    
                }
                let singleObs = Observable.from(obsArray).merge()
                let sequence = singleObs.toArray().flatMap { stresses -> Observable<weeklyStruct> in
                    for object in stresses {
                        if object.stressValue == .Stressed {
                            stressedSamples.append(object)
                        } else {
                            normalSamples.append(object)
                        }
                    }
                    
                    let normalIndexes = self.sampleToHourIndexes(samples: normalSamples)
                    let stressedIndexes = self.sampleToHourIndexes(samples: stressedSamples)
                    let model = self.stressModelFrom(normalIndexes: normalIndexes, stressedIndexes: stressedIndexes, date: date)
                    
                    return Observable.just(model)
                }
                
                
                weeklyStructs.append(sequence)
            }
            
        }
        let single = Observable.from(weeklyStructs).merge()
        let seq = single.toArray()
        return seq
    }
    
    func fetchLastCacheSample() -> Observable<DailyStress> {
        let stressDataStore = StressCacheDataStore()
        let obj = stressDataStore.lastCachedValue().flatMap { stress -> Observable<DailyStress> in
            let stressObj = DailyStress()
            stressObj.stressValue = stressLevel.enumFromString(stringValue: stress.stressLevel!)
            stressObj.bpmValue = stress.bpmValue
            stressObj.dateToShow = stress.timeStamp!
            return Observable.just(stressObj)
        }
        return obj
    }
}

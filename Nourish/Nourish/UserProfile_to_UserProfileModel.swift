//
//  UserProfile_to_UserProfileModel.swift
//  Nourish
//
//  Created by Nova on 11/15/16.
//  Copyright © 2016 SSIC. All rights reserved.
//

import UIKit

class UserProfile_to_UserProfileModel: NSObject {

    func transform(user : UserProfile) -> UserProfileModel {
        let userModel = UserProfileModel()
        userModel.userID = user.userID
        userModel.userFirstName = user.userFirstName
        userModel.userLastName = user.userLastName
        userModel.activeSinceTimeStamp = user.activeSinceTimeStamp
        userModel.activityLevel = user.activityLevel
        userModel.height = user.height
        userModel.weight = user.weight
        userModel.biologicalSex = user.biologicalSex
        userModel.dietaryPreference = user.dietaryPreference
        userModel.metricPreference = user.metricPreference
        userModel.goalPrefsFood = user.goalPrefsFood
        userModel.goalPrefsLifestyle = user.goalPrefsLifestyle
        userModel.goalPrefsActivity = user.goalPrefsActivity
        userModel.userGoal = user.userGoal
        userModel.age = user.age
        userModel.weightLossStartDate = user.weightLossStartDate
        userModel.startWeight = user.startWeight
        userModel.goalValue = user.goalValue
        userModel.email = user.email
        userModel.weightToMaintain = user.weightToMaintain
        userModel.needToInfromUser = user.needToInformUser
        userModel.optifastOcasionsList = user.optifastOcasionsList
        userModel.optifastFoodIds = user.optifastFoodIds
        return userModel
    }
}

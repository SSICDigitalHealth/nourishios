//
//  ActivityBarChart.swift
//  Nourish
//
//  Created by Andrey Zmushko on 9/5/17.
//  Copyright © 2017 SSIC. All rights reserved.
//

import UIKit

protocol ActivityBarChartDelegate {
    func numberOfBars(in activityBarChart: ActivityBarChart) -> Int
    func activityBarChart(_ activityBarChart: ActivityBarChart, barAtIndex: Int) -> ActivityBarChart.Bar?
    
    func topLabel(in activityBarChart: ActivityBarChart) -> String
    func bottomLabel(in activityBarChart: ActivityBarChart) -> String
    func topAxisValue(in activityBarChart: ActivityBarChart) -> CGFloat
}


class ActivityBarChart: UIView {
    
    typealias Bar = (burned: (lower: CGFloat, higher: CGFloat), consumed: (lower: CGFloat, higher: CGFloat), legenda: String)
    
    let leftGap: CGFloat = 56.0
    
    let topLabel = UILabel()
    let bottomLabel = UILabel()
    var bars: [Bar] = []
    var legenda: [UILabel] = []
    let labelColor = NRColorUtility.hexStringToUIColor(hex: "#747474")
    let labelFont = UIFont.systemFont(ofSize: 12.0)
    let legendaFont = UIFont.systemFont(ofSize: UIScreen.main.bounds.width > 320 ? 11.0 : 9.0)
    var topAxisValue = CGFloat()
    
    private func positionLabel(label: UILabel, y: CGFloat) {
        label.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint(item: label,
                           attribute: .left,
                           relatedBy: .equal,
                           toItem: self,
                           attribute: .left,
                           multiplier: 1.0,
                           constant: 0.0).isActive = true
        NSLayoutConstraint(item: label,
                           attribute: .bottom,
                           relatedBy: .equal,
                           toItem: self,
                           attribute: .top,
                           multiplier: 1.0,
                           constant: y).isActive = true
    }
    
    private func positionLegenda(label: UILabel, x: CGFloat) {
        label.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint(item: label,
                           attribute: .top,
                           relatedBy: .equal,
                           toItem: self,
                           attribute: .top,
                           multiplier: 1.0,
                           constant: 75.0).isActive = true
        NSLayoutConstraint(item: label,
                           attribute: .centerX,
                           relatedBy: .equal,
                           toItem: self,
                           attribute: .left,
                           multiplier: 1.0,
                           constant: x).isActive = true
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.addSubview(topLabel)
        self.addSubview(bottomLabel)
        self.topLabel.textColor = self.labelColor
        self.topLabel.font = self.labelFont
        self.bottomLabel.textColor = self.labelColor
        self.bottomLabel.font = self.labelFont
        
       // self.positionLabel(label: self.topLabel, y: 25.0)
        self.positionLabel(label: self.bottomLabel, y: 75.0)
    }
    
    func reloadWith(DataSource delegate: ActivityBarChartDelegate) {
        self.topLabel.text = delegate.topLabel(in: self)
        self.bottomLabel.text = delegate.bottomLabel(in: self)
        self.bars.removeAll()
        for label in legenda {
            label.removeFromSuperview()
        }
        self.topLabel.removeFromSuperview()
        self.legenda.removeAll()
        for index in 0 ..< delegate.numberOfBars(in: self) {
            if let bar = delegate.activityBarChart(self, barAtIndex: index) {
                bars.append(bar)
                let label = UILabel()
                label.textColor = self.labelColor
                label.font = self.legendaFont
                self.addSubview(label)
                legenda.append(label)
            }
        }
        
        self.topAxisValue = 75.0 * (1.0 - delegate.topAxisValue(in: self))
        self.addSubview(self.topLabel)
        self.positionLabel(label: self.topLabel, y: self.topAxisValue)
        
        self.setNeedsDisplay()
    }
    
    override func draw(_ rect: CGRect) {
        if self.bars.count <= 0 {
            return
        }

        self.drawAxis(from: CGPoint(x: 0, y: 75.0),
                      till: CGPoint(x: self.bounds.width, y: 75.0),
                      color: NRColorUtility.hexStringToUIColor(hex: "#cccccc"),
                      dashed: false)
        self.drawAxis(from: CGPoint(x: 0, y: self.topAxisValue),
                      till: CGPoint(x: self.bounds.width, y: self.topAxisValue),
                      color: NRColorUtility.hexStringToUIColor(hex: "#cccccc"),
                      dashed: true)
        
        var width: (burned: CGFloat, gap: CGFloat, consumed: CGFloat) {
            if self.bars.count > 7 {
                return (4.0, 1.0, 1.0)
            }
            return (24.0, 2.0, 4.0)
        }
        
        var chartWidth: CGFloat {
            
            let total = self.bounds.width - leftGap
            let space = total / CGFloat(self.bars.count)
            
            return total + space - width.burned - width.gap - width.consumed - 1.0
        }
        
        func barX(_ index: Int) -> (burned: CGFloat, consumed: CGFloat) {
            let burned = leftGap + (chartWidth / CGFloat(self.bars.count)) * CGFloat(index)
            
            return (burned, burned + width.burned + width.gap)
        }
        
        func legendaX(_ index: Int) -> CGFloat {
            let burned = leftGap + (chartWidth / CGFloat(self.bars.count)) * CGFloat(index)
            
            return burned + (width.burned + width.gap + width.consumed) / 2
        }
        
        func height(value: CGFloat) -> CGFloat {
            return -75.0 * value
        }

        for (index, bar) in bars.enumerated() {
            let burnedRectLower = CGRect(x: barX(index).burned, y: 75.0, width: width.burned, height: height(value: bar.burned.lower))
            self.drawRect(rect: burnedRectLower, color: NRColorUtility.consumedCaloriesDailyWeeklyColor())
            
            let burnedHigher = bar.burned.higher > bar.burned.lower ? bar.burned.higher - bar.burned.lower : 0.0
            let burnedRectHigher = CGRect(x: barX(index).burned, y: 75.0 + height(value: bar.burned.lower), width: width.burned, height: height(value: burnedHigher))
            self.drawRect(rect: burnedRectHigher, color: NRColorUtility.overConsumedCaloriesDailyWeeklyColor())
            
            let consumedRectLower = CGRect(x: barX(index).consumed, y: 75.0, width: width.consumed, height: height(value: bar.consumed.lower))
            self.drawRect(rect: consumedRectLower, color: NRColorUtility.hexStringToUIColor(hex: "#40ad75"))
            
            let consumedHigher = bar.consumed.higher > bar.consumed.lower ? bar.consumed.higher - bar.consumed.lower : 0.0
            let consumedRectHigher = CGRect(x: barX(index).consumed, y: 75 + height(value: bar.consumed.lower), width: width.consumed, height: height(value: consumedHigher))
            self.drawRect(rect: consumedRectHigher, color: NRColorUtility.hexStringToUIColor(hex: "#009247"))
            
            legenda[index].text = bar.legenda
            
            self.positionLegenda(label: legenda[index], x: legendaX(index))
        }
    }
    
    private func drawAxis(from: CGPoint, till: CGPoint, color: UIColor, dashed: Bool) {
        let path = UIBezierPath()
        path.move(to: from)
        path.addLine(to: till)
        path.lineWidth = 1.0
        if dashed == true {
            path.lineJoinStyle = .miter
            path.setLineDash([2.0, 1.0], count: 2, phase: 0)
        }
        color.setStroke()
        path.stroke()
    }

    private func drawRect(rect: CGRect, color: UIColor) {
        let path = UIBezierPath()
        path.lineWidth = rect.size.width
        color.setStroke()
        let x = rect.origin.x + rect.size.width / 2
        path.move(to: CGPoint(x: x,
                              y: rect.origin.y))
        path.addLine(to: CGPoint(x: x,
                                 y: rect.origin.y + rect.size.height))
        path.stroke()
    }

}

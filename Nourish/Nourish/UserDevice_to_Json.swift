//
//  UserDevice_to_Json.swift
//  Nourish
//
//  Created by Nova on 12/7/16.
//  Copyright © 2016 SSIC. All rights reserved.
//

import UIKit

class UserDevice_to_Json: NSObject {
    func transform(device : UserDevice) -> [String:Any] {
        var dictionary : Dictionary<String, Any> = [:]
        dictionary["uid"] = device.userID
        dictionary["dtid"] = device.dtidString
        dictionary["name"] = device.deviceName
        if device.isAuthorized == true {
            dictionary["cloudAuthorization"] = "AUTHORIZED"
        }
        return dictionary
    }
}

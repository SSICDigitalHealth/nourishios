//
//  ProgressCaloriesMealsView.swift
//  Nourish
//
//  Created by Vlad Birukov on 20.06.17.
//  Copyright © 2017 SSIC. All rights reserved.
//

import UIKit
import QuartzCore

protocol ProgressCaloriesMealsProtocol {
    func setupWith(breakfastPercent: Double, lunchPercent: Double, dinnerPercent: Double, snackDrinkPercent: Double)
    func drawArc(value:[Double], color: [UIColor])
}

class ProgressCaloriesMealsView: UIView, ProgressCaloriesMealsProtocol {
    
    let breakfastColor = NRColorUtility.hexStringToUIColor(hex: "#C02859")
    let lunchColor = NRColorUtility.hexStringToUIColor(hex: "#70C397").withAlphaComponent(0.4)
    let dinnerColor = NRColorUtility.hexStringToUIColor(hex: "#1172B9")
    let snackDrinkColor = NRColorUtility.hexStringToUIColor(hex: "#f16527").withAlphaComponent(0.2)
    let backgroundArcColor = NRColorUtility.hexStringToUIColor(hex: "#F2F2F2")
    
    var breakfastValue = 0.0
    var lunchValue = 0.0
    var dinnerValue = 0.0
    var snackDrinkValue = 0.0
    let arcWidth: CGFloat = 28

    
    
    func setupWith(breakfastPercent: Double, lunchPercent: Double, dinnerPercent: Double, snackDrinkPercent: Double) {
        self.breakfastValue = breakfastPercent
        self.lunchValue = lunchPercent
        self.dinnerValue = dinnerPercent
        self.snackDrinkValue = snackDrinkPercent
        
        self.setNeedsDisplay()
    }
    
    override func draw(_ rect: CGRect) {
        
        self.drawArc(value: [breakfastValue, lunchValue, dinnerValue, snackDrinkValue], color: [breakfastColor, lunchColor, dinnerColor, snackDrinkColor])
        self.layer.cornerRadius = bounds.width/2
        self.layer.masksToBounds = true
    }

    func drawArc(value:[Double], color: [UIColor]) {
        let center = CGPoint(x:bounds.width/2, y: bounds.height/2)
        let radius: CGFloat = bounds.width/2
        var startAngle = CGFloat(270).degreesToRadians
        var endAngle = CGFloat(0).degreesToRadians
        
        self.drawBackroundArc(center: center, radius: radius)
        
        for i in 0..<value.count {
            endAngle = startAngle + CGFloat(360 * (value[i]/100)).degreesToRadians
            let bacgroundArc = UIBezierPath(arcCenter: center, radius: radius, startAngle: startAngle, endAngle: endAngle, clockwise: true)
            
            bacgroundArc.lineWidth = 2*arcWidth
            color[i].setStroke()
            bacgroundArc.stroke()
            startAngle = endAngle
        }
    }
    
    private func drawBackroundArc(center: CGPoint, radius: CGFloat) {
        let bacgroundArc = UIBezierPath(arcCenter: center, radius: radius, startAngle: CGFloat(0.degreesToRadians), endAngle: CGFloat(360.degreesToRadians), clockwise: true)
        
        bacgroundArc.lineWidth = 2 * arcWidth
        backgroundArcColor.setStroke()
        bacgroundArc.stroke()
    }
    
}

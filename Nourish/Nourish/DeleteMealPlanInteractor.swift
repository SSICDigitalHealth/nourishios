//
//  DeleteMealPlanInteractor.swift
//  Nourish
//
//  Created by Gena Mironchyk on 9/13/17.
//  Copyright © 2017 SSIC. All rights reserved.
//

import UIKit
import RxSwift
class DeleteMealPlanInteractor: NSObject {
    private let mealPlanRepository = MealPlannerRepository()
    
    func execute() -> Observable<Bool> {
        return self.mealPlanRepository.deleteWeeklyPlan()
    }
}

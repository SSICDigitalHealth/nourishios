//
//  UserScoreViewController.swift
//  Nourish
//
//  Created by Vlad Birukov on 15.06.17.
//  Copyright © 2017 SSIC. All rights reserved.
//

import UIKit

class UserScoreViewController: BaseCardViewController {
    
    @IBOutlet weak var userActivityScore: UserActivityScoreView!
    @IBOutlet weak var userScoreBreakdown: UserScoreView!
    @IBOutlet weak var cardNavigationBar: NewCardNavigationBarView!
    @IBOutlet weak var userActivityScoreHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var userDetailHeight: NSLayoutConstraint!

    override func viewDidLoad() {
        super.viewDidLoad()        
        if let config = self.progressConfig {
            self.userScoreBreakdown.progressConfig = config
            self.userActivityScore.progressConfig = config
        
            self.baseViews = [cardNavigationBar, userActivityScore, userScoreBreakdown]
            
            for v in baseViews! {
                v.viewDidLoad()
            }
        
            userScoreBreakdown.settingView()
            userActivityScore.loadCardStyle(config.period)
            if config.period != .daily {
                userActivityScoreHeightConstraint.constant = 126.0
            } else {
                userDetailHeight.constant = 276

            }
            userActivityScore.renderView(config)
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        if let config = self.progressConfig {
            setUpNavigationBar(title: config.period.scoreCardTitle, style: .score, navigationBar: cardNavigationBar)
        }
        super.viewWillAppear(animated)
    }
}

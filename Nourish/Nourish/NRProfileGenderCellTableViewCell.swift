//
//  NRProfileGenderCellTableViewCell.swift
//  Nourish
//
//  Created by Vino (Vinodha) Sundaramoorthy on 9/1/17.
//  Copyright © 2017 SSIC. All rights reserved.
//

import UIKit

class NRProfileGenderCellTableViewCell: UITableViewCell {

    @IBOutlet weak var maleButton : UIButton!
    @IBOutlet weak var femaleButton : UIButton!
    var isMale : Bool = true

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func setupWithGender(sex:String) {
        DispatchQueue.main.async {
            if sex == "Male" {
                self.maleButton.imageView?.image = UIImage(named:"radio_on_green")
                self.femaleButton.imageView?.image = UIImage(named:"radio_off_green")
            } else {
                self.maleButton.imageView?.image = UIImage(named:"radio_off_green")
                self.femaleButton.imageView?.image = UIImage(named:"radio_on_green")
            }
        }
        
    }
    
}

//
//  ScoreFeedbackPresenter.swift
//  Nourish
//
//  Created by Vino (Vinodha) Sundaramoorthy on 4/18/17.
//  Copyright © 2017 SSIC. All rights reserved.
//

import UIKit
import RxSwift

class ScoreFeedbackPresenter: BasePresenter , UITableViewDelegate , UITableViewDataSource {
    
    var scoreModel : ScoreModel!
    let interactor = ScoreFeedbackInteractor()
    var scoreView : ScoreProtocol? = nil
    var date : Date = Date()
    
    // MARK : BasePresenter Protocol
    override func viewWillAppear(_ animated: Bool) {
        let _ = self.interactor.getScoreModel(date:self.date).observeOn(MainScheduler.instance).subscribe(onNext: { [weak self] model in
            if let instance = self {
                instance.scoreModel = model
            }
        }, onError: {error in }, onCompleted: { [weak self] in
            if let instance = self {
                instance.scoreView?.refreshScore(model: instance.scoreModel)
                instance.scoreView?.stopActivity()
            }
     
        }, onDisposed: {})
    }
    
    // MARK : UITableViewDataSource
    
    func numberOfSections( in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return scoreModel.topNutrients.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell : NutrientsTableViewCell = tableView.dequeueReusableCell(withIdentifier: "kNutrientInfo") as! NutrientsTableViewCell
        let data = self.scoreModel.topNutrients[indexPath.row]
        cell.setUpNutrientInfoWithModel(nut: data)
        cell.selectionStyle = .none
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 50
    }
}

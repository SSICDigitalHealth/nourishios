//
//  DailyReportViewController.swift
//  Nourish
//
//  Created by Vino (Vinodha) Sundaramoorthy on 1/26/17.
//  Copyright © 2017 SSIC. All rights reserved.
//

import UIKit

class DailyReportViewController: BasePresentationViewController {
    @IBOutlet weak var reportView : DailyReportView!
    var reportDate : Date!

    override func viewDidLoad() {
        super.viewDidLoad()
        reportView.reportDate = self.reportDate
        self.baseViews = [reportView]
        
        let formatter  = DateFormatter()
        formatter.dateStyle = .medium
        
        self.title = formatter.string(from: reportDate)
    }
}

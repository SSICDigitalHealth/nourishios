//
//  ChangeDietView.swift
//  Nourish
//
//  Created by Vlad Birukov on 22.08.17.
//  Copyright © 2017 SSIC. All rights reserved.
//

import UIKit

protocol ChangeDietProtocol : BaseViewProtocol{
    func reloadData()
    func registerTableViewCells()
}

class ChangeDietView: BaseView, ChangeDietProtocol {
    @IBOutlet var dietTableView: UITableView!
    @IBOutlet var presenter: ChangeDietPresener!
    @IBOutlet weak var updateButton : UIButton!
    
    override func viewDidLoad() {
        self.xibAutoLayouting(nibName: "ChangeDietView")
        self.updateButton.backgroundColor = NRColorUtility.appBackgroundColor()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.basePresenter = presenter
        self.presenter.changeDietView = self
        self.registerTableViewCells()
        super.viewWillAppear(animated)
    }
    
    
    func reloadData() {
        self.dietTableView.reloadData()
    }
    
    func registerTableViewCells() {
        self.dietTableView?.register(UINib(nibName: "DietChangeTableViewCell", bundle: nil), forCellReuseIdentifier: "cellDietChange")
    }
    
}

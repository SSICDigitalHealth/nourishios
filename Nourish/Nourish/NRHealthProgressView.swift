//
//  NRHealthProgressView.swift
//  Nourish
//
//  Created by Vino (Vinodha) Sundaramoorthy on 12/1/16.
//  Copyright © 2016 SSIC. All rights reserved.
//

import UIKit

// MARK: Defaults
let defaultProgressColor : UIColor = NRColorUtility.defaultHealthProgressColor()
let defaultArcWidth : CGFloat = 12
let defaultRingGap : CGFloat = 25

class NRHealthProgressView: BaseView , NRHealthProgressProtocol {
    
    var presenter = NRHealthProgressPresenter()
    var controller = ProgressViewController()
    var calories : CGFloat = 0
    var steps : CGFloat  = 0
    var activity : CGFloat = 0
    var sleep : CGFloat = 0
    var bmr : CGFloat = 0
    var stress : stressLevel = .Undetermined
    var score : CGFloat = 0
    var ringOrder : [(label:healthProgressRings ,value:CGFloat)] = []
    var needReloaded = false
    var scoreLabel : UILabel!
    
    override func viewWillAppear(_ animated: Bool) {
        self.layer.shouldRasterize = true
        self.layer.rasterizationScale = UIScreen.main.scale
        self.basePresenter = presenter
        presenter.healthProgressView = self
        self.addShadow()
        super.viewWillAppear(animated)
    }

    override func draw(_ rect: CGRect) {
        if needReloaded == true {
            let context = UIGraphicsGetCurrentContext()
            context!.clear(rect)
            context!.setFillColor(UIColor.white.cgColor)
            context!.fill(rect)
            ringOrder = [(label:.Stress,value:0.0),(label:.Activity,value:activity),(label:.Sleep,value:sleep),(label:.Nutrition,value:score)]
            
            self.layer.sublayers?.forEach { $0.removeFromSuperlayer() }
            self.drawHealthScoreCircleWithScore(score:Double(self.score))
            self.drawPlaceholderRings(context: context!,label: ringOrder)
            self.drawRing(context: context!, ringOrder: ringOrder)
            self.drawTitle()
            self.drawBMR()
            self.drawCalories()
            self.drawSteps()
        }
    }
    
    func drawArc(context : CGContext, startAngle : CGFloat, endAngle : CGFloat, color : UIColor, value : CGFloat,radius: CGFloat) {
        let center = CGPoint(x:bounds.width/2, y: bounds.height/2 - 20)
        let arcWidth: CGFloat = defaultArcWidth

        let finalValue = (value * 270) / 100
        let realEndAngle : CGFloat = startAngle + finalValue.degreesToRadians
        let path = UIBezierPath(arcCenter: center,
                                radius: radius,
                                startAngle: startAngle,
                                endAngle: realEndAngle,
                                clockwise: true)
        path.lineWidth = arcWidth
        self.healthRingColorForAngle(angle:realEndAngle).setStroke()
        path.stroke()
    }
    
    func drawPlaceholderRings(context : CGContext,label:[(label:healthProgressRings ,value:CGFloat)]) {
        let center = CGPoint(x:bounds.width/2 , y: bounds.height/2 - 20)
        let arcWidth: CGFloat = defaultArcWidth
        let radius = max(self.bounds.width,self.bounds.height)/2
        var newRadius = radius - 50
       
        for i in 0...2 {
            var color = NRColorUtility.healthRingPlaceholderColor().withAlphaComponent(0.1)
            newRadius = newRadius - defaultRingGap
            let finalRadius = newRadius - arcWidth/2
            let path = UIBezierPath(arcCenter: center,
                                    radius: finalRadius,
                                    startAngle: CGFloat(45).degreesToRadians,
                                    endAngle: CGFloat(135).degreesToRadians,
                                    clockwise: true)
            path.lineWidth = arcWidth
            color.setStroke()
            path.stroke()
            
            //Add Label Text Layer
            let midPointAngle = (CGFloat(45).degreesToRadians + CGFloat(135).degreesToRadians) / 2.0
            let midPoint = CGPoint(x: center.x + finalRadius * cos(midPointAngle), y: center.y + finalRadius * sin(midPointAngle))
            
            let frame = CGRect(x: midPoint.x - 25, y: midPoint.y + 15, width:50, height: 50)
            let font = UIFont.systemFont(ofSize: 9)
            
            
            let scoreTextLayer = NRDrawingUtility.drawTextLayer(frame: frame, text: healthProgressRings.label(ring: ringOrder[i].label), color: UIColor.gray.cgColor, bgColor: UIColor.clear.cgColor, font: font)
            scoreTextLayer.position = CGPoint(x: midPoint.x, y: midPoint.y + 17)
            self.layer.addSublayer(scoreTextLayer)
            
            //Draw placeholder label rings
            color = NRColorUtility.healthRingPlaceholderColor().withAlphaComponent(0.3)
            let placeholderPath = UIBezierPath(arcCenter: center,
                                    radius: finalRadius,
                                    startAngle: CGFloat(135).degreesToRadians,
                                    endAngle: CGFloat(45).degreesToRadians,
                                    clockwise: true)
            placeholderPath.lineWidth = arcWidth
            color.setStroke()
            placeholderPath.stroke()
        }
    }

    func drawRing(context : CGContext, ringOrder:[(label:healthProgressRings ,value:CGFloat)]) {
        //Draw Original arc based on the value
        let center = CGPoint(x:bounds.width/2, y: bounds.height/2 - 20)
        let arcWidth: CGFloat = defaultArcWidth
        let radius = max(self.bounds.width,self.bounds.height)/2
        var newRadius = radius - 50
        
        for i in 0...2 {
            let value = ringOrder[i].value
            var color = self.healthRingColor(score: Double(value))
            newRadius = newRadius - defaultRingGap
            
            if (ringOrder[i].label == .Stress) {
                let path = UIBezierPath(arcCenter: center,
                                        radius: newRadius - arcWidth/2,
                                        startAngle: CGFloat(135).degreesToRadians,
                                        endAngle: CGFloat(45).degreesToRadians,
                                        clockwise: true)
                color = self.colorFrom(stress: self.stress)
                path.lineWidth = arcWidth
                color.setStroke()
                path.stroke()
            } else {
                self.drawArc(context: context, startAngle: CGFloat(135).degreesToRadians, endAngle: 0.0, color: color, value: value, radius: newRadius - arcWidth/2)
            }
        }
    }
    
    func colorFrom(stress : stressLevel) -> UIColor {
        switch stress {
        case .Normal:
            return NRColorUtility.healthGreenColor()
        case .Stressed:
             return NRColorUtility.healthRedColor()
        case .Undetermined:
             return NRColorUtility.healthRingPlaceholderColor().withAlphaComponent(0.1)
        }
    }
    
    func drawHealthScoreCircleWithScore(score :Double) {
        //Health Score Circle
        let center = CGPoint(x:bounds.width/2, y: bounds.height/2 - 20)
        let radius = max(self.bounds.width,self.bounds.height) / 2
        let newRadius = radius - 4 * 25

        let nutritionScorePath = UIBezierPath(arcCenter: center, radius: newRadius - 50, startAngle: 0, endAngle: CGFloat(360).degreesToRadians, clockwise: true)
        self.healthRingColor(score: score).setFill()
        nutritionScorePath.lineWidth = 1.0
        UIColor.clear.setStroke()
        nutritionScorePath.fill()
        
        //Draw score Text
        self.drawHealthScoreWithScore(value: score)
    }
    
    
    func drawHealthScoreWithScore(value : Double) {
        let frame = CGRect(x: self.bounds.origin.x, y: self.bounds.origin.y - 20, width: self.bounds.size.width, height: self.bounds.size.height)
        let font = UIFont.systemFont(ofSize: 44, weight: UIFontWeightMedium)
        
        if scoreLabel == nil {
            scoreLabel = UILabel(frame: frame)
            scoreLabel.textAlignment = .center
            scoreLabel.numberOfLines = 0
            let scoreAttribute = [NSForegroundColorAttributeName: UIColor.white , NSFontAttributeName : font]
            let scoreAttrString = NSMutableAttributedString(string: String(format:"%0.f",value), attributes: scoreAttribute)
            let labelString = "\nNutrition"
            let labelAttributes = [ NSForegroundColorAttributeName: UIColor.white, NSFontAttributeName : UIFont.systemFont(ofSize: 10)]
            scoreAttrString.append(NSAttributedString(string: labelString, attributes: labelAttributes))
            scoreLabel?.attributedText = scoreAttrString
            self.addSubview(scoreLabel)
        } else {
            self.scoreLabel.removeFromSuperview()
            scoreLabel = UILabel(frame: frame)
            scoreLabel.textAlignment = .center
            scoreLabel.numberOfLines = 0
            let scoreAttribute = [NSForegroundColorAttributeName: UIColor.white , NSFontAttributeName : font]
            let scoreAttrString = NSMutableAttributedString(string: String(format:"%0.f",value), attributes: scoreAttribute)
            let labelString = "\nNutrition"
            let labelAttributes = [ NSForegroundColorAttributeName: UIColor.white, NSFontAttributeName : UIFont.systemFont(ofSize: 10)]
            scoreAttrString.append(NSAttributedString(string: labelString, attributes: labelAttributes))
            scoreLabel?.attributedText = scoreAttrString
            self.addSubview(scoreLabel)
        }
    }
    
    func valueToRadians(value : CGFloat) -> CGFloat {
        return (2.0 * CGFloat(Double.pi) * value / 4)
    }
    
    // MARK: Ring color helper methods
    func healthRingColor(score : Double) -> UIColor {
        if score < 33 {
            return NRColorUtility.healthRedColor()
        } else if score > 33 && score < 66 {
            return NRColorUtility.healthOrangeColor()
        } else {
            return NRColorUtility.healthGreenColor()
        }
    }
    
    func healthRingColorForAngle(angle : CGFloat) -> UIColor {
        if angle.radiansToDegrees <= 225.0 {
            return NRColorUtility.healthRedColor()
        } else if angle.radiansToDegrees > 225.0 && angle.radiansToDegrees <= 310.0 {
            return NRColorUtility.healthOrangeColor()
        } else {
            return NRColorUtility.healthGreenColor()
        }
    }
    
    // MARK:NRHealthProgressProtocol Implementation
    func setupWith(progressModel : NRHealthProgressModel)
    {
        self.activity = CGFloat(progressModel.activity)
        self.stress = progressModel.stress
        self.sleep = CGFloat(progressModel.sleep)
        self.calories = CGFloat(progressModel.calories)
        self.steps = CGFloat(progressModel.steps)
        self.score = CGFloat(progressModel.nutritionScore)
        self.bmr = CGFloat(progressModel.bmr)
        self.needReloaded = true
        self.stopActivityAnimation()

        self.setNeedsDisplay()
    }
    
    func drawTitle() {
        let frame  = CGRect(x: 30, y: 10, width: 150, height: 22)
        let font = UIFont.systemFont(ofSize: 16)
        
        let title = NRDrawingUtility.drawTextLayer(frame: frame, text: "Health Progress", color: NRColorUtility.progressLabelColor().cgColor, bgColor: UIColor.clear.cgColor, font: font)
        title.alignmentMode = "left"
        self.layer.addSublayer(title)
    }
    
    
    func drawBMR() {
        //value text
        let myString = String(format: "%0.f",self.bmr)
        let valueFrame = CGRect(x:self.bounds.origin.x + 30 , y: self.bounds.height - 80 , width: 100, height: 50)
        let valueFont = UIFont.systemFont(ofSize: 17)
        let textLayer = NRDrawingUtility.drawTextLayer(frame: valueFrame, text: "", color: NRColorUtility.progressLabelColor().cgColor, bgColor: UIColor.clear.cgColor, font: valueFont)
        textLayer.alignmentMode = "left"
        textLayer.string = myString
        self.layer.addSublayer(textLayer)
        
        // image layer
        let imageFrame = CGRect(x: self.bounds.origin.x + 30, y: self.bounds.height - 50, width: 20, height: 22)
        let imageLayer = NRDrawingUtility.drawImageLayer(bounds: imageFrame, center: CGPoint(x:imageFrame.midX ,y:imageFrame.midY), bgColor: UIColor.clear.cgColor, imageName:"bmi_icon_sm")
        self.layer.addSublayer(imageLayer)
        
        let subTitleFrame = CGRect(x:imageFrame.origin.x + 25, y: self.bounds.height - 45, width: 40, height: 22)
        let subTitleTextLayer = NRDrawingUtility.drawTextLayer(frame: subTitleFrame, text: "", color: NRColorUtility.progressLabelColor().cgColor, bgColor: UIColor.clear.cgColor, font: UIFont.systemFont(ofSize: 14))
        subTitleTextLayer.alignmentMode = "center"
        subTitleTextLayer.string = "BMR"
        self.layer.addSublayer(subTitleTextLayer)
    }
    
    func drawCalories() {
        let myString = String(format: "%0.f",self.calories)
        //Draw Value text
        let valueFrame = CGRect(x:self.bounds.width - 80 , y: self.bounds.height - 80 , width: 100, height: 50)
        let valueFont = UIFont.systemFont(ofSize: 15)
        let textLayer = NRDrawingUtility.drawTextLayer(frame: valueFrame, text: "", color: NRColorUtility.progressLabelColor().cgColor, bgColor: UIColor.clear.cgColor, font: valueFont)
        textLayer.alignmentMode = "left"
        textLayer.string = myString
        self.layer.addSublayer(textLayer)
        
        // image layer
        let imageFrame = CGRect(x: self.bounds.width - 80, y: self.bounds.height - 50, width: 20, height: 22)
        let imageLayer = NRDrawingUtility.drawImageLayer(bounds: imageFrame, center: CGPoint(x:imageFrame.midX ,y:imageFrame.midY), bgColor: UIColor.clear.cgColor, imageName:"burn_icon_sm")
        self.layer.addSublayer(imageLayer)
        
        let subTitleFrame = CGRect(x: imageFrame.origin.x + 25, y: self.bounds.height - 45, width: 40, height: 22)
        let subTitleTextLayer = NRDrawingUtility.drawTextLayer(frame: subTitleFrame, text: "", color: NRColorUtility.progressLabelColor().cgColor, bgColor: UIColor.clear.cgColor, font: UIFont.systemFont(ofSize: 14))
        subTitleTextLayer.alignmentMode = "center"
        subTitleTextLayer.string = "Burn"
        self.layer.addSublayer(subTitleTextLayer)
    }
    
    func drawSteps() {
        let myString = String(format: "%0.f",self.steps)
        //Draw Value text
        let valueFrame = CGRect(x:self.bounds.width/2 , y: self.bounds.height - 80, width: 100, height: 50)
        let valueFont = UIFont.systemFont(ofSize: 17)
        let textLayer = NRDrawingUtility.drawTextLayer(frame: valueFrame, text: "", color: NRColorUtility.progressLabelColor().cgColor, bgColor: UIColor.clear.cgColor, font: valueFont)
        textLayer.alignmentMode = "left"
        textLayer.string = myString
        self.layer.addSublayer(textLayer)
        
        // image layer
        let imageFrame = CGRect(x: self.bounds.width/2 - 20, y: self.bounds.height - 50, width: 20, height: 22)
        let imageLayer = NRDrawingUtility.drawImageLayer(bounds: imageFrame, center: CGPoint(x:imageFrame.midX ,y:imageFrame.midY), bgColor: UIColor.clear.cgColor, imageName:"steps_icon")
        self.layer.addSublayer(imageLayer)
        
        let subTitleFrame = CGRect(x: imageFrame.origin.x + 25, y: self.bounds.height - 45, width: 40, height: 22)
        let subTitleTextLayer = NRDrawingUtility.drawTextLayer(frame: subTitleFrame, text: "", color: NRColorUtility.progressLabelColor().cgColor, bgColor: UIColor.clear.cgColor, font: UIFont.systemFont(ofSize: 14))
        subTitleTextLayer.alignmentMode = "center"
        subTitleTextLayer.string = "Steps"
        self.layer.addSublayer(subTitleTextLayer)

    }
}

//
//  StressModel_to_StressViewModel.swift
//  Nourish
//
//  Created by Vlad Birukov on 29.08.17.
//  Copyright © 2017 SSIC. All rights reserved.
//

import Foundation


class StressModel_to_StressViewModel {
    func transform(model: StressModel) -> StressViewModel {
        let stressViewModel = StressViewModel()
        
        if model.avgBPM != nil {
            stressViewModel.avgBPM = model.avgBPM
        }
        
        if model.maxBPM != nil {
            stressViewModel.maxBPM = model.maxBPM
        }
        
        if model.minBPM != nil {
            stressViewModel.minBPM = model.minBPM
        }
        
        if model.currentBPM != nil {
            stressViewModel.currentBPM = model.currentBPM
        }
        return stressViewModel
    }
}

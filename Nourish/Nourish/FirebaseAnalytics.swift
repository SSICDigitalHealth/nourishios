//
//  FirebaseAnalytics.swift
//  Nourish
//
//  Created by Gena Mironchyk on 5/11/17.
//  Copyright © 2017 SSIC. All rights reserved.
//

import UIKit
import Firebase

class FirebaseAnalytics: NSObject, AnalyticsProtocol {
    
    private var userDict = [String : Any]()
    
    func initializeWith(userMeta : [String : Any]) {
        FirebaseApp.configure()
        self.userDict = userMeta
    }
    
    func logEventWith(name: String) {
        Analytics.logEvent(name, parameters: self.userDict)
    }
    
}

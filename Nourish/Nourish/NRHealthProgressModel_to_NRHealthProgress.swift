//
//  NRHealthProgressModel_to_NRHealthProgress.swift
//  Nourish
//
//  Created by Vino (Vinodha) Sundaramoorthy on 12/1/16.
//  Copyright © 2016 SSIC. All rights reserved.
//

import UIKit

class NRHealthProgressModel_to_NRHealthProgress: NSObject {
    func transform(model : NRHealthProgressModel) -> NRHealthProgress {
        let object = NRHealthProgress()
        object.activity = model.activity
        object.stress = model.stress
        object.sleep = model.sleep
        object.calories = model.calories
        object.steps = model.steps
        return object
    }
    
    func transform(modelArray : [NRHealthProgressModel]) -> [NRHealthProgress] {
        var arrayToReturn : [NRHealthProgress] = []
        for model in modelArray {
            arrayToReturn.append(self.transform(model: model))
        }
        return arrayToReturn
    }
}

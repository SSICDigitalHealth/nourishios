//
//  OptifastSettingsRepository.swift
//  Nourish
//
//  Created by Gena Mironchyk on 10/18/17.
//  Copyright © 2017 SSIC. All rights reserved.
//

import UIKit
import RxSwift
class OptifastSettingsRepository: NSObject {
    private let userRepo = UserRepository.shared
    private let jsonToObjectMapper = json_to_OptifastFoodSearchModel()
    
    func getOptifastSettingsModel() -> Observable<OptifastSettingsModel> {
        return self.userRepo.getCurrentUser(policy: .DefaultPolicy).flatMap{userProfile -> Observable<OptifastSettingsModel> in
            let model = OptifastSettingsModel()
            model.optifastProductsTotal = self.totalOptifastFoodsFromLocalJSON()
            model.optifastUserProducts = self.foodSearchModelsFrom(foodSearchModelIDS: userProfile.optifastFoodIds, foodSearchModelsFromFNDDS: model.optifastProductsTotal)
            model.optifastUserOcasions = userProfile.optifastOcasionsList
            return Observable.just(model)
        }
    }
    
    
    
    private func totalOptifastFoodsFromLocalJSON() -> [foodSearchModel] {
        var foodSearchModels = [foodSearchModel]()
        let path = Bundle.main.url(forResource: "optifast", withExtension: "json")
        if path != nil {
            let jsonData = try?  Data(contentsOf: path!)
                if jsonData != nil {
                    if let jsonDict = self.convertDataToDictionary(data: jsonData!) {
                        foodSearchModels.append(contentsOf: self.jsonToObjectMapper.transform(jsons: jsonDict))
                    }
                }
            }
        return foodSearchModels
    }
    
    private func convertDataToDictionary(data: Data) -> [[String : Any]]? {
        do {
            return try JSONSerialization.jsonObject(with: data, options: []) as? [[String : Any]]
        } catch let error as NSError {
            LogUtility.logToFile(error)
        }
        return nil
    }
    
    
    private func foodSearchModelFrom(foodSearchModelID : String, foodSearchModelsFromFNDDS : [foodSearchModel]) -> foodSearchModel? {
        return foodSearchModelsFromFNDDS.first(where: {$0.foodId == foodSearchModelID})
    }
    
    private func foodSearchModelsFrom(foodSearchModelIDS : [String], foodSearchModelsFromFNDDS : [foodSearchModel]) -> [foodSearchModel]
    {
        var arrayToReturn = [foodSearchModel]()
        let _ = foodSearchModelIDS.map({
            let model = self.foodSearchModelFrom(foodSearchModelID: $0, foodSearchModelsFromFNDDS: foodSearchModelsFromFNDDS)
            if model != nil {
                arrayToReturn.append(model!)
            }
        })
        return arrayToReturn
    }
}

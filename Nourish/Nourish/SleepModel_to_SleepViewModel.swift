//
//  SleepModel_to_SleepViewModel.swift
//  Nourish
//
//  Created by Vlad Birukov on 29.08.17.
//  Copyright © 2017 SSIC. All rights reserved.
//

import Foundation

class SleepModel_to_SleepViewModel {
    func transform(model: SleepModel) -> SleepViewModel {
        let sleepViewModel = SleepViewModel()
        
        if model.sleepHours != nil {
            sleepViewModel.sleepHours = model.sleepHours
        }
        
        if model.avgSleepDay != nil {
            sleepViewModel.avgSleepDay = model.avgSleepDay
        }
        
        return sleepViewModel
    }
}

//
//  ChatBoxInterractor.swift
//  Nourish
//
//  Created by Nova on 2/6/17.
//  Copyright © 2017 SSIC. All rights reserved.
//

import UIKit
import RxSwift

class ChatBoxInterractor: NSObject {
    let repo = ChatBoxRepository()
    
    
    func storeRecordsArray(array : [ChatDataBaseItem]) {
        self.repo.storeRecordsArray(array:array)
    }
    
    func getAllUserMessagesCount(userID : String) -> Int {
        return self.repo.getAllUserMessagesCount(userID: userID)
    }
    func getChatHistoryFor(date : Date, userProfile : UserProfile) -> Observable<[ChatDataBaseItem]> {
        return self.repo.getChatHistoryFor(date : date, userProfile : userProfile)
    }
 
}

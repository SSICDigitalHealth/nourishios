//
//  NMFood_to_MealRecordRoutine.swift
//  Nourish
//
//  Created by Nova on 12/16/16.
//  Copyright © 2016 SSIC. All rights reserved.
//

import UIKit
//import FoodDatabaseKit
import RxSwift

class NMFood_to_MealRecordRoutine: NSObject {
    let noomRepo = NoomRepository.shared
    
   /* func noomFoodToMeal(noomFood : NMFood, model : foodSearchModel, occasion : Ocasion?, date : Date) -> MealRecord {
        let meal = MealRecord()
        meal.name = noomFood.name
        meal.idString = noomFood.uuid
        meal.barCode = noomFood.barcode
        meal.defaultFoodUnit = noomFood.preciseUnits?[0]
        //meal.foodUnits = noomFood.preciseUnits
        meal.msreDesc = meal.defaultFoodUnit?.name
        meal.date = date
        let unit = self.nmfoodUnitFrom(string: model.unit, noomFood: noomFood)
        
        meal.unit = unit.name
        let caloriesPerGram = noomFood.caloriesPerGram
        let caloriesPerMilliliter = noomFood.caloriesPerMilliliter
        let amount = NSNumber(value : model.amount)
        let calories = unit.calories(forAmount: amount)
        meal.nutrientsFromNoom = self.noomNutrienstsToNutrients(foodUnit: unit, amount: model.amount, calories : Double(calories!))
        
        meal.originalSearchString = model.originalSearchString
        
        var grams = 0.0
        
        if caloriesPerMilliliter != nil {
            grams = (calories?.doubleValue)! / (caloriesPerMilliliter?.doubleValue)!
        }
        
        if caloriesPerGram != nil {
            grams = (calories?.doubleValue)! / (caloriesPerGram?.doubleValue)!
        }
        
        meal.kCal = calories as? Double ?? 0.0
        meal.calories = calories as? Double ?? 0.0
        meal.portionKCal = unit.calories(forAmount: 1) as? Double ?? 0.0
        meal.caloriesPerGramm = (caloriesPerGram != nil) ? caloriesPerGram as! Double : 0.0
        meal.caloriesPerMilliliter = (caloriesPerMilliliter != nil) ? caloriesPerMilliliter as! Double : 0.0
        meal.grams = grams
        meal.amount = model.amount
        meal.msreAmount = model.amount
        meal.calories = calories as? Double ?? 0.0
        
        meal.groupID = model.groupID
        
        if occasion != nil {
            meal.ocasion = occasion!
        }
        
        return meal
        
    }*/
    
    func recomendationFoodToMeal(model : foodSearchModel, date : Date, occasion : Ocasion?) -> MealRecord {
        let meal = MealRecord()
        meal.amount = model.amount
        meal.calories = Double(model.calories)!
        meal.kCal = Double(model.calories)!
        meal.msreAmount = model.amount
        meal.msreDesc = model.unit
        
        if model.sourceFoodDBID != nil && model.sourceFoodDB != nil {
            meal.sourceFoodDBID = model.sourceFoodDBID!
            meal.sourceFoodDB = model.sourceFoodDB!
        } else if model.foodId.contains(String(format : "[%@]", kSoundHoundSourceDB)) {
            if let slicedString = model.foodId.slice(from: "[", to: "]") {
                meal.sourceFoodDB = slicedString
            }
            
            let dbID = model.foodId.components(separatedBy: String(format : "[%@] - ", meal.sourceFoodDB))[1]
            meal.sourceFoodDBID = dbID
        }
        
        
        
        
        if model.grams != nil {
            meal.grams = model.grams!
            meal.msreGrams = model.grams!
            let cpg = meal.kCal / meal.grams
            meal.caloriesPerGramm = cpg != Double.infinity ? cpg : 0.0
        }
        meal.idString = model.mealIdString
        meal.name = model.foodTitle
        meal.idString = model.foodId != "" ? model.foodId : model.mealIdString
        
        meal.isFromNoom = false
        if occasion != nil {
            meal.ocasion = occasion!
        } else {
            meal.ocasion = model.occasion ?? .breakfast
        }
        meal.name = model.foodTitle
        meal.groupID = model.groupID
        meal.userPhotoId = model.userPhotoId
        meal.unit = model.unit
        meal.date = date
        return meal
    }
    
   /* func nmfoodUnitFrom(string : String, noomFood : NMFood) -> NMFoodUnit {
        var nmUnit = NMFoodUnit()
        
        for unit in noomFood.preciseUnits! {
            if unit.name == string {
                nmUnit = unit
            }
        }
        
        if nmUnit.name.characters.count == 0 {
            nmUnit = (noomFood.preciseUnits?[0])!
        }
        
        return nmUnit
    }
    
    func noomNutrienstsToNutrients(foodUnit : NMFoodUnit, amount : Double, calories : Double) -> [Nutrient] {
        var array : [Nutrient] = []
        let allNutr : [NMFoodNutrient] = [NMFoodNutrient.totalFat, NMFoodNutrient.saturatedFat, NMFoodNutrient.transFat, NMFoodNutrient.cholesterol, NMFoodNutrient.sodium, NMFoodNutrient.potassium, NMFoodNutrient.carbohydrate, NMFoodNutrient.dietaryFiber, NMFoodNutrient.sugars, NMFoodNutrient.protein]
        
        for nutr in allNutr {
            let value = foodUnit.amountInMg(for: nutr, amount: NSNumber(value : Double(amount)))
            if value != nil && (value as! Double) > 0 {
                var mgram = (value as! Double)
                var unit = ""
                
                if mgram > 5000 {
                   unit = "g"
                   mgram = mgram / 1000
                } else if mgram < 5 {
                    unit = "μg"
                    mgram = mgram * 1000
                } else {
                    unit = "mg"
                }
                
                let nutrition = Nutrient()
                nutrition.name = stringFrom(nutr: nutr).capitalized
                nutrition.representationValue = mgram
                nutrition.clearValue = value as! Double
                nutrition.unit = unit
                array.append(nutrition)
                
                
            }
        }
        
        let cals = Nutrient()
        cals.name = calorieString
        cals.unit = kCalString
        cals.clearValue = calories
        cals.representationValue = calories
        array.insert(cals, at: 0)
        
        return array
    }
    
    func valueInMg(value : Double) -> Dictionary<String, Double> {
        var dict : [String : Double] = [:]
        if value > 5000 {
            dict["g"] = value / 1000
        } else if value < 5 {
            dict["μg"] = value * 1000
        } else {
            dict["mg"] = value
        }
        return dict
    }
    
    func foodUnitFrom(model : foodSearchModel) -> Observable<[NMFoodUnit]> {
        return Observable.create { observer in
                self.noomRepo.foodNoomFrom(uid: model.foodId, withCallback: {food, error in
                    if error == nil && food != nil {
                        if let fo = food {
                            observer.onNext(fo.preciseUnits!)
                            observer.onCompleted()
                        }
                    } else {
                        if error != nil {
                            observer.onError(error!)
                        }
                        observer.onCompleted()
                    }
                })
            return Disposables.create()
        }
    }
    
    private func stringFrom(nutr : NMFoodNutrient) -> String {
        switch nutr {
        case .totalFat:
            return "total fat"
            
        case .saturatedFat:
            return "saturated fat"
            
        case .transFat:
            return "trans fat"
            
        case .cholesterol:
            return "cholesterol"
            
        case .sodium:
            return "sodium"
            
        case .potassium:
            return "potassium"
            
        case .carbohydrate:
            return "carbohydrate"
            
        case .dietaryFiber:
            return "dietary fiber"
            
        case .sugars:
            return "sugars"
            
        case .protein:
            return "protein"
        
        }
    }*/
}

//
//  ChangeDietViewController.swift
//  Nourish
//
//  Created by Vlad Birukov on 22.08.17.
//  Copyright © 2017 SSIC. All rights reserved.
//

import UIKit

class ChangeDietViewController: BasePresentationViewController {
    @IBOutlet weak var changeDietView: ChangeDietView!
    @IBOutlet weak var cardMealPlannerNavigationBarView: CardMealPlannerNavigationBarView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.baseViews = [changeDietView, cardMealPlannerNavigationBarView]
        for v in self.baseViews! {
            v.viewDidLoad()
        }
        changeDietView.presenter.changeDietViewController = self
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.automaticallyAdjustsScrollViewInsets = false
        self.setUpNavigationBar()
    }
    
    func setUpNavigationBar() {
        if self.navigationController != nil {
            self.cardMealPlannerNavigationBarView.settingNavigation(title: "Meal planner")
            self.cardMealPlannerNavigationBarView.backButton.addTarget(self, action: #selector(saveChangeDietAndBack), for: .touchUpInside)
        }
    }
    
    func saveChangeDietAndBack(sender:UIButton!) {
        self.changeDietView.presenter.saveDietAndBack()
    }
}

//
//  MealRecordCache_to_Json.swift
//  Nourish
//
//  Created by Nova on 12/20/16.
//  Copyright © 2016 SSIC. All rights reserved.
//

import UIKit
import RealmSwift

class MealRecordCache_to_Json: NSObject {
    func transform(meal : MealRecordCache, userID : String, includingOcasion : Bool) -> [String : Any] {
        var dictionary : [String : Any] = [:]
        dictionary["userid"] = userID
        dictionary["amount"] = meal.amount
        //dictionary["foodid"] = meal.userFoodId
        
        if meal.grams.isNaN == false && meal.grams != Double.infinity {
            dictionary["grams"] = meal.grams
        } else {
            dictionary["grams"] = 0.0
        }
        dictionary["id"] = meal.idString ?? ""
        if meal.sourceFoodDB != kSoundHoundSourceDB {
             //self.uuidStringFrom(string: meal.idString!)
            let noomID = self.uuidStringFrom(string: meal.idString!)
            
            if noomID != "" {
                dictionary["brand"] = ""
                dictionary["brandAndName"] = meal.name
                dictionary["noom_id"] = noomID
                dictionary["por_kcal"] = meal.portionKCal
                dictionary["msre_amount"] = meal.amount
                dictionary["msre_desc"] = meal.msreDesc
                dictionary["originalSearchString"] = meal.originalSearchString
                
                let dict = self.getNutrints(nutrients: meal.nutrientsFromNoom)
                if dict.count > 0 {
                    dictionary["nutrients"] = dict
                } else {
                    dictionary["nutrients"] = NSNull()
                }
            }
            
            dictionary["userfoodid"] = meal.userFoodId
            
        } else {
            //Sound Hound
            dictionary["source_fooddb"] = meal.sourceFoodDB
            dictionary["source_fooddb_id"] = Int(meal.sourceFoodDBID)
        }

        dictionary["name"] = meal.name
        dictionary["unit"] = meal.unit
        dictionary["caloriesPerGram"] = meal.caloriesPerGramm != Double.infinity ? meal.caloriesPerGramm : 0.0
        
        
        if includingOcasion == true {
            dictionary["occasion"] = meal.occasionEnum.rawValue
            
            dictionary["day"] = self.getDayString(date: meal.date!)
        }
        dictionary["datetime"] = Int64((meal.date?.timeIntervalSince1970)! * 1000)
        let nutrs = meal.nutrientsFromNoom
        print(nutrs)
        
        
        dictionary["calories"] = meal.calories
        
        if meal.userPhotoId != nil {
            if !(meal.userPhotoId?.hasPrefix("local"))! {
                dictionary["user_photo_id"] = meal.userPhotoId
            }
        }
        
        if meal.groupID != nil && !(meal.groupID?.hasPrefix("local"))! {
            dictionary["foodgroupid"] = meal.groupID!
        }
        
        return dictionary
    }
    
    func transform(meals : [MealRecordCache], userID : String, includingOcasion : Bool) -> [[String : Any]] {
        var array : [[String : Any]] = [[:]]
        for meal in meals {
            let obj = self.transform(meal: meal, userID: userID, includingOcasion : includingOcasion)
            array.append(obj)
        }
        array.remove(at: 0)
        return array
    }
    
    private func getDayString(date : Date) -> String {
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyyMMdd"
        let string = formatter.string(from: date)
        return string
    }
    
    private func getNutrints(nutrients : List<Nutrient>) -> [String : Double] {
        var dict : [String : Double] = [:]
        for nutr in nutrients {
            dict[nutr.name] = nutr.clearValue
        }
        if dict["Calories"] != nil {
            dict.removeValue(forKey: "Calories")
        }
        
        return dict
        
    }
    
    
    private func uuidStringFrom(string : String) -> String? {
        if string.count == 32 && string.containsWhitespace == false {
            var stringValue = string
            let indexies = [8,13,18,23]
            for index in indexies {
                stringValue.insert("-", at: stringValue.index(stringValue.startIndex, offsetBy: index))
            }
            stringValue = stringValue.lowercased()
            return stringValue
        } else {
            return ""
        }
    }
    
    
    
}
extension String {
    var containsWhitespace : Bool {
        return(self.rangeOfCharacter(from: .whitespacesAndNewlines) != nil)
    }
}

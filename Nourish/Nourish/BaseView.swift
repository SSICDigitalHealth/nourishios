//
//  BaseView.swift
//  Nourish
//
//  Created by Nova on 11/15/16.
//  Copyright © 2016 SSIC. All rights reserved.
//

import UIKit

extension UIView {
    
    func addGradientWithHeight(height : CGFloat) -> CAGradientLayer {
        let layer = CAGradientLayer()
        layer.frame = CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: height)
        let alphColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.3)
        layer.colors = [UIColor.black.cgColor,alphColor.cgColor, UIColor.clear.cgColor]
        self.layer.addSublayer(layer)
    
    return layer
    }
}

class BaseView: UIView, BaseViewProtocol {
  
    
    var loadingImage : UIImageView!
    var blurView : UIView!
    var gradientLayer : CAGradientLayer?
    var activityInProgress : Bool = false
    var bannerView : UIView!
    
    var basePresenter : BasePresenterProtocol?

    func viewWillAppear(_ animated : Bool) {
        self.prepareLoadView()

        if basePresenter != nil {
            basePresenter?.viewWillAppear(animated)
        }
    }
    
    func viewDidAppear(_ animated : Bool) {
        if basePresenter != nil {
            basePresenter?.viewDidAppear(animated)
        }
    }
    
    func viewWillDisappear(_ animated : Bool) {
        if basePresenter != nil {
            basePresenter?.viewWillDisappear(animated)
        }
    }
    
    func viewDidDisappear(_ animated : Bool) {
        if basePresenter != nil {
            basePresenter?.viewDidDisappear(animated)
        }
    }
    
    func viewDidLoad() {
     //  self.prepareLoadView()
    }
    
    func viewDidLayoutSubviews() {
        if basePresenter != nil {
            basePresenter?.viewDidLayoutSubviews()
        }
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        if (self.blurView != nil) {
            self.loadingImage.center = CGPoint(x: self.blurView.frame.width/2, y: self.blurView.frame.height/2)
        }
        
    }
    
    func prepareLoadView() {
        DispatchQueue.main.async {
            self.translatesAutoresizingMaskIntoConstraints = false
            if !self.activityInProgress  {
                self.isUserInteractionEnabled = false
                self.blurView = UIView(frame: self.bounds)
                self.blurView.backgroundColor = UIColor.white.withAlphaComponent(0.8)
                self.blurView.frame = self.bounds
                self.blurView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
                var imageArray = [UIImage]()
                
                for index in 0..<49 {
                    if let image = UIImage(named: String(format : "hr_%02d",index)) {
                        imageArray.append(image)
                    }
                    
                }
                
                self.loadingImage = UIImageView(frame: CGRect(x: self.bounds.width/2, y: self.bounds.height/2, width: 50, height: 50))
                self.loadingImage.center = CGPoint(x: self.bounds.width/2, y: self.bounds.height/2)
                self.loadingImage.animationImages = imageArray
                self.loadingImage.animationDuration = 4
                self.blurView.addSubview(self.loadingImage)
                self.addSubview(self.blurView)
                self.loadingImage.startAnimating()
                self.activityInProgress = true
            }
        }
    }
    
    func stopActivityAnimation() {
        DispatchQueue.main.async  {
            if self.blurView != nil {
                if self.subviews.contains(self.blurView) {
                    self.blurView.removeFromSuperview()
                }
                self.activityInProgress = false
                self.isUserInteractionEnabled = true
            } else {
                self.activityInProgress = false
                self.isUserInteractionEnabled = true
            }
        }
    }
    
    
    func parseError(error : Error, completion:((Bool) -> Void)?) {
        let vc = self.owningViewController()
        if vc != nil && vc!.isKind(of: BasePresentationViewController.self) {
            (vc as! BasePresentationViewController).parseError(error: error, completion: completion)
        }
    }
    
    func startActivityAnimation() {
        self.prepareLoadView()
    }
    
    
    func removeActivityIndicator() {
        DispatchQueue.main.async {
            self.loadingImage.removeFromSuperview()
        }
    }
    
    func xibAutoLayouting(nibName: String) {
        let view =  UINib(nibName: nibName, bundle: nil).instantiate(withOwner: self, options: nil)[0] as! UIView

        self.addSubview(view)
        
        view.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint(item: view, attribute: .leading, relatedBy: .equal, toItem: self, attribute: .leading, multiplier: 1, constant: 0).isActive = true
        NSLayoutConstraint(item: view, attribute: .trailing, relatedBy: .equal, toItem: self, attribute: .trailing, multiplier: 1, constant: 0).isActive = true
        NSLayoutConstraint(item: view, attribute: .top, relatedBy: .equal, toItem: self, attribute: .top, multiplier: 1, constant: 0).isActive = true
        NSLayoutConstraint(item: view, attribute: .bottom, relatedBy: .equal, toItem: self, attribute: .bottom, multiplier: 1, constant: 0).isActive = true
        

    }
}

extension UIResponder {
    func owningViewController() -> UIViewController? {
        var nextResponser = self
        while let next = nextResponser.next {
            nextResponser = next
            if let vc = nextResponser as? UIViewController {
                return vc
            }
        }
        return nil
    }
}

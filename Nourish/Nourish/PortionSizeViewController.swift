//
//  PortionSizeViewController.swift
//  Optifast
//
//  Created by Gena Mironchyk on 10/12/17.
//  Copyright © 2017 SSIC. All rights reserved.
//

import UIKit


class PortionSizeViewController: BasePresentationViewController {
    @IBOutlet weak var portionSizeView : PortionSizeView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupNavBar()
        self.baseViews = [self.portionSizeView]
        self.portionSizeView.viewDidLoad()
        // Do any additional setup after loading the view.
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    

    
    private func setupNavBar() {
        let titleAttribute = self.navigationBarTitleAttribute()
        let closeButton : UIBarButtonItem =  UIBarButtonItem(image: UIImage(named:"close_icon"), style: .plain, target: self, action: #selector(self.closeAction(_:)))
        closeButton.setTitleTextAttributes(titleAttribute, for: .normal)
        self.title = "Portion size reference"
        self.navigationController?.navigationBar.barTintColor = UIColor.white
        self.navigationController?.navigationBar.tintColor = UIColor.gray
        self.navigationController?.navigationBar.titleTextAttributes = titleAttribute
        self.navigationItem.leftBarButtonItem = closeButton

    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

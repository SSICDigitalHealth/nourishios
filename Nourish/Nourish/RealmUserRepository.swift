//
//  RealmUserRepository.swift
//  Nourish
//
//  Created by Nova on 1/17/17.
//  Copyright © 2017 SSIC. All rights reserved.
//

import UIKit
import RealmSwift
import RxSwift


class RealmUserRepository: NSObject {

    func storeDefault(userCache : UserProfileCacheRealm) {
        let realm = try! Realm()
        
        self.deleteAllUsers(except: userCache)
        
        try! realm.write {
            userCache.lastTransferredToArtik = Date()
            realm.add(userCache)
        }
    }
    
    func getDefaultUser() -> UserProfileCacheRealm {
        let searchResults = try? Realm().objects(UserProfileCacheRealm.self)
        if searchResults?.count == 0 || searchResults == nil {
            return self.getEmptyUser()
        } else {
            return searchResults!.last!
        }
    }
    
    func deleteAllUsers(except : UserProfileCacheRealm) {
        let realm = try! Realm()
        let searchResults = try! Realm().objects(UserProfileCacheRealm.self)
        for object in searchResults {
            if object != except {
                try! realm.write {
                    realm.delete(object)
                }
            }
        }
        
    }
    
    func deleteAllCachedUsers() {
        let realm = try! Realm()
        let searchResults = realm.objects(UserProfileCacheRealm.self)
        if searchResults.count > 0 {
            for user in searchResults {
                try! realm.write {
                    realm.delete(user)
                }
            }
            
        }
    }
    
    func isTermsAndConditionsAgreed(userID : String?) -> Observable<Bool> {
        return Observable.create { observer in
            if userID != nil {
                let searchResults = try? Realm().objects(ChatDataBaseItem.self).filter("userID == %@", userID!)
                if searchResults?.count != nil && (searchResults?.count)! > 0 {
                    observer.onNext(true)
                } else {
                    observer.onNext(false)
                }
            } else {
                observer.onNext(false)
            }
            
            observer.onCompleted()
            return Disposables.create()
        }
        
    }
     private func getEmptyUser() -> UserProfileCacheRealm {
        let realm = try! Realm()
        let cache = UserProfileCacheRealm()
        
        try? realm.write {
            realm.add(cache)
        }
        
        return cache
     }
}

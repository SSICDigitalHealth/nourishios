//
//  WeightLossMeterPresenter.swift
//  Nourish
//
//  Created by Vino (Vinodha) Sundaramoorthy on 2/7/17.
//  Copyright © 2017 SSIC. All rights reserved.
//

import UIKit
import RxSwift


class WeightLossMeterPresenter: BasePresenter {
    let interactor = WeightLossMeterInteractor()
    var weightLossMeter : weightLossMeter!
    var weightLossView : WeightLossMeterProtocol?
    let profileInteractor = UserProfileInteractor()
    let mapperObject = UserProfile_to_UserProfileModel()
    let mapperModel = UserProfileModel_to_UserProfile()
    var userModel : UserProfileModel = UserProfileModel()
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.subscribtions.append(self.currentUser().subscribe(onNext: { [unowned self] user in
            self.userModel = user
        }, onError: {error in
            LogUtility.logToFile("error is ", error)
        }, onCompleted: {
            if self.weightLossView != nil {
                let _ = self.interactor.getRealWeighLossMeterModel().observeOn(MainScheduler.instance).subscribe(onNext: { [unowned self] model in
                        var isMetric = false
                        
                        if self.userModel.metricPreference != nil {
                            isMetric = (self.userModel.metricPreference?.isMetric)!
                        }
                        
                        self.weightLossView?.setMeterModel(model: model,bgColor: UIColor.white, isMetric: isMetric, goal:self.userModel.userGoal!)                    
                }, onError: {error in }, onCompleted: {}, onDisposed: {})
                
            }
        }, onDisposed: {}))
    }
    
    func currentUser() -> Observable<UserProfileModel> {
        let object = self.profileInteractor.getRawUser().flatMap({ userProfile -> Observable<UserProfileModel> in
            self.userModel = self.mapperObject.transform(user: userProfile)
            return Observable.just(self.mapperObject.transform(user: userProfile))
        })
        return object
    }
}


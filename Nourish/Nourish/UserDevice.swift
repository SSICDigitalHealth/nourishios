//
//  UserDevice.swift
//  Nourish
//
//  Created by Nova on 12/7/16.
//  Copyright © 2016 SSIC. All rights reserved.
//

import UIKit
import RealmSwift
import Realm

class UserDevice : Object {
    dynamic var udidString : String = ""
    dynamic var dtidString : String = ""
    dynamic var deviceName : String = ""
    dynamic var userID : String = ""
    dynamic var isAuthorized : Bool = false
}

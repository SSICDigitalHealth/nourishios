//
//  NRProfilePickerTableViewCell.swift
//  Nourish
//
//  Created by Vino (Vinodha) Sundaramoorthy on 11/9/16.
//  Copyright © 2016 SSIC. All rights reserved.
//

import UIKit

class NRProfilePickerTableViewCell: UITableViewCell {
    
    @IBOutlet weak var pickerView : NRPickerView!

    override func awakeFromNib() {
        super.awakeFromNib()
        pickerView.textColor = UIColor.white
        pickerView.highlightedTextColor = UIColor.white
        pickerView.highlightedFont = UIFont.systemFont(ofSize: 24, weight: UIFontWeightMedium)
        pickerView.font = UIFont.systemFont(ofSize: 24, weight: UIFontWeightRegular)
        pickerView.maskDisabled = true
    }

}

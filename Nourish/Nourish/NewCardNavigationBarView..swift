//
//  NewCardNavigationBarView..swift
//  Nourish
//
//  Created by Vlad Birukov on 29.08.17.
//  Copyright © 2017 SSIC. All rights reserved.
//

import Foundation


class NewCardNavigationBarView: BaseView {
    @IBOutlet weak var barTitle: UILabel!
    @IBOutlet weak var backButton: UIButton!
    @IBOutlet weak var backgroundBar: UIView!
    
    override func viewDidLoad() {
        self.xibAutoLayouting(nibName: "NewCardNavigationBarView")
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.stopActivityAnimation()
    }
}

//
//  AmazonLambdaInteractor.swift
//  Nourish
//
//  Created by Gena Mironchyk on 5/23/17.
//  Copyright © 2017 SSIC. All rights reserved.
//

import UIKit
import RxSwift

class AmazonLambdaInteractor: NSObject {
    let amazonRepository = AmazonLamdaManager()
    let userRepository = UserRepository.shared
    
    func uploadPushTokenToLambda(token : String, deviceId : String) -> Observable<Bool> {
        return userRepository.getCurrentUserID().flatMap({userId -> Observable<Bool> in
            return self.amazonRepository.uploadPushToken(token: token, userId: userId, deviceId: deviceId)
        })
    }
    
}

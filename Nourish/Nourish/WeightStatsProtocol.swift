//
//  WeightStatsProtocol.swift
//  Nourish
//
//  Created by Vino (Vinodha) Sundaramoorthy on 1/9/17.
//  Copyright © 2017 SSIC. All rights reserved.
//

import UIKit

protocol  WeightStatsProtocol {
    func fetchWeight()
    func setWeightModel(model : weightModel)
    func fetchActivityStats(period : period)
    func startActivityIndicator()
}

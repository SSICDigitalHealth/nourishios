//
//  MealPlannerCache.swift
//  Nourish
//
//  Created by Gena Mironchyk on 9/12/17.
//  Copyright © 2017 SSIC. All rights reserved.
//

import UIKit
import RealmSwift

class MealPlannerCache: Object {
    dynamic var date : Date? = nil
    dynamic var totalCalories : Double = 0.0
    dynamic var score : Double = 0.0
    dynamic var optifastMealsData : Data? = nil
    var ocasionData = List<OcasionData>()
    dynamic var lunch = 0
    dynamic var breakfast = 0
    dynamic var dinner = 0
    dynamic var snacks = 0
}

class OcasionData : Object {
    dynamic var ocasionName : RealmString?
    dynamic var mealsData : Data? = nil
    var recipiesData = List<RecipiesCacheModel>()
    dynamic var calsTotal : Double = 0.0
}

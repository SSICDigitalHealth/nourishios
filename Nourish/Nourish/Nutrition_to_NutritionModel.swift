//
//  Nutrition_to_NutritionModel.swift
//  Nourish
//
//  Created by Nova on 11/21/16.
//  Copyright © 2016 SSIC. All rights reserved.
//

import UIKit

class Nutrition_to_NutritionModel: NSObject {
    func transform(nutrition : Nutrition) -> NutritionModel {
        let nutritionModel = NutritionModel()
        nutritionModel.vitamins = nutrition.vitamins
        nutritionModel.electrolytes = nutrition.electrolytes
        nutritionModel.macronutrients = nutrition.macronutrients
        nutritionModel.minerals = nutrition.minerals
        nutritionModel.nutritionScore = nutrition.nutritionScore
        return nutritionModel
    }
    func transform(modelArray : [Nutrition]) -> [NutritionModel] {
        var arrayToReturn : [NutritionModel] = []
        for model in modelArray {
            arrayToReturn.append(self.transform(nutrition: model))
        }
        return arrayToReturn
    }
}

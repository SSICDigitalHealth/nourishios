//
//  LocalNotificationsRoutine.swift
//  Nourish
//
//  Created by Nova on 2/28/17.
//  Copyright © 2017 SSIC. All rights reserved.
//

import UIKit
import RxSwift


final class LocalNotificationsRoutine: NSObject {
    
    static let shared = LocalNotificationsRoutine()
    
    let cal = Calendar.current
    
    let foodRepo = MealHistoryRepository.shared
    let cacheToObjectMapper = MealRecordCache_to_MealRecord()
    
    func setupNotifications() {
        if NRUserSession.sharedInstance.accessToken != nil {
            self.removeAllNotifications()
            //self.setupNotificationsForToday()
            self.setupNotificationsNext7Days()
        }
        
       // self.testNotificationOneMinute()
    }
 
    private func setupNotificationsForToday() {
        if NRUserSession.sharedInstance.accessToken != nil {
            let now = Date()
            let comps = cal.dateComponents([.hour, .minute], from: now)
            
            let _ = self.foodRepo.getFoodFor(date: Date(), onlyCache: false).subscribe(onNext :{ [unowned self] records in
                
                let mealModels = self.cacheToObjectMapper.transform(cacheList: records)
                
                let missedOcasions = self.emptyOcasionsFrom(mealArray: mealModels)
                let missedTimeOcasions = self.neededOcasionFrom(hourIndex: comps.hour!)
                
                var aggregatedArray : [Ocasion] = []
                for ocasion in missedTimeOcasions {
                    if missedOcasions.contains(ocasion) {
                        aggregatedArray.append(ocasion)
                    }
                }
                
                self.setupNotificationsFor(ocasions: aggregatedArray, date: Date())
                LogUtility.logToFile("Notifications initialized")
            })
        }
        
        
    }
    
    func setupNotifications(mealRecords : [MealRecord], date : Date) {
        if cal.startOfDay(for: date) == cal.startOfDay(for: Date()) {
            let comps = cal.dateComponents([.hour, .minute], from: Date())
            let missedOcasions = self.emptyOcasionsFrom(mealArray: mealRecords)
            let missedTimeOcasions = self.neededOcasionFrom(hourIndex: comps.hour!)
            
            var aggregatedArray : [Ocasion] = []
            for ocasion in missedTimeOcasions {
                if missedOcasions.contains(ocasion) {
                    aggregatedArray.append(ocasion)
                }
            }
            self.removeAllNotifications()
            self.setupNotificationsFor(ocasions: aggregatedArray, date: Date())
            LogUtility.logToFile("Notifications initialized")
            self.setupNotificationsNext7Days()
        }
        
    }
    
    func logNotificationDates() {
        
        for notification in UIApplication.shared.scheduledLocalNotifications! {
            print("Local Notifications : \(String(describing: notification.fireDate)) \(String(describing: notification.alertBody))")
        }
    }
    
    private func testNotificationOneMinute() {
        let now = Date()
        let fireDate = cal.date(byAdding: .minute, value: 1, to: now)
        
        let notification = UILocalNotification()
        notification.fireDate = fireDate
        notification.alertBody = "Test Notification"
        UIApplication.shared.scheduleLocalNotification(notification)
    }
    
    private func setupNotificationsNext7Days() {
        let now = Date()
        let fullOcasionsArray : [Ocasion] = [.breakfast, .lunch, .dinner]
        
        for index in 1...7 {
            let nextDate = cal.date(byAdding: .day, value: index, to: now)
            self.setupNotificationsFor(ocasions: fullOcasionsArray, date: nextDate!)
        }
    }
    
    func removeAllNotifications() {
        DispatchQueue.main.async {
             UIApplication.shared.cancelAllLocalNotifications()
        }
    }
    
    private func emptyOcasionsFrom(mealArray : [MealRecord]) -> [Ocasion] {
        var arrayOfOccasion : [Ocasion] = [.breakfast, .lunch, .dinner]
        for meal in mealArray {
            if arrayOfOccasion.contains(meal.ocasion) {
                let index = arrayOfOccasion.index(of: meal.ocasion)
                arrayOfOccasion.remove(at: index!)
            }
        }
        return arrayOfOccasion
    }
    
    private func neededOcasionFrom(hourIndex : Int) -> [Ocasion] {
        if hourIndex < 10 {
            return [.breakfast, .lunch, .dinner]
        } else if hourIndex < 14 {
            return [.lunch, .dinner]
        } else if hourIndex < 21 {
            return [.dinner]
        } else {
            return []
        }
    }
    
    private func setupNotificationsFor(ocasions : [Ocasion], date : Date) {
        var dateComps = cal.dateComponents([.day, .month, .year, .hour, .minute], from: date)
        for ocasion in ocasions {
            
            let hourIndex = self.hourIndexFor(ocasion: ocasion)
            dateComps.hour = hourIndex
            dateComps.minute = 0
            let date = cal.date(from: dateComps)
            
            let stringRep = self.stringFor(ocasion: ocasion)
            
            let notification = UILocalNotification()
            notification.fireDate = date
            notification.alertBody = stringRep
            
            DispatchQueue.main.async {
                UIApplication.shared.scheduleLocalNotification(notification)
            }
            
        }
    }
    
    private func stringFor(ocasion : Ocasion) -> String {
        return String(format : "You've missed a %@. Don't forget to log it!", Ocasion.stringDescription(servingType: ocasion))
    }
    
    private func hourIndexFor(ocasion : Ocasion) -> Int {
        if ocasion == .breakfast {
            return 10
        } else if ocasion == .lunch {
            return 14
        } else {
            return 21
        }
    }
}

//
//  ExerciseModel.swift
//  Nourish
//
//  Created by Vlad Birukov on 28.08.17.
//  Copyright © 2017 SSIC. All rights reserved.
//

import Foundation

typealias activityList = (activityName: String, value: Double)
class ExerciseModel {
    var activeHours: Double = 0.0
    var activityList: [activityList] = []
}

//
//  Json_to_WithingsMessage.swift
//  Nourish
//
//  Created by Gena Mironchyk on 4/29/17.
//  Copyright © 2017 SSIC. All rights reserved.
//

import UIKit

class Json_to_WithingsMessage: NSObject {
    func transform(dict :[String : Any]) -> WithingsMessage {
        let message = WithingsMessage()
        let timestamp = dict["ts"] as? Double ?? 0.0
        let date = Date(timeIntervalSince1970 : timestamp/1000.0)
        message.date = date
        
        let artikTimestamp = dict["cts"] as? Double ?? 0.0
        let artikDate = Date(timeIntervalSince1970 : artikTimestamp/1000.0)
        message.dateAddedToArtik = artikDate
        
        let messageInfo = dict["data"] as? [String : Any]
        if messageInfo != nil {
            message.groupID = messageInfo!["groupId"] as? Double ?? 0.0
            if messageInfo!.index(forKey: "weight") == nil {
                message.weight = 0.0
            } else {
                message.weight = messageInfo!["weight"] as? Double ?? 0.0
            }
        }
        
        return message
    }
    
    func transform(array : [[String : Any]]) -> [WithingsMessage] {
        var arrayToReturn : [WithingsMessage] = []
        for object in array {
            arrayToReturn.append(self.transform(dict: object))
        }
        return arrayToReturn
    }
}

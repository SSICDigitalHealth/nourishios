//
//  ScoreCacheRealm.swift
//  Nourish
//
//  Created by Vino (Vinodha) Sundaramoorthy on 4/18/17.
//  Copyright © 2017 SSIC. All rights reserved.
//

import UIKit
import RealmSwift
import Realm


class NutrientInfoRealm : Object {
    dynamic var name = ""
    dynamic var value = 0.0
    dynamic var min = 0.0
    dynamic var max = 0.0
    dynamic var unit = ""
}

class ScoreCacheRealm: Object {
    dynamic var score : Double = 0
    dynamic var scoreDelta : Double = 0
    dynamic var valueToShow : Double = 0
    var nutrientList = List<NutrientInfoRealm>()
    var InDri =  List<RealmString>()
    var aboveDri =  List<RealmString>()
    var closeToUpperLimit = List<RealmString>()
    var worstDelta = List<RealmString>()
    var bestDelta = List<RealmString>()
    var date = Date()
}

//
//  BackgroundFitBitCacher.swift
//  Nourish
//
//  Created by Vino (Vinodha) Sundaramoorthy on 8/17/17.
//  Copyright © 2017 SSIC. All rights reserved.
//

import UIKit
import RxSwift

class BackgroundFitBitCacher: NSObject {

    static let shared = BackgroundFitBitCacher()
    
    let fitBitRepository = FitBitActivityRepository()
    var queue : DispatchQueue? = nil
    var isCaching : Bool = false
    
    func startCaching() {
        if self.isCaching == false {
            LogUtility.logToFile("Started FitBit sync")
            self.isCaching = true
            self.queue = DispatchQueue(label: "queuename", attributes: .concurrent)
            self.queue?.async {
                let _ = self.fitBitRepository.synchronizeFitBitActivities()
            }
        }
    }

}

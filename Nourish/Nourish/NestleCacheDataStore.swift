//
//  NestleCacheDataStore.swift
//  Nourish
//
//  Created by Gena Mironchyk on 8/30/17.
//  Copyright © 2017 SSIC. All rights reserved.
//

import UIKit
import RealmSwift
class NestleCacheDataStore: NSObject {

    func store(cache : NestleProgressCache) {
        print("Storing progresss cache")
        let realm = try! Realm()

        let predicate = NSPredicate(format: "startDate == %@ && endDate == %@", cache.startDate! as NSDate, cache.endDate! as NSDate)
        let objs = realm.objects(NestleProgressCache.self).filter(predicate)
        if objs.count > 0 {
            try! realm.write {
                for obj in objs {
                    realm.delete(obj, cascading: true)
                }
            }
        }
        try! realm.write {
            realm.add(cache)
        }
        
    }
    
    func getCacheFor(startDate : Date, endDate : Date, returnHistory : Bool) -> NestleProgressCache? {
        print("Getting progresss cache")
        let realm = try! Realm()
        let calendar = Calendar.current
        let historyPredicate = NSPredicate(format : "returnHistory == %@", NSNumber(value : returnHistory))
        
        let predicate = NSPredicate(format: "startDate == %@ AND endDate == %@", calendar.startOfDay(for: startDate) as NSDate, calendar.startOfDay(for: endDate) as NSDate)
        let obj = realm.objects(NestleProgressCache.self).filter(predicate).filter(historyPredicate).first
        
        return obj
    }
    
    
    func deletCacheFor(startDate : Date, endDate : Date, returnHistory : Bool) {
        let realm = try! Realm()
        let calendar = Calendar.current
        let startOfDay = calendar.startOfDay(for: startDate)
        let endOfDay = calendar.startOfDay(for: endDate)

        let historyPredicate = NSPredicate(format : "returnHistory == %@", NSNumber(value : returnHistory))
        let predicate = NSPredicate(format : "startDate <= %@ && endDate => %@",startOfDay as NSDate, endOfDay as NSDate)
        
        let objs = realm.objects(NestleProgressCache.self).filter(predicate).filter(historyPredicate)
        if objs.count > 0 {
            try! realm.write {
                realm.delete(objs, cascading: true)
            }
        }
    }
    
    
    
    func deleteAllProgressCache() {
        let realm = try! Realm()
        let objs = realm.objects(NestleProgressCache.self)
        if objs.count > 0 {
            try! realm.write {
                realm.delete(objs, cascading: true)
            }
        }
    }
}

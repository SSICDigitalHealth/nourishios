//
//  NRRecommendationsView.swift
//  Nourish
//
//  Created by Vino (Vinodha) Sundaramoorthy on 11/14/16.
//  Copyright © 2016 SSIC. All rights reserved.
//

import UIKit

class NRRecommendationsView: BaseView , RecommendationProtocol {
    var controller = NRRecommendationViewController()
    @IBOutlet weak var tableView = UITableView()
    var presenter = NRRecommendationPresenter()
    var itemAdded : Bool = false

    override func viewWillAppear(_ animated: Bool) {
        self.tableView?.register(UINib(nibName: "NRRecommendationsCell", bundle: nil), forCellReuseIdentifier: "kRecommendationCell")
        self.basePresenter = presenter
        presenter.recommendationView = self
        self.tableView?.tableHeaderView = UIView(frame: CGRect(x: 0, y: 0, width: (self.tableView?.bounds.width)!, height: 1))
        self.tableView?.tableFooterView = UIView(frame: CGRect(x: 0, y: 0, width: (self.tableView?.bounds.width)!, height: 1))
        self.backgroundColor = UIColor.white
        self.tableView?.backgroundColor = UIColor.clear
        super.viewWillAppear(animated)
    }
    
    func showRecommendationList() {
        DispatchQueue.main.async {
            self.stopActivityAnimation()
            self.tableView?.delegate = self.presenter
            self.tableView?.dataSource = self.presenter
            self.tableView?.reloadData()
            if self.itemAdded {
                self.controller.showToast(message: "Item Added", showingError: false, completion: nil)
                self.itemAdded = false
            }
        }
    }
    
    func getRecomendationIndexPathForCell(cell:UITableViewCell) -> IndexPath {
        return (tableView?.indexPath(for: cell))!
    }
    
    func reloadSectionWithIndex(section : Int) {
        DispatchQueue.main.async {
            self.tableView?.reloadData()
        }
    }
    
    func startActivityIndicator() {
        self.prepareLoadView()
    }
    
    func stopActivittyIndicator() {
        self.stopActivityAnimation()
    }
    
    func showFoodCardwithModel(model:foodSearchModel) {
        self.controller.showFoodCardwithModel(model:model)
    }
}

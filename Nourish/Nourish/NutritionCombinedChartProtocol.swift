//
//  NutritionCombinedChartProtocol.swift
//  Nourish
//
//  Created by Vino (Vinodha) Sundaramoorthy on 1/10/17.
//  Copyright © 2017 SSIC. All rights reserved.
//

import UIKit

protocol  NutritionCombinedChartProtocol {
    func fetchCombinedChartStats(period : period)
    func setCalorieConsumedStats(model : nutritionStatsModel, forPeriod:period)
}


//
//  DatePickerView.swift
//  Nourish
//
//  Created by Andrey Zmushko on 8/22/17.
//  Copyright © 2017 SSIC. All rights reserved.
//

import UIKit
import RxSwift

protocol DatePickerProtocol {
    func onChange(datePicker: DatePickerView, newDate: Date)
}

class DatePickerView: UIView {
    
    enum Schema {
        case progress
        case activity
        case planner
        
        func setup(_ view: DatePickerView) {
            
            func setupButton(button: UIButton, name: String, tintColor: UIColor) {
                button.setImage(UIImage(named: name)?.withRenderingMode(.alwaysTemplate), for: .normal)
                button.tintColor = tintColor
                button.backgroundColor = UIColor.clear
            }
        }
        
        func renderButtons(_ view: DatePickerView, date: Date, isWeekly: Bool = false) {
            switch self {
            case .activity:
                if view.isToday() {
                    view.enableLeftButton()
                    view.disableRightButton()
                }
                else {
                    view.enableRightButton()
                    view.enableLeftButton()
                }
            case .progress:
                if view.isToday() {
                    view.enableLeftButton()
                    view.disableRightButton()
                }
                else {
                    view.enableRightButton()
                    view.enableLeftButton()
                }
            case .planner:
                if view.isToday() {
                    view.disableLeftButton()
                    view.enableRightButton()
                } else if isWeekly == true  {
                    view.enableLeftButton()
                    view.disableRightButton()
                }
                else {
                    view.disableLeftButton()
                    view.enableRightButton()
                }
            }
            
        }
    }
    
    
    enum Period {
        case future
        case daily
        case weekly
        case monthly
        
        func labelText(_ view: DatePickerView) -> String {
            switch self {
            case .daily:
                return view.isToday() ? "Today" : {
                        let dateFormatter = DateFormatter()
                        dateFormatter.dateFormat = "EEEE, MMMM dd"
                        
                        return dateFormatter.string(from: view.date)
                    }()
            case .weekly:
                return view.isToday() ? "Last 7 days" : String(format: "%@ - %@", view.formatedFrom(view.date, format: "MM/dd"), { () -> String in
                    let calendar = Calendar.current
                    return view.formatedFrom(calendar.date(byAdding: .day, value: 6, to: view.date)!, format:  "MM/dd")
                }())
            case .monthly:
                return view.isToday() ? "Last 30 days" : view.formatedFrom(view.date, format: "MMMM YYYY")
            case .future:
                if view.isToday() {
                    return "Today"
                } else if view.isTomorrow() {
                    return "Tomorrow"
                } else {
                    return  view.formatedFrom(view.date, format: "EEEE MMMM dd")
                }
            }
        }
    }

    enum slideDirection {
        case forward
        case backward
        
        var value: Int {
            switch self {
            case .forward:
                return 1
            case .backward:
                return -1
            }
        }
    }
    
    @IBOutlet weak var leftButton: UIButton!
    @IBOutlet weak var label: UILabel!
    @IBOutlet weak var rightButton: UIButton!
    let interactor = MealPlannerCacheInteractor()
    

    var delegate: DatePickerProtocol?
    var date: Date = Date()
    var period: Period?
    var schema: Schema?
    var disposeBag = DisposeBag()

    func setupWith(deligate: DatePickerProtocol, schema: Schema, date: Date = Date()) {
        self.delegate = deligate
        schema.setup(self)
        self.period = .daily
        self.schema = schema
        self.date = date
        self.render()
    }
    
    func setupPeriod(period: Period) {
        let calendar = Calendar.current
        self.period = period
        
        if self.period == .future {
            let _ = interactor.execute(dateWith: calendar.date(byAdding: .day, value: -1, to: self.date)!, dateFor:calendar.date(byAdding: .day, value: 1, to: self.date)!).subscribe(onNext: { [unowned self] (generateLast, generateSecond) in
                self.renderMealPlannerBackState(last: generateLast, future: generateSecond)
            }, onError: {error in
            }, onCompleted: {
            }, onDisposed: {
            }).addDisposableTo(self.disposeBag)
        } else {
            self.render()
        }
    }
    
    func setupDate(date: Date) {
        self.date = date
        self.render()
    }
    
    private func slideDate(direction: slideDirection) {
        if let period = self.period {
            let calendar = Calendar.current
            switch period {
            case .future:
                self.date = calendar.date(byAdding: .day, value: direction.value, to: self.date)!
            case .daily:
                self.date = calendar.date(byAdding: .day, value: direction.value, to: self.date)!
            case .weekly:
                if self.isToday() {
                    self.date = Date().startOfPreviousWeek()!
                }
                else {
                    self.date = calendar.date(byAdding: .day, value: direction.value * 7, to: self.date)!
                }
            case .monthly:
                self.date = (calendar.date(byAdding: .month, value: direction.value, to: self.date)?.startOfMonth())!
            }
            
            let progressPeriod = { () -> Date in
                switch period {
                case .future:
                    return Date()
                case .daily:
                    return Date()
                case .weekly:
                    return calendar.date(byAdding: .day, value: -7, to: Date())!
                case .monthly:
                    return calendar.date(byAdding: .day, value: -30, to: Date())!
                }
            }
            
            if self.period != .future && (isToday() || self.date > progressPeriod()) {
                self.date = Date()
            }
            
            self.delegate?.onChange(datePicker: self, newDate: self.date)
            self.render()
        }
    }

    @IBAction func leftTouchUp(_ sender: Any) {
        self.slideDate(direction: .backward)
    }
    
    @IBAction func rightTouchUp(_ sender: Any) {
        self.slideDate(direction: .forward)
    }
    
    private func formatedFrom(_ date: Date, format: String) -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = format
        return dateFormatter.string(from: date)
    }
    
    private func render() {
        if let period = self.period {
            self.label.text =  period.labelText(self)
        }
        if let schema = self.schema {
            schema.renderButtons(self, date: self.date, isWeekly: self.isWeekly(curentDate: self.date))
        }
    }
    
    private func renderMealPlannerBackState(last: Bool, future: Bool) {
        if let period = self.period {
            self.label.text = period.labelText(self)
        }
        
        if last == true && future == true {
            self.enableLeftButton()
            self.enableRightButton()
        } else if last == true && future == false {
            self.enableLeftButton()
            self.disableRightButton()
        } else {
            self.disableLeftButton()
            self.enableRightButton()
        }
    }
    
    func isToday() -> Bool {
        let calendar = Calendar.current
        let result = calendar.compare(self.date, to: Date(), toGranularity: .day)
        switch result {
        case .orderedSame:
            return true
        default:
            break
        }
        return false
    }
    
    func isWeekly(curentDate: Date) -> Bool {
        let calendar = Calendar.current
        let nowDate = Date()
        let dateTwoWeekly = calendar.date(byAdding: .day, value: 7, to: nowDate)
        
        if calendar.compare(curentDate, to: dateTwoWeekly!, toGranularity: .day) == .orderedSame {
            return true
        } else {
            return false
        }
    }
    
    func isTomorrow() -> Bool {
        let calendar = Calendar.current
        
        return calendar.isDateInTomorrow(self.date)
    }
    
    private func enableLeftButton() {
        self.leftButton.isHidden = false
    }
    private func disableLeftButton() {
        self.leftButton.isHidden = true
    }
    
    private func enableRightButton() {
        self.rightButton.isHidden = false
    }
    private func disableRightButton() {
        self.rightButton.isHidden = true
    }
}

//
//  CaloriesChartView.swift
//  Nourish
//
//  Created by Vlad Birukov on 20.06.17.
//  Copyright © 2017 SSIC. All rights reserved.
//

import UIKit

protocol CaloriesChartProtocol : BaseViewProtocol {
    func reloadWith(dataSource: CaloriesChartPresenter)
    func config() -> ProgressConfig?
}

class CaloriesChartView: ProgressView, CaloriesChartProtocol, ChartViewDelegate {
    var presenter:  CaloriesChartPresenter!
    @IBOutlet weak var caloricChart: UIView!
    @IBOutlet weak var caloriesBarChart: CaloriesBarChart!

    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        self.presenter = CaloriesChartPresenter()
        if let config = self.progressConfig {
            self.renderView(config)
        }
        
        self.basePresenter = self.presenter
        self.presenter.caloriesChartView = self
        super.viewWillAppear(animated)
    }
    
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.presenter = nil
    }
    
    override func renderView(_ config: ProgressConfig) {
        super.renderView(config)
        self.caloricChart =  UINib(nibName: "CaloriesChartView", bundle: nil).instantiate(withOwner: self, options: nil)[0] as! UIView
        self.loadContent(contentView: caloricChart)
        self.loadWhiteThemeWith(header: "Calories consumed")
        self.hideLine()
    }

    override func gestureTapAction() {
        if let hook = tapHook {
            hook(.calories)
        }
    }
    
    func settingButtomSpace() {
      self.contentHolderBottom.constant = 1.25
    }
    
    func reloadWith(dataSource: CaloriesChartPresenter) {
        caloriesBarChart.reloadWith(DataSource: dataSource)
    }

}

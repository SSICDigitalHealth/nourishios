//
//  MealRecord_to_MealRecordModel.swift
//  Nourish
//
//  Created by A on 14.11.16.
//  Copyright © 2016 SSIC. All rights reserved.
//

import Foundation

class MealRecord_to_MealRecordModel : NSObject {
    func transform(mealRecord : MealRecord) -> MealRecordModel {
        let mealModel = MealRecordModel()
        mealModel.caloriesPerServing = mealRecord.portionKCal
        mealModel.ocasion = mealRecord.ocasion
        mealModel.date = mealRecord.date
        mealModel.userFoodID = mealRecord.userFoodId
        mealModel.foodId = mealRecord.foodUid
        mealModel.foodTitle = mealRecord.name
        mealModel.servingType = mealRecord.unit
        mealModel.servingAmmount = mealRecord.amount
        mealModel.calories = mealRecord.calories
        mealModel.grams = mealRecord.grams
        mealModel.isFromNoom = mealRecord.isFromNoom
        mealModel.idString = mealRecord.idString
        
        mealModel.isFavourite = mealRecord.isFavourite
        mealModel.isGroupFavourite = mealRecord.isGroupFavourite
        mealModel.groupedMealArray = mealRecord.groupedMealArray
        mealModel.groupID = mealRecord.groupID
        mealModel.groupName = mealRecord.groupName
        mealModel.noomID = mealRecord.noomID
        mealModel.numberOfItems = mealRecord.numberOfItems
        mealModel.userPhotoId = mealRecord.userPhotoId
        mealModel.groupPhotoID = mealRecord.groupPhotoID
        mealModel.groupFoodUUID = mealRecord.groupFoodUUID
        mealModel.localCacheID = mealRecord.localCacheID
        mealModel.isFromOperaion = mealRecord.isFromOperation
        
        return mealModel
    }
    
    func transform(recordArray : [MealRecord]) -> [MealRecordModel] {
        var arrayToReturn : [MealRecordModel] = []
        for record in recordArray {
            arrayToReturn.append(self.transform(mealRecord: record))
        }
        return arrayToReturn
    }
    
    func prepareForGroups(modelArray : [MealRecordModel]) -> [MealRecordModel] {
        var arrayFiltered = [MealRecordModel]()
        
        let singleItems = modelArray.filter {$0.groupID == nil && $0.groupFoodUUID == nil}
        
        let oldGroups = modelArray.filter {$0.groupID != nil && $0.groupFoodUUID == nil}
        
        let newGroups = modelArray.filter {$0.groupID != nil && $0.groupFoodUUID != nil}
        
        var dictOG = [String : [MealRecordModel]]()
        
        for object in oldGroups {
            var arr = dictOG[object.groupID!]
            
            if arr == nil {
                arr = [MealRecordModel]()
            }
            
            arr?.append(object)
            dictOG[object.groupID!] = arr
        }
        
        var dictNG = [String : [MealRecordModel]]()
        
        for object in newGroups {
            var arr = dictNG[object.groupFoodUUID!]
            
            if arr == nil {
                arr = [MealRecordModel]()
            }
            
            arr?.append(object)
            dictNG[object.groupFoodUUID!] = arr
        }
        
        arrayFiltered.append(contentsOf: singleItems)
        arrayFiltered.append(contentsOf: self.modelsFrom(dict: dictOG))
        arrayFiltered.append(contentsOf: self.modelsFrom(dict: dictNG))
        
        
        return arrayFiltered.sorted{ (item1, item2) -> Bool in
            let t1 = item1.date ?? Date.distantPast
            let t2 = item2.date ?? Date.distantPast
            return t1<t2
        }
    }
    
    private func modelsFrom(dict : [String : [MealRecordModel]]) -> [MealRecordModel] {
        var arrayToReturn = [MealRecordModel]()
        
        for (stringID , array) in dict {
            arrayToReturn.append(self.modelFromID(stringID: stringID, array: array))
        }
        
        return arrayToReturn
    }
    
    private func modelFromID(stringID : String, array : [MealRecordModel]) -> MealRecordModel {
        let model = MealRecordModel()
        let dataModel = array.first!
        
        model.groupedMealArray = array
        model.userFoodID = dataModel.userFoodID
        model.groupName = dataModel.groupName
        model.groupID = dataModel.groupID
        model.ocasion = dataModel.ocasion
        model.servingAmmount = 1
        model.servingType = ""
        model.groupID = dataModel.groupID
        model.isGroupFavourite = dataModel.isGroupFavourite
        model.foodTitle = dataModel.groupName
        model.groupPhotoID = dataModel.groupPhotoID
        model.groupFoodUUID = dataModel.groupFoodUUID
        
        let calories = model.groupedMealArray.reduce(0.0) { $0 + ($1.calories ?? 0) }
        model.calories = calories
        
        let number = dataModel.numberOfItems
        if number != nil && number! == 1 {
            model.isFavourite = false
        }
        
        if model.groupedMealArray.count == 1 {
            model.isGroupFavourite = false
            model.isFavourite = dataModel.isFavourite
            model.servingAmmount = dataModel.servingAmmount
            model.servingType = dataModel.servingType
        }
        
        return model
    }
    
    func groupFoodDetailModelFrom(model : MealRecordModel) -> groudFoodDetailsModel {
        return groudFoodDetailsModel(groupTitle : model.groupName ?? model.foodSearchRepresentation().foodTitle,groupId : model.groupID ?? model.foodSearchRepresentation().foodId,groupBackendID : "", isFromNoom : model.isFromNoom, isFavorite : model.isGroupFavourite,groupMealArray : model.groupedMealArray, photoID : model.groupPhotoID)
    }
}

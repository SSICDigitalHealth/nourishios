//
//  StartMealPlannerViewModel.swift
//  Nourish
//
//  Created by Vlad Birukov on 29.08.17.
//  Copyright © 2017 SSIC. All rights reserved.
//

import Foundation

class StartMealPlannerViewModel: NSObject {
    var diet: String?
    var optifast: String?
    var caloriesDay: Double?
}

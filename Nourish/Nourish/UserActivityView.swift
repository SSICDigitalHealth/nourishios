//
//  UserActivityView.swift
//  Nourish
//
//  Created by Nova on 11/21/16.
//  Copyright © 2016 SSIC. All rights reserved.
//

import UIKit

class UserActivityView: BaseClockView, UserActivityViewProtocol {

    var presenter = UserActivityPresenter()
    var activities : [UserActivityModel]?
    var stepsLabel : UILabel? = nil
    var controller = ProgressViewController()

    override func viewWillAppear(_ animated: Bool) {
        self.basePresenter = presenter
        presenter.activityView = self
        super.viewWillAppear(animated)
    }
    
    func setSteps(steps : Double) {
        if self.stepsLabel == nil {
            self.drawStepsLabel()
        }
        let stepsString = String(format: "%0.f",steps)
        let stepsAttribute = [ NSForegroundColorAttributeName: NRColorUtility.progressLabelColor() , NSFontAttributeName : UIFont.systemFont(ofSize: 34, weight:UIFontWeightMedium)]
        let stepsAttrString = NSMutableAttributedString(string: stepsString, attributes: stepsAttribute)
        let labelString = "\nSteps"
        let labelAttributes = [ NSForegroundColorAttributeName: NRColorUtility.progressLabelColor(), NSFontAttributeName : UIFont.systemFont(ofSize: 16)]
        stepsAttrString.append(NSAttributedString(string: labelString, attributes: labelAttributes))
        self.stepsLabel?.attributedText = stepsAttrString
        self.stopActivityAnimation()

    }
    
    func drawStepsLabel() {
        let label = UILabel(frame: CGRect(x: self.bounds.origin.x, y: self.bounds.origin.y - defaultLegendOffset, width: self.bounds.size.width, height: self.bounds.size.height))
        label.textAlignment = .center
        label.numberOfLines = 0
        self.stepsLabel = label
        self.addSubview(self.stepsLabel!)
    }
    
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        self.addShadow()
        self.setUpLegends()
        self.clockfaceCount = 24
        let context = UIGraphicsGetCurrentContext()
        context!.clear(rect)
        context!.setFillColor((self.backgroundColor?.cgColor)!)
        context!.fill(rect)
        
        self.drawShadeСlockFace(context: context!, colorArray: [])
        
        if activities != nil {
            self.drawActivities(activities: activities!, context: context!)
        }
        self.drawLegends()
        self.drawTitle()
        self.registerGesture()
    }
    
    func setupWith(activitiesArray : [UserActivityModel]) {
        self.activities = activitiesArray
        self.backgroundColor = UIColor.white
        self.stopActivityAnimation()
        self.setNeedsDisplay()
        
        CATransaction.flush()
    }
    
    func drawActivities(activities : [UserActivityModel], context : CGContext) {
        
        for activity in activities {
            
            let secondIntoRadian = 2 * Double.pi / (24 * 60 * 60)
            var startOfDay = activity.startDate
            startOfDay = Calendar.current.startOfDay(for: startOfDay!)
            let dateToCompare = activity.startDate
            let startAngle = CGFloat(270).degreesToRadians +
                CGFloat((dateToCompare?.timeIntervalSince(startOfDay!))! * secondIntoRadian)
            let realEndAngle = startAngle + CGFloat(activity.duration() * secondIntoRadian)
            
            self.drawActivityArc(context: context, startAngle: startAngle, endAngle: realEndAngle, color: activity.colorOfActivity())
        }
        
    }
    
    func drawActivityArc(context : CGContext, startAngle : CGFloat, endAngle : CGFloat, color : UIColor) {
        let center = CGPoint(x:bounds.width/2, y: bounds.height/2 - defaultLegendOffset)
        let radius: CGFloat = max(bounds.width, bounds.height) / 2
        
        let arcWidth: CGFloat = super.defaultArcWidth
        
        let path = UIBezierPath(arcCenter: center,
                                radius: radius - 100 - arcWidth/2,
                                startAngle: startAngle,
                                endAngle: endAngle,
                                clockwise: true)
        path.lineWidth = arcWidth
        color.setStroke()
        path.stroke()
    }
    
    func setUpLegends() {
        legendArray = [(description:"Activity",color:NRColorUtility.activityColorGreen()),(description:"Sleep",color:NRColorUtility.activityColorBlue())]
    }
    
    func drawTitle() {
        let frame  = CGRect(x: 30, y: 10, width: 150, height: 22)
        let font = UIFont.systemFont(ofSize: 16)
        
        let title = NRDrawingUtility.drawTextLayer(frame: frame, text: "Activity", color: NRColorUtility.progressLabelColor().cgColor, bgColor: UIColor.clear.cgColor, font: font)
        title.alignmentMode = "left"
        self.layer.addSublayer(title)
    }
    
    func registerGesture() {
        let gesture = UITapGestureRecognizer(target: self, action: #selector (self.showActivityDetails (_:)))
        self.addGestureRecognizer(gesture)
    }
    
    func showActivityDetails(_ sender:UITapGestureRecognizer){
        self.controller.openVCActivityStats()
    }

}

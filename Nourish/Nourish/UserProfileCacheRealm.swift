//
//  UserProfileCacheRealm.swift
//  Nourish
//
//  Created by Nova on 1/17/17.
//  Copyright © 2017 SSIC. All rights reserved.
//

import UIKit
import RealmSwift
import Realm
let defaults = UserDefaults.standard


class UserProfileCacheRealm: Object {
    dynamic var userID : String? = nil
    dynamic var userFirstName : String? = nil
    dynamic var userLastName : String? = nil
    dynamic var activeSinceTimeStamp : Date? = nil
    var height = RealmOptional<Double>()
    var weight = RealmOptional<Double>()
    var startWeightTest = RealmOptional<Double>()
    var goalTest = RealmOptional<Double>()
    var age = RealmOptional<Int>()
    dynamic var activityLevel : String? = nil
    dynamic var dietaryPreference : String? = nil
    dynamic var metricPreference : String? = nil
    dynamic var biologicalSex : String? = nil
    var goalPrefsActivity = RealmOptional<Float>()
    var goalPrefsFood = RealmOptional<Float>()
    var goalPrefsLifestyle = RealmOptional<Float>()
    dynamic var userGoal : String? = nil
    var weightToMaintain = RealmOptional<Double>()

    dynamic var weightLossStartDate : Date? = nil
    dynamic var email : String? = nil
    dynamic var lastTransferredToArtik : Date? = nil
    dynamic var needToInformUser : Bool = false
    
    var listOfOptifastOcasions = List<RealmInt>()
    var listOfOptifastFoodIds = List<RealmString>()
    
}

class RealmInt : Object {
    dynamic var value = 1
}

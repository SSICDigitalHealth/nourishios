//
//  NutritionalDetailPresenter.swift
//  Nourish
//
//  Created by Vlad Birukov on 19.06.17.
//  Copyright © 2017 SSIC. All rights reserved.
//

import Foundation
import RxSwift

class NutritionalDetailPresenter : BasePresenter, UITableViewDataSource, UITableViewDelegate {
    let objectToModelMapper = NutritionalBalanceModel_to_NutritionBalanceViewModel()
    let interactor = NutritionalDetailInteractor()
    var nutritionalDetailView : NutritionalDetailBalanceProtocol!
    var nutritionalDetailModel = NutritionBalanceViewModel()
    var heightTableView = 1.0
    var date: (Date, Date)?
    
    let nutritionHeight = 225
    let heightHeader = 49
    let grainsHeightCell = 265
    let heightCell = 30
    let weeklyMontlyHeightCell = 237

    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.getNutritionalDetail()
    }
    
    private func getNutritionalDetail() {
        if self.nutritionalDetailView.config() != nil {
            self.date = self.nutritionalDetailView.config()?.getDate()
        }
        
        if self.date != nil {
            self.subscribtions.append(self.interactor.execute(startDate: (self.date?.0)!, endDate: (self.date?.1)!).observeOn(MainScheduler.instance).subscribe(onNext: { [unowned self] nutritionalDetail in
                self.nutritionalDetailModel = self.objectToModelMapper.transform(model: nutritionalDetail)
                
                self.nutritionalDetailView.reloadTable()
                self.nutritionalDetailView.stopActivityAnimation()
            }, onError: {error in
                self.nutritionalDetailView.stopActivityAnimation()
                self.nutritionalDetailView.parseError(error : error, completion: nil)
            }, onCompleted: {
                self.nutritionalDetailView.reloadTable()
            }, onDisposed: {
                self.nutritionalDetailView.stopActivityAnimation()
            }))
        }
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return nutritionalDetailModel.arrNutritionElement.count + 1
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return section == 0 ? CGFloat(self.nutritionHeight) : CGFloat(self.heightHeader)
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        if section == 0 {
            let  headerCell = tableView.dequeueReusableCell(withIdentifier: "cellNutririon") as! NutritionTableViewCell
            
            headerCell.setupWith(model: self.nutritionalDetailModel.nutritionData)

            return headerCell
        } else {
            let  headerCell = tableView.dequeueReusableCell(withIdentifier: "cellNutritionalHeader") as! NuntritionalDetailHEaderTableViewCell
            
            let currentModel = nutritionalDetailModel.arrNutritionElement[section - 1]
            
            headerCell.setupWithModel(model: currentModel, date: self.date)
            
            return headerCell
        }
    }

    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        if nutritionalDetailView.config()?.period != .daily && nutritionalDetailModel.arrNutritionElement[indexPath.section - 1].type != .grains {
            return CGFloat(self.weeklyMontlyHeightCell)
        } else if nutritionalDetailView.config()?.period == .daily {
            return CGFloat(self.heightCell)
        } else {
            return CGFloat(self.grainsHeightCell)
        }
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0 {
            return 0
        } else {
            if nutritionalDetailView.config()?.period == .daily {
                return nutritionalDetailModel.arrNutritionElement[section - 1].arrNutrition != nil ? (nutritionalDetailModel.arrNutritionElement[section - 1].arrNutrition?.count)! : 0
            } else {
                return 1
            }
        }
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if self.nutritionalDetailView.config()?.period == .daily {
            let cell = tableView.dequeueReusableCell(withIdentifier: "cellNutrition", for: indexPath) as! NutritionalDetailTableViewCell
    
            let currentModel = nutritionalDetailModel.arrNutritionElement[indexPath.section - 1]
            cell.setupWithModel(model: currentModel, indexPath: indexPath)
            
            return cell
        } else {
            let cell = tableView.dequeueReusableCell(withIdentifier: "cellDetailNutrient", for: indexPath) as! NutritionalWeeklyMonthlyTableViewCell
            
            cell.setupWithModel(model: nutritionalDetailModel, indexPath: indexPath, date: self.date)
            
            return cell
        }
    }
}

//
//  CaloricProgressModel.swift
//  Nourish
//
//  Created by Vlad Birukov on 28.08.17.
//  Copyright © 2017 SSIC. All rights reserved.
//

import Foundation

typealias caloricProgress = (subtitle: Date, cal: (bmrCal: Double, maxConsumedCal: Double, maxBurnedCal: Double, userConsumedCal: Double, userBurnedCal: Double))

class CaloricProgressModel {
    var userGoal: UserGoal?
    var weeklyPlan: Double?
    var activityLevel: ActivityLevel?
    var caloricState : [caloricProgress]?
    var isMetric: Bool = false
}

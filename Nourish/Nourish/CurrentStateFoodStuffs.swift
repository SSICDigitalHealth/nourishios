//
//  CurrentStateFoodStuffs.swift
//  Nourish
//
//  Created by Vlad Birukov on 14.06.17.
//  Copyright © 2017 SSIC. All rights reserved.
//

import Foundation

class CurrentStateFoodStuffs : NSObject {
    var arrCurentStateFood = [StateFoodStuffsModel]()
}

class StateFoodStuffsModel {
    var type : Ocasion?
    var arrFoodElement : [FoodElementModel]?
}

class FoodElementModel {
    var nameFood : String = ""
    var amountFood : Double?
    var unit: String?
    
}

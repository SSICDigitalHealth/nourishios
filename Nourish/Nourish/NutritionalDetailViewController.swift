//
//  NutritionalDetailViewController.swift
//  Nourish
//
//  Created by Vlad Birukov on 19.06.17.
//  Copyright © 2017 SSIC. All rights reserved.
//

import UIKit

class NutritionalDetailViewController: BaseCardViewController {

    @IBOutlet weak var nutritionalDetailView: NutritionalDetailView!
    @IBOutlet weak var cardNavigationBar: NewCardNavigationBarView!



    override func viewDidLoad() {
        super.viewDidLoad()

        self.baseViews = [cardNavigationBar, nutritionalDetailView]
        
        for v in baseViews! {
            v.viewDidLoad()
        }
        
        nutritionalDetailView.nutritionalDetailViewController = self
        
        if let config = self.progressConfig {
            nutritionalDetailView.progressConfig = config
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        setUpNavigationBar(title: "Nutritional balance", style: .other, navigationBar: cardNavigationBar)
    
        super.viewWillAppear(animated)
    }
    

}

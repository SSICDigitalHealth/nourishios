//
//  NRHealthProgressInteractor.swift
//  Nourish
//
//  Created by Vino (Vinodha) Sundaramoorthy on 12/1/16.
//  Copyright © 2016 SSIC. All rights reserved.
//

import UIKit
import RxSwift
import HealthKit

let kDefaultSteps = 6000.0

class NRHealthProgressInteractor: NSObject {
    let mealRepo = MealHistoryRepository.shared
    let activityRepo = UserActivityRepository()
    let userRepository = UserRepository.shared
    
        func healthProgressFor(date : Date) -> Observable<NRHealthProgress> {
            return self.getProgressForToday()
        }
    
    func getProgressForToday() -> Observable<NRHealthProgress> {
        let score = mealRepo.getFoodScoreFor(date: Date())
        let sleep = activityRepo.getLastDaySleeping()
        let activity = activityRepo.getUserStepsForToday()
        let activeCals = activityRepo.getActiveCaloriesFor(period: .today)
        let basalCals = activityRepo.getBasalCaloriesFor(period: .today)
        let weight = activityRepo.getUserWeight()
        let height = activityRepo.getUserHeight()
        let user = userRepository.getCurrentUser(policy: .DefaultPolicy)
        
        let some = Observable.zip(score,sleep,activity,activeCals,basalCals,weight, height, user, resultSelector : {foodScore, sleepHours, stepsCount, activeCalories,basalCalorie, weightValue, heightValue, userProfile -> NRHealthProgress in
            let data = NRHealthProgress()
            let active = self.realActiveValueFrom(samples: activeCalories)
            var bmr = self.realActiveValueFrom(samples: basalCalorie)
            
            var weightValueToUse = 65.0
            var heightValueToUse = 1.65
            
            if userProfile.weight != nil {
                weightValueToUse = userProfile.weight!
            }
            
            if userProfile.height != nil {
                heightValueToUse = userProfile.height!
            }
            
            if weightValue != nil {
                weightValueToUse = weightValue!
            }
            
            if heightValue != nil {
                heightValueToUse = heightValue!
            }
            
            if bmr == 0 {
                bmr = userProfile.getBMRCalories(weight: weightValueToUse, height: heightValueToUse, date: Date(), isCurrentDay: true)
            }
            
            var activity = stepsCount / kDefaultSteps * 100
            
            if activity > 100 {
                activity = 100
            }
            
            var sleeping = sleepHours / 8 * 100
            
            if sleeping > 100 {
                sleeping = 100
            }
            data.activity = activity
            data.nutritionScore = foodScore[0].nutritionScore
            data.sleep = sleeping
            data.calories = active
            data.bmr = bmr//caloriesBurned - active
            data.steps = stepsCount
            return data
        })
        
        return some
    }
        
        private func generateRandomProgress() -> NRHealthProgress {
            let progress = NRHealthProgress()
            progress.nutritionScore = Double(self.generateRandomBetween(max: 100, min: 0))
            progress.sleep = Double(self.generateRandomBetween(max: 10, min: 0))
            progress.activity = Double(self.generateRandomBetween(max: 10000, min: 0))
            //progress.stress = Double(self.generateRandomBetween(max: 10, min: 0))
            return progress
        }
        
    private func generateRandomBetween(max : Int, min : Int) -> Int {
        return min + Int(arc4random_uniform(UInt32(max - min + 1)))
    }
    
    private func realActiveValueFrom(samples : [HKSample]) -> Double {
        var summary = 0.0
        for sample in samples {
            let value = self.caloriesFromSample(sample: sample)
            summary += value
        }
        return summary
    }
    
    private func caloriesFromSample(sample : HKSample) -> Double {
        let key = sample as! HKQuantitySample
        
        let startComps = Calendar.current.dateComponents([.weekday, .minute], from: sample.startDate)
        let endComps = Calendar.current.dateComponents([.weekday, .minute], from: sample.endDate)
        
        if startComps.weekday != endComps.weekday {
            let interval = sample.endDate.timeIntervalSince(sample.startDate)
            let startOfDay = Calendar.current.startOfDay(for: sample.endDate)
            let intervalStartOfDay = sample.endDate.timeIntervalSince(startOfDay)
            let koef = intervalStartOfDay / interval
            
            return key.quantity.doubleValue(for: HKUnit.kilocalorie()) * koef
        } else {
            return key.quantity.doubleValue(for: HKUnit.kilocalorie())
        }
    }
}

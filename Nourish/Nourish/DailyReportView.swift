//
//  DailyReportView.swift
//  Nourish
//
//  Created by Vino (Vinodha) Sundaramoorthy on 1/26/17.
//  Copyright © 2017 SSIC. All rights reserved.
//

import UIKit

class DailyReportView: BaseView , DailyReportProtocol,IAxisValueFormatter {
    @IBOutlet weak var scrollView : UIScrollView!
    @IBOutlet weak var stepsLabel : UILabel!
    @IBOutlet weak var caloriMeter : WeightLossMeterView!
    @IBOutlet weak var distLabel : UILabel!
    @IBOutlet weak var activityLabel : UILabel!
    @IBOutlet weak var exLabel : UILabel!
    @IBOutlet weak var sleepLabel : UILabel!
    @IBOutlet weak var distValueLabel : UILabel!
    @IBOutlet weak var stepsValueLabel : UILabel!
    @IBOutlet weak var exValueLabel : UILabel!
    @IBOutlet weak var sleepValueLabel : UILabel!
    @IBOutlet weak var activityChartView : BarChartView!
    @IBOutlet weak var tokenView : UIView!
    @IBOutlet weak var stepUnitLabel : UILabel!
    @IBOutlet weak var mileUnitLabel : UILabel!
    @IBOutlet weak var exUnitLabel : UILabel!
    @IBOutlet weak var sleepUnitLabel : UILabel!
    
    var reportModel : DailyReportModel!
    var reportDate : Date!
    var needsReload : Bool = true
    var controller : DailyReportViewController!
    var presenter = DailyReportPresenter()
    
    // MARK: Drawing methods
    override func viewWillAppear(_ animated: Bool) {
        self.basePresenter = presenter
        presenter.dailyView = self
        super.viewWillAppear(animated)
    }
    
    override func draw(_ rect: CGRect) {
        if needsReload {
            self.backgroundColor = NRColorUtility.nourishHomeColor()
            self.scrollView.backgroundColor = NRColorUtility.nourishHomeColor()
            self.scrollView.frame = CGRect(x: 0, y: 0, width: UIScreen.main.bounds.size.width, height: UIScreen.main.bounds.size.height)
            self.scrollView.contentSize = CGSize(width: UIScreen.main.bounds.size.width, height: 1200)
            
            if (reportModel != nil && reportModel.weightProgress != nil) {
                self.caloriMeter.setMeterModel(model: reportModel.weightProgress!, bgColor: NRColorUtility.nourishHomeColor(), isMetric: reportModel.isMetric,goal:reportModel.goal)
            }
            self.caloriMeter.drawShadow = false
            self.caloriMeter.backgroundColor = NRColorUtility.nourishHomeColor()
            let labelColor = NRColorUtility.reportLabelColor()
            self.stepsLabel.textColor = labelColor
            self.distLabel.textColor = labelColor
            self.activityLabel.textColor = labelColor
            self.distValueLabel.textColor = labelColor
            self.sleepLabel.textColor = labelColor
            self.exLabel.textColor = labelColor
            self.distValueLabel.textColor = labelColor
            self.exValueLabel.textColor = labelColor
            self.sleepValueLabel.textColor = labelColor
            self.stepsValueLabel.textColor = labelColor
            self.stepUnitLabel.textColor = labelColor
            self.mileUnitLabel.textColor = labelColor
            self.exUnitLabel.textColor = labelColor
            self.sleepUnitLabel.textColor = labelColor
            
            if reportModel != nil {
                self.stepsValueLabel.text = String(format:"%0.f",reportModel.steps)
                self.distValueLabel.text = String(format:"%0.f",reportModel.distance)
                self.exValueLabel.text = String(format:"%0.f",reportModel.activeHours)
                self.sleepValueLabel.text = String(format:"%0.f",reportModel.sleep)
                self.mileUnitLabel.text = reportModel.isMetric ? "Km" : "Miles"
                self.setUpBarChartView()
                self.setNutrientTokenView()
            }
        }
    }
    func setUpBarChartView() {
        activityChartView.backgroundColor = UIColor.white
        activityChartView.drawGridBackgroundEnabled = false
        activityChartView.chartDescription?.enabled = false;
        activityChartView.dragEnabled = false;
        activityChartView.setScaleEnabled(true)
        activityChartView.autoScaleMinMaxEnabled = false
        activityChartView.backgroundColor = UIColor.clear
        activityChartView.isUserInteractionEnabled = false
        activityChartView.legendRenderer.legend = nil
        activityChartView.drawRoundedBarEnabled = true
        
        // x Axis
        let xAxis : XAxis = activityChartView.xAxis
        xAxis.labelPosition = .bottom
        xAxis.drawGridLinesEnabled = false
        xAxis.granularity = 1
        xAxis.axisLineColor = UIColor.clear
        xAxis.valueFormatter = self
        xAxis.labelCount = 24
        
        // right axis
        let rightAxis = activityChartView.rightAxis
        rightAxis.enabled = false
        rightAxis.drawGridLinesEnabled = true
        
        // left Axis
        let leftAxis = activityChartView.leftAxis
        leftAxis.enabled = false
        
        activityChartView.data = self.setupBarChartData()
        activityChartView.animate(yAxisDuration: 1)
        activityChartView.backgroundColor = UIColor.white
        
    }
    
    func setupBarChartData() -> BarChartData {
        var yValues : [BarChartDataEntry] = []
        
        if self.reportModel.activityArray.count > 0 {
            for i in  0...self.reportModel.activityArray.count-1 {
                yValues.append(BarChartDataEntry(x:Double(i+1) , yValues: [self.reportModel.activityArray[i].activeHr]))
            }
        }
        
        
        var set : BarChartDataSet? = nil
        var data : BarChartData? = nil
        
        set = BarChartDataSet.init(values: yValues, label: "")
        set?.valueFont = UIFont.systemFont(ofSize: 7)
        set?.colors = [NRColorUtility.reportActivityColor()]
        set?.roundCornerRadius = 2
        data = BarChartData.init(dataSets: [set!])
        data?.barWidth = 0.5
        set?.drawValuesEnabled = false
        activityChartView.animate(yAxisDuration: 1)
        return data!
    }
    
    func setNutrientTokenView() {
        var yPositon : CGFloat = 5
        
        // Just Right
        let justRightTitle = UILabel(frame: CGRect(x: 15, y: yPositon, width: 100, height: 22))
        justRightTitle.text = "Just Right"
        justRightTitle.textColor = NRColorUtility.reportLabelColor()
        self.tokenView.addSubview(justRightTitle)
        yPositon = yPositon + justRightTitle.frame.height + 5
        
        // yPositon = self.addNutrientTokens(nut: self.reportModel.justRightNutrients, y: yPositon)
        
        let maxWidth = UIScreen.main.bounds.width - 30
        var currentXPosition : CGFloat = 15
        
        for n in reportModel.justRightNutrients! {
            let stringSize = n.name.sizeOfBoundingRect(width: maxWidth, height: 999, font: UIFont.systemFont(ofSize: 14))
            let tokenWidth = stringSize.width + 10
            
            if currentXPosition + tokenWidth > maxWidth {
                //Move x position and yPosition
                currentXPosition = 15
                yPositon = yPositon + 30
            }
            
            let tokenButton = UIButton(frame: CGRect(x: currentXPosition, y: yPositon, width: tokenWidth, height: 25))
            
            tokenButton.setTitle(n.name, for: UIControlState.normal)
            tokenButton.layer.cornerRadius = 2
            tokenButton.titleLabel?.font = UIFont.systemFont(ofSize: 14)
            tokenButton.setTitleColor(UIColor.white, for: .normal)
            
            tokenButton.backgroundColor = n.tokenColor()
            self.tokenView.addSubview(tokenButton)
            currentXPosition = currentXPosition + tokenWidth + 5
        }
        
        currentXPosition = 15
        yPositon = yPositon + 35
        let tooLittleTitle = UILabel(frame: CGRect(x: 15, y: yPositon, width: 100, height: 22))
        tooLittleTitle.text = "Too Little"
        tooLittleTitle.textColor = NRColorUtility.reportLabelColor()
        self.tokenView.addSubview(tooLittleTitle)
        
        yPositon = yPositon + tooLittleTitle.frame.height + 5
        yPositon = self.addNutrientTokens(nut: self.reportModel.tooLittleNutrients!, y: yPositon)
        
        yPositon = yPositon + 35
        let tooMuchTitle = UILabel(frame: CGRect(x: 15, y: yPositon, width: 100, height: 22))
        tooMuchTitle.text = "Too Much"
        tooMuchTitle.textColor = NRColorUtility.reportLabelColor()
        self.tokenView.addSubview(tooMuchTitle)
        yPositon = yPositon + tooMuchTitle.frame.height + 5
        
        yPositon = self.addNutrientTokens(nut: self.reportModel.tooMuchNutrients!, y: yPositon)
    }
    
    func addNutrientTokens(nut : [nutrients] , y: CGFloat) -> CGFloat{
        var yPos = y
        let maxWidth = UIScreen.main.bounds.width - 30
        var currentXPosition : CGFloat = 15
        
        for n in nut {
            let stringSize = n.name.sizeOfBoundingRect(width: maxWidth, height: 999, font: UIFont.systemFont(ofSize: 14))
            let tokenWidth = stringSize.width + 10
            
            if currentXPosition + tokenWidth > maxWidth {
                //Move x position and yPosition
                currentXPosition = 15
                yPos = yPos + 30
            }
            
            let tokenButton = UIButton(frame: CGRect(x: currentXPosition, y: yPos, width: tokenWidth, height: 25))
            tokenButton.setTitle(n.name, for: UIControlState.normal)
            tokenButton.layer.cornerRadius = 2
            tokenButton.titleLabel?.font = UIFont.systemFont(ofSize: 14)
            tokenButton.setTitleColor(UIColor.white, for: .normal)
            tokenButton.backgroundColor = n.tokenColor()
            
            self.tokenView.addSubview(tokenButton)
            currentXPosition = currentXPosition + tokenWidth + 5
        }
        return yPos
    }
    
    func setReport(report:DailyReportModel) {
        self.reportModel = report
        self.needsReload = true
        self.stopActivityAnimation()

        self.setNeedsDisplay()
    }
    
    func getReportDate() -> Date {
        return self.reportDate
    }
    
    // MARK: IAxisValueFormatter Implementation
    func stringForValue(_ value: Double, axis: AxisBase?) -> String {
        let index = Int(value) - 1
        var periodString = self.reportModel.activityArray[index].periodString
        if periodString == "0" {
            periodString = "12A"
        } else if Int(periodString)! > 12 {
            let period : Float = Float(periodString)!
            periodString = String(format:"%.0f",period.truncatingRemainder(dividingBy: 12))
        }
    
        return periodString
    }
    
}

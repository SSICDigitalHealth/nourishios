//
//  MealHistoryRepository.swift
//  Nourish
//
//  Created by Nova on 12/13/16.
//  Copyright © 2016 SSIC. All rights reserved.
//

import UIKit
import RxSwift
import RealmSwift


let kMonthlyPeriod = 7
let kWeeklyDailyPeriod = 1
let kNourishScore = "kNourishScore"
let kNourishScoreDate = "kNourishScoreDate"

final class MealHistoryRepository: NSObject {
    let bgFetcher = BackgroundChangeScoreFetcher.shared
    let foodDataStore = NourishDataStore.shared
    let userRepository = UserRepository.shared
    let mealCacheMapper = MealRecord_to_MealRecordCache()
    let toCacheMapper = Json_to_MealRecordCache()
    let toNutrientMapper = Json_to_nutrients()
    let toJsonMapper = foodSearchModel_to_json()

    let disposeBag = DisposeBag()
    
    //Score
    let scoreRepo = ScoreFeedbackRepository()
    let scoreJsonMapper = json_to_ScoreCacheRealm()
    
    static let shared = MealHistoryRepository()
    let mealCacheStore = MealDiaryDataStore()
    let operationRepo = OperationCacheRepository()
    
    let photoStore = ImageRealmDataStore()
    let mapper = MealRecord_to_MealRecordCache()
    let routine = NMFood_to_MealRecordRoutine()
    
    
    func filledOccasionsFor(date : Date) -> Observable<[Ocasion]> {
        let history = self.mealCacheStore.getFoodFor(date: date).flatMap { records -> Observable<[Ocasion]> in
            var result = [Ocasion]()
            for record in records {
                if !result.contains(record.occasionEnum) {
                    result.append(record.occasionEnum)
                }
            }
            return Observable.just(result)
        }
        return history
    }
    
    func caloriesFor(date : Date) -> Observable<Double> {
        let history = self.mealCacheStore.getFoodFor(date: date).flatMap { records -> Observable<Double> in
            var sum = 0.0
            
            for record in records {
                sum += record.calories
            }
            return Observable.just(sum)
        }
        return history
    }
    
    func getCacheWithChangesFor(date : Date) -> Observable<[MealRecordCache]> {
        return self.mealCacheStore.getUnSyncedRecordsFor(date: date)
    }
    
    func storeToCache(meal : MealRecord, occasion : Ocasion, date : Date) -> String {
        let model = self.mealCacheMapper.transform(record: meal)
        return self.mealCacheStore.addFoodForDate(date: date, meal: model, occasion: occasion)
    }
    
    func getFoodFor(date : Date,onlyCache : Bool) -> Observable<[MealRecordCache]> {
        return Observable.create { observer in
            let cache = self.mealCacheStore.getFoodFor(date: date)
            let operations = Observable.just(self.operationRepo.fetchRecordsFor(date: date))
            
            let _ = Observable.zip(cache, operations, resultSelector : { [weak self] cacheRecords, operationMerge -> [MealRecordCache] in
                
                var result = [MealRecordCache]()
                if let instance = self {
                    let records = instance.mergeCacheChanges(cache: cacheRecords, operations: operationMerge)
                    
                    result = Array.init(records)
                }
                return result
            }).subscribe(onNext: {results in
                if results.count > 0 {
                    if results.count == 1 {
                        let meal = results.first!
                        if meal.isInvalidated == false && meal.foodUid == kEmptyObjectID {
                            observer.onNext([])
                        } else {
                            observer.onNext(results)
                        }
                    } else {
                        observer.onNext(results)
                    }
                }
                
                if onlyCache == true {
                    observer.onCompleted()
                }
            }, onError: {error in }, onCompleted: {}, onDisposed: {})
            
            let _ = self.userRepository.getCurrentUser(policy: .DefaultPolicy).map { user in
                return user.userID
            }.flatMap { [weak self] userID -> Observable<Data> in
                let token = NRUserSession.sharedInstance.accessToken
                if let instance = self {
                    return instance.foodDataStore.getFoodForDate(date: date, userID: userID!, token: token!)
                } else {
                    return Observable.just(Data())
                }
                
            }.flatMap { [weak self] data -> Observable<[MealRecordCache]> in
                
                if let instance = self {
                    let array = instance.convertDataToDictionary(data: data)
                    
                    var mapped : [MealRecordCache] = []
                    
                    
                    if array != nil && (array?.count)! > 0 {
                        mapped = instance.mapDictionaryIntoObjects(array: array!)
                    }
                    
                    instance.mealCacheStore.storeFoodFor(date: date, array: mapped)
                    let operations = Observable.just(instance.operationRepo.fetchRecordsFor(date: date))
                    let cached = Observable.just(mapped)
                    
                    let zip = Observable.zip(cached, operations, resultSelector : { [weak self] cacheRecords, operationMerge -> [MealRecordCache] in
                        
                        var result = [MealRecordCache]()
                        var records = [MealRecordCache]()
                        if let instance = self {
                            records = instance.mergeCacheChanges(cache: cacheRecords, operations: operationMerge)
                            result = Array.init(records)
                        }
                      
                        
                        if records.count == 1 {
                            let meal = records.first!
                            if meal.foodUid == kEmptyObjectID {
                                result = []
                            }
                        }
                        return result
                    })
                    
                    return zip
                } else {
                    return Observable.just([MealRecordCache]())
                }
            }.subscribe(onNext : { result in
                observer.onNext(result)
                observer.onCompleted()
            }).addDisposableTo(self.disposeBag)
            
            return Disposables.create()
        }
    }
    
    func getRestMealDiaryFor(date : Date, userID : String, token : String) -> Observable<[MealRecordCache]> {

        return self.foodDataStore.getFoodForDate(date: date, userID: userID, token: token).flatMap { data -> Observable<[MealRecordCache]> in
                
                let array = self.convertDataToDictionary(data: data)
                
                var mapped : [MealRecordCache] = []
                
                
                if array != nil && (array?.count)! > 0 {
                    mapped = self.mapDictionaryIntoObjects(array: array!)
                }
    
            return Observable.just(mapped)
        }
    }
    
    func fetchObjectFor(stringID : String) -> MealRecordCache? {
        return self.mealCacheStore.getFoodObjectFor(foodID: stringID)
    }
    
    func mergeCacheChanges(cache : [MealRecordCache], operations : [OperationCache]) -> [MealRecordCache] {
        let realm = try! Realm()
        var resultArray = Array.init(cache)
        
//        for object in cache {
//            try! realm.write {
//                object.isCreated = false
//                object.hasChanges = false
//            }
//        }
        
        let emptyChecker = resultArray.contains {$0.foodUid == kEmptyObjectID}
        
        
        if emptyChecker == true {
            let obj = resultArray.filter {$0.foodUid == kEmptyObjectID}
            if obj.first != nil {
                /*if let index = resultArray.index(of: result) {
                    //resultArray.remove(at: index)
                }*/
            }
            
        }
        
        
        for operation in operations {
            if operation.isInvalidated == false {
                let operationType = operation.operationTypeEnum
                let userPhotoId = operation.userPhotoID
                let foodGroupId = operation.foodGroupID
                let operationDict = operation.dictionary
                if operation.operationTypeEnum == .create {
                    if operation.userFoodID == nil {
                        if let mealModel = self.mealCacheStore.getFoodObjectFor(foodID: operation.affectedIDs.first!) {
                            try! Realm().write {
                                mealModel.hasChanges = true
                                mealModel.isCreated = true
                            }
                            resultArray.append(mealModel)
                        }
                    }
                }
                
                if operationType == .update {
                    
                    if let ident = operation.userFoodID ?? operation.affectedIDs.first {
                        if let mealModel = self.fetchRecordFrom(cache: resultArray, idString: ident) {
                                    if mealModel.isInvalidated == false {
                                        try! realm.write {
                                            let dictionary = operationDict
                                            
                                            if let grams = dictionary["grams"] as? Double {
                                                mealModel.grams = grams
                                            }
                                            
                                            if let amount = dictionary["amount"] as? Double {
                                                mealModel.amount = amount
                                                mealModel.msreAmount = amount
                                            }
                                            
                                            if let unit = dictionary["unit"] as? String {
                                                mealModel.unit = unit
                                                mealModel.msreDesc = unit
                                            }
                                            
                                            if let calories = dictionary["calories"] as? String {
                                                mealModel.calories = Double(calories) ?? 0.0
                                                mealModel.kCal = Double(calories) ?? 0.0
                                            }
                                            
                                            if let photoID = dictionary["user_photo_id"] as? String {
                                                mealModel.userPhotoId = photoID
                                            }
                                            
                                            if let foodID = dictionary["id"] as? String {
                                                mealModel.userFoodId = foodID
                                            }
                                            
                                            mealModel.hasChanges = true
                                            mealModel.isCreated = true
                                            
                                        }
                                    }
                        }
                    }
                }
                
                if operationType == .delete {
                    var copy = Array(resultArray)
                    if operation.affectedIDs.count == 1 {
                        if let ident = operation.userFoodID ?? operation.affectedIDs.first {
                            if let mealModel = self.fetchRecordFrom(cache: copy, idString: ident) {
                                if let index = copy.index(of: mealModel) {
                                    copy.remove(at: index)
                                }
                            }
                        }
                    } else {
                        let ids = operation.affectedIDs
                        for obj in ids {
                            if let mealModel = self.fetchRecordFrom(cache: copy, idString: obj) {
                                if let index = copy.index(of: mealModel) {
                                    copy.remove(at: index)
                                }
                            }
                        }
                    }
                    
                    resultArray = copy
                }
                
                if operationType == .merge {
                    let dict = operation.dictionary
                    let userIDs = dict[kBackendIDKey] as! [String]
                    let cacheIDs = dict[kCacheIDKey] as! [String]
                    let name = dict[kGroupNameKey] as! String
                    
                    let mergedArray = userIDs + cacheIDs
                    let uid = "local" + UUID().uuidString
                    for string in mergedArray {
                        
                        if let record = self.fetchRecordFrom(cache: resultArray, idString: string) {
                            if record.groupFoodUUID == nil && record.groupID == nil {
                                try! realm.write {
                                    record.groupName = name
                                    record.groupFoodUUID = uid
                                    record.groupID = uid
                                    record.isGroupFavourite = mergedArray.count > 1
                                    record.isFavourite = mergedArray.count == 1
                                    record.numberOfItems = mergedArray.count
                                }
                            }
                        }
                    }
                }
                
                if operationType == .createGroup {
                    let realm = try! Realm()
                    var proxyArray = [MealRecordCache]()
                    let groupID = operation.foodGroupID
                    let counter = operation.affectedIDs.count
                    
                    let name = operation.dictionary["name"] as? String
                    for object in operation.affectedIDs {
                        if let object = self.mealCacheStore.getFoodObjectFor(foodID: object) {
                            if realm.isInWriteTransaction == false {
                                realm.beginWrite()
                                object.groupID = groupID
                                object.groupFoodUUID = groupID
                                object.groupName = name
                                object.isGroupFavourite = false
                                object.numberOfItems = counter
                                try! realm.commitWrite()
                                proxyArray.append(object)
                            }
                                
                        }
                        
                    }
                    resultArray.append(contentsOf: proxyArray)
                }
                
                if operationType == .addPhoto {
                    let realm = try! Realm()
                    let ident = operation.userFoodID ?? operation.affectedIDs.first
                    
                    if ident != nil {
                         try! realm.write {
                            if let record = self.fetchRecordFrom(cache: resultArray, idString: ident!) {
                                record.hasChanges = true
                                record.isCreated = true
                                record.userPhotoId = userPhotoId
                            }
                        }
                    }
                    
                    if let groupID = foodGroupId {
                        var records = [MealRecordCache]()
                        for obf in resultArray {
                            print(obf.isInvalidated)
                            if obf.isInvalidated == false && obf.groupID == groupID {
                                records.append(obf)
                            }
                        }
                        //let records = resultArray.filter {$0.groupID == groupID && $0.isInvalidated == false}
                        try! realm.write {
                            for object in records {
                                if object.isInvalidated == false {
                                    object.groupPhotoID = userPhotoId
                                }
                            }
                        }
                    }
                }
            }

        }
        
        return resultArray
    }
    
    func fetchRecordFrom(cache : [MealRecordCache], idString : String) -> MealRecordCache? {
        if idString.hasPrefix("local") {
            return cache.filter {$0.isInvalidated == false && $0.localCacheID == idString}.first
        } else {
            return cache.filter {$0.isInvalidated == false && $0.userFoodId == idString}.first
        }
    }
    
    func mergeCacheChanges(cache : [MealRecordCache], origin : [MealRecordCache], date : Date) -> [MealRecordCache] {
        var result = [MealRecordCache]()
        
        let changes = cache.filter {$0.hasChanges == true}
        result.append(contentsOf: origin)
        
        for object in changes {
            if object.isCreated == true {
                result.append(object)
            } else if object.isDeleted == true && object.hasChanges == true {
                let meal = result.filter {$0.idString == object.idString && $0.calories == object.calories && $0.ocasion == $0.ocasion}.first
                if meal != nil {
                    if let index = result.index(of:meal!) {
                        result.remove(at: index)
                    }
                }
            } else {
                let meal = result.filter {$0.userFoodId == object.userFoodId}.first
                if meal != nil {
                    meal?.amount = object.amount
                    meal?.grams = object.grams
                    meal?.userPhotoId = object.userPhotoId
                    meal?.calories = object.calories
                }
            }
        }
        
        return result
        
    }
    
    func deleteFood(foodID : [String],date:Date,saveChat:Bool) -> Observable<Double> {
        
        /*
        let model = self.mealCacheStore.getFoodObjectFor(foodID: foodID)
        
        if model != nil {
            try! Realm().write {
                model?.isDeleted = true
                model?.hasChanges = true
            }
        }
        */
        if let ident = foodID.first {
            let realm = try! Realm()
            if let object = self.mealCacheStore.getFoodObjectFor(foodID: ident) {
                let objectId = object.userFoodId
                try! realm.write {
                    let operation = OperationCache()
                    operation.affectedIDs = foodID
                    operation.userFoodID = objectId
                    operation.operationTypeEnum = .delete
                    operation.timeStamp = Date()
                    operation.date = date
                    operation.counter = 0
                    operation.userID = NRUserSession.sharedInstance.userID
                    realm.add(operation)
                }
                let int = BackgroundMealDiarySyncInteractor.shared
                int.testFetch()
            }
        }
        
        /*
        let realmStore = ChatBoxRealmDataStore()
        realmStore.deleteMessagesFor(idString: foodID)
        
        let deletion = self.userRepository.getCurrentUser(policy: .DefaultPolicy).flatMap { user -> Observable<String> in
            return Observable.just(user.userID!)
            }
            .flatMap { userId -> Observable<Double> in
                let token = NRUserSession.sharedInstance.accessToken
                let object = self.foodDataStore.changeDeleteFood(dictionary: nil, token: token!, userID: userId, userFoodId: foodID).flatMap { data -> Observable<Double> in
                    let dict = self.convertToSimpleDict(data: data)
                    let score = (dict?["score_after"] as? Double) ?? 0.0
                    let unixTime = dict?["datetime"] as? Double
                   // if unixTime != nil {
                    var dateNow : Date!
                        if unixTime != nil {
                            dateNow = self.dateFromUnixStamp(time: unixTime!)
                        } else {
                            dateNow = date
                        }
                        if Calendar.current.startOfDay(for: dateNow) == Calendar.current.startOfDay(for: Date()) {
                            self.storeCurrentScore(date: Date(), value: score)
                            if (saveChat) {
                                self.storeChatMessagesForChangeFood(data: data)
                            }
                        }
                   // }
             
                    self.bgFetcher.execute()
                    self.mealCacheStore.deleteObjectFor(foodID: foodID)
                    return Observable.just(score)
                }
                return object
        }
        */
        return Observable.just(0.0)
    }
    
    func changeMealRecord(foodsearcMeal : foodSearchModel) -> Observable<Double> {
        let fetchID = foodsearcMeal.foodBackendID ?? foodsearcMeal.localCacheID
        
        let meal = self.mealCacheStore.getFoodObjectFor(foodID: fetchID!)
        
        /*
        try! Realm().write {
            meal?.amount = foodsearcMeal.amount
            meal?.grams = foodsearcMeal.grams ?? 0.0
            meal?.unit = foodsearcMeal.unit
            meal?.calories = Double(foodsearcMeal.calories) ?? 0.0
            meal?.userPhotoId = foodsearcMeal.userPhotoId
            meal?.hasChanges = true
        }
        */
        if meal != nil {
            let realm = try! Realm()
            
            try! realm.write {
                let operation = OperationCache()
                operation.timeStamp = Date()
                operation.counter = 0
                operation.operationTypeEnum = .update
                let mapper = foodSearchModel_to_json()
                let dict = mapper.transform(model: foodsearcMeal)
                operation.userID = NRUserSession.sharedInstance.userID
                operation.affectedIDs = [foodsearcMeal.localCacheID!]
                operation.userFoodID = foodsearcMeal.foodBackendID
                operation.dictionary = dict
                realm.add(operation)
            }
        }
        
        let int = BackgroundMealDiarySyncInteractor.shared
        int.testFetch()
        return Observable.just(0.0)
    }
    
    func fetchNutrientDetailsForRecomFood(foodModel : foodSearchModel) -> Observable<[Nutrient]> {
        let user = self.userRepository.getCurrentUser(policy: .DefaultPolicy).flatMap {
            userProfile in
            Observable.just(userProfile.userID)
            }.flatMap {userID -> Observable<Data> in
            let token = NRUserSession.sharedInstance.accessToken
                let grams = foodModel.grams != nil ? foodModel.grams : 0.0
                let stringToParse = foodModel.mealIdString != "" ? foodModel.mealIdString : foodModel.foodId
                let escapedString = stringToParse?.addingPercentEncoding(withAllowedCharacters: .urlPathAllowed)
            return self.foodDataStore.fetchNutritionScoreForRecFood(token: token!, userID: userID!, foodID: escapedString!, foodWeight: grams!)
            }.flatMap { data -> Observable<[Nutrient]> in
                let array = self.jsonDataToNutrients(data: data)
                return Observable.just(array)
        }
        return user
    }
    
    func fetchFoodScoreCacheForDate(date : Date) -> Observable<Double?> {
        return Observable.create {observer in
            let cache = NutritionCacheDataStore()
            let data = cache.fetchLastCache(date: date)
            
            if data != nil {
                let array = self.convertDataToDictionary(data: data!)
                if array != nil {
                    if let value = self.mapDictionaryIntoNutrients(array: array!).first {
                        observer.onNext(value.nutritionScore)
                    }
                }
            }
            observer.onNext(nil)
            return Disposables.create()
        }
        
    }
    /*
    func getFoodForte(date : Date) -> Observable<[MealRecordCache]> {
        let _ = self.userRepository.getCurrentUser(policy: .DefaultPolicy).flatMap { userProfile in
            let token = NRUserSession.sharedInstance.accessToken
            let userID = userProfile.userID
            
        }
    }
    */
    func getFoodScoreFor(date : Date) -> Observable<[Nutrition]> {
        return Observable.create { observer in
            
            let cache = NutritionCacheDataStore()
            let data = cache.fetchLastCache(date: date)
            
            if data != nil {
                let array = self.convertDataToDictionary(data: data!)
                if array != nil {
                    observer.onNext(self.mapDictionaryIntoNutrients(array: array!))
                }
            }
            
            
            var userId = ""
            let _ = self.userRepository.getCurrentUser(policy: .DefaultPolicy).subscribe(onNext : { [unowned self] user in
                userId = user.userID!
                let token = NRUserSession.sharedInstance.accessToken!
                let _ = self.foodDataStore.getFoodScore(date: date, userID: userId, token: token).subscribe(onNext :{ [unowned self] data in
                    let array = self.convertDataToDictionary(data: data)
                    if array != nil {
                        cache.storeCache(cache: data, date: date)
                        observer.onNext(self.mapDictionaryIntoNutrients(array: array!))
                        observer.onCompleted()
                    } else {
                        let nutr = Nutrition()
                        nutr.minerals = 0.0
                        nutr.electrolytes = 0.0
                        nutr.macronutrients = 0.0
                        nutr.nutritionScore = 0.0
                        nutr.vitamins = 0.0
                        observer.onNext([nutr])
                        observer.onCompleted()
                    }
                })
            })
            return Disposables.create()
        }
    }
    
    func getFoodScoreForePeriod(period : period) -> Observable<[Nutrition]> {
        let some = self.userRepository.getCurrentUser(policy: .DefaultPolicy).flatMap {user -> Observable<String> in
                return Observable.just(user.userID!)
            }.flatMap { userId -> Observable<[Nutrition]> in
                let token = NRUserSession.sharedInstance.accessToken!
                let startDate = Calendar.current.startOfDay(for: Date())
                var components = DateComponents()
                components.day = 1
                components.second = -1
                let endDate: Date = Calendar.current.date(byAdding: components, to: startDate)!
                
                var range = kWeeklyDailyPeriod
                if period == .monthly {
                    range = kMonthlyPeriod
                }
                
                let object = self.foodDataStore.getFoodScore(startDate: self.startDateFrom(periods: period), endDate: endDate, userID: userId, token: token, range : range).flatMap { userData -> Observable<[Nutrition]> in
                    let array = self.convertDataToDictionary(data: userData)
                    return Observable.just(self.mapDictionaryIntoNutrients(array: array!))
                }
                return object
            }
        return some
    }
    
    func getCaloriesStatsFor(period : period) -> Observable<[CaloriesStats]> {
        let returnValue = self.userRepository.getCurrentUser(policy: .Cached).flatMap { user in
            Observable.just(user.userID!)
            }.flatMap {userId -> Observable<Data> in
            let startDate = Calendar.current.startOfDay(for: Date())
            let token = NRUserSession.sharedInstance.accessToken!
            var components = DateComponents()
            components.day = 1
            components.second = -1
            let endDate: Date = Calendar.current.date(byAdding: components, to: startDate)!
            return self.foodDataStore.getCaloriesStats(startDate: self.startDateFrom(periods: period), endDate: endDate, token: token, userId: userId, range: self.rangeFrom(period: period))
            }.flatMap { data in
            Observable.just(self.calorieStatsFrom(data: data))
        }
        return returnValue
    }
    
    func getCaloriesStatsFor(date : Date) -> Observable<[CaloriesStats]> {
        print("Date for calories \(date)")
        let returnValue = self.userRepository.getCurrentUser(policy: .Cached).flatMap { user in
            Observable.just(user.userID!)
            }.flatMap {userId -> Observable<Data> in
                let startDate = Calendar.current.startOfDay(for: date)
                let token = NRUserSession.sharedInstance.accessToken!
                var components = DateComponents()
                components.day = 1
                components.second = -1
                let endDate: Date = Calendar.current.date(byAdding: components, to: startDate)!
                return self.foodDataStore.getCaloriesStats(startDate: startDate, endDate: endDate, token: token, userId: userId, range: self.rangeFrom(period: .today))
            }.flatMap { data in
                Observable.just(self.calorieStatsFrom(data: data))
        }
        return returnValue
    }
    
    private func calorieStatsFrom(data : Data) -> [CaloriesStats] {
        var array : [CaloriesStats] = []
        let dict = self.convertDataToDictionary(data: data)
        
        if dict != nil {
            for object in dict! {
                array.append(self.calorieStatFrom(dict: object))
            }
        }
        
        
        return array
    }
    
    private func calorieStatFrom(dict : [String : Any]) -> CaloriesStats {
        let model = CaloriesStats()
        print("dict is \(dict)")
        let brackeCalories = self.breakDownCaloriesFrom(dict: dict["breakdown_kcals"] as! [String : Any])
        model.score = dict["score"] as? Double
        model.eerCal = dict["eer_kcals"] as? Double
        model.consumedCal = dict["consumed_kcals"] as? Double
        model.day = self.dateFromUnixStamp(time: dict["day"] as! Double)
        model.breakDownCals = brackeCalories
        return model
    }
    
    private func breakDownCaloriesFrom(dict : [String : Any]) -> BreakDownCalories {
        let calories = BreakDownCalories()
        calories.breakfastCals = dict["breakfast"] as? Double
        calories.dinnerCals = dict["dinner"] as? Double
        calories.lunchCals = dict["lunch"] as? Double
        calories.snacksCals = dict["snack"] as? Double
        calories.unknownCals = dict["unknown"] as? Double
        return calories
    }
    
    func dateFromUnixStamp(time : Double) -> Date {
        let interval = time / 1000
        let date = Date(timeIntervalSince1970 : interval)
        return date
    }
    
    private func rangeFrom(period : period) -> Int {
        if period != .monthly {
            return kWeeklyDailyPeriod
        } else {
            return kMonthlyPeriod
        }
    }
    
    private func startDateFrom(periods : period) -> Date {
        return period.startDateFor(period: periods)
    }
    
    private func mapDictionaryIntoObjects(array : [[String : Any]]) -> [MealRecordCache] {
        return toCacheMapper.transform(array: array)
    }
    
    private func mapDictionaryIntoNutrients(array : [[String : Any]]) -> [Nutrition] {
        var arrayToReturn : [Nutrition] = []
        if array.count > 0 {
            for object in array {
                let nutr = Nutrition()
                
                if object["group_scores"] != nil {
                    let nutritionDict = object["group_scores"] as! [String : Any]
                    let score = object["score"] as! Double
                    
                    let interval = object["day"] as! Double
                    nutr.date = self.dateFromUnixStamp(time: interval)
                    
                    let todayStart = Calendar.current.startOfDay(for: Date())
                    if Calendar.current.startOfDay(for: nutr.date!) == todayStart {
                        self.storeCurrentScore(date: Date(), value: score)
                    }
                    
                    nutr.minerals = nutritionDict["Minerals"] != nil ? nutritionDict["Minerals"] as! Double : 0.0
                    nutr.electrolytes = nutritionDict["Electrolytes"] != nil ? nutritionDict["Electrolytes"] as! Double : 0.0
                    nutr.vitamins = nutritionDict["Vitamins"] != nil ? nutritionDict["Vitamins"] as! Double : 0.0
                    nutr.macronutrients = nutritionDict["Macronutrients"] != nil ? nutritionDict["Macronutrients"] as! Double : 0.0
                    nutr.nutritionScore = score * 100
                    nutr.nutrArray = self.toNutrientMapper.transfrom(array: object["nutrients"] as! [[String : Any]])
                }
                arrayToReturn.append(nutr)
            }
            
        }
        return arrayToReturn
    }
    
    func storeCurrentScore(date : Date, value : Double) {
        let defaults = UserDefaults.standard
        defaults.setValue(value, forKey: kNourishScore)
        defaults.setValue(date, forKey: kNourishScoreDate)
        defaults.synchronize()
    }
    
    func scoreForDate(date : Date) -> Double {
        var doubleValue = UserDefaults.standard.value(forKey: kNourishScore) as? Double
        
        if doubleValue == nil {
            doubleValue = 0.0
        }
        
        let startDate = Calendar.current.startOfDay(for: date)
        var comps = DateComponents()
        comps.day = 1
        comps.second = -1
        
        let endDate = Calendar.current.date(byAdding: comps, to: startDate)
        
        let saveDate = UserDefaults.standard.value(forKey: kNourishScoreDate) as? Date
        
        if saveDate != nil {
            if saveDate! > startDate && saveDate! < endDate! {
                return doubleValue! * 100
            } else {
                return 0.0
            }
        } else {
            return 0.0
        }
        
    }
    
    func removeScoreValues() {
        UserDefaults.standard.removeObject(forKey: kNourishScore)
        UserDefaults.standard.removeObject(forKey: kNourishScoreDate)
        UserDefaults.standard.synchronize()
    }
    
    private func jsonDataToNutrients(data : Data) -> [Nutrient] {
        
        var dictionary : [String : Any]? = nil
        do {
            dictionary = try JSONSerialization.jsonObject(with: data, options: []) as? [String : Any]
        } catch let error as NSError {
            print(error)
        }
        
        var nutrArray : [Nutrient] = []
        
        if dictionary != nil  && dictionary?["nutrients"] != nil {
            let arrayToParse = dictionary!["nutrients"]
            if arrayToParse != nil {
                for dict in (arrayToParse as? [[String : Any]])!  {
                    let nutr = Nutrient()
                    nutr.name = dict["nutrient"] as! String
                    nutr.representationValue = dict["amount"] as! Double
                    nutr.unit = dict["unit"] as! String
                    nutrArray.append(nutr)
                }
            }
        }

        return nutrArray
    }
/*
    private func mapJsonIntoNutrients(array : [[String : Any]]) -> [nutrients] {
        for object in array {
            let flag = object["flag"]
            let value = object["amount"] as! Double
            var maxValue = 0.0
            var minValue = 0.0
            var quantityString = ""
            
            if object["lhr"] != nil && object["uhr"] != nil {
                maxValue = object["uhr"] as! Double
                minValue = object["lhr"] as! Double
                
                if value < minValue {
                    quantityString = kQuantityLow
                } else if value > maxValue {
                    quantityString = kQuantityHigh
                } else {
                    quantityString = kQuantityNormal
                }
                
            } else if object["lhr"] == nil {
                maxValue = object["uhr"] as! Double
                
                if value == 0 {
                    quantityString = kQuantityLow
                } else if value <= maxValue {
                    quantityString = kQuantityNormal
                } else {
                    quantityString = kQuantityHigh
                }
                minValue = 0
            } else { //only minimal
                maxValue = object["lhr"] as! Double
                
                if value < maxValue {
                    quantityString = kQuantityLow
                }
                
                if value >= maxValue {
                    quantityString = kQuantityNormal
                }
                minValue = 0
            }
        }
        
        return [nutrients]
    }
 */
    
    private func convertDataToDictionary(data: Data) -> [[String : Any]]? {
        do {
            return try JSONSerialization.jsonObject(with: data, options: []) as? [[String : Any]]
        } catch let error as NSError {
            LogUtility.logToFile(error)
        }
        return nil
    }
    
    private func convertToSimpleDict(data : Data) -> [String : Any]? {
        do {
            return try JSONSerialization.jsonObject(with: data, options: []) as? [String : Any]
        } catch let error as NSError {
            LogUtility.logToFile(error)
        }
        return nil
    }
    
    private func uploadMealVoice(mealRecord : MealRecord, userID : String) -> Observable<Data> {
        
        if mealRecord.nutrientsFromNoom?[0] != nil {
            if (mealRecord.nutrientsFromNoom?[0])!.name == calorieString {
                var nutrs = mealRecord.nutrientsFromNoom
                nutrs?.remove(at: 0)
                mealRecord.nutrientsFromNoom = nutrs
            }
        }
        let timeStamp = mealRecord.date ?? Date()
        
        DispatchQueue.main.async {
            let realm = try! Realm()
            let cacheID = self.storeToCache(meal: mealRecord, occasion: mealRecord.ocasion, date: timeStamp)
            
            try! realm.write {
                let operation = OperationCache()
                operation.counter = 0
                operation.userID = userID
                operation.operationTypeEnum = .createVoice
                operation.userID = NRUserSession.sharedInstance.userID
                operation.timeStamp = Date()
                operation.affectedIDs = [cacheID]
                realm.add(operation)
            }
            
            let eatenChatMessage = ChatDataBaseItem()
            eatenChatMessage.chatBoxType = .typeDish
            eatenChatMessage.date = timeStamp
            eatenChatMessage.content = mealRecord.name!
            eatenChatMessage.isFromUser = true
            eatenChatMessage.messageID = ""
            eatenChatMessage.userID = NRUserSession.sharedInstance.userID ?? ""
            
            let chatStore = ChatBoxRealmDataStore()
            chatStore.storeRecordsArray(array: [eatenChatMessage])
            
            let int = BackgroundMealDiarySyncInteractor.shared
            int.testFetch()
        }
        
        return Observable.just(Data())
    }
    
    private func uploadMealRaw(mealRecord : MealRecord, userID : String) -> Observable<Data> {
        
        if mealRecord.nutrientsFromNoom?[0] != nil {
            if (mealRecord.nutrientsFromNoom?[0])!.name == calorieString {
                var nutrs = mealRecord.nutrientsFromNoom
                nutrs?.remove(at: 0)
                mealRecord.nutrientsFromNoom = nutrs
            }
        }
        let timeStamp = mealRecord.date ?? Date()
        
        DispatchQueue.main.async {
            let realm = try! Realm()
            let cacheID = self.storeToCache(meal: mealRecord, occasion: mealRecord.ocasion, date: timeStamp)
            
            try! realm.write {
                let operation = OperationCache()
                operation.counter = 0
                operation.userID = userID
                operation.operationTypeEnum = .create
                operation.date = timeStamp
                operation.userID = NRUserSession.sharedInstance.userID
                operation.timeStamp = Date()
                operation.affectedIDs = [cacheID]
                realm.add(operation)
            }
            
            let eatenChatMessage = ChatDataBaseItem()
            eatenChatMessage.chatBoxType = .typeDish
            eatenChatMessage.date = timeStamp
            eatenChatMessage.content = mealRecord.name!
            eatenChatMessage.isFromUser = true
            eatenChatMessage.messageID = ""
            eatenChatMessage.userID = NRUserSession.sharedInstance.userID ?? ""
            
            let chatStore = ChatBoxRealmDataStore()
            chatStore.storeRecordsArray(array: [eatenChatMessage])
            
            let int = BackgroundMealDiarySyncInteractor.shared
            int.testFetch()
        }

        return Observable.just(Data())
    }
    
    func uploadGroup(foods : [foodSearchModel], photo : UIImage, ocasion : Ocasion, date : Date) -> Observable<Bool> {
        
        let realm = try! Realm()
        
        var name : String = ""
        var cache : [foodSearchModel] = []
        
        for food in foods {
            var foodCache = food
            foodCache.occasion = ocasion
            cache.append(foodCache)
            name = name + food.foodTitle + ","
        }
        
        name = String(name.dropLast())
        var stampe = Date()
        
        var array = [MealRecord]()
        
         let localUID = "local" + UUID().uuidString
        
        for food in foods {
            let obj = self.routine.recomendationFoodToMeal(model: food, date: date ,occasion : ocasion)
            obj.groupID = localUID
            obj.groupFoodUUID = localUID
            array.append(obj)
        }
        
        var idsArray = [String]()
        
        
        for object in array {
            let timeStamp = object.date ?? Date()
            stampe = timeStamp
            idsArray.append(self.storeToCache(meal: object, occasion: ocasion, date: timeStamp))
        }
        
        let proxyStamp = Date()
        let localPhotoID = self.photoStore.store(image: photo)
        
        var dict = [String : Any]()
        
        dict["ocasion"] = Ocasion.stringRepresentation(servingType: ocasion)
        //dict["date"] = date
        dict["name"] = name
        
       
        
        try! realm.write {
            let operation = OperationCache()
            operation.counter = 0
            operation.date = date
            operation.userID = NRUserSession.sharedInstance.userID ?? ""
            operation.operationTypeEnum = .createGroup
            operation.timeStamp = proxyStamp
            operation.affectedIDs = idsArray
            operation.dictionary = dict
            operation.userPhotoID = localPhotoID
            operation.foodGroupID = localUID
            realm.add(operation)
            
            
            let operationPhoto = OperationCache()
            operationPhoto.counter = 0
            operationPhoto.date = date.addingTimeInterval(0.01)
            operationPhoto.foodGroupID = localUID
            operationPhoto.userID = NRUserSession.sharedInstance.userID ?? ""
            operationPhoto.operationTypeEnum = .addPhoto
            operationPhoto.timeStamp = proxyStamp.addingTimeInterval(0.01)
            operationPhoto.affectedIDs = idsArray
            operationPhoto.userPhotoID = localPhotoID
            
            realm.add(operationPhoto)
        }
        
        let eatenChatMessage = ChatDataBaseItem()
        eatenChatMessage.chatBoxType = .typeDish
        eatenChatMessage.date = stampe
        eatenChatMessage.content = name
        eatenChatMessage.isFromUser = true
        eatenChatMessage.messageID = ""
        eatenChatMessage.userID = NRUserSession.sharedInstance.userID ?? ""
        eatenChatMessage.photoID = localPhotoID
        
        let chatStore = ChatBoxRealmDataStore()
        chatStore.storeRecordsArray(array: [eatenChatMessage])
        
        let syncer = BackgroundMealDiarySyncInteractor.shared
        syncer.testFetch()
        
        return Observable.just(true)
    }
    
    func upload(mealRecord: MealRecord, photo : UIImage) -> Observable<Bool> {
        
        if mealRecord.nutrientsFromNoom?[0] != nil {
            if (mealRecord.nutrientsFromNoom?[0])!.name == calorieString {
                var nutrs = mealRecord.nutrientsFromNoom
                nutrs?.remove(at: 0)
                mealRecord.nutrientsFromNoom = nutrs
            }
        }
        let timeStamp = mealRecord.date ?? Date()
        let userID = NRUserSession.sharedInstance.userID
        
        let realm = try! Realm()
        let cacheID = self.storeToCache(meal: mealRecord, occasion: mealRecord.ocasion, date: timeStamp)
        
        let proxyStamp = Date()
        let localPhotoID = self.photoStore.store(image: photo)
        
        try! realm.write {
            let operation = OperationCache()
            operation.counter = 0
            operation.userID = userID
            operation.operationTypeEnum = .create
            operation.timeStamp = proxyStamp
            operation.affectedIDs = [cacheID]
            realm.add(operation)
            
            let operationPhoto = OperationCache()
            operationPhoto.counter = 0
            operationPhoto.userID = userID
            operationPhoto.operationTypeEnum = .addPhoto
            operationPhoto.timeStamp = proxyStamp.addingTimeInterval(0.01)
            operationPhoto.affectedIDs = [cacheID]
            operationPhoto.userPhotoID = localPhotoID
            
            realm.add(operationPhoto)
        }
        
        let eatenChatMessage = ChatDataBaseItem()
        eatenChatMessage.chatBoxType = .typeDish
        eatenChatMessage.date = timeStamp
        eatenChatMessage.content = mealRecord.name!
        eatenChatMessage.isFromUser = true
        eatenChatMessage.messageID = ""
        eatenChatMessage.userID = NRUserSession.sharedInstance.userID ?? ""
        eatenChatMessage.photoID = localPhotoID
        
        let chatStore = ChatBoxRealmDataStore()
        chatStore.storeRecordsArray(array: [eatenChatMessage])
        
        let syncer = BackgroundMealDiarySyncInteractor.shared
        syncer.testFetch()
        
        return Observable.just(true)
    }
    
    func oldUploadMeal(mealRecord : MealRecord) -> Observable<String> {
        let user = self.userRepository.getCurrentUser(policy: .DefaultPolicy).flatMap({userProfile in
            Observable.just(userProfile.userID!)
        }).flatMap({ userId -> Observable<Data> in
            //let model = self.mealCacheStore.fetchFor(idArray: [cacheID]).first
            let mapper = MealRecord_to_MealRecordCache()
            let model = mapper.transform(record: mealRecord)
            return self.foodDataStore.uploadMealRecord(mealRecord: model, userID: userId, token: NRUserSession.sharedInstance.accessToken!)
            
        }).flatMap { data -> Observable<String> in
            return self.getFoodIDFrom(data: data)
        }
        return user
    }
    
    
    func uploadMealVoice(mealRecord : MealRecord) -> Observable<Double>  {
        let flow = self.userRepository.getCurrentUser(policy: .Cached).flatMap { user -> Observable<String> in
            return Observable.just(user.userID!)
            }.flatMap {userID -> Observable<Data> in
                return self.uploadMealVoice(mealRecord: mealRecord, userID: userID)
            }.flatMap {data -> Observable<Double> in
                //self.storeChatMessages(meal: mealRecord, data: data)
                return Observable.just(0.0)
        }
        return flow
    }
    
    func uploadMeal(mealRecord : MealRecord) -> Observable<Double>  {
        let flow = self.userRepository.getCurrentUser(policy: .Cached).flatMap { user -> Observable<String> in
            return Observable.just(user.userID!)
        }.flatMap {userID -> Observable<Data> in
            return self.uploadMealRaw(mealRecord: mealRecord, userID: userID)
        }.flatMap {data -> Observable<Double> in
            //self.storeChatMessages(meal: mealRecord, data: data)
            return Observable.just(0.0)
        }
        return flow
    }
    
    func newUploadMeal(mealRecord : MealRecord) -> Observable<Bool> {
        if mealRecord.nutrientsFromNoom?[0] != nil {
            if (mealRecord.nutrientsFromNoom?[0])!.name == calorieString {
                var nutrs = mealRecord.nutrientsFromNoom
                nutrs?.remove(at: 0)
                mealRecord.nutrientsFromNoom = nutrs
            }
        }
        
        
        let timeStamp = mealRecord.date ?? Date()
        let _ = self.storeToCache(meal: mealRecord, occasion: mealRecord.ocasion, date: timeStamp)
        
        let user = self.userRepository.getCurrentUser(policy: .DefaultPolicy).flatMap({userProfile in
            Observable.just(userProfile.userID!)
            
        }).flatMap({ userId -> Observable<Bool> in
            //let model = self.mealCacheStore.fetchFor(idArray: [cacheID]).first
            
            return Observable.just(true)
        })
        return user
    }
    
    func uploadMealAndGetFoodID(mealRecord : MealRecord) -> Observable<String> {
//        return self.uploadMealRaw(mealRecord: mealRecord, userID: <#String#>).flatMap { data -> Observable<String> in
//            self.storeChatMessages(meal: mealRecord, data: data)
//            return self.getFoodIDFrom(data: data)
//        }
        
        let flow = self.userRepository.getCurrentUser(policy: .DefaultPolicy).flatMap { user -> Observable<String> in
            return Observable.just(user.userID!)
            }.flatMap {userID -> Observable<Data> in
                return self.uploadMealRaw(mealRecord: mealRecord, userID: userID)
            }.flatMap {data -> Observable<String> in
                self.storeChatMessages(meal: mealRecord, data: data)
                return self.getFoodIDFrom(data: data)
        }
        return flow
    }
    
    private func getFoodIDFrom(data : Data) -> Observable<String> {
        return Observable.create { observer in
            do {
                let dict = try JSONSerialization.jsonObject(with: data, options: []) as? [String : Any]
                var idString = ""
                if dict?["userfoodids"] != nil {
                    let array : [String] = dict?["userfoodids"] as! [String]
                    idString = array[0]
                }
                observer.onNext(idString)
                observer.onCompleted()
            } catch let error as NSError {
                observer.onError(error)
            }
            return Disposables.create()
        }
    }

    private func getMealIDFrom(data : Data) -> Observable<[String:String]> {
        return Observable.create { observer in
            do {
                let dict = try JSONSerialization.jsonObject(with: data, options: []) as? [String : Any]
                var idDict : [String : String] = [:]
                if dict?["userfoodids"] != nil {
                    let userFoodIdArray : [String] = dict?["userfoodids"] as! [String]
                    let mealIdArray : [String] = dict?["foodids"] as! [String]
                    
                    idDict["mealIdString"] = mealIdArray[0]
                    idDict["userFoodId"] = userFoodIdArray[0]
                }
                observer.onNext(idDict)
                observer.onCompleted()
            } catch let error as NSError {
                observer.onError(error)
            }
            return Disposables.create()
        }
    }

    
    func storeChatMessagesForGroup(data : Data,groupName:String) {
        let _ = self.userRepository.getCurrentUser(policy: .DefaultPolicy).subscribe(onNext : { [unowned self] userProfile in
            if userProfile.userID != nil {
                let dateNow = Date()
                let loggedFoodDate = Date()
                
                do {
                    let dict = try JSONSerialization.jsonObject(with: data, options: []) as? [String : Any]
                    var scoreNoChange : Bool = false
                    var idString = ""
                    if dict?["userfoodids"] != nil {
                        let array : [String] = dict?["userfoodids"] as? [String] ?? []
                        if array.count > 0 {
                            idString = array[0]
                        }
                    }
                    
                    var score = 0.0
                    if dict?["score_after"] != nil && dict?["score_before"] != nil {
                        let after = dict?["score_after"] as! Double
                        let before = dict?["score_before"] as! Double
                        score = after - before
                        score = score * 100
                        if round(score) == 0 || round(score) == -0 {
                            scoreNoChange = true
                        }
                    }

                    DispatchQueue.main.async {
                        let eatenChatMessage = ChatDataBaseItem()
                        eatenChatMessage.chatBoxType = .typeDish
                        eatenChatMessage.date = dateNow
                        eatenChatMessage.content = groupName
                        eatenChatMessage.isFromUser = true
                        eatenChatMessage.messageID = idString
                        eatenChatMessage.userID = userProfile.userID!
                        let responceFromApp = ChatDataBaseItem()
                        responceFromApp.chatBoxType = .typeLoggedFood
                        responceFromApp.date = loggedFoodDate
                        responceFromApp.content = scoreNoChange ? "Your score has not changed." : String(format: "You just got %.0f points for the food you logged", round(score))
                        responceFromApp.isFromUser = false
                        responceFromApp.messageID = idString
                        responceFromApp.userID = userProfile.userID!
                        
                        let chatStore = ChatBoxRealmDataStore()
                        chatStore.storeRecordsArray(array: [responceFromApp])
                        
                    }
                    
                //Record score feedback
                    let scoreCahce = self.scoreJsonMapper.transform(dictionary: dict!,date:loggedFoodDate, isScoreDelta: true)
                    self.scoreRepo.storeScoreFeedback(scoreModel: scoreCahce)
                } catch let error {
                    LogUtility.logToFile(error)
                }
            }
        })
        
    }
    
    func storeChatMessagesForChangeFood(data : Data) {
        let _ = self.userRepository.getCurrentUser(policy: .Cached).subscribe(onNext : { [unowned self] userProfile in
            if userProfile.userID != nil {
                
                do {
                    let dict = try JSONSerialization.jsonObject(with: data, options: []) as? [String : Any]
                    var scoreNoChange : Bool = false
                    var idString = ""
                    if dict?["userfoodids"] != nil {
                        let array : [String] = dict?["userfoodids"] as! [String]
                        idString = array[0]
                    }
                    
                    var score = 0.0
                    var after = 0.0
                    if dict?["score_after"] != nil && dict?["score_before"] != nil {
                        after = dict?["score_after"] as! Double
                        let before = dict?["score_before"] as! Double
                        score = after - before
                        score = score * 100
                        if round(score) == 0 || round(score) == -0 {
                            scoreNoChange = true
                        }
                    }
                    
                    let loggedFoodDate = Date()
                    DispatchQueue.main.async {
                        let responceFromApp = ChatDataBaseItem()
                        responceFromApp.chatBoxType = .typeLoggedFood
                        responceFromApp.date = loggedFoodDate
                        responceFromApp.content = scoreNoChange ? "Your score has not changed.": String(format: "Your score has been adjusted. Your new score is %.1f points.", after * 100)
                        responceFromApp.isFromUser = false
                        responceFromApp.messageID = idString
                        responceFromApp.userID = userProfile.userID!
                        
                        let chatStore = ChatBoxRealmDataStore()
                        chatStore.storeRecordsArray(array: [responceFromApp])
                    }
                    let scoreCahce = self.scoreJsonMapper.transform(dictionary: dict!,date:loggedFoodDate, isScoreDelta: false)
                    self.scoreRepo.storeScoreFeedback(scoreModel: scoreCahce)
                    
                } catch let error {
                    LogUtility.logToFile(error)
                }
            }
            
        })
        
    }
    
    func storeChatMessages(meal : MealRecord, data : Data) {
        let _ = self.userRepository.getCurrentUser(policy: .Cached).subscribe(onNext : { [unowned self] userProfile in
            if userProfile.userID != nil {
                var dateNow = Date()

                if meal.date != nil {
                    dateNow = meal.date!
                }
                
                let dateFirst = dateNow
                
                do {
                    let dict = try JSONSerialization.jsonObject(with: data, options: []) as? [String : Any]
                    var scoreNoChange : Bool = false
                    var idString = ""
                    if dict?["userfoodids"] != nil {
                        let array : [String] = dict?["userfoodids"] as! [String]
                        idString = array[0]
                    }
                    
                    var score = 0.0
                    if dict?["score_after"] != nil && dict?["score_before"] != nil {
                        let after = dict?["score_after"] as! Double
                        let before = dict?["score_before"] as! Double
                        score = after - before
                        score = score * 100
                        if round(score) == 0 || round(score) == -0 {
                            scoreNoChange = true
                        }
                    }
                    
                    DispatchQueue.main.async {
                        let eatenChatMessage = ChatDataBaseItem()
                        eatenChatMessage.chatBoxType = .typeDish
                        eatenChatMessage.date = dateFirst
                        eatenChatMessage.content = meal.name!
                        eatenChatMessage.isFromUser = true
                        eatenChatMessage.messageID = idString
                        eatenChatMessage.userID = userProfile.userID!
                        
                        let responceFromApp = ChatDataBaseItem()
                        responceFromApp.chatBoxType = .typeLoggedFood
                        responceFromApp.date = dateNow
                        responceFromApp.content = scoreNoChange ? "Your score has not changed." : String(format: "You just got %.0f points for the food you logged", round(score))
                        responceFromApp.isFromUser = false
                        responceFromApp.messageID = idString
                        responceFromApp.userID = userProfile.userID!
                        
                        let chatStore = ChatBoxRealmDataStore()
                        chatStore.storeRecordsArray(array: [eatenChatMessage, responceFromApp])
                    }
                    self.bgFetcher.executeFor(date: meal.date!)
                    //Record score feedback
                    let scoreCahce = self.scoreJsonMapper.transform(dictionary: dict!,date:dateNow, isScoreDelta: true)
                    self.scoreRepo.storeScoreFeedback(scoreModel: scoreCahce)
                    
                } catch let error {
                    LogUtility.logToFile(error)
                }
            }
            
        })
        
    }
    
    func fetchFor(idArray : [String]) -> [MealRecordCache] {
        return self.mealCacheStore.fetchFor(idArray:idArray)
    }
    
    func fetchCacheObjectFor(ident : String) -> MealRecordCache? {
        return self.mealCacheStore.getFoodObjectFor(foodID:ident)
    }
    
    func getFoodScoreFrom(data : Data) -> Observable<Double> {
        return Observable.create { observer in
            do {
                let dict = try JSONSerialization.jsonObject(with: data, options: []) as? [String : Any]
                let message = (dict?["score_after"] as? Double) ?? 0.0
                let unixTime = dict?["datetime"] as? Double
                if unixTime != nil {
                    let date = self.dateFromUnixStamp(time: unixTime!)
                    if Calendar.current.startOfDay(for: date) == Calendar.current.startOfDay(for: Date()) {
                        self.storeCurrentScore(date: Date(), value: message)
                    }
                }
                observer.onNext(message)
                observer.onCompleted()
            } catch let error as NSError {
                observer.onError(error)
            }
            return Disposables.create()
        }
    }
}

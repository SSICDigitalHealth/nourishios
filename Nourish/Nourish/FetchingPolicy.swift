//
//  FetchingPolicy.swift
//  Nourish
//
//  Created by Nova on 11/28/16.
//  Copyright © 2016 SSIC. All rights reserved.
//

import Foundation

enum FetchingPolicy {
    case ForceReload
    case Cached
    case DefaultPolicy
}

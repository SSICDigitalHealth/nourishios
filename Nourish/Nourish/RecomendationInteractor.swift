//
//  RecomendationInteractor.swift
//  Nourish
//
//  Created by Vlad Birukov on 30.08.17.
//  Copyright © 2017 SSIC. All rights reserved.
//

import Foundation
import RxSwift

class RecomendationInteractor {
    private let recsRepo = NestleRecommendationsRepository()
    
    
    func execute(startDate: Date, endDate: Date) -> Observable <RecomendationModel> {
        return self.recsRepo.getRecommendationsFor(startDate:startDate, endDate: endDate)
        
        
//        return Observable<RecomendationModel>.create{(observer) -> Disposable in
//            let pause = Int(arc4random_uniform(4))
//            let dispat = DispatchTime.now() + .seconds(pause)
//            DispatchQueue.main.asyncAfter(deadline: dispat, execute: {
//                let recommendation = RecomendationModel()
//                recommendation.titleText = "Keep an eye on hidden sugars in your food and drink."
//                recommendation.descriptionText = "Beverages such as fruity drinks often contain more sugar than you realise."
//                observer.onNext(recommendation)
//                observer.onCompleted()
//            })
//            return Disposables.create()
//        }
        
    }

}

//
//  RecipesDetailView.swift
//  Nourish
//
//  Created by Vlad Birukov on 07.09.17.
//  Copyright © 2017 SSIC. All rights reserved.
//

import UIKit

protocol RecipesDetailProtocol : BaseViewProtocol {
    func reloadData()
}

class RecipesDetailView: BaseView, RecipesDetailProtocol {

    @IBOutlet var presenter: RecipesDetailPresenter!
    @IBOutlet var detailTableView: UITableView!
    
    let headerHeight = 500
    let rowHeight = 23.5
    
    override func viewWillAppear(_ animated: Bool) {
        self.basePresenter = self.presenter
        self.presenter.recipesDetailView = self
        self.registerTableViewCells()
        self.settingTableView()

        super.viewWillAppear(animated)
        self.stopActivityAnimation()
    }

    func reloadData() {
        self.detailTableView.reloadData()
    }
    
    func settingTableView() {
        self.detailTableView.sectionHeaderHeight = UITableViewAutomaticDimension
        self.detailTableView.estimatedSectionHeaderHeight = CGFloat(self.headerHeight)
        
        self.detailTableView.rowHeight = UITableViewAutomaticDimension
        self.detailTableView.estimatedRowHeight = CGFloat(self.rowHeight)
    }
    
   
    
    func registerTableViewCells() {
        self.detailTableView.register(UINib(nibName: "RecepiesTableViewCell", bundle: nil), forCellReuseIdentifier: "cellRecepies")
        self.detailTableView.register(UINib(nibName: "RecipesDetailTableViewCell", bundle: nil), forCellReuseIdentifier: "recipesCell")
    }
}

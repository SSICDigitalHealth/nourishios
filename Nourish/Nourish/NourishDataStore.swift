//
//  NourishDataStore.swift
//  Nourish
//
//  Created by Nova on 12/13/16.
//  Copyright © 2016 SSIC. All rights reserved.
//

import UIKit
import RxSwift

#if DEBUG
let kNourishApiAddress = "https://8zpwzil09j.execute-api.us-west-2.amazonaws.com/dev/v2/api/"
#elseif TEST
let kNourishApiAddress = "https://c9a5nvkmu1.execute-api.us-west-2.amazonaws.com/test/v2/api/"
#else
    #if OPTIFASTVERSION
    let kNourishApiAddress = "https://pilot1.aws.dnhapp.nestle.com/v2/api/"
    #else
    let kNourishApiAddress = "https://prod.aws.dnhapp.nestle.com/v2/api/"
    #endif
#endif

let kNourishApiMeals = "aws/meals"
let kNourishFoodScore = "aws/score"
let kNourishRecSteps = "aws/max_steps"
let kNourishRecommend = "recommendations/dayplan"
let kFeedBack = "aws/log"
let kNourishPostFood = "food/savemeal"
let kNutrientsPath = "aws/foodnutrients"
let kNourishChangeDeleteFood = "fooditems"
let kCaloriesScore = "aws/calories"
let kNourishTips = "aws/today_tips"
let kNourishUserID = "?userid="
let kNourishToken = "&token="
let kStartString = "&start="
let kEndString = "&end="
let kLocalTimeQ = "?local_time="
let kLocalTime = "&local_time="
let kDayString = "&day="
let kCountString = "&count="
let kDateTime = "&datetime="
let kGrams = "&grams="
let kStoreGroups = "foodgroups"
let kPhotoPath = "user_photos"

class NourishDataStore: BaseNestleDataStore {
    let mapper = MealRecordCache_to_Json()
    let base64Mapper = ImageToBase64Mapper()
    static let shared = NourishDataStore()
    func getFoodForDate(date : Date, userID : String, token : String) -> Observable<Data> {
        
        return Observable.create { observer in
            let url = self.getFoodUrl(userID: userID, token: token, date: date)
            var request = self.requestWithToken(token: token, url: url)//URLRequest(url: url)
            request.httpMethod = "GET"
            
            let session = URLSession.shared.dataTask(with: request) {
                data, response, error in
                if error == nil && data != nil {
                    observer.onNext(data!)
                } else {
                    observer.onError(error!)
                }
                
                self.log(foodProcessingOrWithings: true, url: url.absoluteString, requestMethod: request.httpMethod, urlResponse: response, requestBody: nil, error: error, responceBody: data)
                observer.onCompleted()
            }
            session.resume()
            return Disposables.create {
                session.cancel()
            }
        }
        
    }
    
    func getRecommendations(date : Date, userID : String, token : String, count : Int) -> Observable<Data> {
        return Observable.create { observer in
            let url = self.getRecommendationsUrl(userID: userID, token: token, date: date, count: count)
            print(url)
            var request = self.requestWithToken(token: token, url: url)
            request.timeoutInterval = 20.0
            request.httpMethod = "GET"
            
            let session = URLSession.shared.dataTask(with: request) {
                data, responce, error in
                if error == nil && data != nil {
                    observer.onNext(data!)
                    observer.onCompleted()
                } else {
                    observer.onError(error!)
                    observer.onCompleted()
                }
            }
            session.resume()
            return Disposables.create {
                session.cancel()
            }
        }
    }
    
    func getTipsFor(userID : String, token : String, date : Date) -> Observable<Data> {
        return Observable.create { observer in
            let url = self.getTipsUrl(userID: userID, token: token, date: date)
            var request = self.requestWithToken(token: token, url: url)
            request.httpMethod = "GET"
            
            let session = URLSession.shared.dataTask(with: request) {
                data, responce, error in
                if error == nil && data != nil {
                    observer.onNext(data!)
                    observer.onCompleted()
                } else {
                    observer.onError(error!)
                    observer.onCompleted()
                }
            }
            session.resume()
            return Disposables.create {
                session.cancel()
            }
        }
    }
    
    func getFoodScore(date : Date, userID : String, token : String) -> Observable<Data> {
        return Observable.create { observer in
            let url = self.getFoodScoreUrl(userID: userID, token: token, date: date)
            var request = self.requestWithToken(token: token, url: url)
            request.httpMethod = "GET"
            let session = URLSession.shared.dataTask(with: request) {
                data, response, error in
                if error == nil && data != nil {
                    observer.onNext(data!)
                } else {
                    observer.onError(error!)
                }
                self.log(foodProcessingOrWithings: true, url: url.absoluteString, requestMethod: request.httpMethod, urlResponse: response, requestBody: nil, error: error, responceBody: data)
                observer.onCompleted()
            }
            session.resume()
            return Disposables.create {
                session.cancel()
            }
        }
    }
    
    func getFoodScore(startDate : Date, endDate : Date, userID : String, token : String, range : Int) -> Observable<Data> {
        return Observable.create({observer in
            let url = self.getFoodScoreUrlWith(userID: userID, token: token, startDate: startDate, endDate: endDate, range: range)
            var request = self.requestWithToken(token: token, url: url)
            request.httpMethod = "GET"
            request.setValue("application/json", forHTTPHeaderField: "Content-Type")
            let session = URLSession.shared.dataTask(with: request) {
                data, response, error in
                if error == nil && data != nil {
                    observer.onNext(data!)
                } else {
                    observer.onError(error!)
                }
                self.log(foodProcessingOrWithings: true, url: url.absoluteString, requestMethod: request.httpMethod, urlResponse: response, requestBody: nil, error: error, responceBody: data)
                observer.onCompleted()
            }
            session.resume()
            return Disposables.create {
                session.cancel()
            }
        })
    }
    
    func getCaloriesStats(startDate : Date, endDate : Date, token : String, userId : String, range : Int) -> Observable<Data> {
        return Observable.create {observer in
            let url = self.getCaloriesUrl(userID: userId, token: token, startDate: startDate, endDate: endDate, range: range)
            var request = self.requestWithToken(token: token, url: url)
            request.httpMethod = "GET"
            request.setValue("application/json", forHTTPHeaderField: "Content-Type")
            let session = URLSession.shared.dataTask(with: request) {
                data, response, error in
                if error == nil && data != nil {
                    observer.onNext(data!)
                } else {
                    observer.onError(error!)
                }
                self.log(foodProcessingOrWithings: true, url: url.absoluteString, requestMethod: request.httpMethod, urlResponse: response, requestBody: nil, error: error, responceBody: data)
                observer.onCompleted()
            }
            session.resume()
            return Disposables.create {
                session.cancel()
            }
        }
    }
      
    func uploadMealRecord(mealRecord : MealRecordCache, userID : String, token : String) -> Observable<Data> {
        return Observable.create { observer in
            let url = self.uploadFoodUrl(userID: userID, token: token)
            var request = self.requestWithToken(token: token, url: url)
            request.setValue("application/json", forHTTPHeaderField: "Content-Type")
            request.httpMethod = "POST"
            let dictRepresentation = self.mapper.transform(meal: mealRecord, userID: userID, includingOcasion : true)
            
            let dict = ["userid" : userID, "meals" : [dictRepresentation]] as [String : Any]
            var data : Data? = nil
            print("Dict \(dict)")
            do {
                data = try JSONSerialization.data(withJSONObject: dict, options: .prettyPrinted)
            } catch let error as NSError {
                LogUtility.logToFile(error)
            }

            request.httpBody = data
            let session = URLSession.shared.dataTask(with: request) {
                data, response, error in
                if error == nil && data != nil {
                    observer.onNext(data!)
                } else {
                    observer.onError(error!)
                }
                self.log(foodProcessingOrWithings: true, url: url.absoluteString, requestMethod: request.httpMethod, urlResponse: response, requestBody: dict, error: error, responceBody: data)
                observer.onCompleted()
            }
            session.resume()
            return Disposables.create {
                //session.cancel()
            }
        }
    }
    
    private func prepareLogString(userId : String, message : String, log : String) -> String {
        let infoDict = Bundle.main.infoDictionary
        
        let version = infoDict?["CFBundleShortVersionString"] as! String
        let build = infoDict?["CFBundleVersion"] as! String
        
        let df = DateFormatter()
        df.dateFormat = "E, MMM d HH:mm:ss Z yyyy"
        
        var resultString = "\n"
        
        resultString.append(String(format:"Nourish feedback. Version %@(%@) on %@\n", version, build, df.string(from: Date())))
        resultString.append("\n")
        
        resultString.append(String(format:"User id is %@\n",userId))
        resultString.append("\n")
        
        resultString.append(String(format:"User comment %@\n", message))
        resultString.append("\n")
            
        resultString.append("Recent log entries:\n")
        resultString.append(log)
        
        return resultString
    }
    
    func uploadFavGroup(groupID : String, occasion : Ocasion, userID : String, token : String, date : Date) -> Observable<Data> {
        return Observable.create { observer in
            let url = self.uploadFoodUrl(userID: userID, token: token)
            var request = self.requestWithToken(token: token, url: url)
            print(url)
            request.setValue("application/json", forHTTPHeaderField: "Content-Type")
            request.httpMethod = "POST"
            
            let dictMeal = ["foodgroupid" : groupID, "occasion" : occasion.rawValue, "day" : self.getDayString(date: date), "datetime" : self.dateTime(date: date), "userid" : userID] as [String : Any]
            
            let dict = ["meals" : [dictMeal], "userid" : userID] as [String : Any]
            
            var data : Data? = nil
            
            do {
                data = try JSONSerialization.data(withJSONObject: dict, options: .prettyPrinted)
            } catch let error as NSError {
                print(error)
            }
            
            request.httpBody = data
            let session = URLSession.shared.dataTask(with: request) {
                data, response, error in
                if error == nil && data != nil {
                    observer.onNext(data!)
                } else {
                    observer.onError(error!)
                }
                self.log(foodProcessingOrWithings: true, url: url.absoluteString, requestMethod: request.httpMethod, urlResponse: response, requestBody: dict, error: error, responceBody: data)
                observer.onCompleted()

            }
            session.resume()
            return Disposables.create {
                session.cancel()
            }
        }
    }
    
    func storeUserFeedback(feedback : String, userID : String, token : String, timeStamp : TimeInterval, log : String) -> Observable<Data> {
        return Observable.create { observer in
            let url = self.getFeedBackUrl()
            
            var request = self.requestWithToken(token: token, url: url)
            
            let dict = ["userId" : userID, "message" : feedback, "timestamp" : Int(timeStamp * 1000), "log" : self.prepareLogString(userId: userID, message: feedback, log: log)] as [String : Any]
            
            var data : Data? = nil
            do {
                data = try JSONSerialization.data(withJSONObject: dict, options: .prettyPrinted)
            } catch let error as NSError {
                LogUtility.logToFile(error)
            }
            request.httpBody = data
            request.httpMethod = "POST"
            
            let session = URLSession.shared.dataTask(with: request) {
                data, response, error in

                if error == nil && data != nil {
                    observer.onNext(data!)
                } else {
                    observer.onError(error!)
                }
                self.log(foodProcessingOrWithings: true, url: url.absoluteString, requestMethod: request.httpMethod, urlResponse: response, requestBody: dict, error: error, responceBody: data)
                observer.onCompleted()
            }
            session.resume()
            return Disposables.create {
                session.cancel()
            }
        }
    }
    
    func changeDeleteFood(dictionary : [String : Any]?, token : String, userID : String, userFoodId : [String]) -> Observable<Data> {
        return Observable.create { observer in
            let url = self.getChangeFoodUrl(userID: userID, token: token, userFoodId: userFoodId)
            var request = self.requestWithToken(token: token, url: url)
            var method = "DELETE"
            if dictionary != nil {
                method = "PUT"
                var data : Data? = nil
                
                do {
                    data = try JSONSerialization.data(withJSONObject: dictionary as Any, options: .prettyPrinted)
                } catch let error as NSError {
                    LogUtility.logToFile(error)
                }
                
                request.httpBody = data
            }
            request.setValue("application/json", forHTTPHeaderField: "Content-Type")
            request.httpMethod = method
            
            let session = URLSession.shared.dataTask(with: request) {
                data, response, error in
                
                if error == nil && data != nil {
                    let status = (response as! HTTPURLResponse).statusCode
                    if status == 200 || status == 410 {
                        observer.onNext(data!)
                    } else {
                        if let dataToJson = data {
                            var dictRep = [String : Any]()
                            do {
                                dictRep = try JSONSerialization.jsonObject(with: dataToJson, options: []) as? [String : Any] ?? [String : Any]()
                            } catch let err as NSError  {
                                print("Error response dictionary serialization ", err.localizedDescription)
                            }
                            observer.onError(self.errorFromBackend(dict: dictRep, errorCode: status))
                        }
                    }
                } else {
                    observer.onError(error!)
                }
                self.log(foodProcessingOrWithings: true, url: url.absoluteString, requestMethod: request.httpMethod, urlResponse: response, requestBody: dictionary, error: error, responceBody: data)
                observer.onCompleted()
            }
            session.resume()
            return Disposables.create {
                session.cancel()
            }
        }
        
    }
    
    func addCacheToGroup(token : String, userID : String, cache : MealRecordCache, name : String) -> Observable<Data> {
        return Observable.create { observer in
            let url = self.getStoreGroupUrl(userID: userID, token: token)
            var request = self.requestWithToken(token: token, url: url)//URLRequest(url: url)
            request.httpMethod = "POST"
            
            let dict = ["description" : "description","favorite" : true, "name" : name, "userid" : userID, "food_items" : [self.mapper.transform(meal: cache, userID: userID, includingOcasion: false)]] as [String : Any]
            
            request.setValue("application/json", forHTTPHeaderField: "Content-Type")
            var data : Data? = nil
            do {
                data = try JSONSerialization.data(withJSONObject: dict, options: .prettyPrinted)
            } catch let error as NSError {
                observer.onError(error)
            }
            
            request.httpBody = data
            
            let session = URLSession.shared.dataTask(with: request) {
                data, response, error in
                
                if error == nil && data != nil {
                    observer.onNext(data!)
                } else {
                    if error != nil {
                        observer.onError(error!)
                    }
                }
                self.log(foodProcessingOrWithings: true, url: url.absoluteString, requestMethod: request.httpMethod, urlResponse: response, requestBody: dict, error: error, responceBody: data)
                observer.onCompleted()
            }
            session.resume()
            return Disposables.create {
                session.cancel()
            }
        }

    }
    
    func addMealsToGroup(token : String, userID : String, foodModels : [foodSearchModel], name : String) -> Observable<Data> {
        return Observable.create { observer in
            let url = self.getStoreGroupUrl(userID: userID, token: token)
            var request = self.requestWithToken(token: token, url: url)//URLRequest(url: url)
            request.httpMethod = "POST"
           
            let dict = ["description" : "description","favorite" : true, "name" : name,"userid" : userID, "food_items" : self.foodItemsFrom(foodModels: foodModels)] as [String : Any]
            
            request.setValue("application/json", forHTTPHeaderField: "Content-Type")
            var data : Data? = nil
            
            do {
                data = try JSONSerialization.data(withJSONObject: dict, options: .prettyPrinted)
            } catch let error as NSError {
                observer.onError(error)
            }
            
            request.httpBody = data
            
            let session = URLSession.shared.dataTask(with: request) {
                data, response, error in
                
                if error == nil && data != nil {
                    observer.onNext(data!)
                } else {
                    if error != nil {
                        observer.onError(error!)
                    }
                }
                self.log(foodProcessingOrWithings: true, url: url.absoluteString, requestMethod: request.httpMethod, urlResponse: response, requestBody: dict, error: error, responceBody: data)
                observer.onCompleted()
            }
            session.resume()
            return Disposables.create {
                session.cancel()
            }
        }
    }

    
    func addRecToGroup(token : String, userID : String, foodModels : [foodSearchModel], name : String, favorite : Bool) -> Observable<Data> {
        return Observable.create { observer in
            let url = self.getStoreGroupUrl(userID: userID, token: token)
            var request = self.requestWithToken(token: token, url: url)//URLRequest(url: url)
            request.httpMethod = "POST"
            
            let dict = ["description" : "description","favorite" : favorite, "name" : name,"userid" : userID, "food_items" : self.jsonFrom(models: foodModels, userID: userID)] as [String : Any]
            
            request.setValue("application/json", forHTTPHeaderField: "Content-Type")
            var data : Data? = nil
            do {
                data = try JSONSerialization.data(withJSONObject: dict, options: .prettyPrinted)
            } catch let error as NSError {
                observer.onError(error)
            }
            
            request.httpBody = data
            
            let session = URLSession.shared.dataTask(with: request) {
                data, response, error in
                
                if error == nil && data != nil {
                    observer.onNext(data!)
                } else {
                    if error != nil {
                        observer.onError(error!)
                    }
                }
                self.log(foodProcessingOrWithings: true, url: url.absoluteString, requestMethod: request.httpMethod, urlResponse: response, requestBody: dict, error: error, responceBody: data)
                observer.onCompleted()
            }
            session.resume()
            return Disposables.create {
                session.cancel()
            }
        }
    }
    
    func searchFnddsFoodWith(search : String, token : String, userID : String) -> Observable<[foodSearchModel]> {
        return Observable.create { observer in
            let url = self.getFnndsSearchUrl(userID: userID, token: token, search: search, limit: 30)
            var request = self.requestWithToken(token: token, url: url)
            request.httpMethod = "GET"
            
            let session = URLSession.shared.dataTask(with: request) {
                data, response, error in
                
                if error == nil && data != nil {
                    do {
                        let dict = try JSONSerialization.jsonObject(with: data!, options: []) as? [[String : Any]]
                        if dict != nil {
                            let proxyArray = self.mapFnddsIntoObjects(array: dict!)
                            observer.onNext(proxyArray)
                        }
                    } catch let error {
                        observer.onError(error)
                    }
                } else {
                    if error != nil {
                        observer.onError(error!)
                    }
                    
                }
                self.log(foodProcessingOrWithings: true, url: url.absoluteString, requestMethod: request.httpMethod, urlResponse: response, requestBody: nil, error: error, responceBody: data)
                observer.onCompleted()
            }
            
            session.resume()
            
            return Disposables.create {
                
            }
        }
    }
    
    func searchFnddsFoodFrom(tags : [String], token : String, userID : String) -> Observable<[foodSearchModel]> {
        return Observable.create { observer in
            var array = [foodSearchModel]()
                if tags.count > 0 {
                    let url = self.getFnndsTagSearchUrl(userID: userID, token: token, tags: tags)
                    var request = self.requestWithToken(token: token, url: url)
                    request.httpMethod = "GET"
            
                    let session = URLSession.shared.dataTask(with: request) {
                        data, response, error in
                
                        if error == nil && data != nil {
                           // print(String(data : data!, encoding : .utf8))
                            do {
                                let dict = try JSONSerialization.jsonObject(with: data!, options: []) as? [[String : Any]]
                                if dict != nil {
                                    let proxyDict = self.mapIntoDict(array: dict!)
                            
                                    for tag in tags {
                                        if proxyDict[tag] != nil {
                                            array.append(proxyDict[tag]!)
                                        }
                                    }
                                    observer.onNext(array)
                                }
                            } catch let error {
                                observer.onError(error)
                            }
                        } else {
                            if error != nil {
                                observer.onError(error!)
                            }
                    
                        }
                        self.log(foodProcessingOrWithings: true, url: url.absoluteString, requestMethod: request.httpMethod, urlResponse: response, requestBody: nil, error: error, responceBody: data)
                        observer.onCompleted()
                    }
            
                    session.resume()
                } else {
                    observer.onNext(array)
                    observer.onCompleted()
            }
            
            return Disposables.create()
        }

    }
    
    private func mapIntoDict(array : [[String : Any]]) -> [String : foodSearchModel] {
        var result = [String : foodSearchModel]()
        
        for object in array {
            let tag = object["q"] as? String
            let array = object["result"] as? [[String : Any]]
            var meal : foodSearchModel?
            if array != nil && (array?.count)! > 0 {
                meal = self.mapFnddsIntoObjects(array: array!).first
            }
            
            if meal != nil {
                result[tag!] = meal
            }
        }
        return result
    }
    
    private func mapFnddsIntoObjects(array : [[String : Any]]) -> [foodSearchModel] {
        var proxyArray = [foodSearchModel]()
        
        for object in array {
            if (object["id"] as? String) != nil {
                var model = foodSearchModel()
                model.unit = object["msre_desc"] as! String
                let porCal = object["por_kcal"] as! Double
                let grams = object["msre_grams"] as? Double
                let amount = object["msre_amount"] as! Double
                let calories = porCal
                model.calories = "\(calories)"
                model.amount = amount
                model.grams = grams
                model.foodId = object["id"] as! String
                model.foodTitle = object["name"] as! String
                model.mealIdString = object["id"] as? String
                proxyArray.append(model)
            }
            
        }
        return proxyArray
    }
    
    func addMealRecordsToGroup(token : String, userID : String, foodModels: [MealRecordCache], name : String) -> Observable<Data> {
        
        return Observable.create { observer in
            let url = self.getStoreGroupUrl(userID: userID, token: token)
            var request = self.requestWithToken(token: token, url: url)//URLRequest(url: url)
            request.httpMethod = "POST"
            
            let dict = ["description" : "description","favorite" : false, "food_items" : self.mapper.transform(meals: foodModels, userID: userID,includingOcasion : false), "name" : name, "userid" : userID] as [String : Any]
            request.setValue("application/json", forHTTPHeaderField: "Content-Type")
            var data : Data? = nil
            do {
                data = try JSONSerialization.data(withJSONObject: dict, options: .prettyPrinted)
            } catch let error as NSError {
                observer.onError(error)
            }
        
            request.httpBody = data
            
            let session = URLSession.shared.dataTask(with: request) {
                data, response, error in
                
                if error == nil && data != nil {
                    //print(String(data : data!, encoding : .utf8))
                    observer.onNext(data!)
                } else {
                    if error != nil {
                        observer.onError(error!)
                    }
                }
                self.log(foodProcessingOrWithings: true, url: url.absoluteString, requestMethod: request.httpMethod, urlResponse: response, requestBody: dict, error: error, responceBody: data)
                observer.onCompleted()
            }
            session.resume()
            
            return Disposables.create {
                session.cancel()
            }
        }
    }
    
    func jsonFrom(models : [foodSearchModel], userID : String) -> [[String : Any]] {
        var dicts : [[String : Any]] = []
        for model in models {
            var dict : [String : Any] = [:]
            dict["amount"] = model.amount
            dict["calories"] = Double(model.calories)
            dict["grams"] = model.grams ?? 0.0
            dict["id"] = model.mealIdString
            dict["msre_amount"] = model.amount
            dict["msre_desc"] = model.unit
            //["caloriesPerGram"] = 0.0
            dict["name"] = model.foodTitle
            dict["unit"] = model.unit
            dict["userid"] = userID
            dict["nutrients"] = []
            dicts.append(dict)
        }
        
        return dicts
    }
    
    func foodItemsFrom(foodModels : [foodSearchModel]) -> [[String : String]] {
        var array : [[String : String]] = []
        
        for model in foodModels {
            if model.foodBackendID != nil {
                let dict = ["userfoodid" : model.foodBackendID!]
                array.append(dict)
            }
        }
        
        return array
    }
    
    func fetchNutritionScoreForRecFood(token : String, userID : String, foodID : String, foodWeight : Double) -> Observable<Data> {
        return Observable.create { observer in
            let url = self.getFetchNutrientsUrl(userID: userID, token: token, userFoodID: foodID, grams: foodWeight)
            var request = self.requestWithToken(token: token, url: url)
            print(url)
            let method = "GET"
            request.httpMethod = method
            
            let session = URLSession.shared.dataTask(with: request) {
                data, response, error in
                
                if error == nil && data != nil {
                    observer.onNext(data!)
                } else {
                    observer.onError(error!)
                }
                self.log(foodProcessingOrWithings: true, url: url.absoluteString, requestMethod: request.httpMethod, urlResponse: response, requestBody: nil, error: error, responceBody: data)
                observer.onCompleted()
            }
            session.resume()
            
            return Disposables.create {
                session.cancel()
            }
        }
    }
    
    func addGroupToFavorite(token : String, userID : String, groupID : String, name : String, userPhotoID : String?) -> Observable<Data> {
        return Observable.create { observer in
            let url = self.getDeleteGroupUrl(foodGroupID: groupID, userID: userID, token: token)
            var request = self.requestWithToken(token: token, url: url)
            print(url)
            request.httpMethod = "PUT"
            
            var dict = ["description" : "description", "favorite" : 1, "name" : name, "userid" : userID]  as [String : Any]
            
            if userPhotoID != nil {
                dict["user_photo_id"] = userPhotoID!
            }
            
            var data : Data? = nil
            
            do {
                data = try JSONSerialization.data(withJSONObject: dict, options: .prettyPrinted)
            } catch let error as NSError {
                LogUtility.logToFile(error)
            }
            
            request.httpBody = data
            
            let session = URLSession.shared.dataTask(with: request) {
                data, response, error in
                if error == nil && data != nil {
                    observer.onNext(data!)
                } else {
                    observer.onError(error!)
                }
                self.log(foodProcessingOrWithings: true, url: url.absoluteString, requestMethod: request.httpMethod, urlResponse: response, requestBody: dict, error: error, responceBody: data)
                observer.onCompleted()
            }
            session.resume()
            return Disposables.create {
                session.cancel()
            }
        }
    }
    
    func deleteGroupWith(groupID : String, token : String, userID : String) -> Observable<Data> {
        return Observable.create { observer in
            let url = self.getDeleteGroupUrl(foodGroupID: groupID, userID: userID, token: token)
            var request = self.requestWithToken(token: token, url: url)
            print(url)
            request.httpMethod = "DELETE"
            
            let session = URLSession.shared.dataTask(with: request) {
                data, response, error in
                
                if error == nil && data != nil {
                    observer.onNext(data!)
                    observer.onCompleted()
                } else {
                    if error != nil {
                        observer.onError(error!)
                    }
                    self.log(foodProcessingOrWithings: true, url: url.absoluteString, requestMethod: request.httpMethod, urlResponse: response, requestBody: nil, error: error, responceBody: data)
                    observer.onCompleted()
                    
                }
            }
            session.resume()
            return Disposables.create {
                session.cancel()
            }
        }
    }
    
    func fetchFavorites(token : String, userID : String) -> Observable<Data> {
        return Observable.create {observer in
            let url = self.getStoreGroupUrl(userID: userID, token: token)
            var request = self.requestWithToken(token: token, url: url)
            print(url)
            request.httpMethod = "GET"
            
            let session = URLSession.shared.dataTask(with: request) {
                data, response, error in
                
                if error == nil && data != nil {
                    let string = String(data : data!, encoding : .utf8)
                    
                    if string != "INTERNAL SERVER ERROR" {
                        observer.onNext(data!)
                    }
                } else {
                    if error != nil {
                        observer.onError(error!)
                    }
                }
                self.log(foodProcessingOrWithings: true, url: url.absoluteString, requestMethod: request.httpMethod, urlResponse: response, requestBody: nil, error: error, responceBody: data)
                observer.onCompleted()
            }
            session.resume()
            
            return Disposables.create {
                session.cancel()
            }
        }
    }
    
    func uploadPhoto(image : UIImage, token : String, userID : String, userfoodID : String?, groupID : String?) -> Observable<String> {
        
        return Observable.create { observer in
            var url : URL? = nil
            
            if userfoodID != nil {
                url = self.getUploadFoodPhotoUrl(token: token, userID: userID, userFoodID: userfoodID!)
            } else {
                if groupID != nil {
                    url = self.getUploadGroupPhotoUrl(token: token, userID: userID, groupFoodID: groupID!)
                } else {
                    observer.onNext("")
                    observer.onCompleted()
                    
                }
            }
            
            
            var request = self.requestWithToken(token: token, url: url!)
            request.httpMethod = "POST"
            request.setValue("application/json", forHTTPHeaderField: "Content-Type")
            
            let imageString = self.base64Mapper.pack(image: image)
            let dict = ["photo_b64" : imageString, "photo_content_type" : "image/jpeg"]
            
            var data : Data? = nil
            
            do {
                data = try JSONSerialization.data(withJSONObject: dict, options: [])
            } catch let error as NSError {
                LogUtility.logToFile(error)
            }
            
            request.httpBody = data
            
            let session = URLSession.shared.dataTask(with: request) {
                dataResp, response, error in
                
                if error == nil && dataResp != nil {
                    do {
                        let dict = try JSONSerialization.jsonObject(with: dataResp!, options: []) as? [String : Any]
                        let photoID = dict?["user_photo_id"] as? String
                        
                        if photoID != nil {
                            observer.onNext(photoID!)
                        } else if let errorCode = dict?["error_code"] as? Int {
                            observer.onError(self.errorFromBackend(dict: dict ?? [String : Any](), errorCode: errorCode))
                        }
                    } catch let error {
                        observer.onError(error)
                    }
                } else {
                    if error != nil {
                        observer.onError(error!)
                    }
                }
                self.log(foodProcessingOrWithings: true, url: url?.absoluteString, requestMethod: request.httpMethod, urlResponse: response, requestBody: ["photo_b64" : "Base 64 encoded photo"], error: error, responceBody: dataResp)
                observer.onCompleted()
            }
            session.resume()
            
            return Disposables.create {
                session.cancel()
            }
        }
    }
    
    func getFoodImage(token : String, userID : String, photoID : String) -> Observable<UIImage> {
        return Observable.create { observer in
            var urlString = "\(kNourishApiAddress)\(kPhotoPath)"
            urlString = urlString + "/\(photoID)\(kNourishUserID)" + userID

            //urlString = self.addAuthorizationHeader(url: urlString, token: token)
            let url = URL(string : urlString)
            
            var request = self.requestWithToken(token: token, url: url!)
            request.httpMethod = "GET"
            request.setValue("application/json", forHTTPHeaderField: "Content-Type")
            
            let session = URLSession.shared.dataTask(with: request) {
                data, response, error in
                
                if error == nil && data != nil {
                    let image = self.getImageFrom(data: data!)
                    if image != nil {
                        observer.onNext(image!)
                    }
                } else {
                    if error != nil {
                        observer.onError(error!)
                    }
                }
                self.log(foodProcessingOrWithings: true, url: url?.absoluteString, requestMethod: request.httpMethod, urlResponse: response, requestBody: nil, error: error, responceBody: nil)
                observer.onCompleted()
            }
            session.resume()
            
            return Disposables.create {
                session.cancel()
            }

        }
    }
    
    private func getImageFrom(data : Data) -> UIImage? {
        let json = try? JSONSerialization.jsonObject(with: data, options: [])
        
        if let dictionary = json as? [String: Any] {
            let base64String = dictionary["photo_b64"] as? String
            if base64String != nil {
                let image = base64Mapper.unpack(base64: base64String!)
                return image
            }
        }
        return nil
    }
    
    private func getFeedBackUrl() -> URL {
        return URL(string : "\(kNourishApiAddress)\(kFeedBack)")!
    }
    
    private func getUploadFoodPhotoUrl(token : String, userID : String, userFoodID : String) -> URL {
        var stringUrl = "\(kNourishApiAddress)\(kPhotoPath)\(kNourishUserID)" + userID
        stringUrl = self.appendFoodId(string: stringUrl, foodID: [userFoodID])
        return URL(string : stringUrl)!
    }
    
    private func getUploadGroupPhotoUrl(token : String, userID : String, groupFoodID : String) -> URL {
        var stringUrl = "\(kNourishApiAddress)\(kPhotoPath)\(kNourishUserID)" + userID
        stringUrl = stringUrl.appending("&foodgroup_id=\(groupFoodID)")
        return URL(string : stringUrl)!
    }
    
    private func getUploadBasePhotoURL(token : String, userID : String) -> URL {
        let stringUrl = "\(kNourishApiAddress)\(kPhotoPath)\(kNourishUserID)" + userID
        return URL(string : stringUrl)!
    }
    
    private func appendFoodId(string : String, foodID : [String]) -> String {
        var stringToAppend = ""
        if foodID.count == 1 {
            stringToAppend = "&userfoodid=\(foodID.first!)"
        } else {
            var charSet = CharacterSet()
            charSet.formUnion(.urlQueryAllowed)
            charSet.remove(",")
            if let join = foodID.joined(separator: "\",\"").addingPercentEncoding(withAllowedCharacters: charSet) {
                stringToAppend = "&userfoodid=[%22\(join)%22]"
            }
        }
        
        var stringToReturn = "\(string)\(stringToAppend)"
        stringToReturn = self.addCalorieBudgetToUrl(url: stringToReturn)
        return stringToReturn
    }
    
    private func getFetchNutrientsUrl(userID : String, token : String, userFoodID : String, grams : Double) -> URL {
        var stringUrl = "\(kNourishApiAddress)\(kNutrientsPath)\(kNourishUserID)" + userID
        stringUrl = self.addLocalTime(stringUrl: stringUrl, local: kLocalTime)
        stringUrl = self.addID(url: stringUrl, idString: userFoodID)
        stringUrl = self.addGrams(url: stringUrl, grams: grams)
        return URL(string : stringUrl)!
    }
    private func getChangeFoodUrl(userID : String, token : String, userFoodId : [String]) -> URL {
        var stringUrl = "\(kNourishApiAddress)\(kNourishChangeDeleteFood)\(kNourishUserID)" + userID
        stringUrl = self.appendFoodId(string: stringUrl, foodID: userFoodId)
        return URL(string : stringUrl)!
    }
    
    private func getRecommendationsUrl(userID : String, token : String, date : Date, count : Int) -> URL {
        var stringUrl = "\(kNourishApiAddress)\(kNourishRecommend)\(kNourishUserID)" + userID
        stringUrl = self.appendTimeToString(url: stringUrl, date: date, includingRange: false,range: 0, q: false)
        stringUrl = self.appendCount(url: stringUrl, count: count)
        stringUrl = self.appendDateTime(url: stringUrl, date: date)
        stringUrl = stringUrl.appending("carousel=1")
        return URL(string : stringUrl)!
    }
    
    private func getTipsUrl(userID : String, token : String, date : Date) -> URL {
        var stringUrl = "\(kNourishApiAddress)\(kNourishTips)\(kNourishUserID)" + userID
        //stringUrl = self.addAuthorizationHeader(url: stringUrl, token: token)
        stringUrl = self.addLocalTime(stringUrl: stringUrl, local : kLocalTime)
        return URL(string : stringUrl)!
    }
    
    private func uploadFoodUrl(userID : String, token : String) -> URL {
        var stringUrl = "\(kNourishApiAddress)\(kNourishPostFood)"
        stringUrl = self.addLocalTime(stringUrl: stringUrl, local: kLocalTimeQ)
        let doneStringUrl = self.addCalorieBudgetToUrl(url: stringUrl)
        //stringUrl = self.addAuthorizationHeader(url: stringUrl, token: token)
        //stringUrl = stringUrl.appending("&calorie_consumption_goal=1609")
        return URL(string: doneStringUrl)!
    }
    
    private func getFoodScoreUrl(userID : String, token : String, date : Date) -> URL {
        var stringUrl = "\(kNourishApiAddress)\(kNourishFoodScore)\(kNourishUserID)" + userID
        stringUrl = appendTimeToString(url: stringUrl, date: date, includingRange: true,range: 1, q : false)
        return URL(string: stringUrl)!
    }
    private func getFoodUrl(userID : String, token : String, date : Date) -> URL {
        var userToUse = userID
        
        if userID == "" {
            userToUse = NRUserSession.sharedInstance.userID ?? ""
        }
        
        var stringUrl = "\(kNourishApiAddress)\(kNourishApiMeals)\(kNourishUserID)" + userToUse
        stringUrl = appendTimeToString(url: stringUrl, date: date, includingRange: false,range: 0, q : false)
        stringUrl = stringUrl.appending("&include_foodgroup_summary=1")
        
        return URL(string: stringUrl)!
    }
    
    private func getFoodScoreUrlWith(userID : String, token : String, startDate: Date, endDate: Date, range: Int) -> URL {
        var stringUrl = "\(kNourishApiAddress)\(kNourishFoodScore)\(kNourishUserID)" + userID
        stringUrl = appendTimeToStringWith(url: stringUrl, startDate: startDate, endDate: endDate, includingRange: true,range: range, q: false, dayCut: false)
        return URL(string : stringUrl)!
    }
    
    private func getCaloriesUrl(userID : String, token : String, startDate : Date, endDate : Date, range : Int) -> URL {
        var stringUrl = "\(kNourishApiAddress)\(kCaloriesScore)\(kNourishUserID)" + userID
        stringUrl = appendTimeToStringWith(url: stringUrl, startDate: startDate, endDate: endDate, includingRange: true, range: range, q: false, dayCut: true)
        stringUrl = self.addCalorieBudgetToUrl(url: stringUrl)
        return URL(string : stringUrl)!
    }
    
    private func getStoreGroupUrl(userID : String, token : String) -> URL {
        let stringUrl = "\(kNourishApiAddress)\(kStoreGroups)\(kNourishUserID)" + userID
        return URL(string : stringUrl)!
    }
    
    private func getFnndsSearchUrl(userID : String, token : String, search : String, limit : Int) -> URL {
        var stringUrl = "\(kNourishApiAddress)food\(kNourishUserID)" + userID
        stringUrl.append("&q=\(search)".addingPercentEncoding(withAllowedCharacters: .urlPathAllowed)!)
        stringUrl.append("&limit=\(limit)")
        return URL(string : stringUrl)!
        
    }
    
    private func getFnndsTagSearchUrl(userID : String, token : String, tags : [String]) -> URL {
        var stringUrl = "\(kNourishApiAddress)food\(kNourishUserID)" + userID
        let join = tags.joined(separator: "\",\"").addingPercentEncoding(withAllowedCharacters: .urlPathAllowed)
        stringUrl = stringUrl + "&q=[%22\(join!)%22]&limit=1"
        return URL(string : stringUrl)!
    }
    
    private func getDeleteGroupUrl(foodGroupID : String, userID : String, token : String) -> URL {
        let stringUrl = "\(kNourishApiAddress)\(kStoreGroups)" + "/" + "\(foodGroupID)\(kNourishUserID)" + userID
        return URL(string : stringUrl)!
        
    }
    /*
    private func addAuthorizationHeader(url : String, token : String) -> String {
        let authorizedUrl = url.appending(kNourishToken + token)
        return self.addCalorieBudgetToUrl(url: authorizedUrl)
    }
    */
    private func addCalorieBudgetToUrl(url : String) -> String {
        if defaults.object(forKey: kCalorieBudgetKey) != nil {
            let calorieString = String(format: "&calorie_consumption_goal=%0.f", defaults.double(forKey: kCalorieBudgetKey))
            let authorizedUrl = url.appending(calorieString)
            return authorizedUrl
        } else {
            return url
        }
    }
    
    private func addLocalTime(stringUrl : String, local : String) -> String {
        let url = stringUrl + local + self.getLocalTimeString()
        return url
    }
    
    private func addGrams(url : String, grams : Double) -> String {
        let urlReturn = url + kGrams + String(format : "%.1f", grams)
        return urlReturn
    }
    
    private func addID(url : String, idString : String) -> String {
        return url + "&id=" + idString
    }
    
    private func appendTimeToString(url : String, date : Date, includingRange : Bool,range : Int, q : Bool) -> String {
        let calendar = Calendar.current
        let startOfDay = calendar.startOfDay(for: date)
        var components = DateComponents()
        components.day = 1
        components.second = -1
        let endDate: Date = calendar.date(byAdding: components, to: startOfDay)!
        let startInterval = String(Int64(startOfDay.timeIntervalSince1970 * 1000.0))
        let endInterval = String(Int64(endDate.timeIntervalSince1970 * 1000.0))
        let localTime = getLocalTimeString()
        let dayString = self.getDayString(date: date)
        let rangeString = includingRange ? "&range=\(range)" : ""
        var stringToReturn = url.appending(kStartString + startInterval + kEndString + endInterval + rangeString)
        let local = q == false ? kLocalTime : kLocalTimeQ
        stringToReturn = stringToReturn.appending(local + String(localTime) + kDayString + String(dayString))
        return stringToReturn
    }
    
    private func appendTimeToStringWith(url : String, startDate : Date, endDate : Date, includingRange : Bool,range: Int, q : Bool, dayCut : Bool) -> String {
        let startInterval = String(Int64(startDate.timeIntervalSince1970 * 1000.0))
        let endInterval = String(Int64(endDate.timeIntervalSince1970 * 1000.0))
        let localTime = getLocalTimeString()
        let dayString = self.getDayString(date: endDate)
        let rangeString = includingRange ? "&range=\(range)" : ""
        var stringToReturn = url.appending(kStartString + startInterval + kEndString + endInterval + rangeString)
        let local = q == false ? kLocalTime : kLocalTimeQ
        if dayCut == true {
            stringToReturn = stringToReturn.appending(local + String(localTime))
        } else {
            stringToReturn = stringToReturn.appending(local + String(localTime) + kDayString + String(dayString))
        }
        
        return stringToReturn
    }
    
    private func appendCount(url : String, count : Int) -> String {
        let stringToReturn = url.appending("\(kCountString)\(count)")
        return stringToReturn
    }
    
    private func appendDateTime(url : String, date : Date) -> String {
        let interval = Int64(date.timeIntervalSince1970 * 1000)
        let stringToReturn = url.appending("\(kDateTime)\(interval)")
        return stringToReturn
    }
    
    private func dateTime(date : Date) -> Int64  {
        return Int64(date.timeIntervalSince1970 * 1000)
    }
    
    private func getLocalTimeString() -> String {
        let formatter = DateFormatter()
        formatter.dateFormat = "HHmmss"
        let localTime = formatter.string(from: Date())
        return localTime
    }
    
    private func getDayString(date : Date) -> String {
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyyMMdd"
        let string = formatter.string(from: date)
        return string
    }
    
    private func errorFromBackend(dict : [String : Any], errorCode : Int) -> Error {
        let errorToReturn = NSError(domain: Bundle.main.bundleIdentifier ?? "", code: errorCode, userInfo: dict)
        return errorToReturn
    }
    
}

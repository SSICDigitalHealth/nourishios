//
//  UserPhotosInteractor.swift
//  NourIQ
//
//  Created by Gena Mironchyk on 4/3/18.
//  Copyright © 2018 SSIC. All rights reserved.
//

import Foundation
class UserPhotosInteractor {
    private let fileManager = FileManager.default
    private let documentsDirectory: String = (NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0] as String)
    private let currentDate = Date()
    private let calendar = Calendar.current
    
    func execute() {
        self.processMealPhotos()
        self.processReciepesPhotos()
    }
    
    
    
    
    private func processMealPhotos () {
        let photosDir = self.documentsDirectory + "/mealPhotos"
        let enumerator = self.fileManager.enumerator(atPath: photosDir)
        while let file = enumerator?.nextObject() as? String {
            let filePath = String(format : "%@/%@",photosDir,file)
            self.processFileAt(filePath: filePath)
        }
    }
    
    
    private func processReciepesPhotos () {
        let enumerator = self.fileManager.enumerator(atPath: self.documentsDirectory)
        while let file = enumerator?.nextObject() as? String {
            if file.hasSuffix(".png")  {
                let filePath = String(format : "%@/%@",self.documentsDirectory,file)
                self.processFileAt(filePath: filePath)
            }
        }
    }
    
    
    private func processFileAt(filePath : String) {
        let attributes = try! self.fileManager.attributesOfItem(atPath: filePath) as NSDictionary
        if let creationDate = attributes.fileCreationDate() {
            if self.getDaysBetween(date: creationDate, secondDate: self.currentDate) >= 30 {
                try! self.fileManager.removeItem(atPath: filePath)
            }
        }
    }
    
    private func getDaysBetween(date: Date, secondDate : Date) -> Int {
        let comps = self.calendar.dateComponents([.day], from: date, to: secondDate)
        return comps.day ?? 0
    }
    
}

//
//  PulsatingView.swift
//  Nourish
//
//  Created by Vino (Vinodha) Sundaramoorthy on 6/16/17.
//  Copyright © 2017 SSIC. All rights reserved.
//

import UIKit

class PulsatingView: UIView {

    var circle : CAShapeLayer!
    override func draw(_ rect: CGRect) {
        let context = UIGraphicsGetCurrentContext()
        context!.clear(rect)
        context!.setFillColor(UIColor.white.cgColor)
        context!.fill(rect)
        
        let startAngle = 0
        let endAngle = CGFloat(360).degreesToRadians
        self.drawArc(context: context!, startAngle: CGFloat(startAngle), endAngle: endAngle, color: UIColor.red, value: 1)
    }
    
    func drawArc(context : CGContext, startAngle : CGFloat, endAngle : CGFloat, color : UIColor, value : CGFloat) {
        let center = CGPoint(x:bounds.width/2 , y: bounds.size.height)
        
        let radius: CGFloat = 300
        let path = UIBezierPath(arcCenter: center,
                                radius: radius ,
                                startAngle: startAngle,
                                endAngle: endAngle,
                                clockwise: true)
        circle = CAShapeLayer()
        circle.path = path.cgPath
        circle.fillColor = NRColorUtility.voiceSearchPulseColor().cgColor
        self.layer.addSublayer(circle)
        self.startAnimation()
    }
    
    func startAnimation() {
        if self.circle != nil {
            self.circle.isHidden = false
            let startAngle = 0
            let endAngle = CGFloat(360).degreesToRadians
            let center = CGPoint(x:bounds.width/2 , y: bounds.size.height)
        
            let newPath = UIBezierPath(arcCenter: center,
                                   radius: 320 ,
                                   startAngle: CGFloat(startAngle),
                                   endAngle: endAngle,
                                   clockwise: true)
            let animation = CABasicAnimation(keyPath: "path")
            animation.fromValue = self.circle.path
            animation.toValue = newPath.cgPath
            animation.autoreverses = true
            animation.duration = 1
            animation.repeatCount = .infinity
            self.circle.add(animation, forKey: "animateRadius")
        } else {
            self.setNeedsDisplay()
        }
    }
    
    func stopAnimation() {
        self.circle.removeAllAnimations()
    }
    
    func stopListeningAnimation() {
        self.stopAnimation()
        self.circle.isHidden = true
    }

}

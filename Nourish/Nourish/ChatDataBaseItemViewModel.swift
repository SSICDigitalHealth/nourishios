//
//  ChatDataBaseItemViewModel.swift
//  Nourish
//
//  Created by Gena Mironchyk on 7/31/17.
//  Copyright © 2017 SSIC. All rights reserved.
//

import UIKit

class ChatDataBaseItemViewModel: NSObject {
    dynamic var date : Date = Date()
    dynamic var chatBoxType : chatBoxType = .typeString
    dynamic var content : String = ""
    dynamic var isFromUser : Bool = false
    dynamic var messageID : String = ""
    dynamic var userID : String = ""
    dynamic var lostWeightValue : Double = 0.0
    dynamic var targetLostWeightValue : Double = 0.0
    dynamic var photoID : String = ""

}

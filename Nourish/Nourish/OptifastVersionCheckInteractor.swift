//
//  OptifastVersionCheckInteractor.swift
//  Optifast
//
//  Created by Gena Mironchyk on 2/28/18.
//  Copyright © 2018 SSIC. All rights reserved.
//

import Foundation
import RxSwift
class OptifastVersionCheckInteractor {
    private let repo = OptifastCheckVersionRepository()
    
    func execute() -> Observable<OptifastCheckVerModel> {
        return self.repo.checkVersion()
    }
}

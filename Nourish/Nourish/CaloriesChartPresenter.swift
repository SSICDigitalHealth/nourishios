//
//  CaloriesChartPresenter.swift
//  Nourish
//
//  Created by Vlad Birukov on 08.08.17.
//  Copyright © 2017 SSIC. All rights reserved.
//

import Foundation
import RxSwift

class CaloriesChartPresenter: BasePresenter, CaloriesBarChartDelegate {
    
    let interactor = CaloricProgressInteractor()
    let objectToModelMapper = CaloricProgressModel_to_CaloriesViewModel()
    var caloricProgressViewModel = CaloriesViewModel()
    var caloriesChartView: CaloriesChartProtocol!
    var date: (Date, Date)?
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.getCaloriesForWeekDetail()
    }
    
    private func getCaloriesForWeekDetail() {
        if self.caloriesChartView.config() != nil {
            self.date = self.caloriesChartView.config()?.getDate()
        }
        
        if self.date != nil {
            let proxyDate = caloriesChartView?.config()?.date

            if Calendar.current.compare((self.date?.0)!, to: (self.date?.1)!, toGranularity: .day) != .orderedSame {
                self.subscribtions.append(self.interactor.execute(startDate: (self.date?.0)!, endDate: (self.date?.1)!).observeOn(MainScheduler.instance).subscribe(onNext: { [unowned self] caloriesForPeriod in
                    if proxyDate == self.caloriesChartView?.config()?.date {
                        self.caloricProgressViewModel = self.objectToModelMapper.transform(model: caloriesForPeriod)
                        
                        self.calculateMaxCal()
                        self.calculateAvgConsumed()
                        
                        self.caloriesChartView.reloadWith(dataSource: self)
                    }
                    
                    self.caloriesChartView.stopActivityAnimation()
                }, onError: {error in
                    DispatchQueue.main.async {
                        self.caloriesChartView.stopActivityAnimation()
                        self.caloriesChartView.parseError(error : error, completion: nil)
                    }
                }, onCompleted: {

                }, onDisposed: {
                    self.caloriesChartView.stopActivityAnimation()
                }))
            }
        }
    }
    
    let middleAxisMultiplier = 0.67
    
    var maxCal = Double()
    var avgConsumed = Double()
    
    func numberOfBars(in activityBarChart: CaloriesBarChart) -> Int {
        if self.caloriesChartView.config() != nil {
            self.date = self.caloriesChartView.config()?.getDate()
        }
        
        if let state = caloricProgressViewModel.caloricState {
            return state.count
        }
        else {
            if let date = self.date {
                return self.numberOfDaysIn(Interval: date.0, date.1)
            }
            else {
                return 0
            }
        }
    }
    
    func topAxisValue(in activityBarChart: CaloriesBarChart) -> CGFloat {
        return CGFloat(avgConsumed / (maxCal + Double.leastNonzeroMagnitude))
    }
    
    func activityBarChart(_ activityBarChart: CaloriesBarChart, barAtIndex: Int) -> CaloriesBarChart.Bar? {
        var consumedColor = NRColorUtility.hexStringToUIColor(hex: "#009247")
        var burnedLower: CGFloat = 0.0
        var consumedLower: CGFloat = 0.0
        var burnedHigher: CGFloat = 0.0
        var consumedHigher: CGFloat = 0.0
        var legenda: String {
            let formatter = DateFormatter()
            if let state = caloricProgressViewModel.caloricState {
                formatter.dateFormat = {
                    if state.count <= 7 {
                        if let date = self.date {
                            if Calendar.current.isDateInToday(date.1) == false {
                                return "E"
                            }
                        }
                    }
                    return "MM/dd"
                }()
                if state.count <= 7 || (barAtIndex == 1 || barAtIndex == state.count - 2) {
                    var index: Int {
                        if state.count > 7 && barAtIndex == 1 {
                            return 0
                        }
                        else if state.count > 7 && barAtIndex == state.count - 2 {
                            return state.count - 1
                        }
                        return barAtIndex
                    }
                    return formatter.string(from: state[index].subtitle).uppercased()
                }
            }
            return ""
        }
        if let state = caloricProgressViewModel.caloricState {
            burnedLower = CGFloat(state[barAtIndex].cal.bmrCal / (self.maxCal + Double.leastNonzeroMagnitude))
                
            burnedHigher = CGFloat(state[barAtIndex].cal.userBurnedCal / (self.maxCal + Double.leastNonzeroMagnitude)) + burnedLower
            
            if state[barAtIndex].cal.userConsumedCal > state[barAtIndex].cal.maxConsumedCal {
                consumedLower = CGFloat(state[barAtIndex].cal.maxConsumedCal / (self.maxCal + Double.leastNonzeroMagnitude))
            } else {
                consumedLower = CGFloat(state[barAtIndex].cal.userConsumedCal / (self.maxCal + Double.leastNonzeroMagnitude))
            }
            
            if state[barAtIndex].cal.maxConsumedCal > state[barAtIndex].cal.userConsumedCal {
                consumedHigher = CGFloat(state[barAtIndex].cal.maxConsumedCal / (self.maxCal + Double.leastNonzeroMagnitude))
                if state[barAtIndex].cal.userConsumedCal == 0.0 {
                    consumedColor = NRColorUtility.hexStringToUIColor(hex: "#bfbfbf").withAlphaComponent(0.25)
                } else {
                    consumedColor = NRColorUtility.hexStringToUIColor(hex: "#70c397").withAlphaComponent(0.2)
                }
            } else {
                consumedHigher = CGFloat((state[barAtIndex].cal.userConsumedCal - state[barAtIndex].cal.maxConsumedCal) / (self.maxCal + Double.leastNonzeroMagnitude)) + consumedLower
            }
        }
        return (burned: (burnedLower, burnedHigher), consumed: (consumedLower, consumedHigher), legenda: legenda, consumedHidherColor: consumedColor)
    }
    
    func topLabel(in activityBarChart: CaloriesBarChart) -> String {
        return String(format: "%.0f kcal", self.avgConsumed)
    }
    
    func bottomLabel(in activityBarChart: CaloriesBarChart) -> String {
        return "0 kcal"
    }
    
    
    private func calculateMaxCal() {
        if let state = caloricProgressViewModel.caloricState {
            var maxBurned = 0.0
            var maxConsumed = 0.0
            
            let maxBurnedCalories = state.max(by: { (a, b) -> Bool in
                return (a.cal.userBurnedCal + a.cal.bmrCal) < (b.cal.userBurnedCal + b.cal.bmrCal)})
            if let state = maxBurnedCalories {
                maxBurned = state.cal.userBurnedCal + state.cal.bmrCal
            }
            
            let maxConsumedCalories = state.max(by: { (a, b) -> Bool in
                return (a.cal.userConsumedCal + a.cal.maxConsumedCal) < (b.cal.userConsumedCal + b.cal.maxConsumedCal)})
            
            if let state = maxConsumedCalories {
                
                if state.cal.userConsumedCal >= state.cal.maxConsumedCal {                    maxConsumed = state.cal.userConsumedCal
                } else {
                    maxConsumed = state.cal.maxConsumedCal
                }
            }
            
            self.maxCal = [maxConsumed, maxBurned].max(by: {(a, b) -> Bool in
                return a < b
            })!
        }
    }
    
    private func calculateAvgConsumed() {
        if let state = caloricProgressViewModel.caloricState {
            let sum = state.reduce(0.0, {$0 + $1.cal.maxConsumedCal})
            self.avgConsumed = sum / Double(state.count) + Double.leastNonzeroMagnitude
        }
    }
    
    private func numberOfDaysIn(Interval from: Date, _ to: Date) -> Int {
        let currentCalendar = Calendar.current
        
        guard let start = currentCalendar.ordinality(of: .day, in: .era, for: from) else { return 0 }
        guard let end = currentCalendar.ordinality(of: .day, in: .era, for: to) else { return 0 }
        
        return end - start
    }
    
//    func startOfDay(_ date: Date) -> Date {
//        let config = caloriesChartView.config()
//        
//        if config != nil {
//            if config?.period == .monthly {
//                let calendar = Calendar.current
//                
//                return calendar.startOfDay(for: date)
//            } else {
//                return date
//            }
//        } else {
//            return date
//        }
//    }
//    
//    func endOfDay(_ date: Date) -> Date {
//        let config = caloriesChartView.config()
//
//        if config != nil {
//            if config?.period == .monthly {
//                let calendar = Calendar.current
//                
//                var components = DateComponents()
//                
//                if config?.type == .report {
//                    components.second = -1
//
//                } else {
//                    components.second = 0
//                }
//                
//                return calendar.date(byAdding: components, to: startOfDay(date)) ?? date
//            } else {
//                return date
//            }
//        } else {
//            return date
//        }
//    }
}

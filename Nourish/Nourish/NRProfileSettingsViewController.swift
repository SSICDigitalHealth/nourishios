//
//  NRProfileSettingsViewController.swift
//  Nourish
//
//  Created by Vino (Vinodha) Sundaramoorthy on 11/9/16.
//  Copyright © 2016 SSIC. All rights reserved.
//

import UIKit

class NRProfileSettingsViewController: BasePresentationViewController {
    
    @IBOutlet weak var profileView : NRProfileSettingsView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = kProfileSettingsViewTitle
        self.setUpNavigationBarButtons()
        self.profileView.controller = self
        self.baseViews = [profileView]
    }
    
    func setUpNavigationBarButtons () {
        let titleAttribute = self.navigationBarTitleAttribute()
        let closeButton : UIBarButtonItem =  UIBarButtonItem(image: UIImage(named:"close_icon"), style: .plain, target: self, action: #selector(self.closeAction(_:)))
        closeButton.setTitleTextAttributes(titleAttribute, for: .normal)
        navigationItem.title = self.title
        self.navigationController?.navigationBar.titleTextAttributes = titleAttribute
        self.navigationController?.navigationBar.barTintColor = UIColor.white
        self.navigationController?.navigationBar.tintColor = UIColor.gray
        navigationItem.leftBarButtonItem = closeButton
    }
    
    //Save profile settings
    func saveAction(){
        self.profileView.saveUserModel()
    }
    
    override func closeAction(_ sender : UIButton){
        if self.profileView.presenter.hasChanges {
            DispatchQueue.main.async {
                let alert = UIAlertController(title: "Save Changes?", message: "You have made changes to your profile, would you like to update/save your changes? Click the “Save Profile” button below to update/save your profile data.", preferredStyle: UIAlertControllerStyle.alert)
                alert.addAction(UIAlertAction(title: "Yes", style: UIAlertActionStyle.default, handler: { action in
                    return
                }))
                alert.addAction(UIAlertAction(title: "No", style: UIAlertActionStyle.cancel, handler: { action in
                    self.dismiss(animated: true, completion: nil)
                }))
                self.present(alert, animated: true, completion: nil)
            }
        } else {
            self.dismiss(animated: true, completion: nil)
        }
    }
    
}


//
//  json_to_OptifastFoodSearchModel.swift
//  Nourish
//
//  Created by Gena Mironchyk on 10/20/17.
//  Copyright © 2017 SSIC. All rights reserved.
//

import UIKit

class json_to_OptifastFoodSearchModel: NSObject {
    func transform(dict : [String : Any]) -> foodSearchModel {
        var model = foodSearchModel()
        
        model.foodId = dict["id"] as! String
        model.caloriesPerGram = dict["kcal_per_gram"] as! Double
        model.amount = dict["msre_amount"] as! Double
        model.foodTitle = dict["name"] as! String
        model.grams = dict["msre_grams"] as? Double
        model.displayUnit = dict["msre_desc"] as! String
        
        if let optiInfo = dict["product_info"] as? [String : Any] {
            model.optifastHeader = optiInfo["header"] as! String
            model.optifastFooter = optiInfo["footer"] as! String
            model.optifastHightlights = (optiInfo["hightlights"] as! [String]).map({return $0})
            model.optifastIngredients = (optiInfo["ingredients"] as! String).components(separatedBy: ",")
            let path = optiInfo["image_src"] as! String
            if let pathToImage = Bundle.main.path(forResource: path, ofType: "") {
                model.optifastImage = UIImage(contentsOfFile: pathToImage)
            }
        }
        return model
    }
    
    func transform(jsons : [[String : Any]]) -> [foodSearchModel] {
        return jsons.map({return self.transform(dict: $0)})
    }
    
}

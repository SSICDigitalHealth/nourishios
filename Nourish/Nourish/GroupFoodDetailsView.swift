//
//  GroupFoodDetailsView.swift
//  Nourish
//
//  Created by Vino (Vinodha) Sundaramoorthy on 3/8/17.
//  Copyright © 2017 SSIC. All rights reserved.
//

import UIKit
import RxSwift

class GroupFoodDetailsView: BaseView , GroupFoodViewProtocol{

    @IBOutlet weak var nutrientDetailsTableView : UITableView!
    @IBOutlet weak var favButton : UIBarButtonItem!
    @IBOutlet weak var foodTitle : UILabel!
    @IBOutlet weak var foodServingQty : UILabel!
    @IBOutlet weak var foodImage : UIImageView!
    @IBOutlet weak var groupMealTableView : UITableView!
    @IBOutlet weak var groupHeightConstraint : NSLayoutConstraint!
    @IBOutlet weak var addToDiaryView : UIView!
    @IBOutlet weak var addToDiaryButton : UIView!
    @IBOutlet weak var addToDiaryHeightConstraint : NSLayoutConstraint!
    @IBOutlet weak var nutrientDetailsHeight : NSLayoutConstraint!
    @IBOutlet weak var foodViewHeight : NSLayoutConstraint!
    
    var fromFav : Bool = false
    var presenter = GroupedFoodDetailsPresenter()
    var viewController : GroupFoodDetailsViewController!
    var groupFoodDetails : groudFoodDetailsModel!
    
    override func viewWillAppear(_ animated: Bool) {
        self.basePresenter = presenter
        self.backgroundColor = NRColorUtility.nourishHomeColor()
        presenter.groupFoodView = self
        presenter.groupFoodDetails = self.groupFoodDetails
        nutrientDetailsTableView?.register(UINib(nibName: "NRFoodNutrientCell", bundle: nil), forCellReuseIdentifier: "kNutrientCell")
        groupMealTableView?.register(UINib(nibName: "GroupDetailsCell", bundle: nil), forCellReuseIdentifier: "kGroupDetails")
        self.nutrientDetailsTableView.delegate = presenter
        self.nutrientDetailsTableView.dataSource = presenter
        self.groupMealTableView.delegate = presenter
        self.groupMealTableView.dataSource = presenter
        self.presenter.fromFav = self.fromFav
        self.addToDiaryButton.backgroundColor = NRColorUtility.appBackgroundColor()
        self.addToDiaryView.backgroundColor = NRColorUtility.appBackgroundColor()
        if fromFav {
            addToDiaryView.isHidden = false
            addToDiaryHeightConstraint.constant = 72
        } else {
            addToDiaryView.isHidden = true
            addToDiaryHeightConstraint.constant = 0
        }
        super.viewWillAppear(animated)
    }
    
    func setupFavouriteButton () {
        self.favButton.target = self
        self.favButton.action = #selector(toggleFavourite)
        if self.groupFoodDetails.isFavorite {
            self.favButton.image = UIImage(named : "fav_green")
        } else {
            self.favButton.image = UIImage(named : "fav_icon_00-1")

        }
    }
    
    
    func setFoodImage(image : UIImage) {
        self.foodImage.image = image
        let _ = self.foodImage.addGradientWithHeight(height: 56)
        self.foodViewHeight.constant = 200.0
    }
    func hideFoodImage() {
        self.foodViewHeight.constant = 0.0
    }
    
    func loadNutrientDetails() {
        self.nutrientDetailsHeight.constant = CGFloat(self.presenter.foodNutrientsArray.count * 30)
        //Reload nutrient table
        self.nutrientDetailsTableView.reloadData()
    }
    
    func startActivity() {
        self.prepareLoadView()
    }
    
    func stopActivity() {
        self.stopActivityAnimation()
    }
    
    func loadGroupMealDetails() {
        self.groupHeightConstraint.constant = CGFloat(88 * self.presenter.groupFoodDetails.groupMealArray.count)
        self.groupMealTableView.reloadData()
    }

    func getGroupIndexPathForCell(cell:UITableViewCell) -> IndexPath?{
        return (self.groupMealTableView?.indexPath(for: cell))
    }
    
    func showFoodDetails(model : foodSearchModel) {
        self.viewController.showFoodDetails(model: model)
    }
    
    func addToDiaryForOcassion(ocasion: Ocasion, date : Date) {
        self.presenter.addToDiary(ocasion: ocasion, date : date)
    }
    
    func finishSaveToDiary() {
        self.viewController.finishSaveToDiary()
    }
    
    func popToMealDiary(){
        self.viewController.popToMealDiary()
    }
    
    @IBAction func toggleFavourite() {
        self.prepareLoadView()
        if self.groupFoodDetails.isFavorite {
            let _ = self.presenter.deleteFromFavorite().observeOn(MainScheduler.instance).subscribe(onNext: { [unowned self] success in
                    if success == true {
                        self.favButton = nil
                    }
                },  onError: {error in
                    print(error)
                }, onCompleted: {
                    self.stopActivityAnimation()
                    self.viewController.unfavoriteGroup()
                }, onDisposed: {})
        } else {
            let _ = self.presenter.addToFavourite().observeOn(MainScheduler.instance).subscribe(onNext: { [unowned self] success in
                if success == true {
                    self.favButton = nil
                }
            },  onError: {error in
                print(error)
            }, onCompleted: {
                self.stopActivityAnimation()
                self.viewController.unfavoriteGroup()
            }, onDisposed: {})

            }
    }
}


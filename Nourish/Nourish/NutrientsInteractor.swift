//
//  NutrientsInteractor.swift
//  Nourish
//
//  Created by Vlad Birukov on 11.09.17.
//  Copyright © 2017 SSIC. All rights reserved.
//

import Foundation
import RxSwift

class NutrientsInteractor {
    private let progressRepository = NestleProgressRepository.shared
    
    func execute(startDate: Date, endDate: Date) -> Observable <NutrientsModel> {
        return self.progressRepository.fetchNutrientsListFor(startDate:startDate, endDate:endDate)
//        print("#", startDate, endDate)
//        return Observable<NutrientsModel>.create{(observer) -> Disposable in
//            let pause = Int(arc4random_uniform(2))
//            let dispat = DispatchTime.now() + .seconds(pause)
//            DispatchQueue.main.asyncAfter(deadline: dispat, execute: {
//                let arr = [0.0, 2000.0, 500.0, 1800, 60.0, 900.0, 65.0, 0.0, 2000.0, 500.0, 1800, 60.0, 900.0, 65.0, 0.0, 2000.0, 500.0, 1800, 60.0, 900.0, 65.0, 0.0, 2000.0, 500.0, 1800, 60.0, 900.0, 65.0, 0.0, 2000.0, 500.0, 1800, 60.0, 900.0, 65.0]
//                let caption = ["Your top Calcium sources this month:", "Foods rich in calcium include:"]
//                let foodName = [["Orange juice", "Yogurt","Green vegetables"], ["Dairy products such as milk, cheese, and yogurt Dairy products such as milk, cheese, and yogurt", "Fish with soft bones that you eat, such as canned sardines and salmon"]]
//                
//                let nutriensModel = NutrientsModel()
//                
//                for _ in 0..<self.generateRandomBetween(max: 12, min: 5) {
//                    let nutrient = NutrientModel()
//                    nutrient.nameNutritional = "Calcium"
//                    nutrient.consumedNutritional = Double(self.generateRandomBetween(max: 2000, min: 1000))
//                    nutrient.targetNutritional = Double(self.generateRandomBetween(max: 2000, min: 1000))
//                    nutrient.unit = "mg"
//                    nutrient.message = "20% more than last month"
//                    nutrient.flagImage = true
//                    
//                    nutriensModel.nutrientChartData.append(nutrient)
//                    
//                    let nutrientDetailInformation = NutrientDetailInformationModel()
//                    nutrientDetailInformation.message = [String]()
//                    nutrientDetailInformation.message?.append("You have more calcium in your body than any other mineral. Calcium has many important jobs. The body stores more than 99 percent of its calcium in the bones and teeth to help make and keep them strong.")
//                    nutrientDetailInformation.message?.append("The rest is throughout the body in blood, muscle and the fluid between cells.")
//                    nutrientDetailInformation.message?.append("Your body needs calcium to help muscles and blood vessels contract and expand, to secrete hormones and enzymes and to send messages through the nervous system.")
//                    
//                    let dataChart = NutrientChartModel()
//                    dataChart.upperLimit = Double(self.generateRandomBetween(max: 2000, min: 1000))
//                    dataChart.lowerLimit = Double(self.generateRandomBetween(max: 900, min: 100))
//                    dataChart.unit = "mg"
//                    dataChart.dataCharts = [NutrientInformationChartModel]()
//                    for i in 0...self.daysBetweenDates(startDate: startDate, endDate: endDate)  {
//                        let data = NutrientInformationChartModel()
//                        data.label = self.addDay(startDate: startDate, number: i)
//                        data.consumed = arr[i]
//                        dataChart.dataCharts?.append(data)
//                    }
//                    
//                    nutrientDetailInformation.dataChart = dataChart
//                    let nutrientComeFrom = NutrientComeFromModel()
//                    nutrientComeFrom.caption = [String]()
//                    nutrientComeFrom.caption = caption
//                    nutrientComeFrom.nameFood = [[String]]()
//                    nutrientComeFrom.nameFood = foodName
//                    nutrientDetailInformation.nutrientComeFrom = nutrientComeFrom
//                    
//                    nutriensModel.nutrientDetail.append(nutrientDetailInformation)
//                }
//                
//                
//                
//                observer.onNext(nutriensModel)
//                observer.onCompleted()
//            })
//            
//            return Disposables.create()
//        }
        
    }

    private func generateRandomBetween(max : Int, min : Int) -> Int {
        return min + Int(arc4random_uniform(UInt32(max - min + 1)))
    }
    
    private func addDay(startDate: Date, number: Int) -> Date {
        return Calendar.current.date(byAdding: .day, value: number, to: startDate)!
    }
    
    private func daysBetweenDates(startDate: Date, endDate: Date) -> Int
    {
        let calendar = Calendar.current
        let components = calendar.dateComponents([.day], from: startDate, to: endDate)
        return components.day!
    }
    
}

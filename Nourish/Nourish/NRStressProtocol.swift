//
//  NRStressProtocol.swift
//  Nourish
//
//  Created by Vino (Vinodha) Sundaramoorthy on 12/5/16.
//  Copyright © 2016 SSIC. All rights reserved.
//

import UIKit

protocol NRStressProtocol {
    func setupWith(stressModel : NRStressModel)
}

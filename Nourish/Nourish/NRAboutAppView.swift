//
//  NRAboutAppView.swift
//  Nourish
//
//  Created by Vino (Vinodha) Sundaramoorthy on 11/29/16.
//  Copyright © 2016 SSIC. All rights reserved.
//

import UIKit
import RxSwift
class NRAboutAppView: BaseView {
    @IBOutlet weak var versionLabel : UILabel!
    @IBOutlet weak var copyRightsLabel : UILabel!
    @IBOutlet weak var appDescriptionTextView : UITextView!
    @IBOutlet weak var versionButton : UIButton!
    @IBOutlet weak var devModeLabel : UILabel!
    @IBOutlet weak var showTermsButton : UIButton!

    var controller = NRAboutAppViewController()
    var presenter = NRAboutAppPresenter()
    var counter = 1
    // MARK: Base Protocol Implementation
    override func viewDidLoad() {
        self.backgroundColor = NRColorUtility.appBackgroundColor()

        self.basePresenter = presenter
        presenter.appView = self
        self.versionButton.addTarget(self, action: #selector(enableDevMode), for: .touchUpInside)
        let appModel = presenter.setUpAppModel()
        let _ = EventLogger.userIsInDeveloperMode().observeOn(MainScheduler.instance).subscribe(onNext: { [unowned self] devModeEnabled in
            if devModeEnabled == true {
                self.versionButton.isEnabled = false
                self.devModeLabel.isHidden = false
            }
        }, onError: {error in}, onCompleted: {
            self.setUpAppWithModel(app: appModel)
        }, onDisposed: {})

    }
    
    func setUpAppWithModel(app : aboutAppModel) {
        versionLabel.text = app.appVersion
        copyRightsLabel.text = app.appCopyrightsString
        appDescriptionTextView.text = app.appDescription
        appDescriptionTextView.textColor = UIColor.white
        
    }
    
    func enableDevMode () {
        if counter >= 5 {
            self.presenter.enableDeveloperMode()
            self.versionButton.isEnabled = false
            self.devModeLabel.isHidden = false
        } else {
            counter += 1
        }
    }
    
}

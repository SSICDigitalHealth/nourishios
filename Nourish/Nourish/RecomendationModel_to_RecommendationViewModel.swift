//
//  RecomendationModel_to_RecommendationViewModel.swift
//  Nourish
//
//  Created by Vlad Birukov on 30.08.17.
//  Copyright © 2017 SSIC. All rights reserved.
//

import Foundation

class RecomendationModel_to_RecommendationViewModel {
    func transform(model: RecomendationModel) -> RecomendationViewModel {
        let recomendationViewModel = RecomendationViewModel()
        
        if model.tips.count > 0 {
            if let messages = model.tips[0].messages {
                recomendationViewModel.descriptionText = messages[0]
            }
            
            if let title = model.tips[0].tipTitle {
                recomendationViewModel.titleText = title
            }
        }
        
        return recomendationViewModel
    }
}

//
//  CaloriesIntakeMealsModel_to_CaloriesIntakeMealsViewModel.swift
//  Nourish
//
//  Created by Vlad Birukov on 29.08.17.
//  Copyright © 2017 SSIC. All rights reserved.
//

import Foundation

class CaloriesIntakeMealsModel_to_CaloriesIntakeMealsViewModel {
    func transform(model: CaloriesIntakeMealsModel) -> CaloriesIntakeMealsViewModel {
        let caloriesIntakeMealsModel = CaloriesIntakeMealsViewModel()
        
        if model.arrCaloriesMeal.count != 0 {
            for caloriesMeal in model.arrCaloriesMeal {
                caloriesIntakeMealsModel.arrCaloriesMeal.append(self.transform(model: caloriesMeal))
            }
        }
        return caloriesIntakeMealsModel
    }

    private func transform(model: CaloriesMealModel) -> CaloriesMealViewModel{
        let caloriesMealViewModel = CaloriesMealViewModel()
        caloriesMealViewModel.type = model.type
        caloriesMealViewModel.percent = model.percent
        
        return caloriesMealViewModel
    }
}

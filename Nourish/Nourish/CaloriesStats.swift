//
//  CaloriesStats.swift
//  Nourish
//
//  Created by Nova on 1/31/17.
//  Copyright © 2017 SSIC. All rights reserved.
//

import UIKit

class CaloriesStats: NSObject {
    var consumedCal : Double? = 0
    var day : Date? = nil
    var eerCal : Double? = 0
    var score : Double? = 0
    var breakDownCals : BreakDownCalories?
}

class BreakDownCalories : NSObject {
    var breakfastCals : Double? = 0
    var lunchCals : Double? = 0
    var dinnerCals : Double? = 0
    var snacksCals : Double? = 0
    var unknownCals : Double? = 0
}

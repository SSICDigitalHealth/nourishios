//
//  NRHealthProgressProtocol.swift
//  Nourish
//
//  Created by Vino (Vinodha) Sundaramoorthy on 12/1/16.
//  Copyright © 2016 SSIC. All rights reserved.
//

import UIKit

protocol NRHealthProgressProtocol {
    func setupWith(progressModel : NRHealthProgressModel)
}

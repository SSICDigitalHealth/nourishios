//
//  VoiceSearchListenerProtocol.swift
//  Nourish
//
//  Created by Vino (Vinodha) Sundaramoorthy on 6/16/17.
//  Copyright © 2017 SSIC. All rights reserved.
//

protocol VoiceSearchListenerProtocol {
    func startListening()
    func stopListenining()
    func speechInProgress(spokenResponse:String)
    func updateSpeechTranscript(text : String)
    func updateVoiceSearchStatus(text:String)
    func showMealListVC(modelArray:[MealRecord],date:Date,ocasion:Ocasion)
    func dismiss()
}

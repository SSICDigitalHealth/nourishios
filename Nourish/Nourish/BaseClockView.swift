//
//  BaseClockView.swift
//  Nourish
//
//  Created by Nova on 11/21/16.
//  Copyright © 2016 SSIC. All rights reserved.
//

import UIKit

extension Int {
    var degreesToRadians: Double { return Double(self) * .pi / 180 }
    var radiansToDegrees: Double { return Double(self) * 180 / .pi }
}
extension FloatingPoint {
    var degreesToRadians: Self { return self * .pi / 180 }
    var radiansToDegrees: Self { return self * 180 / .pi }
}


class BaseClockView: BaseView {
    var defaultArcWidth : CGFloat = 14.0
    var clockfaceCount : Int = 32
    var defaultLineWidth : CGFloat = 0.03
    var defaultLegendOffset : CGFloat = 40.0
    var legendArray : [legends] = []

    override func draw(_ rect: CGRect) {
        let context = UIGraphicsGetCurrentContext()
        context!.clear(rect)
        context!.setFillColor(UIColor.lightGray.cgColor)
        context!.fill(rect)
    }
    
    func drawShadeСlockFace(context : CGContext, colorArray : Array<UIColor>) {
        let step = CGFloat(360).degreesToRadians / CGFloat(self.clockfaceCount)
        let halfOffset = CGFloat(10).degreesToRadians
        if colorArray.count == 0 {
            for i in 0...clockfaceCount - 1 {
                self.drawArc(context: context, startAngle: CGFloat(i) * step + halfOffset, endAngle: 0.0, color: NRColorUtility.clockGrayColor(), value: defaultLineWidth)
            }
        } else {
            for i in 0...3 {
                let startA = CGFloat(i) * CGFloat(Double.pi/2) - CGFloat(Double.pi/2)
                for z in 0...7 {
                    self.drawArc(context: context, startAngle: CGFloat(z) * step + startA + halfOffset, endAngle: 0.0, color: colorArray[i], value: defaultLineWidth)
                }
            }
        }
        
    }
    
    func drawArc(context : CGContext, startAngle : CGFloat, endAngle : CGFloat, color : UIColor, value : CGFloat) {
        let center = CGPoint(x:bounds.width/2 , y: bounds.height/2 - defaultLegendOffset)
        
        let radius: CGFloat = max(bounds.width, bounds.height)/2
        
        let arcWidth: CGFloat = defaultArcWidth
        
        let realEndAngle : CGFloat = startAngle + self.valueToRadians(value: value)
        let path = UIBezierPath(arcCenter: center,
                                radius: radius - 100 - arcWidth/2,
                                startAngle: startAngle,
                                endAngle: realEndAngle,
                                clockwise: true)
        path.lineWidth = arcWidth
        color.setStroke()
        path.stroke()
    }
    
    func valueToRadians(value : CGFloat) -> CGFloat {
        return (2.0 * CGFloat(Double.pi) * value / 4)
    }
    
    func drawLegends () {
        var position = 0
        var currenty = self.bounds.height - defaultLegendOffset - 20
        
        for i in 0...legendArray.count-1  {
            let offsetHeight : CGFloat = 35.0
            let offsetWidth : CGFloat = 30.0
            
            var centerPoint = CGPoint.zero
            let legend = legendArray[i]
            if i % 2 == 0 {
                centerPoint.x = self.bounds.origin.x + offsetWidth
                centerPoint.y = currenty + (CGFloat(position) * offsetHeight)
                position = position + 1
            } else {
                centerPoint.x = UIScreen.main.bounds.width / 2
                centerPoint.y = currenty
            }
            
            currenty = centerPoint.y
            
            //Draw legend circle
            let legendCircle = NRDrawingUtility.drawCircularLegend(center: centerPoint, fillColor: legend.color.cgColor)
            self.layer.addSublayer(legendCircle)
            
            //Draw legend descriptions
            let legendFrame = CGRect(x: centerPoint.x + offsetWidth , y: centerPoint.y - 5, width: 120, height: 30)
            let legendFont = UIFont.systemFont(ofSize: 14)
            let legendDescription = NRDrawingUtility.drawTextLayer(frame: legendFrame, text: legend.description, color: NRColorUtility.legendTextColor().cgColor, bgColor: UIColor.clear.cgColor, font: legendFont)
            legendDescription.alignmentMode = "left"
            self.layer.addSublayer(legendDescription)
        }
    }
    
}

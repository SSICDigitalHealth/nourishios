//
//  ActivityExerciseCellPresenter.swift
//  Nourish
//
//  Created by Andrey Zmushko on 8/25/17.
//  Copyright © 2017 SSIC. All rights reserved.
//

import UIKit
import RxSwift


class ActivityExerciseCellPresenter: ActivityBaseCellPresenter {
    
    let interactor = ExerciseInteractor()
    let mapperObjectExercise = ExerciseModel_to_ExerciseViewModel()
    var exerciseViewModel = ExerciseViewModel()
    
    private func getInformation(cell: ActivityBaseTableViewCell, startDate: Date, endDate: Date) {
        cell.baseView.viewWillAppear(true)
        cell.baseView.removeActivityIndicator()
        self.subscribtions.append(self.interactor.execute(startDate: startDate, endDate: endDate).observeOn(MainScheduler.instance).subscribe(onNext: { [unowned self] exerciseActive in
            self.exerciseViewModel = self.mapperObjectExercise.transform(model: exerciseActive)
        }, onError: {error in
            cell.baseView.parseError(error: error,completion: nil)
        }, onCompleted: {
            self.setupViewForModel(cell: cell)
            if self.exerciseViewModel.activityList.count > 0 {
                self.reloadTable(cell: cell)
            }
            cell.baseView.stopActivityAnimation()
        }, onDisposed: {
        }))
    }
    
    override func fetchModel(For basecell: ActivityBaseCell) {
        super.fetchModel(For: basecell)
        
        let cell = basecell as! ActivityBaseTableViewCell
        
        cell.iconImageView.imageNamedWithTint(named: "exercise_icon", tintColor: UIColor.white)
        cell.iconFrame.backgroundColor = NRColorUtility.hexStringToUIColor(hex: "C02859")
        
        let title = NSMutableAttributedString(string: "Exercise", attributes: cell.boldAttr)
        cell.titleLabel.attributedText = title
        
        cell.bottomGap.constant = 16
        cell.separatorView.isHidden = false
        
        if let delegate = self.delegate {
            let date = delegate.fetchEpoch()
            self.getInformation(cell: cell, startDate: date.startDate, endDate: date.endDate)
            
        }
    }
    
    private func setupViewForModel(cell: ActivityBaseTableViewCell) {
        cell.valueLabel.attributedText = ActivitySleepCellPresenter().calculateHoursAndMins(numberHours: self.exerciseViewModel.activeHours, fitstAtrr: cell.boldAttr, secondAtrr: cell.bookAttr)
        
        if self.exerciseViewModel.activityList.count > 0 {
            var nameExercise = ""
            var exercixeTime = ""
            for i in 0..<self.exerciseViewModel.activityList.count {
                if i == self.exerciseViewModel.activityList.count - 1 {
                    nameExercise += self.exerciseViewModel.activityList[i].activityName
                    exercixeTime += calculateHoursAndMins(numberHours: self.exerciseViewModel.activityList[i].value, flag: false)
                } else {
                    nameExercise += self.exerciseViewModel.activityList[i].activityName + "\n"
                    exercixeTime += calculateHoursAndMins(numberHours: self.exerciseViewModel.activityList[i].value)
                }
            }
            
            cell.descriptionLabel.text = nameExercise
            cell.descriptionLabel.setLineSpacing(spacing: 8)
            cell.descriptionValueLabel.text = exercixeTime
            cell.descriptionValueLabel.setLineSpacing(spacing: 8, alignment: .right)
        }
    }
    
    private func calculateHoursAndMins(numberHours: Double, flag: Bool = true) -> String {
        let hours = Int(floor(numberHours))
        let mins = Int((numberHours * 60).truncatingRemainder(dividingBy: 60).roundToPlaces(places: 0))
        var sleepString = ""
        
        if hours > 0 || mins == 0 {
            sleepString += hours > 0 ? String(format: "%d",hours) : "0"
             sleepString += " hr"
        }
        
        if mins > 0 {
            sleepString += String(format: " %d min",mins)
        }
        
        if flag == true {
            sleepString += "\n"
        }
        
        return sleepString
    }

}

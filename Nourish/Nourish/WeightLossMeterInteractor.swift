//
//  WeightLossMeterInteracor.swift
//  Nourish
//
//  Created by Vino (Vinodha) Sundaramoorthy on 2/7/17.
//  Copyright © 2017 SSIC. All rights reserved.
//

import UIKit
import RxSwift
import HealthKit


class WeightLossMeterInteractor: NSObject {
    let userRepo = UserRepository.shared
    let activityRepo = UserActivityRepository()
    let mealRepo = MealHistoryRepository.shared
    
    /*
    func getWeightLossMeterModel() -> weightLossMeter {
        let meter = weightLossMeter(maxConsumedCal: 1500, maxBurnedCal: 300, userConsumedCal:0, userBurnedCal: 0)
        return meter
    }
    */
    func getWeightLossMeterFor(date : Date) -> Observable<weightLossMeter> {
        let weight = activityRepo.getUserWeightLast3MonthsFrom(date: date)
        let height = activityRepo.getUserHeightsLast3MonthsFrom(date: date)
        let calories = activityRepo.getActviveCaloriesFor(date: date)
        let user = userRepo.getCurrentUser(policy: .DefaultPolicy)
        let consumedCals = mealRepo.getCaloriesStatsFor(date: date)
        
        let zip = Observable.zip(weight, height, calories, user, consumedCals, resultSelector : {
            weightValue, heightValue, caloriesValue, userProfile, consumedCalsValue -> weightLossMeter in
            
            var weightValueToUse = 65.0
            var heightValueToUse = 1.65
            
            if userProfile.weight != nil {
                weightValueToUse = userProfile.weight!
            }
            
            if userProfile.height != nil {
                heightValueToUse = userProfile.height!
            }
            
            if weightValue != nil {
                weightValueToUse = weightValue!
            }
            
            if heightValue != nil {
                heightValueToUse = heightValue!
            }
            
            
            let bmr = userProfile.getBMRCalories(weight: weightValueToUse, height: heightValueToUse, date: Date(), isCurrentDay: false)
            
            let calorieNeed = userProfile.getWeightKeepCalorieBudget(bmrValue: bmr)
            
            let bmrDiff = calorieNeed - 1000
            var calorieBudget = 0.0
            var weeklyWeightLoss = 0.0
            
            if userProfile.userGoal == .LooseWeight {
                if bmrDiff < bmr {
                    calorieBudget = bmr
                } else {
                    calorieBudget = bmrDiff
                }
                
                weeklyWeightLoss = bmrDiff * 2 / bmr
            } else {
                calorieBudget = calorieNeed
            }
            
            LogUtility.logToFile("Calorie budget \(calorieBudget)")
            
            let activeCaloriesMinimal = userProfile.getActiveCalorieMinimum()
            
            var consCals = 0.0
            if consumedCalsValue.count > 0 {
                consCals = (consumedCalsValue.first?.consumedCal)!
            }
            
            let burnedCals = self.burnedCalories(samples: caloriesValue)
            
            let meter = weightLossMeter(maxConsumedCal: calorieBudget, maxBurnedCal: activeCaloriesMinimal, userConsumedCal: consCals, userBurnedCal: burnedCals, weeklyPlan: weeklyWeightLoss)
            
            return meter
            
        })
        
        return zip
        
    }
    
    func getDailyCalorieBudget() -> Observable<Double> {
        let weight = activityRepo.getUserWeight()
        let height = activityRepo.getUserHeight()
        let calories = activityRepo.getActiveCaloriesFor(period: .today)
        let user = userRepo.getCurrentUser(policy: .DefaultPolicy)
        let consumedCals = mealRepo.getCaloriesStatsFor(period: .today)
        
        let zip = Observable.zip(weight, height, calories, user, consumedCals, resultSelector : {
            weightValue, heightValue, caloriesValue, userProfile, consumedCalsValue -> Double in
            
            var weightValueToUse = 65.0
            var heightValueToUse = 1.65
            
            if userProfile.weight != nil {
                weightValueToUse = userProfile.weight!
            }
            
            if userProfile.height != nil {
                heightValueToUse = userProfile.height!
            }
            
            if weightValue != nil {
                weightValueToUse = weightValue!
            }
            
            if heightValue != nil {
                heightValueToUse = heightValue!
            }
            
            let bmr = userProfile.getBMRCalories(weight: weightValueToUse, height: heightValueToUse, date: Date(), isCurrentDay: false)
            
            let calorieNeed = userProfile.getWeightKeepCalorieBudget(bmrValue: bmr)
            
            let bmrDiff = calorieNeed - 1000
            var calorieBudget = 0.0
            
            if bmrDiff < bmr {
                calorieBudget = bmr
            } else {
                calorieBudget = bmrDiff
            }
            return calorieBudget
        })
        return zip
    }
    
    
    
    
    //tbd refactore
    func getRealWeighLossMeterModel() -> Observable<weightLossMeter> {
        let weight = activityRepo.getUserWeight()
        let height = activityRepo.getUserHeight()
        let calories = activityRepo.getActiveCaloriesFor(period: .today)
        let user = userRepo.getCurrentUser(policy: .DefaultPolicy)
        let consumedCals = mealRepo.caloriesFor(date: Date())
        
        let zip = Observable.zip(weight, height, calories, user, consumedCals, resultSelector : {
            weightValue, heightValue, caloriesValue, userProfile, consumedCalsValue -> weightLossMeter in
            
            var weightValueToUse = 65.0
            var heightValueToUse = 1.65
            
            if userProfile.weight != nil {
                weightValueToUse = userProfile.weight!
            }
            
            if userProfile.height != nil {
                heightValueToUse = userProfile.height!
            }
            
            if weightValue != nil {
                weightValueToUse = weightValue!
            }
            
            if heightValue != nil {
                heightValueToUse = heightValue!
            }
            
            
            let bmr = userProfile.getBMRCalories(weight: weightValueToUse, height: heightValueToUse, date: Date(), isCurrentDay: false)
            
            let calorieNeed = userProfile.getWeightKeepCalorieBudget(bmrValue: bmr)
            
            let bmrDiff = calorieNeed - 1000
            var calorieBudget = 0.0
            
            if bmrDiff < bmr {
                calorieBudget = bmr
            } else {
                calorieBudget = bmrDiff
            }
            
            calorieBudget = userProfile.calculateAndStoreCaloriePlan()
            LogUtility.logToFile("Calorie budget \(calorieBudget)")
            
            let weeklyWeightLoss = bmrDiff * 2 / bmr
            
            let activeCaloriesMinimal = userProfile.getActiveCalorieMinimum()
            
            let consCals = consumedCalsValue
            
            let burnedCals = self.burnedCalories(samples: caloriesValue)
            
            let meter = weightLossMeter(maxConsumedCal: calorieBudget, maxBurnedCal: activeCaloriesMinimal, userConsumedCal: consCals, userBurnedCal: burnedCals, weeklyPlan: weeklyWeightLoss)
            
            return meter
        
        })
        
     return zip
    }
    
    func burnedCalories(samples : [HKSample]) -> Double {
        var summary = 0.0
        for samlpe in samples {
            let key = samlpe as! HKQuantitySample
            summary = summary + key.quantity.doubleValue(for: HKUnit.kilocalorie())
        }
        return summary
    }

}

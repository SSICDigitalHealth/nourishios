//
//  ReportView.swift
//  Nourish
//
//  Created by Vino (Vinodha) Sundaramoorthy on 2/8/17.
//  Copyright © 2017 SSIC. All rights reserved.
//

import UIKit

class ReportView: BaseView,ReportsProtocol {
    @IBOutlet weak var reportsTableView : UITableView!

    var presenter = ReportsViewPresenter()
    var controller : ReportsViewController!
    var reports : [reportsModel]!
    
    override func viewWillAppear(_ animated: Bool) {
        reportsTableView.register(UITableViewCell.self, forCellReuseIdentifier: "reportsCell")
        self.basePresenter = presenter
        presenter.reportsView = self
        super.viewWillAppear(animated)
    }
    
    // MARK:ReportsProtocol
    func setReports(report: [reportsModel]) {
        self.reports = report
        DispatchQueue.main.async {
            self.reportsTableView.dataSource = self.presenter
            self.reportsTableView.delegate = self.presenter
            self.reportsTableView.reloadData()
            self.stopActivityAnimation()
        }
    }
   
    func showDetailedReport(date: Date) {
        self.controller.showDetailedReport(date: date)
    }

}

//
//  SamiSessionConstants.swift
//  Nourish
//
//  Created by Vino (Vinodha) Sundaramoorthy on 10/31/16.
//  Copyright © 2016 SSIC. All rights reserved.
//

import UIKit

let kSAMIAuthBaseUrl = "https://accounts.artik.cloud"
let kSamiRefreshUrl = "https://accounts.samsungsami.io/token?"
let kSAMIApiUrl = "https://api.artik.cloud/v1.1"
let kSAMIAuthPath = "/authorize?prompt=login"
let kSAMIRedirectUrl = ""
let kOAUTHAuthorizationHeader = "Authorization"
#if DEBUG
let kDHUrl = "https://api-proxy-dev.dh.ssic.io"
#else
let kDHUrl = "https://api-proxy-general.dh.ssic.io"
#endif
let kCheckArtikToken = "/tokens/"
let kBlacklistCheck = "/blacklistcheck"

#if OPTIFASTVERSION
let kAppParameter = "?appType=OptifastiOS"
let kSamiClientID = "3b781c969d37455f840704efb59efdac"
#else
let kAppParameter = "?appType=NourIQiOS"
let kSamiClientID = "c5692fa261e64588a8077686d338156d"
#endif

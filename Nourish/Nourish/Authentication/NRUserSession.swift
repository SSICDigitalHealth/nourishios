//
//  NRUserSession.swift
//  Nourish
//
//  Created by Vino (Vinodha) Sundaramoorthy on 11/1/16.
//  Copyright © 2016 SSIC. All rights reserved.
//

import UIKit
import SwiftKeychainWrapper
import RealmSwift
import RxSwift

let kUserCacheRefreshDateKey = "lastRefreshedUserDate"
class NRUserSession: NSObject {

    static let sharedInstance : NRUserSession = {
        let instance = NRUserSession()
        let artikDataStore = ArtikCloudConnectionManager()
        return instance
    }()
    
    var accessToken : String? {
        set (newValue) {
            KeychainWrapper.standard.set(newValue!, forKey: kAuthTokenConstantKey, withAccessibility: KeychainItemAccessibility.afterFirstUnlock)
            UserDefaults.standard.set(newValue, forKey: kAuthTokenConstantKey)
            UserDefaults.standard.synchronize()
        }
        get {
            let token = UserDefaults.standard.string(forKey: kAuthTokenConstantKey)
            if token == nil {
                return nil
            } else {
                return UserDefaults.standard.string(forKey: kAuthTokenConstantKey)
            }
        }
    }
    
    var userID : String? {
        set (newValue) {
            KeychainWrapper.standard.set(newValue!, forKey: kUserIDKey, withAccessibility: KeychainItemAccessibility.afterFirstUnlock)
            UserDefaults.standard.set(newValue, forKey: kUserIDKey)
            UserDefaults.standard.synchronize()
        }
        get {
            let token = UserDefaults.standard.string(forKey: kUserIDKey)
            if token == nil {
                return nil
            } else {
                return UserDefaults.standard.string(forKey: kUserIDKey)
            }
        }
    }
    
    var refreshToken : String? {
        set (newValue) {
            KeychainWrapper.standard.set(newValue!, forKey: kRefreshTokenConstantKey, withAccessibility: KeychainItemAccessibility.afterFirstUnlock)
        }
        get {
            return KeychainWrapper.standard.string(forKey: kRefreshTokenConstantKey)
        }
    }
    
    var tokenExpirationDate : Date? {
        set (newValue) {
            let interval = newValue?.timeIntervalSince1970
            KeychainWrapper.standard.set(interval!, forKey: kAuthTokenExpirationDateKey, withAccessibility: KeychainItemAccessibility.afterFirstUnlock)
        }
        get {
            if let interval = KeychainWrapper.standard.double(forKey: kAuthTokenExpirationDateKey) {
                return Date(timeIntervalSince1970 : interval)
            } else {
                return nil
            }
            
        }
    }
    
    var tokenRefreshDate : Date? {
        set (newValue) {
            let interval = newValue?.timeIntervalSince1970
            KeychainWrapper.standard.set(interval!, forKey: kAuthTokenRefreshDate, withAccessibility: KeychainItemAccessibility.afterFirstUnlock)
        }
        get {
            if let interval = KeychainWrapper.standard.double(forKey: kAuthTokenRefreshDate) {
                return Date(timeIntervalSince1970 : interval)
            } else {
                return nil
            }
            
        }
    }
    
    var tokenType : String? {
        set (newValue) {
            KeychainWrapper.standard.set(newValue!, forKey: kTokenTypeKey, withAccessibility: KeychainItemAccessibility.afterFirstUnlock)
        }
        get {
            return KeychainWrapper.standard.string(forKey: kTokenTypeKey)
        }
    }
    
    
    func isEmailLocked () -> Observable<Bool> {
        #if !OPTIFASTVERSION
            return Observable.just(false)
        #else
            if self.accessToken != nil {
                let artik = ArtikCloudConnectionManager()
                return artik.isEmailLocked(token: self.accessToken!)
            } else {
                return Observable.just(false)
            }
        #endif
    }
    
    
    var diaryDumpDate : Date? {
        set (newValue) {
            let interval = newValue?.timeIntervalSince1970
            defaults.setValue(interval, forKey: kDiaryDumpLastDate)
        }
        get {
            if let interval = defaults.value(forKey: kDiaryDumpLastDate) {
                let val = interval as? TimeInterval
                return Date(timeIntervalSince1970 : val!)
            } else {
                return nil
            }
            
        }
    }
    
    private func extLogic() {
        let date = defaults.value(forKey: kOnBoardingKeyDate) as? Date
        let startDate = Calendar.current.date(byAdding: .day, value: -1, to: Date())
        if date != nil && startDate != nil {
            if date! < startDate! {
                #if !OPTIFASTVERSION
                let logic = BackgroundDamperInteractor()
                logic.execute()
                #endif
            }
        }
    }
    
    func refreshArtikToken() {
        LogUtility.logToFile("attempt on refresh token")
        if self.accessToken != nil && self.refreshToken != nil {
            if self.needToBeRefreshed(date: self.tokenRefreshDate) == true {
                #if !OPTIFASTVERSION
                    self.extLogic()
                 #endif
                let artik = ArtikCloudConnectionManager()
                let _ = artik.refreshToken(refreshToken: self.refreshToken!, oldAccessToken: self.accessToken!).subscribe(onNext: { [unowned self] token in
                    if token != "" {
                        LogUtility.logToFile("New artik token : \(token)")
                    } else {
                        if let expDate = self.tokenExpirationDate {
                            if Date() > expDate {
                                self.processTokenRefreshError()
                            }
                        } else {
                            self.processTokenRefreshError()
                        }
                    }
                }, onError: {error in
                    self.processTokenRefreshError()
                }, onCompleted: {}, onDisposed: {})
            }
        } else {
            self.logout(keepCurrent: false)
            NavigationUtility.changeRootViewController(userProfile:nil,awakedFromBackground: false)
        }
    }
    
    private func needToBeRefreshed(date : Date?) -> Bool {
        var returnValue = true
        
        if date != nil {
            returnValue = !Calendar.current.isDateInToday(date!)
        }
        
        return returnValue
    }
    
    private func processTokenRefreshError() {
        LogUtility.logToFile("Token refresh failed, log out!")
        DispatchQueue.main.async {
            self.logout(keepCurrent: false)
            NavigationUtility.changeRootViewController(userProfile : nil,awakedFromBackground: false)
        }
    }
    
    private func typesToCleanup() -> [Object.Type] {
        var newTypes : [Object.Type] = []
        
        newTypes.append(MealRecordCache.self)
        newTypes.append(FavoriteCache.self)
        newTypes.append(FoodFavWithCounter.self)
        newTypes.append(MealPlannerCache.self)
        newTypes.append(OcasionData.self)
        newTypes.append(NestleFoodComponentsCache.self)
        
        
        newTypes.append(NestleProgressCache.self)
        newTypes.append(NestleRecommendationsCache.self)
        newTypes.append(OperationCache.self)
        newTypes.append(RecommendationCache.self)
        
        newTypes.append(RealmString.self)
        newTypes.append(ScoreCacheRealm.self)
        newTypes.append(StressCacheRealm.self)
        
        return newTypes
    }

    func cleanCache() {
        var types = [Object.Type]()
        
        types.append(Calories.self)
        types.append(CaloriesForDay.self)
        types.append(CaloriesHistory.self)
        types.append(ClarifaiCache.self)
        types.append(FNDDSSearchCache.self)
        types.append(FavoriteCache.self)
        types.append(FoodElement.self)
        types.append(FoodCardCache.self)
        types.append(FoodFavWithCounter.self)
        types.append(FoodImageObject.self)
        types.append(FoodRecordCache.self)
        types.append(FoodSearchCacheModel.self)
        types.append(MealRecordCache.self)
        types.append(Microelement.self)
        types.append(MicroelementHistoryObject.self)
        types.append(NestleCacheTipsModel.self)
        types.append(NestleFoodComponentsCache.self)
        types.append(NestleProgressCache.self)
        types.append(NestleRecommendationsCache.self)
        types.append(NewFoodCardCache.self)
        types.append(Nutrient.self)
        types.append(NutrientInfoRealm.self)
        types.append(NutritionScoreCache.self)
        types.append(RecommendationCache.self)
        types.append(ScoreCacheRealm.self)
        types.append(StressCacheRealm.self)
        types.append(RealmString.self)
        
        let realm = try! Realm()
        
        for type in types {
            if type != NestleRecommendationsCache.self {
                let values = realm.objects(type)
                try! realm.write {
                    realm.delete(values)
                }
            } else {
                let values = realm.objects(type)
                try! realm.write {
                    realm.delete(values, cascading: true)
                }
            }
            
        }
        LogUtility.logToFile("cache cleanedup")
        defaults.set(true, forKey: kForceCleanCache)
        defaults.synchronize()
    }
    
    
    //Logout the user
    func logout(keepCurrent : Bool) {
        
        let localNots = LocalNotificationsRoutine.shared
        localNots.removeAllNotifications()
        
        self.realmClearCache(objectTypes: self.typesToCleanup())
        let userRepo = RealmUserRepository()
        
        if keepCurrent == true {
            let user = userRepo.getDefaultUser()
            userRepo.deleteAllUsers(except: user)
        } else {
            userRepo.deleteAllCachedUsers()
        }
        
        KeychainWrapper.standard.removeObject(forKey: kActivitySourceKey)
        UserDefaults.standard.removeObject(forKey: kAuthTokenConstantKey)
        UserDefaults.standard.removeObject(forKey: firstShowKey)
        UserDefaults.standard.removeObject(forKey: onBoardingKey)
        UserDefaults.standard.removeObject(forKey: kOnBoardingKeyDate)
        UserDefaults.standard.removeObject(forKey: backgroundDateKey)
        UserDefaults.standard.removeObject(forKey: kCalorieBudgetKey)
        UserDefaults.standard.removeSuite(named: kDiaryDumpLastDate)
        UserDefaults.standard.removeObject(forKey: kLastSyncWithingsDateKey)
        UserDefaults.standard.removeObject(forKey: kUserCacheRefreshDateKey)
        KeychainWrapper.standard.removeObject(forKey: kAuthTokenConstantKey)
        KeychainWrapper.standard.removeObject(forKey: kAuthTokenExpirationDateKey)
        KeychainWrapper.standard.removeObject(forKey: kTokenTypeKey)
        KeychainWrapper.standard.removeObject(forKey: kRefreshTokenConstantKey)
        
        UserDefaults.standard.synchronize()
    }
    
    private func realmClearCache(objectTypes : [Object.Type]) {
        let realm = try! Realm()
        
        for type in objectTypes {
            let values = realm.objects(type)
            try! realm.write {
                realm.delete(values, cascading: true)
            }
        }
    }
}

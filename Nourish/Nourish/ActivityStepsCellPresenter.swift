//
//  ActivityStepsCellPresenter.swift
//  Nourish
//
//  Created by Andrey Zmushko on 8/25/17.
//  Copyright © 2017 SSIC. All rights reserved.
//

import UIKit
import RxSwift


class ActivityStepsCellPresenter: ActivityBaseCellPresenter {
    
    let interactor = StepsInteractor()
    let mapperObjectSteps = StepsModel_to_StepsViewModel()
    var stepsViewModel = StepsViewModel()
    
    private func getInformation(cell: ActivityBaseTableViewCell, startDate: Date, endDate: Date) {
        self.subscribtions.append(self.interactor.execute(startDate: startDate, endDate: endDate).observeOn(MainScheduler.instance).subscribe(onNext: { [unowned self] stepsActive in
            self.stepsViewModel = self.mapperObjectSteps.transform(model: stepsActive)
        }, onError: {error in
            cell.baseView.parseError(error: error, completion: nil)
        }, onCompleted: {
            self.setupViewForData(cell: cell)
            cell.baseView.stopActivityAnimation()
        }, onDisposed: {
        }))
    }
    
    
    override func fetchModel(For basecell: ActivityBaseCell) {
        super.fetchModel(For: basecell)
        
        let cell = basecell as! ActivityBaseTableViewCell
        
        cell.iconImageView.imageNamedWithTint(named: "steps_icon", tintColor: UIColor.white)
        cell.iconFrame.backgroundColor = UIColor.black
        
        cell.titleLabel.attributedText = NSMutableAttributedString(string: "Steps", attributes: cell.boldAttr)
        
        if let delegate = self.delegate {
            let date = delegate.fetchEpoch()
            cell.baseView.viewWillAppear(true)
            cell.baseView.removeActivityIndicator()
            getInformation(cell: cell, startDate: date.startDate, endDate: date.endDate)
            
            if Calendar.current.compare(date.startDate, to: date.endDate, toGranularity: .day) != .orderedSame {
                cell.descriptionLabel.text = "Avg. steps/day:"
                cell.bottomGap.constant = 0
            }
        }
    }
    
    private func setupViewForData(cell: ActivityBaseTableViewCell) {
        if let stepsNumber = self.stepsViewModel.numberSteps {
            cell.valueLabel.attributedText = NSMutableAttributedString(string: String(format: "%.0f", stepsNumber), attributes: cell.boldAttr)
        }
        
        if let avgStepsNumber = self.stepsViewModel.avgStepsDay {
            if let delegate = self.delegate {
                let date = delegate.fetchEpoch()
                
                if Calendar.current.compare(date.startDate, to: date.endDate, toGranularity: .day) != .orderedSame {
                    
                    cell.descriptionValueLabel.text = String(format: "%.0f", avgStepsNumber)
                }
            }
        }
        cell.baseView.stopActivityAnimation()
    }
}

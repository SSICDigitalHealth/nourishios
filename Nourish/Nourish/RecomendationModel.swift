//
//  RecomendationModel.swift
//  Nourish
//
//  Created by Andrey Zmushko on 7/21/17.
//  Copyright © 2017 SSIC. All rights reserved.
//

import UIKit

class RecomendationModel: NSObject {
    var caption: String?
    var meals : [foodSearchModel] = []
    var tips : [NestleTipsModel] = []
    var dataRecipies: [RecipiesModel]?
}

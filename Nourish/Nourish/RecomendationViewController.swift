//
//  RecomendationViewController.swift
//  Nourish
//
//  Created by Andrey Zmushko on 7/28/17.
//  Copyright © 2017 SSIC. All rights reserved.
//

import UIKit

class RecomendationViewController: BaseCardViewController {

    @IBOutlet var presenter: RecomendationDetailsPresenter!
    @IBOutlet weak var recomendationDetailView: BaseView!
    @IBOutlet weak var cardNavigationBar: NewCardNavigationBarView!

    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.baseViews = [cardNavigationBar, recomendationDetailView]
        
        for v in baseViews! {
            v.viewDidLoad()
        }
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.setUpNavigationBar(title: (self.progressConfig?.recomendationTitle())!, style: .other, navigationBar: self.cardNavigationBar)

        self.recomendationDetailView.basePresenter = self.presenter
        self.presenter.recomendationDetailViewController = self
        self.presenter.config = self.progressConfig
        super.viewWillAppear(animated)

    }
    
}

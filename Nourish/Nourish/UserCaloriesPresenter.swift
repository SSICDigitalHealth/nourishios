//
//  UserCaloriesPresenter.swift
//  Nourish
//
//  Created by Vlad Birukov on 07.06.17.
//  Copyright © 2017 SSIC. All rights reserved.
//

import Foundation
import RxSwift


class UserCaloriesPresenter: BasePresenter {
    
    let objectToModelMapper = CaloricProgressModel_to_CaloriesViewModel()
    let interactor = CaloricProgressInteractor()
    var userCaloriesView: UserCaloriesProtocol!
    var date: (Date, Date)?
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.getUserCalories()
    }
    
    private func getUserCalories(){
        if self.userCaloriesView.config() != nil {
            self.date = self.userCaloriesView.config()?.getDate()
        }
        
        if let view = self.userCaloriesView {
            if self.date != nil {
                let proxyDate = userCaloriesView?.config()?.date

                if Calendar.current.compare((self.date?.0)!, to: (self.date?.1)!, toGranularity: .day) == .orderedSame {
                    self.subscribtions.append(self.interactor.execute(startDate: (self.date?.0)!, endDate: (self.date?.1)!).observeOn(MainScheduler.instance).subscribe(onNext: { [weak self] caloriesModel in
                        if proxyDate == self?.userCaloriesView?.config()?.date {
                            view.setupWithModel(model: (self?.objectToModelMapper.transform(model: caloriesModel))!)
                        }
                        view.stopActivityAnimation()
                }, onError: {error in
                    view.stopActivityAnimation()
                }, onCompleted: {
                }, onDisposed: {
                    view.stopActivityAnimation()
                }))
                }
            }
        }
    }
}

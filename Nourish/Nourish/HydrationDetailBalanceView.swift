//
//  HydrationDetailBalanceView.swift
//  Nourish
//
//  Created by Vlad Birukov on 16.06.17.
//  Copyright © 2017 SSIC. All rights reserved.
//

import UIKit

protocol HydrationDetailBalanceProtocol : BaseViewProtocol {
    func reloadTable()
    func registerTableViewCells()
    func config() -> ProgressConfig?
    func setHydrationMessage(message : String)
}

class HydrationDetailBalanceView: BaseView, HydrationDetailBalanceProtocol {
    @IBOutlet weak var detailTableView: UITableView!
    @IBOutlet var presenter: HydrationDetailBalancePresenter!
    @IBOutlet weak var hydrationLabel : UITextView!
    @IBOutlet weak var bckgImageContainer : UIView!
    @IBOutlet weak var ideaImageView : UIImageView!
    var progressConfig: ProgressConfig?
    
    @IBOutlet weak var heightTableView: NSLayoutConstraint!


    func config() -> ProgressConfig? {
        return self.progressConfig
    }
    
    override func viewDidLoad() {
        self.xibAutoLayouting(nibName: "HydrationDetailBalanceView")
        self.hydrationLabel.text = ""
        self.ideaImageView.imageNamedWithTint(named: "idea", tintColor: UIColor.white)
        self.bckgImageContainer.layer.cornerRadius = self.bckgImageContainer.bounds.size.height / 2
    }
    
    func setHydrationMessage(message: String) {
        if self.hydrationLabel.text == "" {
            self.hydrationLabel.text = message
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.basePresenter = presenter
        self.presenter.hydrationDetailView = self
        self.registerTableViewCells()
        self.settingTableCell()

        super.viewWillAppear(animated)
    }
    
    
    func reloadTable() {
        self.detailTableView.reloadData()
    }
    
    func registerTableViewCells() {
        self.detailTableView?.register(UINib(nibName: "HydrationDetailTableViewCell", bundle: nil), forCellReuseIdentifier: "cellHydrationDetail")
    }
    
    func settingTableCell() {
        self.detailTableView.rowHeight = UITableViewAutomaticDimension
        self.detailTableView.estimatedRowHeight = 30
    }
}

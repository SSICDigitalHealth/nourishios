//
//  ChatTableView.swift
//  Nourish
//
//  Created by Gena Mironchyk on 2/9/17.
//  Copyright © 2017 SSIC. All rights reserved.
//

import UIKit


class ChatTableView: UITableView {
    let customRowAnimation : UITableViewRowAnimation = UITableViewRowAnimation(rawValue: UITableViewRowAnimation.automatic.rawValue  + 1)!
    
    override func insertRows(at indexPaths: [IndexPath], with animation: UITableViewRowAnimation) {
        if (animation != customRowAnimation) {
            super.insertRows(at: indexPaths, with: animation)
        } else {
            
        }
    }
    
    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */

}

//
//  StressCaptionView.swift
//  Nourish
//
//  Created by Vlad Birukov on 17.08.17.
//  Copyright © 2017 SSIC. All rights reserved.
//

import UIKit

protocol StressCaptionProtocol : BaseViewProtocol{
    func setupWith(model: [DailyStressModel])
}

class StressCaptionView: BaseView, StressCaptionProtocol {

    @IBOutlet weak var minLabel: UILabel!
    @IBOutlet weak var avgLabel: UILabel!
    @IBOutlet weak var maxLabel: UILabel!

    override func viewDidLoad() {
        self.xibAutoLayouting(nibName: "StressCaptionView")
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }

   
    func setupWith(model: [DailyStressModel]) {
        if model.count != 0 {
            self.maxLabel.text = String(format: "%0.fbpm", (model.max(by: { $0.bpmValue! < $1.bpmValue! })?.bpmValue)!)
            self.minLabel.text = String(format: "%.0fbpm", (model.min(by: { $0.bpmValue! < $1.bpmValue! })?.bpmValue)!)
            self.avgLabel.text = String(format: "%dbpm", (Int(model.reduce(0, {$0 + $1.bpmValue!})) / model.count))
        }
    }
}

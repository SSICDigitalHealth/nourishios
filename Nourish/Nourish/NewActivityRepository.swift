//
//  NewActivityRepository.swift
//  Nourish
//
//  Created by Nova on 9/15/17.
//  Copyright © 2017 SSIC. All rights reserved.
//

import UIKit
import RxSwift
import RealmSwift
import HealthKit
import SwiftKeychainWrapper

#if OPTIFASTVERSION
let kFitBitKey = "opti"
#else
let kFitBitKey = "nouri"
#endif

class NewActivityRepository: NSObject {
    let activityRepo = UserActivityRepository()
    let userRepo = UserRepository.shared
    let nourishDS = NourishDataStore.shared
    let nestleRepo = NestleProgressRepository.shared
    
    static let shared = NewActivityRepository()
    
    func distanceModel(startDate : Date, endDate : Date) -> Observable<DistanceModel> {
        let distance = self.fetchDistanceSamples(startDate: startDate, endDate: endDate)
        let user = self.userRepo.getCurrentUser(policy: .Cached)
        
        return Observable.zip(distance, user, resultSelector: {dstn, userFetched -> DistanceModel in
            let model = DistanceModel()
            model.isMetric = userFetched.metricPreference == .Metric
            
            var totalDistance = 0.0
            let activitySource = self.activitySourceString()
            let filterDistance = dstn.filter{$0.sourceRevision.source.bundleIdentifier.contains(activitySource) || $0.sourceRevision.source.name.lowercased().contains(activitySource)}
            let distanceSample = filterDistance.count > 0 ? filterDistance : dstn
            for sample in distanceSample {
                let quantityValue = sample as! HKQuantitySample
                totalDistance = totalDistance + quantityValue.quantity.doubleValue(for: HKUnit.meter())
            }
            model.distance = totalDistance / 1000
            let day = self.separateSamplesWithDays(samples: distanceSample)
            let dem = day > 0 ? day : 1
            model.avgDistance = (totalDistance / Double(dem)) / 1000
            
            return model
        })
    }
    
    func stayActiveModel(startDate : Date, endDate : Date) -> Observable<StayActiveModel> {
        return self.userRepo.getCurrentUser(policy: .DefaultPolicy).flatMap{userProfile -> Observable<StayActiveModel> in
            let token = NRUserSession.sharedInstance.accessToken
            let calories = self.nourishDS.getCaloriesStats(startDate: startDate, endDate: endDate, token: token!, userId: userProfile.userID!, range: 1)
            let steps = self.stepsModelFrom(startDate: startDate, endDate: endDate)
            let zip = Observable.zip(calories, steps, resultSelector: {calsData, stepsTotal -> StayActiveModel in
                let calorieModels = self.calorieStatsFrom(data: calsData)
                var cals = 0.0
                for model in calorieModels {
                    cals += model.consumedCal ?? 0.0
                }
                
                let budget = defaults.double(forKey: kCalorieBudgetKey)
                
                var goal = 6000.0
                
                if cals > budget {
                    goal = 6000.0 + (cals - budget) / 500 * 10000
                }
                
                let todaySteps = stepsTotal.numberSteps ?? 0.0
                
                var final = goal - todaySteps
                
                if final > 13000 {
                    final = 13000
                }
                
                let model = StayActiveModel()
                model.currentStepsToGoal = final
                return model
            })
            return zip
            
        }
    }
        
//        let consumed = self.nestleRepo.fetchConsumedCaloriesFor(starDate: startDate, endDate: endDate)
//        let steps = self.stepsModelFrom(startDate: startDate, endDate: endDate)
//        let zip = Observable.zip(consumed, steps, resultSelector: {cals, stepsTotal -> StayActiveModel in
//            let budget = defaults.double(forKey: kCalorieBudgetKey)
//
//            var goal = 6000.0
//
//            if cals > budget {
//                goal = 6000.0 + (cals - budget) / 500 * 10000
//            }
//
//            let todaySteps = stepsTotal.numberSteps ?? 0.0
//
//            var final = goal - todaySteps
//
//            if final > 13000 {
//                final = 13000
//            }
//
//            let model = StayActiveModel()
//            model.currentStepsToGoal = final
//            return model
//        })
//        return zip
    
    func caloricBurnModel(startDate : Date, endDate : Date) -> Observable<CaloricBurnedModel> {
        let activeCalories = self.fetchActiveCalories(startDate: startDate, endDate: endDate)
        let basalCalories = self.fetchBasalCalories(startDate: startDate, endDate: endDate)
        let numberOfDays = self.daysBetweenDates(date1: startDate, date2: endDate)
        let basalCaloriesToday = self.bmrForFullDay()
        let bmrForDate = self.getBMRForDate(date: startDate)
        
        let calendar = Calendar.current
        
        let zip = Observable.zip(activeCalories, basalCalories,basalCaloriesToday,bmrForDate, resultSelector: {active, basal,basalToday,bmrForDateResult -> CaloricBurnedModel in
            
            let model = CaloricBurnedModel()
            
            var totalBasal = self.totalCalories(samples: basal)
            
            if totalBasal == 0 {
                totalBasal = bmrForDateResult * numberOfDays
            }
            
            let totalActive = self.totalCalories(samples: active)
            
            if calendar.isDateInToday(startDate) {
                totalBasal = basalToday
            }
            
            let total = totalBasal + totalActive
            
            model.userBurnedCal = total
            
            if numberOfDays <= 1 {
                if calendar.isDateInToday(startDate) {
                    
                    model.caloricState = self.caloricStateFrom(activeCals: active, basalTotal: totalBasal)
                } else {
                    let bmrHour = bmrForDateResult / 24.0
                    var caloricArray : [(subtitle: Date, cal: (basalCalories: Double, activeCalories: Double))] = []
                    
                    for i in 0...23 {
                        //let basals = self.fetchSampleForHourIndex(index: i, samples: basal)
                        let actives = self.fetchSampleForHourIndex(index: i, samples: active)
                        
                        caloricArray.append((subtitle: startDate, cal: (basalCalories: bmrHour, activeCalories: self.totalCalories(samples: actives))))
                    }
                    model.caloricState = caloricArray
                }
                
                
            } else { //daily /weekly
                model.avgActiveCal = self.totalCalories(samples: active, avg: true)
                model.avgBurnedCal = total / numberOfDays
            }
            
            return model
        })
        
        return zip
    }
    
    func weightModel(startDate : Date, endDate : Date) -> Observable<WeightModel> {
        let cal = Calendar.current
        
        let user = self.userRepo.getCurrentUser(policy: .DefaultPolicy)
        let tuple = self.weightStats(startDate: startDate, endDate: endDate)
        
        let zip =  Observable.zip(user, tuple, resultSelector: {userValue, tupleValue -> WeightModel in
            
            let model = WeightModel()
            model.isMetric = userValue.metricPreference == .Metric
            model.userId = userValue.userID
     
            if self.daysBetweenDates(date1: startDate, date2: endDate) == 1 {
                if userValue.userGoal == .LooseWeight {
                    if (tupleValue?.count)! > 0 {
                        if let value = tupleValue?[0].value {
                            model.progress = value - userValue.startWeight!
                        }
                    }
                }
                model.weight = tupleValue
                if cal.isDateInToday(startDate) {
                    if let singleTuple = (tupleValue?.filter {$0.0 == startDate})?.first {
                        model.dailyWeightLogged = (singleTuple.1 != nil)
                    }
                }
            } else {
                model.weight = tupleValue
            }
            return model
            
        })
        
        return zip
    }
    
    private func weightStats(startDate : Date, endDate : Date) -> Observable<[(subtitle:Date, value:Double?)]?> {
        return Observable.create { observer in
            
            var dates = [startDate]
            if self.daysBetweenDates(date1: startDate, date2: endDate) > 1 {
                dates = self.datesArrayFrom(startDate: startDate, endDate: endDate)
            }
            
            ActivityUtility.getWeightFor(startDate: startDate, endDate: endDate, completion: {samples, error in
                if error != nil {
                    observer.onError(error!)
                } else {
                    let result = self.convert(dates: dates, samples: samples)
                    observer.onNext(result)
                    observer.onCompleted()
                }
            })
            return Disposables.create()
        }
    }
    
    private func convert(dates : [Date], samples : [HKSample]) -> [(subtitle:Date, value:Double?)]? {
        var proxyDict : [Date : Double?] = [:]
        
        for date in dates {
            let array = samples.filter {self.isSampleInDate(sample: $0, date: date) == true}
            
            if dates.count == 1 {
                if array.count > 0 {
                    let weight = array.last as! HKQuantitySample
                    proxyDict[date] = weight.quantity.doubleValue(for: HKUnit.gramUnit(with: .kilo))
                }
            } else {
                proxyDict[date] = self.averageWeightFrom(samples: array)
            }
        }
        var result : [(subtitle:Date, value:Double?)] = []
        for (key, value) in proxyDict {
            result.append((subtitle: key, value: value))
        }
        
        return result
    }
    
    private func averageWeightFrom(samples : [HKSample]) -> Double? {
        if samples.count > 0 {
            var totalWeight = 0.0
            let totalCount = Double(samples.count)
            for sample in samples {
                let weight = sample as! HKQuantitySample
                totalWeight += weight.quantity.doubleValue(for: HKUnit.gramUnit(with: .kilo))
            }
            return totalWeight / totalCount
        } else {
            return nil
        }
    }
    
    private func isSampleInDate(sample : HKSample, date : Date) -> Bool {
        var cmp = DateComponents()
        cmp.day = 1
        cmp.second = -1
        
        let eDate = Calendar.current.date(byAdding: cmp, to: date)
        
        return (sample.startDate > date && sample.startDate < eDate!)
    }
    
    private func datesArrayFrom(startDate : Date, endDate : Date) -> [Date] {
        let calendar = Calendar.current
        let normSD = calendar.startOfDay(for: startDate)
        let normED = calendar.startOfDay(for: endDate)
        
        var currentDate = normSD
        
        var cmp = DateComponents()
        cmp.day = 1
        var dates = [normSD]
        
        repeat {
            currentDate = calendar.date(byAdding: cmp, to: currentDate)!
            dates.append(currentDate)
        } while !calendar.isDate(currentDate, inSameDayAs: normED)
        
        return dates
    }
    
    func stressModel(startDate : Date, endDate : Date) -> Observable<StressModel> {
        return Observable.create {observer in
            let calendar = Calendar.current
            let activitySource = self.activitySourceString()
            let daysBtw = self.daysBetweenDates(date1: startDate, date2: endDate)
            if daysBtw == 1 && calendar.isDateInToday(startDate) && calendar.isDateInToday(endDate) {
                ActivityUtility.heartRateForToday(completion: {samples, error in
                    if error != nil {
                        observer.onError(error!)
                    } else {
                        let model = StressModel()
                        let filterSourceSample = samples.filter{$0.sourceRevision.source.bundleIdentifier.contains(activitySource) || $0.sourceRevision.source.name.lowercased().contains(activitySource)}
                        let sampleSource = filterSourceSample.count > 0 ? filterSourceSample : samples
                        if sampleSource.count > 0 {
                            let sample = sampleSource.first
                            if sample != nil {
                                let proxyValue = sample?.quantity.doubleValue(for: HKUnit(from: "count/min"))
                                model.currentBPM = proxyValue
                            } else {
                                model.currentBPM = nil
                            }
                        }
                        observer.onNext(model)
                        observer.onCompleted()
                    }
                })
            } else {
                let heartRate = HKQuantityType.quantityType(forIdentifier: HKQuantityTypeIdentifier.heartRate)
                
                ActivityUtility.getStatisticFrom(startDate: startDate, endDate: endDate, quantityType: heartRate!, options: [.discreteMin, .discreteMax, .discreteAverage], completion: { statistics, error in
                    if error != nil {
                        observer.onError(error!)
                    } else {
                        let model = StressModel()
                        var source : [HKSource]!
                        source = statistics?.sources().filter{$0.bundleIdentifier.contains(activitySource) || $0.name.lowercased().contains(activitySource)}

                        if error == nil && statistics != nil {
                            if statistics?.statistics() != nil && (statistics?.statistics().count)! > 0 {
                                if let stat = statistics?.statistics().first {
                                    if source.count > 0 {
                                        model.avgBPM = stat.averageQuantity(for:source![0])?.doubleValue(for: HKUnit(from:"count/min"))
                                        model.minBPM = stat.minimumQuantity(for:source![0])?.doubleValue(for: HKUnit(from:"count/min"))
                                        model.maxBPM = stat.maximumQuantity(for:source![0])?.doubleValue(for: HKUnit(from:"count/min"))
                                    } else {
                                        model.avgBPM = stat.averageQuantity()?.doubleValue(for: HKUnit(from:"count/min"))
                                        model.minBPM = stat.minimumQuantity()?.doubleValue(for: HKUnit(from:"count/min"))
                                        model.maxBPM = stat.maximumQuantity()?.doubleValue(for: HKUnit(from:"count/min"))
                                    }
                                }
                            }
                        }
                        observer.onNext(model)
                        observer.onCompleted()
                    }
                    
                })
            }
            
            return Disposables.create()
        }
    }
    
    
    func calorieStatsFrom(startDate : Date, endDate : Date) -> Observable<ConsumedModel> {
        let token = NRUserSession.sharedInstance.accessToken
        let user = userRepo.getCurrentUser(policy: .DefaultPolicy).map { user -> String in
            return user.userID!
        }.flatMap { userID -> Observable<Data> in
            return self.nourishDS.getCaloriesStats(startDate: startDate, endDate: endDate, token: token!, userId: userID, range: 1)
            
        }.flatMap { data -> Observable<[CaloriesStats]> in
            return Observable.just(self.calorieStatsFrom(data: data))
        }.flatMap { models -> Observable<ConsumedModel> in
            
            let model = ConsumedModel()
            
            var totalConsumed = 0.0
            
            for object in models {
                totalConsumed += object.consumedCal ?? 0.0
            }
            let daysTotal = models.filter({$0.consumedCal != 0.0})
            var avgValue = totalConsumed
            
            if daysTotal.count != 0 {
                avgValue = totalConsumed / Double(daysTotal.count)
            }
            
            model.consumedCalories = totalConsumed
            model.avgConsumed = avgValue
            
            return Observable.just(model)
        }
        return user
    }
    
    private func calorieStatsFrom(data : Data) -> [CaloriesStats] {
        var array : [CaloriesStats] = []
        let dict = self.convertDataToDictionary(data: data)
        
        if dict != nil {
            for object in dict! {
                array.append(self.calorieStatFrom(dict: object))
            }
        }
        
        
        return array
    }
    private func convertDataToDictionary(data: Data) -> [[String : Any]]? {
        do {
            return try JSONSerialization.jsonObject(with: data, options: []) as? [[String : Any]]
        } catch let error as NSError {
            LogUtility.logToFile(error)
        }
        return nil
    }
    private func dateFromUnixStamp(time : Double) -> Date {
        let interval = time / 1000
        let date = Date(timeIntervalSince1970 : interval)
        return date
    }
    
    private func calorieStatFrom(dict : [String : Any]) -> CaloriesStats {
        let model = CaloriesStats()
        print("dict is \(dict)")
        
        if dict["breakdown_kcals"] != nil {
            let brackeCalories = self.breakDownCaloriesFrom(dict: dict["breakdown_kcals"] as! [String : Any])
            model.breakDownCals = brackeCalories
        }
        
        model.score = dict["score"] as? Double
        model.eerCal = dict["eer_kcals"] as? Double
        model.consumedCal = dict["consumed_kcals"] as? Double
        
        if dict["day"] != nil {
            model.day = self.dateFromUnixStamp(time: dict["day"] as! Double)
        }
        
        return model
    }
    
    private func breakDownCaloriesFrom(dict : [String : Any]) -> BreakDownCalories {
        let calories = BreakDownCalories()
        calories.breakfastCals = dict["breakfast"] as? Double
        calories.dinnerCals = dict["dinner"] as? Double
        calories.lunchCals = dict["lunch"] as? Double
        calories.snacksCals = dict["snack"] as? Double
        calories.unknownCals = dict["unknown"] as? Double
        return calories
    }
    
    
    private func hourIndexFromSample(sample : HKSample) -> Int {
        return Calendar.current.component(.hour, from: sample.startDate)
    }
    
    private func fetchSampleForHourIndex(index : Int, samples : [HKSample]) -> [HKSample] {
        var result : [HKSample] = []
        
        for obj in samples {
            if self.hourIndexFromSample(sample: obj) == index {
                result.append(obj)
            }
        }
        
        return result
    }
    
    private func bmrForFullDay() -> Observable<Double> {
        let user = UserRepository.shared.getCurrentUser(policy: .DefaultPolicy).flatMap { userProfile -> Observable<Double> in
            
            let bmrForDay = userProfile.getBMRCalories(weight: 0.0, height: 0.0, date: Date(), isCurrentDay: true)
            
            return Observable.just(bmrForDay)
        }
        
        return user
    }
    
    private func getBMRForDate(date : Date) -> Observable<Double> {
        let user = UserRepository.shared.getCurrentUser(policy: .DefaultPolicy).flatMap { userProfile -> Observable<Double> in
            
            let bmrForDay = userProfile.getBMRCalories(weight: 0.0, height: 0.0, date: date, isCurrentDay: false)
            
            return Observable.just(bmrForDay)
        }
        
        return user
    }
    
    private func totalCalories(samples : [HKSample], avg: Bool = false) -> Double {
        var total = 0.0
        
        let sampleSource = self.activitySourceString()
        let filterSourceSample = samples.filter{$0.sourceRevision.source.bundleIdentifier.contains(sampleSource) || $0.sourceRevision.source.name.lowercased().contains(sampleSource)}
        let sampleArray = filterSourceSample.count > 0 ? filterSourceSample : samples
        for obj in sampleArray {
           // if obj.sourceRevision.source.bundleIdentifier.contains(sampleSource) {
                total += caloriesFromSample(sample: obj)
            //}
        }
        
        if avg == true {
            if sampleArray.count != 0 {
                let day = self.separateSamplesWithDays(samples: sampleArray)
                let sampleDay = day > 0 ? day : 1
                total = total / Double(sampleDay)
            }
        }
        
        return total
    }
    
    private func caloriesFromSample(sample : HKSample) -> Double {
        let key = sample as! HKQuantitySample
        
        return key.quantity.doubleValue(for: HKUnit.kilocalorie())
        /*
        let startComps = Calendar.current.dateComponents([.weekday, .minute], from: sample.startDate)
        let endComps = Calendar.current.dateComponents([.weekday, .minute], from: sample.endDate)
        
        if startComps.weekday != endComps.weekday {
            let interval = sample.endDate.timeIntervalSince(sample.startDate)
            let startOfDay = Calendar.current.startOfDay(for: sample.endDate)
            let intervalStartOfDay = sample.endDate.timeIntervalSince(startOfDay)
            let koef = intervalStartOfDay / interval
            
            return key.quantity.doubleValue(for: HKUnit.kilocalorie()) * koef
        } else {
            
        }
        */
    }
    
    private func fetchBasalCalories(startDate : Date, endDate : Date) -> Observable<[HKSample]> {
        return Observable.create { observer in
            
            ActivityUtility.getBasalCaloriesFor(startDate: startDate, endDate: endDate, completion: {samples, error in
                if error != nil {
                    observer.onError(error!)
                } else {
                    observer.onNext(samples)
                    observer.onCompleted()
                }
            })
            
            return Disposables.create()
        }
    }
    
    private func fetchActiveCalories(startDate : Date, endDate : Date) -> Observable<[HKSample]> {
        return Observable.create { observer in
            
            ActivityUtility.getCaloriesFor(startDate: startDate, endDate: endDate, completion:  {samples, error in
                if error != nil {
                    observer.onError(error!)
                } else {
                    
                    observer.onNext(samples)
                    observer.onCompleted()
                }
            })
            
            return Disposables.create()
        }
    }
    
    func sleepModel(startDate : Date, endDate : Date) -> Observable<SleepModel> {
        return self.fetchSleepingSample(startDate: startDate, endDate: endDate).flatMap {samples -> Observable<SleepModel> in
            let model = SleepModel()
            var totalSeconds = 0.0
            let activitySource = self.activitySourceString()
            let filterSourceSample = samples.filter{$0.sourceRevision.source.bundleIdentifier.contains(activitySource) || $0.sourceRevision.source.name.lowercased().contains(activitySource)}
            let sleepSource = filterSourceSample.count > 0 ? filterSourceSample : samples
            let daysTotal = self.separateSamplesWithDays(samples: sleepSource.filter({($0 as? HKCategorySample)?.value == HKCategoryValueSleepAnalysis.asleep.rawValue}))
            for activity in sleepSource {
                let category = activity as? HKCategorySample
                if category?.value == HKCategoryValueSleepAnalysis.asleep.rawValue  {
                    totalSeconds = totalSeconds + activity.endDate.timeIntervalSince(activity.startDate)
                }
            }
            let sleepHours = totalSeconds / 3600
            
            model.sleepHours = sleepHours
            if daysTotal != 0 {
                model.avgSleepDay = sleepHours / Double(daysTotal)
            } else {
                model.avgSleepDay = sleepHours
            }
            
            return Observable.just(model)
        }
    }
    
    private func fetchSleepingSample(startDate : Date, endDate : Date) -> Observable<[HKSample]> {
        return Observable.create { observer in
            
            ActivityUtility.sleepingActivityFor(startDate: startDate, endDate: endDate, completion: {samples, error in
                if error != nil {
                    observer.onError(error!)
                } else {
                    if samples != nil {
                        observer.onNext(samples!)
                    }
                    observer.onCompleted()
                }
            })
            
            return Disposables.create()
        }
    }
    
    private func fetchDistanceSamples(startDate : Date, endDate : Date) -> Observable<[HKSample]> {
        return Observable.create { observer in
            ActivityUtility.getDistanceSamples(startDate: startDate, endDate: endDate, completion: {samples, error in
                if error != nil {
                    observer.onError(error!)
                } else {
                    observer.onNext(samples)
                    observer.onCompleted()
                }
            })
            
            return Disposables.create()
        }
    }
    
    private func fetchWorkoutsFor(startDate : Date, endDate : Date) -> Observable<[HKWorkout]> {
        return Observable.create {observer in
            ActivityUtility.getWorkoutSamples(startDate: startDate, endDate: endDate, completion: {workouts, error in
                if error != nil {
                    observer.onError(error!)
                } else {
                    observer.onNext(workouts)
                }
                observer.onCompleted()
            })
            return Disposables.create()
        }
    }
    
    private func fetchStepsFor(startDate : Date, endDate : Date) -> Observable<HKStatisticsCollection> {
        return Observable.create { observer in
            let stepsQuantity = HKQuantityType.quantityType(forIdentifier: HKQuantityTypeIdentifier.stepCount)
            
            ActivityUtility.getStatisticFrom(startDate: startDate, endDate: endDate, quantityType: stepsQuantity!, options: nil, completion: {collection, error in
                if error != nil {
                    observer.onError(error!)
                } else {
                    if collection != nil {
                        observer.onNext(collection!)
                    }
                }
                observer.onCompleted()
            })
            
            return Disposables.create()
        }
    }
    
    func excerciseModelFromStart(date : Date, endDate : Date) -> Observable<ExerciseModel> {
        let result = self.fetchWorkoutsFor(startDate: date, endDate: endDate).flatMap { wrk -> Observable<ExerciseModel> in
            let model = ExerciseModel()
            model.activeHours = self.activeTimeFrom(array: wrk)
            model.activityList = self.separateWorkoutsForHours(arr: wrk)
            
            return Observable.just(model)
        }
        return result
    }
    
    func stepsModelFrom(startDate : Date, endDate : Date) -> Observable<StepsModel> {
        let result = self.fetchStepsFor(startDate: startDate, endDate: endDate).flatMap {collection -> Observable<StepsModel> in
            let model = StepsModel()
            var totalSteps = 0.0
            let sourceString = self.activitySourceString()
            
            var source = collection.sources().filter{$0.bundleIdentifier.contains(sourceString) || $0.name.lowercased().contains(sourceString)}
            
            if sourceString == "" {
                source = Array(collection.sources())
            }
            
            for object in collection.statistics() {
                for src in source {
                    if let value = object.sumQuantity(for: src)?.doubleValue(for: HKUnit.count()) {
                        totalSteps += value
                    }
                }
            }
            
            let dem = collection.statistics().count > 0 ? collection.statistics().count : 1
            let avgValue = totalSteps / Double(dem)
            
            model.numberSteps = Double(totalSteps)
            model.avgStepsDay = avgValue
            return Observable.just(model)
            
        }
        return result
        
    }
    
    
    private func separateWorkoutsForHours(arr : [HKWorkout]) -> [(activityName: String, value: Double)] {
        let dict = self.separateWorkoutsIntoDict(array: arr)
        var dictToReturn : [(activityName: String, value: Double)] = []
        
        for (name, works) in dict {
        
            var value = 0.0
            for work in works {
                value = value + work.duration
                
            }
            dictToReturn.append((activityName: name, value: value/3600))
        }
        
        return dictToReturn
    }
    
    private func separateWorkoutsIntoDict(array : [HKWorkout]) -> [String : [HKWorkout]] {
        var proxyDict : [String : [HKWorkout]] = [:]
        for workout in array {
            let stringRep = self.stringFromEnum(type: workout.workoutActivityType)
            if proxyDict[stringRep] != nil {
                var arr = proxyDict[stringRep]
                arr?.append(workout)
                proxyDict[stringRep] = arr
            } else {
                proxyDict[stringRep] = [workout]
            }
        }
        return proxyDict
    }
    
    func stringFromEnum(type : HKWorkoutActivityType) -> String {
        
        switch type {
        case .mixedCardio:
            return "Mixed Cardio"
        case .americanFootball:
            return "American Football"
        case .archery:
            return "Archery"
        case .australianFootball:
            return "Australian Football"
        case .badminton:
            return "Badminton"
        case .baseball:
            return "Baseball"
        case .basketball:
            return "Basketball"
        case .bowling:
            return "Bowling"
        case .boxing:
            return "Boxing"
        case .climbing:
            return "Climbing"
        case .cricket:
            return "Cricket"
        case .crossTraining:
            return "Cross Training"
        case .curling:
            return "Curling"
        case .cycling:
            return "Cycling"
        case .dance:
            return "Dance"
        case .danceInspiredTraining:
            return "Dance Inspired Training"
        case .elliptical:
            return "Elliptical"
        case .equestrianSports:
            return "Equestrian Sports"
        case .fencing:
            return "Fencing"
        case .fishing:
            return "Fishing"
        case .functionalStrengthTraining:
            return "Functional Strength Training"
        case .golf:
            return "Golf"
        case .gymnastics:
            return "Gymnastics"
        case .handball:
            return "Handball"
        case .hiking:
            return "Hiking"
        case .hockey:
            return "Hockey"
        case .hunting:
            return "Hunting"
        case .lacrosse:
            return "Lacrosse"
        case .martialArts:
            return "MartialArts"
        case .mindAndBody:
            return "Mind And Body"
        case .mixedMetabolicCardioTraining:
            return "Mixed Metabolic Cardio Training"
        case .paddleSports:
            return "Paddle Sports"
        case .play:
            return "Play"
        case .preparationAndRecovery:
            return "Preparation And Recovery"
        case .racquetball:
            return "Racquetball"
        case .rowing:
            return "Rowing"
        case .rugby:
            return "Rugby"
        case .running:
            return "Running"
        case .sailing:
            return "Sailing"
        case .skatingSports:
            return "Skating Sports"
        case .snowSports:
            return "Snow Sports"
        case .soccer:
            return "Soccer"
        case .softball:
            return "Softball"
        case .squash:
            return "Squash"
        case .stairClimbing:
            return "Stair Climbing"
        case .surfingSports:
            return "Surfing Sports"
        case .swimming:
            return "Swimming"
        case .tableTennis:
            return "TableTennis"
        case .tennis:
            return "Tennis"
        case .trackAndField:
            return "Track And Field"
        case .traditionalStrengthTraining:
            return "Traditional Strength Training"
        case .volleyball:
            return "Volleyball"
        case .walking:
            return "Walking"
        case .waterFitness:
            return "Water Fitness"
        case .waterPolo:
            return "Water Polo"
        case .waterSports:
            return "Water Sports"
        case .wrestling:
            return "Wrestling"
        case .yoga:
            return "Yoga"
        case .other:
            return "Other"
        case .barre:
            return "Barre"
        case .coreTraining:
            return "Core Training"
        case .crossCountrySkiing:
            return "Cross Country Skiing"
        case .downhillSkiing:
            return "Downhill Skiing"
        case .flexibility:
            return "Flexibility"
        case .highIntensityIntervalTraining:
            return "HighIntensity Interval Training"
        case .jumpRope:
            return "Jump Rope"
        case .kickboxing:
            return "Kickboxing"
        case .pilates:
            return "Pilates"
        case .snowboarding:
            return "Snowboarding"
        case .stairs:
            return "Stairs"
        case .stepTraining:
            return "Step Training"
        case .wheelchairWalkPace:
            return "Wheelchair Walk Pace"
        case .wheelchairRunPace:
            return "Wheelchair Run Pace"
        case .taiChi:
            return "Tai Chi"
        case .handCycling:
            return "Hand Cycling"
        }
    }

    private func activeTimeFrom(array : [HKWorkout]) -> Double {
        var totalTime = 0.0
        
        for workout in array {
            totalTime += workout.duration
        }
        
        return totalTime / 3600
    }
    
    private func daysBetweenDates(date1 : Date, date2 : Date) -> Double {
        let calendar = NSCalendar.current
        
        let date1 = calendar.startOfDay(for: date1)
        let date2 = calendar.startOfDay(for: date2)
        
        let components = calendar.dateComponents([.day], from: date1, to: date2)
        if let days = components.day {
            return Double(days + 1)
        } else {
            return 1.0
        }
    }
    
    private func hoursBetweenDates(date1 : Date, date2 : Date) -> Double {
        let interval = date2.timeIntervalSince(date1)
        return interval / 3600
    }
    
    private func caloricStateFrom(activeCals : [HKSample], basalTotal : Double) -> [(subtitle: Date, cal: (basalCalories: Double, activeCalories: Double))] {
        let cal = Calendar.current
        
        let currentHourIndex = cal.component(.hour, from: Date())
        let doubleHours = self.hoursBetweenDates(date1: cal.startOfDay(for: Date()), date2: Date())
        
        let bmrHour = basalTotal / doubleHours
        var bmrSnap = basalTotal
        var caloricArray : [(subtitle: Date, cal: (basalCalories: Double, activeCalories: Double))] = []
        
        for i in 0...currentHourIndex {
            if i == currentHourIndex {
                let arr = self.fetchSampleForHourIndex(index: i, samples: activeCals)
                let countedCals = self.totalCalories(samples: arr)
                
                caloricArray.append((subtitle: cal.startOfDay(for: Date()), cal: (basalCalories: bmrSnap, activeCalories: countedCals)))
            } else {
                bmrSnap -= bmrHour
                let arr = self.fetchSampleForHourIndex(index: i, samples: activeCals)
                let countedCals = self.totalCalories(samples: arr)
                
                caloricArray.append((subtitle: cal.startOfDay(for: Date()), cal: (basalCalories: bmrHour, activeCalories: countedCals)))
            }
            
        }
        return caloricArray
    }
    
    func activitySourceString() -> String {
        var activeSource : String = ""
        
        if KeychainWrapper.standard.string(forKey: kActivitySourceKey) != nil {
            let source = KeychainWrapper.standard.string(forKey: kActivitySourceKey)
            activeSource = source == "Fit Bit" ? kFitBitKey : source == "Apple Watch" ?  "watch" : ""
        }
        
        return activeSource
    }
    
    private func separateSamplesWithDays(samples : [HKSample]) -> Int {
        let calendar = Calendar.current
        var proxyDict : [Date : [HKSample]] = [:]
        
        for sample in samples {
            let key = calendar.startOfDay(for: sample.startDate)
            if proxyDict[key] != nil {
                var proxyArray = proxyDict[key]
                proxyArray?.append(sample)
                proxyDict[key] = proxyArray
            } else {
                var array : [HKSample] = []
                array.append(sample)
                proxyDict[key] = array
            }
        }
        
        return proxyDict.count
    }
    
}

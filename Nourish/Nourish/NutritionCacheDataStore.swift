//
//  NutritionCacheDataStore.swift
//  Nourish
//
//  Created by Nova on 7/28/17.
//  Copyright © 2017 SSIC. All rights reserved.
//

import UIKit
import RealmSwift


class NutritionCacheDataStore: NSObject {

    func storeCache(cache : Data, date : Date) {
        let realm = try! Realm()
        let objs = realm.objects(NutritionScoreCache.self)
        if objs.count > 0 {
            try! realm.write {
                realm.delete(objs)
            }
        }
        
        let nutr = NutritionScoreCache()
        nutr.data = cache
        nutr.date = Calendar.current.startOfDay(for: date)
        
        
        try! realm.write {
            realm.add(nutr)
        }
        
    }
    
    func fetchLastCache(date: Date) -> Data? {
        let realm = try! Realm()
        let predicate = NSPredicate(format: "date == %@", Calendar.current.startOfDay(for: date) as NSDate)
        let obj = realm.objects(NutritionScoreCache.self).filter(predicate).sorted(byKeyPath: "date", ascending : false).first
        return obj?.data
    }
}

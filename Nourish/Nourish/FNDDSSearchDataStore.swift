//
//  FNDDSSearchDataStore.swift
//  Nourish
//
//  Created by Gena Mironchyk on 8/15/17.
//  Copyright © 2017 SSIC. All rights reserved.
//

import UIKit
import RealmSwift
class FNDDSSearchDataStore: NSObject {
    let foodSearchModelToJson = foodSearchModel_to_jsonClarifai()
    let jsonToFoodSearchModel = json_to_foodSearchModel()
    
    func storeCache(cache : [Date : [String : [foodSearchModel]]]) {
        
        let realm = try! Realm()
        let objs = realm.objects(FNDDSSearchCache.self)
        if objs.count > 0 {
            try! realm.write {
                realm.delete(objs)
            }
        }
        
        do {
            let transformedCache = self.transform(cache: cache)
            let cacheData = try JSONSerialization.data(withJSONObject: transformedCache, options: .prettyPrinted)
            
            let tags = FNDDSSearchCache()
            tags.data = cacheData
            
            
            try! realm.write {
                realm.add(tags)
            }
            
        } catch let error {
            LogUtility.logToFile("Error saving FNDDS tags to cache", error)
        }
        
        
    }
    
    func fetchLastCache() -> [Date : [String : [foodSearchModel]]] {
        let realm = try! Realm()
        let obj = realm.objects(FNDDSSearchCache.self).first
        var dictToReturn = [Date : [String:[foodSearchModel]]]()
        if obj?.data != nil {
            do {
                let cachedDictionary = try JSONSerialization.jsonObject(with: (obj?.data)!, options: []) as! [String : [String :[[String : Any]]]]
                dictToReturn = self.transformFrom(cache: cachedDictionary)
                for (key , _) in dictToReturn {
                    if self.moreThanSevenDays(date: key) {
                        dictToReturn.removeValue(forKey: key)
                    }
                }
            } catch let error {
                LogUtility.logToFile("Error parsing FNDDS cache", error)
            }
        }
        return dictToReturn
    }
    
    
    private func transform(cache : [Date : [String : [foodSearchModel]]]) -> [String : [String : [[String : Any]]]] {
        let df = DateFormatter()
        df.dateStyle = .full
        df.timeStyle = .full
        var dictToReturn =  [String : [String : [[String : Any]]]]()
        for (key,element) in cache {
            for (tag, arrayOfFoods) in element {
                var dictionaryOfArrayOfMeals = [String : [[String : Any]]]()
                var arrayOfTransformedMeals = [[String : Any]]()
                for model in arrayOfFoods {
                    arrayOfTransformedMeals.append(self.foodSearchModelToJson.transform(model: model))
                }
                dictionaryOfArrayOfMeals[tag] = arrayOfTransformedMeals
                dictToReturn[df.string(from: key)] = dictionaryOfArrayOfMeals
            }
        }
        return dictToReturn
    }
    
    private func transformFrom(cache : [String : [String :[[String : Any]]]]) ->  [Date : [String : [foodSearchModel]]] {
        let df = DateFormatter()
        df.dateStyle = .full
        df.timeStyle = .full
        var dictToReturn =  [Date : [String : [foodSearchModel]]]()
        for (key, element) in cache {
            for (tag, arrayOfDictionaries) in element {
                var dictionaryArrayOfMeals = [String : [foodSearchModel]]()
                var arrayOfTransformedMeals = [foodSearchModel]()
                for dict in arrayOfDictionaries {
                    arrayOfTransformedMeals.append(self.jsonToFoodSearchModel.transform(dict: dict))
                }
                dictionaryArrayOfMeals[tag] = arrayOfTransformedMeals
                let date = df.date(from: key) ?? Date()
                dictToReturn[date] = dictionaryArrayOfMeals
            }
        }
        
        return dictToReturn
    }
    
    private func moreThanSevenDays(date:Date) -> Bool {
        var moreThanSevenDays = false
        let componentsFromDate = Calendar.current.dateComponents([.day], from: date)
        let componentsFromNow = Calendar.current.dateComponents([.day], from: Date())
        if componentsFromNow.day! - componentsFromDate.day! > 7 {
            moreThanSevenDays = true
        }
        
        return moreThanSevenDays
    }

}

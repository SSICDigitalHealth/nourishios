//
//  MealRecordCache.swift
//  Nourish
//
//  Created by Nova on 12/12/16.
//  Copyright © 2016 SSIC. All rights reserved.
//

import Foundation
import RealmSwift
import RxSwift

class Nutrient : Object {
    dynamic var name = ""
    dynamic var clearValue = 0.0
    dynamic var representationValue = 0.0
    dynamic var unit = ""
}

class RealmString: Object {
    dynamic var stringValue = ""
}

class MealRecordCache: Object {
    //v5 version db
    
    //cache options
    dynamic var isDeleted : Bool = false
    dynamic var hasChanges : Bool = false
    dynamic var isCreated : Bool = false
    dynamic var localCacheID = "local" + UUID().uuidString
    
    override static func primaryKey() -> String? {
        return "localCacheID"
    }
    
    // specified by the user
    dynamic var ocasion = Ocasion.breakfast.rawValue
    var occasionEnum : Ocasion {
        get {
            return Ocasion(rawValue : ocasion) ?? .breakfast
        }
        set {
            ocasion = newValue.rawValue
        }
    }
    dynamic var grams : Double = 0.0
    dynamic var amount : Double = 0.0
    
    //from server
    dynamic var date : Date? = nil
    dynamic var idString : String? = nil
    dynamic var name : String? = nil
    dynamic var msreDesc : String? = nil
    dynamic var msreAmount : Double = 0.0
    dynamic var msreGrams : Double = 0.0
    dynamic var kCal : Double = 0.0
    dynamic var portionKCal : Double = 0.0
    var group = List<RealmString>()
    dynamic var brand : String? = nil
    dynamic var score : Double = 0.0
    dynamic var userFoodId : String? = nil
    dynamic var calories : Double = 0.0
    dynamic var unit : String? = nil
    
    //Sound Hound
    dynamic var sourceFoodDB : String = ""
    dynamic var sourceFoodDBID : String = ""
    
    //Local
    dynamic var isFavourite : Bool = false
    dynamic var searchCount : Int = 0
    dynamic var caloriesPerMilliliter : Double = 0.0
    dynamic var caloriesPerGramm : Double = 0.0
    dynamic var barCode : String? = nil
    dynamic var brandAndName : String? = nil
    var nutrientsFromNoom = List<Nutrient>()
    dynamic var foodUid : String? = nil
    dynamic var originalSearchString : String? = nil
    dynamic var isFromNoom : Bool = false
    
    //groups
    dynamic var isGroupFavourite : Bool = false
    dynamic var isMarkedForFav : Bool = false
    //var groupedMealArray = List<MealRecordModel>()
    dynamic var groupName : String? = nil
    dynamic var groupID : String? = nil
    dynamic var noomID : String? = nil
    dynamic var numberOfItems : Int = 0
    
    //photo 
    dynamic var userPhotoId : String?
    dynamic var groupPhotoID : String?
    
    dynamic var groupFoodUUID : String?
    
    
    func nutrientsListFromDict(dict : Dictionary<String, Double>) -> [Nutrient] {
        let list = dict.keys
        var array : [Nutrient] = []
        for key in list {
            let nutrientObject = Nutrient()
            nutrientObject.name = key
            nutrientObject.clearValue = dict[key]!
            array.append(nutrientObject)
        }
        return array
    }
    
    func shortModelRepresentation() -> foodSearchModel {
        var model = foodSearchModel()
        model.amount = self.amount
        model.grams = self.grams
        model.unit = self.unit ?? ""
        model.calories = String(format : "%.1f",self.calories)
        model.userPhotoId = self.userPhotoId
        model.foodBackendID = self.userFoodId
        model.measure = self.unit ?? ""
        model.occasion = self.occasionEnum
        
        
        return model
    }
    
    func isFromOperation() -> Bool {
        return self.hasChanges && self.isCreated
    }
    
    func dhMealRepresentation() -> Observable<[String : Any]> {
        let repo = NewFoodCardRepository()
        var dict = [String : Any]()
        
        let date = self.date ?? Date()
        let interval = Int(date.timeIntervalSince1970) * 1000
        
        dict["portion"] = self.portionDict()
        dict["caloriesPerGrams"] = self.caloriesPerGramm
        dict["id"] = self.idString ?? self.foodUid
        dict["source"] = self.sourceFoodDB
        dict["name"] = self.name ?? ""
        dict["source"] = self.sourceFoodDB
        dict["occasion"] = self.ocasion
        dict["datetime"] = interval
        dict["day"] = self.getDayString(date: self.date ?? Date())
        dict["userfoodid"] = self.userFoodId ?? ""
        
        if let photoID = self.userPhotoId {
            dict["user_photo_id"] = photoID
        }
        
        if self.groupID != nil {
             dict["foodgroup_summary"] = self.foodGroupSummayDict()
        }
        let grams = self.grams
        
        let foodCard = repo.fetchFoodCardFor(id: self.idString!).observeOn(MainScheduler.instance).map {foodCard -> [String : Any] in
            if let ruler = foodCard.servings.filter({$0.unit == self.unit}).first?.ruler {
                var dictR = [String : Any]()
                dictR["max"] = ruler.max
                dictR["min"] = ruler.min
                dictR["step"] = ruler.step
                dict["ruler"] = dictR
            }
            
            if foodCard.nutrs.count > 0 && grams != 0 {
                var nutrsDict = [[String : Any]]()
                for object in foodCard.nutrs {
                    nutrsDict.append(object.dictionaryRepresentationfor(grams: grams))
                }
                
                dict["nutrients"] = nutrsDict
            }
            
            return dict
            
        }
        
        return foodCard
        
    }
    
    private func getDayString(date : Date) -> String {
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyyMMdd"
        let string = formatter.string(from: date)
        return string
    }
    
    private func foodGroupSummayDict() -> [String : Any] {
        var dict = [String : Any]()
        
        dict["favorite"] = self.isGroupFavourite
        dict["foodgroupid"] = self.groupID ?? ""
        dict["foodgroupuuid"] = self.groupFoodUUID
        dict["name"] = self.name ?? ""
        
        return dict
    }
    
    private func dhFoodRepresentation() -> [String : Any] {
        var dict = [String : Any]()
        
        dict["Food"] = self.name ?? ""
        dict["Calories"] = self.calories
        dict["Size"] = String(self.grams)
        dict["OptionalData"] = self.dhMealRepresentation()
        
        return dict
    }
    
    private func portionDict() -> [String : Any] {
        var dict = [String : Any]()
        
        dict["amount"] = String(self.amount)
        dict["calories"] = self.calories
        dict["grams"] = self.grams
        dict["unit"] = self.unit ?? ""
        
        return dict
    }
}

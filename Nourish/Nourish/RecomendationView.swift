//
//  RecomendationView.swift
//  Nourish
//
//  Created by Andrey Zmushko on 7/21/17.
//  Copyright © 2017 SSIC. All rights reserved.
//

import UIKit

protocol RecomendationProtocol : BaseViewProtocol {
    func setupWithModel(model: RecomendationViewModel)
    func config() -> ProgressConfig?
}

class RecomendationView: ProgressView, RecomendationProtocol {
    @IBOutlet var contentView: UIView!
    @IBOutlet weak var lampHolder: UIView!
    @IBOutlet weak var ideaImage: UIImageView!
    @IBOutlet weak var headerLabel: UILabel!
    var presenter: RecomendationPresenter!
    @IBOutlet weak var dashboardHeaderLabel: UILabel!
    @IBOutlet weak var dashboardMessageLabel: UILabel!
    
    var config: ProgressConfig?
    var controller: RecomendationViewController?
 
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func renderView(_ config: ProgressConfig) {
        super.renderView(config)
        self.config = config
        self.contentView = UINib(nibName: "RecomendationView", bundle: nil).instantiate(withOwner: self, options: nil)[0] as! UIView
        self.loadContent(contentView: contentView)
        self.lampHolder.layer.cornerRadius = self.lampHolder.bounds.size.width / 2
        self.clipsToBounds = true
        self.loadWhiteThemeWith(header: (self.progressConfig?.recomendationTitle())!)
        self.ideaImage.imageNamedWithTint(named: "idea", tintColor: UIColor.white)
                
        self.setNeedsDisplay()
    }
    
    override func reloadData() {
    }

    func setupWithModel(model: RecomendationViewModel) {
        if let config = self.config {
            if config.state == .card {
                self.headerLabel.isHidden = false
                self.dashboardHeaderLabel.isHidden = true
                self.dashboardMessageLabel.isHidden = true
                let paragraphStyle = NSMutableParagraphStyle()
                paragraphStyle.lineSpacing = 1
                
                let headerAttr: [String : Any] = [NSFontAttributeName : UIFont(name: "Campton-Bold", size: 16)!, NSForegroundColorAttributeName : NRColorUtility.hexStringToUIColor(hex: "1172b9"), NSParagraphStyleAttributeName: paragraphStyle]
                
                paragraphStyle.lineSpacing = 10
                let bodyAttr: [String: Any] = [NSFontAttributeName : UIFont.systemFont(ofSize: 12, weight: UIFontWeightRegular), NSForegroundColorAttributeName : NRColorUtility.hexStringToUIColor(hex: "808080"), NSParagraphStyleAttributeName: paragraphStyle]
                
                let attributedText = NSMutableAttributedString(string: String(format: "%@\n", model.titleText), attributes: headerAttr)
                attributedText.append(NSMutableAttributedString(string: String(format: "%@", model.descriptionText), attributes: bodyAttr))
                
                self.headerLabel.attributedText = attributedText
            
            }
            else {
                self.headerLabel.isHidden = true
                self.dashboardHeaderLabel.isHidden = false
                self.dashboardMessageLabel.isHidden = false
                self.dashboardHeaderLabel.text = model.titleText
                self.dashboardMessageLabel.text = model.descriptionText
            }
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.presenter = RecomendationPresenter()
        if let config = self.progressConfig {
            self.renderView(config)
        }
        self.basePresenter = self.presenter
        self.presenter.recomendationView = self
        super.viewWillAppear(animated)
        self.removeActivityIndicator()
    }
    
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.presenter = nil
    }
    
    
    override func gestureTapAction() {
        if let hook = tapHook {
            hook(.recomendation)
            EventLogger.logClickedOnRecommendations()
            EventLogger.logClickedOnRecommendationWith(period: self.config?.period ?? .daily)
        }
    }
    
    override func draw(_ rect: CGRect) {
        super.draw(rect)

        var newHeight = CGFloat()
        
        for label in [self.headerLabel] {
            if let label = label {
                
                if let config = self.config {
                    if config.state == .card {
                        label.numberOfLines = 0
                        label.lineBreakMode = .byWordWrapping
                    }
                }
                
                newHeight += label.bounds.size.height
            }
        }
        
        if let config = self.config {
            if config.state != .card {
                newHeight = 122.0
            } else {
                newHeight += 10
            }
            
            if let hook = self.updateCardHeightHook {
                hook(newHeight, .recomendation)
            }
        }
    }
}

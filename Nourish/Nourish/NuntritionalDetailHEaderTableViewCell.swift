//
//  NuntritionalDetailHEaderTableViewCell.swift
//  Nourish
//
//  Created by Vlad Birukov on 19.06.17.
//  Copyright © 2017 SSIC. All rights reserved.
//

import UIKit

class NuntritionalDetailHEaderTableViewCell: UITableViewCell {

    @IBOutlet weak var backView: UIView!
    @IBOutlet weak var imageNutritional: UIImageView!
    @IBOutlet weak var nameNutritional: UILabel!
    @IBOutlet weak var totalLabel: UILabel!
    
    let colorBackground = [NutritionType.grains: NRColorUtility.hexStringToUIColor(hex: "F0C517"), NutritionType.protein: NRColorUtility.hexStringToUIColor(hex: "A64DB6"), NutritionType.dairy: NRColorUtility.hexStringToUIColor(hex: "1172B9"), NutritionType.vegs: NRColorUtility.hexStringToUIColor(hex: "70C397"), NutritionType.fruits: NRColorUtility.hexStringToUIColor(hex: "f6321d")]
    
    override func awakeFromNib() {
        self.backView.layer.cornerRadius = self.backView.bounds.size.width / 2
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }
    
    func setupWithModel(model: NutritionElementViewModel, date: (Date, Date)?) {
        let calendar = Calendar.current
        if model.type != nil {
            nameNutritional.text = NutritionType.description(type: model.type!)
            self.backView.backgroundColor = colorBackground[model.type!]
            self.imageNutritional.imageNamedWithTint(named:  NutritionType.description(type: model.type!), tintColor: UIColor.white)
        }
        
        if let currentDate = date {
            if calendar.compare(currentDate.0, to: currentDate.1, toGranularity: .day) == .orderedSame {

                var totalLabel = ""
                
                if model.unit == "cup" {
                    totalLabel = String(format: "%@/%@ %@", NRFormatterUtility.doubleToString(num: NRFormatterUtility.roundCupElements(number: model.consumedAmount)), NRFormatterUtility.doubleToString(num: NRFormatterUtility.roundCupElements(number: model.target ?? 0.0)), model.unit)
                } else {
                   totalLabel = String(format: "%@/%@ %@", NRFormatterUtility.numberToStringNutrition(number: model.consumedAmount), NRFormatterUtility.numberToStringNutrition(number: (model.target ?? 0.0)), model.unit)
                }
                
                self.totalLabel.text = totalLabel
            }
        }
    }
}

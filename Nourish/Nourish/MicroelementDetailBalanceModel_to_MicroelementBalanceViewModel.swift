//
//  MicroelementDetailBalanceModel_to_MicroelementBalanceViewModel.swift
//  Nourish
//
//  Created by Vlad Birukov on 16.06.17.
//  Copyright © 2017 SSIC. All rights reserved.
//

import Foundation

class MicroelementDetailBalanceModel_to_MicroelementBalanceViewModel {
    let mapperElement = CalorificFoodModel_to_CalorificFoodViewModel()
    func transform(microelementDetail: MicroelementBalanceModel) -> MicroelementBalanceViewModel {
        let microelementDetailViewModel = MicroelementBalanceViewModel()

        if microelementDetail.arrMicroelementName != nil {
            microelementDetailViewModel.arrMicroelementName = [MicroelementNameViewModel]()
            microelementDetailViewModel.arrMicroelementName = self.transform(model: microelementDetail.arrMicroelementName!)
        }
    
        return microelementDetailViewModel
    }
    
    private func transform(model: [MicroelementNameModel]) -> [MicroelementNameViewModel] {
        var microelementNameViewModels = [MicroelementNameViewModel]()
        if model.count != 0 {
            for microelement in model {
                let microelementNameViewModel = MicroelementNameViewModel()
                microelementNameViewModel.target = microelement.target
                microelementNameViewModel.type = microelement.type
                if microelement.arrMicroelement != nil {
                    microelementNameViewModel.arrMicroelement = [ElementViewModel]()
                    microelementNameViewModel.arrMicroelement = self.mapperElement.transform(model: microelement.arrMicroelement!)
                }
                microelementNameViewModels.append(microelementNameViewModel)
            }
        }
        return microelementNameViewModels
    }
}

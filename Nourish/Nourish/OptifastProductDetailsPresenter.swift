//
//  OptifastProductDetailsPresenter.swift
//  Nourish
//
//  Created by Gena Mironchyk on 10/20/17.
//  Copyright © 2017 SSIC. All rights reserved.
//

import UIKit

struct OptifastDetailsSection {
    var collapsed : Bool
    var nameSection : String
    init(nameSection: String, collapsed: Bool = false) {
        self.nameSection = nameSection
        self.collapsed = collapsed
    }
}


class OptifastProductDetailsPresenter: BasePresenter, UITableViewDelegate, UITableViewDataSource, OptifastDetailsSectionCellDelegate {
   
    private let interactor = OptifastFoodDetialsInteractor()
    var foodModel : foodSearchModel!
    var detailsView : OptifastDetailsViewProtocol!
    var foodCard = NewFoodCard()
    var sections = [OptifastDetailsSection]()
    
    override func viewWillAppear(_ animated: Bool) {
        let _ = self.interactor.execute(model: self.foodModel).subscribe(onNext: { [unowned self] foodCard in
            DispatchQueue.main.async {
                self.foodCard = foodCard
                self.prepareData()
                self.detailsView.reloadData()
            }
        }, onError: {error in
            DispatchQueue.main.async {
                self.detailsView.parseError(error : error, completion: nil)
            }
        }, onCompleted: {}, onDisposed: {})
    }
    
    private func prepareData() {
        self.sections.removeAll()
        let nutritionalInformationSection = OptifastDetailsSection(nameSection: "Nutritional information")
        let ingredientsSection = OptifastDetailsSection(nameSection: "Ingredients")
        sections.append(nutritionalInformationSection)
        sections.append(ingredientsSection)
        
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch section {
        case 0:
            return 1
        case 1:
            return self.sections[section - 1].collapsed ? self.foodCard.nutrs.count : 0
        default:
            return self.sections[section - 1].collapsed ? self.foodModel.optifastIngredients.count : 0
        }
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        if sections.count > 0 {
            return self.sections.count + 1
        } else {
            return 0
        }
        
    }
    
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch indexPath.section {
        case 0:
            let cell = tableView.dequeueReusableCell(withIdentifier: optifastDetailsPictureCellID) as? OptifastProductPictureCell ?? OptifastProductPictureCell()
            cell.setupWith(model: self.foodModel)
            cell.optifastOrderButton.addTarget(self, action: #selector(orderOnline), for: .touchUpInside)
            cell.selectionStyle = .none
            return cell
        case 1:
            let cell = tableView.dequeueReusableCell(withIdentifier: optifastDetailsNutrientsCellID) as? NRFoodNutrientCell ?? NRFoodNutrientCell()
            let nutrient = self.foodCard.nutrs[indexPath.row]
            cell.name.text = nutrient.name
            cell.value.text = String(format:"%.2f %@",nutrient.densityPerGram,nutrient.unit)
            cell.selectionStyle = .none
            return cell
        default:
            let cell = tableView.dequeueReusableCell(withIdentifier: optifastDetailsNutrientsCellID) as? NRFoodNutrientCell ?? NRFoodNutrientCell()
            cell.name.text = self.foodModel.optifastIngredients[indexPath.row]
            cell.value.text = ""
            cell.selectionStyle = .none
            return cell
        }
    }
        
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        switch section {
        case 0:
            return UIView()
        default:
            
            var sectionsCell = tableView.dequeueReusableHeaderFooterView(withIdentifier: optSecCellID) as? OptifastDetailsSectionCell
            
            if sectionsCell == nil {
                sectionsCell = OptifastDetailsSectionCell(reuseIdentifier : optSecCellID)
            }
            sectionsCell!.sectionTitleLabel.text = self.sections[section - 1].nameSection
            sectionsCell!.section = section - 1
            sectionsCell!.setCollapsed(collapsed : self.sections[section - 1].collapsed)
            sectionsCell!.delegate = self
            return sectionsCell
        }
    }
        
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.section == 0 {
            return UITableViewAutomaticDimension
        } else {
            return self.sections[indexPath.section - 1].collapsed ? UITableViewAutomaticDimension : 0
        }
        
    }

    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if section == 0 {
            return 0
        } else {
            return 44.0
        }
    }
    
    
    func toggleSection(header: OptifastDetailsSectionCell, section: Int) {
        let collapsed = !self.sections[section].collapsed
        self.sections[section].collapsed = collapsed
        header.setCollapsed(collapsed: collapsed)
        self.detailsView.reloadSectionAt(index : section + 1)
    }
    
    func orderOnline() {
        EventLogger.logEstoreOpened()
        UIApplication.shared.openURL(kOptifastShopURL)
    }
}

//
//  MicroelementDescription.swift
//  Nourish
//
//  Created by Andrey Zmushko on 8/17/17.
//  Copyright © 2017 SSIC. All rights reserved.
//

import UIKit

class MicroelementDescription: UIView {
    @IBOutlet weak var image: UIImageView!
    @IBOutlet weak var label: UILabel!
    
    func actualHeight() -> CGFloat {
        self.label.sizeToFit()
        
        return (self.label.bounds.height < self.image.bounds.height ? self.image.bounds.height : self.label.bounds.height) + 8.0
    }

}

//
//  WeeklyStressModel_to_WeeklyStress.swift
//  Nourish
//
//  Created by Gena Mironchyk on 1/23/17.
//  Copyright © 2017 SSIC. All rights reserved.
//

import UIKit
class WeeklyStressModel_to_WeeklyStress : NSObject {
    func transform(weeklyStressModel: WeeklyStressModel) -> WeeklyStress {
        let stress = WeeklyStress()
        stress.avgBPM = weeklyStressModel.avgBPM
        stress.minBPM = weeklyStressModel.minBPM
        stress.maxBPM = weeklyStressModel.maxBPM
        stress.weeklyArray = weeklyStressModel.weeklyArray
        return stress
    }
    
}

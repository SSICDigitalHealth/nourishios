//
//  FitBitMessage.swift
//  Nourish
//
//  Created by Vino (Vinodha) Sundaramoorthy on 7/26/17.
//  Copyright © 2017 SSIC. All rights reserved.
//

import UIKit

enum fitBitMessageType {
    case sleep
    case heartRate
    case activites
}

class FitBitMessage: NSObject {
    dynamic var date : Date?
    dynamic var messageType : String = "Activity"
    dynamic var dateAddedToArtik : Date?
    dynamic var caloriesBMR : Double = 0.0
    dynamic var activeEnergyBurned : Double = 0.0
    dynamic var stepCount : Double = 0.0
    dynamic var sleepTime : Double = 0.0
    dynamic var distance : Double = 0.0
    dynamic var calories : Double = 0.0
    dynamic var restingHeartRate : Double = 0.0
    dynamic var activeMinutes : Double = 0.0
}

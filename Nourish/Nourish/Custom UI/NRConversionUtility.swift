//
//  NRConversionUtility.swift
//  Nourish
//
//  Created by Vino (Vinodha) Sundaramoorthy on 11/10/16.
//  Copyright © 2016 SSIC. All rights reserved.
//

import UIKit

let k_INCH_IN_CM : Double = 2.54
let k_KILO_TO_POUND : Double = 2.2046
let kMeterToCentimeter : Double = 100
let kFeetToMeter : Double = 3.28
let kInchToMeter : Double = 0.0254
let kKmToMiles : Double = 1.6
//progress
let kMlToCup : Double = 236.588

class NRConversionUtility: NSObject {
    
    class func inchesToCentimeters(valueInInches : Double) -> Double {
        let valueInCenti = valueInInches * k_INCH_IN_CM
        return valueInCenti
    }
    
    class func centimetersToInches(valueInCenti : Double) -> Double {
        let valueInInches = valueInCenti / k_INCH_IN_CM
        return valueInInches
    }
    
    class func meresToFeetAndInch(valueInMeter : Double) -> String {
        let numberOfInches = round((valueInMeter * kMeterToCentimeter) / k_INCH_IN_CM)
        let feet = floor(numberOfInches / 12)
        let inches = numberOfInches.truncatingRemainder(dividingBy: 12)
        let feetInchString =  String(format:"%0.f'%0.f''",feet,inches)
        return feetInchString
    }
    
    class func feetAndInchToMeters(feetAndInch : String) -> Double {
        var valueInMeters : Double = 0
        let feet = feetAndInch.components(separatedBy: "'")
        
        if feet.count > 0 {
            let feetValue = Double(feet[0])
            let inchValue = Double(feet[1])
            valueInMeters = (feetValue! / kFeetToMeter) + (inchValue! * kInchToMeter)
        }
        return valueInMeters
    }

    class func kiloToPounds(valueInKilo : Double) -> Double {
        let valueInPounds = valueInKilo * k_KILO_TO_POUND
        return valueInPounds
    }
    
    class func poundsToKil(valueInPounds : Double) -> Double {
        let valueInKilo = valueInPounds / k_KILO_TO_POUND
        return valueInKilo
    }
    
    class func metersToCentimeter(valueInMeters : Double) -> Double {
        let valueInCenti = valueInMeters * kMeterToCentimeter
        return valueInCenti
    }
    
    class func centimeterToMeter(valueInCenti : Double) -> Double {
        let valueInMeter = valueInCenti / kMeterToCentimeter
        return valueInMeter
    }
    
    class func kiloToMiles(valueInKilo : Double) -> Double {
        let valueInMiles = valueInKilo / kKmToMiles
        return valueInMiles
    }
    
    class func hourToString(hour:Double) -> String {
        var str = ""
        let hours = Int(floor(hour))
        let mins = Int(floor(hour * 60).truncatingRemainder(dividingBy: 60))
        
        if hours > 0 {
            str = String(format:"%d Hours",hours)
        }
        
        if mins > 0 {
            str =  str + String(format:" %.1d mins",mins)
        }
        return str
    }
}

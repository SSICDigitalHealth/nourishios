//
//  CircularSliderUtility.swift
//  Nourish
//
//  Created by Vino (Vinodha) Sundaramoorthy on 10/20/16.
//  Copyright © 2016 SSIC. All rights reserved.
//

import UIKit

class CircularSliderUtility: NSObject {
    
    class func squareNumber(n: Double) -> Double {
        return n * n
    }
    
   class func CartesianToCompass(rad: Double) -> Double {
        return rad + Double.pi/2
    
    }
    
    class func CompassToCartesian(rad: Double) -> Double {
        return rad - Double.pi/2
    }
    
    class func toRad(deg : Double) -> Double {
        return (Double.pi * deg)/180.0
    }
    
    class func toDeg(rad : Double) -> Double {
        return (180.0 * rad)/Double.pi
    }
    
    class func angleRelativeToNorth(from fromPoint: CGPoint, to toPoint: CGPoint) -> CGFloat {
        var v = CGPoint(x: toPoint.x - fromPoint.x, y: toPoint.y - fromPoint.y)
        let x = squareNumber(n: Double(v.x))
        let y = squareNumber(n: Double(v.y))
        let vmag  = sqrt(CGFloat(x) + CGFloat(y))
        v.x /= CGFloat(vmag)
        v.y /= CGFloat(vmag)
        let cartesianRadians: Double = atan2(Double(v.y), Double(v.x))

        // Need to convert from cartesian style radians to compass style
        var compassRadians: Double = CartesianToCompass(rad: cartesianRadians)
        if compassRadians < 0 {
            compassRadians += (2 * .pi)
        }
        assert(compassRadians >= 0 && compassRadians <= 2 * .pi, "angleRelativeToNorth should be always positive")
        return CGFloat(toDeg(rad: compassRadians))
    }
    
    class func point(onRadius radius: CGFloat, atAngleFromNorth angleFromNorth: CGFloat) -> CGPoint {
        //Get the point on the circle for this angle
        var result: CGPoint = CGPoint()
        // Need to adjust from 'compass' style angle to cartesian angle
        let cartesianAngle: Float = Float(CompassToCartesian(rad: toRad(deg: Double(angleFromNorth))))
        result.y = round(radius * sin(CGFloat(cartesianAngle)))
        result.x = round(radius * cos(CGFloat(cartesianAngle)))
        return result
    }
    // MARK: - Draw arcs
    
    class func drawFilledCircle(inContext ctx: CGContext, center: CGPoint, radius: CGFloat) {
        ctx.fillEllipse(in: CGRect(x: center.x - (radius), y: center.y - (radius), width: 2 * radius, height: 2 * radius))
    }
    
    class func drawUnfilledCircle(inContext ctx: CGContext, center: CGPoint, radius: CGFloat, lineWidth: CGFloat) {
        self.drawUnfilledArc(inContext: ctx, center: center, radius: radius, lineWidth: lineWidth, fromAngleFromNorth: 0, toAngleFromNorth: 360)
        // 0 - 360 is full circle
    }
    
    class func drawUnfilledArc(inContext ctx: CGContext, center: CGPoint, radius: CGFloat, lineWidth: CGFloat, fromAngleFromNorth: CGFloat, toAngleFromNorth: CGFloat) {
        let cartesianFromAngle: Float = Float(CompassToCartesian(rad: toRad(deg: Double(fromAngleFromNorth))))
        let cartesianToAngle: Float = Float(CompassToCartesian(rad: toRad(deg: Double(toAngleFromNorth))))

        ctx.addArc(center: CGPoint(x: center.x, y:     // arc start point x
            center.y), radius:     // arc start point y
            radius, startAngle:     // arc radius from center
            CGFloat(cartesianFromAngle), endAngle: CGFloat(cartesianToAngle), clockwise: false)
        // iOS flips the y coordinate so anti-clockwise (specified here by 0) becomes clockwise (desired)!
    }
    
    class func outerRadiuOfUnfilledArc(withRadius radius: CGFloat, lineWidth: CGFloat) -> CGFloat {
        return radius + 0.5 * lineWidth
    }
    
    class func innerRadiusOfUnfilledArc(withRadius radius: CGFloat, lineWidth: CGFloat) -> CGFloat {
        return radius - 0.5 * lineWidth
    }

}

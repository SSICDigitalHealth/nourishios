//
//  NRPickerCollectionViewCell.swift
//  Nourish
//
//  Created by Vino (Vinodha) Sundaramoorthy on 11/2/16.
//  Copyright © 2016 SSIC. All rights reserved.
//

import UIKit

class NRPickerCollectionViewCell: UICollectionViewCell {
    
    var cellDescription : String?
    var value : String?
    
    override func draw(_ rect: CGRect) {
        super.draw(rect)
        
        for subV in self.subviews {
            subV.removeFromSuperview()
        }
        //create UI label for value
        let valueLabel = UILabel()
        //TO DO : the x , y position must be calculated based on the screen size
        valueLabel.frame = CGRect(x: 35, y: 15, width: 25, height: 25)
        valueLabel.text = self.value
        valueLabel.textColor = UIColor.white
        self.addSubview(valueLabel)
    }
    
}

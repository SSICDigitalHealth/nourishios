//
//  NRPieChartView.swift
//  Nourish
//
//  Created by Vino (Vinodha) Sundaramoorthy on 10/20/16.
//  Copyright © 2016 SSIC. All rights reserved.
//

import UIKit

//Represents each slice of the pie
struct Segment {
    var color : UIColor
    var value : CGFloat
    var imageName : String
    var description : String
}


class NRPieChartView: UIView {
    var reDraw : Bool = false
    var legendX : CGFloat = 0.0
    var legendY : CGFloat = 0.0
    var segments = [Segment]() {
        didSet {
            DispatchQueue.main.async {
                self.setNeedsDisplay() // re-draw pie view when the segment values change
            }
        }
    }
    var segmentLayers : [CALayer] = [CALayer]()
    var currentSliderAngle : CGFloat = 0.0

    override init(frame: CGRect) {
        super.init(frame: frame)
        isOpaque = false
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    func getPieRadius() -> CGFloat {
        return min(frame.size.width,frame.size.height) * 0.5 - 50
    }
    
    func getCenterPoint() -> CGPoint {
        return CGPoint(x: frame.size.width * 0.5, y: frame.size.height * 0.5)
    }
    
    func drawLegends(segment : Segment) {
        let legendCircleAndRadiusPadding :CGFloat = 7.0
        let legendCircleAndTextPadding : CGFloat = 10
        let legendCircleRadius :CGFloat = 7.0
        let legendTextWidth :CGFloat = 70.0
        
        //Draw legend
        let legendCenterPoint = CGPoint(x:legendX,y:legendY + legendCircleAndRadiusPadding)
        let legendCircleLayer = NRDrawingUtility.drawCircularLegend(center: legendCenterPoint, fillColor: segment.color.cgColor)
        
        //Draw legend description
        let desc = String(format: "%@", segment.description)
        let frame = CGRect(x:legendX, y: legendY, width:legendTextWidth, height: 20)
        legendX = legendX + legendCircleRadius + legendCircleAndTextPadding
        let font = UIFont.systemFont(ofSize: 12)
        let legendTextLayer = NRDrawingUtility.drawTextLayer(frame: frame, text: desc, color: UIColor.black.cgColor, bgColor: UIColor.clear.cgColor, font: font)
        
        self.layer.addSublayer(legendCircleLayer)
        self.layer.addSublayer(legendTextLayer)
        
        legendX = legendX + legendTextWidth + legendCircleRadius
    }
    
    override func draw(_ rect: CGRect) {
        self.layer.shouldRasterize = true
        self.layer.rasterizationScale = UIScreen.main.scale
        //Remove all the old layers
        if (self.segmentLayers.count > 0) {
            for pieLayer in self.segmentLayers {
                pieLayer.removeFromSuperlayer()
            }
            self.segmentLayers = []
        }
        
        let context = UIGraphicsGetCurrentContext()
        let radius = min(frame.size.width,frame.size.height) * 0.5 - 50;
        let viewCenter = CGPoint(x: frame.size.width * 0.5, y: frame.size.height * 0.5)
        let valueCount = segments.reduce(0){ $0 + $1.value}
        
        var startAngle = -(CGFloat(Double.pi / 2) - (CGFloat(Double.pi / 180) * currentSliderAngle))
        legendX = viewCenter.x - radius - 30
        legendY = frame.size.height - 20
        var i = 0
        for segment in segments {
           let endAngle = startAngle + CGFloat(Double.pi * 2) * (segment.value)/valueCount
            context?.move(to: CGPoint(x: viewCenter.x, y: viewCenter.y))
            let slicePath = UIBezierPath()
            slicePath.move(to: viewCenter)
            slicePath.addArc(withCenter: viewCenter, radius: radius, startAngle: startAngle, endAngle: endAngle, clockwise: true)
            let pieSliceLayer = CAShapeLayer()
            pieSliceLayer.path = slicePath.cgPath
            pieSliceLayer.fillColor = segment.color.cgColor
            pieSliceLayer.strokeColor = UIColor.white.cgColor
            pieSliceLayer.lineWidth = 2.0
            self.layer.addSublayer(pieSliceLayer)

            let boundingBox = pieSliceLayer.path?.boundingBox
            let centerPoint = CGPoint(x:(boundingBox?.origin.x)! + (boundingBox?.size.width)!/2,y:(boundingBox?.origin.y)!+(boundingBox?.size.height)!/2)
            let midPointAngle = (startAngle + endAngle) / 2.0
            let midPoint = CGPoint(x: viewCenter.x + radius * cos(midPointAngle), y: viewCenter.y + radius * sin(midPointAngle))
            var newX = midPoint.x + 20
            var newY = midPoint.y - 10
            
            if (midPoint.x < 150) {
                newX = midPoint.x - 45
            }
            
            if newX.isNaN {
                newX = 20
            }
            
            if newY.isNaN {
                newY = 20
            }

            //Draw image icon
            let imageLayer = NRDrawingUtility.drawImageLayer(bounds: CGRect(x: centerPoint.x, y: centerPoint.y , width: 15, height: 15), center: CGPoint(x:centerPoint.x ,y:centerPoint.y), bgColor: UIColor.clear.cgColor, imageName: segment.imageName)
            
            //Draw Value text
            let valueFrame = CGRect(x:newX , y: newY , width: 50, height: 20)
            let valueFont = UIFont.systemFont(ofSize: 15)
            let textLayer = NRDrawingUtility.drawTextLayer(frame: valueFrame, text: String(format: "%.0f%@", segment.value,"%"), color: segment.color.cgColor, bgColor: UIColor.clear.cgColor, font: valueFont)
            pieSliceLayer.addSublayer(imageLayer)
            pieSliceLayer.addSublayer(textLayer)
            slicePath.close()
            startAngle = endAngle
            
            if (!self.reDraw) {
                self.drawLegends(segment: segment)
            }

            self.segmentLayers.append(pieSliceLayer)
            i += 1
        }
   }
}

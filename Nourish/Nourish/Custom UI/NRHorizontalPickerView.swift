//
//  NRHorizontalPickerView.swift
//  Nourish
//
//  Created by Vino (Vinodha) Sundaramoorthy on 11/2/16.
//  Copyright © 2016 SSIC. All rights reserved.
//

import UIKit
private let reuseIdentifier = "kPickerCell"

class NRHorizontalPickerView: UIViewController,UICollectionViewDataSource,UICollectionViewDelegate {
    //Collection View
    @IBOutlet weak var collectionView : UICollectionView?

    var minimumRange : Int = 0
    var maximunRange : Int = 0
    var metric : String = ""
    var cellDescription : String = ""
    var pickerDataArray : [(value : Int,description:String)] = []
    var collectionViewDelegate : UICollectionViewDelegate? = nil
    var currentSelectedIndex : Int = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        //Add dummy placeholders to mark starting position of the uicollection view scroll
        self.pickerDataArray.append((0,""))
        self.pickerDataArray.append((0,""))
        
        //TO DO : Data source array can be populated from home screen / profile screen
        for i in self.minimumRange  ..< self.minimumRange {
            self.pickerDataArray.append((i,""))
        }
        self.collectionView?.register(NRPickerCollectionViewCell.self, forCellWithReuseIdentifier: reuseIdentifier)
        self.collectionView?.isUserInteractionEnabled = true
    }
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.pickerDataArray.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: reuseIdentifier, for: indexPath) as! NRPickerCollectionViewCell
        //TO DO - Based on the metrics flag append metrics string to value
        let cellData = pickerDataArray[indexPath.row]
        if (cellData.value > 0) {
            cell.value  = String(cellData.value)
            cell.cellDescription  = cellData.description
        } else {
            cell.value = ""
            cell.cellDescription = ""
        }
        cell.backgroundColor = UIColor.clear
        cell.isUserInteractionEnabled = true
        cell.setNeedsDisplay()
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let selectedData = self.pickerDataArray[indexPath.row]
        self.currentSelectedIndex = selectedData.value
    }
    
    func collectionView(_ collectionView: UICollectionView, shouldHighlightItemAt indexPath: IndexPath) -> Bool {
        return true
    }

}

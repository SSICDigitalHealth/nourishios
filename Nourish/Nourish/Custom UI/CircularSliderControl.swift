//
//  CircularSliderControl.swift
//  Nourish
//
//  Created by Vino (Vinodha) Sundaramoorthy on 10/21/16.
//  Copyright © 2016 SSIC. All rights reserved.
//

import UIKit

class CircularSliderControl: UIControl {
    var minimumValue : Float
    var maximumValue : Float
    var currentValue : Float
    var lineWidth : Int
    var handleColor : UIColor
    var sliderColor : UIColor
    var radius : Float
    var centerPoint : CGPoint
    var angleFromNorth : Int
    var handleWidth : Float
    let kFitFrameRadius = -1
        
    init(frame: CGRect ,currentAngle:Float){
        self.sliderColor = UIColor.darkGray
        self.centerPoint = CGPoint.zero
        self.angleFromNorth = Int(currentAngle)
        self.radius = 6
        self.maximumValue = 15.0
        self.minimumValue = 0.0
        self.lineWidth = 5
        self.handleColor = UIColor.white
        self.sliderColor = UIColor.black
        self.currentValue = currentAngle
        self.handleWidth = 10
        super.init(frame: frame)
        self.backgroundColor = UIColor.clear
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override public var intrinsicContentSize : CGSize {
        // Total width is: diameter + (2 * MAX(halfLineWidth, halfHandleWidth))
        let diameter = self.radius * 2
        let div = 2.0
        let halfLineWidth = ceilf(Float(Double(self.lineWidth)/div))
        let halfHandleWidth = ceilf(Float( Double(self.handleWidth) / div));
        let widthWithHandle = diameter + (2 * max(halfHandleWidth, halfLineWidth))
        return CGSize(width: CGFloat(widthWithHandle), height: CGFloat(widthWithHandle))
    }
    
    override func draw(_ rect: CGRect) {
            super.draw(rect)
            let ctx = UIGraphicsGetCurrentContext()!
            // Draw the circular lines that slider handle moves along
            self.drawLine(ctx)
            // Draw the draggable 'handle'
            self.drawHandle(ctx)
        }
    
        override func point(inside point: CGPoint, with event: UIEvent?) -> Bool {
            if self.pointInsideHandle(point: point, withevent: event!){
                return true
                // Point is indeed within handle bounds
            }
            else {
                return pointInsideCircle(insideCircle: point, with: event!)
                // Return YES if point is inside slider's circle
            }
        }
        
        func pointInsideCircle(insideCircle point: CGPoint, with event: UIEvent) -> Bool {
            let p1 = self.centerPoint
            let p2 = point
            let xDist: CGFloat = (p2.x - p1.x)
            let yDist: CGFloat = (p2.y - p1.y)
            let distance: Double = sqrt(Double(xDist * xDist) + Double(yDist * yDist))
            return distance < Double(self.radius) + Double(self.lineWidth) * 0.5
        }
    
    func pointInsideHandle(point : CGPoint , withevent: UIEvent) -> Bool {
        let handleCenter = self.pointOnCircleAtAngle(fromNorth: self.angleFromNorth)
        let handleRadius = max(self.handleWidth, 44.0) * 0.5;
        let pointInsideHorzontalHandleBounds = (point.x >= CGFloat(handleCenter.x) - CGFloat(handleRadius)
            && point.x <= CGFloat(handleCenter.x) + CGFloat(handleRadius))
        let pointInsideVerticalHandleBounds  = (point.y >= CGFloat(handleCenter.y) - CGFloat(handleRadius)
            && point.y <= CGFloat(handleCenter.y) + CGFloat(handleRadius));
        
        return pointInsideHorzontalHandleBounds && pointInsideVerticalHandleBounds
    }
    
    func drawLine(_ ctx: CGContext) {
        // Draw an unfilled circle (this shows what can be filled)
        CircularSliderUtility.drawUnfilledCircle(inContext: ctx, center: self.centerPoint, radius: CGFloat(self.radius), lineWidth: CGFloat(self.lineWidth))
        // Draw an unfilled arc up to the currently filled point
        CircularSliderUtility.drawUnfilledArc(inContext: ctx, center: self.centerPoint, radius: CGFloat(self.radius), lineWidth: CGFloat(self.lineWidth), fromAngleFromNorth: 0, toAngleFromNorth: CGFloat(self.angleFromNorth))
    }
    
    func drawHandle(_ ctx: CGContext) {
        ctx.saveGState()
        self.handleColor.set()
        let handleCenter = self.pointOnCircleAtAngle(fromNorth: self.angleFromNorth)
        // Ensure that handle is drawn in the correct color
        self.handleColor = UIColor.white
        CircularSliderUtility.drawFilledCircle(inContext: ctx, center: handleCenter, radius:CGFloat(self.handleWidth))
        ctx.restoreGState()
    }
    
    
    func pointOnCircleAtAngle(fromNorth angleFromNorth: Int) -> CGPoint {
        let offset = CircularSliderUtility.point(onRadius: CGFloat(self.radius), atAngleFromNorth: CGFloat(angleFromNorth))
        return CGPoint(x: self.centerPoint.x + offset.x, y: self.centerPoint.y + offset.y)
    }
    
    override func continueTracking(_ touch: UITouch, with event: UIEvent?) -> Bool {
        super.continueTracking(touch, with: event)
        let lastPoint = touch.location(in: self)
        self.moveHandle(lastPoint)
        self.sendActions(for: .valueChanged)
        return true
    }
    
    
    override func endTracking(_ touch: UITouch?, with event: UIEvent?) {
        super.endTracking(touch, with: event)
    }
    
    func moveHandle(_ point: CGPoint) {
        self.currentValue = Float(Int(floor(Double(CircularSliderUtility.angleRelativeToNorth(from: self.centerPoint, to: point)))))
        self.angleFromNorth = Int(floor(Double(CircularSliderUtility.angleRelativeToNorth(from: self.centerPoint, to: point))))
        self.setNeedsDisplay()
    }
}

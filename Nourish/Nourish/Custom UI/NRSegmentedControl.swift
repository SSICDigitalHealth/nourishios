//
//  NRSegmentedControl.swift
//  Nourish
//
//  Created by Vino (Vinodha) Sundaramoorthy on 12/9/16.
//  Copyright © 2016 SSIC. All rights reserved.
//

import UIKit

public struct segmentAppearance {
    public var backgroundColor: UIColor
    public var selectedBackgroundColor: UIColor
    public var textColor: UIColor
    public var font: UIFont
    public var selectedTextColor: UIColor
    public var selectedFont: UIFont
    public var bottomLineColor: UIColor
    public var selectorColor: UIColor
    public var bottomLineHeight: CGFloat
    public var selectorHeight: CGFloat
    public var labelTopPadding: CGFloat
    public var hasImage : Bool
    public var imageSize : CGSize?
}

// MARK: - Control Protocol

@objc public protocol NRSegmentedControlDelegate {
    @objc optional func segmentedControlWillPressItemAtIndex (segmentedControl: NRSegmentedControl, index: Int)
    @objc optional func segmentedControlDidPressedItemAtIndex (segmentedControl: NRSegmentedControl, index: Int)
    @objc optional func segmentedControlDidDeselectAll (segmentedControl : NRSegmentedControl, dismissKeyboard : Bool)
}


typealias controlItemAction = (_ item: NRSegmentedControlItem) -> Void

class NRSegmentedControlItem: UIControl {
    
    // MARK: Properties
    private var willPress: controlItemAction?
    private var didPressed: controlItemAction?
    var label: UILabel!
    var imageView : UIImageView!
    var text : String!
    var selectedText : String!

    // MARK: Init
    init (frame: CGRect,text: String,selectedText:String, hasImage : Bool ,appearance: segmentAppearance,willPress: controlItemAction?,didPressed: controlItemAction?) {
        super.init(frame: frame)
        self.willPress = willPress
        self.didPressed = didPressed
        if  !hasImage {
            label = UILabel(frame: CGRect(x: 0, y: 0, width: frame.size.width, height: frame.size.height))
            label.textColor = appearance.textColor
            label.font = appearance.font
            label.textAlignment = .center
            label.text = text
            self.text = text
            self.selectedText = selectedText
            addSubview(label)
        } else {
            self.text = text
            self.selectedText = selectedText
            let tabImage = UIImage(named:text)
            imageView = UIImageView.init(image: tabImage)
            if appearance.imageSize != nil {
                let xPoint = self.frame.size.width/2.0 - (appearance.imageSize?.width)!/2.0
                let yPoint = self.frame.size.height/2.0 - (appearance.imageSize?.height)!/2.0
                imageView.frame = CGRect(x: xPoint, y: yPoint, width: (tabImage?.size.width)!, height: (tabImage?.size.height)!)
            }
            imageView.backgroundColor = appearance.backgroundColor

            addSubview(imageView)
        }
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init (coder: aDecoder)
    }
    
    // MARK: Events
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        willPress?(self)
    }
   
    override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
        didPressed?(self)

    }

}

public typealias controlAction = (_ segmentedControl: NRSegmentedControl, _ index: Int) -> Void

public class NRSegmentedControl: UIView {
    
    // MARK: Properties
    
    weak var delegate: NRSegmentedControlDelegate?
    var action: controlAction?
    
    public var appearance: segmentAppearance! {
        didSet {
            self.draw()
        }
    }
    
    var titles: [String]!
    var selectedtitles : [String]!
    var items: [NRSegmentedControlItem]!
    var selector: UIView!
    
    // MARK: Init
    
    public init (frame: CGRect, titles: [String],selectedtitles:[String], action: controlAction? = nil) {
        super.init (frame: frame)
        self.action = action
        self.titles = titles
        self.selectedtitles = selectedtitles
        defaultAppearance()
    }
    
    required public init? (coder aDecoder: NSCoder) {
        super.init (coder: aDecoder)
    }
    
    // MARK: Draw
    
    private func reset () {
        for sub in subviews {
            let v = sub
            v.removeFromSuperview()
        }
        items = []
    }
    
    func draw () {
        reset()
        backgroundColor = appearance.backgroundColor

        let width = frame.size.width / CGFloat(titles.count)
        var currentX: CGFloat = 0
        var i = 0
        for title in titles {
            
          let item =  NRSegmentedControlItem(frame: CGRect(
                x: currentX,
                y: appearance.labelTopPadding,
                width: width,
                height: frame.size.height - appearance.labelTopPadding),
                text: title,
                selectedText:selectedtitles != nil  ? selectedtitles[i] : "",
                hasImage: appearance.hasImage,
                appearance: appearance,
                willPress: { segmentedControlItem in
                    let index = self.items.index(of: segmentedControlItem)!
                    self.delegate?.segmentedControlWillPressItemAtIndex?(segmentedControl: self, index: index)
          } ,didPressed: {
                segmentedControlItem in
                let index = self.items.index(of: segmentedControlItem)!
                self.selectItemAtIndex(index: index, withAnimation: true)
                self.action?(self, index)
                self.delegate?.segmentedControlDidPressedItemAtIndex?(segmentedControl: self, index: index)
        })
        
        addSubview(item)
        items.append(item)
        currentX += width
        i += 1
        }
        // bottom line
        let bottomLine = CALayer ()
        bottomLine.frame = CGRect(
            x: 0,
            y: frame.size.height - appearance.bottomLineHeight,
            width: frame.size.width,
            height: appearance.bottomLineHeight)
        bottomLine.backgroundColor = appearance.bottomLineColor.cgColor

        layer.addSublayer(bottomLine)
        // selector
        selector = UIView (frame: CGRect (
            x: 0,
            y: frame.size.height - appearance.selectorHeight,
            width: width,
            height: appearance.selectorHeight))
        selector.backgroundColor = appearance.selectorColor

        addSubview(selector)
        selectItemAtIndex(index: 0, withAnimation: true)
    }
    
    private func defaultAppearance () {
        appearance = segmentAppearance(backgroundColor:UIColor.red
            , selectedBackgroundColor: UIColor.clear, textColor: UIColor.gray, font: UIFont.systemFont(ofSize: 15), selectedTextColor: UIColor.black, selectedFont: UIFont.systemFont(ofSize: 15), bottomLineColor:  UIColor.black, selectorColor: UIColor.black, bottomLineHeight: 0.5, selectorHeight: 2, labelTopPadding: 0, hasImage: false, imageSize: nil)
    }
    
    // MARK: Select
    
    public func selectItemAtIndex (index: Int, withAnimation: Bool) {
        moveSelectorAtIndex(index: index, withAnimation: withAnimation)
        for item in items {

            if item == items[index] {
                if !appearance.hasImage {
                    item.label.textColor = appearance.selectedTextColor
                    item.label.font = appearance.selectedFont
                } else {
                    item.imageView.image = UIImage(named:item.selectedText)?.withRenderingMode(.alwaysTemplate)
                    item.imageView.tintColor = NRColorUtility.appBackgroundColor()
                    
                }
                
                item.backgroundColor = appearance.backgroundColor

            } else {
                if !appearance.hasImage {
                    item.label.textColor = appearance.textColor
                    item.label.font = appearance.font
                } else {
                    item.imageView.image = UIImage(named:item.text)
                }
                item.backgroundColor = appearance.backgroundColor
            }
        }
    }
    
    private func moveSelectorAtIndex (index: Int, withAnimation: Bool) {
        self.selector.backgroundColor = appearance.selectorColor
        let width = frame.size.width / CGFloat(items.count)
        let target = width * CGFloat(index)
        UIView.animate(withDuration: withAnimation ? 0.3 : 0,
                                   delay: 0,
                                   usingSpringWithDamping: 1,
                                   initialSpringVelocity: 0,
                                   options: [],
                                   animations: {
                                    [unowned self] in
                                    self.selector.frame.origin.x = target
            },
                                   completion: nil)
    }
    
    
    func deselectAll(dismissKeyboard : Bool){
        selector.backgroundColor = UIColor.clear
        
        for item in items {
            if appearance.hasImage {
                item.imageView.image = UIImage(named:item.text)
            }
        }
        
        self.delegate?.segmentedControlDidDeselectAll?(segmentedControl: self, dismissKeyboard: dismissKeyboard)
    }
}



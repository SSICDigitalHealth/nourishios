//
//  NutritionalDetailView.swift
//  Nourish
//
//  Created by Vlad Birukov on 19.06.17.
//  Copyright © 2017 SSIC. All rights reserved.
//

import UIKit

protocol NutritionalDetailBalanceProtocol  : BaseViewProtocol{
    func reloadTable()
    func registerTableViewCells()
    func config() -> ProgressConfig?
}

class NutritionalDetailView: BaseView, NutritionalDetailBalanceProtocol {

    @IBOutlet weak var detailTableView: UITableView!
    @IBOutlet var presenter: NutritionalDetailPresenter!
    var nutritionalDetailViewController: NutritionalDetailViewController?

    
    var progressConfig: ProgressConfig?
    
    func config() -> ProgressConfig? {
        return self.progressConfig
    }
    
    override func viewDidLoad() {
        self.xibAutoLayouting(nibName: "NutritionalDetailView")
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        self.basePresenter = presenter
        self.presenter.nutritionalDetailView = self
        self.registerTableViewCells()

        super.viewWillAppear(animated)
    }
    
    
    func reloadTable() {
        detailTableView.reloadData()
    }
    
    func registerTableViewCells() {
        self.detailTableView?.register(UINib(nibName: "NutritionTableViewCell", bundle: nil), forCellReuseIdentifier: "cellNutririon")
        self.detailTableView?.register(UINib(nibName: "NuntritionalDetailHEaderTableViewCell", bundle: nil), forCellReuseIdentifier: "cellNutritionalHeader")

        if self.progressConfig?.period == .daily {
            self.detailTableView?.register(UINib(nibName: "NutritionalDetailTableViewCell", bundle: nil), forCellReuseIdentifier: "cellNutrition")
        } else {
            self.detailTableView?.register(UINib(nibName: "NutritionalWeeklyMonthlyTableViewCell", bundle: nil), forCellReuseIdentifier: "cellDetailNutrient")
        }
    }


}

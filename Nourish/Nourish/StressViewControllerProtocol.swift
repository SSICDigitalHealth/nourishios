//
//  StressViewControllerProtocol.swift
//  Nourish
//
//  Created by Gena Mironchyk on 2/2/17.
//  Copyright © 2017 SSIC. All rights reserved.
//

import Foundation
protocol StressViewControllerProtocol{
    func showDailyChart()
    func showWeeklyChart()
}

//
//  UserProfile_to_UserProfileCacheRealm.swift
//  Nourish
//
//  Created by Nova on 1/17/17.
//  Copyright © 2017 SSIC. All rights reserved.
//

import UIKit

class UserProfile_to_UserProfileCacheRealm: NSObject {
    func transform(userProfile : UserProfile) -> UserProfileCacheRealm {
        let cache = UserProfileCacheRealm()
        cache.userID = userProfile.userID
        cache.userFirstName = userProfile.userFirstName
        cache.userLastName = userProfile.userLastName
        cache.activeSinceTimeStamp = userProfile.activeSinceTimeStamp
        cache.activityLevel = userProfile.activityLevel?.description
        cache.height.value = userProfile.height
        cache.weight.value = userProfile.weight
        cache.biologicalSex = userProfile.biologicalSex?.description
        cache.dietaryPreference = userProfile.dietaryPreference?.description
        cache.metricPreference = userProfile.metricPreference?.description
        cache.goalPrefsFood.value = userProfile.goalPrefsFood ?? 0.0
        cache.goalPrefsLifestyle.value = userProfile.goalPrefsLifestyle ?? 0.0
        cache.goalPrefsActivity.value = userProfile.goalPrefsActivity ?? 0.0
        cache.userGoal = userProfile.userGoal?.description
        cache.age.value = userProfile.age
        cache.startWeightTest.value = userProfile.startWeight
        cache.goalTest.value = userProfile.goalValue
        cache.weightLossStartDate = userProfile.weightLossStartDate
        cache.email = userProfile.email
        cache.weightToMaintain.value = userProfile.weightToMaintain
        cache.needToInformUser = userProfile.needToInformUser
        
        let result = userProfile.optifastOcasionsList.map({return $0.rawValue})
        let _ = result.map({
            let rlmInt = RealmInt()
            rlmInt.value = $0
            cache.listOfOptifastOcasions.append(RealmInt(value : rlmInt))
        })
        
        let _ = userProfile.optifastFoodIds.map({
            let rlmString = RealmString()
            rlmString.stringValue = $0
            cache.listOfOptifastFoodIds.append(RealmString(value : rlmString))
        })
        
        
        return cache
    }
}

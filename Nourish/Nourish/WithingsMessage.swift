//
//  WithingsMessage.swift
//  Nourish
//
//  Created by Gena Mironchyk on 4/28/17.
//  Copyright © 2017 SSIC. All rights reserved.
//

import UIKit

class WithingsMessage: NSObject {
    dynamic var groupID : Double = 0.0
    dynamic var date : Date?
    dynamic var weight : Double = 0.0
    dynamic var dateAddedToArtik : Date?
}

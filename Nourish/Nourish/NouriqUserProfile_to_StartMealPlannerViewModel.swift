
//
//  UserProfile_to_StartMealPlannerViewModel.swift
//  Nourish
//
//  Created by Vlad Birukov on 18.08.17.
//  Copyright © 2017 SSIC. All rights reserved.
//

import Foundation


class NouriqUserProfile_to_StartMealPlannerViewModel {
    func transform(model: UserProfile) -> NouriqStartMealPlannerViewModel {
        let startMealPlannerModel = NouriqStartMealPlannerViewModel()
        let plan = model.calculateAndStoreCaloriePlan()
        print(plan)
        startMealPlannerModel.goal = model.userGoal?.descriptionUI
        startMealPlannerModel.diet = model.dietaryPreference?.descriptionUI
        startMealPlannerModel.caloriesDay = defaults.object(forKey: kCalorieBudgetKey) as? Double
        return startMealPlannerModel
    }
}

//
//  WeightStatsTotal.swift
//  Nourish
//
//  Created by Nova on 1/24/17.
//  Copyright © 2017 SSIC. All rights reserved.
//

import UIKit
import HealthKit


class WeightStatsTotal : NSObject {
    var averageWeight : Double = 0.0
    var averageCalorie : Double = 0.0
    var weightsArray : [WeightStatsSample] = []
    var caloriesArray : [CalorieStatsSample] = []
    var weeklyPlanArray : [Double] = []
}

class WeightStatsSample : NSObject {
    var startDate : Date = Date()
    var endDate : Date = Date()
    var weight : Double = 0.0
}

class CalorieStatsSample : NSObject {
    var startDate : Date = Date()
    var endDate : Date = Date()
    var calories : Double = 0.0
}


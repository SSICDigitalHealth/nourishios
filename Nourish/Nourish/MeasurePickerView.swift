//
//  MeasurePickerView.swift
//  Nourish
//
//  Created by Vino (Vinodha) Sundaramoorthy on 11/21/16.
//  Copyright © 2016 SSIC. All rights reserved.
//

import UIKit

class MeasurePickerView: UIView ,NRPickerViewDelegate, NRPickerViewDataSource {
    
    var picker : NRPickerView?
    var viewController : MealDiaryViewController?
    var servingType : ServingType?
    
    var measureArray : [String]? {
        get {
            return self.prepareMeasureArray()
        }
    }
    
    func setupPicker() {
        picker = NRChatUtility.horizontalPickerView()
        self.addSubview(picker!)
        picker?.delegate = self
        picker?.dataSource = self
        picker?.maskDisabled = false
        picker?.pickerViewStyle = .flat
        picker?.font = UIFont(name: "HelveticaNeue-Light", size: 20)!
        picker?.highlightedFont = UIFont(name: "HelveticaNeue-Light", size: 20)!
        picker?.reloadData()
        picker?.textColor = UIColor.white
        picker?.highlightedTextColor = UIColor.white
    }
    
    func prepareMeasureArray() -> [String] {
        let array : [String]? = ["1/4","1/2","1","1 1/4","1 1/2","2"]
        return array!
    }
    
    // MARK: Picker Delegates and data source
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
    }
    
    func numberOfItemsInPickerView(_ pickerView: NRPickerView) -> Int {
        return (measureArray?.count)!
    }
    
    func pickerView(_ pickerView: NRPickerView, titleForItem item: Int) -> String {
        return measureArray![item]
    }
    
    func pickerView(_ pickerView: NRPickerView, didSelectItem item: Int) {
       // let date = self.measureArray?[item]
        //viewController?.tabledView?.presenter.showDataFor(date: date!)
    }
    
}

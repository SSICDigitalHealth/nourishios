//
//  ScoreBreakDownModel.swift
//  Nourish
//
//  Created by Vlad Birukov on 14.06.17.
//  Copyright © 2017 SSIC. All rights reserved.
//

import Foundation

typealias historyScoreList = (subtitle: Date, value: Double)

class ScoreBreakDownModel : NSObject {
    var scoreUser: Double = 0.0
    var historyScore: [historyScoreList] = [historyScoreList]()
    var isDairyEmpty: Bool = false
}


//
//  OptifastFoodDetialsInteractor.swift
//  Nourish
//
//  Created by Gena Mironchyk on 10/23/17.
//  Copyright © 2017 SSIC. All rights reserved.
//

import UIKit
import RxSwift
class OptifastFoodDetialsInteractor: NSObject {
    private let foodCardRepo = NewFoodCardRepository()
    
    func execute(model : foodSearchModel) -> Observable<NewFoodCard> {
        return self.foodCardRepo.fetchOptifastFoodCard(model : model)
    }
}

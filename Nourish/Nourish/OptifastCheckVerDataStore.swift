//
//  OptfastCheckVerDataStore.swift
//  Optifast
//
//  Created by Gena Mironchyk on 2/28/18.
//  Copyright © 2018 SSIC. All rights reserved.
//

import UIKit
import RxSwift
let checkVerURL = "https://d1tn7dsuksyflf.cloudfront.net/VersionData.json"

class OptifastCheckVerDataStore: BaseNestleDataStore {
    
    func checkVersion() -> Observable<[String : Any]> {
        return Observable.create{observer in
            let url = URL(string: checkVerURL)!
            let request = URLRequest(url: url)
            
            let session = URLSession.shared.dataTask(with: request) {
                data, response, error in
                
                if error == nil && data != nil {
                    do {
                        if let jsonDict = try JSONSerialization.jsonObject(with: data!, options:[]) as? [String : Any] {
                            if let platformDict = jsonDict["Platform"] as? [String : Any] {
                                observer.onNext(platformDict["iOS"] as? [String : Any] ?? [String : Any]())
                            }
                        }
                    } catch let error {
                        LogUtility.logToFile("Error checking Optfiast version ", error)
                        observer.onError(error)
                    }
                } else {
                    if error != nil {
                        LogUtility.logToFile("Error checking Optfiast version ", error!)
                        observer.onError(error!)
                    }
                }
                self.log(foodProcessingOrWithings: false, url: url.absoluteString, requestMethod: request.httpMethod, urlResponse: response, requestBody: nil, error: error, responceBody: nil)
                observer.onCompleted()
            }
            session.resume()
            
            return Disposables.create {
                session.cancel()
            }
        }
    }
    
}

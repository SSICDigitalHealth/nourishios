//
//  File.swift
//  Nourish
//
//  Created by Andrey Zmushko on 8/3/17.
//  Copyright © 2017 SSIC. All rights reserved.
//

import Foundation

extension UIView {
    func drawRectangle(x: CGFloat, y: CGFloat, width: CGFloat, heigth: CGFloat, color: UIColor) {
        let rect = CGRect(x: x, y: y, width: width, height: heigth)
        
        let drawRect = UIBezierPath(rect: rect)
        
        color.setFill()
        drawRect.fill()
    }

    func drawSector(fillColor: UIColor, center: CGPoint, radius: CGFloat, startAngle: CGFloat, endAngle: CGFloat, clockwise: Bool) {
        let path = UIBezierPath(arcCenter: center,
                                radius: radius / 2,
                                startAngle: startAngle.degreesToRadians,
                                endAngle: endAngle.degreesToRadians,
                                clockwise: clockwise)
        path.lineWidth = radius
        fillColor.setStroke()
        path.stroke()
    }
    
    func setupContent<T>(_ view: inout T, nibName: String) {
        view = UINib(nibName: nibName, bundle: nil).instantiate(withOwner: nil, options: nil).first as! T
        self.addSubview(view as! UIView)
        (view as! UIView).bindEdgeTo(self)
    }
    
    func bindEdgeTo(_ view: UIView) {
        self.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint(item: self, attribute: .left, relatedBy: .equal, toItem: view, attribute: .left, multiplier: 1.0, constant: 0.0).isActive = true
        NSLayoutConstraint(item: self, attribute: .right, relatedBy: .equal, toItem: view, attribute: .right, multiplier: 1.0, constant: 0.0).isActive = true
        NSLayoutConstraint(item: self, attribute: .top, relatedBy: .equal, toItem: view, attribute: .top, multiplier: 1.0, constant: 0.0).isActive = true
        NSLayoutConstraint(item: self, attribute: .bottom, relatedBy: .equal, toItem: view, attribute: .bottom, multiplier: 1.0, constant: 0.0).isActive = true
    }
    

}

extension UILabel {
    func setLineSpacing(spacing: CGFloat, alignment: NSTextAlignment = .left) {
        if let text = self.text {
            let attributeString = NSMutableAttributedString(string: text)
            let style = NSMutableParagraphStyle()
            
            style.lineSpacing = spacing
            style.alignment = alignment
            attributeString.addAttribute(NSParagraphStyleAttributeName,
                                         value: style,
                                         range: NSMakeRange(0, text.count))
            
            self.attributedText = attributeString
        }
    }
}

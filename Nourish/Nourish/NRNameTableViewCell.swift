//
//  NRNameTableViewCell.swift
//  Nourish
//
//  Created by Vino (Vinodha) Sundaramoorthy on 11/14/16.
//  Copyright © 2016 SSIC. All rights reserved.
//

import UIKit

class NRNameTableViewCell: UITableViewCell {
    
    @IBOutlet weak var firstName : UITextField!
    @IBOutlet weak var lastName : UITextField!
    @IBOutlet weak var email : UITextField!
    @IBOutlet weak var updateImage : UIButton!
    @IBOutlet weak var profilePicture : UIImageView!


    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func setupWithNameModel(user : UserProfileModel) {
        self.firstName.text = user.userFirstName
        self.lastName.text = user.userLastName
        self.email.text = user.email
        profilePicture.image = NRImageUtility.getProfilePicture(isGlobalDrawer: false,userId: user.userID!)
    }
    
}

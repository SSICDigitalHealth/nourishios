//
//  Json_to_nutrients.swift
//  Nourish
//
//  Created by Nova on 1/25/17.
//  Copyright © 2017 SSIC. All rights reserved.
//

import UIKit
let kLHRValue = "lhr"
let kUHRValue = "uhr"
let kAmountValue = "amount"
let kFlagKey = "flag"

let kLittleFlag = "LHR"
let kMuchFlag = "UHR"
let kNormalFlag = "HR"

let kDeficiencyKey = "deficiency"
let kDescriptionKey = "description"
let kExcessKey = "excess"

let kUnitKey = "unit"
let kSourcesKey = "sources"
let kNameKey = "nutrient"

let kInfomissingKey = "infomissing"
let kScoreKey = "score"

class Json_to_nutrients: NSObject {
    func transform(dict : [String : Any]) -> nutrients {
        
        let flag = dict[kFlagKey] as? String ?? ""
        let value = dict[kAmountValue] as? NSNumber
        let descriptionString = dict[kDescriptionKey] as? String ?? ""
        let unitStr = dict[kUnitKey] as? String ?? ""
        let sources = dict[kSourcesKey] as? String ?? ""
        let name = dict[kNameKey] as? String ?? ""
        let score = dict[kScoreKey] as? Double ?? 0.0
        let deficiency = dict[kDeficiencyKey] as? String ?? ""
        let excess = dict[kExcessKey] as? String ?? ""
        
        let uhr : Double? = dict[kUHRValue] as? Double
        let lhr : Double? = dict[kLHRValue] as? Double
        
        
/*
        if self.nullToNil(value: dict[kLHRValue] as AnyObject?)  != nil && self.nullToNil(value: dict[kUHRValue] as AnyObject?) != nil {
            maxValue = dict[kUHRValue] as! Double
            minValue = dict[kLHRValue] as! Double
         
            if value < minValue {
                quantityString = kQuantityLow
            } else if value > maxValue {
                quantityString = kQuantityHigh
            } else {
                quantityString = kQuantityNormal
            }
            
        } else if self.nullToNil(value: dict[kLHRValue] as AnyObject?) == nil {
            maxValue = dict[kUHRValue] as! Double
            
            if value == 0 {
                quantityString = kQuantityLow
            } else if value <= maxValue {
                quantityString = kQuantityNormal
            } else {
                quantityString = kQuantityHigh
            }
            minValue = 0
        } else { //only minimal
            maxValue = dict[kLHRValue] as! Double
            
            if value < maxValue {
                quantityString = kQuantityLow
            }
            
            if value >= maxValue {
                quantityString = kQuantityNormal
                maxValue = value
            }
            minValue = 0
        }
*/
        let nutrient = nutrients()
        nutrient.flag = flag
        nutrient.descriptionString = descriptionString
        nutrient.value = value?.doubleValue ?? 0
        nutrient.unit = unitStr
        nutrient.sources = sources
        nutrient.name = name
        nutrient.score = score
        nutrient.deficiency = deficiency
        nutrient.excess = excess
        nutrient.uhr = uhr
        nutrient.lhr = lhr
        
        return nutrient
    }
    
    func transfrom(array : [[String : Any]]) -> [nutrients] {
        var arrayRe : [nutrients] = []
        
        for object in array {
            arrayRe.append(self.transform(dict: object))
        }
        return arrayRe
    }
    
    private func nullToNil(value : AnyObject?) -> AnyObject? {
        if value is NSNull {
            return nil
        } else {
            return value
        }
    }
    
    private func descriptionFrom(dict : [String : Any], flag : String) -> String {
        var descriptionString = ""
        if flag == kLittleFlag {
            descriptionString = dict[kDeficiencyKey] as? String ?? ""
        } else if flag == kMuchFlag {
            descriptionString = dict[kExcessKey] as? String ?? ""
        } else {
            descriptionString = dict[kDescriptionKey] as? String ?? ""
        }
        return descriptionString
    }
 }



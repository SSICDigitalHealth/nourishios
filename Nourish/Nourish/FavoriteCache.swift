//
//  FavoriteCache.swift
//  Nourish
//
//  Created by Nova on 7/13/17.
//  Copyright © 2017 SSIC. All rights reserved.
//

import Foundation
import RealmSwift

class FavoriteCache: Object {
    
    dynamic var data : Data!
    dynamic var date : Date!
    
}

//
//  foodSearchModel_to_jsonClarifai.swift
//  Nourish
//
//  Created by Gena Mironchyk on 9/11/17.
//  Copyright © 2017 SSIC. All rights reserved.
//

import UIKit

class foodSearchModel_to_jsonClarifai: foodSearchModel_to_json {
    override func transform(model : foodSearchModel) -> [String : Any] {
        var dict = super.transform(model: model)
        dict["id"] = model.mealIdString
        dict["name"] = model.foodTitle
        return dict
    }
}

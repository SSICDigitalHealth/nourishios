//
//  NRFoodDetailsViewController.swift
//  Nourish
//
//  Created by Vino (Vinodha) Sundaramoorthy on 11/21/16.
//  Copyright © 2016 SSIC. All rights reserved.
//

import UIKit
import RxSwift
protocol FoodDetailsViewControllerProtocol {
    func performDelegateActionWith(model : foodSearchModel)
}
protocol FoodDetailsViewControllerDelegate {
    func foodDetailsViewControllerDidFinishWith(foodSearchModel : foodSearchModel, presentedModal : Bool)
}
class NRFoodDetailsViewController: BasePresentationViewController, FoodDetailsViewControllerProtocol  {
    
    @IBOutlet weak var foodDetailsView : NRFoodDetailsView?
    @IBOutlet weak var backgroundBlurImageView : UIImageView!
    
    
    @IBOutlet weak var topLayoutConstraint : NSLayoutConstraint!
    @IBOutlet weak var bottomLayoutConstraint : NSLayoutConstraint!
    @IBOutlet weak var leadingLayoutConstraint : NSLayoutConstraint!
    @IBOutlet weak var trailingLayoutConstraint : NSLayoutConstraint!
    
    var blurView : UIVisualEffectView?
    var backgroundImage : UIImage?
    var ocassion : Ocasion?
    var date : Date?
    var foodModel : MealRecordModel?
    var foodSearchModel : foodSearchModel?
    var isFromGroup : Bool = false
    var clarifaiFood : Bool = false
    var delegate : FoodDetailsViewControllerDelegate?
    var updatingAnmount : Bool = false
    var presentedModally : Bool = false
    var hidesNavBarFavButton : Bool = true
    var isMealPrediction : Bool = false
    var hideFavButton : Bool = false
    
    override func viewWillAppear(_ animated: Bool) {
        if let view = self.foodDetailsView {
            view.viewController = self
            view.addToDiaryButton.addTarget(self, action: #selector(self.addToDiary(_:)), for: .touchUpInside)
            view.isFromGroup = self.isFromGroup
            view.updatingAmount = self.updatingAnmount
            view.presenter?.foodDetailsViewControler = self
            view.isMealPrediction = self.isMealPrediction
            view.showFavButton = self.isMealPrediction || self.hideFavButton
            if ocassion == nil {
                view.addToDiaryButton.setTitle("UPDATE AMOUNT", for: .normal)
            }
            self.baseViews = [view]
            if self.isBeingPresented == true {
                self.setupViewForModalPresentation()
                view.presentedModally = true
                self.presentedModally = true
            } else {
                let button = UIBarButtonItem(image: UIImage(named : "fav_inactive"), style: .plain, target: nil, action: nil)
                view.navBarFavButton = button
                self.navigationItem.rightBarButtonItem = view.navBarFavButton
            }
        }
    
        
        super.viewWillAppear(animated)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
    }

    private func applyBlurTo(image : UIImage) -> UIImage {
        let imageToBlur = CIImage(image: image)
        let blurfilter = CIFilter(name: "CIGaussianBlur")
        blurfilter?.setValue(5, forKey: kCIInputRadiusKey)
        blurfilter?.setValue(imageToBlur, forKey: "inputImage")
        let resultImage = blurfilter?.value(forKey: "outputImage") as! CIImage
        var blurredImage = UIImage(ciImage: resultImage)
        let cropped:CIImage=resultImage.cropping(to: CGRect(x:0, y: 0,width: (imageToBlur?.extent.size.width)!, height: (imageToBlur?.extent.size.height)!))
        blurredImage = UIImage(ciImage: cropped)
        return blurredImage
    }
    
    func performDelegateActionWith(model: foodSearchModel) {
        self.foodDetailsView?.deinitAll()
        self.foodDetailsView = nil
        self.delegate?.foodDetailsViewControllerDidFinishWith(foodSearchModel: model, presentedModal: self.presentedModally)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    
    private func setupViewForModalPresentation() {
        if let view = self.foodDetailsView {
            self.topLayoutConstraint.constant = 80.0
            self.leadingLayoutConstraint.constant = 10.0
            self.trailingLayoutConstraint.constant = 10.0
            self.bottomLayoutConstraint.constant = 86.0
            view.titleLabel.text = self.foodSearchModel?.foodTitle
            view.titleLabel.textColor = NRColorUtility.navigationTitleColor()
            if self.backgroundImage != nil {
                self.backgroundBlurImageView.image = self.backgroundImage!.applyBlurWithRadius(1.5, tintColor: UIColor.black.withAlphaComponent(0.6), saturationDeltaFactor: 1)!
            }
            self.view.layoutIfNeeded()
        }
        
    }
    
    func removeRightItem(){
        self.navigationItem.rightBarButtonItems = nil
    }

    
    @IBAction func addToDiary(_ sender:UIButton) {
        sender.isEnabled = false
        if self.updatingAnmount == true {
            self.dismiss(animated: true, completion: {
                self.foodDetailsView?.performDelegateAction()
            })
        } else {
            if ocassion != nil {
                self.foodDetailsView?.addToDiaryForOcassion(ocasion: self.ocassion!)
            } else {
                self.foodDetailsView?.changeFood()
            }
        }
    }
    
    func dismissView () {
        if self.presentedModally == true {
            self.dismiss(animated: true, completion: nil)
        } else {
            self.navigationController?.popViewController(animated: true)
        }
    }
    
    func diableNavigationBar() {
        self.navigationController?.navigationBar.isUserInteractionEnabled = false
        self.navigationController?.view.isUserInteractionEnabled = false
    }
    
    func finishSaveToDiary() {
        DispatchQueue.main.async {
            self.foodDetailsView?.deinitAll()
            self.foodDetailsView = nil
            if self.presentedModally == true {
                self.foodDetailsView?.deinitAll()
                self.foodDetailsView = nil
                self.delegate?.foodDetailsViewControllerDidFinishWith(foodSearchModel: self.foodSearchModel!, presentedModal: self.presentedModally)
            }
            else {
                self.navigationController?.navigationBar.isUserInteractionEnabled = true
                self.navigationController?.view.isUserInteractionEnabled = true
                self.navigationController?.popViewControllerWithHandler {
                    self.foodDetailsView?.deinitAll()
                    self.foodDetailsView = nil
                    self.delegate?.foodDetailsViewControllerDidFinishWith(foodSearchModel: self.foodSearchModel!, presentedModal: self.presentedModally)
                }
            }
        }
    }
    
    override func respondToSwipeGesture(gesture: UISwipeGestureRecognizer) {
        let touchPoint = gesture.location(in: self.view)
        if touchPoint.x < 90 {
            super.respondToSwipeGesture(gesture: gesture)
        }
    }
}

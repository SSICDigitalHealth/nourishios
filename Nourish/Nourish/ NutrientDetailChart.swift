//
//   NutrientDetailChart.swift
//  Nourish
//
//  Created by Vlad Birukov on 29.08.17.
//  Copyright © 2017 SSIC. All rights reserved.
//

import Foundation

class NutrientDetailChart {
    func transform(model: NutrientModel) -> NutrientViewModel {
        let nutrientViewModel = NutrientViewModel()
        nutrientViewModel.nameNutritional = model.nameNutritional
        nutrientViewModel.consumedNutritional = model.consumedNutritional
        nutrientViewModel.targetNutritional = model.targetNutritional
        nutrientViewModel.unit = model.unit
        nutrientViewModel.message = model.message
        nutrientViewModel.flagImage = model.flagImage
        
        return nutrientViewModel
    }
}

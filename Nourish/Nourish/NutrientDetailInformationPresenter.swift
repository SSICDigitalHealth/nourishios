 //
//  NutrientDetailInformationPresenter.swift
//  Nourish
//
//  Created by Vlad Birukov on 11.08.17.
//  Copyright © 2017 SSIC. All rights reserved.
//

import Foundation

class NutrientDetailInformationPresenter: BasePresenter, UITableViewDataSource,UITableViewDelegate  {
    
    var nutrientDetailInformationView: NutrientDetailInformationProtocol!
    var nutrienModel = NutrientDetailInformationViewModel()
    var nutrienChartModel = NutrientViewModel()
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    
    public func numberOfSections(in tableView: UITableView) -> Int {
        return nutrienModel.nutrientComeFrom.caption != nil ? ((nutrienModel.nutrientComeFrom.caption?.count)! + 1) : 0
    }
    
    public func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        if section == 0 {
            let  headerCell = tableView.dequeueReusableCell(withIdentifier: "cellNutrientHeader") as! NutrientHeaderTableViewCell
            
            headerCell.setupWith(model: self.nutrienChartModel, nutrientInformationModel: self.nutrienModel)
            
            return headerCell
            
        } else {
            let  headerCell = tableView.dequeueReusableCell(withIdentifier: "cellHeaderNutrient") as! HeaderNutrientTableViewCell
            
            let currentText = self.nutrienModel.nutrientComeFrom.caption?[section - 1]
            
            if let config = self.nutrientDetailInformationView.config() {
                headerCell.setupWith(text: currentText!, section: section, config: config)
            }
            
            return headerCell
        }
    }
    
    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch section {
        case 0:
            return 0
        case 1:
            if self.nutrienModel.nutrientComeFrom.nameFood?[section - 1] != nil && self.nutrienModel.nutrientComeFrom.nameFood![section - 1][0] != ""{
                if self.nutrienModel.nutrientComeFrom.nameFood![section - 1].count > 4 {
                    return 4
                } else {
                    return self.nutrienModel.nutrientComeFrom.nameFood![section - 1].count
                }
            } else {
                return 0
            }
        case 2:
            return 1
        default:
            return 0
        }
    }
    
    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if indexPath.section == 1 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "cellNutrien", for: indexPath) as! NutrientTableViewCell
            
            let currentName = self.nutrienModel.nutrientComeFrom.nameFood?[indexPath.section - 1][indexPath.row]
            cell.setupWith(name: currentName!)
            
            return cell
        } else {
            let cell = tableView.dequeueReusableCell(withIdentifier: "cellNutrientRich", for: indexPath) as! NutrientRichTableViewCell
            
            let message = self.nutrienModel.nutrientComeFrom.nameFood?[indexPath.section - 1][indexPath.row]
            cell.setupWith(message: message!)
            
            return cell
        }
        
    }
}

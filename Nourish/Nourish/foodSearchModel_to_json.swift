//
//  foodSearchModel_to_json.swift
//  Nourish
//
//  Created by Nova on 2/14/17.
//  Copyright © 2017 SSIC. All rights reserved.
//

import UIKit

class foodSearchModel_to_json: NSObject {
    func transform(model : foodSearchModel) -> [String : Any] {
        var dict : [String : Any] = [:]
        dict["amount"] = model.amount
        dict["calories"] = Double(model.calories)
        dict["unit"] = model.unit
        
        if model.grams?.isNaN == false && model.grams != Double.infinity {
            dict["grams"] = model.grams
        } else {
            dict["grams"] = 0.0
        }
        
        if model.userPhotoId != nil {
            dict["user_photo_id"] = model.userPhotoId
        }
        
        if model.sourceFoodDB != nil && model.sourceFoodDBID != nil {
            dict[sfdbid] = model.sourceFoodDBID
            dict[sfdb] = model.sourceFoodDB
        }
    
        return dict
    }
}

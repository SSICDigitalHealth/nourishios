//
//  StressViewController.swift
//  Nourish
//
//  Created by Gena Mironchyk on 2/2/17.
//  Copyright © 2017 SSIC. All rights reserved.
//

import UIKit
class StressViewController : BasePresentationViewController, StressViewControllerProtocol {
    @IBOutlet var dailyStressChartView : DailyStressChartView!
    @IBOutlet var weeklyStressChartView : WeeklyStressChartView!
    var presenter = StressPresenter()
    @IBOutlet weak var segmentedControl : NRSegmentedControl!

    override func viewDidLoad() {
        super.viewDidLoad()
        presenter.stressController = self
        self.automaticallyAdjustsScrollViewInsets = false
        self.setupNavigationBar()
        self.setUpSegmentControl()
        self.baseViews = [self.dailyStressChartView,self.weeklyStressChartView]
        self.navigationController?.interactivePopGestureRecognizer?.isEnabled = false
        self.swipeRight.isEnabled = false
        //self.baseViews = [self.dailyStressChartView!]
        //self.dailyStressChartView?.viewDidLoad()
    }
    

    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.navigationController?.interactivePopGestureRecognizer?.isEnabled = true

    }
    func setupNavigationBar() {
        self.title = "Stress"
        self.navigationController?.navigationBar.backItem?.title = ""
        self.navigationController?.navigationBar.setBackgroundImage(NRColorUtility.symplifiedNavigationColor().as1ptImage(), for: .default)
        self.navigationController?.navigationBar.shadowImage = NRColorUtility.symplifiedNavigationColor().as1ptImage()
    }
    
    func showDailyChart() {
        self.dailyStressChartView.isHidden = false
        // self.dailyStressChartView?.v()
        self.baseViews = [self.dailyStressChartView]
        self.weeklyStressChartView.isHidden = true
    }
    
    func showWeeklyChart() {
        self.dailyStressChartView.isHidden = true
        self.weeklyStressChartView.isHidden = false
        //self.weeklyStressChartView?.viewDidLoad()
        self.baseViews = [self.weeklyStressChartView]
    }

    
    func setUpSegmentControl(){
        let segmentColor = NRColorUtility.nourishNavigationColor()
        let textFont = UIFont.systemFont(ofSize: 15)
        let textColor = UIColor.white
        
        segmentedControl = NRSegmentedControl(frame: CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: 45), titles: ["DAILY","WEEKLY"], selectedtitles:[], action: {
            control, index in
        })
        segmentedControl.appearance = segmentAppearance(backgroundColor: segmentColor, selectedBackgroundColor:segmentColor, textColor: textColor, font: textFont, selectedTextColor: textColor, selectedFont: textFont, bottomLineColor: segmentColor, selectorColor: UIColor.white, bottomLineHeight: 5, selectorHeight: 5, labelTopPadding: 0, hasImage: false, imageSize: nil)
        segmentedControl.delegate = presenter
        self.view.addSubview(segmentedControl)
    }

}

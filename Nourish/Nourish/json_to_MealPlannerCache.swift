//
//  json_to_MealPlannerCache.swift
//  Nourish
//
//  Created by Gena Mironchyk on 9/13/17.
//  Copyright © 2017 SSIC. All rights reserved.
//

import UIKit

class json_to_MealPlannerCache: NSObject {
    
    
    func transform(dictionary : [String : Any], dictionaryFull: [String: Any]) -> MealPlannerCache {
        let cache = MealPlannerCache()
        
        if dictionary["day"] != nil {
            cache.date = self.dateFrom(dayString: dictionary["day"] as! String)

        }
        
        if dictionary["summary"] != nil {
            let summaryDictionary = dictionary["summary"] as! [String : Any]
            cache.totalCalories =  summaryDictionary["total_kcal"] as? Double ?? 0.0
            cache.score = (summaryDictionary["score"] as? Double ?? 0.0) * 100.0
            
            if dictionary["occasions"] != nil {
                let ocasionsDict = dictionary["occasions"] as! [[String : Any]]
                cache.ocasionData.append(contentsOf: self.ocasionDataFrom(arrayOfDictionaries: ocasionsDict))
            }
            
            if dictionaryFull["additional_info"] != nil && !(dictionaryFull["additional_info"] is NSNull) {
                let addditionalInfo =  dictionaryFull["additional_info"] as! [String : Any]
                
                if addditionalInfo["optifast_products"] != nil {
                    cache.optifastMealsData = self.dataFrom(array: addditionalInfo["optifast_products"] as! [Any])
                }
            }
        }
        
        return cache
    }
    
    private func ocasionDataFrom(arrayOfDictionaries : [[String : Any]]) -> [OcasionData] {
        var arrayToReturn = [OcasionData]()
        for dictionary in arrayOfDictionaries {
            let data = OcasionData()
            data.calsTotal = dictionary["total_kcal"] as? Double ?? 0.0
            
            if dictionary["meals"] != nil {
                data.mealsData = self.dataFrom(arrayOfDicts: dictionary["meals"] as! [[String : Any]])
            }
            
            let realmString = RealmString()
            
            if dictionary["meals"] != nil {
                let convertedArray = self.jsonToNestleRecipes(arraysOfDictionaries: dictionary["meals"] as! [[String: Any]])
                let _ = convertedArray.map({data.recipiesData.append($0)})
            }
            
            realmString.stringValue = dictionary["occasion_name"] as? String ?? ""
            data.ocasionName = RealmString(value : realmString)
            arrayToReturn.append(data)
        }
        return arrayToReturn
    }
    

    private func jsonToNestleRecipes(arraysOfDictionaries: [[String : Any]]) -> [RecipiesCacheModel] {
        var arrayOfRecipesReturn = [RecipiesCacheModel]()
        
        for recipesObj in arraysOfDictionaries {
            arrayOfRecipesReturn.append(self.arrayToRecipes(recepiesDict: recipesObj))
        }
        
        return arrayOfRecipesReturn
    }
    
    private func arrayToRecipes(recepiesDict: [String : Any]) -> RecipiesCacheModel {
        let recipesModel = RecipiesCacheModel()
        
        if !(recepiesDict["food_id"] is NSNull) {
            recipesModel.foodId = recepiesDict["food_id"] as? String ?? ""
        }
        
        if !(recepiesDict["food_name"] is NSNull) {
            recipesModel.nameFood = recepiesDict["food_name"] as? String ?? ""
        }
        
        if !(recepiesDict["unit"] is NSNull) {
            recipesModel.unit = recepiesDict["unit"] as? String ?? ""
        }
        
        if !(recepiesDict["grams"] is NSNull) && !(recepiesDict["kcal_per_gram"] is NSNull) {
            recipesModel.numberCal = (recepiesDict["grams"] as? Double ?? 0.0) * (recepiesDict["kcal_per_gram"] as? Double ?? 0.0)
        }
        
        if recepiesDict["ingredients"] != nil {
            let convertedArray =  recepiesDict["ingredients"] as? [String]
            if convertedArray != nil {
                for item in convertedArray! {
                    let realmStr = RealmString()
                    realmStr.stringValue = item
                    recipesModel.ingredient.append(RealmString(value: realmStr))
                }
            }
        }
        
        if !(recepiesDict["amount"] is NSNull) {
            recipesModel.amount = recepiesDict["amount"] as? Double ?? 0.0
        }
        
        if !(recepiesDict["grams"] is NSNull) {
            recipesModel.grams = recepiesDict["grams"] as? Double ?? 0.0
        }
        
        if !(recepiesDict["image_url"] is NSNull) && !(recepiesDict["food_id"] is NSNull) {
            let result  = self.saveImage(url: recepiesDict["image_url"] as? String, foodId: recepiesDict["food_id"] as? String)
            if result.1 == true {
                recipesModel.pathImage = result.0
            }
        }
        
        if recepiesDict["instructions"] != nil {
            let convertedArray =  recepiesDict["instructions"] as? [String]
            if convertedArray != nil {
                for item in convertedArray! {
                    let realmStr = RealmString()
                    realmStr.stringValue = item
                    recipesModel.instruction.append(RealmString(value: realmStr))
                }
            }
        }
        
        return recipesModel
    }
    
    private func dateFrom(dayString : String) -> Date {
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyyMMdd"
        let date = formatter.date(from: dayString)
        return Calendar.current.startOfDay(for: date!)
    }
    
    private func dataFrom(arrayOfDicts : [[String : Any]]) -> Data {
       return try! JSONSerialization.data(withJSONObject: arrayOfDicts, options: .prettyPrinted)
    }
    
    private func dataFrom(array: [Any]) -> Data {
        return try! JSONSerialization.data(withJSONObject: array, options: .prettyPrinted)
    }
    
    private func saveImage(url: String?, foodId: String?) -> (String, Bool) {
        var path = ""
        var result  = false
        
        if url != nil && foodId != nil {
            path = String(format: "%@.png", foodId!)
            result = true
            
            if !(self.isLoadImageLater(foodId: foodId!)) {
                DispatchQueue.global().async {
                    let urlImage = URL(string: url!)
                    let data = try? Data(contentsOf: urlImage!)
                    if data != nil {
                        let imageRecipes = UIImage(data: data!)
                        
                        let documentsURL = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first!
                        let fileURL = documentsURL.appendingPathComponent(String(format: "%@", path))
                        if let pngImageData = UIImagePNGRepresentation(imageRecipes!) {
                            try? pngImageData.write(to: fileURL, options: .atomic)
                        }
                    }
                }
            }
            
        }
        
        return (path, result)
    }
    
    private func isLoadImageLater(foodId: String) -> Bool {
        let documentsURL = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first!
        let filePath = documentsURL.appendingPathComponent(String(format: "%@.png", foodId)).path
        
        if FileManager.default.fileExists(atPath: filePath) {
            return true
        }
        
        return false
    }
}

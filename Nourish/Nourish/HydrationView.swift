//
//  HydrationView.swift
//  Nourish
//
//  Created by Vlad Birukov on 14.06.17.
//  Copyright © 2017 SSIC. All rights reserved.
//

import UIKit

protocol HydrationProtocol : BaseViewProtocol {
    func setupWithModel(model: HydrationViewModel)
    func config() -> ProgressConfig?
}

class HydrationView: ProgressView, HydrationProtocol {

    @IBOutlet weak var hydration: UILabel!
    @IBOutlet weak var blobCustomView: BlobCustomView!
    @IBOutlet var hydrationView: UIView!
    var presenter: HydrationPresenter!
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.presenter.hydrationView = nil
        self.presenter = nil
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.presenter = HydrationPresenter()
        if let config = self.progressConfig {
            self.renderView(config)
        }
        self.basePresenter = self.presenter
        self.presenter.hydrationView = self
        super.viewWillAppear(animated)
        self.removeActivityIndicator()
    }
    
    func setupWithModel(model: HydrationViewModel) {
        var hydration = 0.0
        var daysInPeriod: Int {
            if let config = self.progressConfig {
                let epoch = config.getDate()
                return (Calendar.current.dateComponents([.day], from: epoch.start, to: epoch.end).day ?? 0) + 1
            }
            return 1
        }
        
        self.hydration.text = String(format: "%@/%@ %@", NRFormatterUtility.doubleToString(num: NRFormatterUtility.roundCupElements(number: model.consumedHydration)), NRFormatterUtility.doubleToString(num:(NRFormatterUtility.roundCupElements(number: model.targetHydration))), model.unit)
        
        if model.targetHydration != 0.0 {
            hydration = NRFormatterUtility.roundCupElements(number:model.consumedHydration) / NRFormatterUtility.roundCupElements(number:model.targetHydration)
        } else if model.consumedHydration != 0.0 {
            hydration = 2.0 + model.consumedHydration
        }
        
        self.blobCustomView.setupWith(hydrationValue: hydration)
    }
    
    override func renderView(_ config: ProgressConfig) {
        super.renderView(config)
        self.hydrationView =  UINib(nibName: "HydrationView", bundle: nil).instantiate(withOwner: self, options: nil)[0] as! UIView
        self.loadContent(contentView: hydrationView)
        self.loadWhiteThemeWith(header: "Hydration")
    }
    
    override func setupWith(Model model: AnyObject) {
        super.setupWith(Model: model)
    }
    
    override func gestureTapAction() {
        if let hook = tapHook {
            EventLogger.logClickedOnHydration()
            hook(.hydration)
        }
    }
}

//
//  MetaDataRepository.swift
//  NourIQ
//
//  Created by Igor on 6/5/18.
//  Copyright © 2018 SSIC. All rights reserved.
//

import Foundation
import RxSwift

let kSessionID = "26AC9476-1CEF-49F5-9B5B-A0541F8EDC61"
let instanceID = UIDevice.current.identifierForVendor?.uuidString ?? ""
let kNA = "N/A"
let kDHSourceString = "com.samsung.nestle.nourish_optifast"

enum DHDataSchema : String {
    case annotation = "Annotation"
    case binary = "Binary"
    case image = "Image"
    case jSON = "JSON"
    case questionnaire = "Questionnaire"
    case timeRangeValueSeries = "TimeRangeValueSeries"
    case timeSeries = "TimeSeries"
    case timeValueSeries = "TimeValueSeries"
    case video = "Video"
}


enum DHDataType : String {
    case ecg = "com.samsung.ssic.dh.ecg"
    case ecgAnnotation = "com.samsung.ssic.dh.ecg-annotation"
    case calories = "com.samsung.ssic.dh.fitness.calories"
    case distance = "com.samsung.ssic.dh.fitness.distance"
    case excercise = "com.samsung.ssic.dh.fitness.excercise"
    case height = "com.samsung.ssic.dh.fitness.height"
    case hr = "com.samsung.ssic.dh.fitness.hr"
    case sleep = "com.samsung.ssic.dh.fitness.sleep"
    case stepCount = "com.samsung.ssic.dh.fitness.stepcount"
    case weight = "com.samsung.ssic.dh.fitness.weight"
    case ppg = "com.samsung.ssic.dh.ppg"
    case food = "com.samsung.ssic.dh.food_1"
    
    static func dhDataTypeToStreams(type: DHDataType) -> [String] {
        switch type {
        case .ecg:
            return ["ECG"]
        case .ppg:
            return ["PPG"]
        default:
            return ["ECG"]
        }
    }
    
}
class MetaDataRepository {
    let userRepository = UserRepository.shared
    
    func getDHMetaDataFor(dhSchema : DHDataSchema, dhType : DHDataType) -> Observable<[String : Any]> {
        let user = self.userRepository.getCurrentUser(policy: .Cached)
        let instanceID = UUID().uuidString
        
        return user.map { userProfile -> [String : Any] in
            var dictToReturn = [String : Any]()
            
            var dataContentsDict = [String : Any]()
            dataContentsDict["Schema"] = dhSchema.rawValue
            dataContentsDict["Type"] = dhType.rawValue
            
            var applicationContentsDict = [String : Any]()
            applicationContentsDict["InstanceID"] = instanceID
            applicationContentsDict["Name"] = kDHSourceString
            
            var deviceContentsDict = [String : Any]()
            deviceContentsDict["FW version"] = kNA
            deviceContentsDict["ID"] = "FoodManager"
            deviceContentsDict["Name"] = kNA
            deviceContentsDict["Sensor"] = ["SubType" : kNA, "Type" : kNA]
            deviceContentsDict["SW version"] = kNA
            deviceContentsDict["Vendor"] = kNA
            
            var sourceContentDict = [String : Any]()
            sourceContentDict["Application"] = applicationContentsDict
            sourceContentDict["Device"] = deviceContentsDict
            
            
            var anthropometricsContentsDict = [String : Any]()
            anthropometricsContentsDict["Age"] = 0
            anthropometricsContentsDict["Ethnicity"] = kNA
            anthropometricsContentsDict["Gender"] = kNA
            anthropometricsContentsDict["Height"] = 0
            anthropometricsContentsDict["Weight"] = 0
            
            var subjectContentsDict = [String : Any]()
            subjectContentsDict["ID"] = userProfile.userID ?? ""
            subjectContentsDict["Anthropometrics"] = anthropometricsContentsDict
            subjectContentsDict["Lifestyle"] = self.generateFakeLifeStyleDict()
            
            subjectContentsDict["MedicalHistory"] = self.generateFakeMedicalHistory()
            subjectContentsDict["Protocol"] = self.generateFakeProtocol()
            subjectContentsDict["Skin"] = self.generateFakeSkin()
            
            var metaContent = [String : Any]()
            metaContent["Data"] = dataContentsDict
            metaContent["Source"] = sourceContentDict
            metaContent["Subject"] = subjectContentsDict
            
            var startTime = [String : Any]()
            startTime["StartTime"] = Int(Date().timeIntervalSince1970) * 1000
            
            metaContent["Time"] = startTime
            
            return metaContent
        }
        
    }
    
    
    private func generateFakeLifeStyleDict() -> [String : Any] {
        var dictToReturn = [String : Any]()
        let dietContentsDict = ["AvgAlcoholConsumption" : 0,"AvgCaffeineConsumption" : 0, "CaffeineConsumed" : 0 ]
        let excersiseContentDict = ["Frequency" : 0, "Type" : kNA] as [String : Any]
        let sleepContentDict = ["Alone" : true, "AvgHours" : 8] as [String : Any]
        
        dictToReturn["Diet"] = dietContentsDict
        dictToReturn["Exercise"] = excersiseContentDict
        dictToReturn["Sleep"] = sleepContentDict
        dictToReturn["Smoker"] = false
        
        return dictToReturn
        
    }
    
    
    private func generateFakeMedicalHistory() -> [String : Any]{
        return ["CardiacDisease" : "None","Diabetic" : "NO", "Hypertension" : false, "RespiratoryDisease" : "None"] as [String : Any]
    }
    
    private func generateFakeProtocol() -> [String : Any] {
        return ["Admin" : "Brian Whitty", "Description" : "Afib Study using ArtikBio Band", "Duration" : 30, "ID" : 274500969, "Location" : "USA", "Purpose" : "Afib Study", "SessionID" : kSessionID] as [String : Any]
    }
    
    private func generateFakeSkin() -> [String : Any] {
        return ["Color" : kNA,"FitzPatrickScale" : 0] as [String : Any]
    }
    
}

//
//  NRFoodSearchViewController.swift
//  Nourish
//
//  Created by Vino (Vinodha) Sundaramoorthy on 11/15/16.
//  Copyright © 2016 SSIC. All rights reserved.
//

import UIKit
import AVFoundation
import SpeechKit

protocol NRFoodSearchProtocol : BaseViewProtocol {
    // Food Details
    func openFoodDetailsWithModel(model: foodSearchModel, isFrequentMeal : Bool)
    func openGroupDetailswithModel(model:MealRecordModel, date : Date)
    func getGroupIndexPathForCell(cell:UITableViewCell) -> IndexPath?
    //Display search result
    func showSearchResult(dismissKeyboard : Bool)
    
    //Voice Search
    func updateVoiceSearchState(state : SKSState)
    func hideVoiceProgressView(searchString : String)
    
    // Bar code
    func finishScanning(barcode : String)
    func stopScanning(barcode : String)
    
    func showTableView()
    func hideTableView()
    
    func showCameraSnap ()
    func showVoiceSearch()
    
    func reloadTableView()
    func showNoResultsLabel()
    func showEmptyFrequentMealsLabel()
    func clearSearchBar()
    func deselctAllTabs(dismissKeyboard : Bool)
    
    func setPreviousSelectedSegment(segmentIndex : Int)
    
    func selectedPickerIndex() -> SelectedTab
    
    func setSearchBarFrame()
    func toggleInActiveSearchImage(canShow:Bool)
}


class NRFoodSearchViewController: BasePresentationViewController, FoodDetailsViewControllerDelegate, RecognitionViewContollerDelegate ,VoiceMealListDelegate{
    @IBOutlet weak var foodSearchView : NRFoodSearchView?
    var captureSession : AVCaptureSession!
    var previewLayer : AVCaptureVideoPreviewLayer!
    // Settings
    var language: String!
    var contextTag: String!
    var occasion : Ocasion?
    var endpointer: SKTransactionEndOfSpeechDetection!
    var date : Date?
    var shouldPredictMeal : Bool = false
    
    var skSession:SKSession?
    var skTransaction:SKTransaction?
    var recognitionType: String!
    var state = SKSState.idle
    var automaticSearchText : String = ""
    var showAsModel : Bool = false
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        self.foodSearchView?.setSearchBarFrame()
    }
    
    func setUpNavigationBarButtons () {
        let closeButton : UIBarButtonItem =  UIBarButtonItem(image: UIImage(named:"close_icon"), style: .plain, target: self, action: #selector(self.closeAction(_:)))
        self.navigationController?.navigationItem.leftBarButtonItem = closeButton
        self.navigationController?.navigationBar.tintColor = NRColorUtility.progressLabelColor()
        self.navigationController?.navigationBar.barTintColor = UIColor.white
        navigationItem.leftBarButtonItem = closeButton
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.foodSearchView?.setNeedsLayout()
        recognitionType = SKTransactionSpeechTypeDictation
        self.foodSearchView?.controller = self
        self.foodSearchView?.initialSetUp(canShowPrediction: self.shouldPredictMeal)
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
    }
    
    func reloadTableView() {
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.title = Ocasion.mealTypeString(servingType: self.occasion!).uppercased()
        self.navigationController?.navigationBar.setValue(true, forKey: "hidesShadow")
        if self.showAsModel {
            self.setUpNavigationBarButtons()
        }
        self.foodSearchView?.updateView()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.navigationController?.navigationBar.setValue(false, forKey: "hidesShadow")
        NotificationCenter.default.removeObserver(self, name: .UIKeyboardWillShow, object: nil)
        NotificationCenter.default.removeObserver(self, name: .UIKeyboardWillHide, object: nil)
        self.title = "Back"
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }
    
    // MARK : NRFoodSearchProtocol implementation
    func showSearchResult() {
        
    }
    
    func showFavouriteFood() {
        
    }
    
    func updateVoiceSearchState(state: SKSState) {
        self.state = state
    }
    
    func stopScanning(barcode : String) {
        if ((self.previewLayer) != nil) {
            self.previewLayer .removeFromSuperlayer()
            self.captureSession = nil
        }
    }
    
    func finishScanning(barcode: String) {
    }
    
    // MARK: Bar code scanner method
     func createMediaLayerWith(mediaType: String) {
        
        if self.captureSession == nil {
        self.captureSession = AVCaptureSession()

        let captureDevice = AVCaptureDevice.defaultDevice(withMediaType: mediaType)
        
        //capture input
        let videoInput : AVCaptureDeviceInput?
        
        do {
            videoInput = try AVCaptureDeviceInput(device: captureDevice)
        } catch {
            return
        }
        
        //Add input to the session
        
        if (self.captureSession.canAddInput(videoInput)) {
            self.captureSession.addInput(videoInput)
        } else {
            print("Scanning not possible")
        }
        
        //Create output
        
        let metaDataOutput = AVCaptureMetadataOutput()
        
        //Add output to session
        if (self.captureSession.canAddOutput(metaDataOutput)) {
            self.captureSession.addOutput(metaDataOutput)
            metaDataOutput.setMetadataObjectsDelegate(self.foodSearchView?.presenter, queue: DispatchQueue.main)
            metaDataOutput.metadataObjectTypes = [AVMetadataObjectTypeEAN13Code,AVMetadataObjectTypeUPCECode,AVMetadataObjectTypeEAN8Code,AVMetadataObjectTypeCode39Code,AVMetadataObjectTypeQRCode,AVMetadataObjectTypeCode93Code]
        }
        
        self.previewLayer = AVCaptureVideoPreviewLayer(session: self.captureSession)
        self.previewLayer.frame = (self.foodSearchView?.resultsTableView?.frame)!
        self.previewLayer.videoGravity = AVLayerVideoGravityResizeAspectFill
        self.foodSearchView?.layer.addSublayer(self.previewLayer)
        self.foodSearchView?.presenter.captureSession = self.captureSession
        self.captureSession.startRunning()
        EventLogger.logBarcodeScan()

        }
    }
    
    func hideVoiceProgressView (searchString : String) {
        foodSearchView?.stopVoiceSearchWithText(searchString : searchString)
    }
    
    func openFoodDetailsWithModel(model:foodSearchModel, isFrequentMeal : Bool) {
        let vc = NRFoodDetailsViewController(nibName: "NRFoodDetailsViewController", bundle: nil)
        vc.title = model.foodTitle
        vc.foodSearchModel = model
        vc.foodSearchModel?.isFromNoom = true
        vc.ocassion = self.occasion
        vc.date = self.date
        //vc.isMealPrediction = isFrequentMeal
        vc.delegate = self

        if model.userPhotoId == nil {
            vc.backgroundImage = NavigationUtility.takeScreenShot()
            vc.hidesNavBarFavButton = false
            self.present(vc, animated: true)
        } else {
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    
    func foodDetailsViewControllerDidFinishWith(foodSearchModel: foodSearchModel, presentedModal: Bool) {
        self.foodSearchView?.itemAdded = true
        if presentedModal == true {
            self.dismiss(animated: true, completion: {
                self.foodSearchView?.reloadTableView()
            })
        } else {
            self.foodSearchView?.reloadTableView()
        }
    }
    
    func openGroupDetailswithModel(model:MealRecordModel, date : Date) {
        let vc = GroupFoodDetailsViewController(nibName: "GroupFoodDetailsViewController", bundle: nil)
        vc.date = date
        vc.ocasion = self.occasion
        vc.mealRecord = model
        vc.title = model.groupName
        vc.fromFav = true
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    // MARK: Voice Search Methods
    /*func setUpVoiceSearch() {
        endpointer = .short
        language = LANGUAGE
        contextTag = SKSNLUContextTag
        state = .idle
        skTransaction = nil
        
        // Create a session
        skSession = SKSession(url: URL(string: SKSServerUrl), appToken: SKSAppKey)
        
        if (skSession == nil) {
            let alertView = UIAlertController(title: "SpeechKit", message: "Failed to initialize SpeechKit session.", preferredStyle: .alert)
            let defaultAction = UIAlertAction(title: "OK", style: .default) { (action) in }
            alertView.addAction(defaultAction)
            present(alertView, animated: true, completion: nil)
            return
        }
        loadEarcons()
    }
    
    func loadEarcons() {
        //Load the wave gif
    }
    
    func toggleRecognition() {
        switch state {
        case .idle:
            recognize()
        case .listening:
            stopRecording()
        case .processing:
            cancel()
        }
    }*/
    
    
    func showErrorAlertFor(type : String) {
        var alertMessage = ""
        var alertTitle = ""
        if type == AVMediaTypeAudio{
            alertMessage = "You can re-enable microphone access for Nourish in Settings"
            alertTitle = "Can't use microphone"
        } else {
            alertMessage = "You can re-enable camera access for Nourish in Settings"
            alertTitle = "Camera Access Disabled"
        }
        let alert = UIAlertController.init(title: alertTitle, message: alertMessage, preferredStyle: UIAlertControllerStyle.alert)
        let settingsAction = UIAlertAction(title: "Settings", style: UIAlertActionStyle.default, handler: { action in
            UIApplication.shared.openURL(URL(string: UIApplicationOpenSettingsURLString)!)
            self.foodSearchView?.updateView()
        })
        
        let dismissAction = UIAlertAction(title: "Cancel", style: UIAlertActionStyle.default, handler: { action in
            self.foodSearchView?.updateView()
        })
        alert.addAction(dismissAction)
        alert.addAction(settingsAction)
        self.present(alert, animated: true, completion: nil)
        
    }
    
    /*func recognize() {
        // Start listening to the user.
        //Context tag is not working fine , will use NLU after the issue is resolved
        
         skTransaction = skSession!.recognize(withService: contextTag,
         detection: endpointer,
         language: language,
         data: nil,
         options: nil,
         delegate: self.foodSearchView?.presenter)
        
        self.foodSearchView?.showVoiceSearchProgressView()
 /*
        skTransaction = skSession!.recognize(withType: recognitionType,
                                             detection: endpointer,
                                             language: language,
                                             options: nil,
                                             delegate: self.foodSearchView?.presenter)
 */
    }*/
    
    
    func showCameraSnapViewController() {
        let dateToShow = self.date ?? Date()
        
        if AVCaptureDevice.authorizationStatus(forMediaType: AVMediaTypeVideo) ==  AVAuthorizationStatus.authorized
        {
            self.showCameraSnapController(date: dateToShow)
        }
        else
        {
            AVCaptureDevice.requestAccess(forMediaType: AVMediaTypeVideo, completionHandler: { (granted :Bool) -> Void in
                if granted == true
                {
                    self.showCameraSnapController(date: dateToShow)
                }
                else
                {
                    self.showCameraErrorAlert()
                }
            });
        }
        
        
    }
    
    private func showCameraErrorAlert() {
        
        let alertMessage = "You can re-enable camera access for Nourish in Settings"
        let alertTitle = "Camera Access Disabled"
        
        let alert = UIAlertController.init(title: alertTitle, message: alertMessage, preferredStyle: UIAlertControllerStyle.alert)
        let settingsAction = UIAlertAction(title: "Settings", style: UIAlertActionStyle.default, handler: { action in
            UIApplication.shared.openURL(URL(string: UIApplicationOpenSettingsURLString)!)
        })
        
        let dismissAction = UIAlertAction(title: "Cancel", style: UIAlertActionStyle.default, handler: { action in
            self.foodSearchView?.updateView()
        })
        alert.addAction(settingsAction)
        alert.addAction(dismissAction)
        self.present(alert, animated: true, completion: nil)
        
    }
    
    
    private func showCameraSnapController(date : Date) {
        let vc = CameraSnapViewController(nibName : "CameraSnapViewController", bundle : nil)
        vc.date = date
        vc.ocasion = self.occasion
        vc.recognitionDelegate = self
        self.present(vc, animated: true, completion: nil)
    }

    func recognitionViewControllerDidAddedFood() {
        self.foodSearchView?.itemAdded = true
        self.dismiss(animated: true, completion: {
            self.foodSearchView?.reloadTableView()
        })
    }
    
    func recognitionViewControllerDidCancel() {
        self.dismiss(animated: true, completion: {
            self.foodSearchView?.reloadTableView()
        })
    }
    
    // MARK : VoiceMealListDelegate
    func soundHoundViewControllerDidAddedFood() {
        self.foodSearchView?.itemAdded = true
        self.dismiss(animated: true, completion: {
            self.foodSearchView?.reloadTableView()
        })
    }
    
    /*func stopRecording() {
        // Stop recording the user.
        skTransaction!.stopRecording()
        self.foodSearchView?.stopVoiceSearchWithText(searchString: "")
    }
    
    func cancel() {
        // Cancel the Reco transaction.
        // This will only cancel if we have not received a response from the server yet.
        skTransaction!.cancel()
        self.foodSearchView?.stopVoiceSearchWithText(searchString: "")
    }*/
    
    func showSoundHoundVoiceSearch() {
        let vc = VoiceSearchListenerViewController(nibName : "VoiceSearchListenerViewController", bundle : nil)
        vc.date = self.date
        DispatchQueue.main.async {
            vc.delegateVC = self
            self.present(vc, animated: false, completion: nil)
        }
    }
    
}

//
//  MicroelementDetailForPeriodPresenter.swift
//  Nourish
//
//  Created by Andrey Zmushko on 8/16/17.
//  Copyright © 2017 SSIC. All rights reserved.
//

import Foundation
import RxSwift


class MicroelementDetailForPeriodPresenter: BasePresenter {
    @IBOutlet weak var contentView: BaseView!
    @IBOutlet weak var contentViewHeight: NSLayoutConstraint!
    
    @IBOutlet weak var sugarsChart: ProgressBarChart!
    @IBOutlet weak var saltChart: ProgressBarChart!
    @IBOutlet weak var saturatedFatChart: ProgressBarChart!
    
    @IBOutlet weak var sugarsDescriptionHolderView: UIView!
    @IBOutlet weak var sugarsDescriptionHeight: NSLayoutConstraint!
    var sugarsDescriptionView: MicroelementDescription?
    
    @IBOutlet weak var saltDescriptionHolderView: UIView!
    @IBOutlet weak var saltDescriptionHeight: NSLayoutConstraint!
    var saltDescriptionView: MicroelementDescription?
    
    @IBOutlet weak var saturatedFatDescriptionHolderView: UIView!
    @IBOutlet weak var saturatedFatsDescriptionHeight: NSLayoutConstraint!
    var saturatedFatsDescriptionView: MicroelementDescription?
    
    @IBOutlet weak var sugarsTopSourcesHolder: UIView!
    var sugarsTopSourcesContent: MicroelementTopSources?
    @IBOutlet weak var sugarsTopSourcesHeight: NSLayoutConstraint!
    
    @IBOutlet weak var saltTopSourceHolder: UIView!
    var saltTopSourcesContent: MicroelementTopSources?
    @IBOutlet weak var saltTopSourcesHeight: NSLayoutConstraint!
    
    @IBOutlet weak var saturatedFatTopSourcesHolder: UIView!
    var saturatedFatTopSourcesContent: MicroelementTopSources?
    @IBOutlet weak var saturatedFatTopSourcesHeight: NSLayoutConstraint!
    
    var config: ProgressConfig?
    
    let sugarsChartDelegate = ProgressBarChartMicroelementsDelegate()
    let saltChartDelegate = ProgressBarChartMicroelementsDelegate()
    let saturatedFatChartDelegate = ProgressBarChartMicroelementsDelegate()
    let interactor = MicroelementBalanceInteractor()
    let objectToModelMapper = MicroelementModel_to_MicroelementViewModel()
    var microelemenModel = MicroelementViewModel()
    var date: (Date, Date)?

    private func getMicroelementDetail() {
        if self.config != nil {
            self.date = self.config?.getDate()
        }
        
        if self.date != nil {
            self.subscribtions.append(self.interactor.execute(startDate: (self.date?.0)!, endDate: (self.date?.1)!).observeOn(MainScheduler.instance).subscribe(onNext: { [unowned self] microelementDetail in
                self.microelemenModel = self.objectToModelMapper.transform(model: microelementDetail)
                self.reloadView()
                self.contentView.stopActivityAnimation()
            }, onError: {error in
                DispatchQueue.main.async {
                    self.contentView.stopActivityAnimation()
                    self.contentView.parseError(error: error, completion: nil)
                }
            }, onCompleted: {
            }, onDisposed: {
                self.contentView.stopActivityAnimation()
            }))
        }
    }

    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        if let config = self.config {
            if config.period == .weekly {
                for chart in [self.sugarsChart, self.saltChart, self.saturatedFatChart] {
                    chart?.widthBarPattern = (0.33, 0.0)
                }
            }
        }

        for chart in [self.sugarsChart, self.saltChart, self.saturatedFatChart] {
            chart?.delimiterMultiplier = 0.2
        }
        
   
        self.sugarsTopSourcesHolder.setupContent(&self.sugarsTopSourcesContent, nibName: "MicroelementTopSources")
        self.saltTopSourceHolder.setupContent(&self.saltTopSourcesContent, nibName: "MicroelementTopSources")
        self.saturatedFatTopSourcesHolder.setupContent(&self.saturatedFatTopSourcesContent, nibName: "MicroelementTopSources")
        
        getMicroelementDetail()
    }
    
    private func reloadView() {
        
        if let config = self.config {
            self.sugarsChartDelegate.setupWith(Model: self.microelemenModel, config: config, type: .sugars)
            self.saltChartDelegate.setupWith(Model: self.microelemenModel, config: config, type: .salt)
            self.saturatedFatChartDelegate.setupWith(Model: self.microelemenModel, config: config, type: .saturatedFat)

            self.sugarsChart.reloadData(DataSource: self.sugarsChartDelegate)
            self.saltChart.reloadData(DataSource: self.saltChartDelegate)
            self.saturatedFatChart.reloadData(DataSource: self.saturatedFatChartDelegate)
        }
        
        
        self.sugarsDescriptionView?.image.imageNamedWithTint(named: "exclamationPoint", tintColor: NRColorUtility.hexStringToUIColor(hex: "F0C517"))
        
        if let detailInformation = microelemenModel.detailInformation {
            
            if detailInformation.count >= 3 {
             
                self.sugarsTopSourcesContent?.label.text = detailInformation[0].title
                self.sugarsTopSourcesContent?.sources = detailInformation[0].topFood
                self.sugarsTopSourcesContent?.tableView.reloadData()
                self.sugarsTopSourcesHeight.constant = (self.sugarsTopSourcesContent?.actualHeight())!
            
                self.saltTopSourcesContent?.label.text = detailInformation[1].title
                self.saltTopSourcesContent?.sources = detailInformation[1].topFood
                self.saltTopSourcesContent?.tableView.reloadData()
                self.saltTopSourcesHeight.constant = (self.saltTopSourcesContent?.actualHeight())!
                
                
                self.saturatedFatTopSourcesContent?.label.text = detailInformation[2].title
                self.saturatedFatTopSourcesContent?.sources = detailInformation[2].topFood
                self.saturatedFatTopSourcesContent?.tableView.reloadData()
                self.saturatedFatTopSourcesHeight.constant = (self.saturatedFatTopSourcesContent?.actualHeight())!
            }
        }
    }
}


//
//  NewFoodCardRepository.swift
//  Nourish
//
//  Created by Nova on 10/3/17.
//  Copyright © 2017 SSIC. All rights reserved.
//

import UIKit
import RxSwift

class NewFoodCardRepository: NSObject {
    let cacheStore = NewFoodCardCacheStore()
    let restStore = NestleRestDataStore()
    let toObjectMapper = json_to_NewFoodCard()
    let userRepo = UserRepository.shared
    
    func fetchFoodCardFor(model : foodSearchModel) -> Observable<NewFoodCard> {
        let obj = NewFoodCard()
        let foodID = model.mealIdString != "" ? model.mealIdString : model.foodId
        
        if foodID != nil && foodID != "" {
            if let cache = self.cacheStore.fetchForIdent(identifier: foodID!) {
                return Observable.just(self.mapData(data: cache))
            } else {
                if let token = NRUserSession.sharedInstance.accessToken {
                    let user = self.userRepo.getCurrentUser(policy: .DefaultPolicy).flatMap { user -> Observable<String> in
                        return Observable.just(user.userID!)
                    }.flatMap { userID -> Observable<NewFoodCard> in
                        let obj = self.restStore.fetchFoodCardFor(ident: [foodID!], token: token, userID: userID).flatMap { data -> Observable<NewFoodCard> in
                            
                            self.cacheStore.storeData(data: data, date: Date(), ident: foodID!)
                            
                            return Observable.just(self.mapData(data: data))
                        }
                        return obj
                    }
                 return user
                } else {
                    return Observable.just(obj)
                }
            }
        } else {
            return Observable.just(obj)
        }
        
    }
    
    func fetchFoodCardFor(id : String) -> Observable<NewFoodCard> {
        var model = foodSearchModel()
        model.mealIdString = id
        model.foodId = id
        return self.fetchFoodCardFor(model: model).map {foodCard -> NewFoodCard in
            foodCard.defaultFoodSearchModel = model
            return foodCard
        }
        
    }
    
    func fetchFoodCardsFor(idents : [String]) -> Observable<[NewFoodCard]> {
        var result = [Observable<NewFoodCard>]()
        
        for obj in idents {
            var model = foodSearchModel()
            model.mealIdString = obj
            result.append(self.fetchFoodCardFor(model: model))
        }
        
        return Observable.from(result).merge().toArray()
    }
    
    func fetchOptifastFoodCard(model : foodSearchModel) -> Observable<NewFoodCard> {
        let obj = NewFoodCard()
        let path = Bundle.main.url(forResource: "optifast", withExtension: "json")
        if path != nil {
            let jsonData = try?  Data(contentsOf: path!)
            if jsonData != nil {
                return Observable.just(self.mapData(data: jsonData!))
            }
        }
        return Observable.just(obj)
    }
    
    
    private func mapData(data : Data) -> NewFoodCard {
        if let obj = self.toObjectMapper.transform(data: data) {
            return obj
        }
        
        return NewFoodCard()
    }
}

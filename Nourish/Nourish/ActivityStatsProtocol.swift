//
//  ActivityStatsProtocol.swift
//  Nourish
//
//  Created by Vino (Vinodha) Sundaramoorthy on 12/23/16.
//  Copyright © 2016 SSIC. All rights reserved.
//

import UIKit

protocol  ActivityStatsProtocol {
    func setActivityModel(model:activityModel,period:period)
    func fetchActivityStats(period : period)
    func setMetricsPreference(isMetric : Bool)
    func startActivity()
}

//
//  FoodRecordCache.swift
//  Nourish
//
//  Created by Nova on 1/20/17.
//  Copyright © 2017 SSIC. All rights reserved.
//

import UIKit
import RealmSwift


class FoodRecordCache: Object {
    dynamic var foodTitle : String?
    dynamic var foodId : String?
    dynamic var measure : String?
    dynamic var calories : String?
    
    dynamic var amount : Double = 1
    dynamic var unit : String?
}

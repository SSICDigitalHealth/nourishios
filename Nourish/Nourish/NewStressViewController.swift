//
//  NewStressViewController.swift
//  Nourish
//
//  Created by Vlad Birukov on 17.08.17.
//  Copyright © 2017 SSIC. All rights reserved.
//

import UIKit

class NewStressViewController: BasePresentationViewController {

    @IBOutlet weak var stressCaptionView: StressCaptionView!
    @IBOutlet weak var stressDetailView: StressDetailView!
    @IBOutlet var presenter: StressDetailPresenter!
    @IBOutlet weak var navigationBar: StressNavigationBarView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.presenter.stressViewController = self
        self.baseViews = [navigationBar, stressCaptionView, stressDetailView]
        for v in self.baseViews! {
            v.viewDidLoad()
        }
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.presenter.viewWillAppear(animated)
        self.setUpNavigationBar()

    }
    
    func setUpNavigationBar() {
        if self.navigationController != nil {
            self.navigationBar.settingNavigation(title: "Stress")
            self.navigationBar.backButton.addTarget(self, action: #selector(showResetView), for: .touchUpInside)
        }
    }
    
    func showResetView(sender:UIButton!) {
        self.presenter.showActivityView()
    }

}

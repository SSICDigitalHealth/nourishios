//
//  ScoreModel.swift
//  Nourish
//
//  Created by Vino (Vinodha) Sundaramoorthy on 4/18/17.
//  Copyright © 2017 SSIC. All rights reserved.
//

import UIKit

struct nutrientInfo {
    var name = ""
    var value = 0.0
    var min : Double?
    var max : Double?
    var unit = ""
    
    var quantityString : String {
        get {
            if self.min != nil && self.max != nil {
                let maxValue = self.max!
                let minValue = self.min!
                
                if self.value < minValue {
                    return kQuantityLow
                } else if self.value > maxValue {
                    return kQuantityHigh
                } else {
                    return kQuantityNormal
                }
                
            } else if self.min == nil {
                let maxValue = self.max!
                
                if self.value == 0 {
                    return kQuantityLow
                } else if value <= maxValue {
                    return kQuantityNormal
                } else {
                    return kQuantityHigh
                }
            } else { //only minimal
                let maxValue = self.min!
                
                if self.value < maxValue {
                    return kQuantityLow
                } else {
                    return kQuantityNormal
                }
            }
        }
    }

    func progressColor() -> UIColor {
        if quantityString == kQuantityNormal {
            return NRColorUtility.justRightColor()
        } else {
            return NRColorUtility.tooMuchColor()
        }
    }
}

struct ScoreModel {
    var score : Double = 0
    var scoreDelta : Double = 0
    var valueToShow : Double = 0
    var topNutrients : [nutrientInfo] = []
    var closeToUpperLimit : [String] = []
    var InDri : [String] = []
    var aboveDri : [String] = []
    var worstDelta : [String] = []
    var bestDelta : [String] = []
}

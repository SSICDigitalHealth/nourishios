//
//  Progress.swift
//  Nourish
//
//  Created by Vlad Birukov on 26.06.17.
//  Copyright © 2017 SSIC. All rights reserved.
//

import Foundation

class Progress: NSObject {
    var userScore = UserActivityScoreModel()
    var userCallories = CaloriesModel()
    var userOcasion = [Element]()
    var userMicroelement = [MicroelementInformation]()
    var nutrientsLisToShow = [[MicroelementInformation]]()
    var calorieHistory : [CalorieHistoryModel]?
    var componentsHistory : [MicroelementInformation]!
    var watchoutForToShow = [[MicroelementInformation]]()
    var wholeGrainsHistory = [MicroelementInformation]()
    var wholeGrains = [MicroelementInformation]()
    var refinedGrainsHistory = [MicroelementInformation]()
}

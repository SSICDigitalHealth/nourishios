//
//  UserActivityViewProtocol.swift
//  Nourish
//
//  Created by Nova on 11/21/16.
//  Copyright © 2016 SSIC. All rights reserved.
//

import UIKit

protocol UserActivityViewProtocol : BaseViewProtocol {
    func setupWith(activitiesArray : [UserActivityModel])
    func setSteps(steps : Double)
}

//
//  MealPlannerInteractor.swift
//  Nourish
//
//  Created by Vlad Birukov on 21.08.17.
//  Copyright © 2017 SSIC. All rights reserved.
//

import Foundation
import RxSwift

class MealPlannerInteractor {
    private let mealPlannerRepository = MealPlannerRepository()
    private let favRepo = FavoritesRepository()
    
    func execute(date: Date) -> Observable <MealPlannerModel?> {
        let mealPlannerFood = self.mealPlannerRepository.getPlanFor(date:date)
        let favoriteMeal = self.favRepo.fetchFavoritesWithCache()
        
        let zip = Observable.zip(favoriteMeal, mealPlannerFood, resultSelector: {favMeal, mealFood -> MealPlannerModel? in
            if mealFood != nil {
                let mealPlannerModel = mealFood
                
                if let meals = mealPlannerModel?.meal {
                    for meal in meals {
                        for detail in meal.detail {
                            if detail.occasion != nil {
                                
                                if favMeal.count > 0 {
                                    for favorite in favMeal {
                                        detail.occasion = self.isFavorite(favorite: favorite, mealArray: detail.occasion!)
                                    }
                                }
                            }
                        }
                        
                    }
                }
                return mealPlannerModel
            }
            
            return nil

        })
        
        return zip
    }

    private func isFavorite(favorite: MealRecordModel, mealArray: [(foodSearchModel, RecipiesModel?)]) -> [(foodSearchModel, RecipiesModel?)]{
        var mealsModel = [(foodSearchModel, RecipiesModel?)]()
        let favoriteTitle = favorite.groupedMealArray.filter({$0.foodTitle != nil}).map({return $0.foodTitle!})
        let mealFoodTitle = mealArray.map({return $0.0.foodTitle})
        
        if (favoriteTitle.count == mealFoodTitle.count) && mealFoodTitle.sorted() == favoriteTitle.sorted() {
            for meal in mealArray {
                var foodsearchModel = foodSearchModel()
                foodsearchModel = meal.0
                foodsearchModel.groupID = favorite.groupID
                foodsearchModel.isFavorite = true
                
                mealsModel.append((foodsearchModel, self.favoriteRecipies(meal: meal, favoriteTitle: favoriteTitle, favorite: favorite)))
            }
            
            return mealsModel
        } else {
            for meal in mealArray {
                
                mealsModel.append((meal.0, self.favoriteRecipies(meal: meal, favoriteTitle: favoriteTitle, favorite: favorite)))
            }
            return mealsModel
        }        
    }
    
    private func favoriteRecipies(meal: (foodSearchModel, RecipiesModel?), favoriteTitle: [String], favorite: MealRecordModel) -> RecipiesModel? {
        
        var recipiesModel: RecipiesModel?
        if meal.1 != nil {
            recipiesModel = RecipiesModel()
            recipiesModel = meal.1
            
            if favoriteTitle.count == 1 && favoriteTitle.sorted() == [recipiesModel!.nameFood] {
                recipiesModel?.isFavorite = true
                recipiesModel?.groupId = favorite.groupID
            }
        }
        
        return recipiesModel
    }
}

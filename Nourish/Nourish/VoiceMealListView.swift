//
//  VoiceMealListView.swift
//  Nourish
//
//  Created by Vino (Vinodha) Sundaramoorthy on 7/1/17.
//  Copyright © 2017 SSIC. All rights reserved.
//

import UIKit

class VoiceMealListView: BaseView , VoiceMealListProtocol {
    
    @IBOutlet weak var ocassionPicker : NRPickerView!
    @IBOutlet weak var mealListTableView : UITableView!
    @IBOutlet weak var addToDiaryButton : UIButton!
    var presenter = VoiceMealListPresenter()
    var controller : VoiceMealListViewController!
    
    override func viewWillAppear(_ animated: Bool) {
        mealListTableView?.register(UINib(nibName: "VoiceMealItemCellTableViewCell", bundle: nil), forCellReuseIdentifier: "kVoiceMealCell")
        self.ocassionPicker.font = UIFont.systemFont(ofSize: 14, weight: UIFontWeightRegular)
        self.ocassionPicker.highlightedFont = UIFont.systemFont(ofSize: 14, weight: UIFontWeightBold)
        self.ocassionPicker.textColor = UIColor.white
        self.ocassionPicker.highlightedTextColor = UIColor.white
        self.ocassionPicker.maskDisabled = true
        self.basePresenter = presenter
        presenter.mealListView = self
        presenter.mealRecordArray = self.controller.mealArray
        presenter.ocassion = self.controller.ocasion
        self.addToDiaryButton.backgroundColor = NRColorUtility.appBackgroundColor()
        super.viewWillAppear(animated)
    }
    

    
    // MARK : VoiceMealListProtocol Implementation
    
    func startActivity() {
        self.prepareLoadView()
    }
    
    func stopActivity() {
        self.stopActivityAnimation()
    }
    
    func setUpInitialView(ocasion:Int) {
        self.reloadTableView()
        self.ocassionPicker.delegate = self.presenter
        self.ocassionPicker.dataSource = self.presenter
        
        self.ocassionPicker.selectItem(ocasion, animated: false, notifySelection: false)
    
        self.stopActivityAnimation()
    }
    
    func reloadTableView() {
        self.mealListTableView.delegate = self.presenter
        self.mealListTableView.dataSource = self.presenter
        self.mealListTableView.reloadData()
    }
    
    func reloadCell(cell :VoiceMealItemCellTableViewCell) {
       let indexPath = self.mealListTableView.indexPath(for: cell)
        if indexPath != nil {
            self.mealListTableView.reloadRows(at: [indexPath!], with: .none)
        }
    }

    func finishSave() {
        self.controller.finishSave()
    }
    
    func dismiss() {
        self.controller.dismiss(animated: false, completion: nil)
    }

}

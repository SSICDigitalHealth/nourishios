//
//  StressPresenter.swift
//  Nourish
//
//  Created by Gena Mironchyk on 2/2/17.
//  Copyright © 2017 SSIC. All rights reserved.
//

import UIKit
import RxSwift

enum viewOnScreen{
    case dailyStressView
    case weeklyStressView
}
class StressPresenter : BasePresenter, UITableViewDataSource, UITableViewDelegate, NRSegmentedControlDelegate {
    let interactor = StressInteractor()
    let mapperObjectDaily = DailyStress_to_DailyStressModel()
    let mapperObjectWeekly = WeeklyStress_to_WeeklyStressModel()
    var dailyChartView : DailyStressChartProtocol?
    var weeklyChartView : WeeklyStressProtocol?
    
    var dailyTableData : [DailyStressModel] = []
    var weeklyTableData : [WeeklyStressModel] = []
    var cachedWeeklyData : [WeeklyStressModel]?
    var lastCachedDate : Date?
    
    var stressController : StressViewControllerProtocol?

    var currentView : viewOnScreen = .dailyStressView
    
    override func viewWillAppear(_ animated : Bool) {
        if currentView == .dailyStressView {
            self.updateDailyStress()
        } else {
            if self.cachedWeeklyData == nil || Date().timeIntervalSince(self.lastCachedDate!) > 60 {
                self.updateWeeklyStress()
            } else {
                self.weeklyTableData = self.cachedWeeklyData!
                self.setWeeklyChartData(model: self.weeklyTableData.first!)
                self.weeklyChartView?.updateTableData()
                
            }
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch self.currentView {
        case .dailyStressView:
            let cell : DailyStressCell = tableView.dequeueReusableCell(withIdentifier: "DailyStressCell") as! DailyStressCell
            cell.setupWithModel(model: dailyTableData[indexPath.row])
            cell.selectionStyle = .none
            return cell
            
        case .weeklyStressView :
            let cell : WeeklyStressCell = tableView.dequeueReusableCell(withIdentifier: "WeeklyStressCell") as! WeeklyStressCell
            cell.setupWithModel(model: weeklyTableData[indexPath.row])
            return cell
            
        }
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if self.currentView == .weeklyStressView {
            self.setWeeklyChartData(model: self.weeklyTableData[indexPath.row])
            tableView.deselectRow(at: indexPath, animated: true)
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch self.currentView {
        case .dailyStressView:
            return dailyTableData.count
        case .weeklyStressView:
            return weeklyTableData.count
        }
    }
    
    
    func updateWeeklyStress () {
        self.subscribtions.append(self.interactor.weeklyStressData(period: .weekly).observeOn(MainScheduler.instance).subscribe(onNext: { [unowned self] weeklyArray in
            let weeklyStressArray : [WeeklyStressModel] = self.mapperObjectWeekly.transform(array: weeklyArray)
            if weeklyStressArray.count != 0{
                self.cachedWeeklyData = weeklyStressArray
                self.lastCachedDate = Date()
                self.weeklyTableData = weeklyStressArray
                self.setWeeklyChartData(model: weeklyStressArray.first!)
            }
        }, onError: {error in}, onCompleted: {
            self.weeklyChartView?.updateTableData()
        }, onDisposed: {}))
        
    }
   
    
    func setWeeklyChartData(model : WeeklyStressModel) {
        let array = model.weeklyArray
        var titles = [""]
        var dataSets : [ScatterChartDataSet] = []
        if (array?.count != 0) {
            for i in 0...(array?.count)! - 1{
                let weekly : weeklyStruct = array![i]
                titles.append(self.formatDate(date: weekly.dateToShow))
                let hours : [hoursStruct] = weekly.hoursArray!
                for index in 0...hours.count - 1 {
                    let measure : hoursStruct = hours[index]
                    let entry = ChartDataEntry.init(x: Double(i + 1), y: Double(measure.index))
                    let set = ScatterChartDataSet.init(values: [entry], label: "")
                    set.shapeRenderer = WeeklyStressShapeRenderer()
                    set.scatterShapeSize = 24.0
                    set.setColor(stressLevel.graphColorForStress(stress: measure.stressValue!))
                    dataSets.append(set)
                    }
            }
        }
        let data = ScatterChartData.init(dataSets: dataSets)
        data.setDrawValues(false)
        self.weeklyChartView?.setChartData(data: data, xTitles: titles, maxBpm: model.maxBPM!, minBpm: model.minBPM!, avgBpm: model.avgBPM!)

    }
    
    private func formatDate(date : Date)->String {
        let df = DateFormatter()
        df.dateFormat = "MM/dd"
        return df.string(from: date)
        
    }
    
    func updateDailyStress () {
        let _ = self.interactor.dailyStressData(period: .today).observeOn(MainScheduler.instance).subscribe(onNext: { [unowned self] dailyArray in
            let arrayWithData : [DailyStressModel] = self.mapperObjectDaily.transform(array: dailyArray)
                self.dailyChartView?.setChartData(data: arrayWithData)
                self.dailyTableData = arrayWithData.sorted(by:{ $0.dateToShow!.compare($1.dateToShow!) == ComparisonResult.orderedDescending })
        }, onError: {error in}, onCompleted: {}, onDisposed: {})

    }
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        let cell = self.tableView(tableView, cellForRowAt: indexPath)
        return cell.frame.size.height
    }
    
    func segmentedControlDidPressedItemAtIndex(segmentedControl: NRSegmentedControl, index: Int) {
        if (index == 1){
            self.currentView = .weeklyStressView
            self.stressController?.showWeeklyChart()
        } else {
            self.currentView = .dailyStressView
            self.stressController?.showDailyChart()
            self.cachedWeeklyData = nil
        }
    }
    
}

//
//  MealPlannerRepository.swift
//  Nourish
//
//  Created by Gena Mironchyk on 9/13/17.
//  Copyright © 2017 SSIC. All rights reserved.
//

import UIKit
import RxSwift
class MealPlannerRepository: NSObject {
    private let cacheDataStore = MealPlannerCacheDataStore()
    private let planDataStore = MealPlannerDataStore()
    private let toCacheMapper = json_to_MealPlannerCache()
    private let toModelMapper = MealPlannerCache_to_MealPlannerModel()
    private let cacheToOptifastMealPlanMapper = MealPlannerCache_to_OptifastMealPlannerModel()
    private let userRepository = UserRepository.shared
    
    func getPlanFor(date : Date) -> Observable<MealPlannerModel?> {
       return Observable<MealPlannerModel?>.create{ observer in
            let cachedPlan = self.cacheDataStore.getMealPlanCacheFor(date: date)
            if cachedPlan != nil {
                observer.onNext(self.toModelMapper.transform(cache: cachedPlan!))
            } else {
                observer.onNext(nil)
            }
            observer.onCompleted()
            return Disposables.create()
        }
        
        
    }
    
    func getOptifastPlanFor(date : Date) -> Observable<OptifastMealPlannerModel?> {
        return Observable<OptifastMealPlannerModel?>.create{ observer in
            let cachedPlan = self.cacheDataStore.getMealPlanCacheFor(date: date)
            if cachedPlan != nil {
                observer.onNext(self.cacheToOptifastMealPlanMapper.transform(cache: cachedPlan!))
            } else {
                observer.onNext(nil)
            }
            observer.onCompleted()
            return Disposables.create()
        }
    }
    
    func getCacheMealPlanFor(date: Date) -> Observable<Bool> {
        return Observable<Bool>.create{ observer in
            let cachedPlan = self.cacheDataStore.getMealPlanCacheFor(date: date)
            if cachedPlan != nil {
                observer.onNext(true)
            } else {
                observer.onNext(false)
            }
            observer.onCompleted()
            return Disposables.create()
        }
    }
    
    func getCacheMealPlanFor(dateWith: Date, dateFor: Date) -> Observable<(Bool, Bool)> {
        return Observable<(Bool, Bool)>.create{ observer in
            var last = false
            var future = false
            
            let cacheLatest = self.cacheDataStore.getMealPlanCacheFor(date: dateWith)
            let cacheFuture = self.cacheDataStore.getMealPlanCacheFor(date: dateFor)
            
            if cacheLatest != nil {
                last = true
            }
            
            if cacheFuture != nil {
                future = true
            }
            
            observer.onNext((last, future))
            observer.onCompleted()
            
            return Disposables.create()
        }
    }
    
    func generateWeeklyPlanFor(date : Date) -> Observable<Bool> {
        return self.userRepository.getCurrentUser(policy: .Cached).flatMap { [weak self] userProfile -> Observable<Bool> in
            let dietaryPrefString = userProfile.dietaryPreference?.stringForMealPlan
            let token = NRUserSession.sharedInstance.accessToken
            let userId = userProfile.userID
            if let instance = self {
                return instance.planDataStore.getWeeklyPlan(token: token!, userId: userId!, date: date, dietaryPreference: dietaryPrefString!).flatMap{ [weak self] planDictionary -> Observable<Bool> in
                    if let instance = self {
                        if planDictionary["days"] != nil {
                            let arrayOfDays = planDictionary["days"] as! [[String : Any]]
                            for dayDict in arrayOfDays {
                                instance.cacheDataStore.store(cache: instance.toCacheMapper.transform(dictionary: dayDict, dictionaryFull: planDictionary))
                            }
                        }
                        return Observable.just(true)
                    } else {
                        return Observable.just(false)
                    }
                }
            } else {
                return Observable.just(true)
            }
            
        }
    
    }
    
    func deleteWeeklyPlan() -> Observable<Bool> {
        return Observable<Bool>.create{observer in
            self.cacheDataStore.removeAllCache()
            observer.onNext(true)
            observer.onCompleted()
            return Disposables.create()
        }
    }
    
    func changePreferenceMealPlan(date: Date, lunch: Int, dinner: Int, breakfast: Int, snacks: Int) -> Observable<Bool> {
        return Observable<Bool>.create{observer in
            self.cacheDataStore.changePreferenceMealPlan(date: date, lunch: lunch, dinner: dinner, breakfast: breakfast, snacks: snacks)
            observer.onNext(true)
            observer.onCompleted()
            return Disposables.create()
        }
    }
    
}

//
//  NRFoodDetailsPresenter.swift
//  Nourish
//
//  Created by Vino (Vinodha) Sundaramoorthy on 11/21/16.
//  Copyright © 2016 SSIC. All rights reserved.
//

import UIKit
import RxSwift
let calorieString = "Calories"
let kCalString = "kcal"

class NRFoodDetailsPresenter: BasePresenter , UITableViewDataSource, UITableViewDelegate,NRPickerViewDelegate,NRPickerViewDataSource {
    
    var foodSearchModel : foodSearchModel!
    var dataLoaded = false
    var occasion : Ocasion?
    var date : Date!
    var servingTypes : [String]?
    var measureArray : [String] = []
    var foodView : NRFoodDetailsProtocol?
    var foodDetailsConfiguration : [foodDetailConfiguration] = foodDetailConfiguration.allValues
    var foodNutrientsArray : [Nutrient] = []
    var interactor = NRFoodDetailsInteractor()
    var mealDiary = MealDiaryInteractor()
    var favInteractor = FavouritesInteractor()
    var objectToModelMapper = MealRecord_to_MealRecordModel()
    var isFavourite : Bool = false
    let imageRepo = ImageRepository()
    
    var foodDetailsViewControler : FoodDetailsViewControllerProtocol?
    let newFoodCardRepo = NewFoodCardRepository()
    
    var foodCard : NewFoodCard?

    override func viewWillAppear(_ animated: Bool) {
        if let view = self.foodView {
            view.setupFoodDetailsView()
            if self.foodSearchModel.userPhotoId != nil {
                let _ = self.imageRepo.getPhoto(userPhotoID: self.foodSearchModel.userPhotoId!).observeOn(MainScheduler.instance).subscribe(onNext: { image in
                    view.setFoodImage(image: image)
                })
            } else {
                view.hideFoodImageView()
            }
        }
    }
    
    
    
   // MARK : Fetch Data
    func fetchAllData() -> Observable<Bool> {
        if date == nil {
            date = Date()
        }
        //check
        if foodSearchModel.isFromNoom == true && foodSearchModel.foodId.rangeOfCharacter(from: .whitespaces) == nil && foodSearchModel.foodId.count > 0 {
            return self.fetchNewFoodCard()
        } else {
            return self.fetchNewFoodCard()
        }
    }
   
   /* func fetchNoomFood() -> Observable<Bool> {
        return self.interactor.fetchFoodCardDetails(foodSearchModel : foodSearchModel).flatMap({details -> Observable<Bool> in
            DispatchQueue.main.async {
                self.foodView.setUpFoodDetailsModel(detailsModel: self.foodSearchModel)
                self.isFavourite = false//isFavourite
                let nutrss = self.appendCalories(cal: Double(self.foodSearchModel.calories)!, arr: details.nutrients!)
                NutritionsUtility.formatNutrientName(nutritions: nutrss)
                self.foodNutrientsArray = nutrss
                
                self.servingTypes = details.servingTypes!
                
                if let index = self.servingTypes?.index(of: self.foodSearchModel.unit) {
                    self.foodView.refreshServingsWithIndex(index: index)
                }
                self.dataLoaded = true
            }
            return Observable.just(true)
        })
        
        
//        let nutr = interactor.fetchNutrientDetailsForModel(foodModel: foodSearchModel)
//        let servingType = interactor.servingTypesFrom(model: foodSearchModel)
//        let zipValue = Observable.zip(nutr, servingType, resultSelector : { nutrArray, servingArray -> Bool in
//            var boolValue = false
//            
//            DispatchQueue.main.async {
//                self.foodView.setUpFoodDetailsModel(detailsModel: self.foodSearchModel)
//                self.isFavourite = false//isFavourite
//                let nutrss = self.appendCalories(cal: Double(self.foodSearchModel.calories)!, arr: nutrArray)
//                NutritionsUtility.formatNutrientName(nutritions: nutrss)
//                self.foodNutrientsArray = nutrss
//                
//                self.servingTypes = servingArray
//                
//                if let index = self.servingTypes?.index(of: self.foodSearchModel.unit) {
//                    self.foodView.refreshServingsWithIndex(index: index)
//                }
//                self.dataLoaded = true
//                boolValue = true
//            }
//            
//            
//            return boolValue
//        })
//        return zipValue
    }
    
    func fetchNonNoomFood() -> Observable<Bool> {
        
        return self.mealDiary.fetchFoodCardDetails(foodSearchModel: self.foodSearchModel).flatMap({details -> Observable<Bool> in
            
            var boolValue = false
            DispatchQueue.main.async {
                self.foodView.setUpFoodDetailsModel(detailsModel: self.foodSearchModel)
                self.isFavourite = false//isFavourite
                
                let nutrss = self.appendCalories(cal: Double(self.foodSearchModel.calories)!, arr: details.nutrients!)

                NutritionsUtility.formatNutrientName(nutritions: nutrss)
                self.foodNutrientsArray = nutrss
                
                self.servingTypes = details.servingTypes
                
                if let index = self.servingTypes?.index(of: self.foodSearchModel.unit) {
                    self.foodView.refreshServingsWithIndex(index: index)
                }

                self.dataLoaded = true
                boolValue = true
            }
            
            return  Observable.just(boolValue)
        })
        
//        let nutr = mealDiary.fetchNutrientsFor(model: foodSearchModel)
//        let servingType = Observable.just([foodSearchModel.unit])
//        let zipValue = Observable.zip(nutr, servingType, resultSelector : { nutrArray, servingArray -> Bool in
//                   })
//        return zipValue
    }*/
    
    func fetchNewFoodCard() -> Observable<Bool> {
        let obj = self.newFoodCardRepo.fetchFoodCardFor(model: self.foodSearchModel).flatMap { foodCard -> Observable<Bool> in
            
            var boolValue = false
            DispatchQueue.main.async {
                
                let exists = foodCard.nutrs.first(where: {$0.unit == self.foodSearchModel.unit})
                if exists == nil {
                    let nestleServingType = NestleServings()
                    nestleServingType.ruler = rulerTuple(min: 1, max: 10, step : 1)
                    nestleServingType.unit = self.foodSearchModel.unit
                    foodCard.servings.append(nestleServingType)
                }

                self.foodCard = foodCard

                
                self.foodView?.setUpFoodDetailsModel(detailsModel: self.foodSearchModel)
                
                let nutrss = self.appendCalories(cal: Double(self.foodSearchModel.calories)!, arr: self.convertNewNutrs(newNutrs: foodCard.nutrs, grams: self.foodSearchModel.grams!))
                
                NutritionsUtility.formatNutrientName(nutritions: nutrss)
                self.foodNutrientsArray = nutrss
                
                self.servingTypes = self.unitsFrom(nestleUnits: foodCard.servings)
                
                if let index = self.servingTypes?.index(of: self.foodSearchModel.unit) {
                    self.foodView?.refreshServingsWithIndex(index: index)
                }
                
                if let proxyServ = self.nestleServingFrom(ident: self.foodSearchModel.unit) {
                    let defaultRuler = rulerTuple(min: 1, max: 10, step : 1)
                    self.newPopulateServingAmountDataSourceWith(ruller: proxyServ.ruler ?? defaultRuler)
                } else {
                    self.populateServingAmountDataSource()
                }

                
                self.dataLoaded = true
                
                boolValue = true
            }
            return Observable.just(boolValue)
        }
        return obj
    }
    
    private func convertNewNutrs(newNutrs : [NestleNutrs], grams : Double) -> [Nutrient] {
        var result : [Nutrient] = []
        
        for obj in newNutrs {
            let nutrient = Nutrient()
            nutrient.unit = obj.unit
            nutrient.name = obj.name
            nutrient.representationValue = grams * obj.densityPerGram
            result.append(nutrient)
        }
        
        return result
    }
    
    private func unitsFrom(nestleUnits : [NestleServings]) -> [String] {
        return nestleUnits.map {$0.unit}
    }
    
    // MARK: UITableView Delegate & DataSource
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if dataLoaded == true {
            return self.foodNutrientsArray.count
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell : NRFoodNutrientCell = tableView.dequeueReusableCell(withIdentifier: "kNutrientCell") as! NRFoodNutrientCell
        if self.foodNutrientsArray.count > indexPath.row {
            let data = self.foodNutrientsArray[indexPath.row]
            cell.name.text = data.name
            cell.value.text = String(format:"%.2f %@",data.representationValue,data.unit)

        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if tableView.tag == 1 {
            if foodDetailsConfiguration[indexPath.row] == .recordMeal {
                self.addToDiary(ocasion: self.occasion!, date : date)
            }
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        let cell = self.tableView(tableView, cellForRowAt: indexPath)
        return cell.frame.size.height
    }
    
    // MARK : NRPickerViewDelegate Implementation
    
    func pickerView(_ pickerView: NRPickerView, didSelectItem item: Int) {
        if pickerView.tag == 10 {
            //reload the serving amount picker
        
            let proxyServ = self.nestleServingFrom(ident: self.foodSearchModel.unit)
            
            if let serving = self.nestleServingFrom(ident: self.servingTypes![item]) {
                
                if proxyServ?.unit != serving.unit {
                    self.foodSearchModel.unit = serving.unit
                    self.foodSearchModel.amount = serving.defaultAmount
                    let grams = serving.defaultGrams
                    
                    if let proxyModel = self.foodCard?.defaultFoodSearchModel {
                        let calories = proxyModel.caloriesPerGram * grams
                        self.foodSearchModel.calories = String(format : "%.2f", calories)
                    }
                    
                    self.foodSearchModel.grams = grams
                    
                    let nutrss = self.appendCalories(cal: Double(self.foodSearchModel.calories)!, arr: self.convertNewNutrs(newNutrs: self.foodCard!.nutrs, grams: self.foodSearchModel.grams!))
                    
                    NutritionsUtility.formatNutrientName(nutritions: nutrss)
                    self.foodNutrientsArray = nutrss
                    
                    self.populateServingAmountDataSource()
                }
                
            }
            
            
            
            
            /*
            self.foodSearchModel.unit = self.servingTypes![item]
            let seq = interactor.fetchCaloriesForModel(foodModel: self.foodSearchModel).flatMap {
                calories -> Observable<foodSearchModel> in
                self.foodSearchModel.calories = String(format : "%.2f",calories)
                return Observable.just(self.foodSearchModel)
            }.flatMap { model -> Observable<foodSearchModel> in
                self.foodSearchModel = model
                return self.interactor.fetchGramsForModel(foodmodel: model)
            }
            
            let _ = seq.subscribe(onNext: {model in
                self.foodSearchModel = model
            }, onError: {error in}, onCompleted: {
                DispatchQueue.main.async {
                    self.populateServingAmountDataSource()
                    //self.populateServingAmountDataSource()
                }
            }, onDisposed: {})
          */
            
        } else {
            //reload the nutrient details
            let proxyAmount = foodSearchModel.amount
            //let proxyNutrs = self.foodNutrientsArray
            
            self.foodSearchModel.amount = self.getMeasure(meassure: self.measureArray[item])
            
            if self.foodSearchModel.isFromNoom == true && self.foodSearchModel.foodId.rangeOfCharacter(from: .whitespaces) == nil && self.foodSearchModel.foodId.count > 0 {
                
                /*let flow = interactor.fetchCaloriesForModel(foodModel: self.foodSearchModel).flatMap { calories -> Observable<foodSearchModel> in
                    self.foodSearchModel.calories = String(format : "%.1f",calories)
                    return Observable.just(self.foodSearchModel)
                }.flatMap {model -> Observable<foodSearchModel> in
                    return self.interactor.fetchGramsForModel(foodmodel: model)
                }.flatMap { model -> Observable<[Nutrient]> in
                    self.foodSearchModel = model
                    return self.interactor.fetchNutrientDetailsForModel(foodModel: model)
                }
                
                let _ = flow.observeOn(MainScheduler.instance).subscribe(onNext: {nutrs in
                    DispatchQueue.global().sync {
                        NutritionsUtility.formatNutrientName(nutritions: nutrs)
                        self.foodNutrientsArray = nutrs
                    }
                }, onError: {error in}, onCompleted: {
                        self.foodView.reloadNutrientDetails(servingType: self.foodSearchModel.unit, measure: self.foodSearchModel.amount)
                    }
                }, onDisposed: {})*/
                
                
            } else {
                /*
                if let serving = self.nestleServingFrom(ident: self.foodSearchModel.unit) {
                    let grams = serving.defaultGrams
                    
                    let newGrams = grams / (serving.defaultAmount / proxyAmount)
                    if let proxyModel = self.foodCard?.defaultFoodSearchModel {
                        let calories = proxyModel.caloriesPerGram * newGrams
                        self.foodSearchModel.calories = String(format : "%.2f", calories)
                    }
                    
                    self.foodSearchModel.grams = newGrams
                    
                    let nutrss = self.appendCalories(cal: Double(self.foodSearchModel.calories)!, arr: self.convertNewNutrs(newNutrs: self.foodCard!.nutrs, grams: self.foodSearchModel.grams!))
                    
                    NutritionsUtility.formatNutrientName(nutritions: nutrss)
                    self.foodNutrientsArray = nutrss
                    
                    if let index = self.servingTypes?.index(of: self.foodSearchModel.unit) {
                        self.foodView.refreshServingsWithIndex(index: index)
                    }
                }
                */
                
                
                
                
                let flow = interactor.recountCaloriesGramsFor(foodModel: self.foodSearchModel, oldAmount: proxyAmount).flatMap { model -> Observable<foodSearchModel> in
                    self.foodSearchModel.grams = model.grams
                    self.foodSearchModel.calories = model.calories
                    return Observable.just(self.foodSearchModel)
                }.flatMap { model -> Observable<[Nutrient]> in
                    return self.interactor.recountNutritionDetailsForModel(foodModel: self.foodSearchModel, oldAmount: proxyAmount, nutrArray: self.foodNutrientsArray)
                }
                let _ = flow.subscribe(onNext: { [unowned self] nutrs in
                    DispatchQueue.global().sync {
                        NutritionsUtility.formatNutrientName(nutritions: nutrs)
                        self.foodNutrientsArray = nutrs
                        
                    }
                }, onError: {error in}, onCompleted: {
                    DispatchQueue.main.async {
                        self.foodView?.reloadNutrientDetails(servingType: self.foodSearchModel.unit, measure: self.foodSearchModel.amount)
                    }
                }, onDisposed: {})
                
                
                
            }

        }
    }
    
    func pickerView(_ pickerView: NRPickerView, titleForItem item: Int) -> String {
        if dataLoaded == true {
            if pickerView.tag == 10 {
                return self.servingTypes![item]
            } else {
                if item < self.measureArray.count {
                    return self.measureArray[item]
                } else {
                    return ""
                }
            }
        } else {
                return ""
        }
    }
    
    func numberOfItemsInPickerView(_ pickerView: NRPickerView) -> Int {
        if dataLoaded == true {
            if pickerView.tag == 10 {
                return (self.servingTypes?.count)!
            } else {
                return (self.measureArray.count)
            }
        } else {
            return 0
        }
        
    }
    
    private func nestleServingFrom(ident : String) -> NestleServings? {
        if self.foodCard != nil {
            let result = self.foodCard?.servings.filter {$0.unit == ident}
            return result?.first
        }
        return nil
    }
    
    func changeFoodAmount() {
     //   self.foodView.startActivityIndicator()
       // self.foodView.disableNavigationBar()
        let _ = self.interactor.changeFood(model: self.foodSearchModel).observeOn(MainScheduler.instance).subscribe(onNext: {score in
            LogUtility.logToFile("currentScore \(score)")
        }, onError: {error in}, onCompleted: {
           // self.foodView.stopActivityIndicator()
        }, onDisposed: {})
        self.foodView?.finishSaveToDiary()
    }

    func performDelegateAction() {
        self.foodDetailsViewControler?.performDelegateActionWith(model: foodSearchModel)
    }
    
    func addToDiary(ocasion : Ocasion, date : Date) {
       // self.foodView.startActivityIndicator()
      //  self.foodView.disableNavigationBar()
        self.occasion = ocasion
        let cal = Calendar.current
        let addedDate = cal.isDateInToday(date) == true ? Date() : date
     /*   if foodSearchModel.isFromNoom == true && foodSearchModel.foodId.rangeOfCharacter(from: .whitespaces) == nil && foodSearchModel.foodId.characters.count > 0 {
            let _ = self.interactor.addToDiary(model: self.foodSearchModel, occasion: ocasion, date: addedDate).subscribe(onNext: {score in
                /*
                let cal = Calendar.current
                if cal.isDateInToday(date) == true {
                    MealHistoryRepository.shared.storeCurrentScore(date: date, value: score)
                }
                LogUtility.logToFile("CurrentScore \(score)")
                */
            }, onError: {error in
                self.foodView.parseError(error : error,completion: nil)
                print(error)
            }, onCompleted: {
                EventLogger.logPortionSizeChanged()
                EventLogger.logSaveFoodFor(date: addedDate, ocasion: ocasion, isFavourite: self.foodSearchModel.isFavorite!, isGroup: false)
                self.foodView.stopActivityAnimation()
                self.foodView.finishSaveToDiary()
                self.foodSearchModel.occasion = ocasion
                self.interactor.addToCounted(model: self.foodSearchModel)
            }, onDisposed: {})
        } else {*/
            let _ = self.interactor.addToDiaryFromRecommendation(model: self.foodSearchModel, date: addedDate, occasion : self.occasion).observeOn(MainScheduler.instance).subscribe(onNext: {score in
                print("CurrentScore \(score)")
            }, onError: {error in
                self.foodView?.parseError(error : error, completion: nil)
                print(error)
            }, onCompleted: {
                EventLogger.logPortionSizeChanged()
                EventLogger.logSaveFoodFor(date: addedDate, ocasion: ocasion, isFavourite: self.foodSearchModel.isFavorite!, isGroup: false)
                self.foodSearchModel.occasion = ocasion
                self.interactor.addToCounted(model: self.foodSearchModel)
            }, onDisposed: {})
        //}
        self.foodView?.finishSaveToDiary()
    }
    
    
    func newPopulateServingAmountDataSourceWith(ruller : rulerTuple) {
        self.dataLoaded = false
        self.measureDataSource(start: ruller.min, end: ruller.max, step: ruller.step)
    }
    
    // MARK :Helper methods
    func populateServingAmountDataSource() {
        self.dataLoaded = false
        let servingType = foodSearchModel.unit.lowercased()
        if (servingType.contains("bowl")) || (servingType.contains("cup")) || (servingType.contains("tsp")) || (servingType.contains("pie")) || (servingType.contains("slice")) || (servingType.contains("tbsp")) || (servingType.contains("mug")) || (servingType.contains("pizza")) || (servingType.contains("serving")) || (servingType.contains("apple")) {
             self.measureDataSource(start: 0.25, end: 10, step: 0.25)
        } else if servingType == "g" {
            self.measureDataSource(start: 10, end: 2000, step: 10)
        } else if servingType == "oz" || servingType == "fl oz" {
            self.measureDataSource(start:1, end: 50, step: 1)
        } else if servingType == "ml" {
            self.measureDataSource(start: 5, end: 5000, step: 5)
        } else if servingType == "calories" {
            self.measureDataSource(start: 10, end: 4000, step: 10)
        } else if servingType == "kj" {
            self.measureDataSource(start: 10, end: 1000, step: 10)
        } else {
            self.measureDataSource(start: 1, end: 10, step: 1)
        }
    }
    

    func measureDataSource(start:Double , end:Double , step:Double) {
        self.measureArray = []
        for index in stride(from: start, through: end, by: step) {
            self.measureArray.append(self.getMeasure(number: index))
        }
        self.dataLoaded = true
        /*
            if let index = self.measureArray.index(of: self.getMeasure(number: self.foodSearchModel.amount)) {
                self.foodView.refreshMeasureWithServingType(index: index)
            } else {
                self.foodView.refreshMeasureWithServingType(index: 0)
            }
        */
        let index =  nearestIndexInitial(measureArray : self.measureArray, amount : self.foodSearchModel.amount)
        self.foodView?.refreshMeasureWithServingType(index: index)
    }
    
    private func nearestIndexInitial(measureArray : [String], amount : Double) -> Int {
        var indexToReturn = 0
        
        if let index = self.measureArray.index(of: self.getMeasure(number: amount)) {
            indexToReturn = index
        } else {
            indexToReturn = self.nearestInd(measureArray: self.measureArray, amount: amount)
        }
        return indexToReturn
    }
    
    private func nearestInd(measureArray : [String], amount : Double) -> Int {
        var indexToReturn = 0
        
        var doubleArray = measureArray.map { self.getMeasure(meassure: $0)}
        
        if doubleArray.count > 0 {
            var closest: Double = doubleArray[0]
            
            
            for index in 0...doubleArray.count - 1 {
                let item = doubleArray[index]
                if abs(amount - item) < abs(amount - closest) {
                    closest = item
                    indexToReturn = index
                }
            }
        }
        return indexToReturn
        
    }
    
    func getMeasure(meassure : String) -> Double {
        let str = String(meassure).components(separatedBy: " ")
        var servingAmount : Double = 0
        
        if str.count > 1 {
            let amount = str[0] == "" ? 0 : Double(str[0])
            if str[1] == "1/2" {
                servingAmount = amount! + 0.50
            }else if str[1] == "1/4" {
                servingAmount = amount! + 0.25
            }else if str[1] == "3/4" {
                servingAmount = amount! + 0.75
            } else {
                servingAmount = Double(str[0])!
            }
        } else {
            servingAmount = Double(meassure)!
        }
        return servingAmount
    }
    
    func getMeasure(number : Double) -> String {
        let str = String(number).components(separatedBy: ".")
        var newString = ""
        
        if str.count > 1 {
            if str[1] == "25" {
                newString = String(format: "%@ 1/4", str[0] == "0" ? "" : str[0])
            } else if str[1] == "5" {
                newString = String(format: "%@ 1/2", str[0] == "0" ? "" : str[0])
            } else if str[1] == "75" {
                newString = String(format: "%@ 3/4", str[0] == "0" ? "" : str[0])
            } else {
                newString = String(format: "%@", str[0] == "0" ? "" : str[0])
            }
        }
        return newString
    }
    
    func deleteFromFavorite() -> Observable<Bool> {
        if self.foodSearchModel.groupID != nil {
            return self.favInteractor.deleteFavouriteGroup(groupID: self.foodSearchModel.groupID!)
        } else {
            return Observable.just(false)
        }
        
    }
    func addToFavourites() -> Observable<Bool> {
        return self.favInteractor.addToFav(model: self.foodSearchModel)
    }
    
    
    func appendCalories(cal : Double, arr : [Nutrient]) -> [Nutrient] {
        let nutr = Nutrient()
        nutr.name = calorieString
        nutr.unit = kCalString
        nutr.representationValue = cal
        nutr.clearValue = cal
        var arrayTotal = arr
        arrayTotal.insert(nutr, at: 0)
        return arrayTotal
    }

}

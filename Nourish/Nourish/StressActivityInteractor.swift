//
//  StressActivityInteractor.swift
//  Nourish
//
//  Created by Vlad Birukov on 28.08.17.
//  Copyright © 2017 SSIC. All rights reserved.
//

import Foundation
import RxSwift

class StressActivityInteractor {
    let repo = NewActivityRepository.shared
    /*
    func execute(startDate: Date, endDate: Date) -> Observable <StressModel> {
        return Observable<StressModel>.create{(observer) -> Disposable in
            let pause = Int(arc4random_uniform(4))
            let dispat = DispatchTime.now() + .seconds(pause)
            DispatchQueue.main.asyncAfter(deadline: dispat, execute: {
                let stress = StressModel()
                if Calendar.current.isDateInToday(startDate) && Calendar.current.isDateInToday(endDate) {
                    stress.currentBPM = Double(self.generateRandomBetween(max: 150, min: 90))

                } else {
                    stress.minBPM = Double(self.generateRandomBetween(max: 70, min: 50))
                    stress.maxBPM = Double(self.generateRandomBetween(max: 130, min: 100))
                    stress.avgBPM = Double(self.generateRandomBetween(max: Int(stress.maxBPM!), min: Int(stress.minBPM!)))
                }
                
                observer.onNext(stress)
                observer.onCompleted()
            })
            
            return Disposables.create()
        }
    }
    */
    
    func execute(startDate: Date, endDate: Date) -> Observable <StressModel> {
        return self.repo.stressModel(startDate:startDate, endDate:endDate)
    }
    
    private func generateRandomBetween(max : Int, min : Int) -> Int {
        return min + Int(arc4random_uniform(UInt32(max - min + 1)))
    }

}

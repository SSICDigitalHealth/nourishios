//
//  DiaryViewTabled.swift
//  Nourish
//
//  Created by Nova on 11/10/16.
//  Copyright © 2016 Samsung. All rights reserved.
//

import UIKit

class MealDiaryView: BaseView, MealDiarySenderProtocol, MealDiaryViewProtocol {
    
    internal func changeFoodAmountWith(model: foodSearchModel) {
        self.viewController?.changeFoodAmountWith(model: model)
    }

    var viewController : MealDiarySenderProtocol?
    @IBOutlet weak var tableView = UITableView()
    var presenter = MealDiaryPresenter()
    
    func showDataForDate(date : Date) {
       let _ = self.presenter.reloadFoodData(date: date, onlyCache: false).subscribe(onNext: { [unowned self] ready in
            if ready == true {
                DispatchQueue.main.async(){
                    self.tableView?.reloadData()
                    self.stopActivityAnimation()
                }
            }
        }, onError: {error in }, onCompleted: {}, onDisposed: {})
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.setupView()
        self.basePresenter = presenter
        presenter.tabledView = self
        
        tableView?.register(UINib(nibName: "NoOcasionTableViewCell", bundle: nil), forCellReuseIdentifier: "AddCell")
        tableView?.register(UINib(nibName: "AddFoodTableViewCell", bundle: nil), forCellReuseIdentifier: "OcasionCell")
        tableView?.register(UINib(nibName: "MealPreviewTableViewCell", bundle: nil), forCellReuseIdentifier: "FoodCell")        
        super.viewWillAppear(animated)
    }
    
    func touchesBegan(touches: Set<NSObject>, withEvent event: UIEvent) {
        self.presenter.isUserInteracted = true
        super.touchesBegan(touches as! Set<UITouch>, with: event)
    }
    
    func setupView() {
        let view = UINib(nibName: "MealDiaryView", bundle: nil).instantiate(withOwner: self, options: nil)[0] as! UIView
        view.translatesAutoresizingMaskIntoConstraints = false
        self.translatesAutoresizingMaskIntoConstraints = false
        self.addSubview(view)
        NSLayoutConstraint(item: view, attribute: .leading, relatedBy: .equal, toItem: self, attribute: .leading, multiplier: 1, constant: 0).isActive = true
        NSLayoutConstraint(item: view, attribute: .trailing, relatedBy: .equal, toItem: self, attribute: .trailing, multiplier: 1, constant: 0).isActive = true
        NSLayoutConstraint(item: view, attribute: .top, relatedBy: .equal, toItem: self, attribute: .top, multiplier: 1, constant: 0).isActive = true
        NSLayoutConstraint(item: view, attribute: .bottom, relatedBy: .equal, toItem: self, attribute: .bottom, multiplier: 1, constant: 0).isActive = true
    }
    
    func showAddFoodControllerForOcasion(ocasion: Ocasion, date: Date,showPredictions:Bool) {
        self.viewController?.showAddFoodControllerForOcasion(ocasion: ocasion, date: date,showPredictions:showPredictions)
    }
    
    func setupViewForDate(date : Date) {
        self.showDataForDate(date: date as Date)
    }
    
    func reloadMealViews() {
        self.tableView?.delegate = self.presenter
        self.tableView?.dataSource = self.presenter
        UIView.transition(with: self.tableView!,
                          duration: 0.15,
                          options: .transitionCrossDissolve,
                          animations: { self.tableView?.reloadData() })
        self.stopActivityAnimation()
    }
    
    func startActivityIndicator() {
        self.prepareLoadView()
    }
    
    func stopActivityIndicator() {
        self.stopActivityAnimation()
    }
    
    func reloadAddTableCell(section:Int) {
        DispatchQueue.main.async {
            let indexPath : IndexPath = IndexPath(row: 0, section: section)
            self.tableView?.reloadRows(at: [indexPath], with: .none)
        }
    }
    
    func reloadSectionWithIndex(section:Int) {
        DispatchQueue.main.async {
            self.tableView?.reloadSections(IndexSet(integer:section), with: .none)
        }
    }
    
    func showGroupFavouriteVC(mealGroup: [MealRecordModel] ) {
        self.viewController?.showGroupFavouriteVC(mealGroup: mealGroup)
    }

    func showGroupDetailsControllerForOcasion(ocasion: Ocasion, date: Date, model: MealRecordModel) {
        self.viewController?.showGroupDetailsControllerForOcasion(ocasion: ocasion, date: date, model: model)
    }
    
    func getTableViewIndexPath(cell : UITableViewCell) -> IndexPath {
        
        if let index = self.tableView?.indexPath(for: cell) {
            return index
        }
        return IndexPath()
    }
    
    func disableNavigationBar() {
        self.viewController?.disableNavigationBar()
    }
    
    func enableNavigationBar() {
        self.viewController?.enableNavigationBar()
    }
    
    func enableNavBar () {
        self.enableNavigationBar()
    }
    
    func disableNavBar () {
        self.disableNavigationBar()
    }
}

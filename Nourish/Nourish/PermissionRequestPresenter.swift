//
//  PermissionRequestPresenter.swift
//  Nourish
//
//  Created by Gena Mironchyk on 6/14/17.
//  Copyright © 2017 SSIC. All rights reserved.
//

import UIKit
class PermissionRequestPresenter: BasePresenter {
    
    var controller : PermissionRequestViewControllerProtocol!
    override func viewWillAppear(_ animated: Bool) {
        NotificationCenter.default.addObserver(self, selector: #selector(finish), name: NSNotification.Name("Notifications permissions was asked"), object: nil)
          }
    
    override func viewWillDisappear(_ animated: Bool) {
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name("Notifications permissions was asked"), object: nil);

    }
    
    @IBAction func gotItButtonTapped(){
        PermissionsUtility.shared.askPermissionsForNotifications()
    }
    
    func finish () {
        let dispatchTime = DispatchTime.now() + 0.1
        DispatchQueue.main.asyncAfter(deadline: dispatchTime) {
            self.controller.finishPermissionsQuestions()
        }
    }
}

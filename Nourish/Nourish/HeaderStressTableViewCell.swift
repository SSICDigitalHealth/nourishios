//
//  HeaderStressTableViewCell.swift
//  Nourish
//
//  Created by Vlad Birukov on 17.08.17.
//  Copyright © 2017 SSIC. All rights reserved.
//

import UIKit

class HeaderStressTableViewCell: UITableViewCell {
    @IBOutlet weak var bpmValue: UILabel!
    @IBOutlet weak var bpm: UILabel!
    @IBOutlet weak var imageStress: UIImageView!
    @IBOutlet weak var firstBackgroundView: UIView!
    @IBOutlet weak var secondBackgroundView: UIView!
    @IBOutlet weak var bottomLine: UIView!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func setupWith(model: DailyStressModel) {
        let currentColor = stressLevel.textColorForStress(stress: model.stressValue!)
        self.firstBackgroundView.layer.cornerRadius = self.firstBackgroundView.frame.size.height/2
        self.secondBackgroundView.layer.cornerRadius = self.secondBackgroundView.frame.size.height/2
        self.secondBackgroundView.backgroundColor = currentColor
        self.firstBackgroundView.backgroundColor = currentColor.withAlphaComponent(0.6)
        self.imageStress.imageNamedWithTint(named: "heartrate", tintColor: UIColor.white)
        
        if currentColor != NRColorUtility.undeterminedTableViewStressCellFontColor() {
            self.bpmValue.font = UIFont(name: "Campton-Bold", size: 20)
        } else {
            self.bpmValue.font = UIFont(name: "Campton-Bold", size: 20)
        }
        
        self.bpmValue.textColor = currentColor
        self.bpm.textColor = currentColor
        self.bpmValue.text = String(format: "%.0f", model.bpmValue!)
    }
    
}

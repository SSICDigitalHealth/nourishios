//
//  ConsumedInteractor.swift
//  Nourish
//
//  Created by Vlad Birukov on 31.08.17.
//  Copyright © 2017 SSIC. All rights reserved.
//

import Foundation
import RxSwift

class ConsumedInteractor {
    let newActivityRepo = NewActivityRepository.shared
    
    func execute(startDate: Date, endDate: Date) -> Observable <ConsumedModel> {
        return self.newActivityRepo.calorieStatsFrom(startDate:startDate, endDate:endDate)
    }
    /*
    func execute(startDate: Date, endDate: Date) -> Observable <ConsumedModel> {
        return Observable<ConsumedModel>.create{(observer) -> Disposable in
            let pause = Int(arc4random_uniform(4))
            let dispat = DispatchTime.now() + .seconds(pause)
            DispatchQueue.main.asyncAfter(deadline: dispat, execute: {
                let consumedModel = ConsumedModel()
                consumedModel.consumedCalories = Double(self.generateRandomBetween(max: 55000, min: 20000))
                consumedModel.avgConsumed = Double(self.generateRandomBetween(max: 25000, min: 20000))
                observer.onNext(consumedModel)
                observer.onCompleted()
            })
            return Disposables.create()
        }
    }
    */
    private func generateRandomBetween(max : Int, min : Int) -> Int {
        return min + Int(arc4random_uniform(UInt32(max - min + 1)))
    }
}

//
//  HydrationDetailTableViewCell.swift
//  Nourish
//
//  Created by Vlad Birukov on 08.09.17.
//  Copyright © 2017 SSIC. All rights reserved.
//

import UIKit

class HydrationDetailTableViewCell: UITableViewCell {
    @IBOutlet weak var name: UILabel!
    @IBOutlet weak var currentState: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func setupWithModel(model: HydrationBalanceViewModel, indexPath: IndexPath, isAmount: Bool) {
        if let hydrationElement = model.arrHydrationElement {
            self.name.text = String(format: "%@", hydrationElement[indexPath.row].nameElement)
            self.name.setLineSpacing(spacing: 4)
            
            if isAmount == true {
                currentState.text =  String(format: "%@ %@", NRFormatterUtility.doubleToString(num: hydrationElement[indexPath.row].consumedState!),  model.unit)
            }
        }
    }
    
    func setupWithModel(model: HydrationBalanceViewModel) {
        self.name.textColor = NRColorUtility.progressTableBlackColor()
        self.name.font = UIFont(name: "Campton-Bold", size: 15)!
        self.name.text = "Total"
        self.currentState.textColor = NRColorUtility.progressTableBlackColor()
        self.currentState.font = UIFont(name: "Campton-Bold", size: 15)!
        
        if model.arrHydrationElement != nil {
            self.currentState.text = String(format: "%@ %@", NRFormatterUtility.doubleToString(num: model.total), model.unit)
            
        }
    }
    
}

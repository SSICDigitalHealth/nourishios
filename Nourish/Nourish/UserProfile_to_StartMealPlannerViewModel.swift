
//
//  UserProfile_to_StartMealPlannerViewModel.swift
//  Nourish
//
//  Created by Vlad Birukov on 18.08.17.
//  Copyright © 2017 SSIC. All rights reserved.
//

import Foundation


class UserProfile_to_StartMealPlannerViewModel {
    func transform(model: UserProfile) -> StartMealPlannerViewModel {
        let startMealPlannerModel = StartMealPlannerViewModel()
        var optifastString = ""


        startMealPlannerModel.diet = model.dietaryPreference?.descriptionUI
        startMealPlannerModel.caloriesDay =  model.calculateAndStoreCaloriePlan()
        
        if model.optifastOcasionsList.count > 0 {
            optifastString = "For "
            for occasion in  model.optifastOcasionsList {
                optifastString += String(format: "%@, ", Ocasion.stringRepresentation(servingType: occasion).lowercased())
            }
            
            optifastString = String(optifastString.dropLast(2))
        }
        
        if model.optifastFoodIds.count > 0 {
            optifastString += "\n"
            for foodTitle in model.optifastFoodIds {
                optifastString += String(format: "%@, ", foodTitle)
            }
            
            optifastString = String(optifastString.dropLast(2))
        }
        
        if optifastString != "" {
            startMealPlannerModel.optifast = optifastString
        }
        
        return startMealPlannerModel
    }
}

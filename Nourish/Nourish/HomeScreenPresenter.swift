//
//  HomeScreenPresenter.swift
//  Nourish
//
//  Created by Andrey Zmushko on 9/14/17.
//  Copyright © 2017 SSIC. All rights reserved.
//

import UIKit
import AVFoundation
import RxSwift


class HomeScreenPresenter: BasePresenter {
    
    var controller: HomeScreenViewController?
    let mapper = HomeScreenModel_to_HomeScreenViewModel()
    let interactor = HomeScreenInteractor()
    var model: HomeScreenViewModel?
    
    func actionFor(controller: HomeScreenViewController, action: HomeScreenViewController.Actions) {
        self.controller = controller
        switch action {
        case .camera:
            self.cameraAction()
        case .foodDiary:
            self.foodDiaryAction()
        case .lifestyle:
            NavigationUtility.showActivityController()
        case .microphone:
            self.showVoiceSearch()
        case .nutrition:
            NavigationUtility.showNutritionController()
        case .profile:
            self.controller?.onSlideMenuButtonPressed(UIButton())
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.getModel()
    }

    private func getModel() {
        self.subscribtions.append(self.interactor.execute().observeOn(MainScheduler.instance).subscribe(onNext: { [weak self] dataModel in
            if let instance = self {
                instance.model = instance.mapper.transform(dataModel: dataModel)
                if let controller = instance.controller {
                    controller.setupWith(model: instance.model)
                }
            }
        }, onError: { [weak self] error in
            if let instance = self {
                instance.controller?.parseError(error: error, completion: nil)
            }
        }, onCompleted: {
            let cacheRepo = ProxyCacherRepo.shared
            cacheRepo.storeLastCache()
        }, onDisposed: {
        }))
    }

    func setupWith(controller: HomeScreenViewController) {
        self.controller = controller
        self.getModel()
    }
    
    private func foodDiaryAction() {
        EventLogger.logClickedOnFoodDiaryIcon()
        NavigationUtility.navigateToFoodDiaryFor(date: Date())
    }
    
    private func cameraAction() {
        EventLogger.logOnCameraClick()
        self.showPhotoSearch()
    }

    func showVoiceSearch() {
        EventLogger.logClickedOnVoiceIcon()
        let mediaType = AVMediaTypeAudio
        let authStatus = AVCaptureDevice.authorizationStatus(forMediaType: mediaType)
        switch authStatus {
        case .authorized:
            self.showSoundHoundViewController()
            break
        case .denied:
            self.showMicrophoneErrorAlert()
            break
            
        case .restricted:
            self.showSoundHoundViewController()
            break
            
        case .notDetermined:
            AVCaptureDevice.requestAccess(forMediaType : mediaType, completionHandler: { granted in
                if granted{
                    self.showSoundHoundViewController()
                } else {
                    self.showMicrophoneErrorAlert()
                }
            })
            
            break
        }
    }
    
    private func showMicrophoneErrorAlert() {
        let alertMessage = "You can re-enable microphone access for Nourish in Settings"
        let alertTitle = "Microphone Access Disabled"
        
        let alert = UIAlertController.init(title: alertTitle, message: alertMessage, preferredStyle: UIAlertControllerStyle.alert)
        let settingsAction = UIAlertAction(title: "Settings", style: UIAlertActionStyle.default, handler: { action in
            UIApplication.shared.openURL(URL(string: UIApplicationOpenSettingsURLString)!)
        })
        
        let dismissAction = UIAlertAction(title: "Cancel", style: UIAlertActionStyle.default, handler: { action in
            //self.dismissMultiModalView()
        })
        alert.addAction(dismissAction)
        alert.addAction(settingsAction)
        if let controller = self.controller {
            controller.present(alert, animated: true, completion: nil)
        }
    }
    
    func showSoundHoundViewController() {
        let vc = VoiceSearchListenerViewController(nibName : "VoiceSearchListenerViewController", bundle : nil)
        vc.date = Date()
        vc.delegateVC = self.controller
        NavigationUtility.presentVC(vc: vc, needDismission: false)
    }
    
    private func showCameraSnapController() {
//        let portionSizeVc = PortionSizeViewController(nibName : "PortionSizeViewController", bundle : nil)
//        let nav = BaseNavigationController(rootViewController: portionSizeVc)
//        NavigationUtility.presentVC(vc: nav, needDismission: false)
        let vc = CameraSnapViewController(nibName : "CameraSnapViewController", bundle : nil)
        vc.date = Date()
        vc.ocasion = self.occasionFromDate(date: Date())
        vc.recognitionDelegate = self.controller
        NavigationUtility.presentVC(vc: vc, needDismission: false)
    }
    
    private func occasionFromDate(date : Date) -> Ocasion {
        let hour = Double(Calendar.current.component(.hour, from: date)) + Double(Calendar.current.component(.minute, from: date))/60.0
        if hour >= 5 && hour <= 10.5 {
            return .breakfast
        } else if hour >= 11 && hour <= 13.5 {
            return .lunch
        } else if hour >= 17 && hour <= 21.5 {
            return .dinner
        } else {
            return .snacks
        }
        
    }

    
    private func showCameraErrorAlert() {
        
        let alertMessage = "You can re-enable camera access for Nourish in Settings"
        let alertTitle = "Camera Access Disabled"
        
        let alert = UIAlertController.init(title: alertTitle, message: alertMessage, preferredStyle: UIAlertControllerStyle.alert)
        let settingsAction = UIAlertAction(title: "Settings", style: UIAlertActionStyle.default, handler: { action in
            UIApplication.shared.openURL(URL(string: UIApplicationOpenSettingsURLString)!)
        })
        
        let dismissAction = UIAlertAction(title: "Cancel", style: UIAlertActionStyle.default, handler: { action in
            //self.dismissMultiModalView()
        })
        alert.addAction(dismissAction)
        alert.addAction(settingsAction)
        if let controller = self.controller {
            controller.present(alert, animated: true, completion: nil)
        }
        
    }
    
    func showPhotoSearch() {
        if AVCaptureDevice.authorizationStatus(forMediaType: AVMediaTypeVideo) ==  AVAuthorizationStatus.authorized
        {
            self.showCameraSnapController()
        }
        else
        {
            AVCaptureDevice.requestAccess(forMediaType: AVMediaTypeVideo, completionHandler: { (granted :Bool) -> Void in
                if granted == true
                {
                    self.showCameraSnapController()
                }
                else
                {
                    self.showCameraErrorAlert()
                }
            });
        }
    }
}

//
//  ActivityBaseCellPresenter.swift
//  Nourish
//
//  Created by Andrey Zmushko on 8/25/17.
//  Copyright © 2017 SSIC. All rights reserved.
//

import UIKit

protocol ActivityBaseCellProtocol {
    func fetchModel(For cell: ActivityBaseCell)
}

protocol ActivityBaseCellDelegate {
    func fetchEpoch() -> (startDate: Date, endDate: Date)
    func actionTap(sender: ActivityBaseCellPresenter)
    func reloadRowAt(indexRow: Int)
    func transferUserId(userId: String)
}

class ActivityBaseCellPresenter: BasePresenter, ActivityBaseCellProtocol {
    
    typealias Epoch = (startDate: Date, endDate: Date)
    var delegate: ActivityBaseCellDelegate?
    var indexRow: Int?

    func reloadTable(cell: ActivityBaseCell) {
        if let delegate = self.delegate {
            if let index = self.indexRow {
                delegate.reloadRowAt(indexRow: index)
            }
        }
    }

    func reload() {
        if let delegate = self.delegate {
            if let index = self.indexRow {
                delegate.reloadRowAt(indexRow: index)
            }
        }
    }

    func transferUserId(userId: String) {
        if let delegate = self.delegate {
            delegate.transferUserId(userId: userId)
        }
    }
    
    func fetchModel(For cell: ActivityBaseCell) {
    }
    
    internal func numberOfDaysIn(Interval from: Date, _ to: Date) -> Int {
        let currentCalendar = Calendar.current
        
        guard let start = currentCalendar.ordinality(of: .day, in: .era, for: from) else { return 0 }
        guard let end = currentCalendar.ordinality(of: .day, in: .era, for: to) else { return 0 }
        
        return end - start
    }
    
}

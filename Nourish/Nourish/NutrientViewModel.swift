//
//  NutrientViewModel.swift
//  Nourish
//
//  Created by Vlad Birukov on 29.08.17.
//  Copyright © 2017 SSIC. All rights reserved.
//

import Foundation


class NutrientViewModel: NSObject {
    var nameNutritional = ""
    var consumedNutritional: Double = 0.0
    var targetNutritional: Double = 0.0
    var unit: String = ""
    var message: String = ""
    var flagImage: Bool = false
}

//
//  NestleFoodComponentsRepository.swift
//  Nourish
//
//  Created by Gena Mironchyk on 9/20/17.
//  Copyright © 2017 SSIC. All rights reserved.
//

import UIKit
import RxSwift
class NestleFoodComponentsRepository: NSObject {
    private let store = NestleFoodComponentsDataStore()
    private let userRepo = UserRepository.shared
    private let cacheStore = NestleFoodComponentsCacheDataStore()
    
    func storeFoodComponents() -> Observable<Bool> {
        return self.userRepo.getCurrentUser(policy: .DefaultPolicy).flatMap{userProfile -> Observable<Bool> in
            let token = NRUserSession.sharedInstance.accessToken
            if token != nil {
                return self.store.getFoodComponents(userId: userProfile.userID!, token: token!).flatMap{data -> Observable<Bool> in
                    self.cacheStore.storeTo(array: data)
                    return Observable.just(true)
                }
            } else {
                return Observable.just(false)
            }
        }
    }
    
    
    func getStoredFoodComponents() ->  Observable<[[String : Any]]> {
        let cache = self.cacheStore.getFoodComponentsCache()
        if cache != nil {
            return Observable.just(cache!)
        } else {
            return self.storeFoodComponents().flatMap{ stored -> Observable<[[String : Any]]> in
                let cache = self.cacheStore.getFoodComponentsCache()
                return Observable.just(cache!)
            }
        }
    }
}

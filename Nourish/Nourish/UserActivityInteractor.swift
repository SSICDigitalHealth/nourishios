//
//  UserActivityInteractor.swift
//  Nourish
//
//  Created by Nova on 11/21/16.
//  Copyright © 2016 SSIC. All rights reserved.
//

import UIKit
import RxSwift

class UserActivityInteractor: NSObject {
    
    let repository = UserActivityRepository()
    
    func userActivity() -> [UserActivity] {
        return self.generateFakeActivityProgress()
    }
    
    func userActivitiesForToday() -> Observable<[UserActivity]> {
        return self.repository.receiveCurrentUserActivities()
    }
    
    func receiveUserStepsForToday() -> Observable<Double> {
        return self.repository.getUserStepsForToday()
    }
    
    private func generateFakeActivityProgress() -> [UserActivity] {
        var generatedDate = NSDate() as Date
        generatedDate = Calendar.current.startOfDay(for: generatedDate as Date)
        var arrayToReturn = [UserActivity]()
        let count = self.generateRandomBetween(max: 20, min: 10)
        
        for _ in 0...count {
            let timeOffset = self.generateRandomBetween(max: 5000, min: 100)
            let duration = self.generateRandomBetween(max: 3000, min: 1000)
            let object = UserActivity()
            object.activityType = userActivityType(rawValue: UInt32(self.generateRandomBetween(max: 1, min: 0)))!
            object.startDate = generatedDate.addingTimeInterval(TimeInterval(timeOffset))
            let endOffset = duration + timeOffset
            
            object.endDate = generatedDate.addingTimeInterval(TimeInterval(endOffset))
            generatedDate = generatedDate.addingTimeInterval(TimeInterval(endOffset))
            arrayToReturn.append(object)
        }
        
        return arrayToReturn
    }
    
    func generateRandomBetween(max : Int, min : Int) -> Int {
        return min + Int(arc4random_uniform(UInt32(max - min + 1)))
    }
    
}

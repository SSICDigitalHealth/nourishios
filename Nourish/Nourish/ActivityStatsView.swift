//
//  ActivityStatsView.swift
//  Nourish
//
//  Created by Vino (Vinodha) Sundaramoorthy on 12/23/16.
//  Copyright © 2016 SSIC. All rights reserved.
//

import UIKit

let kActivityListHeight : CGFloat = 45
let kActivityViewHeightPadding : CGFloat = 15
let kHourUnit = "hr"
let kMinUnit = "min"

class ActivityStatsView: BaseView , ActivityStatsProtocol{
    
    var periodSegmentControl : NRSegmentedControl!
    @IBOutlet weak var activityView : UserActivityView!
    @IBOutlet weak var caloriesChartView : CaloriesBurnedChartView!
    @IBOutlet weak var scrollView : UIScrollView!
    @IBOutlet weak var detailedView : UIView!
    @IBOutlet weak var heightConstraint :NSLayoutConstraint!
    @IBOutlet weak var activityListHeight :NSLayoutConstraint!
    
    var presenter = ActivityStatsPresenter()
    var controller = ActivityStatsViewController()
    var titleLabel : UILabel!
    var activityModel : activityModel!
    var period : period = .today
    var needReload : Bool = false
    var isMetric : Bool = false
    
    // MARK: BaseProtocol Implementation
    override func viewWillAppear(_ animated: Bool) {
        self.setupSegmentControl()
        basePresenter = presenter
        presenter.activitystatsView = self
        super.viewWillAppear(animated)
    }
    
    override func draw(_ rect: CGRect) {
        if needReload {
            self.caloriesChartView.activityModel = activityModel
            self.caloriesChartView.period = period
            self.caloriesChartView.setNeedsDisplay()
            self.scrollView.backgroundColor = NRColorUtility.nourishHomeColor()
            if period == .today {
                self.activityView.viewWillAppear(false)
                self.activityView.isHidden = false
                heightConstraint.constant = 400
            } else {
                self.activityView.isHidden = true
                heightConstraint.constant = 0
            }
            self.scrollView.scrollRectToVisible(CGRect(x: 0, y: 0, width: self.bounds.width, height: 20), animated: false)
            self.drawDetailedActivityView()
        }
    }
    
    func setupSegmentControl() {
        let segmentColor = NRColorUtility.nourishNavigationColor()
        let textFont = UIFont.systemFont(ofSize: 15)
        let textColor = UIColor.white
        
        if periodSegmentControl == nil {
            periodSegmentControl = NRSegmentedControl(frame: CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: 45), titles: ["TODAY","WEEKLY","MONTHLY"],selectedtitles:[], action: {
                control, index in
            })
            periodSegmentControl.appearance = segmentAppearance(backgroundColor: segmentColor, selectedBackgroundColor:segmentColor, textColor: textColor, font: textFont, selectedTextColor: textColor, selectedFont: textFont, bottomLineColor: segmentColor, selectorColor: UIColor.white, bottomLineHeight: 5, selectorHeight: 5, labelTopPadding: 0, hasImage: false, imageSize: nil)
            periodSegmentControl.delegate = presenter
            self.addSubview(periodSegmentControl)
        }
    }
    
    func drawDetailedActivityView() {
        for view in self.detailedView.subviews {
            view.removeFromSuperview()
        }
        
        self.detailedView.backgroundColor = NRColorUtility.nourishHomeColor()
        
        //Steps tile
        let tileWidth = detailedView.frame.width
        let stepsFrame = CGRect(x: 0, y: 0, width:tileWidth, height: 96)
        
        let stepsTile = UIView(frame: stepsFrame)
        stepsTile.backgroundColor = UIColor.white
        
        // tile image
        let stepsImage = UIImageView(frame: CGRect(x: 20, y: 20, width: 40, height: 40))
        stepsImage.image = UIImage(named: "steps_icon")
        stepsImage.center = CGPoint(x: 30, y: stepsTile.bounds.height/2)
        stepsTile.addShadow()
        stepsTile.addSubview(stepsImage)
        
        // tile label
        let stepsLabel = UILabel(frame:CGRect(x: stepsImage.frame.origin.x + stepsImage.frame.size.width + 30, y: 20, width: 200, height: 50))
        let myString = String(format: "%0.f",self.activityModel.steps)
        let myAttribute = [ NSForegroundColorAttributeName: NRColorUtility.progressLabelColor() , NSFontAttributeName : UIFont.systemFont(ofSize: 36, weight: UIFontWeightMedium)]
        let myAttrString = NSMutableAttributedString(string: myString, attributes: myAttribute)
        let labelString = " Steps"
        let labelAttributes = [ NSForegroundColorAttributeName: NRColorUtility.progressLabelColor(), NSFontAttributeName : UIFont.systemFont(ofSize: 14)]
        myAttrString.append(NSAttributedString(string: labelString, attributes: labelAttributes))
        stepsLabel.attributedText = myAttrString
        stepsTile.addSubview(stepsLabel)
        self.detailedView.addSubview(stepsTile)
        
        //Distance tile
        let distFrame = CGRect(x: 0, y: stepsTile.frame.origin.y + stepsTile.frame.height + 15, width:tileWidth, height: 96)
        let distTile = UIView(frame: distFrame)
        distTile.backgroundColor = UIColor.white
        
        // tile image
        let distImage = UIImageView(frame: CGRect(x: 20, y: 20, width: 40, height: 40))
        distImage.image = UIImage(named: "distance_icon")
        distImage.center = CGPoint(x: 30, y: distTile.bounds.height/2)
        distTile.addShadow()
        distTile.addSubview(distImage)
        
        let distStr : String = self.isMetric ?  String(format: "%2.f",self.activityModel.distance) : String(format:"%2.f",NRConversionUtility.kiloToMiles(valueInKilo: self.activityModel.distance))
        
        // tile label
        let distLabel = UILabel(frame:CGRect(x: distImage.frame.origin.x + distImage.frame.size.width + 28, y: 20, width: 200, height: 50))
        distLabel.textAlignment = .left
        let distString = NSMutableAttributedString(string: distStr, attributes: myAttribute)
        let disUnitString = self.isMetric ? " km" :" Miles"
        
        distString.append(NSAttributedString(string: disUnitString, attributes: labelAttributes))
        distLabel.attributedText = distString
        distTile.addSubview(distLabel)
        self.detailedView.addSubview(distTile)
        
        //Active time tile
        let activeFrame = CGRect(x: 0, y: distTile.frame.origin.y + distTile.frame.height + 15, width:tileWidth, height: 96)
        let sleepView = UIView(frame: activeFrame)
        sleepView.addShadow()
        sleepView.backgroundColor = UIColor.white
        
        // tile image
        let sleepImage = UIImageView(frame: CGRect(x: 20, y: 20, width: 40, height: 40))
        sleepImage.image = UIImage(named: "sleep_icon")
        sleepImage.center = CGPoint(x: 30, y: sleepView.bounds.height/2)
        sleepView.addSubview(sleepImage)
        
        // tile label
        let hours = Int(floor(self.activityModel.sleepHours))
        let mins = Int(floor(self.activityModel.sleepHours * 60).truncatingRemainder(dividingBy: 60))
        let sleepString = NSMutableAttributedString(string:"", attributes: myAttribute)
        
        let sleepLabel = UILabel(frame:CGRect(x: sleepImage.frame.origin.x + sleepImage.frame.size.width + 30, y: 20, width: 200, height: 50))
        sleepLabel.textAlignment = .left
        
        if hours > 0 || mins == 0 {
            sleepString.append(NSAttributedString(string: hours > 0 ? String(format: "%d",hours) : "0", attributes: myAttribute))
            let actUnitString = " Hours"
            sleepString.append(NSAttributedString(string: actUnitString, attributes: labelAttributes))
        }
        
        if mins > 0 {
            sleepString.append(NSAttributedString(string: String(format: " %.1d",mins), attributes: myAttribute))
            sleepString.append(NSAttributedString(string: " mins", attributes: labelAttributes))
        }
        
        sleepLabel.attributedText = sleepString
        sleepView.addSubview(sleepLabel)
        self.detailedView.addSubview(sleepView)
        
        //Time tile
        let timeFrame = CGRect(x: 0, y: sleepView.frame.origin.y + sleepView.frame.height + 15, width:tileWidth, height: 200)
        let tileTime = UIView(frame: timeFrame)
        tileTime.backgroundColor = UIColor.white
        
        // tile image
        let timeImage = UIImageView(frame: CGRect(x: 20, y: 20, width: 40, height: 26))
        timeImage.image = UIImage(named: "active_icon")
        tileTime.addSubview(timeImage)
        
        // tile label
        let timeLabel = UILabel(frame:CGRect(x: timeImage.frame.origin.x + timeImage.frame.size.width + 30, y: 10, width: 200, height: 50))
        timeLabel.textAlignment = .left
        let activityString = NSMutableAttributedString(string:"", attributes: myAttribute)
        let activeTimeInHours = self.activityModel.activeHours > 0 ? self.activityModel.activeHours / 60 : self.activityModel.activeHours
        let activityHours = Int(floor(activeTimeInHours))
        let activityMins = Int(floor(activeTimeInHours * 60).truncatingRemainder(dividingBy: 60))
        
        if activityHours > 0 || activityMins == 0 {
            activityString.append(NSAttributedString(string: String(format: "%d",activityHours), attributes: myAttribute))
            activityString.append(NSAttributedString(string: " Hours", attributes: labelAttributes))
        }
        
        if activityMins > 0 {
            activityString.append(NSAttributedString(string: String(format: " %.1d",activityMins), attributes: myAttribute))
            activityString.append(NSAttributedString(string: " mins", attributes: labelAttributes))
        }
        
        timeLabel.attributedText = activityString
        tileTime.addSubview(timeLabel)
        self.detailedView.addSubview(tileTime)
        
        if activityModel.activityList.count > 0 {
            tileTime.frame = CGRect(x: timeFrame.origin.x, y: timeFrame.origin.y, width: timeFrame.size.width, height: 60)
            var yPosition = tileTime.frame.origin.y + tileTime.frame.size.height
            
            for (name,value,type) in activityModel.activityList {
                let activityFrame = CGRect(x: 0, y: yPosition, width: tileWidth, height: 50)
                let activeDetailView = UIView(frame: activityFrame)
                activeDetailView.backgroundColor = UIColor.white
                let label = UILabel(frame: CGRect(x: 20, y: 15, width:200, height: 30))
                label.textColor = NRColorUtility.progressLabelColor()
                label.text = name
                label.font = UIFont.systemFont(ofSize: 20)
                label.textAlignment = .left
                activeDetailView.addSubview(label)
                
                let valueLabel = UILabel(frame: CGRect(x: tileWidth - 175, y: 15, width:150, height: 30))
                valueLabel.textAlignment = .right
                
                // tile label
                let hours = Int(floor(value))
                let mins = Int(floor(value * 60).truncatingRemainder(dividingBy: 60))
                let actString = NSMutableAttributedString(string:"", attributes: myAttribute)
                let typeAttribute = [ NSForegroundColorAttributeName: NRColorUtility.progressLabelColor() , NSFontAttributeName : UIFont.systemFont(ofSize: 20,weight:UIFontWeightMedium)]
                let labelAttributes = [ NSForegroundColorAttributeName: NRColorUtility.progressLabelColor(), NSFontAttributeName : UIFont.systemFont(ofSize: 14)]
                
                if (type != kHourUnit) {
                    actString.append(NSAttributedString(string: hours > 0 ? String(format: "%.0f",value) : "", attributes: typeAttribute))
                    actString.append(NSAttributedString(string: type, attributes: labelAttributes))
                } else {
                    if hours > 0 || mins == 0 {
                        actString.append(NSAttributedString(string: hours > 0 ? String(format: "%d",hours) : "", attributes: typeAttribute))
                        let actUnitString = " Hours"
                        actString.append(NSAttributedString(string: actUnitString, attributes: labelAttributes))
                    }
                    
                    if mins > 0 {
                        actString.append(NSAttributedString(string: String(format: " %.1d",mins), attributes: typeAttribute))
                        actString.append(NSAttributedString(string: " mins", attributes: labelAttributes))
                    }
                }
                valueLabel.attributedText = actString
                activeDetailView.addSubview(valueLabel)
                self.detailedView.addSubview(activeDetailView)
                yPosition = kActivityListHeight + yPosition
            }
            activityListHeight.constant = yPosition + kActivityViewHeightPadding
        }
        tileTime.addShadow()
    }
    
    // MARK: ActivityStatsProtocol Implementation
    func fetchActivityStats(period: period) {
    }
    
    func startActivity() {
        self.prepareLoadView()
    }
    
    func setMetricsPreference(isMetric : Bool) {
        self.isMetric = isMetric
    }
    
    func setActivityModel(model:activityModel,period:period) {
        self.activityModel = model
        self.period = period
        self.needReload = true
        self.stopActivityAnimation()
        self.setNeedsDisplay()
    }
}

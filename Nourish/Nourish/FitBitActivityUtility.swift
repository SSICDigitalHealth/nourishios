//
//  FitBitActivityUtility.swift
//  Nourish
//
//  Created by Vino (Vinodha) Sundaramoorthy on 5/17/17.
//  Copyright © 2017 SSIC. All rights reserved.
//

import UIKit
import HealthKit
import SwiftKeychainWrapper

let kFitBitCaloriesPath = "activities/calories/date"
let kFitBitCaloriesBMRPath = "activities/caloriesBMR/date"
let kFitBitStepsPath = "activities/steps/date"
let kFitBitDistancePath = "activities/distance/date"
let kFitBitFloorsPath = "activities/floors/date"
let kFitBitActiveCaloriesPath = "activities/activityCalories/date"
let kFitBitSleepPath = "sleep/date"
let kFitBitHeartRatePath = "activites/heart/date"
let kFitBitWeightPath = "sleep/date"
let kFitBitHeightPath = "sleep/date"
let kFitBitIntraDayInterval = "10mins"
let kFitBitTodayInterval = "today"
let kFitBitOneDayInterval = "1d"
let kFitBitWeeklyInterval = "7d"
let kFitBitMonthlyInterval = "30d"
let kFitBitHeartRateInterval = "1min"

enum fitBitDataType {
    case sleep
    case steps
    case heartrate
    case activeCalories
    case caloriesBMR
    case activeDistance
    case activeTime
    
    static func stringForType(type: fitBitDataType) -> String {
        switch type {
            case .heartrate:
                return "heart rate"
            case .steps:
                return "steps"
            case .activeDistance:
                return "active distance"
            case .activeCalories:
                return "active energy burned"
            case .caloriesBMR:
                return "basal energy burned"
            case .sleep:
                return "sleep"
            default:
                return ""
        }
    }
    
    static func unitForType(type : fitBitDataType) -> HKUnit {
        switch type {
        case .heartrate:
            return HKUnit.count().unitDivided(by: HKUnit.minute())
        case .steps:
            return HKUnit.count()
        case .activeCalories:
            return HKUnit.kilocalorie()
        case .caloriesBMR:
            return HKUnit.kilocalorie()
        case .activeDistance:
            return HKUnit.meterUnit(with: .kilo)
        default:
            return HKUnit.count()
        }
    }
    
    static func quantityType(type : fitBitDataType) -> HKQuantityType {
        switch type {
            case .heartrate:
                return HKQuantityType.quantityType(forIdentifier: .heartRate)!
            case .steps:
                 return HKQuantityType.quantityType(forIdentifier: .stepCount)!
            case .activeCalories:
                 return HKQuantityType.quantityType(forIdentifier: .activeEnergyBurned)!
            case .caloriesBMR:
                 return HKQuantityType.quantityType(forIdentifier: .basalEnergyBurned)!
            case .activeDistance:
                return HKQuantityType.quantityType(forIdentifier: .distanceWalkingRunning)!
            default:
                return HKQuantityType.quantityType(forIdentifier: .stepCount)!
        }
    }
    
    static func sampleType(type : fitBitDataType) -> HKSampleType {
        switch type {
        case .steps:
            return HKSampleType.quantityType(forIdentifier: HKQuantityTypeIdentifier.stepCount)!
        case .activeCalories:
            return HKSampleType.quantityType(forIdentifier: HKQuantityTypeIdentifier.activeEnergyBurned)!
        case .caloriesBMR:
            return HKSampleType.quantityType(forIdentifier: HKQuantityTypeIdentifier.basalEnergyBurned)!
        case .activeDistance:
            return HKSampleType.quantityType(forIdentifier: HKQuantityTypeIdentifier.distanceWalkingRunning)!
        default:
            return HKSampleType.quantityType(forIdentifier: HKQuantityTypeIdentifier.stepCount)!
        }
    }
    
    static func quantity(type: fitBitDataType , message:FitBitMessage) -> Double {
        switch type {
            case .heartrate:
                return message.restingHeartRate
            case .activeCalories:
                return message.activeEnergyBurned
            case .caloriesBMR:
                return message.caloriesBMR
            case .steps:
                return message.stepCount
            case .sleep :
                return message.sleepTime
            case .activeDistance:
                return message.distance
            default:
                return 0.0
        }
    }
}

class FitBitActivityUtility : NSObject {
    
    static let healthStore: HKHealthStore? = HKHealthStore()
    var calender = Calendar.current

    static func fitBitDateString(date:Date) -> String {
        var dateString = ""
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd"
        dateString = dateFormatter.string(from: date)
        
        return dateString
    }
    
    static func fitBitIntervalString(date:Date) -> String {
        var intervalString = ""
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "HH:mm"
        intervalString = dateFormatter.string(from: date)
        return intervalString
    }
    
    func saveSleepAnalysis(messageArray:[FitBitMessage]) {
        
        if let sleepType = HKObjectType.categoryType(forIdentifier: HKCategoryTypeIdentifier.sleepAnalysis){
            var sleepSampleArray : [HKCategorySample] = []
            
            for sleepMessage in messageArray {
                let startTime = sleepMessage.date
                let endTime = sleepMessage.date?.addingTimeInterval(sleepMessage.sleepTime)
                let object = HKCategorySample(type:sleepType, value: HKCategoryValueSleepAnalysis.asleep.rawValue, start: startTime!, end: endTime!)
                sleepSampleArray.append(object)
            }
            
            if sleepSampleArray.count > 0 {
                HKHealthStore().save(sleepSampleArray, withCompletion:{ (success, error) -> Void in
                    if error != nil {
                        return
                    }
                    if success {
                        LogUtility.logToFile("FitBit - Sleep data saved to HealthKit")
                    } else {
                        LogUtility.logToFile("FitBit - Error saving Sleep to HealthKit")
                    }
                })
            }
        }
    }
    
    func saveHeartRate(messageArray:[FitBitMessage]) {
        var heartRateSampleArray : [HKQuantitySample] = []

        for heartRateMessage in messageArray {
            let unit = HKUnit.count().unitDivided(by: HKUnit.minute())
            let quantity = HKQuantity(unit: unit, doubleValue: heartRateMessage.restingHeartRate)
            
            if #available(iOS 11.0, *) {
                let type = HKQuantityType.quantityType(forIdentifier: .restingHeartRate)!
                if heartRateMessage.restingHeartRate > 0 {
                    let heartRateSample = HKQuantitySample(type: type, quantity: quantity, start: heartRateMessage.date!, end: heartRateMessage.date!)
                    heartRateSampleArray.append(heartRateSample)
                }
            }
            
        }
        
        if heartRateSampleArray.count > 0 {
            HKHealthStore().save(heartRateSampleArray, withCompletion:{ (success, error) -> Void in
                if error != nil {
                    return
                }
                if success {
                    LogUtility.logToFile("FitBit - Heart rate data saved to HealthKit")
                } else {
                    LogUtility.logToFile("FitBit - Error saving Heart rate to HealthKit")
                }
            })
        }
    }
    
    func saveSleepActivity(messageArray:[FitBitMessage],type:fitBitDataType) {
        let sortedArray = messageArray.sorted(by: {$0.date! < $1.date!})
        let index : Int = 0
        var previousDay : Date? = nil

        for i in index...sortedArray.count - 1 {
            let activity = sortedArray[i]
            //if date changes then call the steps methods
            if previousDay == nil {
                self.sleepForDate(day: activity.date!, lowerIndex: i, upperIndex:sortedArray.count - 1, messageArray: sortedArray)
            } else if self.calender.startOfDay(for: previousDay!) != self.calender.startOfDay(for:activity.date!) {
                self.sleepForDate(day: previousDay!, lowerIndex: i, upperIndex:sortedArray.count - 1, messageArray: sortedArray)
            }
            previousDay = activity.date!
        }
    }
    
    func sleepDate(date:Date,completion:@escaping (Double, NSError?) -> ()) {
        let typeSleep = HKSampleType.categoryType(forIdentifier: HKCategoryTypeIdentifier.sleepAnalysis)

        let (starDate, endDate): (Date, Date) = self.datesFromDate(date: date)
        let predicate = ActivityUtility.predicateFrom(startDate: starDate, endDate: endDate)
        
        let query = HKSampleQuery(sampleType: typeSleep!, predicate: predicate, limit: 0, sortDescriptors: nil) { query, results, error in
            var sampleTotal = 0.0
            
            if results != nil && (results?.count)! > 0
            {
                for result in results as! [HKCategorySample]
                {
                    //Duplicate in the sample can occur , so it can be restricted to choose any source
                    if result.sourceRevision.source.bundleIdentifier.contains("nourish") || result.sourceRevision.source.name.lowercased().contains("nouriq") {
                        sampleTotal = sampleTotal + result.endDate.timeIntervalSince(result.startDate)
                    }
                }
            }
            completion(sampleTotal, error as NSError?)
        }
        HKHealthStore().execute(query)
    }
    
    func sleepForDate(day:Date , lowerIndex:Int , upperIndex:Int,messageArray:[FitBitMessage]) {
        var statsSampleArray : [HKCategorySample] = []
        let sortedArray = messageArray.sorted(by: {$0.date! < $1.date!})
        var index : Int = lowerIndex
        var previousDay = sortedArray[lowerIndex].date
        var totalSeconds : Double = 0
        let typeSleep = HKSampleType.categoryType(forIdentifier: HKCategoryTypeIdentifier.sleepAnalysis)

        //Get total step count for the new day in message array
        self.sleepDate(date:previousDay!,completion: {stats, error in
            var nextDay : Bool = false
            let startingActivity = sortedArray[lowerIndex]
            let startingSeconds = abs(startingActivity.sleepTime - stats)
            if startingSeconds > 0 {
                //Do not write sleep again , if it was already recorded for that day
                let startTime = startingActivity.date
                let endTime = startingActivity.date?.addingTimeInterval(startingSeconds)
                let object = HKCategorySample(type:typeSleep!, value: HKCategoryValueSleepAnalysis.asleep.rawValue, start: startTime!, end: endTime!)
                statsSampleArray.append(object)
                previousDay = sortedArray[index].date
            }
            totalSeconds = startingActivity.sleepTime
            
            if index < upperIndex {
                index += 1
            }
            if lowerIndex <= upperIndex && !nextDay {
                for i in lowerIndex...upperIndex{
                    let activity = sortedArray[i]
                    
                    //If new day , then repeat the steps collection
                    if self.calender.startOfDay(for: previousDay!) == self.calender.startOfDay(for:activity.date!) {
                        let sleepSeconds = abs(activity.sleepTime - totalSeconds)
                        
                        if sleepSeconds > 0 {
                            let startTime = activity.date
                            let endTime = activity.date?.addingTimeInterval(sleepSeconds)
                            let object = HKCategorySample(type:typeSleep!, value: HKCategoryValueSleepAnalysis.asleep.rawValue, start: startTime!, end: endTime!)
                            statsSampleArray.append(object)
                        }
                        
                        totalSeconds = activity.sleepTime
                        previousDay = activity.date
                        index += 1
                    } else {
                        nextDay = true
                        previousDay = activity.date
                        break
                    }
                }
            }
            
            let _ = ActivityUtility.sleepingActivityLastNight(completion: {samples, error in
                if samples != nil {
                    HKHealthStore().delete(samples!, withCompletion: { (success, error) -> Void in
                        if success {
                        }
                    })
                }
            })
            
            //Steps count
            if statsSampleArray.count > 0 {
                HKHealthStore().save(statsSampleArray, withCompletion:{ (success, error) -> Void in
                    if error != nil {
                        LogUtility.logToFile(String(format:"FitBit - Error saving Sleep to HealthKit"))
                        return
                    }
                    if success {
                        LogUtility.logToFile(String(format:"FitBit - Sleep data saved to HealthKit"))
                    }
                })
            }
        })
    }
    
    func saveFitBitActivity(messageArray:[FitBitMessage],type:fitBitDataType) {
        let sortedArray = messageArray.sorted(by: {$0.date! < $1.date!})
        let index : Int = 0
        var previousDay : Date? = nil
        
        for i in index...sortedArray.count - 1 {
            let activity = sortedArray[i]
            //if date changes then call the steps methods
            if previousDay == nil {
                self.statsForDate(day: activity.date!, lowerIndex: i, upperIndex:sortedArray.count - 1, messageArray: sortedArray,dataType:type)
            } else if self.calender.startOfDay(for: previousDay!) != self.calender.startOfDay(for:activity.date!) {
                self.statsForDate(day: previousDay!, lowerIndex: i, upperIndex:sortedArray.count - 1, messageArray: sortedArray,dataType:type)
            }
            previousDay = activity.date!
        }
    }
    
    func statsForDate(day:Date , lowerIndex:Int , upperIndex:Int,messageArray:[FitBitMessage],dataType:fitBitDataType) {
        var statsSampleArray : [HKQuantitySample] = []
        let sortedArray = messageArray.sorted(by: {$0.date! < $1.date!})
        var index : Int = lowerIndex
        var previousDay = sortedArray[lowerIndex].date
        var statsCount : Double = 0
        let type = fitBitDataType.quantityType(type: dataType)
        let unit = fitBitDataType.unitForType(type: dataType)
        
        //Get total step count for the new day in message array
        self.statsForDate(date:previousDay!,dataType:dataType,completion: {stats, error in
            var nextDay : Bool = false
            let startingActivity = sortedArray[lowerIndex]
            let startingStats = abs( fitBitDataType.quantity(type: dataType, message: startingActivity) - stats)
            if startingStats > 0 {
                let quantity = HKQuantity(unit: unit, doubleValue: Double(startingStats))
                let chunkStartDate = self.calender.date(byAdding: .minute, value: Int( -startingActivity.activeMinutes), to: previousDay!) ?? previousDay
                let statsCountSample = HKQuantitySample(type: type, quantity:quantity , start: chunkStartDate!, end: previousDay!)
                previousDay = sortedArray[index].date
                statsSampleArray.append(statsCountSample)
            }
            statsCount =  fitBitDataType.quantity(type: dataType, message: startingActivity)
            
            if index < upperIndex {
                index += 1
            }
            if lowerIndex <= upperIndex && !nextDay {
                for i in lowerIndex...upperIndex{
                    let activity = sortedArray[i]
                    
                    //If new day , then repeat the steps collection
                    if self.calender.startOfDay(for: previousDay!) == self.calender.startOfDay(for:activity.date!) {
                        let stepQuantity = abs(fitBitDataType.quantity(type: dataType, message: activity) - statsCount)
                        
                        if stepQuantity > 0 {
                            let quantity = HKQuantity(unit: unit, doubleValue: Double(stepQuantity))
                            let chunkStartDate = self.calender.date(byAdding: .minute, value: Int( -activity.activeMinutes), to: previousDay!) ?? previousDay
                            let statsCountSample = HKQuantitySample(type: type, quantity:quantity , start: chunkStartDate!, end: activity.date!)
                            previousDay = activity.date
                            statsSampleArray.append(statsCountSample)
                        }
                        statsCount = fitBitDataType.quantity(type: dataType, message: activity)
                        previousDay = activity.date
                        index += 1
                        
                    } else {
                        nextDay = true
                        previousDay = activity.date
                        break
                    }
                }
            }

            //Steps count
            if statsSampleArray.count > 0 {
                HKHealthStore().save(statsSampleArray, withCompletion:{ (success, error) -> Void in
                    if error != nil {
                        LogUtility.logToFile(String(format:"FitBit - Error saving %@ to HealthKit",fitBitDataType.stringForType(type: dataType)))
                        return
                    }
                    if success {
                        LogUtility.logToFile(String(format:"FitBit - %@ data saved to HealthKit",fitBitDataType.stringForType(type: dataType)))
                    }
                })
            }
        })
    }
    
    
    func statsForDate(date:Date,dataType:fitBitDataType,completion:@escaping (Double, NSError?) -> ()) {
        let type = fitBitDataType.sampleType(type: dataType)
        var deleteSample : Bool = false
        
        //Delete - when activity sources is fitbit , if data was already synced for that day
        if defaults.object(forKey: kLastSyncFitBitDateKey) != nil && KeychainWrapper.standard.hasValue(forKey: "kLastSyncFitBit") && KeychainWrapper.standard.string(forKey: kActivitySourceKey) == "Fit Bit" {
            deleteSample = true
        }
        let (starDate, endDate): (Date, Date) = self.datesFromDate(date: date)
        let predicate = ActivityUtility.predicateFrom(startDate: starDate, endDate: endDate)
        
        let query = HKSampleQuery(sampleType: type, predicate: predicate, limit: 0, sortDescriptors: nil) { query, results, error in
            var sampleTotal: Double = 0
        
            if results != nil && (results?.count)! > 0
            {
                if deleteSample {
                    LogUtility.logToFile(String(format:"FitBit - deleting today's %@",fitBitDataType.stringForType(type: dataType)))
                    for result in results as! [HKQuantitySample] {
                        HKHealthStore().delete(result, withCompletion: { (success, error) -> Void in
                            if success {
                            }
                        })
                    }
                    completion(sampleTotal, error as NSError?)
                } else {
                    for result in results as! [HKQuantitySample]
                    {   //Duplicate in the sample can occur , so it can be restricted to choose any source
                        if result.sourceRevision.source.bundleIdentifier.contains(kFitBitKey) || result.sourceRevision.source.name.lowercased().contains(kFitBitKey) {
                            sampleTotal += result.quantity.doubleValue(for: fitBitDataType.unitForType(type:dataType))
                        }
                    }
                    completion(sampleTotal, error as NSError?)
                }
            }
        }
        HKHealthStore().execute(query)
    }
    
    
    func syncFitBitMessages(messageArray : [FitBitMessage]) {
        let sleepMessages = messageArray.filter{$0.messageType == "Sleep"}
        if sleepMessages.count > 0 {
            //FitBitActivityUtility().saveSleepAnalysis(messageArray:sleepMessages)
            FitBitActivityUtility().saveSleepActivity(messageArray: sleepMessages, type: .sleep)
        }
        
        let heartRate = messageArray.filter{$0.messageType == "Heart Rate"}
        if heartRate.count > 0 {
            FitBitActivityUtility().saveHeartRate(messageArray: heartRate)
        }
        
        let activityMessage = messageArray.filter{$0.messageType == "Activity"}
        if activityMessage.count > 0 {
            FitBitActivityUtility().saveFitBitActivity(messageArray: activityMessage, type: .steps)
            FitBitActivityUtility().saveFitBitActivity(messageArray: activityMessage, type: .activeCalories)
            FitBitActivityUtility().saveFitBitActivity(messageArray: activityMessage, type: .caloriesBMR)
            FitBitActivityUtility().saveFitBitActivity(messageArray: activityMessage, type: .activeDistance)
        }
    }
    
    func datesFromDate(date:Date) -> (Date, Date)
    {
        let calendar = Calendar.current
        let nowDate = date
        
        let starDate: Date = calendar.startOfDay(for: nowDate)
        var dateComps = DateComponents()
        dateComps.day = 1
        dateComps.second = -1
        
        let endDate: Date = calendar.date(byAdding: dateComps, to: starDate)!
        return (starDate, endDate)
    }
    

}

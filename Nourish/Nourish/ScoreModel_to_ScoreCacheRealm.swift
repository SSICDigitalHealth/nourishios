//
//  ScoreModel_to_ScoreCacheRealm.swift
//  Nourish
//
//  Created by Vino (Vinodha) Sundaramoorthy on 4/18/17.
//  Copyright © 2017 SSIC. All rights reserved.
//

import UIKit

class ScoreModel_to_ScoreCacheRealm: NSObject {
    
    func transform(model : ScoreModel) -> ScoreCacheRealm {
        let scoreDetails = ScoreCacheRealm()
        scoreDetails.score = model.score
        scoreDetails.scoreDelta = model.scoreDelta
        
        if model.bestDelta.count > 0 {
            for nut in model.bestDelta {
                scoreDetails.bestDelta.append(RealmString(value: nut))
            }
        }
        
        if model.worstDelta.count > 0 {
            for nut in model.worstDelta {
                scoreDetails.worstDelta.append(RealmString(value: nut))
            }
        }
        
        if model.InDri.count > 0 {
            for nut in model.InDri {
                scoreDetails.InDri.append(RealmString(value: nut))
            }
        }
        
        if model.closeToUpperLimit.count > 0 {
            for nut in model.closeToUpperLimit {
                scoreDetails.closeToUpperLimit.append(RealmString(value: nut))
            }
        }
        
        if model.topNutrients.count > 0 {
            for nut in model.topNutrients {
                scoreDetails.nutrientList.append(self.convertToNutrientList(nutrient: nut))
            }
        }
        
        if model.topNutrients.count > 0 {
            for nut in model.topNutrients {
                scoreDetails.nutrientList.append(self.convertToNutrientList(nutrient: nut))
            }
        }
        return scoreDetails
    }

    func convertToNutrientList(nutrient : nutrientInfo) -> NutrientInfoRealm {
        let realmInfo = NutrientInfoRealm()
        realmInfo.max = nutrient.max!
        realmInfo.min = nutrient.min!
        realmInfo.value = nutrient.value
        realmInfo.name = nutrient.name
        realmInfo.unit = nutrient.unit
        return realmInfo
    }
}

//
//  NRRecommendationInteractor.swift
//  Nourish
//
//  Created by Vino (Vinodha) Sundaramoorthy on 11/14/16.
//  Copyright © 2016 SSIC. All rights reserved.
//

import UIKit
import RxSwift

class NRRecommendationInteractor: NSObject {
    
    let recommenDationRepository = RecommendationsRepository.shared
    let userRepo = UserRepository.shared
    let activityRepo = UserActivityRepository()
    
    /*
    func getRecommendationsList() -> [recommendationLists] {
        var list : [recommendationList] = []
        
        let item1 = recommendation(description: "Bananas are great fuel after a workout", timeStamp: "7.00 AM")
        let item2 = recommendation(description: "Try comsuming a cup of yogurt to help with calcium", timeStamp: "2.00 PM")
        let item3 = recommendation(description: "Eat a light salad tonight for dinner to add fiber", timeStamp: "5.00 pM")
        let day1 = recommendationList(day: "Tuesday , Nove 14",[item1,item2,item3])
        list.append(day1)
        
        let item4 = recommendation(description: "Make sure you drink plenty of fluids.8 glasses a day!", timeStamp: "7.00 AM")
        let item5 = recommendation(description: "Consider yogurt instead of that candy bar", timeStamp: "8.00 AM")
        let item6 = recommendation(description: "Start your day with hearty whole grain bread", timeStamp: "6.00 PM")
        
        let day2 = recommendationList(day:"Wednesday Nov 15",[item4,item5,item6])
        list.append(day2)
        
        return list
    }
     */
    
    func getRecommendationList(count : Int) -> Observable<[recommendationListStruct]> {
        let object = userRepo.getCurrentUser(policy: .DefaultPolicy).flatMap({ user -> Observable<[recommendationListStruct]> in
            
            let token = NRUserSession.sharedInstance.accessToken
            let date = Date()
            
            let steps = self.userRecommendationStepsList(user: user)
            let tips = self.recommenDationRepository.getRecTipsFor(date: date, userID: user.userID!, token: token!)
            let recs = self.recommenDationRepository.getRecommendationsFor(date: date, userID: user.userID!, token: token!, count: count)
            
            let zip = Observable.zip(tips, recs, steps, resultSelector : {tipsArray, recsArray, stepsRec -> [recommendationListStruct] in
                var array = tipsArray
                array.append(contentsOf: recsArray)
                
                var recList : [recommendationListStruct] = []
                
                if tipsArray.count > 0 {
                    recList.append(recommendationListStruct(type:recommendationType.tips ,recommendation:tipsArray))
                }
                
                if recsArray.count > 0 {
                    recList.append(contentsOf: self.separateRecs(array: recsArray))
                }
                
                if self.isEmptyRec(rec: stepsRec) == false {
                    array.append(stepsRec)
                    recList.append(recommendationListStruct(type:recommendationType.activity ,recommendation:[stepsRec]))
                }
                
                let formatter = DateFormatter()
                formatter.dateStyle = .medium
                return recList
            })
            return zip
        })
        return object
    }
    
    private func isEmptyRec(rec : recommendationStruct) -> Bool {
        if rec.description.count > 0 {
            return false
        } else {
            return true
        }
    }
    
    private func separateRecs(array : [recommendationStruct]) -> [recommendationListStruct] {
        
        var breakfastArray : [recommendationStruct] = []
        var dinnerArray : [recommendationStruct] = []
        var lunchArray : [recommendationStruct] = []
        var snackArray : [recommendationStruct] = []
        
        for obj in array {
            switch obj.type {
            case .breakfast:
                breakfastArray.append(obj)
            case .lunch:
                lunchArray.append(obj)
            case .dinner:
                dinnerArray.append(obj)
            case .snack:
                snackArray.append(obj)
            default:
                break
            }
        }
        var array : [recommendationListStruct] = []
        
           array.append(recommendationListStruct(type:recommendationType.breakfast ,recommendation:breakfastArray))
        
            array.append(recommendationListStruct(type:recommendationType.lunch ,recommendation:lunchArray))
        
            array.append(recommendationListStruct(type:recommendationType.dinner ,recommendation:dinnerArray))

            array.append(recommendationListStruct(type:recommendationType.snack ,recommendation:snackArray))
        
        return array
    }
    
    private func userRecommendationStepsList(user : UserProfile) -> Observable<recommendationStruct> {
        let emptyRec = recommendationStruct(description: "", title: "", subTitle: "", type: .activity, foodModel: nil)

        if user.userGoal != .LooseWeight {
            return Observable.just(emptyRec)
        } else {
            let steps = self.userRepo.getUserRecommendedSteps()
            let stepsCount = self.activityRepo.getUserStepsForToday()
            let zip = Observable.zip(steps, stepsCount, resultSelector : {recSteps, currentSteps -> recommendationStruct in
                let diff = recSteps - currentSteps
                if diff > 0 {
                    let str = "To achieve your goal, you will need to walk \(Int(diff)) steps before the end of the day."
                    let rec = recommendationStruct(description: str, title: "Walking", subTitle: "", type: .activity, foodModel: nil)

                    return rec
                } else {
                    return emptyRec
                }
                
            })
            return zip
        }
    }
}

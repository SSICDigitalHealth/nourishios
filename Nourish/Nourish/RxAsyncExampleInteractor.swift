//
//  RxAsyncExampleInteractor.swift
//  Nourish
//
//  Created by Nova on 1/12/17.
//  Copyright © 2017 SSIC. All rights reserved.
//

import UIKit
import RxSwift

class SomeTestObject : NSObject {
    var someInt : Int?
    var anotherDouble : Double?
    var testString : String?
}
class RxAsyncExampleInteractor: NSObject {

    
    //interactor func
    //SomeTestObject -> viewModel for presenter
    
    private func loadSomeFakeData() -> Observable<[SomeTestObject]> {

        return Observable.create {observer in
            let test = self.generateRandomBetween(max: 30, min: 10)
            let offset = Double(test)/10.0
            print(offset)
            DispatchQueue.main.asyncAfter(deadline: .now() + offset) {
                var array : [SomeTestObject] = []
                let count = self.generateRandomBetween(max: 5, min: 1)
                for _ in 1...count {
                    array.append(self.generateRandomObject())
                }
                    
                observer.onNext(array)
                observer.onCompleted()
            }
            return Disposables.create()
        }
    }
    
    private func generateRandomObject() -> SomeTestObject {
        let test = SomeTestObject()
        test.someInt = self.generateRandomBetween(max: 100, min: 10)
        test.anotherDouble = Double(self.generateRandomBetween(max: 1000, min: 100)) / Double(self.generateRandomBetween(max: 10, min: 1))
        test.testString = "Some test string"
        return test
    }
    
    func generateRandomBetween(max : Int, min : Int) -> Int {
        return min + Int(arc4random_uniform(UInt32(max - min + 1)))
    }
    
    //presenter func
    
    func testPresenterFunc() {
        let _ = self.loadSomeFakeData().subscribe(onNext: {testArray in
            //here we store data into local properties
            print(testArray)
        },
            onError: {error in
            //manage something if error occured
            print(error)
        },
            onCompleted: {
            //nice place to Update UI on main thread
                DispatchQueue.main.async {
                    print("Completed")
                }
        
        },
            onDisposed: {})
    }
    
}

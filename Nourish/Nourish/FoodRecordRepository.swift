//
//  FoodRecordRepository.swift
//  Nourish
//
//  Created by Nova on 1/20/17.
//  Copyright © 2017 SSIC. All rights reserved.
//

import UIKit
import RxSwift
import RealmSwift

typealias MultiCountedRecord = (FoodFavWithCounter, Int)

final class FoodRecordRepository: NSObject {
    
    static let shared = FoodRecordRepository()
    
    var cachedFavourites : [FoodRecordCache] = []
    var toCacheMapper = foodSearchModel_to_FoodRecordCache()
    var fromCacheMapper = FoodRecordCache_to_foodsearchModel()
    var fromCountedMapper = FoodFavWithCounter_to_foodSearchModel()
    var toCountCacheMapper = foodSearchModel_to_FoodFavWithCounter()
    
    
    func getFavouriteFood() -> Observable<[foodSearchModel]> {
        return Observable.create {observer in
            
            let searchResults = try! Realm().objects(FoodRecordCache.self)
            if searchResults.count > 0 {
                self.cachedFavourites = Array(searchResults)
                let mapped = self.fromCacheMapper.transform(caches: self.cachedFavourites)
                observer.onNext(mapped)
                observer.onCompleted()
            } else {
                observer.onNext([])
                observer.onCompleted()
            }
            return Disposables.create()
        }
    }
    func getCachedFavorites() -> Observable<[MealRecordModel]> {
        let realmCache = FavoritesRepository()
        return Observable.just(realmCache.fetchCachedFavorites())
    }
    
    func isFavourite(foodItem : foodSearchModel) -> Bool {
        if self.cachedFavourites.count > 0 {
            return self.isAvailable(idString: foodItem.foodId, array: self.cachedFavourites)
        } else {
            let searchResults = try! Realm().objects(FoodRecordCache.self)
            if searchResults.count > 0 {
                self.cachedFavourites = Array(searchResults)
            }
            return self.isAvailable(idString: foodItem.foodId, array: self.cachedFavourites)
        }
    }
    
    func toggleFavourite(foodItem : foodSearchModel) -> Observable<Bool> {
            let isFav = self.getFavouriteFood().flatMap({model -> Observable<Bool> in
                let realm = try! Realm()
                if self.isFavourite(foodItem: foodItem) == false {
                    let object = self.toCacheMapper.transform(model: foodItem)
                    try! realm.write {
                        realm.add(object)
                    }
                    let searchResults = try! Realm().objects(FoodRecordCache.self)
                    self.cachedFavourites = Array(searchResults)
                    
                    return Observable.just(true)
                } else {
                    let stringId = foodItem.foodId
                    let predicate = NSPredicate(format : "foodId == %@",stringId)
                    
                    let results = realm.objects(FoodRecordCache.self).filter(predicate).first
                    if results != nil {
                        try! realm.write {
                            realm.delete(results!)
                        }
                        let searchResults = try! Realm().objects(FoodRecordCache.self)
                        self.cachedFavourites = Array(searchResults)
                    }
                    return Observable.just(false)
                }
            })
            return isFav
    }
    
    private func isAvailable(idString : String, array : [FoodRecordCache]) -> Bool {
        var contains = false
        for food in array {
            if food.foodId == idString {
                contains = true
            }
        }
        return contains
    }
    
    func getCountedFavourites() -> Observable<[foodSearchModel]> {
        return Observable.create { observer in
             let searchResults = try! Realm().objects(FoodFavWithCounter.self).sorted(byKeyPath: "counter", ascending: true)
            if searchResults.count > 0 {
                let caches = Array(searchResults)
                let mapped = self.fromCountedMapper.transform(caches: caches)
                observer.onNext(mapped)
                observer.onCompleted()
            } else {
                observer.onCompleted()
            }
            return Disposables.create()
        }
    }
    
    func getCountedFavoritesFor(ocasion : Ocasion, count : Int, minimalCount : Int) -> Observable<[foodSearchModel]> {
        return Observable.create { observer in
            let realm = try! Realm()
            
            let counter = 1
            let predicateOcasion = NSPredicate(format : "foodOccasion == %d", ocasion.rawValue)
            let predicateMinimalCount = NSPredicate(format : "counter >= %d", counter)
            
            let result = realm.objects(FoodFavWithCounter.self).filter(predicateOcasion).filter(predicateMinimalCount).sorted(byKeyPath: "counter", ascending: false)
            
            let objSec = Array(result)
            
            let foodIDDict = self.sortRecordsToDict(records: objSec)
            
            var resultingArray : [foodSearchModel] = []
            
            for (_, object) in foodIDDict {
                let separated = self.separateForCalories(array: object)
                if separated.1 >= minimalCount {
                    resultingArray.append(self.fromCountedMapper.transform(cache: separated.0))
                }
            }
            
            let smallest = min(10, resultingArray.count)
            
            let final = Array(resultingArray[0..<smallest])
            observer.onNext(final.sorted(by: {$0.counted > $1.counted}))
            observer.onCompleted()
            
            return Disposables.create()
        }
    }
    
    private func sortRecordsToDict(records : [FoodFavWithCounter]) -> [String : [FoodFavWithCounter]] {
        var foodIDDict : [String : [FoodFavWithCounter]] = [:]
        for obj in records {
            let foodID = obj.foodId
            if foodID != nil && foodIDDict[foodID!] == nil {
                let filteredArray = records.filter {$0.foodId == foodID}
                foodIDDict[foodID!] = filteredArray
            }
        }
        return foodIDDict
    }
    
    private func separateForCalories(array : [FoodFavWithCounter]) -> MultiCountedRecord {
        var dict = [String : Int]()
        
        var maxCount = 0
        var separatedCounts = 0
        
        for obj in array {
            separatedCounts += obj.counter
            
            if obj.calories != nil && dict[obj.calories!] == nil {
                let count = array.filter {$0.calories == obj.calories!}.count * obj.counter
                maxCount = count > maxCount ? count : maxCount
                dict[obj.calories!] = count
            }
        }
        
        let keyString = self.findKeyForValue(value: maxCount, dictionary: dict)
        let correctedSample = array.filter {$0.calories! == keyString!}.first!
       
        return MultiCountedRecord(correctedSample, separatedCounts)
    }
    
    private func findKeyForValue(value: Int, dictionary: [String: Int]) ->String? {
        for (key, valueArray) in dictionary {
            if valueArray == value {
                return key
            }
        }
        
        return nil
    }
    
    func addToCounted(models : [foodSearchModel]) {
        for model in models {
            self.addToCounted(model: model)
        }
    }
    
    func addToCounted(model : foodSearchModel) {
        let realm = try! Realm()
        let stringId = model.foodId
        
        let predicate = NSPredicate(format : "foodId == %@ AND calories == %@ AND amount == %f && foodOccasion == %d",stringId, model.calories, model.amount, (model.occasion?.rawValue)!)
        
        let result = realm.objects(FoodFavWithCounter.self).filter(predicate).sorted(byKeyPath: "counter", ascending: true).first
        if result != nil {
            realm.beginWrite()
            result!.counter = (result?.counter)! + 1
            try! realm.commitWrite()
        } else {
            let foodFav = self.toCountCacheMapper.transform(model: model)
            foodFav.counter = foodFav.counter + 1
            try! realm.write {
                realm.add(foodFav)
            }
        }
    }
    
    func clearAllCountedFoods() {
        let realm = try! Realm()
        let searchResults = realm.objects(FoodFavWithCounter.self)
        
        if searchResults.count > 0 {
            for food in searchResults {
                try! realm.write {
                    realm.delete(food)
                }
            }
        }
    }
}

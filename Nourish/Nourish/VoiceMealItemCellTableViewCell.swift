//
//  VoiceMealItemCellTableViewCell.swift
//  Nourish
//
//  Created by Vino (Vinodha) Sundaramoorthy on 6/30/17.
//  Copyright © 2017 SSIC. All rights reserved.
//

import UIKit

class VoiceMealItemCellTableViewCell: UITableViewCell {
    @IBOutlet weak var mealName : UILabel!
    @IBOutlet weak var calories : UILabel!
    @IBOutlet weak var deleteButton : UIButton!
    @IBOutlet weak var servingTypePicker : NRPickerView!
    @IBOutlet weak var measurePicker : NRPickerView!
    var mealModel : MealRecord!

    override func awakeFromNib() {
        super.awakeFromNib()
        self.measurePicker.textColor = UIColor.white
        self.measurePicker.highlightedTextColor = UIColor.white
        self.measurePicker.font = UIFont.systemFont(ofSize: 24, weight: UIFontWeightRegular)
        self.measurePicker.highlightedFont = UIFont.systemFont(ofSize: 24, weight: UIFontWeightBold)
        self.measurePicker.maskDisabled = true
        self.servingTypePicker.textColor = NRColorUtility.hexStringToUIColor(hex: "#808080")
        self.servingTypePicker.highlightedTextColor = NRColorUtility.hexStringToUIColor(hex: "#303030")
        self.servingTypePicker.maskDisabled = true
        self.servingTypePicker.font = UIFont.systemFont(ofSize: 14, weight: UIFontWeightRegular)
        self.servingTypePicker.highlightedFont = UIFont.systemFont(ofSize: 14, weight: UIFontWeightBold)
    }

    func setUpMealCell(model : MealRecord) {
        self.mealName.text = model.name
        self.calories.text = String(format:"%.2f kcal",model.calories)
        self.servingTypePicker.tag = 10
        self.measurePicker.tag = 20
        self.mealModel = model
    }
    
}

//
//  MealPreviewTableViewCell.swift
//  Nourish
//
//  Created by Nova on 11/11/16.
//  Copyright © 2016 Samsung. All rights reserved.
//

import UIKit

class MealPreviewTableViewCell: MGSwipeTableCell {
    
    @IBOutlet var titleLabel : UILabel?
    @IBOutlet var servingCountLabel : UILabel?
    @IBOutlet var caloriesLabel : UILabel?
    @IBOutlet weak var leftView : UIView?
    @IBOutlet weak var actionButton : UIButton?
    @IBOutlet weak var pendingLabel : UILabel?
    var userMealId : String = ""
    var ocassion : Ocasion? = nil
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func setupWithModel(record : MealRecordModel) {
        var servingType : String = ""
        let id = record.userFoodID ?? record.localCacheID
        if id != nil && id != "" {
            self.userMealId = id!
        }
        self.ocassion = record.ocasion
        titleLabel?.text = record.descriptionText() 
        let scoreFormatter    = NumberFormatter()
        scoreFormatter.minimumFractionDigits = 0
        scoreFormatter.maximumFractionDigits = 2
        
        if let  ser = record.servingType {
            servingType = ser
        }
        
        if (servingType.count) > 0  {
            let servingAmount = scoreFormatter.string(from: NSNumber(value: record.servingAmmount!))
            servingCountLabel?.text = String(format:"%@ %@",servingAmount!,record.servingType!)
        } else {
            servingCountLabel?.text = servingType
        }
        
        
        self.pendingLabel?.textColor = UIColor.white
        self.pendingLabel?.layer.masksToBounds = true
        self.pendingLabel?.layer.cornerRadius = 2.0
        self.pendingLabel?.backgroundColor = NRColorUtility.appBackgroundColor()
            
        if record.groupedMealArray.count > 0 {
            if record.groupedMealArray.first?.isFromOperaion == true {
                self.pendingLabel?.isHidden = false
            } else {
                self.pendingLabel?.isHidden = true
            }
        } else {
            //print("meal -> \(record.idString) : \(record.foodId) : \(record.userFoodID)")
            if record.isFromOperaion == true {
                self.pendingLabel?.isHidden = false
            } else {
                self.pendingLabel?.isHidden = true
            }
        }
       
        
        caloriesLabel?.text = String(format : "%.1f cal",record.groupCalories())
        var actionImage : UIImage?  = nil
        if record.isGroupFavourite == true {
            servingCountLabel?.text = ""
            actionImage = UIImage(named:"favgroup")
        } else if record.isFavourite == true {
            actionImage = UIImage(named:"fav_inactive")
        } else if (record.groupedMealArray.count > 1 && record.isGroupFavourite == false) {
            actionImage = UIImage(named: "fav_grp")
        } else {
            actionImage = UIImage(named:"circle_select")
        }
        actionButton?.setImage(actionImage, for: .normal)
    }
}

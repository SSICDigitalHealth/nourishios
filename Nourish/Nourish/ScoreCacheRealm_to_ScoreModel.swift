//
//  ScoreCacheRealm_to_ScoreModel.swift
//  Nourish
//
//  Created by Vino (Vinodha) Sundaramoorthy on 4/19/17.
//  Copyright © 2017 SSIC. All rights reserved.
//

import UIKit
import RealmSwift

class ScoreCacheRealm_to_ScoreModel: NSObject {
    
    func transform(cache: ScoreCacheRealm) -> ScoreModel {
        var best : [String] = []
        var worst : [String] = []
        var aboveDri : [String] = []
        var intoDri: [String] = []
        var closeToUpperLimit : [String] = []
        var nutrients : [nutrientInfo] = []
        
        for nut in cache.bestDelta {
            best.append(nut.stringValue)
        }
        
        for nut in cache.worstDelta {
            worst.append(nut.stringValue)
        }
        
        for nut in cache.aboveDri {
            aboveDri.append(nut.stringValue)
        }
        
        for nut in cache.InDri {
            intoDri.append(nut.stringValue)
        }
        
        for nut in cache.closeToUpperLimit {
            closeToUpperLimit.append(nut.stringValue)
        }
        
        for nut in cache.nutrientList {
            nutrients.append(nutrientInfo(name: nut.name, value: nut.value, min: nut.min, max: nut.max, unit: nut.unit))
        }
        
        let scoreModel = ScoreModel(score: cache.score, scoreDelta: cache.scoreDelta, valueToShow : cache.valueToShow, topNutrients: nutrients, closeToUpperLimit: closeToUpperLimit, InDri: intoDri, aboveDri: aboveDri, worstDelta: worst, bestDelta: best)
        return scoreModel
    }

}

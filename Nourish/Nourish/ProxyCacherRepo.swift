//
//  ProxyCacherRepo.swift
//  NourIQ
//
//  Created by Igor on 5/25/18.
//  Copyright © 2018 SSIC. All rights reserved.
//

import Foundation


class ProxyCacherRepo  {
    static let kCacheValue = "cacheValue"
    static let shared = ProxyCacherRepo()
    
    func storeLastCache() {
        let nestleProgressCacheDataStore = NestleCacheDataStore()
        
        var cacheValue = nestleProgressCacheDataStore.getCacheFor(startDate: Date(), endDate: Date(), returnHistory: false)?.userScore
        if cacheValue == nil {
            cacheValue = nestleProgressCacheDataStore.getCacheFor(startDate: Date(), endDate: Date(), returnHistory: true)?.userScore
        }
        
        let df = DateFormatter()
        df.dateFormat = "yyyy-MM-dd"
        
        var stringRep = df.string(from: Date())
        stringRep.append(ProxyCacherRepo.kCacheValue)
        
        if let value = cacheValue {
            defaults.set(value, forKey: stringRep)
            defaults.synchronize()
        }
        
    }
    
    func retreiveLastCacheFor(date : Date) -> Double? {
        let df = DateFormatter()
        df.dateFormat = "yyyy-MM-dd"
        
        var stringRep = df.string(from: date)
        stringRep.append(ProxyCacherRepo.kCacheValue)
        
        let value = defaults.double(forKey: stringRep)
        
        return value
    }
}


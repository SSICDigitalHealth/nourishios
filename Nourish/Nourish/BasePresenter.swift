//
//  BasePresenter.swift
//  Nourish
//
//  Created by Nova on 11/16/16.
//  Copyright © 2016 SSIC. All rights reserved.
//

import UIKit
import RxSwift

class BasePresenter : NSObject, BasePresenterProtocol {
    
   // var disposeBag = DisposeBag()
    var subscribtions = [Disposable]()

    func viewWillAppear(_ animated : Bool) {
    }
    
    func viewDidAppear(_ animated : Bool) {
    }
    
    func viewWillDisappear(_ animated : Bool) {
        for (index, subscription) in self.subscribtions.enumerated() {
            subscription.dispose()
            if self.subscribtions.count > index {
                self.subscribtions.remove(at: index)
            }
        }
    }
    
    func viewDidDisappear(_ animated : Bool) {
      //  self.disposeBag = DisposeBag()
    }
    
    func viewDidLayoutSubviews() {
        
    }

}

//
//  MealPlannerModel.swift
//  Nourish
//
//  Created by Vlad Birukov on 21.08.17.
//  Copyright © 2017 SSIC. All rights reserved.
//

import Foundation

class MealPlannerModel: NSObject {
    var targetCalories: Double = 0.0
    var score: Double = 0.0
    var meal: [MealModel]?
    var preference = PreferenceMealPlannerModel()
}

class MealModel: NSObject {
    var nameOccasion: Ocasion = Ocasion.breakfast
    var calories = [Double]()
    var nameFood = [[String]]()
    var detail = [OccasionMealPlannerModel]()
}

class PreferenceMealPlannerModel: NSObject {
    var lunch = 0
    var breakfast = 0
    var dinner = 0
    var snacks = 0
}

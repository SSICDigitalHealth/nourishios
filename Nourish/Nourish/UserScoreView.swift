//
//  UserScoreView.swift
//  Nourish
//
//  Created by Vlad Birukov on 15.06.17.
//  Copyright © 2017 SSIC. All rights reserved.
//

import UIKit

protocol UserScoreProtocol : BaseViewProtocol {
    func config() -> ProgressConfig?
}

class UserScoreView: BaseView,  UserScoreProtocol {
    
    @IBOutlet weak var topCaption: NSLayoutConstraint!
    @IBOutlet weak var bottomCaption: NSLayoutConstraint!
    @IBOutlet weak var chartHeight: NSLayoutConstraint!
    @IBOutlet weak var heightChartView: NSLayoutConstraint!
    @IBOutlet weak var chartDetailView: UIView!
    @IBOutlet weak var captionLabel: UILabel!
    @IBOutlet var presenter: UserScorePresenter!
    var progressConfig: ProgressConfig?
    
    func config() -> ProgressConfig? {
        return self.progressConfig
    }
    
    override func viewDidLoad() {
        self.xibAutoLayouting(nibName: "UserScoreView")
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.basePresenter = presenter
        self.presenter.userScoreView = self
        super.viewWillAppear(animated)
    }
    
    func settingView() {
        if let config = progressConfig {
            switch config.period {
            case .daily:
                topCaption.constant = 0
                bottomCaption.constant = 0
                chartHeight.constant = 0
                heightChartView.constant = 0
                chartDetailView.isHidden = true
                self.layoutIfNeeded()
            case .monthly:
                self.captionLabel.text = "You monthly score over time"
            case .weekly:
                self.captionLabel.text = "Your weekly score over time"
            }
        }
    }
    
   
}

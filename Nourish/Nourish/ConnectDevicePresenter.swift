//
//  ConnectDevicePresenter.swift
//  Nourish
//
//  Created by Vino (Vinodha) Sundaramoorthy on 5/18/17.
//  Copyright © 2017 SSIC. All rights reserved.
//

import UIKit
import RxSwift
import SwiftKeychainWrapper

class ConnectDevicePresenter: BasePresenter , UITableViewDataSource , UITableViewDelegate {
    let deviceTypes : [String] = ["Trackers","Scales"]
    var scaleTypes : [(name:String,connectedStatus : Bool)] = [(name:"Nokia / Withings",connectedStatus:false),(name:"None",connectedStatus:true)]
    var trackerTypes : [(name:String,connectedStatus : Bool)] = [(name:"Apple Watch",connectedStatus:false),(name:"FitBit",connectedStatus:false),(name:"None",connectedStatus:false)]
    var deviceView : ConnectDeviceProtocol?
    let withingsInteractor = WithingsInteractor()
    let fitBitInteractor = FitBitInteractor()
    var fitBitAuthorized : Bool = false
    var withingsAuthorized : Bool = false
    

    // MARK : BasePresenterProtocol
    
     override func viewWillAppear(_ animated: Bool) {
    
        let _ = withingsInteractor.pairedDevices().subscribe(onNext: { [unowned self] pairedDevices in
            if pairedDevices.count > 0 {
                let pairedWithingsDevices = pairedDevices.filter({$0.isInvalidated == false && $0.dtidString == WITHINGS_DTID})
                
                if pairedWithingsDevices.count > 0 {
                    let withingsDevice = pairedWithingsDevices.first
                        self.withingsAuthorized = (withingsDevice?.isAuthorized)!
                        self.scaleTypes[0].connectedStatus = (withingsDevice?.isAuthorized)!
                        self.scaleTypes[1].connectedStatus = (!(withingsDevice?.isAuthorized)!)
                }
                
                let pariedFitBitDevices = pairedDevices.filter({$0.dtidString == kFitBitDeviceTypeId})
                
                if pariedFitBitDevices.count > 0 {
                    let fitBitDevice = pariedFitBitDevices.first
                    self.fitBitAuthorized = (fitBitDevice?.isAuthorized)!
                }
            }
        }, onError: {error in}, onCompleted: {
            DispatchQueue.main.async {
                self.refreshTrackerConnectedStatus()
            }
        }, onDisposed: {})
        
    }
    
    // MARK : UITableViewDateSource 
    func numberOfSections(in tableView: UITableView) -> Int {
        return deviceTypes.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0 {
            return trackerTypes.count
        } else {
            return scaleTypes.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let data = indexPath.section == 0 ? trackerTypes[indexPath.row] : scaleTypes[indexPath.row]
        let cell : ConnectDeviceTableViewCell = tableView.dequeueReusableCell(withIdentifier: "kConnectDeviceCell") as! ConnectDeviceTableViewCell
        cell.selectionStyle = UITableViewCellSelectionStyle.none
        cell.deviceName.text = data.name
        cell.setConnectedStatus(status: data.connectedStatus)
        return cell
    }
    
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerView = UIView(frame: CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: 60))
        headerView.backgroundColor = UIColor.white
        
        //Title
        let titleLabel = UILabel(frame: CGRect(x: 16, y: 25, width: 300, height: 21))
        titleLabel.text = deviceTypes[section]
        titleLabel.textColor = NRColorUtility.hexStringToUIColor(hex: "#303030")
        titleLabel.font = UIFont.init(name: "Campton-Bold", size: 18)
        titleLabel.backgroundColor = UIColor.clear
        headerView.addSubview(titleLabel)
        return headerView
    }
    
   
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 60
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 25
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        if section == 0 {
        let footerView = UIView(frame: CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: 25))
        footerView.backgroundColor = UIColor.white
        let lineView = UIView(frame: CGRect(x: 0, y: 20, width: UIScreen.main.bounds.width, height: 1))
        lineView.backgroundColor = NRColorUtility.hexStringToUIColor(hex: "#efefef")
        footerView.addSubview(lineView)
        return footerView
        }
        return nil
    }
    
    // MARK : UITableViewDelegate
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let section = indexPath.section
        let row = indexPath.row
        let dataSource = section == 0 ? self.trackerTypes : self.scaleTypes
        let data = dataSource[row]
        
        switch data.name {
            case "Nokia / Withings":
                if !self.withingsAuthorized {
                    self.deviceView?.openWithingsLogin()
                }
                break
            case "Apple Watch":
                if self.fitBitAuthorized {
                    self.unathorizeFitBitWith(sourceType: "Apple Watch")
                } else {
                    KeychainWrapper.standard.set("Apple Watch", forKey: kActivitySourceKey)
                    self.refreshTrackerConnectedStatus()
                }
                break
            case "FitBit":
                if self.fitBitAuthorized {
                    KeychainWrapper.standard.set("Fit Bit", forKey: kActivitySourceKey)
                    self.refreshTrackerConnectedStatus()
                } else {
                    self.deviceView?.openFitBitLogin()
                }
                break
            case "None":
                
                let section = indexPath.section
                if section == 0 {
                    if self.fitBitAuthorized {
                        self.unathorizeFitBitWith(sourceType: "None")
                    } else {
                        KeychainWrapper.standard.set("None", forKey: kActivitySourceKey)
                        self.refreshTrackerConnectedStatus()
                    }
                } else {
                    self.deviceView?.startActivityAnimation()
                    let _ = self.withingsInteractor.unathorizeWithingsDevice().observeOn(MainScheduler.instance).subscribe(onNext: { [unowned self] unauthorized in
                        self.scaleTypes[0].connectedStatus = false
                        self.scaleTypes[1].connectedStatus = true
                        self.withingsAuthorized = false
                    }, onError: {error in
                        LogUtility.logToFile("Error unpairing withings ", error)
                    }, onCompleted: {
                        self.refreshTrackerConnectedStatus()
                    }, onDisposed: {})
                
                }
                
                break
            default: break
        }
    }
    
    
    private func unathorizeFitBitWith(sourceType : String) {
        self.deviceView?.startActivityAnimation()
        let _ = self.fitBitInteractor.unauthorizeFitBitDevice().observeOn(MainScheduler.instance).subscribe(onNext: {unauthorized in
            KeychainWrapper.standard.set(sourceType, forKey: kActivitySourceKey)
        }, onError: {error in
            LogUtility.logToFile("Error unpairing FitBit ", error)
        }, onCompleted: {
            self.refreshTrackerConnectedStatus()
        }, onDisposed: {})
    }
    
    
    func refreshTrackerConnectedStatus() {
        for i in 0...self.trackerTypes.count-1 {
            self.trackerTypes[i].connectedStatus = false
        }
        
        var connectTrackerIndex = 2
        if KeychainWrapper.standard.string(forKey: kActivitySourceKey) != nil {
            connectTrackerIndex = KeychainWrapper.standard.string(forKey: kActivitySourceKey)   == "Apple Watch" ? 0 : KeychainWrapper.standard.string(forKey: kActivitySourceKey)  == "Fit Bit" ? 1 : 2
        }
        trackerTypes[connectTrackerIndex].connectedStatus = true
        self.deviceView?.loadDevices()
    }

}

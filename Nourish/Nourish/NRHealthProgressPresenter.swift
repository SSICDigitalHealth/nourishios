//
//  NRHealthProgressPresenter.swift
//  Nourish
//
//  Created by Vino (Vinodha) Sundaramoorthy on 12/1/16.
//  Copyright © 2016 SSIC. All rights reserved.
//

import UIKit
import RxSwift

class NRHealthProgressPresenter: BasePresenter {
    let interactor = NRHealthProgressInteractor()
    let stress = NRStressInteractor.shared
    var healthProgressView : NRHealthProgressProtocol!
    let mapperObject = NRHealthProgress_to_NRHealthProgressModel()
    let mapperModel = NRHealthProgressModel_to_NRHealthProgress()
    let cachedModel : NRHealthProgressModel? = nil
    var cachedStress : stressLevel = .Undetermined
    
    override func viewWillAppear(_ animated : Bool) {
        if healthProgressView != nil {
            let stress = self.stress.stressLastHour.asObservable()
            let progress = self.interactor.healthProgressFor(date: Date())
            let value = Observable.combineLatest(stress, progress, resultSelector: {str, prog -> NRHealthProgressModel in
                prog.stress = str
                return self.mapperObject.transform(healthProgress: prog)
            })
            
            self.subscribtions.append(value.observeOn(MainScheduler.instance).observeOn(MainScheduler.instance).subscribe(onNext: { [unowned self] model in
                self.healthProgressView.setupWith(progressModel: model)
            }, onError: {error in }, onCompleted: {}, onDisposed: {}))
        }
    }
    
    
}

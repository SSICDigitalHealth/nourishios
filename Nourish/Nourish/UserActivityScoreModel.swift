//
//  UserActivityScoreModel.swift
//  Nourish
//
//  Created by Vlad Birukov on 06.06.17.
//  Copyright © 2017 SSIC. All rights reserved.
//

import Foundation

class UserActivityScoreModel: NSObject {
    var scoreActivity: Double = 0.0
    var historyScore: [(subtitle: Date, value: Double)] = [(subtitle: Date, value: Double)]()
    var isDairyEmpty = Bool(false)
}

//
//  VoiceMealListProtocol.swift
//  Nourish
//
//  Created by Vino (Vinodha) Sundaramoorthy on 7/1/17.
//  Copyright © 2017 SSIC. All rights reserved.
//

import UIKit

protocol VoiceMealListProtocol : BaseViewProtocol {
    func setUpInitialView(ocasion:Int)
    func reloadCell(cell :VoiceMealItemCellTableViewCell)
    func reloadTableView()
    func finishSave()
    func dismiss()
}

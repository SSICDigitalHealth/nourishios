//
//  ImageToBase64Mapper.swift
//  Nourish
//
//  Created by Nova on 4/27/17.
//  Copyright © 2017 SSIC. All rights reserved.
//

import UIKit

class ImageToBase64Mapper: NSObject {
    
    func pack(image : UIImage) -> String {
        let imageData = UIImageJPEGRepresentation(image, 0.8)
        let base64String = imageData?.base64EncodedString(options: .lineLength64Characters)
        
        return base64String!
    }
    
    func unpack(base64 : String) -> UIImage {
        let decodedData = Data.init(base64Encoded: base64)
        let decodedimage = UIImage(data: decodedData!)
        
        return decodedimage!
    }
    
}

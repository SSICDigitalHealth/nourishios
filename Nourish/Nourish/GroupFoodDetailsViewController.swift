//
//  GroupFoodDetailsViewController.swift
//  Nourish
//
//  Created by Vino (Vinodha) Sundaramoorthy on 3/8/17.
//  Copyright © 2017 SSIC. All rights reserved.
//

import UIKit

class GroupFoodDetailsViewController: BasePresentationViewController, FoodDetailsViewControllerDelegate {

    @IBOutlet weak var groupFoodDetailsView : GroupFoodDetailsView!
    let mapper = MealRecord_to_MealRecordModel()
    var mealRecord : MealRecordModel!
    var ocasion : Ocasion!
    var date : Date!
    var fromFav : Bool = false
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)

    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.groupFoodDetailsView.viewController = self
        self.groupFoodDetailsView.fromFav = self.fromFav
        self.baseViews = [groupFoodDetailsView]
        self.groupFoodDetailsView.groupFoodDetails = mapper.groupFoodDetailModelFrom(model: mealRecord)
        let button = UIBarButtonItem(image: UIImage(named : "fav_green"), style: .plain, target: nil, action: nil)
        let titleAttribute = [NSForegroundColorAttributeName : NRColorUtility.hexStringToUIColor(hex: "#242424") ,NSFontAttributeName : UIFont(name: "Campton-Bold", size: 20)!]
        self.navigationController?.navigationBar.titleTextAttributes = titleAttribute
        self.groupFoodDetailsView.favButton = button
        self.navigationItem.rightBarButtonItem = self.groupFoodDetailsView.favButton
        self.navigationItem.rightBarButtonItem?.tintColor = NRColorUtility.hexStringToUIColor(hex: "#70c397")
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
        self.navigationItem.backBarButtonItem?.tintColor = NRColorUtility.hexStringToUIColor(hex: "#808080")
        self.groupFoodDetailsView.setupFavouriteButton()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func showFoodDetails(model : foodSearchModel) {
        let vc = NRFoodDetailsViewController(nibName: "NRFoodDetailsViewController", bundle: nil)
        vc.title = model.foodTitle
        vc.foodSearchModel = model
        vc.isFromGroup = true
        vc.delegate = self
        if model.userPhotoId == nil {
            vc.backgroundImage = NavigationUtility.takeScreenShot()
            self.present(vc, animated : true)
        } else {
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    
    func foodDetailsViewControllerDidFinishWith(foodSearchModel: foodSearchModel, presentedModal: Bool) {
        if presentedModal == true {
            self.dismiss(animated: true, completion: nil)
        }
        self.navigationController?.popViewController(animated: true)
    }
    
    
    @IBAction func addToDiary(_ sender:UIButton) {
        sender.isEnabled = false
        if ocasion != nil {
            self.groupFoodDetailsView.addToDiaryForOcassion(ocasion: self.ocasion!, date : date)
        }
    }
    
//    @IBAction func removeFromFavorites(_ sender : UIButton) {
//        self.groupFoodDetailsView.toggleFavourite(sender)
//    }
    
    func finishSaveToDiary() {
        if let viewControllers: [UIViewController] = self.navigationController?.viewControllers {
            self.navigationController?.navigationBar.isUserInteractionEnabled = true
            self.navigationController?.view.isUserInteractionEnabled = true
            if self.fromFav {
                for vc in viewControllers {
                    if vc.isKind(of: NRFoodSearchViewController.self) {
                        let searchVC =  vc as! NRFoodSearchViewController
                        searchVC.foodSearchView?.itemAdded = true
                        self.navigationController!.popToViewController(searchVC, animated: true)
                        break
                    }
                }
            } else {
                let _ = self.navigationController?.popViewController(animated: true)
            }
        }
    }
    
    func popToMealDiary () {
        DispatchQueue.main.async {
            self.navigationController?.popViewController(animated: true)
        }
    }
    
    func unfavoriteGroup() {
        let _ = self.navigationController?.popViewController(animated: true)
    }
}

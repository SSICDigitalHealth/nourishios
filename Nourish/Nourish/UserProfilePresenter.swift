//
//  NRUserProfilePresenter.swift
//  Nourish
//
//  Created by Nova on 11/15/16.
//  Copyright © 2016 SSIC. All rights reserved.
//

import UIKit
import RxSwift


let kHeaderHeight : CGFloat = 30
let kFooterHeight : CGFloat = 20

class UserProfilePresenter: BasePresenter, UITableViewDelegate,UITableViewDataSource ,NRPickerViewDelegate,NRPickerViewDataSource  {
    
    let interactor = UserProfileInteractor()
    let activityRepo = UserActivityRepository()
    let mapperObject = UserProfile_to_UserProfileModel()
    let mapperModel = UserProfileModel_to_UserProfile()
    var userModel : UserProfileModel = UserProfileModel()
    var userModelView : UserProfileViewProtocol?
    var hasChanges : Bool = false
    
    //Data Source Arrays
    var profileSettingCells : [profileCells] = [.nameCell,.genderCell,.ageCell,.metricsCell,.heightCell,.weightCell,.dietCell,.activityCell]
    var genderOptionsArray : [(name:String,isSelected:Bool)] = [(name:"Male",isSelected:false),(name:"Female",isSelected:false)]
    var dietaryPreferenceOptionArray : [(name:String,isSelected:Bool)] = [(name:"No Restriction",isSelected:true),(name:"Vegetarian",isSelected:false)]
    var activityOptionArray : [(name:String,isSelected:Bool)] = [(name:"Sedentary",isSelected:false),(name:"Moderate",isSelected:true),(name:"Active",isSelected:false),(name:"Very Active",isSelected:false)]
    var metricsOptionArray = [units(name: "Imperial", description: "Feet,Inches,Pounds,Ounces",
                                    isSelected: true),units(name: "Metric", description: "Meters,Centimeters,Grams,Liters", isSelected: false)]
    var profileArray = [profileModel(title: "First", value: "", placeHolder: "First Name"),profileModel(title: "Last", value: "", placeHolder: "Last Name"),profileModel(title: "Email", value: "", placeHolder: "Address")]
    
    //Picker Data Source Array
    var ageDataSource :[Int] = []
    var heightDataSource : [String] = []
    var weightDataSource : [String] = []
    
    // MARK: Base Protocol Implementation
    override func viewWillAppear(_ animated : Bool) {
        let _ = self.currentUser().observeOn(MainScheduler.instance)
.subscribe(onNext: { [unowned self] user in
            self.userModel = user
            self.setUserProfile()
        }, onError: {error in
            if self.userModelView != nil {
                self.userModelView?.stopActivityAnimation()
                self.userModelView?.parseError(error: error, completion: nil)
            }
            LogUtility.logToFile("error is ", error)
        }, onCompleted: {
            if self.userModelView != nil {
                self.setUserProfile()
            }
        }, onDisposed: {})
    }
    
    func currentUser() -> Observable<UserProfileModel> {
        /*
        let object = self.interactor.getCurrentUser().flatMap({ userProfile -> Observable<UserProfileModel> in
            self.userModel = self.mapperObject.transform(user: userProfile)
            return Observable.just(self.mapperObject.transform(user: userProfile))
        })
        return object
        */
        let user = self.interactor.getRawUser()
        let userWeightHK = self.activityRepo.getUserWeight()
        
        let zip = Observable.zip(user, userWeightHK, resultSelector : {userProfile, userWeight -> UserProfileModel in
            let userModel = self.mapperObject.transform(user: userProfile)
            if userWeight != nil {
                userModel.weight = userWeight
            }
            return userModel
        })
        return zip
    }
    
    func saveCurrentUser() {
        self.userModelView?.startActivityAnimation()
        let some = interactor.storeUser(user: mapperModel.transform(userModel: userModel), needToRefreshToken: true)
        let _ = some.observeOn(MainScheduler.instance)
.subscribe(onNext: {artikError in
            if artikError != nil {
                LogUtility.logToFile("error is \(artikError!)")
            }
        }, onError: {error in
            LogUtility.logToFile(error)
            if self.userModelView != nil {
                self.userModelView?.parseError(error: error, completion: nil)
            }
        }, onCompleted: {
            LogUtility.logToFile("save to Artik completed")
            self.hasChanges = false
            self.userModelView?.refreshUserSettings()
            self.userModelView?.showToast(message: "Profile Saved")
        }, onDisposed: {})
    }
    
    func firstNameChanged(firstName : String) {
        if firstName == userModel.userFirstName {
            return
        }
        userModel.userFirstName = firstName
        self.hasChanges = true
    }
    
    func lastNameChanged(lastName : String) {
        if lastName == userModel.userLastName {
            return
        }
        userModel.userLastName = lastName
        self.hasChanges = true
    }
    
    func emailAdressChanged(email : String) {
        if email == userModel.email {
            return
        }
        userModel.email = email
        self.hasChanges = true
    }

    
    func heightChanged(height : Double) {
        userModel.height = height
        self.hasChanges = true
    }
    
    func weightChanged(weight : Double) {
        userModel.weight = weight
        self.hasChanges = true
    }
    
    func ageChanged(age : Int) {
        userModel.age = age
        self.hasChanges = true
    }
    
    func activityLevelChanged(activityLevel : ActivityLevel) {
        userModel.activityLevel = activityLevel
        self.hasChanges = true
    }
    
    func dieteryPreferenceChanged(dietaryPreference : DietaryPreference) {
        userModel.dietaryPreference = dietaryPreference
        self.hasChanges = true
    }
    
    func metricPreferenceChanged(metricPreference : MetricPreference) {
        userModel.metricPreference = metricPreference
        self.hasChanges = true
        self.populatePickerDataSource()
        self.userModelView?.reloadMetricPreference()
    }
    
    func biologicalSexChanged(biologicalSex : BiologicalSex) {
        self.hasChanges = true
        userModel.biologicalSex = biologicalSex
    }
    
    //MARK : Helper methods
    func setUserProfile() {
        self.populatePickerDataSource()
        
        //Set user name info
        self.profileArray[0].value = self.userModel.userFirstName!
        self.profileArray[1].value = self.userModel.userLastName != nil ? self.userModel.userLastName! : ""
        self.profileArray[2].value = self.userModel.email != nil ? self.userModel.email! : ""
        
        //Gender
        for  i  in 0...self.genderOptionsArray.count - 1 {
            if self.genderOptionsArray[i].name.lowercased() == self.userModel.biologicalSex?.description {
                self.genderOptionsArray[i].isSelected = true
            } else {
                self.genderOptionsArray[i].isSelected = false
            }
        }
        
        //Dietry Preference
        for  i  in 0...self.dietaryPreferenceOptionArray.count - 1 {
            if self.dietaryPreferenceOptionArray[i].name == self.userModel.dietaryPreference?.profileDescription {
                self.dietaryPreferenceOptionArray[i].isSelected = true
            } else {
                self.dietaryPreferenceOptionArray[i].isSelected = false
            }
        }
        
        //Activity Level
        for  i  in 0...self.activityOptionArray.count - 1 {
            if self.activityOptionArray[i].name.lowercased() == self.userModel.activityLevel?.dataSourceMappingString {
                self.activityOptionArray[i].isSelected = true
            } else {
                self.activityOptionArray[i].isSelected = false
            }
        }
        
        //Metrics Preference
        for  i  in 0...self.metricsOptionArray.count - 1 {
            if self.metricsOptionArray[i].name.lowercased() == self.userModel.metricPreference?.dataSourceMappingString {
                self.metricsOptionArray[i].isSelected = true
            } else {
                self.metricsOptionArray[i].isSelected = false
            }
        }
        self.userModelView?.refreshUserSettings()
    }
    
    func populatePickerDataSource() {
        //Change datasource of pickers based on metrics preference
        self.weightDataSource = []
        self.heightDataSource = []
        self.ageDataSource = []
        
        for age in 18...kMaxAge {
            self.ageDataSource.append(age)
        }
        
        //Metric - centimeter ,kilogram
        if self.userModel.metricPreference != nil {
            if self.userModel.metricPreference == MetricPreference.Metric {
                for height  in kMinHeight...kMaxHeight {
                    self.heightDataSource.append(String(height))
                }
                for weight in kMinWeightMetric...kMaxWeightMetric {
                    self.weightDataSource.append(String(weight))
                }
            }
            else {
                //Feet , inches , pounds
                self.heightDataSource = ["1'8''","1'9''","1'10''","1'11''"
                ,"2'0''","2'1''","2'2''","2'3''","2'4''","2'5''","2'6''","2'7''","2'8''","2'9''","2'10''","2'11''"
                ,"3'0''","3'1''","3'2''","3'3''","3'4''","3'5''","3'6''","3'7''","3'8''","3'9''","3'10''","3'11''",
                 "4'0''","4'1''","4'2''","4'3''","4'4''","4'5''","4'6''","4'7''","4'8''","4'9''","4'10''","4'11''",
                 "5'0''","5'1''","5'2''","5'3''","5'4''","5'5''","5'6''","5'7''","5'8''","5'9''","5'10''","5'11''",
                 "6'0''","6'1''","6'2''","6'3''","6'4''","6'5''","6'6''","6'7''","6'8''","6'9''","6'10''","6'11''"
                ,"7'0''","7'1''","7'2''","7'3''","7'4''","7'5''","7'6''","7'7''","7'8''","7'9''","7'10''","7'11''","8'0''","8'1''","8'2''"]
            
                for weight in Int(NRConversionUtility.kiloToPounds(valueInKilo: Double(kMinWeightMetric)))...kMaxWeightImperial
                {
                    self.weightDataSource.append(String(weight))
                }
            }
        }
    }
    
    func getUserHeight(newHeight : String) -> String {
        var heightString = ""
        
        heightString = self.userModel.metricPreference == MetricPreference.Metric ?  String(format:"%.0f", NRConversionUtility.metersToCentimeter(valueInMeters: Double(newHeight)!)) :  NRConversionUtility.meresToFeetAndInch(valueInMeter: Double(newHeight)!)
        return heightString
    }
    
    func getUserWeight(newWeight : Double) -> String {
        var weight = ""
        weight = self.userModel.metricPreference == MetricPreference.Metric ?  String(format:"%.0f", newWeight) :  String(format:"%.0f",NRConversionUtility.kiloToPounds(valueInKilo: newWeight))

        return weight
    }
    
    
    func setUserHeight(newHeight : Double) -> Double {
        var height : Double = 0
        
        if self.userModel.height != nil {
            height = self.userModel.metricPreference == MetricPreference.Metric ? NRConversionUtility.centimeterToMeter(valueInCenti: newHeight) :  NRConversionUtility.feetAndInchToMeters(feetAndInch: String(format:"%0.f",newHeight))
        }
        
        return height
    }
    
    func setUserWeight(newWeight : Double) -> Double {
        var weight : Double = 0
        
        if self.userModel.height != nil {
            weight = self.userModel.metricPreference == MetricPreference.Metric ? newWeight :  NRConversionUtility.poundsToKil(valueInPounds:newWeight)
        }
        
        return weight
    }

    
    // MARK: UITableViewDataSource
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        let cellSection = self.profileSettingCells[section]
        switch cellSection {
        case .metricsCell:
            return metricsOptionArray.count
        case .activityCell:
            return activityOptionArray.count
        case .dietCell:
            return dietaryPreferenceOptionArray.count
        default:
            return 1
        }
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return self.profileSettingCells.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cellSection = self.profileSettingCells[indexPath.section]
        
        switch cellSection {
        case .nameCell:
            let cell : NRNameTableViewCell = tableView.dequeueReusableCell(withIdentifier: "kProfileNameCell") as! NRNameTableViewCell
            cell.setupWithNameModel(user: self.userModel)
            cell.selectionStyle = .none
            cell.updateImage.addTarget(self, action: #selector(addProfilePicture(sender:)), for: .touchUpInside)
            return cell
        case .genderCell :
            let cell : NRProfileGenderCellTableViewCell = tableView.dequeueReusableCell(withIdentifier: "kProfileGender") as! NRProfileGenderCellTableViewCell
        
            if self.userModel.biologicalSex != nil {
                cell.setupWithGender(sex: (self.userModel.biologicalSex?.descriptionForChat)!)
            }
            cell.maleButton.addTarget(self, action: #selector(genderChanged(sender:)), for:.touchUpInside)
            cell.femaleButton.addTarget(self, action: #selector(genderChanged(sender:)), for: .touchUpInside)

            cell.selectionStyle = .none
            return cell
        case  .ageCell:
            let cell : NRProfilePickerTableViewCell = tableView.dequeueReusableCell(withIdentifier: "kProfilePickerCell") as! NRProfilePickerTableViewCell
            cell.pickerView.tag = 10
            cell.pickerView.dataSource = self
            cell.pickerView.delegate = self
            cell.pickerView.reloadData()

            cell.selectionStyle = .none
            if userModel.age != nil {
                if let index = self.ageDataSource.index(of: userModel.age!) {
                    cell.pickerView.selectItem(index, animated: false, notifySelection: false)
                }
            }

            return cell
        case .metricsCell:
            let data = self.metricsOptionArray[indexPath.row]
            let cell : NRUnitsTableViewCell = tableView.dequeueReusableCell(withIdentifier: "kUnitsCell") as! NRUnitsTableViewCell
            cell.unitName.text = data.name
            cell.unitDesc.text = data.description
            let imageName = data.isSelected ? "radio_on_green" : "radio_off_green"
            cell.selectedImageView.image  = UIImage(named: imageName)
            cell.selectionStyle = .none
            return cell
        case .heightCell:
            let cell : NRProfilePickerTableViewCell = tableView.dequeueReusableCell(withIdentifier: "kProfilePickerCell") as! NRProfilePickerTableViewCell
            cell.pickerView.tag = 20
            cell.pickerView.delegate = self
            cell.pickerView.dataSource = self
            cell.selectionStyle = .none
            cell.pickerView.reloadData()

            if userModel.height != nil {
                let height = self.getUserHeight(newHeight: String(userModel.height!))
                if let index = self.heightDataSource.index(of: height) {
                    cell.pickerView.selectItem(index, animated: false, notifySelection: false)
                    }
                }
            return cell
        case .weightCell:
            let cell : NRProfilePickerTableViewCell = tableView.dequeueReusableCell(withIdentifier: "kProfilePickerCell") as! NRProfilePickerTableViewCell
            cell.pickerView.tag = 30
            cell.pickerView.delegate = self
            cell.pickerView.dataSource = self
            cell.pickerView.reloadData()
            cell.selectionStyle = .none
            if userModel.weight != nil {
                let weight = self.getUserWeight(newWeight: userModel.weight!)
                if let index = self.weightDataSource.index(of: weight) {
                    cell.pickerView.selectItem(index, animated: false, notifySelection: false)
                }
            }
            return cell
        case .dietCell:
            let data = self.dietaryPreferenceOptionArray[indexPath.row]
            let cell : NRListTableViewCell = tableView.dequeueReusableCell(withIdentifier: "kListTableCell") as! NRListTableViewCell
            cell.listName.text = data.name
            let imageName = data.isSelected ? "radio_on_green" : "radio_off_green"
            cell.slectedImage.image  = UIImage(named: imageName)
            cell.selectionStyle = .none
            return cell
        case .activityCell:
            let data = self.activityOptionArray[indexPath.row]
            let cell : NRListTableViewCell = tableView.dequeueReusableCell(withIdentifier: "kListTableCell") as! NRListTableViewCell
            cell.listName.text = data.name
            let imageName = data.isSelected ? "radio_on_green" : "radio_off_green"
            cell.slectedImage.image  = UIImage(named: imageName)
            cell.selectionStyle = .none

            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let cellSection = self.profileSettingCells[section]
        
        if (cellSection != .nameCell){
            let headerView = UIView(frame: CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: kHeaderHeight))
            headerView.backgroundColor  = UIColor.white
            let titleLabel = UILabel(frame: CGRect(x: 15, y: 20, width: 200, height: 20))
            let boldAttr: [String : Any] = [NSFontAttributeName : UIFont(name: "Campton-Bold", size: 18)!, NSForegroundColorAttributeName : NRColorUtility.hexStringToUIColor(hex: "303030")]
            let bookAttr: [String : Any] = [NSFontAttributeName : UIFont(name: "Campton-Book", size: 18)!, NSForegroundColorAttributeName : NRColorUtility.hexStringToUIColor(hex: "303030")]
            titleLabel.textColor = NRColorUtility.hexStringToUIColor(hex: "#303030")
            titleLabel.font = UIFont.init(name: "Campton-Bold", size: 18)
            
            if cellSection == .heightCell {
                let value = NSMutableAttributedString(string: cellSection.rawValue, attributes:boldAttr)
                
                if self.userModel.metricPreference != nil {
                    value.append(NSMutableAttributedString(string: (self.userModel.metricPreference?.isMetric)! ? String(format:" (Centimeters)") : String(format:" (Feet, Inches)"), attributes: bookAttr))
                }
                titleLabel.attributedText = value
            } else if cellSection == .weightCell {
                let value = NSMutableAttributedString(string: cellSection.rawValue, attributes:boldAttr)
                if self.userModel.metricPreference != nil {
                    value.append(NSMutableAttributedString(string: (self.userModel.metricPreference?.isMetric)! ? String(format:" (Kilogram)") : String(format:" (Pounds)"), attributes: bookAttr))
                }
                titleLabel.attributedText = value
            }else {
                titleLabel.text = cellSection.rawValue
            }

            headerView.addSubview(titleLabel)
            return headerView
        }
        return nil
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        let footerView = UIView(frame: CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: kHeaderHeight))
        footerView.backgroundColor  = UIColor.white
        let lineView = UIView(frame: CGRect(x: 0, y: 19, width: UIScreen.main.bounds.width, height: 1))
        lineView.backgroundColor = NRColorUtility.hexStringToUIColor(hex: "#efefef")
        footerView.addSubview(lineView)
        return footerView
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        let cellSection = self.profileSettingCells[section]
        
        if cellSection == .nameCell {
            return 0
        }
        return 50
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        /*let cellSection = self.profileSettingCells[section]
        
        if cellSection == .nameCell {
            return 15
        }*/
        return kFooterHeight
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        let cell = self.tableView(tableView, cellForRowAt: indexPath)
        return cell.frame.size.height
    }
    

    // MARK: UITableViewDelegate
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let cellSection = self.profileSettingCells[indexPath.section]

        if cellSection == .dietCell {
            let data = dietaryPreferenceOptionArray[indexPath.row]
            let newDietryPreference = DietaryPreference.enumFromString(string: data.name)!
            
            if newDietryPreference != self.userModel.dietaryPreference {
                self.dieteryPreferenceChanged(dietaryPreference: newDietryPreference)
                for index in 0...dietaryPreferenceOptionArray.count-1  {
                    dietaryPreferenceOptionArray[index].isSelected = false
                }
                dietaryPreferenceOptionArray[indexPath.row].isSelected = true
                tableView.reloadSections(IndexSet(integer: indexPath.section), with: .none)
            }
        }else if cellSection == .activityCell {
            let data = activityOptionArray[indexPath.row]
            let newActivityLevel = ActivityLevel.stringToDataSource(string: data.name.lowercased())!
            
            if newActivityLevel != self.userModel.activityLevel {
                self.activityLevelChanged(activityLevel: newActivityLevel)
                for index in 0...activityOptionArray.count-1  {
                    activityOptionArray[index].isSelected = false
                }
                activityOptionArray[indexPath.row].isSelected = true
                tableView.reloadSections(IndexSet(integer: indexPath.section), with: .none)
            }
        } else if cellSection == .metricsCell {
            let data = metricsOptionArray[indexPath.row]
            let newMetrics = MetricPreference.stringToDataSource(string: data.name.lowercased())!
            
            if newMetrics != self.userModel.metricPreference {
                self.metricPreferenceChanged(metricPreference: newMetrics)
                    for index in 0...metricsOptionArray.count-1  {
                        metricsOptionArray[index].isSelected = false
                }
                metricsOptionArray[indexPath.row].isSelected = true
                
            }
        }
    }

    // MARK: Horizontal Picker Delegates & DataSource
    // MARK: Picker Delegates and data source
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
    }
    
    func numberOfItemsInPickerView(_ pickerView: NRPickerView) -> Int {
        if (pickerView.tag == 10) {
            return self.ageDataSource.count
        } else if (pickerView.tag == 20) {
            return self.heightDataSource.count
        } else if (pickerView.tag == 30) {
            return self.weightDataSource.count
        }
        return 0
    }
    
    func pickerView(_ pickerView: NRPickerView, titleForItem item: Int) -> String {
        if (pickerView.tag == 10) {
            if item >= self.ageDataSource.count {
                return ""
            }
            return String(self.ageDataSource[item])
        } else if (pickerView.tag == 20) {
            if item >= self.heightDataSource.count {
                return ""
            }
            return self.heightDataSource[item]
        } else if (pickerView.tag == 30) {
            if item >= self.weightDataSource.count {
                return ""
            }
            return String(self.weightDataSource[item])
        }
        return ""
    }
    
    func pickerView(_ pickerView: NRPickerView, didSelectItem item: Int) {
        if pickerView.tag == 10 {
            let age = Int(self.ageDataSource[item])
            if age != self.userModel.age  {
            self.ageChanged(age: age)
            }
        } else if pickerView.tag == 20 {
            if self.userModel.height != Double(self.heightDataSource[item]) {
                let newHeight : Double = self.userModel.metricPreference == MetricPreference.Metric ? Double(self.setUserHeight(newHeight: Double(self.heightDataSource[item])!)) : NRConversionUtility.feetAndInchToMeters(feetAndInch: self.heightDataSource[item])
                self.heightChanged(height: newHeight)
            }
        }   else if pickerView.tag == 30 {
                if self.userModel.weight != Double(self.weightDataSource[item]) {
                    let weight = self.setUserWeight(newWeight:Double(self.weightDataSource[item])!)
                    self.weightChanged(weight:weight)
            }
        }
    }
    
    func genderChanged(sender:UIButton) {
        let cell = sender.superview?.superview as! NRProfileGenderCellTableViewCell
        
        if sender == cell.maleButton {
            self.biologicalSexChanged(biologicalSex:BiologicalSex.Male)
        } else {
            self.biologicalSexChanged(biologicalSex:BiologicalSex.Female)
        }
        
        self.userModelView?.reloadCellSection(sections: IndexSet(integer: 1))
    }
    
    func addProfilePicture(sender:UIButton) {
        self.userModelView?.chooseImageFromGallery()
    }
}

//
//  MealPlannerCacheDataStore.swift
//  Nourish
//
//  Created by Gena Mironchyk on 9/12/17.
//  Copyright © 2017 SSIC. All rights reserved.
//

import UIKit
import RealmSwift

class MealPlannerCacheDataStore: NSObject {
    func store(cache : MealPlannerCache) {
    
        let realm = try! Realm()
        let predicate = NSPredicate(format: "date == %@", cache.date! as NSDate)
        let objs = realm.objects(MealPlannerCache.self).filter(predicate)
        if objs.count > 0 {
            try! realm.write {
                realm.delete(objs, cascading: true)
            }
        }
        try! realm.write {
            realm.add(cache)
        }
        print("Store weekly plan completed")

    }
    
    func getMealPlanCacheFor(date : Date) -> MealPlannerCache? {
        let realm = try! Realm()
        let predicate = NSPredicate(format: "date == %@", Calendar.current.startOfDay(for: date) as NSDate)
        return realm.objects(MealPlannerCache.self).filter(predicate).first
    }
    
    func changePreferenceMealPlan(date: Date, lunch: Int, dinner: Int, breakfast: Int, snacks: Int) {
        let realm = try! Realm()
        let predicate = NSPredicate(format: "date == %@", Calendar.current.startOfDay(for: date) as NSDate)
        let objs = realm.objects(MealPlannerCache.self).filter(predicate)
        if objs.count > 0 {
            let mealPlan = objs.first
            try!  realm.write {
                mealPlan?.breakfast = breakfast
                mealPlan?.lunch = lunch
                mealPlan?.dinner = dinner
                mealPlan?.snacks = snacks
            }
        }
    }
    
    
    func removeAllCache () {
        let realm = try! Realm()
        let objs = realm.objects(MealPlannerCache.self)
        if objs.count > 0 {
            try! realm.write {
                realm.delete(objs, cascading: true)
            }
        }
        print("Weekly plan deleted")

    }
}

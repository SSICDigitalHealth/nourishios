//
//  UserOcasionDetailView.swift
//  Nourish
//
//  Created by Vlad Birukov on 19.06.17.
//  Copyright © 2017 SSIC. All rights reserved.
//

import UIKit

protocol UserOcasionDetailProtocol : BaseViewProtocol{
    func reloadTable()
    func registerTableViewCells()
    func config() -> ProgressConfig?
}

class UserOcasionDetailView: BaseView, UserOcasionDetailProtocol {
    @IBOutlet weak var detailTableView: UITableView!
    @IBOutlet var presenter: UserOcasionDetailPresenter!
    
    var progressConfig: ProgressConfig?
    
    func config() -> ProgressConfig? {
        return self.progressConfig
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.basePresenter = presenter
        self.presenter.userOcasionDetailView = self
        registerTableViewCells()
        settingTableCell()
        
        super.viewWillAppear(animated)
    }
    
    func stopAnimation() {
        self.stopActivityAnimation()
    }
    
    func reloadTable() {
        detailTableView.reloadData()
    }
    
    func registerTableViewCells() {
        self.detailTableView?.register(UINib(nibName: "HeaderUserOcasionTableViewCell", bundle: nil), forCellReuseIdentifier: "cellHeaderUserOcasion")
        self.detailTableView?.register(UINib(nibName: "UserOcasionTableViewCell", bundle: nil), forCellReuseIdentifier: "cellUserOcasion")
    }
    
    func settingTableCell() {
        detailTableView.rowHeight = UITableViewAutomaticDimension
        detailTableView.estimatedRowHeight = 30
    }
}

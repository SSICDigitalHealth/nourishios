//
//  MealPlannerCache_to_OptifastMealPlannerModel.swift
//  Optifast
//
//  Created by Gena Mironchyk on 1/9/18.
//  Copyright © 2018 SSIC. All rights reserved.
//

import UIKit

class MealPlannerCache_to_OptifastMealPlannerModel: NSObject {
    
    func transform(cache : MealPlannerCache) -> OptifastMealPlannerModel {
        let model = OptifastMealPlannerModel()
        model.targetCalories = cache.totalCalories
        model.score = cache.score
        model.date = cache.date
        if cache.optifastMealsData != nil {
            let optifastArray = self.transformToArray(data: cache.optifastMealsData!)
            for optiFood in optifastArray {
                model.optifastFoods.append(self.foodSearchModelFrom(dict: (optiFood as? [String: Any] ?? [String: Any]())))
            }
        }
        model.foods = self.foodsFrom(ocasionDataArray: Array(cache.ocasionData))
        
        return model
    }
    
    
    
    
    private func foodsFrom(ocasionDataArray : [OcasionData]) -> [OptifastMealModel] {
        var arrayToReturn = [OptifastMealModel]()
        for data in ocasionDataArray {
            let model = OptifastMealModel()
            model.totalCalories = data.calsTotal
            model.occasion = Ocasion.enumFromMealPlanner(string: data.ocasionName?.stringValue ?? "")
            if data.mealsData != nil {
                let foodsDict = self.transformToDictionary(data: data.mealsData!)
                for index in 0..<foodsDict.count {
                    model.ocasionFoods.append((meal: self.foodSearchModelFrom(dict: foodsDict[index]), mealReciepe : self.recipeFrom(model: data.recipiesData[index])))
                }
            }
            arrayToReturn.append(model)
        }
        
        
        return arrayToReturn
    }
    
    
    private func foodSearchModelFrom(dict: [String : Any]) -> foodSearchModel {
        var model = foodSearchModel()
        model.amount = dict["amount"] as? Double ?? 0.0
        model.unit = dict["unit"] as? String ?? ""
        model.foodId = dict["food_id"] as? String ?? ""
        model.mealIdString = dict["food_id"] as? String ?? ""
        model.foodTitle = dict["food_name"] as? String ?? ""
        model.grams = dict["grams"] as? Double
        model.caloriesPerGram = dict["kcal_per_gram"] as? Double ?? 0.0
        model.calories = String((dict["kcal_per_gram"] as? Double ?? 0.0) * (dict["grams"] as? Double ?? 0.0))
        return model
    }
    
    private func transformToDictionary(data : Data) -> [[String : Any]] {
        return try! JSONSerialization.jsonObject(with: data, options: .mutableLeaves) as? [[String : Any]] ?? [[String: Any]]()
    }
    
    private func transformToArray(data: Data) -> [Any] {
        return try! JSONSerialization.jsonObject(with: data, options: .mutableLeaves) as? [Any] ?? [Any]()
    }
    
    private func recipeFrom(model: RecipiesCacheModel) -> RecipiesModel? {
        let recipiesModel = RecipiesModel()
        recipiesModel.foodId = model.foodId
        recipiesModel.amount = model.amount
        recipiesModel.nameFood = model.nameFood
        
        if model.ingredient.count > 0 {
            for stringObj in model.ingredient {
                recipiesModel.ingredient.append(stringObj.stringValue)
            }
        } else {
            return nil
        }
        
        
        if model.groupId != nil {
            recipiesModel.groupId = model.groupId
        }
        
        if model.pathImage != "" {
            recipiesModel.image = self.loadImageFromPath(path: model.pathImage)
        }
        
        recipiesModel.numberCal = model.numberCal
        recipiesModel.grams = model.grams
        recipiesModel.unit = model.unit
        
        if model.instruction.count > 0 {
            for stringObj in model.instruction {
                recipiesModel.instructions.append(stringObj.stringValue)
            }
        }
        
        recipiesModel.isFavorite = model.isFavorite
        
        return recipiesModel
    }
    
    private func loadImageFromPath(path: String) -> UIImage? {
        var imageFood: UIImage?
        
        let documentsURL = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first!
        let filePath = documentsURL.appendingPathComponent(path).path
        
        if FileManager.default.fileExists(atPath: filePath) {
            imageFood = UIImage(contentsOfFile: filePath)
        }
        
        return imageFood
    }
}

//
//  BackgroundChangeScoreFetcher.swift
//  Nourish
//
//  Created by Nova on 7/19/17.
//  Copyright © 2017 SSIC. All rights reserved.
//

import UIKit
import RxSwift

let kDiaryReFetched = "diaryReFetched"
class BackgroundChangeScoreFetcher: NSObject {
    static let shared = BackgroundChangeScoreFetcher()
    private var disposeBag = DisposeBag()
    var mealdiaryInteractor : MealDiaryInteractor?
    
    let recommendationInteractor = NRRecommendationInteractor()
    
    private func recreateDisposeBag () {
        self.disposeBag = DisposeBag()
    }
    
    private func getMealInteractor() -> MealDiaryInteractor {
        return (self.mealdiaryInteractor != nil) ? self.mealdiaryInteractor! : MealDiaryInteractor()
    }
    
    
    func executeFor(date : Date) {
        self.recreateDisposeBag()
        let token = NRUserSession.sharedInstance.accessToken
       // let backgroundQueue = DispatchQueue(label: "com.backgroundfethcer", qos: .background)
        
        var isOnBoardingFinished = true
        
        if defaults.object(forKey: onBoardingKey) != nil {
            isOnBoardingFinished = defaults.bool(forKey: onBoardingKey)
        } else {
            isOnBoardingFinished = false
        }
        
        if token != nil && isOnBoardingFinished == true {
            self.mealdiaryInteractor = self.getMealInteractor()
            let _ = self.mealdiaryInteractor?.arrayOfFetchedMealsForDate(date: date, onlyCache: false).subscribe(onNext: {models in }, onError: {error in
                LogUtility.logToFile("BG diary failed \(error)")
            }, onCompleted: {
                LogUtility.logToFile("BG diary finished")
            }, onDisposed: {}).addDisposableTo(self.disposeBag)
            
            let mealRepo = MealHistoryRepository.shared
            var counter = 0
            
            let _ = mealRepo.getFoodFor(date: Date(), onlyCache: false).subscribe(onNext: {models in
                counter += 1
            }, onError: {error in
                LogUtility.logToFile("BG diary failed \(error)")
            }, onCompleted: {
                if counter > 0 {
                    print("updated")
                    let notificationName = Notification.Name(kDiaryReFetched)
                    NotificationCenter.default.post(name: notificationName, object: nil)
                }
                LogUtility.logToFile("BG diary finished")
            }, onDisposed: {}).addDisposableTo(self.disposeBag)
            
            if self.needToBeRefreshed(date: defaults.object(forKey: kUserCacheRefreshDateKey) as? Date) == true {
                let userRepo = UserRepository.shared
                let _ = userRepo.getCurrentUser(policy: .ForceReload).subscribe(onNext: {userProfile in
                    LogUtility.logToFile("BG User cache updated")
                    defaults.set(Date(), forKey: kUserCacheRefreshDateKey)
                    
                }, onError: {error in
                    LogUtility.logToFile("BG User cache update failed")
                }, onCompleted: {}, onDisposed: {}).addDisposableTo(self.disposeBag)
            }
            
            
            
            if NRUserSession.sharedInstance.accessToken != nil {
                let nestleProgressRepo = NestleProgressRepository.shared
                let _ = nestleProgressRepo.refetchProgressFor(startDate: date, endDate: date, returningHistory: true).subscribe(onNext: {progress in }, onError : {error in
                    LogUtility.logToFile("BG Progress failed")
                }, onCompleted: {
                    LogUtility.logToFile("BG Progress finished")

                }, onDisposed:{}).addDisposableTo(self.disposeBag)
                
                let nestleRecomendationsRepo = NestleRecommendationsRepository()
                
                
                nestleRecomendationsRepo.refetchRecomendationFor(date: date)
                
                let _ = nestleRecomendationsRepo.refetchRecommendationsForToday().subscribe(onNext: {model in }, onError: {error in }, onCompleted: {}, onDisposed: {}).addDisposableTo(self.disposeBag)
            }
        }
    }
    
    private func needToBeRefreshed(date : Date?) -> Bool {
        var returnValue = true
        
        if date != nil {
            returnValue = !Calendar.current.isDateInToday(date!)
        }
        
        return returnValue
    }
    
}

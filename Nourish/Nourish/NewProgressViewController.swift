//
//  NewProgressViewController.swift
//  Nourish
//
//  Created by Vlad Birukov on 06.06.17.
//  Copyright © 2017 SSIC. All rights reserved.
//

import UIKit

func DEBUG(_ message: String,
         function: String = #function,
         file: String = #file,
         line: Int = #line) {
    
    print("DEBUG: \"\(message)\" (File: \(file), Function: \(function), Line: \(line))")
}

enum ProgressPeriod {
    case daily
    case weekly
    case monthly

    
    var watchOut: (nibName: String, header: String, height: CGFloat) {
        switch self {
        case .daily:
            return ("WatchOutDailyView", "Watch out for", 166)
        case .weekly:
            return ("WatchOutWeeklyView", "Watch out for", 438)
        case .monthly:
            return ("WatchOutWeeklyView", "Watch out for", 438)
        }
    }
    
    var scoreCardTitle: String {
        switch self {
        case .daily:
            return "Daily score"
        case .weekly:
            return "Weekly score"
        case .monthly:
            return "Monthly score"
        }
    }
}

enum ProgressType {
    case progress
    case report
    case planer
    
    var period: Int {
        switch self {
        case .report:
            return -1
        case .progress:
            return 1
        default:
            return 0
        }
    }
}

enum ProgressState {
    case dashboard
    case card
}

enum ProgressCard {
    case hydration
    case logged
    case calories
    case nutrition
    case watchOut
    case score
    case recomendation
    
    var nibName: String {
        switch self {
        case .hydration:
            return "HydrationBalanceViewController"
        case .logged:
            return "UserOcasionDetailViewController"
        case .calories:
            return "CaloriesDetailViewController"
        case .nutrition:
            return "NutritionalDetailViewController"
        case .watchOut:
            return "MicroelementDetailViewController"
        case .score:
            return "UserScoreViewController"
        case .recomendation:
            return "RecomendationViewController"
        }
    }
    
    func cardFactory(config: ProgressConfig) -> BaseCardViewController {
        switch self {
        case .hydration:
            return HydrationBalanceViewController(_: config)
        case .calories:
            return CaloriesDetailViewController(_: config)
        case .nutrition:
            return NutritionalDetailViewController(_: config)
        case .watchOut:
            switch config.period {
            case .daily:
                return MicroelementDetailViewController(_: config)
            default:
                return MicroelementDetailPeriodViewController(_: config)
            }
        case .score:
            return UserScoreViewController(_: config)
        case .recomendation:
            return RecomendationViewController(_: config)
        default:
            return BaseCardViewController()
        }
    }
}

class ProgressConfig {
    var date = Date()
    var period: ProgressPeriod = .daily
    var type: ProgressType = .progress
    var state: ProgressState = .dashboard
    var card: ProgressCard = .score
    
    init(date: Date, period: ProgressPeriod, type: ProgressType, state: ProgressState, card: ProgressCard) {
        self.date = date
        self.card = card
        self.state = state
        self.type = type
        self.period = period
    }
    
    static func != (left: ProgressConfig, right: ProgressConfig) -> Bool {
        return !(
            left.card == right.card &&
            left.period == right.period &&
            left.state == right.state &&
            left.type == right.type)
    }
    
    func getDate() -> (start: Date, end: Date) {
            var startDate: Date
            var endDate: Date
            let calendar = Calendar.current
            if calendar.isDateInToday(self.date) {
                switch self.period {
                case .daily:
                    startDate = self.date
                    endDate = self.date
                case .weekly:
                    startDate = calendar.date(byAdding: .day, value: -6, to: self.date)!
                    endDate = self.date
                case .monthly:
                    startDate = calendar.date(byAdding: .day, value: -29, to: self.date)!
                    endDate = self.date
                }
            }
            else {
                switch self.period {
                case .daily:
                    startDate = self.date
                    endDate = self.date
                case .weekly:
                    startDate = self.date
                    endDate = calendar.date(byAdding: .day, value: 6, to: self.date)!
                case .monthly:
                    startDate = self.date
                    endDate = calendar.date(byAdding: .month, value: 1, to: self.date)!
                    endDate = calendar.date(byAdding: .day, value: -1, to: endDate)!
                }
            }
            
            return(startDate, endDate)
    }
    
    func recomendationTitle() -> String {
        let calendar = Calendar.current
        if calendar.isDateInToday(self.date) {
            switch self.period {
            case .daily:
                return "For your next meal"
            case .weekly:
                return "What to eat next"
            case .monthly:
                return "What to eat next"
            }
        }
        else {
            switch self.period {
            case .daily:
                return "For your next day"
            case .weekly:
                return "For your next week"
            case .monthly:
                return "For your next month"
            }
        }
    }
}

enum periodType: Int {
    case byDays = 1
    case byWeeks
    case byMonths
}

class NewProgressViewController: BasePresentationViewController, DatePickerProtocol, NRSegmentedControlDelegate {
    @IBOutlet weak var userActivityScore: UserActivityScoreView!
    @IBOutlet weak var userCaloriesView: UserCaloriesView!
    @IBOutlet weak var nutritionalBalanceView: NutritionalBalanceView!
    @IBOutlet weak var microelementBalanceView: MicroelementBalanceView!
    @IBOutlet weak var userCurrenOcasionView: LoggedUserEatStateView!
    @IBOutlet weak var hydrationView: HydrationView!
    @IBOutlet weak var recomendationsView: RecomendationView!
    @IBOutlet weak var caloriesChartView: CaloriesChartView!
    @IBOutlet weak var caloriesDailyAverageView: CaloriesDailyAverageView!
    @IBOutlet weak var progressNavigationView: ActivityNavigationBarView!
    @IBOutlet weak var nutrientsView: NutrientsView!


    @IBOutlet weak var UserActivityScoreHeight: NSLayoutConstraint!
    @IBOutlet weak var recomendationHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var watchOutHeight: NSLayoutConstraint!
    @IBOutlet weak var heightDailyCalories: NSLayoutConstraint!
    @IBOutlet weak var heightWeeklyMonthlyCalories: NSLayoutConstraint!
    @IBOutlet weak var caloricDailyAverageHeight: NSLayoutConstraint!
    @IBOutlet weak var nutrientsHeight: NSLayoutConstraint!
    @IBOutlet weak var userCurrenOcasionHeight: NSLayoutConstraint!
    
    @IBOutlet weak var datePickerHolder: UIView!
    var datePickerView: DatePickerView?
    
    var compositeDate: (daily: Date, weekly: Date, monthly: Date) = (Date(), Date(), Date())
    
    var navigationPicker: UIView!
    
    var listOfViews = [ProgressView]()
    
    var currentConfig = ProgressConfig(date: Date(), period: .daily, type: .progress, state: .dashboard, card: .score)
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.listOfViews = [userActivityScore, hydrationView, userCaloriesView, caloriesChartView, caloriesDailyAverageView, microelementBalanceView, nutritionalBalanceView, userCurrenOcasionView, recomendationsView]
        
        self.baseViews = [progressNavigationView, userActivityScore, userCaloriesView,caloriesChartView, caloriesDailyAverageView,nutritionalBalanceView, microelementBalanceView, userCurrenOcasionView, hydrationView, recomendationsView, nutrientsView]
        
        for view in self.baseViews! {
            view.viewDidLoad()
        }
        self.datePickerHolder.setupContent(&self.datePickerView, nibName: "DatePickerView")
        self.datePickerView?.setupWith(deligate: self, schema: .progress)
        self.progressNavigationView.title.text = "Nutrition"
        self.progressNavigationView.segmentedControl.delegate = self
        self.nutrientsView.progressViewController = self
        settingShowView(period: self.currentConfig.period)
        switchState(config: currentConfig)
    }
    
    func onChange(datePicker: DatePickerView, newDate: Date) {
        switch self.currentConfig.period {
        case .daily:
            self.compositeDate.daily = newDate
        case .weekly:
            self.compositeDate.weekly = newDate
        case .monthly:
            self.compositeDate.monthly = newDate
        }
        self.switchState(config: ProgressConfig(date: newDate,
                                                period: self.currentConfig.period,
                                                type: datePicker.isToday() ? .progress : .report,
                                                state: .dashboard,
                                                card: .score))
    }
    
    func resetForTodayDailyNutrition() {
        DispatchQueue.main.async {
            if let picker = self.datePickerView {
                picker.setupDate(date: Date())
                picker.setupPeriod(period: .daily)
            }
            self.progressNavigationView.segmentedControl.selectItemAtIndex(index: 0, withAnimation: true)

            self.switchState(config: ProgressConfig(date: Date(),
                                                    period: .daily,
                                                    type: .progress,
                                                    state: .dashboard,
                                                    card: .score))
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.setupNavigationBar()
        NotificationCenter.default.addObserver(self, selector:#selector(resetForTodayDailyNutrition), name:.NSCalendarDayChanged, object:nil)
    }

    override func viewWillDisappear(_ animated: Bool) {
        NotificationCenter.default.removeObserver(self, name: .NSCalendarDayChanged, object: nil)
        super.viewWillDisappear(animated)
    }
   
    func switchState(config: ProgressConfig) {
        if config != currentConfig {
            currentConfig = config
        }
        currentConfig.date = config.date
        
        switch config.state {
        case .card:
            let controller = config.card.cardFactory(config: config)
            presentController(statsController: controller)
        case .dashboard:
            self.loadContent()
        }
    }
    
    func loadContent () {        
        for view : ProgressView in self.listOfViews {
            view.tapHook = {
                self.switchState(config: ProgressConfig(date: self.currentConfig.date,
                                                        period: self.currentConfig.period,
                                                        type: self.currentConfig.type,
                                                        state: .card,
                                                        card: $0))
            }
            view.updateCardHeightHook = {
                switch $1 {
                case .score:
                    self.UserActivityScoreHeight.constant = $0
                case .recomendation:
                    self.recomendationHeightConstraint.constant = $0
                case .watchOut:
                    self.watchOutHeight.constant = $0
                default: break
                }
            }
            view.renderView(currentConfig)
            view.viewWillAppear(true)
        }
        nutrientsView.progressConfig = currentConfig
        nutrientsView.viewWillAppear(true)
    }
    
    func presentController(statsController controller: BasePresentationViewController) {
        self.navigationController?.pushViewController(controller, animated: true)
        NavigationUtility.hideTabBar(animated: false)
    }
    
    func segmentedControlDidPressedItemAtIndex(segmentedControl: NRSegmentedControl, index: Int) {
            switch index {
            case 1:
                self.switchState(config: ProgressConfig(date: self.compositeDate.weekly, period: .weekly, type: currentConfig.type, state: .dashboard, card: .score))
                self.settingShowView(period: .weekly)
                self.datePickerView?.setupPeriod(period: .weekly)
                self.datePickerView?.setupDate(date: self.compositeDate.weekly)
                EventLogger.logOnNutritionScreenClickFor(period:.weekly)

            case 2:
                self.switchState(config: ProgressConfig(date: self.compositeDate.monthly, period: .monthly, type: currentConfig.type, state: .dashboard, card: .score))
                self.settingShowView(period: .monthly)
                self.datePickerView?.setupPeriod(period: .monthly)
                self.datePickerView?.setupDate(date: self.compositeDate.monthly)
                EventLogger.logOnNutritionScreenClickFor(period:.monthly)
            default:
                self.switchState(config: ProgressConfig(date: self.compositeDate.daily, period: .daily, type: currentConfig.type, state: .dashboard, card: .score))
                self.settingShowView(period: .daily)
                self.datePickerView?.setupPeriod(period: .daily)
                self.datePickerView?.setupDate(date: self.compositeDate.daily)
                EventLogger.logOnNutritionScreenClickFor(period:.daily)
            }
    }
    
    private func setupNavigationBar() {
        self.navigationController?.setNavigationBarHidden(true, animated: false)
        self.automaticallyAdjustsScrollViewInsets = false

        let shadowPath = UIBezierPath(rect: CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: self.progressNavigationView.frame.height))
        self.progressNavigationView.layer.shadowColor = UIColor.darkGray.cgColor
        self.progressNavigationView.layer.shadowOffset = CGSize(width: 0.0, height: 1.0)
        self.progressNavigationView.layer.shadowOpacity = 0.5
        self.progressNavigationView.layer.shadowRadius = 2.0
        self.progressNavigationView.layer.masksToBounds =  false
        self.progressNavigationView.layer.shadowPath = shadowPath.cgPath
    }

    func setupRecomendationHeightConstraint(_ newHeight: CGFloat) {
        self.recomendationHeightConstraint.constant = 80.0 + newHeight
    }
    
    private func settingShowView(period: ProgressPeriod) {
        switch period {
        case .daily:
            caloriesChartView.isHidden = true
            caloriesChartView.hideView()
            
            caloriesDailyAverageView.isHidden = true
            caloriesDailyAverageView.hideView()

            caloricDailyAverageHeight.constant = 0
            heightWeeklyMonthlyCalories.constant = 0
            heightDailyCalories.constant = 189
            userCurrenOcasionHeight.constant = 229
            nutrientsHeight.constant = 265
            
            userCaloriesView.isHidden = false
            userCaloriesView.showView()
            
            userCurrenOcasionView.isHidden = false
            userCurrenOcasionView.showUserCurrenOcasionView()
        default:
            userCaloriesView.isHidden = true
            userCaloriesView.hideView()
            
            userCurrenOcasionView.isHidden = true
            userCurrenOcasionView.hideView()
            
            nutrientsHeight.constant = 265
            caloricDailyAverageHeight.constant = 63.5
            heightWeeklyMonthlyCalories.constant = 155
            userCurrenOcasionHeight.constant = 0
            heightDailyCalories.constant = 0
            
            caloriesChartView.isHidden = false
            caloriesChartView.showCaloricChart()
            
            caloriesDailyAverageView.isHidden = false
            caloriesDailyAverageView.showView(caloricAverage: true)
        }
        self.view.layoutIfNeeded()
    }
}

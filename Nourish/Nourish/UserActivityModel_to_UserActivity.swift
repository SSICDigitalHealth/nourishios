//
//  UserActivityModel_to_UserActivity.swift
//  Nourish
//
//  Created by Nova on 11/21/16.
//  Copyright © 2016 SSIC. All rights reserved.
//

import UIKit

class UserActivityModel_to_UserActivity: NSObject {
    func transform(model : UserActivityModel) -> UserActivity {
        let object = UserActivity()
        object.startDate = model.startDate
        object.endDate = model.endDate
        object.activityType = model.activityType
        return object
    }
    
    func transform(modelArray : [UserActivityModel]) -> [UserActivity] {
        var arrayToReturn : [UserActivity] = []
        for model in modelArray {
            arrayToReturn.append(self.transform(model: model))
        }
        return arrayToReturn
    }
}

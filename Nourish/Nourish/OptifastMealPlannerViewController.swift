//
//  OptifastMealPlannerViewController.swift
//  Optifast
//
//  Created by Gena Mironchyk on 1/9/18.
//  Copyright © 2018 SSIC. All rights reserved.
//

import UIKit

class OptifastMealPlannerViewController: BasePresentationViewController {

    @IBOutlet weak var mealPlannerNavigationBarView: MealPlannerNavigationBarView!
    @IBOutlet weak var optifastMealPlannerView : OptifastMealPlannerView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.baseViews = [optifastMealPlannerView, mealPlannerNavigationBarView]
        for v in self.baseViews! {
            v.viewDidLoad()
        }
        self.optifastMealPlannerView.presenter.optifastMealPlannerViewController = self
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        NavigationUtility.showTabBar(animated: false)
        self.automaticallyAdjustsScrollViewInsets = false
        self.setUpNavigationBar()
    }
    
    func setUpNavigationBar() {
        if self.navigationController != nil {
            self.navigationController?.setNavigationBarHidden(true, animated: false)
            self.mealPlannerNavigationBarView.resetButton.addTarget(self, action: #selector(showResetView), for: .touchUpInside)
        }
    }
    
    func showResetView(sender:UIButton!) {
        self.optifastMealPlannerView.presenter.showResetView()
    }
    
    


}

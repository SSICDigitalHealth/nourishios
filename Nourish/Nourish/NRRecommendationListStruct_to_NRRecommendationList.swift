//
//  NRRecommendationListStruct_to_NRRecommendationList.swift
//  Nourish
//
//  Created by Nova on 1/11/17.
//  Copyright © 2017 SSIC. All rights reserved.
//

import UIKit

class NRRecommendationListStruct_to_NRRecommendationList: NSObject {
    
    func transform(str : recommendationListStruct) -> recommendationList {
        let recArray = self.transformRecs(recs: str.recommendation)
        return recommendationList(type: str.type, recommendation : recArray)
    }
    
    func transform(arr : [recommendationListStruct]) -> [recommendationList] {
        var array : [recommendationList] = []
        for rec in arr {
            array.append(self.transform(str: rec))
        }
        return array
    }
    
    private func transformRec(rec : recommendationStruct) -> recommendation {
        return recommendation(description: rec.description, title: rec.title, subTitle: rec.subTitle, type: rec.type, foodModel: rec.foodModel)
    }
    
    private func transformRecs(recs : [recommendationStruct]) -> [recommendation] {
        var arrayRecs : [recommendation] = []
        for rec in recs {
            arrayRecs.append(self.transformRec(rec: rec))
        }
        return arrayRecs
    }

}

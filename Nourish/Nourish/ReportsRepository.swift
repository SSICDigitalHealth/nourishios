//
//  ReportsRepository.swift
//  Nourish
//
//  Created by Nova on 3/23/17.
//  Copyright © 2017 SSIC. All rights reserved.
//

import UIKit
import RxSwift

class ReportsRepository: NSObject {
    let activityRepo = UserActivityRepository()
    let mealRepo = MealHistoryRepository.shared
    let weightSome = WeightLossMeterInteractor()
    let userRepo = UserRepository.shared
    
    func getReportFor(date : Date) -> Observable<DailyReportModel> {
        
        let steps = activityRepo.getStepsStatisticsFor(date: date)
        let sleep = activityRepo.getSleepingFor(date: date)
        let distance = activityRepo.getDistanceFor(date: date)
        let workouts = activityRepo.getWorkoutsTimeFor(date: date)
        let nutrients = mealRepo.getFoodScoreFor(date: date)
        let budget = weightSome.getWeightLossMeterFor(date: date)
        let user = userRepo.getCurrentUser(policy: .DefaultPolicy)
        
        let zip = Observable.zip(steps, sleep, distance, workouts,nutrients,budget,user, resultSelector : { stepsValue, sleepValue, distanceValue, workoutsValue, nutrientsValue, budgetModel, userProf -> DailyReportModel in
            
            let model = DailyReportModel()
            
            let stepsCount = stepsValue.reduce(0, +)
            let nutrsArray = nutrientsValue.first?.nutrArray
            
            var activityArray : [activityTime] = []
            
            if stepsValue.count > 0 {
                for index in 0...stepsValue.count-1 {
                    let object = stepsValue[index]
                    activityArray.append(activityTime(activeHr : object, periodString : String(format : "%lu",index)))
                }
            }
            
            
            if nutrsArray != nil && (nutrsArray?.count)! > 0 {
                model.tooMuchNutrients = nutrsArray?.filter { $0.flag == kMuchFlag }
                model.tooLittleNutrients = nutrsArray?.filter { $0.flag == kLittleFlag }
                model.justRightNutrients = nutrsArray?.filter { $0.flag == kNormalFlag }
            } else {
                model.tooMuchNutrients = []
                model.tooLittleNutrients = []
                model.justRightNutrients = []
            }
            
            model.steps = stepsCount
            model.distance = distanceValue
            model.sleep = sleepValue
            model.activityArray = []
            model.weightProgress = budgetModel
            model.activeHours = workoutsValue
            model.activityArray = activityArray
            var isMetric = false
            model.goal = userProf.userGoal!
            
            if userProf.metricPreference != nil {
                isMetric = (userProf.metricPreference?.isMetric)!
            }
            model.isMetric = isMetric
            
            return model
        })
        return zip
    }
    
    func fetchReports() -> Observable<[reportsModel]> {
        let reports = activityRepo.firstActivityDate().flatMap { date -> Observable<[reportsModel]> in
            var arr : [reportsModel] = []
            
            if date != nil {
                let dates = self.datesArrayFromStart(date: date!)
                arr.append(contentsOf: self.reportsFromDates(dates: dates))
            }
            return Observable.just(arr.reversed())
        }
        return reports

    }
    
    private func datesArrayFromStart(date : Date) -> [Date] {
        let cal = Calendar.current
        var comps = DateComponents()
        comps.day = 1
        
        var array : [Date] = []
        
        var dateL : Date = date
        while dateL < cal.startOfDay(for: Date()) {
            array.append(dateL)
            dateL = cal.date(byAdding: comps, to: dateL)!
        }
        return array
    }
    
    private func reportsFromDates(dates : [Date]) -> [reportsModel] {
        var modelsArray : [reportsModel] = []
        for date in dates {
            modelsArray.append(reportsModel(date : date, report : nil))
        }
        return modelsArray
    }
    
}

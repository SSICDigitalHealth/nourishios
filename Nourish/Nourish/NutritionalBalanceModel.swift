//
//  NutritionalBalanceModel.swift
//  Nourish
//
//  Created by Vlad Birukov on 06.06.17.
//  Copyright © 2017 SSIC. All rights reserved.
//

import Foundation

enum NutritionType : Int {
    case grains = 0
    case fruits
    case vegs
    case protein
    case dairy
    
    static func description(type:NutritionType) -> String {
        switch (type) {
        case .fruits:
            return "Fruit"
        case .vegs:
            return "Veg"
        case .grains:
            return "Grains"
        case .protein:
            return "Protein"
        case .dairy:
            return "Dairy"
        }
    }
    
    static func stringToType(string: String) -> NutritionType{
        switch string {
        case "Fruit_total":
            return .fruits
        case "Veg_total":
            return .vegs
        case "Grain_refined":
            return .grains
        case "Grain_total":
            return .grains
        case "Prot_total":
            return .protein
        case "Dairy_total":
            return .dairy
        default:
            return .fruits
        }
    }
    
    static func nutritionUnit(type: NutritionType) -> String {
        switch type {
        case .grains:
            return "oz"
        case .protein:
            return "oz"
        default:
            return "cup"
        }
    }
}

typealias nutritionalList = (type: NutritionType, nutrition: UserNutrition)
class NutritionalBalanceModel: NSObject {
    var data: [nutritionalList]?
}

class UserNutrition {
    var consumedNutritional: Double = 0.0
    var targetNutritional: Double = 0.0
    var unit: String = ""
    
    init() {
    }
    
    init(consumed: Double, target: Double, unit: String) {
        self.consumedNutritional = consumed
        self.targetNutritional = target
        self.unit = unit
    }

}

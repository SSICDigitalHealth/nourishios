//
//  ActivityBaseTableViewCell.swift
//  Nourish
//
//  Created by Andrey Zmushko on 8/24/17.
//  Copyright © 2017 SSIC. All rights reserved.
//

import UIKit

class ActivityBaseTableViewCell: ActivityBaseCell {

    @IBOutlet weak var iconFrame: UIView!
    @IBOutlet weak var iconImageView: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var valueLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var descriptionValueLabel: UILabel!
    
    @IBOutlet weak var separatorView: UIView!
    @IBOutlet weak var bottomGap: NSLayoutConstraint!    
    
    @IBOutlet weak var baseView: BaseView!

    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.iconFrame.layer.cornerRadius = self.iconFrame.bounds.width / 2
    }
    
}

//
//  RecomendationHeaderViewCell.swift
//  Nourish
//
//  Created by Andrey Zmushko on 7/31/17.
//  Copyright © 2017 SSIC. All rights reserved.
//

import UIKit

class RecomendationHeaderViewCell: UITableViewCell {

    @IBOutlet weak var labelCell: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func setupWith(model: RecomendationDetailViewModel, section: Int) {
        if section == 1 {
            if let caption = model.caption {
                self.labelCell.text = String(format: "%@, try these:", caption)
            } else {
                self.labelCell.text = "Try these"
            }
        } else {
            self.labelCell.text = "Recommended recipes"
        }
        
    }
    
}

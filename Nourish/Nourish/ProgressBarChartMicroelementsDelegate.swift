//
//  ProgressBarChartMicroelementsDelegate.swift
//  Nourish
//
//  Created by Andrey Zmushko on 8/17/17.
//  Copyright © 2017 SSIC. All rights reserved.
//

import UIKit

class ProgressBarChartMicroelementsDelegate: NSObject, ProgressBarChartDelegate {
    
    var model: MicroelementViewModel?
    var config: ProgressConfig?
    var type: MicroelementType?
    
    var consumed = [CGFloat]()
    var target = [CGFloat]()
    
    var legends = [Int:(String, Int)]()

    func setupWith(Model model: MicroelementViewModel, config: ProgressConfig, type: MicroelementType) {
        self.model = model
        self.config = config
        self.type = type
    }

    func numberOfBars(in progressBarChart: ProgressBarChart) -> Int {
        if let model: MicroelementViewModel = self.model {
            if let data = model.data {
                
                if let config = self.config {
                    switch config.period {
                    case .weekly:
                        var label = { (index: Int) -> (String, Int) in
                            var numLines = Int()
                            var text = String()
                            
                            let calendar = Calendar.current
                            let currentDate: Date = {
                                if data.count > index {
                                    return data[index].date ?? Date()
                                }
                                return Date()
                            }()
                            
                            let formatter = DateFormatter()
                            if Calendar.current.isDateInToday(config.date) {
                                formatter.dateFormat = "MM/dd"
                            }
                            else {
                                formatter.dateFormat = "E"
                            }
                            text = formatter.string(from: currentDate).uppercased()
                            numLines = 1
                            
                            return (text, numLines)
                        }
                        
                        self.legends = [0:label(0), 1:label(1), 2:label(2), 3:label(3), 4:label(4), 5:label(5), 6:label(6)]
                    default:
                        var label = { (index: Int) -> (String, Int) in
                            var numLines = Int()
                            var text = String()
                            
                            let calendar = Calendar.current
                            if index == 1 || index == data.count - 2 {
                                let formatter = DateFormatter()
                                formatter.dateFormat = "MM/dd"
                                text = formatter.string(from: data[index - 1].date!)
                                numLines = 1
                            } else if index == data.count {
                                let formatter = DateFormatter()
                                formatter.dateFormat = "MM/dd"
                                text = formatter.string(from: data[index - 1].date!)
                                numLines = 1
                            }
                            return (text, numLines)
                        }
                        
                        self.legends = [0:label(1), (data.count - 2): label(data.count)]
                    }
                }
                
                func valFrom(data: (date: Date?, elements: [(type: MicroelementType, element: UserMicroelementViewModel)]), byType: MicroelementType) -> (consumed:  CGFloat, target: CGFloat) {
                    if let item = data.elements.filter({ element in element.type == byType }).first {
                        return (CGFloat(item.element.consumedMicroelement), CGFloat(item.element.targetMicroelement))
                    }
                    return (CGFloat(), CGFloat())
                }
                
                for element in data {
                    if let type = self.type {
                        consumed.append(valFrom(data: element, byType: type).consumed)
                        target.append(valFrom(data: element, byType: type).target)
                    }
                }
                
                return data.count
            }
        }
        return 0
    }
    
    func numberOfAxis(in progressBarChart: ProgressBarChart) -> Int {
        return 3
    }
    
    func progressBarChart(_ progressBarChart: ProgressBarChart, axisAtIndex:Int) -> ProgressBarChart.Axis? {
        if let model: MicroelementViewModel = self.model {
            if let data = model.data {
                let type: ProgressBarChart.AxisType!
                let value: Double!
                var color = UIColor.gray
                var aboveLabel: ProgressBarChartLabel?
                var belowLabel: ProgressBarChartLabel?
                
                var aboveLabelData: (String, Int)?
                var belowLabelData: (String, Int)?
                
                let labelText = { (type: MicroelementType) -> String in
                    if data.count > axisAtIndex {
                        if let item = data[axisAtIndex].elements.filter({ element in element.type == type }).first {
                            return String(format: "%.0f %@", item.element.targetMicroelement, item.element.unit)
                        }
                    }
                    return ""
                }
                
                if let type = self.type {
                    switch type {
                    case .sugars:
                        aboveLabelData = ("Added sugars", 1)
                        belowLabelData = (labelText(MicroelementType.sugars), 1)
                    case .salt :
                        aboveLabelData = ("Sodium", 1)
                        belowLabelData = (labelText(MicroelementType.salt), 1)
                    case .saturatedFat :
                        aboveLabelData = ("Saturated\nfat", 2)
                        belowLabelData = (labelText(MicroelementType.saturatedFat), 1)
                    }
                }
                
                switch axisAtIndex {
                case 2:
                    type = .dashed
                    value = 1.0
                    color = UIColor.clear
                case 1:
                    type = .dashed
                    value = 0.60
                    color = UIColor.black.withAlphaComponent(0.2)
                    if let data = aboveLabelData {
                        aboveLabel = ProgressBarChartLabel()
                        aboveLabel?.minHeightWith(height: 6.0,
                                                  text: data.0,
                                                  numberOfLines: data.1,
                                                  font: UIFont(name: "Campton-Bold", size: 12)!,
                                                  color: NRColorUtility.hexStringToUIColor(hex: "303030"),
                                                  verticalAlignment: ProgressBarChartLabel.LabelAlignment.top)
                    }
                    if let data = belowLabelData {
                        belowLabel = ProgressBarChartLabel()
                        belowLabel?.minHeightWith(height: 4.0,
                                                  text: data.0,
                                                  numberOfLines: data.1,
                                                  font: UIFont(name: "Roboto-Regular", size: 12)!,
                                                  color: NRColorUtility.hexStringToUIColor(hex: "808080"),
                                                  verticalAlignment: ProgressBarChartLabel.LabelAlignment.bottom)
                    }
                default:
                    type = .solid
                    value = 0.0
                    color = UIColor.black.withAlphaComponent(0.2)
                }
                
                return (color, type, value, aboveLabel, belowLabel) as ProgressBarChart.Axis
            }
        }
        
        return nil
    }
    
    func progressBarChart(_ progressBarChart: ProgressBarChart, barAtIndex:Int) -> ProgressBarChart.Bar? {
        if let model: MicroelementViewModel = self.model {
            if let data = model.data {
                
                var legendaView: ProgressBarChartLabel?
                
                func calcBar(consumedAtIndex: CGFloat, maxConsumed: CGFloat, maxTarget: CGFloat) -> [CGFloat] {
                    if consumedAtIndex == 0 {
                        return [0.01, 0]
                    }
                    else if consumedAtIndex < maxTarget {
                        return [0.6 * consumedAtIndex / maxTarget, 0]
                    }
                    else {
                        return [0.6, 0.4 * (consumedAtIndex - maxTarget) / (maxConsumed - maxTarget) ]
                    }
                }
                
                let barDataFor = { (type: MicroelementType, index: Int) -> [CGFloat] in
                    return calcBar(consumedAtIndex: self.consumed[index], maxConsumed: self.consumed.max()!, maxTarget: self.target.max()!)
                }
                
                let barDataAtIndex = { (index: Int) -> [CGFloat] in
                    if let type = self.type {
                        return barDataFor(type, index)
                    }
                    return [0.0, 0.0]
                }
                
                let barData = barDataAtIndex(barAtIndex)
                
                let consumedColorAtIndex = { (val: CGFloat) -> UIColor in
                    if val == 0 {
                        return NRColorUtility.hexStringToUIColor(hex: "BFBFBF")
                    }
                    else {
                        return NRColorUtility.hexStringToUIColor(hex: "70C397")
                    }
                }
                
                let consumedColor = { () -> UIColor in
                    return consumedColorAtIndex(self.consumed[barAtIndex])
                }
                
                let consumedLabelColorAtIndex = { (val: CGFloat) -> UIColor in
                    if let config = self.config {
                        if val == 0 && config.period == .weekly {
                            return NRColorUtility.hexStringToUIColor(hex: "BFBFBF")
                        }
                    }
                    return NRColorUtility.hexStringToUIColor(hex: "808080")
                }
                
                let consumedLabelColor = { () -> UIColor in
                    return consumedLabelColorAtIndex(self.consumed[barAtIndex])
                }
            
                if let legenda = self.legends[barAtIndex] {
                    legendaView = ProgressBarChartLabel()
                    legendaView?.label.textAlignment = .center
                    legendaView?.minHeightWith(height: 8.0,
                                               text: legenda.0,
                                               numberOfLines: legenda.1,
                                               font: UIFont(name: "Roboto-Regular", size: 11)!,
                                               color: consumedLabelColor(),
                                               verticalAlignment: ProgressBarChartLabel.LabelAlignment.center)
                }
                
                let legendaAligment = { () -> ProgressBarChart.LegendaAlignment in
                    if let config = self.config {
                        switch config.period {
                        case .weekly:
                            //return .center
                            break
                        default:
                            break
                        }
                    }
                    return .center
                }
                
                return (barData, [consumedColor(), NRColorUtility.hexStringToUIColor(hex: "EC1C28")], legendaView as UIView?, legendaAligment()) as ProgressBarChart.Bar
            }
        }
        return nil
    }
    
}

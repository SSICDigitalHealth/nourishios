//
//  CalorificFoodPresenter.swift
//  Nourish
//
//  Created by Vlad Birukov on 20.06.17.
//  Copyright © 2017 SSIC. All rights reserved.
//

import Foundation
import RxSwift


class CalorificFoodPresenter: BasePresenter, UITableViewDataSource,UITableViewDelegate {
    let objectToModelMapper = CalorificFoodModel_to_CalorificFoodViewModel()
    let interactor = CalorificFoodInteractor()
    var calorificFoodView: CalorificFoodProtocol!
    var calorificFoodModel = CalorificFoodViewModel()
    var date: (Date, Date)?
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.getСalorificFoodOfWeekDetail()
    }
    
    private func getСalorificFoodOfWeekDetail() {
        if self.calorificFoodView.config() != nil {
            self.date = (self.calorificFoodView.config()?.getDate())
        }
        
        if self.date != nil {
            self.subscribtions.append(self.interactor.execute(startDate: (self.date?.0)!, endDate: (self.date?.1)!).observeOn(MainScheduler.instance).subscribe(onNext: { [unowned self] calorificFood in
                self.calorificFoodModel = self.objectToModelMapper.transform(model: calorificFood)
                self.calorificFoodView.reloadTable()
                self.calorificFoodView.stopActivityAnimation()
            }, onError: {error in
                self.calorificFoodView.stopActivityAnimation()
                self.calorificFoodView.parseError(error : error,completion: nil)
            }, onCompleted: {
            }, onDisposed: {
                self.calorificFoodView.stopActivityAnimation()
            }))
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if calorificFoodModel.arrCaloriesFood != nil {
            return (calorificFoodModel.arrCaloriesFood?.count)!
        } else {
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cellCalorificFood", for: indexPath) as! CalorificFoodOfWeekTableViewCell
        
        let currenModel = calorificFoodModel.arrCaloriesFood?[indexPath.row]
        
        cell.setupWithModel(model: currenModel!)
    
        return cell
    }

}

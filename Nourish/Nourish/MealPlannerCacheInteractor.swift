//
//  MealPlannerCacheInteractor.swift
//  Nourish
//
//  Created by Vlad Birukov on 22.09.17.
//  Copyright © 2017 SSIC. All rights reserved.
//

import Foundation
import RxSwift

class MealPlannerCacheInteractor {
    private let mealPlanRepository = MealPlannerRepository()
    
    func executeFor(date: Date) -> Observable <Bool> {
        return self.mealPlanRepository.getCacheMealPlanFor(date: date)
    }
    
    func execute(dateWith: Date, dateFor: Date) -> Observable<(Bool, Bool)> {
        return self.mealPlanRepository.getCacheMealPlanFor(dateWith: dateWith, dateFor: dateFor)
    }
}

//
//  FavoritesRepository.swift
//  Nourish
//
//  Created by Nova on 3/20/17.
//  Copyright © 2017 SSIC. All rights reserved.
//

import UIKit
import RxSwift
import RealmSwift

let kBackendIDKey = "kBackendIDKey"
let kCacheIDKey = "kCacheIDKey"
let kGroupNameKey = "kGroupNameKey"
let sfdb = "source_fooddb"
let sfdbid = "source_fooddb_id"

class foodGroup : NSObject {
    var groupID : String?
    var name : String?
    var favourite : Bool?
    var foodItems : [MealRecord]?
    
}

class FavoritesRepository: NSObject {
    let store = NourishDataStore.shared
    let userRepo = UserRepository.shared
    let mealRepo = MealHistoryRepository.shared
    let realmRepo = FoodRecordRepository.shared
    let realmCacheStore = FavoriteCacheDataStore()
    let routine = NMFood_to_MealRecordRoutine()
    let bgFetcher = BackgroundChangeScoreFetcher.shared
    
    func addtoFavourites(models : [foodSearchModel], name : String) -> Observable<Bool> {
        /*
        let data = userRepo.getCurrentUser(policy: .DefaultPolicy).flatMap { user -> Observable<String> in
            return Observable.just(user.userID!)
            }.flatMap { userID -> Observable<Data> in
            let token = NRUserSession.sharedInstance.accessToken
            
            let value = self.store.addMealsToGroup(token: token!, userID: userID, foodModels: models, name: name)
            return value
            }.flatMap {data -> Observable<Bool> in
                self.refreshCacheFavorites()
                return Observable.just(true)
        }
        */
        
        var userFoodIDs = [String]()
        var localCacheIDs = [String]()
        let realm = try! Realm()
        
        for object in models {
            if object.foodBackendID != nil && object.foodBackendID != "" {
                userFoodIDs.append(object.foodBackendID!)
            } else {
                localCacheIDs.append(object.localCacheID!)
            }
        }
        
        for object in userFoodIDs {
            if let object = self.mealRepo.fetchCacheObjectFor(ident: object) {
                try! realm.write {
                    object.isFavourite = true
                }
                
            }
        }
        
        for loclaobject in localCacheIDs {
            if let localObject = self.mealRepo.fetchCacheObjectFor(ident: loclaobject) {
                try! realm.write {
                    localObject.isFavourite = true
                }
            }
        }
        
        let dict = [kBackendIDKey : userFoodIDs, kCacheIDKey : localCacheIDs, kGroupNameKey : name] as [String : Any]
        
        try! realm.write {
            let operation = OperationCache()
            operation.operationTypeEnum = .merge
            operation.timeStamp = Date()
            operation.dictionary = dict
            operation.counter = 0
            operation.userID = NRUserSession.sharedInstance.userID
            realm.add(operation)
        }
        let int = BackgroundMealDiarySyncInteractor.shared
        int.testFetch()
        
        return Observable.just(false)
    }
    
    func addtoFavorites(cache : MealRecordCache, name : String) -> Observable<String> {
        cache.isFavourite = true
        let seq = userRepo.getCurrentUser(policy: .DefaultPolicy).map { user in
            return user.userID
        }.flatMap { userID -> Observable<Data> in
            let token = NRUserSession.sharedInstance.accessToken
            
            let value = self.store.addCacheToGroup(token: token!, userID: userID!, cache: cache, name: name)
            return value
        }.flatMap {data -> Observable<String> in
                self.bgFetcher.executeFor(date: Date())
                let groupID = self.getGroupIDFrom(data: data)
                self.refreshCacheFavorites()
                if groupID != nil {
                    return Observable.just(groupID!)
                } else {
                    return Observable.just("")
                }
        }
        return seq
    }
    
    func addToDiaryWithPhoto(models : [MealRecordCache], name : String, ocasion : Ocasion, date : Date, userID : String) -> Observable<String> {
        
        let data = Observable.just(userID).flatMap { userID -> Observable<Data> in
                let token = NRUserSession.sharedInstance.accessToken
                
                let value = self.store.addMealRecordsToGroup(token: token!, userID: userID, foodModels: models, name: name)
                return value
            }.flatMap {data -> Observable<String> in
                let groupID = self.getGroupIDFrom(data: data)
                self.bgFetcher.executeFor(date: date)
                self.refreshCacheFavorites()
                if groupID != nil {
                    return Observable.just(groupID!)
                } else {
                    return Observable.just("")
                }
        }
        return data
    }
    
    func addToFavouritesWithPhoto(models : [foodSearchModel], name : String) -> Observable<String> {
            let data = userRepo.getCurrentUser(policy: .DefaultPolicy).flatMap { user -> Observable<String> in
                return Observable.just(user.userID!)
                }.flatMap { userID -> Observable<Data> in
                    let token = NRUserSession.sharedInstance.accessToken
                    
                    let value = self.store.addRecToGroup(token: token!, userID: userID, foodModels: models, name: name, favorite : false)
                    return value
                }.flatMap {data -> Observable<String> in
                    let groupID = self.getGroupIDFrom(data: data)
                    self.bgFetcher.executeFor(date: Date())
                    self.refreshCacheFavorites()
                    if groupID != nil {
                        return Observable.just(groupID!)
                    } else {
                        return Observable.just("")
                    }
            }
            return data
    }
    
    func addGroupToFavorites(models: [foodSearchModel], name: String) -> Observable<Bool> {
        let sucess = userRepo.getCurrentUser(policy: .DefaultPolicy).flatMap { user -> Observable<String> in
            return Observable.just(user.userID!)
            }.flatMap { userID -> Observable<Data> in
                let token = NRUserSession.sharedInstance.accessToken
                
                let value = self.store.addRecToGroup(token: token!, userID: userID, foodModels: models, name: name, favorite : true)
                return value
            }.flatMap {data -> Observable<Bool> in
                let groupID = self.getGroupIDFrom(data: data)
                
                self.refreshCacheFavorite()
                if groupID != nil {
                    return Observable.just(true)
                } else {
                    return Observable.just(false)
                }
        }
        return sucess
    }
    
    func checkGroupName(name : String) -> String {
        var stringFinal = String(format : "%@",name)
        let mealNames = self.fetchFavGroupsForName(name: name)
        var maxValue = 0
        
        if mealNames.count > 0 {
            for object in mealNames {
                let decimalCharacters = CharacterSet.decimalDigits
                let decimalRange = object.rangeOfCharacter(from: decimalCharacters)
                if decimalRange != nil {
                    let number = object.substring(with: decimalRange!)
                    let index = Int(number)
                    
                    if index != nil {
                        maxValue = index! > maxValue ? index! : maxValue
                    }
                }
                
            }
            
            stringFinal = String(format : "%@ (%d)",name, 2)
            
            if maxValue > 0 {
                stringFinal = String(format : "%@ (%d)", name, maxValue + 1)
            }
            
        }
        return stringFinal
        
    }
    
    private func fetchFavGroupsForName(name : String) -> [String] {
        let data = self.realmCacheStore.fetchLastCache()
        var returnArray = [String]()
        
        if data != nil {
            let array = self.arrayFromData(data: data!)
            let meals = self.jsonToRecords(array: array)
            
            let filtered = meals.filter {($0.groupName?.contains(name))!}
            let _ = filtered.map { returnArray.append($0.groupName!) }
        }
        
        return returnArray
    }
    
    private func getGroupIDFrom(data : Data) -> String? {
        let json = try? JSONSerialization.jsonObject(with: data, options: [])
        
        if let dictionary = json as? [String: Any] {
            let dict = dictionary["data"] as? [String : Any]
            let groupID = dict?["foodgroup_id"] as? String
            if groupID != nil {
                return groupID
            }
        }
        return nil
    }
    
    func addRecToFavorite(model : foodSearchModel, name : String) -> Observable<Bool> {
       // let store = MealRecordCache()
        let data = userRepo.getCurrentUser(policy: .DefaultPolicy).flatMap {
            user -> Observable<String> in
            return Observable.just(user.userID!)
            }.flatMap { userID -> Observable<Data> in
            let token = NRUserSession.sharedInstance.accessToken
                var value = self.store.addRecToGroup(token: token!, userID: userID, foodModels: [model], name: name, favorite : true)
                if model.foodBackendID != "" {
                    value = self.store.addMealsToGroup(token: token!, userID: userID, foodModels: [model], name: name)
                }
                
            return value
            }.flatMap { data -> Observable<Bool> in
                self.refreshCacheFavorite()

                return Observable.just(true)
                
        }
        return data
    }
    func fetchFavorites() -> Observable<[MealRecordModel]> {
        let data = userRepo.getCurrentUser(policy: .DefaultPolicy).flatMap {user -> Observable<String> in
            return Observable.just(user.userID!)
            }.flatMap { userID -> Observable<Data> in
            let token = NRUserSession.sharedInstance.accessToken
                
            let value = self.store.fetchFavorites(token: token!, userID: userID)
            return value
            }.flatMap {data -> Observable<[MealRecordModel]> in
                let array = self.arrayFromData(data: data)
            let arrayOfMeals = self.jsonToRecords(array: array)
                
            return Observable.just(arrayOfMeals)
        }
        return data
    }
    
    func fetchCacheFavorites() -> [MealRecordModel] {
        
        let cache = self.realmCacheStore.fetchLastCache()
        var result = [MealRecordModel]()
        
        if cache != nil {
            let array = self.arrayFromData(data: cache!)
            let arrayOfMeals = self.jsonToRecords(array: array)
            result.append(contentsOf: arrayOfMeals)
        }
        
        return result
    }
    
    func fetchFavoritesWithCache() -> Observable<[MealRecordModel]> {
        return Observable.create { observer in
            let cache = self.realmCacheStore.fetchLastCache()
            
            if cache != nil {
                let array = self.arrayFromData(data: cache!)
                let arrayOfMeals = self.jsonToRecords(array: array)
                observer.onNext(arrayOfMeals)
            }
            
            let _ = self.userRepo.getCurrentUser(policy: .DefaultPolicy).flatMap {user -> Observable<String> in
                return Observable.just(user.userID!)
                }.flatMap { userID -> Observable<Data> in
                    let token = NRUserSession.sharedInstance.accessToken
                    
                    let value = self.store.fetchFavorites(token: token!, userID: userID)
                    return value
                }.map {data -> [MealRecordModel] in
                    self.realmCacheStore.storeCache(cache: data, date: Date())
                    let array = self.arrayFromData(data: data)
                    let arrayOfMeals = self.jsonToRecords(array: array)
                    
                    return arrayOfMeals
                }.subscribe(onNext: {meals in
                    observer.onNext(meals)
                }, onError: {error in}, onCompleted: {
                    observer.onCompleted()
                }, onDisposed: {})
            
            return Disposables.create()
        }
    }
    
    func fetchCachedFavorites() -> [MealRecordModel] {
        var result = [MealRecordModel]()
        let cache = self.realmCacheStore.fetchLastCache()
        
        if cache != nil {
            let array = self.arrayFromData(data: cache!)
            let arrayOfMeals = self.jsonToRecords(array: array)
            result.append(contentsOf: arrayOfMeals)
        }
        return result
    }
    
    func deleteFavouriteGroup(groupID : String) -> Observable<Bool> {
        let data = userRepo.getCurrentUser(policy: .DefaultPolicy).flatMap {user ->
            Observable<String> in
            return Observable.just(user.userID!)
        }.flatMap { userID -> Observable<Data> in
            let token = NRUserSession.sharedInstance.accessToken
            let value = self.store.deleteGroupWith(groupID: groupID, token: token!, userID: userID)
            return value
        }.flatMap {data -> Observable<Bool> in
            self.refreshCacheFavorite()
            return Observable.just(true)
        }
        return data
    }
    
    func addGroupToFav(groupdID : String, name : String, userPhotoID : String?) -> Observable<Bool> {
        let data = userRepo.getCurrentUser(policy: .DefaultPolicy).flatMap {user ->
            Observable<String> in
            return Observable.just(user.userID!)
            }.flatMap { userID -> Observable<Data> in
                let token = NRUserSession.sharedInstance.accessToken
                let value = self.store.addGroupToFavorite(token: token!, userID: userID, groupID: groupdID, name: name, userPhotoID: userPhotoID)
                return value
            }.flatMap {data -> Observable<Bool> in
                self.refreshCacheFavorites()
                return Observable.just(true)
        }
        return data
    }
    
    func addToDiary(foodGroupd : String, groupName:String , occasion : Ocasion, date : Date, models : [foodSearchModel]) -> Observable<Bool> {
        let data = userRepo.getCurrentUser(policy: .DefaultPolicy).flatMap { user -> Observable<String> in
            return Observable.just(user.userID!)
            }.flatMap { userID -> Observable<Data> in
                let token = NRUserSession.sharedInstance.accessToken
                let value = self.store.uploadFavGroup(groupID: foodGroupd, occasion: occasion, userID: userID, token: token!, date: date)
                return value
            }.flatMap { responceData -> Observable<Bool> in
                let mapped = self.populate(models: models, ocasion: occasion)
                self.realmRepo.addToCounted(models: mapped)
                
                if Calendar.current.startOfDay(for: date) == Calendar.current.startOfDay(for: Date()) {
                    self.mealRepo.storeChatMessagesForGroup(data: responceData, groupName:groupName)
                }
                self.refreshCacheFavorites()
                return Observable.just(true)
        }
        return data
    }
    
    func populate(models : [foodSearchModel], ocasion : Ocasion) -> [foodSearchModel] {
        var mappedModels = [foodSearchModel]()
        
        for obj in models {
            var mapped = obj
            mapped.occasion = ocasion
            mappedModels.append(mapped)
        }
        return mappedModels
    }
    
    private func arrayFromData(data : Data) -> [[String : Any]] {
        var object : [String : Any] = [:]
        do {
            object = try JSONSerialization.jsonObject(with: data, options: []) as? [String : Any] ?? [String: Any]()
        } catch let error as NSError {
            print(error)
        }
        var proxyArray : [[String : Any]] = []
        if let dict = object["data"] as? [[String : Any]] {
            
            
            for dictR in dict {
                if dictR["deleted"] == nil || dictR["deleted"] as! Bool == false {
                    if dictR["favorite"] != nil {
                        if dictR["favorite"] as! Bool == true {
                            proxyArray.append(dictR)
                        }
                    }
                }
            }
        }
        
        return proxyArray
    }
    
    private func jsonToRecords(array : [[String : Any]]) -> [MealRecordModel] {
        var arrayToReturn : [MealRecordModel] = []
        for object in array {
            let model = MealRecordModel()
            model.groupID = object["foodgroup_id"] as? String
            model.groupName = object["name"] as? String
            var calories = 0.0
            let arrayOfMeals = object["food_items"] as? [[String : Any]]
            var mealArray : [MealRecordModel] = []
            if arrayOfMeals != nil {
                for objectMeal in arrayOfMeals! {
                    let record = self.mealFrom(dict: objectMeal)
                    mealArray.append(record)
                    calories += record.calories ?? 0.0
                }
            }
            
            model.userPhotoId = object["user_photo_id"] as? String
            model.groupedMealArray = mealArray
            if model.groupedMealArray.count > 1 {
                model.isGroupFavourite = true
                model.groupPhotoID = object["user_photo_id"] as? String
            } else {
                model.isFavourite = true
            }
            model.calories = calories
            arrayToReturn.append(model)
        }
        return arrayToReturn
    }
    
    private func mealFrom(dict : [String : Any]) -> MealRecordModel {
        let mapper = Json_to_MealRecordCache()
        let model = MealRecordModel()
        model.servingAmmount = dict["amount"] as? Double
        
        let grams = dict["grams"] as? Double
        model.grams = grams ?? 0.0
        
        model.calories = dict["calories"] as? Double
        
        if let sfdb = dict[sfdb] {
            model.sourceFoodDB = sfdb as? String
        }
        
        if let sfdbid = dict[sfdbid] {
            if let dbID = sfdbid as? Int {
                model.sourceFoodDBID = String(dbID)
            }
        }
        
        var calories = dict["calories"] as? String
        if calories == nil || Double(calories!) == 0 {
            if let kpm = dict["kcalPerGram"] as? Double ?? dict["caloriesPerGram"] as? Double {
                let caloriesDouble = kpm * model.grams!
                calories = String(format : "%.2f",caloriesDouble)
            }
            if calories == nil {
                if let calValue = dict["calories"] as? Double {
                    calories = String(format : "%.2f",calValue)
                } else {
                    calories = "0.0"
                }
                
            }
        }
        
        model.calories = Double(calories!)
        model.idString = dict["id"] as? String
        model.isFromNoom = dict["is_noom"] as? Bool ?? false
        model.servingType = dict["msre_desc"] as? String
        let servString = dict["unit"] as? String
        if servString != nil {
            model.servingType = servString
        }
        if model.isFromNoom == true {
            let stringRep = dict["noom_id"] as? String
            if stringRep != nil {
                model.foodId = mapper.noomDBStringFrom(stringV: stringRep!)
            }
            let name = dict["noom_name"] as? String
            if name != nil {
                model.foodTitle = name
            }
        } else {
            let name = dict["name"] as? String
            if name != nil {
                model.foodTitle = name
            }
        }
        model.userPhotoId = dict["user_photo_id"] as? String
        let nutArray = dict["nutrients"] as? [Any]
        if nutArray != nil && (nutArray?.count)! > 0 {
            
            if dict["nutrients"] != nil {
                let dictNutrs = dict["nutrients"] as! [String : Double]
                model.nutrs = self.nutrsFromDict(nutrs: dictNutrs)
            }
            
        }
        
        return model
    }
    
    private func nutrsFromDict(nutrs :[String : Double]) -> [Nutrient] {
        var array : [Nutrient] = []
        
        for (name, clearValue) in nutrs {
            let model = Nutrient()
            model.name = name
            model.clearValue = clearValue
            array.append(model)
        }
        let utility = NutrientUtility()
        
        return utility.recountValues(array: array)
    }
    
    func isFoodGroupSingleFavorite(groupID : String?) -> Bool {
        var result = [MealRecordModel]()
        if groupID != nil {
            let cache = self.realmCacheStore.fetchLastCache()
            
            
            if cache != nil {
                let array = self.arrayFromData(data: cache!)
                let meals = self.jsonToRecords(array: array)
                let filtered = meals.filter { $0.groupID == groupID }
                result = filtered.filter { $0.groupedMealArray.count == 1 }
            }
        }
        return result.count > 0
    }
    
    func refreshCacheFavorite() {
            let _ = self.userRepo.getCurrentUser(policy: .DefaultPolicy).flatMap {user -> Observable<String> in
                return Observable.just(user.userID!)
                }.flatMap { userID -> Observable<Data> in
                    let token = NRUserSession.sharedInstance.accessToken
                    
                    let value = self.store.fetchFavorites(token: token!, userID: userID)
                    return value
                }.flatMap {data -> Observable<[MealRecordModel]> in
                    self.realmCacheStore.storeCache(cache: data, date: Date())
                    let array = self.arrayFromData(data: data)
                    let arrayOfMeals = self.jsonToRecords(array: array)
                    
                    return Observable.just(arrayOfMeals)
                }.subscribe(onNext: {meals in
                    NotificationCenter.default.post(name: NSNotification.Name(rawValue: kFavReload), object: nil)
                }, onError: {error in}, onCompleted: {
                }, onDisposed: {})
    }
    
    func refreshCacheFavorites() {
        DispatchQueue.global().async {
            let _ = self.userRepo.getCurrentUser(policy: .DefaultPolicy).flatMap {user -> Observable<String> in
                return Observable.just(user.userID!)
                }.flatMap { userID -> Observable<Data> in
                    let token = NRUserSession.sharedInstance.accessToken
                    
                    let value = self.store.fetchFavorites(token: token!, userID: userID)
                    return value
                }.flatMap {data -> Observable<[MealRecordModel]> in
                    self.realmCacheStore.storeCache(cache: data, date: Date())
                    let array = self.arrayFromData(data: data)
                    let arrayOfMeals = self.jsonToRecords(array: array)
                    
                    return Observable.just(arrayOfMeals)
                }.subscribe(onNext: {meals in
                }, onError: {error in}, onCompleted: {
                }, onDisposed: {})

        }
    }
}

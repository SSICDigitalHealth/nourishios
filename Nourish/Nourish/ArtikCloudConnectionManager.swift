//
//  ArtikCloudConnectionManager.swift
//  Nourish
//
//  Created by Nova on 11/22/16.
//  Copyright © 2016 SSIC. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa
let kGetUserString = "/users/self"
let kGetUsers = "/users/"
let kGetProperties = "/properties"
let kPostMessages = "/messages"
let kDeviceID = "dtccd2fb82bc234d87abc27caed84e7973"
let kDeviceString = "UserDetails"
let kPostDevice = "/devices"
let kGetDeviceTypes = "/devices"
let kRegisterDevice = "/devices/"
let kAuthProviderDevice = "/providerauth"
let kGetMessages = "/messages"
let kNewAppIDLogout = "kNewAppIDLogout"

let kBlackListUrl = "https://optiforlife-blacklist.ssic.io/v1"

class ArtikCloudConnectionManager: BaseNestleDataStore {
    
    var userCache : UserProfileCacheRealm {
        set(value) {
            let repo = RealmUserRepository()
            repo.storeDefault(userCache: value)
        }
        get {
            let repo = RealmUserRepository()
            return repo.getDefaultUser()
        }
    }
    let mapperToJson = UserProfileCacheRealm_to_Json()
    
    func refreshToken(refreshToken : String, oldAccessToken : String) -> Observable<String> {
        return Observable.create { observer in
            let stringUrl = kSAMIAuthBaseUrl + String(format : "/token?grant_type=refresh_token&refresh_token=\(refreshToken)")
            let url = URL(string : stringUrl)
            var request = URLRequest(url: url!)
            request.httpMethod = "POST"
            request.setValue("application/x-www-form-urlencoded", forHTTPHeaderField: "Content-Type")
            request.setValue("bearer \(oldAccessToken)", forHTTPHeaderField: "Authorization")
            
            let session = URLSession.shared.dataTask(with: request) {
                data, response, error in
                var dataStr = ""
                if error == nil && data != nil {
                    var token = ""
                    let dict = self.convertStringToDictionary(text: String(data : data!, encoding : .utf8)!)
                    if dict!["access_token"] != nil {
                        token = dict!["access_token"] as? String ?? ""
                        NRUserSession.sharedInstance.accessToken = dict!["access_token"] as? String
                        NRUserSession.sharedInstance.tokenRefreshDate = Date()
                    }
                    if dict!["refresh_token"] != nil {
                        NRUserSession.sharedInstance.refreshToken = dict!["refresh_token"] as? String
                    }
                    if dict!["token_type"] != nil {
                        NRUserSession.sharedInstance.tokenType = dict!["token_type"] as? String
                    }
                    if dict!["expires_in"] != nil {
                        let doubleTime = dict!["expires_in"] as! Double
                        let date = Date().addingTimeInterval(doubleTime - kTimeOffsetSeconds)
                        NRUserSession.sharedInstance.tokenExpirationDate = date
                    }
                    observer.onNext(token)
                } else {
                    if error != nil {
                        observer.onError(error!)
                    }
                }
                self.log(foodProcessingOrWithings: false, url: url?.absoluteString, requestMethod: request.httpMethod, urlResponse: response, requestBody: nil, error: error, responceBody: nil)
                observer.onCompleted()

            }
            session.resume()
            
            return Disposables.create {
                session.cancel()
            }
        }
        
        
    }
    
    func addAthorizationHeader(url : URL) -> URL {
        if NRUserSession.sharedInstance.accessToken != nil {
            var stringRep = url.absoluteString
            stringRep.append("?Authorization=bearer+\(NRUserSession.sharedInstance.accessToken!)")
            let urlToReturn = URL(string: stringRep)
            return urlToReturn!
        } else {
            return url
        }
        
    }
    
    func receiveCurrentUser() -> Observable<Dictionary<String, Any>> {
        return Observable.create { observer in
            var url = URL(string: "\(kSAMIApiUrl)\(kGetUserString)")
            url = self.addAthorizationHeader(url: url!)
            var request = URLRequest(url: url!)
            request.httpMethod = "GET"
            
            let session = URLSession.shared.dataTask(with: request) {
                data, response, error in
                if error == nil && data != nil  {
                    let stringData = String(data : data!, encoding : .utf8)
                    
                    if stringData != nil {
                        let dictionaryRepresentation = self.convertStringToDictionary(text: String.init(data: data!, encoding: .utf8)!)
                        
                        let user = dictionaryRepresentation?["data"]
                        
                        if user != nil {
                            observer.onNext(user! as! [String : Any])
                        } else {
                            
                            if let errorDict = dictionaryRepresentation?["error"] as? [String : Any] {
                                let err = NSError(domain: "", code: errorDict["code"] as? Int ?? 0, userInfo: ["error message" : [errorDict["message"] as? String ?? ""]])
                                observer.onError(err)
                            }
                          //  self.logout(keepCurrent: false)
                        }

                    } else {
                        self.logout(keepCurrent: false)
                    }
                    
                } else {
                    observer.onError(error!)
                }
                self.log(foodProcessingOrWithings: false, url: url?.absoluteString, requestMethod: request.httpMethod, urlResponse: response, requestBody: nil, error: error, responceBody: nil)

                
                observer.onCompleted()

            }
            session.resume()
            return Disposables.create {
                session.cancel()
            }
        }
        
    }
    
    func logout(keepCurrent : Bool) {
        DispatchQueue.main.async {
            NRUserSession.sharedInstance.logout(keepCurrent: keepCurrent)
            NavigationUtility.changeRootViewController(userProfile: nil, awakedFromBackground: false)
        }

    }
    
    func receiveCurrentUserProperties() -> Observable<Dictionary<String, Any>> {
        return Observable.create { observer in
            let url = self.userPropertiesUrl()
            let token = NRUserSession.sharedInstance.accessToken ?? ""
            var request = self.requestWithToken(token: token, url: url)
            request.httpMethod = "GET"
            let session = URLSession.shared.dataTask(with: request) {
                data, response, error in
                if error == nil && data != nil  {
                    let stringData = String.init(data: data!, encoding: .utf8)
                    var dict = self.convertStringToDictionary(text: stringData!)
                    
                    if dict?["error"] != nil {
                        let dict = dict!["error"]
                        let artikError = self.artikErrorFrom(error: dict! as! [String : AnyObject])
                        self.log(foodProcessingOrWithings: false, url: url.absoluteString, requestMethod: request.httpMethod, urlResponse: response, requestBody: nil, error: artikError as Error, responceBody: nil)
                        observer.onError(artikError as Error)
                        observer.onCompleted()
                    } else {
                        dict = dict?["data"] as! [String : String]!
                        
                        if let aid = dict?["aid"] as? String {
                            if aid != kSamiClientID {
                                self.logout(keepCurrent: true)
                                defaults.set(true, forKey: kNewAppIDLogout)
                                defaults.synchronize()
                            } else {
                                let propertiesString = dict?["properties"] as? String
                                
                                if propertiesString != nil {
                                    observer.onNext(self.convertStringToDictionary(text: propertiesString!)!)
                                }
                            }
                        }
                    }
                   
                }
                self.log(foodProcessingOrWithings: false, url: url.absoluteString, requestMethod: request.httpMethod, urlResponse: response, requestBody: nil, error: error, responceBody: nil)
                observer.onCompleted()

            }
            session.resume()
            return Disposables.create {
                session.cancel()
            }
        }
        
    }
    
    func deleteUserProperties(user : UserProfileCacheRealm) -> Observable<ArtikError> {
        return Observable.create { observer in
            let dataValue = ["aid" : kSamiClientID,  "properties" : ""] as [String : Any]
            let url = self.userPropertiesUrl()
            var request = URLRequest(url: url)
            request.httpMethod = "DELETE"
            
            var data = Data()
            
            do {
                data = try JSONSerialization.data(withJSONObject: dataValue, options: [])
            } catch let error as NSError {
                LogUtility.logToFile(error)
            }
            
            request.httpBody = data
            
            let session = URLSession.shared.dataTask(with: request) {
                data, response, error in
                if error == nil && data != nil  {
                    
                    let dictionaryRepresentation = self.convertStringToDictionary(text: String.init(data: data!, encoding: .utf8)!)
                    if dictionaryRepresentation?["error"] != nil {
                        let dict = dictionaryRepresentation!["error"]
                        let artikError = self.artikErrorFrom(error: dict! as! [String : AnyObject])
                        observer.onNext(artikError)
                    }
                }
                self.log(foodProcessingOrWithings: false, url: url.absoluteString, requestMethod: request.httpMethod, urlResponse: response, requestBody: nil, error: error, responceBody: nil)
                observer.onCompleted()

            }
            session.resume()
            return Disposables.create {
                session.cancel()
            }
        }

        
    }
    
    func uploadUserMessage(userMessage : [String : Any], deviceId : String) -> Observable<Bool> {
        return Observable.create { observer in
            var dict = [String: Any]()
            dict["data"] = userMessage
            dict["type"] = "message"
            dict["ddid"] = deviceId
            dict["sdid"] = deviceId
            
            var data = Data()
            
            do {
                data = try JSONSerialization.data(withJSONObject: dict, options: [])
            } catch let error as NSError {
                LogUtility.logToFile(error)
            }
            let url = self.postMessageUrl()
            var request = URLRequest(url : url)
            
            request.setValue("application/json; charset=utf-8", forHTTPHeaderField: "Content-Type")
            request.httpMethod = "POST"
            
            request.httpBody = data
            
            let session = URLSession.shared.dataTask(with: request) {
                data, response, error in
                if error == nil  {
                    let _ = self.convertStringToDictionary(text: String(data : data!, encoding: .utf8)!)
                    observer.onNext(true)
                } else {
                    observer.onError(error!)
                }
                self.log(foodProcessingOrWithings: false, url: url.absoluteString, requestMethod: request.httpMethod, urlResponse: response, requestBody: nil, error: error, responceBody: nil)
                observer.onCompleted()

            }
            
            session.resume()
            return Disposables.create {
                session.cancel()
            }
        }
    }
    
    
    func storeUserProperties(user : UserProfileCacheRealm, method : String) ->Observable<ArtikError?> {
        return Observable.create { observer in
            let jsonUser = self.mapperToJson.transform(userCache: user)
            var data = Data()
            
            do {
                data = try JSONSerialization.data(withJSONObject: jsonUser, options: [])
            } catch let error as NSError {
                LogUtility.logToFile(error)
            }
            
            let stringRep = String(data: data, encoding: .utf8)
            let dataValue = ["aid" : kSamiClientID,  "properties" : stringRep!] as [String : Any]
            let url = self.userPropertiesUrl()
            var request = self.requestWithToken(token: NRUserSession.sharedInstance.accessToken ?? "", url: url)
            
            request.setValue("application/json; charset=utf-8", forHTTPHeaderField: "Content-Type")
            request.httpMethod = method

            var json = Data()
            
            do {
                json = try JSONSerialization.data(withJSONObject: dataValue, options: [])
            } catch let error as NSError {
                LogUtility.logToFile(error)
            }
            
            request.httpBody = json
            let session = URLSession.shared.dataTask(with: request) {
                data, response, error in
                if error == nil && data != nil  {
                   
                    let dictionaryRepresentation = self.convertStringToDictionary(text: String.init(data: data!, encoding: .utf8)!)
                    if dictionaryRepresentation?["error"] != nil {
                        let dict = dictionaryRepresentation!["error"]
                        let artikError = self.artikErrorFrom(error: dict! as! [String : AnyObject])
                        self.log(foodProcessingOrWithings: false, url: url.absoluteString, requestMethod: request.httpMethod, urlResponse: response, requestBody: nil, error: artikError, responceBody: nil)
                        observer.onNext(artikError)
                        observer.onCompleted()
                    } else {
                        observer.onNext(nil)
                    }
                }
                self.log(foodProcessingOrWithings: false, url: url.absoluteString, requestMethod: request.httpMethod, urlResponse: response, requestBody: nil, error: error, responceBody: nil)
                observer.onCompleted()
            
            }
            session.resume()
            return Disposables.create {
                session.cancel()
            }
        }
    }
    
    func artikErrorFrom(error : [String : AnyObject]) -> ArtikError {
        let artikError = ArtikError()
        artikError.code = error["code"] as? Int
        artikError.errorDescription = (error["message"] as? String)
        return artikError
    }
    
    func userPropertiesUrl() -> URL {
        var url = URL(string : "\(kSAMIApiUrl)")
        if self.userCache.userID != nil {
            url = URL(string: "\(kSAMIApiUrl)\(kGetUsers)\(self.userCache.userID!)\(kGetProperties)\(kAppParameter)")
            //url = self.addAthorizationHeader(url: url!)

        }
        return url!
    }
    
    func getDeviceListUrl() -> URL {
        var url = URL(string : "\(kSAMIApiUrl)\(kGetUsers)\(self.userCache.userID!)\(kGetDeviceTypes)")
       // url = self.addAthorizationHeader(url: url!)
        return url!
    }
    
    func postDeviceUrl() -> URL {
        var url = URL(string : "\(kSAMIApiUrl)\(kPostDevice)")
       // url = self.addAthorizationHeader(url: url!)
        return url!
    }
    
    func postMessageUrl() -> URL {
        var url = URL(string : "\(kSAMIApiUrl)\(kPostMessages)")
      //  url = self.addAthorizationHeader(url: url!)
        return url!
    }
    
    private func authorizeDeviceUrl(device : UserDevice) -> URL {
        let url = URL(string: "\(kSAMIApiUrl)\(kRegisterDevice)\(device.udidString)\(kAuthProviderDevice)")
        //url = self.addAthorizationHeader(url: url!)
        return url!
    }
    
    
    private func getDeviceMessagesUrl() -> URL {
        var url = URL(string: "\(kSAMIApiUrl)\(kGetMessages)")
        url = self.addAthorizationHeader(url: url!)
        return url!
    }
    
    
    func getUserDevices() -> Observable<[[String : Any]]> {
        return Observable.create { observer in
            let url = self.getDeviceListUrl()
            var request = self.requestWithToken(token: NRUserSession.sharedInstance.accessToken ?? "", url: url)
            request.httpMethod = "GET"
            let session = URLSession.shared.dataTask(with: request) {
                data, response, error in
            if data != nil {
                let dictionaryRepresentation = self.convertStringToDictionary(text: String(data: data!, encoding: .utf8)!)
                let proxyData = dictionaryRepresentation?["data"] as? [String : [[String : Any]]]
                if proxyData != nil {
                    let dataToReturn = proxyData!["devices"]
                    observer.onNext(dataToReturn ?? [[:]])
                }
            } else {
                observer.onError(error!)
            }
                self.log(foodProcessingOrWithings: false, url: url.absoluteString, requestMethod: request.httpMethod, urlResponse: response, requestBody: nil, error: error, responceBody: nil)
                observer.onCompleted()

        }
            session.resume()
            return Disposables.create {
                session.cancel()
            }
        }
    }
    
    func saveDevice(device : UserDevice) -> Observable<[String : Any]> {
        return Observable.create { observer in
            let url = self.postDeviceUrl()
            var request = self.requestWithToken(token: NRUserSession.sharedInstance.accessToken ?? "", url: url)
            request.httpMethod = "POST"
            request.setValue("application/json", forHTTPHeaderField: "Content-Type")
            let mapper = UserDevice_to_Json()
            let dict = mapper.transform(device: device)
            
            var data = Data()
            
            let nameToCompare = dict["name"] as? String
            
            do {
                data = try JSONSerialization.data(withJSONObject: dict, options: [])
            } catch let error as NSError {
                LogUtility.logToFile(error)
            }
            
            request.httpBody = data
            let session = URLSession.shared.dataTask(with: request) {
                data, response, error in
                if data != nil {
                    var dictionaryRepresentation = self.convertStringToDictionary(text: String.init(data: data!, encoding: .utf8)!)
                    let device = dictionaryRepresentation?["data"] as? [String : Any]
                    if device?["name"] as? String == nameToCompare {
                        observer.onNext(device!)
                    }
                }
                self.log(foodProcessingOrWithings: false, url: url.absoluteString, requestMethod: request.httpMethod, urlResponse: response, requestBody: nil, error: error, responceBody: nil)
                observer.onCompleted()

            }
            session.resume()
            return Disposables.create {
                session.cancel()
            }
        }

    }
    
    
    func saveUserDevice(userDevice : UserDevice) -> Observable<Bool> {
        return Observable.create { observer in
            let url = self.postDeviceUrl()
            var request = self.requestWithToken(token: NRUserSession.sharedInstance.accessToken ?? "", url: url)
            request.httpMethod = "POST"
            request.setValue("application/json", forHTTPHeaderField: "Content-Type")
            let mapper = UserDevice_to_Json()
            let dict = mapper.transform(device: userDevice)
            
            var data = Data()
            
            let nameToCompare = dict["name"] as? String
            
            do {
                data = try JSONSerialization.data(withJSONObject: dict, options: [])
            } catch let error as NSError {
                LogUtility.logToFile(error)
            }
            
            request.httpBody = data
            
            let session = URLSession.shared.dataTask(with: request) {
                    data, response, error in
                if data != nil {
                    var dictionaryRepresentation = self.convertStringToDictionary(text: String.init(data: data!, encoding: .utf8)!)
                    let device = dictionaryRepresentation?["data"] as? [String : Any]
                    if device?["name"] as? String == nameToCompare {
                        observer.onNext(true)
                    } else {
                        observer.onNext(false)
                    }
                }
                self.log(foodProcessingOrWithings: false, url: url.absoluteString, requestMethod: request.httpMethod, urlResponse: response, requestBody: nil, error: error, responceBody: nil)
                observer.onCompleted()
            }
            session.resume()
            return Disposables.create {
                session.cancel()
            }
        }
    }
    
    func authorizeDevice(device: UserDevice) -> Observable<URL> {
        return Observable.create { observer in
            let url = self.authorizeDeviceUrl(device: device)
            var request = URLRequest(url:url)
            request.httpMethod = "GET"
            request.setValue("application/json", forHTTPHeaderField: "Content-Type")
            let session = URLSession.shared.dataTask(with: request) {
                data, response, error in
                if error == nil {
                    observer.onNext(request.url!)
                }
                self.log(foodProcessingOrWithings: false, url: url.absoluteString, requestMethod: request.httpMethod, urlResponse: response, requestBody: nil, error: error, responceBody: nil)
                observer.onCompleted()
            }
            session.resume()
            return Disposables.create {
                session.cancel()
            }
        }
    }
    
    
    func unathorizeDevice(device : UserDevice) -> Observable<Bool> {
        return Observable.create { observer in
            let url = self.authorizeDeviceUrl(device: device)
            var request = self.requestWithToken(token: NRUserSession.sharedInstance.accessToken ?? "", url: url)
            request.httpMethod = "DELETE"
            request.setValue("application/json", forHTTPHeaderField: "Content-Type")
            let session = URLSession.shared.dataTask(with: request) {
                data, response, error in
                if error == nil {
                    observer.onNext(true)
                } else {
                    observer.onError(error!)
                }
                self.log(foodProcessingOrWithings: false, url: url.absoluteString, requestMethod: request.httpMethod, urlResponse: response, requestBody: nil, error: error, responceBody: nil)
                observer.onCompleted()
            }
            session.resume()
            return Disposables.create {
                session.cancel()
            }
        }
    }
    
    
    private func stringFromHttpParameters(params : [String: Any]) -> String {
        
        var parametersString = ""
        for (key, value) in params {
            let value = value as? String 
            parametersString = parametersString + key + "=" + value! + "&"
        }
        parametersString = parametersString.substring(to: parametersString.index(before: parametersString.endIndex))
        return parametersString.addingPercentEncoding(withAllowedCharacters: .urlHostAllowed)!
    }
    
    func addStringTo(url : URL, string : String) -> URL {
        var urlString = String(describing: url)
        urlString.append(String(format: "&%@", string))
        return URL(string:urlString)!
    }
        
    func getDeviceMessagesFor(device : UserDevice, startDate : Date, endDate : Date) -> Observable<[[String:Any]]> {
        return Observable.create { observer in
            let url = self.getDeviceMessagesUrl()
            var dict : [String : Any] = [:]
            dict["sdid"] = device.udidString
            dict["startDate"] = String(Int64(startDate.timeIntervalSince1970 * 1000.0))
            dict["endDate"] = String(Int64(endDate.timeIntervalSince1970 * 1000.0))
            if defaults.value(forKey: kFitBitNextToken) != nil {
                let nextToken = defaults.value(forKey: kFitBitNextToken) as! String
                dict["offset"] = nextToken
            }
            
            var request = URLRequest(url: self.addStringTo(url: url, string: self.stringFromHttpParameters(params: dict)))
            request.httpMethod = "GET"
            request.setValue("application/json", forHTTPHeaderField: "Content-Type")

            
            let session = URLSession.shared.dataTask(with: request) {
                data, response, error in
                if data != nil {
                    var dictionaryRepresentation = self.convertStringToDictionary(text: String.init(data: data!, encoding: .utf8)!)
                    let messages = dictionaryRepresentation?["data"] as? [[String : Any]]
                    if device.dtidString == kFitBitDeviceTypeId {
                        if dictionaryRepresentation?["next"] != nil {
                             defaults.set(dictionaryRepresentation?["next"] as? String, forKey: kFitBitNextToken)
                        } else {
                            defaults.set(dictionaryRepresentation?[""] as? String, forKey: kFitBitNextToken)
                        }
                    }

                    observer.onNext(messages ?? [[String : Any]]())
                } else {
                    observer.onError(error!)
                }
                if device.dtidString == WITHINGS_DTID {
                    LogUtility.logToFile("Synchronizing withings messages")
                }
                self.log(foodProcessingOrWithings: true, url: url.absoluteString, requestMethod: request.httpMethod, urlResponse: response, requestBody: dict, error: error, responceBody: data)
                observer.onCompleted()
            }
            session.resume()
            return Disposables.create {
                session.cancel()
            }
        }

    }
    
    
    func isEmailLocked  (token : String) -> Observable<Bool> {
        return Observable.create{observer in
            let url = self.checkEmailUrlFor(token: token)
            var locked = true
            var request = URLRequest(url: url)
            request.httpMethod = "GET"
            let session = URLSession.shared.dataTask(with: request) {
                data, response, error in
                if error == nil && data != nil{
                    if let httpResp = response as? HTTPURLResponse {
                        if httpResp.statusCode == 200 {
                            let dict = self.convertStringToDictionary(text: String(data: data!, encoding: .utf8)!)
                            let isBlacklisted = dict?["isBlacklisted"] as? Bool ?? true
                            let isValid = dict?["isValid"] as? Bool ?? false
                            if isBlacklisted == false && isValid == true {
                                locked = false
                            }
                        } else {
                            locked = false
                        }
                    } else {
                        locked = false
                    }
                   
                } else if error != nil {
                    LogUtility.logToFile("Error checking email ", error!)
                    locked = false
                }
                observer.onNext(locked)
                self.log(foodProcessingOrWithings: false, url: url.absoluteString, requestMethod: request.httpMethod, urlResponse: response, requestBody: nil, error: error, responceBody: nil)
                observer.onCompleted()
                
            }
            session.resume()
            
            return Disposables.create {
                session.cancel()
            }
        }
    }
    
    
    private func checkEmailUrlFor (token : String) -> URL {
        let url = URL(string: "\(kBlackListUrl)\(kCheckArtikToken)\(token)\(kBlacklistCheck)")
        return url!
    }
    
    func convertStringToDictionary(text: String) -> [String : Any]? {
        if let data = text.data(using: String.Encoding.utf8) {
            do {
                return try JSONSerialization.jsonObject(with: data, options: []) as? [String : Any]
            } catch let error as NSError {
                LogUtility.logToFile(error)
            }
        }
        return nil
    }
    
    
}

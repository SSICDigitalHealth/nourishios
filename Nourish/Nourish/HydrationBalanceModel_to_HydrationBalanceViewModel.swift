//
//  HydrationBalanceModel_to_HydrationBalanceViewModel.swift
//  Nourish
//
//  Created by Vlad Birukov on 29.08.17.
//  Copyright © 2017 SSIC. All rights reserved.
//

import Foundation

class HydrationBalanceModel_to_HydrationBalanceViewModel {
    let mapperElement = CalorificFoodModel_to_CalorificFoodViewModel()

    func transform(model: HydrationBalanceModel) -> HydrationBalanceViewModel {
        let hydrationBalanceViewModel = HydrationBalanceViewModel()
        hydrationBalanceViewModel.total = model.total
        hydrationBalanceViewModel.unit = model.unit
        hydrationBalanceViewModel.message = model.message
        if model.arrHydrationElement != nil {
            hydrationBalanceViewModel.arrHydrationElement = [ElementViewModel]()
            hydrationBalanceViewModel.arrHydrationElement = self.mapperElement.transform(model: model.arrHydrationElement!)
            hydrationBalanceViewModel.arrHydrationElement = hydrationBalanceViewModel.arrHydrationElement?.filter({$0.consumedState != 0.0})
        }
        return hydrationBalanceViewModel
    }
}

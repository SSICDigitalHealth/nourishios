//
//  FavoriteCacheDataStore.swift
//  Nourish
//
//  Created by Nova on 7/13/17.
//  Copyright © 2017 SSIC. All rights reserved.
//

import UIKit
import RealmSwift

class FavoriteCacheDataStore: NSObject {

    func storeCache(cache : Data, date : Date) {
        let realm = try! Realm()
        let objs = realm.objects(FavoriteCache.self)
        if objs.count > 0 {
            try! realm.write {
                realm.delete(objs)
            }
        }
        
        let fav = FavoriteCache()
        fav.data = cache
        fav.date = date
        
        
        try! realm.write {
            realm.add(fav)
        }
        
    }
    
    func fetchLastCache() -> Data? {
        let realm = try! Realm()
        let obj = realm.objects(FavoriteCache.self).sorted(byKeyPath: "date", ascending : false).first
        return obj?.data
    }
    
}

//
//  NutrientDetailInformationView.swift
//  Nourish
//
//  Created by Vlad Birukov on 11.08.17.
//  Copyright © 2017 SSIC. All rights reserved.
//

import UIKit

protocol NutrientDetailInformationProtocol : BaseViewProtocol {
    func reloadTable()
    func registerTableViewCells()
    func config() -> ProgressConfig?
}

class NutrientDetailInformationView: BaseView, NutrientDetailInformationProtocol {
    @IBOutlet weak var detailTableView: UITableView!
    
    @IBOutlet  var presenter: NutrientDetailInformationPresenter!
    var controller: NutritionDetailViewController?
    var progressConfig: ProgressConfig?
    
    let heightCell = 30
    let heightHeaderCell = 46
    
    func config() -> ProgressConfig? {
        return self.progressConfig
    }
    
    override func viewDidLoad() {
        self.xibAutoLayouting(nibName: "NutrientDetailInformationView")
        settingTableCell()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.basePresenter = presenter
        self.presenter.nutrientDetailInformationView = self
        
        self.registerTableViewCells()
        super.viewWillAppear(animated)
        self.stopActivityAnimation()
    }
    
   
    func reloadTable() {
        detailTableView.reloadData()
    }
    
    
    func registerTableViewCells() {
        self.detailTableView?.register(UINib(nibName: "NutrientHeaderTableViewCell", bundle: nil), forCellReuseIdentifier: "cellNutrientHeader")
        self.detailTableView?.register(UINib(nibName: "NutrientTableViewCell", bundle: nil), forCellReuseIdentifier: "cellNutrien")
        self.detailTableView?.register(UINib(nibName: "HeaderNutrientTableViewCell", bundle: nil), forCellReuseIdentifier: "cellHeaderNutrient")
        self.detailTableView?.register(UINib(nibName: "NutrientRichTableViewCell", bundle: nil), forCellReuseIdentifier: "cellNutrientRich")
    }
    
    private func settingTableCell() {
        detailTableView.rowHeight = UITableViewAutomaticDimension
        detailTableView.estimatedRowHeight = CGFloat(self.heightCell)
        detailTableView.sectionHeaderHeight = UITableViewAutomaticDimension
        detailTableView.estimatedSectionHeaderHeight = CGFloat(self.heightHeaderCell)
    }

}

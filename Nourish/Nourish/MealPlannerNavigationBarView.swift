//
//  MealPlannerNavigationBarView.swift
//  Nourish
//
//  Created by Vlad Birukov on 21.08.17.
//  Copyright © 2017 SSIC. All rights reserved.
//

import UIKit

class MealPlannerNavigationBarView: BaseView {
    @IBOutlet weak var resetButton: UIButton!
    
    override func viewDidLoad() {
        self.xibAutoLayouting(nibName: "MealPlannerNavigationBarView")
        self.setupResetButton()
        settingNavigation()
    }
    
    private func setupResetButton() {
        #if OPTIFASTVERSION
            self.resetButton.setTitle("", for: .normal)
            self.resetButton.setImage(#imageLiteral(resourceName: "reset"), for: .normal)
        #else
            self.resetButton.setImage(nil, for: .normal)
            self.resetButton.setTitle("RESET", for: .normal)
        #endif
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.stopActivityAnimation()
    }
    
    func settingNavigation() {
        self.layer.borderColor = UIColor.white.cgColor
        self.layer.borderWidth = 0.1
        self.layer.shadowColor = UIColor.black.cgColor
        self.layer.shadowOffset = CGSize(width: 0.0, height: 1.0)
        self.layer.shadowRadius = 1.5
        self.layer.shadowOpacity = 0.2
        self.layer.masksToBounds = false
    }
}

//
//  StressDetailTableViewCell.swift
//  Nourish
//
//  Created by Vlad Birukov on 17.08.17.
//  Copyright © 2017 SSIC. All rights reserved.
//

import UIKit

class StressDetailTableViewCell: UITableViewCell {
    @IBOutlet weak var bpmValue: UILabel!
    @IBOutlet weak var timeValue: UILabel!
    @IBOutlet weak var bottomLine: UIView!


    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func setupWith(model: DailyStressModel) {
        let currentColor = stressLevel.textColorForStress(stress: model.stressValue!)
        if currentColor != NRColorUtility.undeterminedTableViewStressCellFontColor() {
            self.bpmValue.font =  UIFont(name: "Campton-Bold", size: 20)
        }
        self.bpmValue.textColor = currentColor
        self.bpmValue.text = String(format: "%.0f", model.bpmValue!)
        self.timeValue.text = self.format(date: model.dateToShow!)
    }
    
    private func format(date:Date) ->String {
        let df = DateFormatter()
        df.dateFormat = "hh:mm a"
        return df.string(from: date)
    }
}

//
//  MealPlannerModel_to_MealPlannerViewModel.swift
//  Nourish
//
//  Created by Vlad Birukov on 29.08.17.
//  Copyright © 2017 SSIC. All rights reserved.
//

import Foundation

class MealPlannerModel_to_MealPlannerViewModel {
    func transform(model: MealPlannerModel) -> MealPlannerViewModel {
        let mealPlannerViewModel = MealPlannerViewModel()
        mealPlannerViewModel.targetCalories = model.targetCalories
        mealPlannerViewModel.score = model.score
        
        let preferenceMealPlannerViewModel = PreferenceMealPlannerViewModel()
        preferenceMealPlannerViewModel.breakfast = model.preference.breakfast
        preferenceMealPlannerViewModel.dinner = model.preference.dinner
        preferenceMealPlannerViewModel.snacks = model.preference.snacks
        preferenceMealPlannerViewModel.lunch = model.preference.lunch
        
        mealPlannerViewModel.preference = preferenceMealPlannerViewModel
        
        if model.meal != nil {
            model.meal?.sort(by: { $0.nameOccasion.rawValue < $1.nameOccasion.rawValue})
            
            mealPlannerViewModel.meal = [MealViewModel]()
            for element in model.meal! {
                let mealViewModel = MealViewModel()
                
                if element.detail.count > 0 {
                    for item in element.detail {
                        mealViewModel.detail.append(self.transform(model: item))
                        
                    }
                }
                
                mealViewModel.nameOccasion = element.nameOccasion
                mealViewModel.calories = element.calories
                
                mealViewModel.nameFood = element.nameFood
                
                mealPlannerViewModel.meal?.append(mealViewModel)
                
            }
        }
        
        return mealPlannerViewModel
    }
    
    private func transform(model: OccasionMealPlannerModel) -> OccasionMealPlannerViewModel {
        let occasionMealPlannerModel = OccasionMealPlannerViewModel()
        if model.message != nil {
            occasionMealPlannerModel.message = model.message
        }
        
        if model.occasion != nil {
            occasionMealPlannerModel.occasion = [(foodSearchModel, RecipiesViewModel?)]()

            for occasion in model.occasion! {
                occasionMealPlannerModel.occasion?.append((occasion.0, self.transform(recipies: occasion.1)))
            }
        }
        
        return occasionMealPlannerModel
    }
    
    private func transform(recipies: RecipiesModel?) -> RecipiesViewModel? {
        
        if recipies != nil {
            let recipiesModel = RecipiesViewModel()
            recipiesModel.nameFood = (recipies?.nameFood)!
            
            if recipies?.image != nil {
                recipiesModel.image = recipies?.image
            }
            
            if recipies?.groupId != nil {
                recipiesModel.groupId = recipies?.groupId
            }
            
            recipiesModel.foodId = (recipies?.foodId)!
            recipiesModel.numberCal = (recipies?.numberCal)!
            recipiesModel.unit = (recipies?.unit)!
            recipiesModel.grams = (recipies?.grams)!
            recipiesModel.amount = (recipies?.amount)!
            recipiesModel.ingredient = (recipies?.ingredient)!
            recipiesModel.instructions = (recipies?.instructions)!
            recipiesModel.isFavorite = (recipies?.isFavorite)!
            
            return recipiesModel
        }
        return nil
    }
}

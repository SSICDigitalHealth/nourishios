//
//  Combo_to_json.swift
//  NourIQ
//
//  Created by Igor on 12/23/17.
//  Copyright © 2017 SSIC. All rights reserved.
//

import UIKit

typealias combinedRecord = (record : MealRecordCache, nutrs : [NestleNutrs])

class Combo_to_json: NSObject {

    let mealmapper = MealRecordCache_to_Json()
    
    func transform(array : [comboObject]) -> [[String : Any]] {
        return array.map {return self.transform(object: $0)}
    }
    
    func transform(object : comboObject) -> [String : Any] {
        var dict = [String : Any]()
        
        let records = object.dateID.records
        let date = object.dateID.date
        let nutrs = object.nutrs
        
        var combinedArray = [combinedRecord]()
        
        for record in records {
            combinedArray.append(self.getCombinedRecord(record: record, nutrs: nutrs))
        }
        
        dict["meals"] = self.combinedJson(arr: combinedArray)
        dict["startOfDay"] = date.timeIntervalSince1970
        
        return dict
    }
    
    private func mapNutrs(array : [NestleNutrs]) -> [[String : Any]] {
        return array.map {return self.mapNutr(nutr: $0)}
    }
    
    private func mapNutr(nutr : NestleNutrs) -> [String : Any] {
        var dict = [String : Any]()
        
        dict["density"] = nutr.densityPerGram
        dict["nutrient"] = nutr.name
        dict["unit"] = nutr.unit
        
        return dict
    }
    
    private func combinedToJson(object : combinedRecord) -> [String : Any] {
        let userid = NRUserSession.sharedInstance.userID ?? ""
        var mealDict = self.mealmapper.transform(meal: object.record, userID: userid, includingOcasion: true)
        
        let nutrsDict = self.mapNutrs(array: object.nutrs)
        
        mealDict["nutrients"] = nutrsDict
        
        return mealDict
    }
    
    private func combinedJson(arr : [combinedRecord]) -> [[String : Any]] {
        return arr.map {return self.combinedToJson(object: $0)}
    }
    
    private func getCombinedRecord(record : MealRecordCache, nutrs : [nutrientsWithID]) -> combinedRecord {
        let foodIDString = record.idString
        
        let filtered = nutrs.filter {$0.ident == foodIDString}
        var array = [NestleNutrs]()
        
        if let obj = filtered.first {
            array.append(contentsOf: obj.nutrs)
        }
        let combine = combinedRecord(record : record, nutrs : array)
        return combine
    }
    
    
}

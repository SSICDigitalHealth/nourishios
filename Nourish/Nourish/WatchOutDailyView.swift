//
//  WatchOutDailyView.swift
//  Nourish
//
//  Created by Andrey Zmushko on 7/24/17.
//  Copyright © 2017 SSIC. All rights reserved.
//

import UIKit

class WatchOutDailyView: ProgressContentView {
    @IBOutlet weak var sugarsChartView: WatchOutChartView!
    @IBOutlet weak var saltChartView: WatchOutChartView!
    @IBOutlet weak var satFatChartView: WatchOutChartView!
    
    @IBOutlet weak var sugarsLabel: UILabel!
    @IBOutlet weak var saltLabel: UILabel!
    @IBOutlet weak var satFatLabel: UILabel!
    
    @IBOutlet weak var sugarsScorePercents: UILabel!
    @IBOutlet weak var saltScorePercents: UILabel!
    @IBOutlet weak var satFatScorePercents: UILabel!

    override func reloadDataWith(config: ProgressConfig) {
        super.reloadDataWith(config: config)
        self.setNeedsDisplay()
    }
    
    override func draw(_ rect: CGRect) {
        super.draw(rect)
        
        if let model = self.model as! MicroelementViewModel! {
            if let data = model.data?[0] {
                for element in data.elements {
                    let consumed = element.element.consumedMicroelement
                    let target = element.element.targetMicroelement
                    let label = String(format: "%@/%@ %@", consumed.roundToTenIfLessOrMore(target: target), target.roundToTenIfLessOrMore(target: target), element.element.unit)
                    let percents = String(format: "%.0f%%", (consumed / target) * 100  )
                    let color = consumed < target ? UIColor.black : UIColor.white
                    switch element.type {
                    case .sugars:
                        self.sugarsChartView.setupWith(Data: (consumed, target))
                        self.sugarsScorePercents.textColor = color
                        self.sugarsScorePercents.text = percents
                        self.sugarsLabel.text = label
                    case .saturatedFat:
                        self.saltChartView.setupWith(Data: (consumed, target))
                        self.saltScorePercents.textColor = color
                        self.saltScorePercents.text = percents
                        self.saltLabel.text = label
                    case .salt:
                        self.satFatChartView.setupWith(Data: (consumed, target))
                        self.satFatScorePercents.textColor = color
                        self.satFatScorePercents.text = percents
                        self.satFatLabel.text = label
                    }
                }
            }
        }                
    }
}

//
//  CaloriesDailyAverageView.swift
//  Nourish
//
//  Created by Vlad Birukov on 20.06.17.
//  Copyright © 2017 SSIC. All rights reserved.
//

import UIKit

protocol CaloriesDailyAverageProtocol : BaseViewProtocol {
    func setInformation(caloriesDailyModel: CaloriesDailyAverageViewModel)
    func config() -> ProgressConfig?
}

class CaloriesDailyAverageView: ProgressView, CaloriesDailyAverageProtocol {

    @IBOutlet weak var dailyAverageView: UIView!
    @IBOutlet weak var dailyAverageCalories: UILabel!
    @IBOutlet weak var overTargetCalories: UILabel!
    var presenter: CaloriesDailyAveragePresenter!
    @IBOutlet weak var topLabel: NSLayoutConstraint!
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.presenter = nil
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.presenter = CaloriesDailyAveragePresenter()
        if let config = self.progressConfig {
            self.renderView(config)
        }
        
        self.basePresenter = presenter
        self.presenter.caloriesDailyAverageView = self
        
        super.viewWillAppear(animated)
        self.removeActivityIndicator()
    }
    
    override func renderView(_ config: ProgressConfig) {
        super.renderView(config)
        self.dailyAverageView =  UINib(nibName: "CaloriesDailyAverageView", bundle: nil).instantiate(withOwner: self, options: nil)[0] as! UIView
        self.loadContent(contentView: dailyAverageView)
        deleteHeader(config: config)
    }
    
    override func gestureTapAction() {
        if let hook = tapHook {
            hook(.calories)
        }
    }
    
    func setInformation(caloriesDailyModel: CaloriesDailyAverageViewModel) {
        self.dailyAverageCalories.text = String(format: "%.0f %@", caloriesDailyModel.caloriesDailyAverage, "kcal daily average")
        
        if caloriesDailyModel.caloriesOverTarget > 0 {
            self.overTargetCalories.text = String(format: "%.0f %@",  caloriesDailyModel.caloriesOverTarget, "kcal over target")
        } else {
            self.overTargetCalories.text = String(format: "%.0f %@",  abs(caloriesDailyModel.caloriesOverTarget), "kcal below target")
        }
    }
    
    func deleteHeader(config: ProgressConfig) {
        self.captionHolderView.isHidden = true
        self.captionTop.constant = 0

        if config.state == .card {
            self.contentHolderTop.constant = 0
            self.contentHolderBottom.constant = 6.5
        }
    }
}


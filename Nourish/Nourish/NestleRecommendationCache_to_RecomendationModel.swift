//
//  NestleRecommendationCache_to_RecomendationModel.swift
//  Nourish
//
//  Created by Vlad Birukov on 12.10.2017.
//  Copyright © 2017 SSIC. All rights reserved.
//

import Foundation

class NestleRecommendationCache_to_RecomendationModel {
    
    func transform(model: NestleRecommendationsCache) -> RecomendationModel {
        let recomendationModel = RecomendationModel()
        
        if model.caption != nil {
            recomendationModel.caption = model.caption
        }
        
        if model.meals.count > 0 {
            for item in model.meals {
                recomendationModel.meals.append(self.transorm(model: item))
            }
        }
        
        if model.tips.count > 0 {
            for item in model.tips {
                recomendationModel.tips.append(self.transform(model: item))
            }
        }
        
        if model.dataRecipies.count > 0 {
            recomendationModel.dataRecipies = [RecipiesModel]()
            for item in model.dataRecipies {
                recomendationModel.dataRecipies?.append(self.transform(model: item))
            }
        }
        
        return recomendationModel
    }
    
    private func transorm(model: FoodSearchCacheModel) -> foodSearchModel {
        var foodsearchModel = foodSearchModel()
        foodsearchModel.amount = model.amount
        foodsearchModel.displayUnit = model.displayUnit
        foodsearchModel.unit = model.unit
        foodsearchModel.foodId = model.foodId
        foodsearchModel.foodTitle = model.foodTitle
        foodsearchModel.mealIdString = model.mealIdString
        if model.grams.value != nil {
            foodsearchModel.grams = model.grams.value
        }
        
        foodsearchModel.caloriesPerGram = model.caloriesPerGram
        foodsearchModel.calories = model.calories
        
        return foodsearchModel
    }
    
    private func transform(model: NestleCacheTipsModel) -> NestleTipsModel {
        let nestleTipsModel = NestleTipsModel()
        
        if model.componentId != nil {
            nestleTipsModel.componentId = model.componentId
        }
        
        if model.indicator != nil {
            nestleTipsModel.indicator = model.indicator
        }
        
        if model.tipTitle != nil {
            nestleTipsModel.tipTitle = model.tipTitle
        }
        
        if model.tipType != nil {
            nestleTipsModel.tipType = model.tipType
        }
        
        if model.messages.count > 0 {
            nestleTipsModel.messages = [String]()
            
            for stringMessage in model.messages {
                nestleTipsModel.messages?.append(stringMessage.stringValue)
            }
        }
        
        return nestleTipsModel
    }
    
    private func transform(model: RecipiesCacheModel) -> RecipiesModel {
        let recipiesModel = RecipiesModel()
        recipiesModel.foodId = model.foodId
        recipiesModel.amount = model.amount
        recipiesModel.nameFood = model.nameFood
        
        if model.groupId != nil {
            recipiesModel.groupId = model.groupId
        }
        
        if model.pathImage != "" {
            recipiesModel.image = self.loadImageFromPath(path: model.pathImage)
        }
        
        recipiesModel.numberCal = model.numberCal
        recipiesModel.grams = model.grams
        recipiesModel.unit = model.unit
        
        if model.ingredient.count > 0 {
            for stringObj in model.ingredient {
                recipiesModel.ingredient.append(stringObj.stringValue)
            }
        }
        
        if model.instruction.count > 0 {
            for stringObj in model.instruction {
                recipiesModel.instructions.append(stringObj.stringValue)
            }
        }
        
        recipiesModel.isFavorite = model.isFavorite
        
        return recipiesModel
    }
    
    private func loadImageFromPath(path: String) -> UIImage? {
        var imageFood: UIImage?
        
        let documentsURL = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first!
        let filePath = documentsURL.appendingPathComponent(path).path
        
        if FileManager.default.fileExists(atPath: filePath) {
            imageFood = UIImage(contentsOfFile: filePath)
        }
        
        return imageFood
    }
}

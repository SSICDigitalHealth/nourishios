//
//  PortionSizeDetailsCell.swift
//  Optifast
//
//  Created by Gena Mironchyk on 10/12/17.
//  Copyright © 2017 SSIC. All rights reserved.
//

import UIKit

class PortionSizeDetailsCell: UITableViewCell {
    
    @IBOutlet weak var foodImageView : UIImageView!
    @IBOutlet weak var looksLikeImageView : UIImageView!
    @IBOutlet weak var segmentedControl : ScrollableSegmentedControl!
    @IBOutlet weak var descriptionLabel : UILabel!
    @IBOutlet weak var labelBottomConstraint : NSLayoutConstraint!
    @IBOutlet weak var pictureLeftConstraint : NSLayoutConstraint!
    @IBOutlet weak var pictureRightConstraint : NSLayoutConstraint!
    private var details = [ComponentDetail]()
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    
    func setupWith(details : [ComponentDetail]) {
        var arrayOfTitles = [String]()
        self.details = details
        let _ = details.map({arrayOfTitles.append($0.foodName!)})
        self.setupSegmentedControl(titles: arrayOfTitles)
      //  self.updateWithIndex(index: 0)
    }
    
    
    private func setupSegmentedControl(titles : [String]) {
        self.segmentedControl.segmentStyle = .textOnly
        let attributes = [NSFontAttributeName : UIFont(name: "Campton-Book", size: 15)!,NSForegroundColorAttributeName : NRColorUtility.hexStringToUIColor(hex: "808080")]
        let selectedAttributes = [NSFontAttributeName : UIFont(name: "Campton-Book", size: 15)!,NSForegroundColorAttributeName : NRColorUtility.appBackgroundColor()]
        self.segmentedControl.setTitleTextAttributes(attributes as [NSAttributedStringKey : Any], for: .normal)
        self.segmentedControl.setTitleTextAttributes(selectedAttributes as [NSAttributedStringKey : Any], for: .selected)
        
        self.segmentedControl.tintColor =  NRColorUtility.appBackgroundColor()
        if self.segmentedControl.numberOfSegments != 0 {
            self.segmentedControl.selectedSegmentIndex = -1
            for _ in 0...self.segmentedControl.numberOfSegments-1{
                self.segmentedControl.removeSegment(at: 0)
            }
        }
        
        for index in 0...titles.count-1 {
            self.segmentedControl.insertSegment(withTitle: titles[index], at: index)
            
        }
        self.segmentedControl.layoutSubviews()
        self.segmentedControl.addTarget(self, action: #selector(updateWith(segmentedControl:)), for: .valueChanged)
        self.segmentedControl.underlineSelected = true
        self.segmentedControl.selectedSegmentIndex = 0

    }
    
    func updateWith(segmentedControl : ScrollableSegmentedControl) {
        self.updateWithIndex(index: segmentedControl.selectedSegmentIndex)
    }
    
    private func updateWithIndex(index : Int) {
        if index >= 0 {
            let detailsToWork = self.details[index]
     
        
            var constantForLooksLike = CGFloat(0.0)


            if self.looksLikeImageView.image != nil {
                let imageDataToCompare = UIImagePNGRepresentation(self.looksLikeImageView.image!)
                let imageDataToCompareNew = UIImagePNGRepresentation(detailsToWork.looksLikeImage!)
                if imageDataToCompare != imageDataToCompareNew {
                    constantForLooksLike = self.frame.size.width/2.0
                }
            }

            var constantForFoodImage = CGFloat(0.0)
            if self.foodImageView.image != nil {
                let imageDataToCompare = UIImagePNGRepresentation(self.foodImageView.image!)
                let imageDataToCompareNew = UIImagePNGRepresentation(detailsToWork.foodImage!)
                if imageDataToCompare != imageDataToCompareNew {
                    constantForFoodImage = self.frame.size.width/2.0
                }
            }

            self.pictureLeftConstraint.constant -= constantForFoodImage
            self.pictureRightConstraint.constant += constantForLooksLike
            self.labelBottomConstraint.constant -= 100
            UIView.animate(withDuration: 0.5, animations: {
                self.layoutIfNeeded()
            }, completion: { completed in
                self.looksLikeImageView.image = detailsToWork.looksLikeImage
                self.foodImageView.image = detailsToWork.foodImage
                self.descriptionLabel.text = detailsToWork.descriptionText
                self.pictureLeftConstraint.constant += constantForFoodImage
                self.pictureRightConstraint.constant -= constantForLooksLike
                self.labelBottomConstraint.constant += 100
                UIView.animate(withDuration: 0.5, animations: {
                    self.layoutIfNeeded()
                })
            })
        
        }
    }
        
    
}

//
//  CaloricProgressInteractor.swift
//  Nourish
//
//  Created by Vlad Birukov on 31.08.17.
//  Copyright © 2017 SSIC. All rights reserved.
//

import Foundation
import RxSwift

class CaloricProgressInteractor {
    private let repository = CaloricRepository()
    let userRepo = UserRepository.shared
    
    func execute(startDate: Date, endDate: Date) -> Observable <CaloricProgressModel> {
        let calendar = Calendar.current
        
        if calendar.isDateInToday(startDate) {
            let calProgress = self.repository.fetchCaloricProgressModelFor(startDate: startDate, endDate: endDate)
            let user = self.userRepo.getCurrentUser(policy: .DefaultPolicy)
            let zip = Observable.zip(calProgress, user, resultSelector: {calories, user -> CaloricProgressModel in
                let bmr = user.calculateAndStoreCaloriePlan()
                
                if let cal = calories.caloricState?.first {
                    let tuple = caloricProgress(subtitle: cal.subtitle, cal: (bmrCal: cal.cal.bmrCal, maxConsumedCal: bmr, maxBurnedCal: cal.cal.maxBurnedCal, userConsumedCal: cal.cal.userConsumedCal, userBurnedCal: cal.cal.userBurnedCal))
                    
                    calories.caloricState = [tuple]
                }
                
                return calories
            })
            return zip
        } else {
            return self.repository.fetchCaloricProgressModelFor(startDate : startDate, endDate : endDate)
        }
        
//        return Observable<CaloricProgressModel>.create{(observer) -> Disposable in
//            let pause = Int(arc4random_uniform(4))
//            let dispat = DispatchTime.now() + .seconds(pause)
//            DispatchQueue.main.asyncAfter(deadline: dispat, execute: {
//                let metric = [true, false]
//                let caloricProgressModel = CaloricProgressModel()
//                caloricProgressModel.caloricState = [(subtitle: Date, cal: (bmrCal: Double, maxConsumedCal: Double, maxBurnedCal: Double, userConsumedCal: Double, userBurnedCal: Double))]()
//                
//                caloricProgressModel.activityLevel = .Active
//                if Calendar.current.compare(startDate, to: endDate, toGranularity: .day) == .orderedSame {
//                    caloricProgressModel.isMetric = metric[self.generateRandomBetween(max: 1, min: 0)]
//                    caloricProgressModel.weeklyPlan = Double(self.generateRandomBetween(max: 30, min: 1))
//                    caloricProgressModel.caloricState?.append((subtitle: startDate, cal:(bmrCal: Double(self.generateRandomBetween(max: 1400, min: 1000)), maxConsumedCal: Double(self.generateRandomBetween(max: 1400, min: 1000)), maxBurnedCal: Double(self.generateRandomBetween(max: 1400, min: 1000)), userConsumedCal: Double(self.generateRandomBetween(max: 2900, min: 0)), userBurnedCal: Double(self.generateRandomBetween(max: 2900, min: 0)))))
//                } else {
//                    for i in 0...self.daysBetweenDates(startDate: startDate, endDate: endDate) {
//                        caloricProgressModel.caloricState?.append((subtitle: self.addDay(startDate: startDate, number: i), cal:(bmrCal: Double(self.generateRandomBetween(max: 1400, min: 1000)), maxConsumedCal: Double(self.generateRandomBetween(max: 1400, min: 1000)), maxBurnedCal: Double(self.generateRandomBetween(max: 1400, min: 1000)), userConsumedCal: Double(self.generateRandomBetween(max: 2900, min: 0)), userBurnedCal: Double(self.generateRandomBetween(max: 2900, min: 0)))))
//
//                    }
//                }
//    
//                observer.onNext(caloricProgressModel)
//                observer.onCompleted()
//            })
//            return Disposables.create()
//        }
    }
    
    private func addDay(startDate: Date, number: Int) -> Date {
        return Calendar.current.date(byAdding: .day, value: number, to: startDate)!
    }
    
    private func daysBetweenDates(startDate: Date, endDate: Date) -> Int
    {
        let calendar = Calendar.current
        let components = calendar.dateComponents([.day], from: startDate, to: endDate)
        return components.day!
    }
    
    private func generateRandomBetween(max : Int, min : Int) -> Int {
        return min + Int(arc4random_uniform(UInt32(max - min + 1)))
    }

}

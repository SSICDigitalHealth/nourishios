//
//  NestleFoodComponentsInteractor.swift
//  Nourish
//
//  Created by Gena Mironchyk on 9/20/17.
//  Copyright © 2017 SSIC. All rights reserved.
//

import UIKit
import RxSwift

class NestleFoodComponentsInteractor: NSObject {
    private let repo = NestleFoodComponentsRepository()
    func execute() {
        let _ = self.repo.storeFoodComponents().subscribe(onNext: {stored in
            if stored {
                LogUtility.logToFile("Store food components completed")
            } else {
                LogUtility.logToFile("Store food components failed")
            }
        }, onError: {error in }, onCompleted: {}, onDisposed: {})
    }
}

//
//  RecomendationDetailModel.swift
//  Nourish
//
//  Created by Andrey Zmushko on 7/31/17.
//  Copyright © 2017 SSIC. All rights reserved.
//

import Foundation

class RecomendationDetailModel {
    var caption: String?
    var titleText = ""
    var descriptionText = ""
    var data : [foodSearchModel]?
    var dataRecipies : [RecipiesModel]?
}

class RecipiesModel {
    var foodId: String = ""
    var amount: Double = 0.0
    var nameFood: String = ""
    var groupId: String?
    var image: UIImage?
    var pathForImage = ""
    var numberCal: Double = 0.0
    var grams: Double = 0.0
    var unit: String = ""
    var ingredient = [String]()
    var instructions = [String]()
    var isFavorite = false
}

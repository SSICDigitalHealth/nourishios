//
//  NutrientsView.swift
//  Nourish
//
//  Created by Vlad Birukov on 11.09.17.
//  Copyright © 2017 SSIC. All rights reserved.
//

import UIKit

protocol NutrientsProtocol : BaseViewProtocol {
    func config() -> ProgressConfig?
    func reloadData()
    func registerCell()
}

class NutrientsView: BaseView, NutrientsProtocol {
    @IBOutlet weak var nutrientsView: UIView!
    @IBOutlet weak var nutrientsTableView: UITableView!
    var progressViewController: NewProgressViewController?
    var presenter: NutrientsPresenter!
    @IBOutlet weak var captionHeight: NSLayoutConstraint!
    
    var progressConfig: ProgressConfig?
    
    func config() -> ProgressConfig? {
        return self.progressConfig
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.presenter.nutrientsView = nil
        self.presenter = nil
    }
    
    override func viewDidLoad() {
        self.xibAutoLayouting(nibName: "NutrientsView")
        super.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.presenter = NutrientsPresenter()
        self.basePresenter = self.presenter
        self.presenter.nutrientsView = self
        self.nutrientsTableView.delegate = self.presenter
        self.nutrientsTableView.dataSource = self.presenter
        if let controller = progressViewController {
            self.presenter.progressViewController = controller
        }
        
        self.registerCell()
        super.viewWillAppear(animated)
        self.removeActivityIndicator()
    }
    
    func reloadData() {
        self.nutrientsTableView.reloadData()
        
        if let controller = progressViewController {
                controller.nutrientsHeight.constant = nutrientsTableView.contentSize.height + 120
                self.layoutIfNeeded()
            }
    }

    func registerCell() {
        self.nutrientsTableView.register(UINib(nibName: "NutrientsDetailTableViewCell", bundle: nil), forCellReuseIdentifier: "cellDetailNutrients")
    }

    func stopAnimation() {
        self.stopActivityAnimation()
    }
    
}

//
//  RecommendationCell.swift
//  Nourish
//
//  Created by Andrey Zmushko on 7/31/17.
//  Copyright © 2017 SSIC. All rights reserved.
//

import UIKit

class RecommendationCell: UITableViewCell {

    @IBOutlet weak var labelCell: UILabel!
    @IBOutlet weak var favoriteImage: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    func setupWith(model: RecomendationDetailViewModel, row: Int) {
        if let recomendationElement = model.data {
            self.labelCell.text = String(format: "%@ %@", recomendationElement[row].displayUnit, recomendationElement[row].foodTitle)
            self.labelCell.setLineSpacing(spacing: 4)
            
            if let favorite = recomendationElement[row].isFavorite {
                self.isFavorite(favorite: favorite)
            }
        }
    }
    
    func isFavorite(favorite: Bool) {
        if favorite == true {
            self.favoriteImage.setImage(UIImage(named: "fav_green"), for: .normal)
        } else {
            self.favoriteImage.setImage(UIImage(named: "fav_icon_00-1"), for: .normal)
        }
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}

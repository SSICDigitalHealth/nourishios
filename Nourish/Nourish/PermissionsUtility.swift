//
//  PermissionsUtility.swift
//  Nourish
//
//  Created by Gena Mironchyk on 6/15/17.
//  Copyright © 2017 SSIC. All rights reserved.
//

import UIKit
import HealthKit
import CoreLocation
import UserNotifications
import CoreMotion

class PermissionsUtility: NSObject, CLLocationManagerDelegate {
    
    let sharedSentiance : SENTSDK! = SENTSDK.sharedInstance() as! SENTSDK
    static let shared = PermissionsUtility()
    let locationManager = CLLocationManager()
    let motionManager = CMMotionActivityManager ()
    func permissionsWasAsked () -> Bool {
        var wasAsked = false
        if self.checkHealthKitPermissionsWasAsked() == true {
            wasAsked = true
        }
        return wasAsked
    }
    
    
    func checkHealthKitPermissionsWasAsked () -> Bool {
        let authorizationStatus = ActivityUtility.permissionsWasAsked()
        if authorizationStatus == .notDetermined {
            return false
        } else {
            return true
        }
    }
    
    private func checkLocationPermissionsWasAsked () -> Bool {
        let authStatus = CLLocationManager.authorizationStatus()
        if authStatus == .notDetermined {
            return false
        } else {
            return true
        }
    }
    
    func setupNotifications() {
        let localNots = LocalNotificationsRoutine.shared
        localNots.setupNotifications()
    }
    
    
    func askLocationPermissions () {
            self.locationManager.delegate = self
            self.locationManager.requestAlwaysAuthorization()       
    }
    
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        NotificationCenter.default.post(name: NSNotification.Name("Location permissions was asked"), object: nil)
    }
    
    func askHealthPermissions () {
        ActivityUtility.getHealthKitPermission(completion: {granted, error in
                if granted {
                    NotificationCenter.default.post(name: NSNotification.Name("Health permissions was asked"), object: nil)
                }
            })
    }
    
    func askMotionPermissions() {
        let motionActivityQueue = OperationQueue()
        self.motionManager.queryActivityStarting(from: Date(), to: Date(), to: motionActivityQueue, withHandler: {activities, error in
            NotificationCenter.default.post(name: NSNotification.Name("Motion permissions was asked"), object: nil)            
        })
    }
    
    func askPermissionsForNotifications () {
        if #available(iOS 10.0, *) {
            UNUserNotificationCenter.current().requestAuthorization(options: [.alert, .sound, .badge]) { (granted: Bool, error: Error?) in
                NotificationCenter.default.post(name: NSNotification.Name("Notifications permissions was asked"), object: nil)
                if granted == true {
                    UIApplication.shared.registerForRemoteNotifications()
                    self.setupNotifications()
                }
            }
        } else {
            let setting = UIUserNotificationSettings(types: [.alert, .badge, .sound], categories: nil)
            UIApplication.shared.registerUserNotificationSettings(setting)
            UIApplication.shared.registerForRemoteNotifications()
            self.setupNotifications()
        }
    }
    
    func initSentianceSDK(launchOptions :[UIApplicationLaunchOptionsKey: Any]?) {
        let config = SENTConfig(appId : kSentianceAPIKey, secret : kSentianceSecretKey, launchOptions : launchOptions)
        if sharedSentiance.isInitialised() == false {
            sharedSentiance.initWith(config, success: {
                LogUtility.logToFile("Sentiance SDK successfully init")
                self.startSentianceSDK()
            }, failure: {senIssueType in
                LogUtility.logToFile("Sentiance SDK failed with issue %lu", senIssueType)
            })
        }
    }
    
    func startSentianceSDK () {
        print(sharedSentiance.getVersion())
        sharedSentiance.start({status in
            switch (status?.startStatus)! {
            case .started:
                LogUtility.logToFile("Sentiance SDK started properly")
                break
            case .pending:
                LogUtility.logToFile("Something prevented the Sentiance SDK to start properly. Once fixed, the SDK will start automatically")
                break
            case .notStarted:
                LogUtility.logToFile("Sentiance SDK did not start")
                break
            }
        })
    }


}

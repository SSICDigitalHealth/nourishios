//
//  MealDiaryViewProtocol.swift
//  Nourish
//
//  Created by Nova on 11/16/16.
//  Copyright © 2016 SSIC. All rights reserved.
//

import Foundation

protocol MealDiaryViewProtocol : BaseViewProtocol {
    func setupViewForDate(date : Date)
    func showAddFoodControllerForOcasion(ocasion: Ocasion, date: Date,showPredictions:Bool)
    func changeFoodAmountWith(model : foodSearchModel)
    func reloadMealViews()
    func reloadAddTableCell(section:Int)
    func reloadSectionWithIndex(section:Int)
    func showGroupFavouriteVC(mealGroup: [MealRecordModel] )
    func showGroupDetailsControllerForOcasion(ocasion:Ocasion,date:Date,model:MealRecordModel)
    func getTableViewIndexPath(cell : UITableViewCell) -> IndexPath
    func disableNavBar()
    func enableNavBar()
}

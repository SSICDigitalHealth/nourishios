//
//  BlobCustomView.swift
//  Nourish
//
//  Created by Vlad Birukov on 08.06.17.
//  Copyright © 2017 SSIC. All rights reserved.
//

import UIKit

class BlobCustomView: UIView {
    @IBOutlet var dropsFull: [UIImageView]!
    @IBOutlet var dropsEmpt: [UIImageView]!
        
    func setupWith(hydrationValue: Double) {
        drawBlob(value: hydrationValue)
        
        self.setNeedsDisplay()
    }
    
    override func awakeFromNib(){
        super.awakeFromNib()
        drawBlob(value: 0)
    }
    
    private func drawBlob(value: Double) {
        
        for imageView in (dropsEmpt)! {
            imageView.imageNamedWithTint(named: "drop_full", tintColor: {
                NRColorUtility.progressDropColor().withAlphaComponent(0.2)
            }())
        }

        var numberDropFull = Int((value * 5).rounded(.down))
        let decimalPart = (value * 5).truncatingRemainder(dividingBy: 1)
        
        dropsFull = dropsFull.sorted(by: { $0.tag < $1.tag })
        
        numberDropFull = (numberDropFull >= 5 ? 5 : numberDropFull)
        
        if numberDropFull != 0 || decimalPart != 0 {
            for i in 0 ..< numberDropFull {
                dropsFull[i].imageNamedWithTint(named: "drop_full", tintColor: NRColorUtility.progressDropColor())
            }
            
            if numberDropFull < 5 {
                if decimalPart > 0.5 {
                    dropsFull[numberDropFull].imageNamedWithTint(named: "drop_full", tintColor: NRColorUtility.progressDropColor())
                } else if decimalPart > 0 && decimalPart <= 0.5 {
                    dropsFull[numberDropFull].imageNamedWithTint(named: "drop_half", tintColor: NRColorUtility.progressDropColor())
                }
            }
        }
    }
}

//
//  NutrientsDetailTableViewCell.swift
//  Nourish
//
//  Created by Vlad Birukov on 11.09.17.
//  Copyright © 2017 SSIC. All rights reserved.
//

import UIKit

class NutrientsDetailTableViewCell: UITableViewCell {
    @IBOutlet weak var separatorLine: UIView!
    @IBOutlet weak var arrowImage: UIImageView!
    @IBOutlet weak var nameNutrient: UILabel!
    @IBOutlet weak var nutrientLabel: UILabel!
    @IBOutlet weak var chartNutrient: NutrientChartView!

    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

    }
    
    func setupWithModel(model: NutrientViewModel, nutrientDetailInformation: NutrientChartViewModel) {
        var target = 0.0
        self.nameNutrient.text = model.nameNutritional
        
        
        if model.consumedNutritional <= nutrientDetailInformation.lowerLimit {
            target = nutrientDetailInformation.lowerLimit
            
        } else if model.consumedNutritional > nutrientDetailInformation.lowerLimit && model.consumedNutritional <= nutrientDetailInformation.upperLimit && nutrientDetailInformation.upperLimit != 0 {
            
            target = nutrientDetailInformation.lowerLimit
        } else if model.consumedNutritional > nutrientDetailInformation.lowerLimit && nutrientDetailInformation.upperLimit == 0 {
            target = nutrientDetailInformation.lowerLimit
        } else {
            target = nutrientDetailInformation.upperLimit
        }
        
        if model.consumedNutritional > nutrientDetailInformation.upperLimit &&  nutrientDetailInformation.upperLimit != 0.0 {
            nutrientLabel.textColor = NRColorUtility.hexStringToUIColor(hex: "303030")
            nutrientLabel.font = UIFont(name: "Campton-Bold", size: 15)
            
        } else {
            nutrientLabel.textColor = NRColorUtility.hexStringToUIColor(hex: "808080")
            nutrientLabel.font = UIFont(name: "Roboto-Regular", size: 14)
        }
        
        self.nutrientLabel.text = String(format: "%@/%@ %@", model.consumedNutritional.roundToTenIfLessOrMore(target: target), target.roundToTenIfLessOrMore(target: target), model.unit)
        chartNutrient.setupWith(consumedNutrient: model.consumedNutritional, upperLimit: nutrientDetailInformation.upperLimit, lowerLimit: nutrientDetailInformation.lowerLimit)
    }
}

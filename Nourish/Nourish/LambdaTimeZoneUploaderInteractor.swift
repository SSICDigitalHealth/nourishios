//
//  LambdaTimeZoneUploaderInteractor.swift
//  Nourish
//
//  Created by Nova on 6/1/17.
//  Copyright © 2017 SSIC. All rights reserved.
//

import UIKit
import RxSwift

let kTimeZoneUploadKey = "kTimeZoneUploadKey"

class LambdaTimeZoneUploaderInteractor: NSObject {
    
    let repo = AmazonLamdaManager()
    let userRepo = UserRepository.shared
    
    
    func execute() {
        if NRUserSession.sharedInstance.accessToken != nil {
            let lastSyncDate = self.readUploadDate()
            let cal = Calendar.current
            
            if lastSyncDate == nil || cal.startOfDay(for: lastSyncDate!) != cal.startOfDay(for: Date()) {
                let _ = self.uploadTimeZone().subscribe(onNext: { [unowned self] success in
                    if success == true {
                        self.storeUploadDate(date: Date())
                    }
                }, onError: {error in
                    LogUtility.logToFile(error)
                }, onCompleted: {}, onDisposed: {})
            } else {
                print("timeZone already pushed")
            }
        }
    }
    
    private func storeUploadDate(date : Date) {
        defaults.set(date, forKey: kTimeZoneUploadKey)
        defaults.synchronize()
    }
    
    private func readUploadDate() -> Date? {
        return defaults.value(forKey: kTimeZoneUploadKey) as? Date
    }
    
    private func uploadTimeZone() -> Observable<Bool> {
        
        let deviceID = UIDevice.current.identifierForVendor!.uuidString
        let token = NRUserSession.sharedInstance.accessToken
        let timeZone = TimeZone.current.identifier
        
        let user = self.userRepo.getCurrentUser(policy: .DefaultPolicy).flatMap { user -> Observable<String> in
            let id = user.userID ?? ""
            return Observable.just(id)
        }.flatMap { userID in
             return self.repo.uploadUserTimeZone(token: token!, userID: userID, deviceID: deviceID, timeZone: timeZone)
        }
        return user
    }
}

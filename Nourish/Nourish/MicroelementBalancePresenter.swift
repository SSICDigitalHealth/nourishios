//
//  MicroelementBalancePresenter.swift
//  Nourish
//
//  Created by Vlad Birukov on 08.06.17.
//  Copyright © 2017 SSIC. All rights reserved.
//

import Foundation
import RxSwift


class MicroelementBalancePresenter: BasePresenter {
    
    let objectToModelMapper = MicroelementModel_to_MicroelementViewModel()
    let interactor = MicroelementBalanceInteractor()
    var userMicroelementBalanceView: MicroelementBalanceProtocol!
    var date: (Date, Date)?
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.getMicroelementBalance()
    }
    
    private func getMicroelementBalance() {
        if self.userMicroelementBalanceView.config() != nil {
            self.date = self.userMicroelementBalanceView.config()?.getDate()
        }
        
        if self.date != nil {
            let proxyDate = userMicroelementBalanceView?.config()?.date

            self.subscribtions.append(self.interactor.execute(startDate: (self.date?.0)!, endDate: (self.date?.1)!).observeOn(MainScheduler.instance).subscribe(onNext: { [weak self] microelementDetail in
                if let instance = self {
                    if proxyDate == instance.userMicroelementBalanceView?.config()?.date {
                        instance.userMicroelementBalanceView.setupWith(Model: instance.objectToModelMapper.transform(model: microelementDetail))
                    }
                    if let view = instance.userMicroelementBalanceView{
                        view.stopActivityAnimation()
                    }
                }
                
            }, onError: { [weak self] error in
                if let instance = self {
                    if let view = instance.userMicroelementBalanceView{
                        view.stopActivityAnimation()
                        view.parseError(error : error, completion: nil)
                    }
                }
            }, onCompleted: {
            }, onDisposed: { [weak self] in
                if let instance = self {
                    if let view = instance.userMicroelementBalanceView{
                        view.stopActivityAnimation()
                    }
                }
            }))
        }
    }
}
    


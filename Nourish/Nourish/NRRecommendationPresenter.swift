//
//  NRRecommendationPresenter.swift
//  Nourish
//
//  Created by Vino (Vinodha) Sundaramoorthy on 11/14/16.
//  Copyright © 2016 SSIC. All rights reserved.
//

import UIKit
import RxSwift

let kDefaultRecCount = 15
let kHeightPadding : CGFloat = 15
let kBfFoodId : String = "breakfastMeal"
let kLunchFoodId : String = "lunchMeal"
let kDinnerFoodId : String = "dinnerMeal"
let kSnacksFoodId : String = "snacksMeal"
let kLastViewedDate : String = "recommViewedDate"
let kBreakfastString : String = "Breakfast"
let kLunchString : String = "Lunch"
let kDinnerString : String = "Dinner"
let kSnackString : String = "Snacks"

class NRRecommendationPresenter: BasePresenter , UITableViewDelegate,UITableViewDataSource ,MGSwipeTableCellDelegate{

    var interactor = NRRecommendationInteractor()
    var foodInteractor = NRFoodDetailsInteractor()
    var dailyBudgetInteractor = WeightLossMeterInteractor()
    var mealInteractor = MealDiaryInteractor()
    
    var recommendationList : [recommendationList] = []
    var recommendationView : RecommendationProtocol?
    let mapper = NRRecommendationListStruct_to_NRRecommendationList()
    
    //Variables used to maintain state of the UI
    var lunchIndex : Int = 0
    var snackIndex : Int = 0
    var breakfastIndex : Int = 0
    var dinnerIndex : Int = 0
    var lunchCollapsed : Bool = false
    var bfCollapsed : Bool = false
    var dinnerCollapsed : Bool = false
    var snackCollapsed : Bool = false
    
    var showAnimation : Bool = false
    var currentDirection : MGSwipeDirection? = .leftToRight
    var budgetCalorie : Double = 2000
    var mealRecordArray : [MealRecord] = []
    var allMealsArray : [recommendationType] = [.breakfast,.lunch,.dinner,.snack]
    
    //Contains the current swipped food id for corresponding occassion
    var breakFastMeal : String = ""
    var lunchMeal : String = ""
    var dinnerMeal : String = ""
    var snackMeal : String = ""

    var recommendedCalorie : Double {
        get {
            var total : Double = 0
            if self.recommendationList.count > 0 {
                for object in self.recommendationList {
                    let type = recommendationType.stringFromEnum(type: object.type)
                    var mealIndex = 0
                    var ocassion : Ocasion!
                   
                    switch type {
                        case kBreakfastString:
                            mealIndex = self.breakfastIndex
                            ocassion = Ocasion(rawValue: 1)
                        case kLunchString:
                            mealIndex = self.lunchIndex
                            ocassion = Ocasion(rawValue: 2)
                        case kSnackString:
                            mealIndex = self.snackIndex
                            ocassion = Ocasion(rawValue: 4)
                        case kDinnerString:
                            mealIndex = self.dinnerIndex
                            ocassion = Ocasion(rawValue: 3)
                        default:break
                    }
                    
                    
                    let loggedArray = self.mealRecordArray.filter{$0.ocasion == ocassion}
                    if loggedArray.count > 0 {
                        if loggedArray.count > 0 {
                            for meal in loggedArray {
                                total = total + meal.calories
                            }
                        }
                    } else if object.recommendation.count > 0 && object.type != recommendationType.tips && object.type != recommendationType.activity {
                        total = total + Double((object.recommendation[mealIndex].foodModel?.calories)!)!
                    }
                }
            }
            return total
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        EventLogger.logVisitRecommendationsScreen()
        self.loadRecommendationList(completion: { finished in
            if finished == true {
                //onCompleted called
                DispatchQueue.main.async {
                    self.recommendationView?.showRecommendationList()
                }
                
            }
        })
    }
    
    // MARK: RxSwift subscription example with completion handler for UI updating
    func loadRecommendationList(completion : @escaping (Bool) ->()) {
        
        let rec = interactor.getRecommendationList(count: kDefaultRecCount)
        let dailyBudget = dailyBudgetInteractor.getDailyCalorieBudget()
        let mealRecords = mealInteractor.arrayOfFetchedMealsForDate(date: Date(), onlyCache: false).take(1)
        
        let zipValue = Observable.zip(rec,dailyBudget, mealRecords , resultSelector :  { (recomList,totalBudget,loggedMeals) in
            let recommendationList = self.mapper.transform(arr: recomList)
            self.recommendationList = recommendationList
            self.mealRecordArray = loggedMeals
            self.budgetCalorie = totalBudget
        })
        
        let _ = zipValue.observeOn(MainScheduler.instance).subscribe(onNext: { [unowned self] model in
            self.getCurrentRecommendationStatus()
        }, onError: {error in }, onCompleted: {
        completion(true)
        
        }, onDisposed: {})
    }
    
    // MARK: UITableViewDelegate & UITableViewDataSource
    func numberOfSections(in tableView: UITableView) -> Int {
        return self.recommendationList.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell : NRRecommendationsCell = tableView.dequeueReusableCell(withIdentifier: "kRecommendationCell") as! NRRecommendationsCell
        let d = self.recommendationList[indexPath.section]
        var dataIndex : Int = 0
        var collapsed : Bool = false
        var ocasion : Ocasion!
        var loggedOcassion : Bool = false
        
        switch d.type {
            case .lunch:
                dataIndex = lunchIndex
                collapsed = self.lunchCollapsed
                ocasion = .lunch
            case .breakfast:
                dataIndex = breakfastIndex
                collapsed = self.bfCollapsed
                ocasion = .breakfast
            case .dinner:
                dataIndex = dinnerIndex
                collapsed = self.dinnerCollapsed
                ocasion = .dinner
            case .snack:
                dataIndex = snackIndex
                collapsed = self.snackCollapsed
                ocasion = .snacks
            default:
                dataIndex = 0
        }
        
        let loggedArray = self.mealRecordArray.filter{$0.ocasion == ocasion}
        loggedOcassion = loggedArray.count > 0 ? true : false

        if d.recommendation.count > 0 && loggedArray.count == 0 {
            let data = d.recommendation[dataIndex]
            cell.setWithModel(model: data,collapsed: collapsed,loggedOcasion:loggedOcassion)
            cell.delegate = self
        } else {
            cell.titleLabel.text = ""
            cell.recommendationLabel.text = ""
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        let d = self.recommendationList[indexPath.section]
        var dataIndex : Int = 0
        var collapsed : Bool = false
        var ocassion : Ocasion!
        var loggedOcassion : Bool = false
        
        switch d.type {
            case .lunch:
                dataIndex = lunchIndex
                collapsed = self.lunchCollapsed
                ocassion = .lunch
            case .breakfast:
                dataIndex = breakfastIndex
                collapsed = self.bfCollapsed
                ocassion = .breakfast
            case .dinner:
                dataIndex = dinnerIndex
                collapsed = self.dinnerCollapsed
                ocassion = .dinner
            case .snack:
                dataIndex = snackIndex
                collapsed = self.snackCollapsed
                ocassion = .snacks
            default:
                dataIndex = 0
        }
        
        
        var rowHeight : CGFloat = 0.0
        let loggedArray = self.mealRecordArray.filter{$0.ocasion == ocassion}

        if d.recommendation.count > 0 && loggedArray.count == 0  {
            let data = d.recommendation[dataIndex]
            loggedOcassion = loggedArray.count > 0 ? true : false
        
            if !collapsed && !loggedOcassion {
                let recommMaxWidth = UIScreen.main.bounds.width - kLabelWidthPadding
                if data.title.count == 0 {
                    rowHeight = data.description.sizeOfBoundingRect(width: UIScreen.main.bounds.width - kLabelWidthPadding, height: maxLabelSize.height, font: UIFont.systemFont(ofSize: 12)).height + 30
                } else {
                    let titleHeight = data.title.sizeOfBoundingRect(width: UIScreen.main.bounds.width - kLabelWidthPadding, height: maxLabelSize.height, font: UIFont.boldSystemFont(ofSize: 12)).height
                    let descHeight = data.description.sizeOfBoundingRect(width: recommMaxWidth, height: maxLabelSize.height, font: UIFont.systemFont(ofSize: 12)).height
                    rowHeight = titleHeight + descHeight + kLabelHeightPadding + 25
                }
            }
        }

        return rowHeight
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let data = self.recommendationList[section]
        let headerView = UIView(frame: CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: 50))
        headerView.backgroundColor = UIColor.clear
        var recomIndex = 0
       
        if recommendationType.stringFromEnum(type: data.type) == "Tip" || recommendationType.stringFromEnum(type: data.type) == "Activity" {
            //Image
            let imageView = UIImageView(frame: CGRect(x: 15, y: 20, width: 20, height: 22))
            imageView.image = recommendationType.imageFromEnum(type: data.type)
            headerView.addSubview(imageView)
        
            //Title
            let titleLabel = UILabel(frame: CGRect(x: 45, y: 25, width: 300, height: 20))
            titleLabel.text = recommendationType.stringFromEnum(type: data.type)
            titleLabel.textColor = NRColorUtility.progressLabelColor()
            titleLabel.font = UIFont.systemFont(ofSize: 14, weight: UIFontWeightMedium)
            titleLabel.backgroundColor = UIColor.clear
            headerView.addSubview(titleLabel)
        } else {
            let bgView = UIView(frame: CGRect(x: 15, y: 5, width: UIScreen.main.bounds.width - 30, height: 40))
            var ocassion : Ocasion!
            var loggedOcassion : Bool = false
            
            //Expand // collapse button
            let actionButton = UIButton(frame: CGRect(x: 0, y: 0, width: 40, height: 40))
            
            var title = ""
            var calorie = ""
            var collapsed : Bool = false
            if recommendationType.stringFromEnum(type: data.type) == kSnackString {
                title = kSnackString
                actionButton.tag = 4
                collapsed = self.snackCollapsed
                ocassion = .snacks
                recomIndex = self.snackIndex
            } else if recommendationType.stringFromEnum(type: data.type) == kBreakfastString  {
                title = kBreakfastString
                actionButton.tag = 1
                collapsed = self.bfCollapsed
                ocassion = .breakfast
                recomIndex = self.breakfastIndex
            } else if recommendationType.stringFromEnum(type: data.type) == kLunchString {
                title = kLunchString
                actionButton.tag = 2
                collapsed = self.lunchCollapsed
                ocassion = .lunch
                recomIndex = self.lunchIndex
            } else if recommendationType.stringFromEnum(type: data.type) == kDinnerString {
                title = kDinnerString
                actionButton.tag = 3
                collapsed = self.dinnerCollapsed
                ocassion = .dinner
                recomIndex = self.dinnerIndex
            }
            
            let loggedArray = self.mealRecordArray.filter{$0.ocasion == ocassion}
            loggedOcassion = loggedArray.count > 0 ? true : false
            if loggedOcassion {
                var total : Double = 0
                for meal in loggedArray {
                    total = total + meal.calories
                }
                calorie = String(format:"%.0f Cal",total)
                bgView.backgroundColor = NRColorUtility.nourishLightGray()
            } else {
                if data.recommendation.count > 0 {
                    calorie = data.recommendation[recomIndex].subTitle.replacingOccurrences(of: "| ", with: "")
                    actionButton.addTarget(self, action:#selector(expandCollapseSection(sender:)), for: .touchUpInside)
                    
                    bgView.backgroundColor = UIColor.white
                    bgView.layer.cornerRadius = 2
                    bgView.layer.shadowColor = UIColor.lightGray.cgColor
                    bgView.layer.shadowOpacity = 1.0
                    bgView.layer.shadowRadius = 5.0
                    let shadowBounds = CGRect(x: bgView.bounds.origin.x, y: bgView.bounds.origin.y + 5, width: bgView.bounds.size.width + 1, height: bgView.bounds.size.height + 5)
                    bgView.layer.shadowPath = UIBezierPath(rect: shadowBounds).cgPath
                    bgView.layer.masksToBounds = false
                    bgView.clipsToBounds = false
                }
                
            }

            actionButton.setImage(UIImage(named: loggedOcassion ? "accordion_expand_disabled" : collapsed ?"accordion_collapse" :  "accordion_expand"), for: .normal)
            bgView.addSubview(actionButton)

            //Title Label
            let titleLabel = UILabel(frame: CGRect(x: 40, y: 10, width: 110, height: 22))
            titleLabel.text = title
            titleLabel.textColor = loggedOcassion ? UIColor.white : UIColor.black
            titleLabel.font = UIFont.systemFont(ofSize: 14, weight: UIFontWeightMedium)
            titleLabel.backgroundColor = UIColor.clear
            bgView.addSubview(titleLabel)
            
            //Calorie Label
            let calorieLabel = UILabel(frame: CGRect(x: UIScreen.main.bounds.width - 150, y: 10, width: 100, height: 22))
            calorieLabel.text = calorie
            calorieLabel.textColor = loggedOcassion ? UIColor.white : UIColor.lightGray
            calorieLabel.font = UIFont.systemFont(ofSize: 14)
            calorieLabel.backgroundColor = UIColor.clear
            calorieLabel.textAlignment = .right
            bgView.addSubview(calorieLabel)
            headerView.addSubview(bgView)
        }
        
        return headerView
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let data = self.recommendationList[indexPath.section]
        var currentIndex = 0
        switch data.type {
            case .lunch:
                currentIndex = self.lunchIndex
                break
            case .breakfast:
                currentIndex = self.breakfastIndex
                break
            case .dinner:
                currentIndex = self.dinnerIndex
                break
            case .snack:
                currentIndex = self.snackIndex
                break
            default:
               currentIndex = 0
            }
        
        let model = data.recommendation[currentIndex]
        if model.foodModel != nil {
            //place to start animation
            self.recommendationView?.startActivityIndicator()
            self.recommendationView?.showFoodCardwithModel(model: model.foodModel!)
        }
    }
    
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        let data = self.recommendationList[section]
        let footerPosition = "Snacks"
        
        if recommendationType.stringFromEnum(type: data.type) == footerPosition {
            let footerView = UIView(frame: CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: 60))
            
            let shapelayer = CAShapeLayer()
            let path = UIBezierPath()
            path.move(to: CGPoint(x: 15, y: 0))
            path.addLine(to: CGPoint(x: UIScreen.main.bounds.width - 15, y: 0))
            
            shapelayer.strokeColor = UIColor.lightGray.cgColor
            shapelayer.lineWidth = 3.0 / UIScreen.main.scale
            shapelayer.lineJoin = kCALineJoinMiter
            shapelayer.lineDashPattern = [2, 2]
            shapelayer.path = path.cgPath
            footerView.layer.addSublayer(shapelayer)
            
            let shapelayer1 = CAShapeLayer()
            let path1 = UIBezierPath()
            path1.move(to: CGPoint(x: 15, y: 45))
            path1.addLine(to: CGPoint(x: UIScreen.main.bounds.width - 15, y: 45))
            
            shapelayer1.strokeColor = UIColor.lightGray.cgColor
            shapelayer1.lineWidth = 3.0 / UIScreen.main.scale
            shapelayer1.lineJoin = kCALineJoinMiter
            shapelayer1.lineDashPattern = [2, 2]
            shapelayer1.path = path1.cgPath
            footerView.layer.addSublayer(shapelayer1)
        
            //total label
            let totalLabel = UILabel(frame: CGRect(x: tableView.bounds.width/2, y: 15, width: 50, height: 22))
            totalLabel.font = UIFont.systemFont(ofSize: 14, weight: UIFontWeightMedium)
            totalLabel.textColor = UIColor.black
            totalLabel.text = "Total"
            footerView.addSubview(totalLabel)
            
            //Calorie Label
            if self.recommendationList.count > 0 {
                let calorieLabel = UILabel(frame: CGRect(x: totalLabel.bounds.width + 5 + tableView.bounds.width/2 , y: 15, width: 110, height: 22))

                let font = UIFont.systemFont(ofSize: 14)
                let currentAttribute = [NSForegroundColorAttributeName: NRColorUtility.healthGreenColor() , NSFontAttributeName : font]
                let totalAttribute = [ NSForegroundColorAttributeName: NRColorUtility.calorieBudgetColor(), NSFontAttributeName : font]
                
                let calorieString = NSMutableAttributedString(string: String(format:"%.0f Cal",self.recommendedCalorie), attributes: currentAttribute)
                let totalString = String(format:" / %.0f",self.budgetCalorie)
                calorieString.append(NSAttributedString(string: totalString, attributes: totalAttribute))
                calorieLabel.attributedText = calorieString
                footerView.addSubview(calorieLabel)
            }
            
            return footerView
        } else if recommendationType.stringFromEnum(type: data.type) == "Tip" {
            let footerView = UIView(frame: CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: 50))
            footerView.backgroundColor = UIColor.clear
            
            //Image
            let imageView = UIImageView(frame: CGRect(x: 15, y: 20, width: 16, height: 22))
            imageView.image = UIImage(named: "rec_icon_food")
            footerView.addSubview(imageView)
            
            //Title
            let titleLabel = UILabel(frame: CGRect(x: 45, y: 25, width: 300, height: 20))
            titleLabel.text = "Meals"
            titleLabel.textColor = NRColorUtility.progressLabelColor()
            titleLabel.font = UIFont.systemFont(ofSize: 14, weight: UIFontWeightMedium)
            titleLabel.backgroundColor = UIColor.clear
            footerView.addSubview(titleLabel)
            return footerView
        }
        
        return nil
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 50
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        let data = self.recommendationList[section]
        let footerPosition = "Snacks"
       
        if recommendationType.stringFromEnum(type: data.type) == footerPosition {
            return 60
        } else if recommendationType.stringFromEnum(type: data.type) == "Tip" {
            return 50
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        let data = self.recommendationList[indexPath.section]
        if !self.showAnimation {
            if data.type == recommendationType.lunch || data.type == recommendationType.dinner || data.type == recommendationType.breakfast || data.type == recommendationType.snack {
                self.showAnimation = true
                
                let when = DispatchTime.now() + 1
                DispatchQueue.main.asyncAfter(deadline: when) {
                    let swipeCell = cell as! MGSwipeTableCell
                    self.showAnimations(cell:swipeCell)
                }
            }
        }
    }
    
    func showAnimations(cell:MGSwipeTableCell) {
        cell.showSwipe(currentDirection!, animated: true) { (Bool) in
            self.currentDirection = self.currentDirection == .leftToRight ? .rightToLeft : nil
            self.hideAnimation(cell: cell)
        }
    }
    
    func hideAnimation(cell:MGSwipeTableCell) {
        let when = DispatchTime.now() + 1
        DispatchQueue.main.asyncAfter(deadline: when) {
            cell.hideSwipe(animated: true, completion: { (Bool) in
                if self.currentDirection != nil {
                    self.showAnimations(cell: cell)
                }
            })
        }
    }
    
    // MARK: MGSwipeTableCellDelegate
    
    func swipeTableCell(_ cell: MGSwipeTableCell, canSwipe direction: MGSwipeDirection) -> Bool {
        let index = self.recommendationView?.getRecomendationIndexPathForCell(cell: cell)
        let data = self.recommendationList[(index?.section)!]
        
        if data.type == .activity || data.type == .tips {
            return false
        }

        return true
    }
    
    func swipeTableCell(_ cell: MGSwipeTableCell, swipeButtonsFor direction: MGSwipeDirection, swipeSettings: MGSwipeSettings, expansionSettings: MGSwipeExpansionSettings) -> [UIView]? {
        swipeSettings.transition = MGSwipeTransition.border;
        expansionSettings.buttonIndex = 0;
        let index = self.recommendationView?.getRecomendationIndexPathForCell(cell: cell)
        let data = self.recommendationList[(index?.section)!]
        var currentIndex = 0
        
        switch data.type {
        case .lunch:
            currentIndex = self.lunchIndex
            break
        case .breakfast:
            currentIndex = self.breakfastIndex
            break
        case .dinner:
            currentIndex = self.dinnerIndex
            break
        case .snack:
            currentIndex = self.snackIndex
            break
        default:
            currentIndex = 0
        }
        
        let model = data.recommendation[currentIndex]
        
        if direction == MGSwipeDirection.leftToRight {
            expansionSettings.fillOnTrigger = true;
            expansionSettings.threshold = 1;
            return [
                MGSwipeButton(title: "Favourite", icon: UIImage(named:"favtab_white_icon"), backgroundColor: NRColorUtility.favItemColor(), callback: { (cell) -> Bool in
                    
                    if model.foodModel != nil {
                        self.recommendationView?.startActivityIndicator()
                        let int = FavouritesInteractor()
                        let _ = int.addRecToFavourites(searchModel: model.foodModel!, name: UUID().uuidString).subscribe(onNext: {success in
                            print("added to favorites")
                        }, onError: {error in
                            print(error)
                        }, onCompleted: {
                           self.recommendationView?.stopActivittyIndicator()
                        }, onDisposed: {})
                    }
                    
                    
                    return true
                })
            ]
        }
        else if direction == MGSwipeDirection.rightToLeft {
            expansionSettings.fillOnTrigger = true;
            expansionSettings.threshold = 1;
            return [
                MGSwipeButton(title: "Next Recommendation", icon: UIImage(named:"refresh_icon"), backgroundColor: NRColorUtility.nextRecommendationColor(), callback: { (cell) -> Bool in
                    let defaults = UserDefaults.standard
                    
                    switch data.type {
                    case .lunch:
                        if self.lunchIndex == data.recommendation.count - 1 {
                            self.lunchIndex = 0
                        } else {
                            self.lunchIndex = self.lunchIndex + 1
                        }
                        
                        defaults.set(data.recommendation[self.lunchIndex].foodModel?.foodBackendID, forKey: kLunchFoodId)
                        defaults.synchronize()
                    case .breakfast:
                        if self.breakfastIndex == data.recommendation.count - 1 {
                            self.breakfastIndex = 0
                        } else {
                            self.breakfastIndex = self.breakfastIndex + 1
                        }
                        defaults.set(data.recommendation[self.breakfastIndex].foodModel?.foodBackendID, forKey: kBfFoodId)
                        defaults.synchronize()
                        
                    case .dinner:
                        if self.dinnerIndex == data.recommendation.count - 1 {
                            self.dinnerIndex = 0
                        } else {
                            self.dinnerIndex = self.dinnerIndex + 1
                        }
                        defaults.set(data.recommendation[self.dinnerIndex].foodModel?.foodBackendID, forKey: kDinnerFoodId)
                        defaults.synchronize()
                    case .snack:
                        if self.snackIndex == data.recommendation.count - 1 {
                            self.snackIndex = 0
                        } else {
                            self.snackIndex = self.snackIndex + 1
                        }
                        defaults.set(data.recommendation[self.snackIndex].foodModel?.foodBackendID, forKey: kSnacksFoodId)
                        defaults.synchronize()
                    default:
                        print("Default recom type")
                    }
                    
                    self.recommendationView?.reloadSectionWithIndex(section: (index?.section)!)
                    return true
                })
            ]
        }
        return nil
    }
    
    // MARK: UserDefault utility methods 
    
    func getCurrentRecommendationStatus() {
        let defaults = UserDefaults.standard
        let dateFormatter = DateFormatter()
        dateFormatter.dateStyle = .short
        
        if defaults.value(forKey: kLastViewedDate) != nil {
            let lastViewedDate : String = defaults.value(forKey: kLastViewedDate) as? String ?? ""
            if lastViewedDate.count > 0 {
                //let date = dateFormatter.date(from: lastViewedDate as! String)
                //If last viewed date is today , then fetch saved state
                if lastViewedDate == dateFormatter.string(from: Date())  {
                    
                    for meal in self.allMealsArray {
                        var mealId : String = ""
                        var type : recommendationType!
                        
                        switch(meal) {
                            case .breakfast:
                                let meal  = defaults.string(forKey: kBfFoodId) != nil ? defaults.string(forKey: kBfFoodId) :""
                                self.breakFastMeal = meal!
                                mealId = meal!
                                type = .breakfast
                            case .lunch:
                                let meal  = defaults.string(forKey: kLunchFoodId) != nil ? defaults.string(forKey: kLunchFoodId) : ""
                                self.lunchMeal = meal!
                                mealId = meal!
                                type = .lunch
                            case .dinner:
                                let meal  = defaults.string(forKey: kDinnerFoodId) != nil ? defaults.string(forKey: kDinnerFoodId) : ""
                                self.dinnerMeal = meal!
                                mealId = meal!
                                type = .dinner
                            case .snack:
                                let meal  = defaults.string(forKey: kSnacksFoodId) != nil ? defaults.string(forKey: kSnacksFoodId) : ""
                                self.snackMeal = meal!
                                mealId = meal!
                                type = .snack
                            default:
                                mealId = ""
                        }
                        
                        let mealArray = self.recommendationList.filter{$0.type == type}
                        if mealArray.count > 0 {
                            var currentIndex = 0
                            let recommArray : [recommendation] = mealArray[0].recommendation
                            if let mealIndex = recommArray.index(where: {(($0.foodModel?.foodBackendID) == mealId)}) {
                                currentIndex = mealIndex
                            }
                            
                            switch (meal) {
                            case .breakfast:
                                self.breakfastIndex = currentIndex
                            case .lunch:
                                self.lunchIndex = currentIndex
                            case .dinner:
                                self.dinnerIndex = currentIndex
                            case .snack:
                                self.snackIndex = currentIndex
                            default: break
                            }
                        }
                    }
                    
                } else {
                    //Set last viwed date to today
                    defaults.setValue(dateFormatter.string(from: Date()), forKey: kLastViewedDate)

                    //Reset state index
                    defaults.set("", forKey: kBfFoodId)
                    defaults.set("", forKey: kLunchFoodId)
                    defaults.set("", forKey: kDinnerFoodId)
                    defaults.set("", forKey: kSnacksFoodId)
                    defaults.synchronize()
                    
                    self.breakfastIndex = 0
                    self.lunchIndex = 0
                    self.dinnerIndex = 0
                    self.snackIndex = 0
                }
            }
        } else {
            self.breakFastMeal = ""
            self.lunchMeal = ""
            self.dinnerMeal = ""
            self.snackMeal = ""
            self.breakfastIndex = 0
            self.lunchIndex = 0
            self.dinnerIndex = 0
            self.snackIndex = 0
            
            defaults.setValue(dateFormatter.string(from: Date()), forKey: kLastViewedDate)
            defaults.synchronize()
        }
    }
    
    // MARK : Selector actions
    
    func expandCollapseSection(sender:UIButton) {
        
        switch sender.tag {
            case 1 :
                self.bfCollapsed = !self.bfCollapsed
            case 2 :
                self.lunchCollapsed = !self.lunchCollapsed
            case 3 :
                self.dinnerCollapsed = !self.dinnerCollapsed
            case 4 :
                self.snackCollapsed = !self.snackCollapsed
           default : break
        }
        //Reload table
        self.recommendationView?.reloadSectionWithIndex(section:0)
    }
    
}

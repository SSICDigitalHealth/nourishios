//
//  OccasionMealPlannerView.swift
//  Nourish
//
//  Created by Vlad Birukov on 22.08.17.
//  Copyright © 2017 SSIC. All rights reserved.
//

import UIKit

protocol OccasionMealPlannerProtocol : BaseViewProtocol {
    func reloadData()
    func registerTableViewCells()
}

class OccasionMealPlannerView: BaseView, OccasionMealPlannerProtocol {
    @IBOutlet var presenter: OccasionMealPlannerPresenter!
    @IBOutlet weak var occasionTableView: UITableView!
    
    let heightCell = 30
    let heightFooterCell = 44
    
    override func viewDidLoad() {
        self.xibAutoLayouting(nibName: "OccasionMealPlannerView")
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.basePresenter = presenter
        self.presenter.occasionMealPlannerView = self
        self.registerTableViewCells()
        self.settingTableCell()
        super.viewWillAppear(animated)
        self.stopActivityAnimation()
    }
    
 
    
    func reloadData() {
        self.occasionTableView.reloadData()
    }
    
    private func settingTableCell() {
        self.occasionTableView.rowHeight = UITableViewAutomaticDimension
        self.occasionTableView.estimatedRowHeight = CGFloat(self.heightCell)
        self.occasionTableView.sectionFooterHeight = UITableViewAutomaticDimension
        self.occasionTableView.estimatedSectionFooterHeight = CGFloat(self.heightFooterCell)
    }
    
    func registerTableViewCells() {
        self.occasionTableView?.register(UINib(nibName: "OccasionMealPlannerTableViewCell", bundle: nil), forCellReuseIdentifier: "cellOcassionMealPlanner")
        self.occasionTableView?.register(UINib(nibName: "FooterOccasionMealPlannerTableViewCell", bundle: nil), forCellReuseIdentifier: "cellFooterOcassionMealPlanner")
    }
}

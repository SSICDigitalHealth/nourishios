//
//  CalorificFoodOfWeekTableViewCell.swift
//  Nourish
//
//  Created by Vlad Birukov on 20.06.17.
//  Copyright © 2017 SSIC. All rights reserved.
//

import UIKit

class CalorificFoodOfWeekTableViewCell: UITableViewCell {
    @IBOutlet weak var nameFood: UILabel!
    @IBOutlet weak var calories: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func setupWithModel(model: ElementViewModel) {
        nameFood.text = String(format: "%@", model.nameElement)
        nameFood.setLineSpacing(spacing: 4)
        
        if model.consumedState != nil {
            calories.text = String(format: "%.0f %@", model.consumedState!, "kcal")
        }
    }
}

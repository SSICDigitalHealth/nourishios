//
//  NRStressView.swift
//  Nourish
//
//  Created by Vino (Vinodha) Sundaramoorthy on 12/5/16.
//  Copyright © 2016 SSIC. All rights reserved.
//

import UIKit

let defaultLine : CGFloat = 12.0
let defaultArcWid : CGFloat = 20


class NRStressView: BaseClockView , NRStressProtocol {
    var needReloaded = false
    
    var presenter = NRStressPresenter()
    var controller = ProgressViewController()
    var stressScore : CGFloat = 0
    var stressMetrics : String = ""
    var stressArray : [stressLevel] = []
    var stressLabel : UILabel!

    // MARK: NRStressProtocol Implementation
    func setupWith(stressModel: NRStressModel) {
        stressScore = CGFloat(stressModel.stressScore)
        stressMetrics = stressModel.stressMetricsLabel
        stressArray = stressModel.hourlyStressArray!
        needReloaded = true
        self.stopActivityAnimation()
        self.setNeedsDisplay()
        CATransaction.flush()
    }
    
    // MARK: BaseProtocol Implementation
    override func viewWillAppear(_ animated: Bool) {
        self.layer.shouldRasterize = true
        self.layer.rasterizationScale = UIScreen.main.scale
        self.basePresenter = presenter
        presenter.stressView = self
        self.addShadow()
        super.viewWillAppear(animated)
    }
    
    // MARK:Drawing methods
    override func draw(_ rect: CGRect) {
        self.setUpLegends()
        if needReloaded == true {
            let context = UIGraphicsGetCurrentContext()
            context!.clear(rect)
            context!.setFillColor(UIColor.white.cgColor)
            context!.fill(rect)
            self.drawStressScoreWithValue(value: self.stressScore)
            self.drawStressRing(context: context!)
            self.drawLegends()
            self.drawTitle()
            self.registerGesture()
        }
    }
    
    override func drawArc(context : CGContext, startAngle : CGFloat, endAngle : CGFloat, color : UIColor, value:CGFloat) {
        let center = CGPoint(x:bounds.width/2, y: bounds.height/2 - defaultLegendOffset)
        let arcWidth: CGFloat = defaultArcWid
        let radius = max (self.bounds.width , self.bounds.height) / 2
        
        let realEndAngle : CGFloat = startAngle + value.degreesToRadians
        let path = UIBezierPath(arcCenter: center,
                                radius: radius - 100 - arcWidth/2,
                                startAngle: startAngle,
                                endAngle: realEndAngle,
                                clockwise: true)
        path.lineWidth = arcWidth
        color.setStroke()
        path.stroke()
    }
    
    func drawStressRing(context : CGContext) {
        //Divide the circle by 24 hours
        let step = CGFloat(360).degreesToRadians / 24
        let halfOffset = CGFloat(5).degreesToRadians
        let defaultStartAngle = CGFloat(270).degreesToRadians
        if stressArray.count > 0 {
            for i in 0...stressArray.count - 1 {
                self.drawArc(context: context, startAngle:CGFloat(i) * step + halfOffset + defaultStartAngle, endAngle: 0.0, color: stressLevel.colorForStress(stress: stressArray[i]) , value: defaultLine)
            }
        }
    }
    
    func drawStressScoreWithValue(value : CGFloat) {
        let frame = CGRect(x: self.bounds.origin.x, y: self.bounds.origin.y - defaultLegendOffset, width: self.bounds.size.width, height: self.bounds.size.height)
        let font = UIFont.systemFont(ofSize: 44, weight: UIFontWeightMedium)

        if stressLabel == nil {
            stressLabel = UILabel(frame: frame)
            stressLabel.textAlignment = .center
            stressLabel.numberOfLines = 0
            let stressAttribute = [NSForegroundColorAttributeName: NRColorUtility.progressLabelColor() , NSFontAttributeName : font]
            let stressAttrString = NSMutableAttributedString(string: String(format:"%0.f",value), attributes: stressAttribute)
            let labelString = "\nbpm"
            let labelAttributes = [ NSForegroundColorAttributeName: NRColorUtility.progressLabelColor(), NSFontAttributeName : UIFont.systemFont(ofSize: 16)]
            stressAttrString.append(NSAttributedString(string: labelString, attributes: labelAttributes))
            stressLabel?.attributedText = stressAttrString
            self.addSubview(stressLabel)
        } else {
            let stressAttribute = [NSForegroundColorAttributeName: NRColorUtility.progressLabelColor() , NSFontAttributeName : font]
            let stressAttrString = NSMutableAttributedString(string: String(format:"%0.f",value), attributes: stressAttribute)
            let labelString = "\nbpm"
            let labelAttributes = [ NSForegroundColorAttributeName: NRColorUtility.progressLabelColor(), NSFontAttributeName : UIFont.systemFont(ofSize: 16)]
            stressAttrString.append(NSAttributedString(string: labelString, attributes: labelAttributes))
            stressLabel?.attributedText = stressAttrString
        }
    }
    
    func setUpLegends() {
        legendArray = [(description:"Normal",color:NRColorUtility.StressGreenColor()),(description:"Stress",color:NRColorUtility.healthRedColor())]
    }
    
    func drawTitle() {
        let frame  = CGRect(x: 30, y: 10, width: 150, height: 22)
        let font = UIFont.systemFont(ofSize: 16)
        
        let title = NRDrawingUtility.drawTextLayer(frame: frame, text: "Stress", color: NRColorUtility.progressLabelColor().cgColor, bgColor: UIColor.clear.cgColor, font: font)
        title.alignmentMode = "left"
        self.layer.addSublayer(title)
    }
    
    func registerGesture() {
        let gesture = UITapGestureRecognizer(target: self, action: #selector (self.showStressDetails (_:)))
        self.addGestureRecognizer(gesture)
    }
    
    func showStressDetails(_ sender:UITapGestureRecognizer){
        self.controller.openDailyStressController()
    }
}


//
//  ChatSession.swift
//  Nourish
//
//  Created by Gena Mironchyk on 2/14/17.
//  Copyright © 2017 SSIC. All rights reserved.
//

import UIKit

class ChatSession: NSObject {
    dynamic var questions : [ChatDataBaseItemViewModel] = []
    dynamic var answers : [ChatDataBaseItemViewModel] = []
    dynamic var needToSave : Bool = false
    
    
    func appendQuestion(message : ChatDataBaseItemViewModel) {
        self.questions.append(message)
    }
    
    func appendAnswer(message : ChatDataBaseItemViewModel) {
        self.answers.append(message)
    }
}

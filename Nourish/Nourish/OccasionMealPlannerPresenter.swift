//
//  OccasionMealPlannerPresenter.swift
//  Nourish
//
//  Created by Vlad Birukov on 22.08.17.
//  Copyright © 2017 SSIC. All rights reserved.
//

import Foundation


class OccasionMealPlannerPresenter: BasePresenter, UITableViewDataSource,UITableViewDelegate {
    var occasionMealPlannerViewController: OccasionMealPlannerViewController?
    var occasionMealPlannerView: OccasionMealPlannerProtocol?
    var occasionMealPlannerModel: OccasionMealPlannerViewModel?
    let favInteractor = FavouritesInteractor()
    var favoriteButton: UIButton!
    var backButton: UIButton!
    var primaryFavorite = false
    var favorite = false
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.isFavorite()
        self.addTargetButton()
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        if self.occasionMealPlannerModel != nil {
            return self.occasionMealPlannerModel?.occasion != nil ? 1 : 0
        } else {
            return 0
        }
    }

    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        let  footerCell = tableView.dequeueReusableCell(withIdentifier: "cellFooterOcassionMealPlanner") as! FooterOccasionMealPlannerTableViewCell
    
        if self.occasionMealPlannerModel != nil {
            footerCell.setUpWith(model: self.occasionMealPlannerModel!)

        }
    
        return footerCell
    }
    
     func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if self.occasionMealPlannerModel != nil {
            return self.occasionMealPlannerModel?.occasion != nil ? (occasionMealPlannerModel!.occasion?.count)! : 0
        } else {
            return 0
        }
    }
    
     func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cellOcassionMealPlanner", for: indexPath) as! OccasionMealPlannerTableViewCell
        
        if self.occasionMealPlannerModel != nil {
            
            if self.occasionMealPlannerModel?.occasion?[indexPath.row] != nil {
                cell.setUpWithModel(model: (self.occasionMealPlannerModel?.occasion?[indexPath.row].0)!)
            }
        }
        
        return cell
    }
    
     private func dismiss() {
        if self.occasionMealPlannerViewController != nil {
            self.occasionMealPlannerViewController?.navigationController?.popViewController(animated: true)
                NavigationUtility.showTabBar(animated: false)
        }
    }
    
    private func addTargetButton() {
        if self.favoriteButton != nil {
            self.favoriteButton.addTarget(self, action: #selector(changeStatusFavorite), for: .touchUpInside)
        }
        
        if self.backButton != nil {
            self.backButton.addTarget(self, action: #selector(backAndSaveState), for: .touchUpInside)
        }
    }
    
    
    func changeStatusFavorite(sender:UIButton!) {
        self.favorite = self.favorite == true ? false : true
        self.favorite == true ? self.favoriteButton.setImage(UIImage(named: "fav_green"), for: .normal) : self.favoriteButton.setImage(UIImage(named: "fav_icon_00-1"), for: .normal)
    }
    
    func backAndSaveState(sender:UIButton!) {
        
        if self.favorite != self.primaryFavorite {
            if self.favorite == false {
                if self.occasionMealPlannerModel?.occasion != nil && (self.occasionMealPlannerModel?.occasion?.count)! > 0 {
                    if self.occasionMealPlannerModel?.occasion?.first?.0.groupID != nil {
                        self.deleteFavorite(groupId: (self.occasionMealPlannerModel?.occasion?.first?.0.groupID)!)
                        self.dismiss()
                    }
                }
            } else {
                if self.occasionMealPlannerModel?.occasion != nil && (self.occasionMealPlannerModel?.occasion?.count)! > 0 {
                    var nameItem = ""
                    var foodSearch = [foodSearchModel]()

                    for item in (self.occasionMealPlannerModel?.occasion)! {
                        nameItem = nameItem + item.0.foodTitle + ","
                        foodSearch.append(item.0)
                    }
                    nameItem = String(nameItem.dropLast())
                    
                    self.addFavorite(model: foodSearch, name: nameItem)
                    self.dismiss()
                }
            }
        } else {
            self.dismiss()
        }
    }
    
    private func addFavorite(model: [foodSearchModel], name: String) {
        let _ = self.favInteractor.addToFavourites(models: model, name: name).subscribe(onNext: {success in
        }, onError: {error in
        }, onCompleted: {
        }, onDisposed: {})
    }
    
     private func deleteFavorite(groupId: String) {
        let _ = self.favInteractor.deleteFavouriteGroup(groupID: groupId).subscribe(onNext: { [unowned self] success in
        }, onError: {error in
            DispatchQueue.main.async {
                self.occasionMealPlannerView?.parseError(error : error,completion: nil)
            }
        }, onCompleted: {
        }, onDisposed: {})
    }
    
    private func isFavorite() {
        if self.occasionMealPlannerModel?.occasion != nil {
            self.favorite = self.occasionMealPlannerModel?.occasion?.filter({$0.0.isFavorite == true}).count == self.occasionMealPlannerModel?.occasion?.count ? true : false
            self.primaryFavorite = self.favorite
            
            self.favorite == true ? self.favoriteButton.setImage(UIImage(named: "fav_green"), for: .normal) : self.favoriteButton.setImage(UIImage(named: "fav_icon_00-1"), for: .normal)
        }
    }
    
}

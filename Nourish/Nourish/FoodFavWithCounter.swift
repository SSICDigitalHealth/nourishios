//
//  FoodFavWithCounter.swift
//  Nourish
//
//  Created by Nova on 1/23/17.
//  Copyright © 2017 SSIC. All rights reserved.
//

import UIKit
import RealmSwift

class FoodFavWithCounter: Object {
    dynamic var foodTitle : String?
    dynamic var foodId : String?
    dynamic var measure : String?
    dynamic var calories : String?
    
    dynamic var amount : Double = 1
    dynamic var unit : String?
    
    dynamic var counter : Int = 0
    dynamic var grams : Double = 0
    
    //3
    var foodOccasion = RealmOptional<Int>()
    
    //4 
    dynamic var photoID : String?
    
    //5
    dynamic var foodGroupID : String?
    
}

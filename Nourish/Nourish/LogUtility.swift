//
//  LogUtility.swift
//  Nourish
//
//  Created by Gena Mironchyk on 3/16/17.
//  Copyright © 2017 SSIC. All rights reserved.
//

import UIKit

class LogUtility: NSObject {
private static let lastLoggedDateKey = "Last logged date for log file"
private static let documentsDirectory: String = (NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0] as String)
private static let fileManager = FileManager.default
private static let logFilePath = documentsDirectory.appending("/logfile.log")
private static let fullDayLogFilePath = documentsDirectory.appending("/fullDayLog.log")
private static let calendar = Calendar.current

    public class func logToFile(_ items: Any..., separator: String = " ", terminator: String = "\n") {
        let date = Date()
        var output = String(format: "%@ ", date as CVarArg)
        let logToSave = items.map { "\($0)" }.joined(separator: separator)
        output.append(logToSave)
        output.append("\n")
        let data = output.data(using: .utf8, allowLossyConversion: false)!
        
        self.clearDailyLogIfNeeded()
        if fileManager.fileExists(atPath: fullDayLogFilePath) {
            let fileHandle = FileHandle(forWritingAtPath : fullDayLogFilePath)
            fileHandle?.seekToEndOfFile()
            fileHandle?.write(data)
            fileHandle?.closeFile()
        } else {
            try! output.write(toFile: fullDayLogFilePath, atomically: true, encoding: .utf8)
            defaults.set(Date(), forKey: lastLoggedDateKey)
        }
        
        
        Swift.print("------> ",output, terminator : terminator)
    }
    
    
    public class func dailyLogAsData() -> Data? {
        let data = NSData(contentsOfFile: fullDayLogFilePath) as Data?
        return data
    }
    
    
    public class func logAsString() -> String {
        return try! String(contentsOfFile: logFilePath, encoding: .utf8)
    }
    
    
    private class func removeLinesIfNeeded () {
        let contents = self.logAsString()
        var strings = contents.components(separatedBy: .newlines)
        if strings.count > 100 {
            var index = 0
            while strings.count != 100 {
                strings.remove(at: index)
                index += 1
            }
            let results = strings.joined(separator:"\n")
            try! results.write(toFile: logFilePath, atomically: true, encoding: .utf8)
        }
    }
    
    private class func clearDailyLogIfNeeded() {
        if let date = defaults.value(forKey: lastLoggedDateKey) as? Date {
            if !self.calendar.isDateInToday(date) {
                if fileManager.fileExists(atPath: fullDayLogFilePath) {
                   try! fileManager.removeItem(atPath: fullDayLogFilePath)
                }
            }
        }
    }
    
}

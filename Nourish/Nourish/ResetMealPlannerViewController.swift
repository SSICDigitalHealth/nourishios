//
//  ResetMealPlannerViewController.swift
//  Nourish
//
//  Created by Vlad Birukov on 23.08.17.
//  Copyright © 2017 SSIC. All rights reserved.
//

import UIKit

protocol ResetMealControllerDelegate {
    func resetMealPlanControllerDidReset()
}

class ResetMealPlannerViewController: BasePresentationViewController {
    @IBOutlet weak var backgroundImageView: UIImageView!
    @IBOutlet weak var resetMealPlannerView: ResetMealPlannerView!
    var delegate : ResetMealControllerDelegate?
    var backgroundImage: UIImage?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.baseViews = [resetMealPlannerView]
        self.resetMealPlannerView.viewDidLoad()
        self.resetMealPlannerView.presenter.resetMealPlannerViewController = self
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.setupResetView()
    }
    
    func setupResetView() {
        if self.backgroundImage != nil {
            self.backgroundImageView.image = self.backgroundImage
        }
    
        let width = UIScreen.main.bounds.width - 66
        let shadowPath = UIBezierPath(rect: CGRect(x: 0, y: 5, width: width, height: self.resetMealPlannerView.frame.height + 10))
        self.resetMealPlannerView.layer.cornerRadius = 2
        self.resetMealPlannerView.layer.shadowColor = UIColor.darkGray.cgColor
        self.resetMealPlannerView.layer.shadowOffset = CGSize(width: 0.01, height: 0.01)
        self.resetMealPlannerView.layer.shadowOpacity = 0.5
        self.resetMealPlannerView.layer.shadowRadius = 10.0
        self.resetMealPlannerView.layer.masksToBounds =  false
        self.resetMealPlannerView.layer.shadowPath = shadowPath.cgPath
    }
}

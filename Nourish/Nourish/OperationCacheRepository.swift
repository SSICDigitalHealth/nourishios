//
//  OperationCacheRepository.swift
//  Nourish
//
//  Created by Nova on 8/30/17.
//  Copyright © 2017 SSIC. All rights reserved.
//

import UIKit
import RealmSwift

class OperationCacheRepository: NSObject {
    
    let store = OperationCacheDataStore()
    
    func fetchAllRecords() -> [OperationCache] {
        return self.store.fetchAllRecords()
    }
    
    func fetchRecordsFor(date : Date) -> [OperationCache] {
        return self.store.fetchAllRecordsFor(date: date)
    }
    
    func storeRecords(operations : [OperationCache]) {
       return self.store.store(operations:operations)
    }
    
    func fetchFirstRecordFor(date : Date) -> OperationCache? {
        return self.store.fetchFirstRecordFor(date:date)
    }
    
    func deleteOperationWithIDent(string : String) {
        return self.store.deleteWithIDent(string:string)
    }
    
    func increaseCounterForID(stringID : String) {
        return self.store.increaseCounterFor(stringID:stringID)
    }
    
    func fetchUpdateOperationsFor(date : Date, affectedID : String) -> [OperationCache] {
        return self.store.fetchUpdateOperationsFor(date:date, affectedID:affectedID)
    }
    
    func reFetchCreateGroupOperations(date : Date, affectedID : String, localID : String) {
        return self.store.reFetchCreateGroupOperations(date:date , affectedID:affectedID, localID:localID)
    }
    
    func fetchForGroupID(ident : String) -> OperationCache? {
        return self.store.fetchForGroupID(ident:ident)
    }
}

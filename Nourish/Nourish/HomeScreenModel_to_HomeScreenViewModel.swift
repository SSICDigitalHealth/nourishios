//
//  HomeScreenModel_to_HomeScreenViewModel.swift
//  Nourish
//
//  Created by Andrey Zmushko on 9/14/17.
//  Copyright © 2017 SSIC. All rights reserved.
//

import UIKit

class HomeScreenModel_to_HomeScreenViewModel: NSObject {
    func transform(dataModel : HomeScreenModel) ->  HomeScreenViewModel {
        let presentModel = HomeScreenViewModel()
        presentModel.score = dataModel.score
        presentModel.activity = dataModel.activity
        presentModel.avatar = dataModel.avatar
        presentModel.burned = dataModel.burned
        presentModel.consumed = dataModel.consumed
        presentModel.dairy = dataModel.dairy
        presentModel.fruits = dataModel.fruits
        presentModel.grains = dataModel.grains
        presentModel.protein = dataModel.protein
        presentModel.vegetable = dataModel.vegetable
        presentModel.sleep = dataModel.sleep
        presentModel.stress = dataModel.stress
        presentModel.avatar = dataModel.avatar
        return presentModel
    }

}

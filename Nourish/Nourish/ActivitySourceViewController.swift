//
//  ActivitySourceViewController.swift
//  Nourish
//
//  Created by Vino (Vinodha) Sundaramoorthy on 5/16/17.
//  Copyright © 2017 SSIC. All rights reserved.
//

import UIKit
import SwiftKeychainWrapper

class ActivitySourceViewController: UIViewController {
    
    @IBOutlet weak var skipButton : UIButton!
    @IBOutlet weak var appleWatchButton : UIButton!
    @IBOutlet weak var fitBitButton : UIButton!

    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    @IBAction func fitBit(sender:UIButton) {
        let loginVC = FitBitLoginViewController()
        loginVC.login()
    }

    @IBAction func appleWatch(sender:UIButton) {
        KeychainWrapper.standard.set("Apple Watch", forKey: kActivitySourceKey, withAccessibility: KeychainItemAccessibility.afterFirstUnlock)
        //defaults.set("Apple Watch", forKey: kActivitySourceKey)
        //defaults.synchronize()
        self.dismiss(animated: false, completion: nil)
    }
    
    @IBAction func skip(sender:UIButton) {
        self.dismiss(animated: false, completion: nil)
    }

}

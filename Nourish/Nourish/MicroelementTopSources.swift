//
//  MicroelementTopSources.swift
//  Nourish
//
//  Created by Andrey Zmushko on 8/17/17.
//  Copyright © 2017 SSIC. All rights reserved.
//

import UIKit

class MicroelementTopSources: UIView, UITableViewDataSource {
    @IBOutlet weak var label: UILabel!
    var sources: [String] = []
    
    @IBOutlet weak var tableView: UITableView!
    
    override func awakeFromNib() {
        self.tableView.register(UINib.init(nibName: "MicroelementTopSourcesCell", bundle: nil), forCellReuseIdentifier: "Cell")
    }

    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.sources.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as! MicroelementTopSourcesCell
        cell.rowTextLabel.text = self.sources[indexPath.row]
        
        
        return cell
    }
    
    func actualHeight() -> CGFloat {
        return self.tableView.contentSize.height + self.label.bounds.height + 8.0
    }

}

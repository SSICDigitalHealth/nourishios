//
//  KeychainWrapperConstants.swift
//  Nourish
//
//  Created by Nova on 11/22/16.
//  Copyright © 2016 SSIC. All rights reserved.
//

import Foundation

//keychain cache constants
let kAuthTokenConstantKey = "Nourish_access_token"
let kAuthTokenExpirationDateKey = "Nourish_expires"
let kAuthTokenRefreshDate = "Nourish_refresh"
let kRefreshTokenConstantKey = "Nourish_refresh_token"
let kTokenTypeKey = "Nourish_token_type"
let kActivitySourceKey = "Nourish_activity_source"

//Fit Bit cache constants
let kFitBitAccessTokenKey = "Nourish_fitbit_access_token"
let kFitBitRefreshTokenKey = "Nourish_fitbit_refresh_token"
let kFitBitExpirationDateKey = "Nourish_fitbit_expires"

let kUserIDKey = "kUserIDKEY"
//user cache constants
let kArtikUserId = "kArtikUserId"
let kArtikUserFirstName = "kArtikUserFirstName"
let kArtikLastName = "kArtikLastName"
let kArtikActiveSince = "kArtikActiveSince"
let kArtikHeight = "kArtikHeight"
let kArtikWeight = "kArtikWeight"
let kArtikAge = "kArtikAge"
let kArtikActivityLevel = "kArtikActivityLevel"
let kArtikDietaryPreference = "kArticDietaryPreference"
let kArtikMetricPreference = "kArticMetricPreference"
let kArtikBiologicalSex = "kArticBiologicalSex"
let kArtikGoalPrefsActivity = "kArtikGoalPrefsActivity"
let kArtikGoalPrefsFood = "kArtikGoalPrefsFood"
let kArtikGoalPrefsLifestyle = "kArtikGoalPrefsLifestyle"
let kArtikUserGoal = "kArtikUserGoal"
let kArtikLastSyncDate = "kArtikLastSyncDate"
let kArtikStartWeight = "kArtikStartWeight"
let kArtikGoalValue = "kArtikGoalValue"
let kArtikWeightLossStartDate = "kArtikWeightLossStartDate"
let kArtikEmail = "kArtikEmail"
let kArtikMessageUploadKey = "kArtikMessageUploadKey"

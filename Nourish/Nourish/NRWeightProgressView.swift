//
//  NRWeighProgressView.swift
//  Nourish
//
//  Created by Vino (Vinodha) Sundaramoorthy on 12/6/16.
//  Copyright © 2016 SSIC. All rights reserved.
//

import UIKit

class NRWeightProgressView: BaseClockView , NRWeightProgressProtocol {
    
    var progress : CGFloat  = 0.0
    var metricsLabel : String = ""
    var weight : CGFloat = 0.0
    var weightProgress : CGFloat = 0.0
    var presenter = NRWeightProgressPresenter()
    var controller = ProgressViewController()
    var weightLabel : UILabel!
    var goalLabel : UILabel!
    var weightModel : NRWeightProgressModel!
    
    // MARK: BaseProtocol Implementation
    override func viewWillAppear(_ animated: Bool) {
        defaultLineWidth = 0.05 //to control the thickness of the clock shade
        self.layer.shouldRasterize = true
        self.layer.rasterizationScale = UIScreen.main.scale
        self.basePresenter = presenter
        presenter.weightProgressView = self
        super.viewWillAppear(animated)
    }
    
    // MARK: Drawing methods
    override func draw(_ rect: CGRect) {
        let context = UIGraphicsGetCurrentContext()
        context!.clear(rect)
        context!.setFillColor((self.backgroundColor?.cgColor)!)
        context!.fill(rect)
        self.clockfaceCount = 10
        self.addShadow()
        let startAngle = CGFloat(270).degreesToRadians
        let endValue = (progress / 100)
        self.drawShadeСlockFace(context: context!, colorArray: [])
        self.drawWeightWithValue()
        self.registerGesture()
        self.drawTitle()
        
        if weightModel.dailyWeightLogged == true {
            self.drawArc(context: context!, startAngle: startAngle, endAngle: 0.0, color: NRColorUtility.macronutrientsColor(), endValue: endValue)
            self.drawGoalLabel()
        }

    }
    
    func drawWeightWithValue() {
        //Draw the health score
        let frame = CGRect(x: self.bounds.origin.x, y: self.bounds.origin.y - defaultLegendOffset, width: self.bounds.size.width, height: self.bounds.size.height)
        let font = UIFont.systemFont(ofSize: 36, weight: UIFontWeightMedium)

        if weightLabel == nil {
            weightLabel = UILabel(frame: frame)
            weightLabel.textAlignment = .center
            weightLabel.numberOfLines = 0
            if weightModel.dailyWeightLogged == true {
                let weightAttribute = [NSForegroundColorAttributeName: NRColorUtility.progressLabelColor() , NSFontAttributeName : font]
                let weightAttrString = NSMutableAttributedString(string: String(format:"%2.f",metricsLabel == "kg" ? weight : NRConversionUtility.kiloToPounds(valueInKilo: Double(weight))), attributes: weightAttribute)
                
                let labelString = String(format:"\n%0.1f%@",metricsLabel == "kg" ? weightProgress : NRConversionUtility.kiloToPounds(valueInKilo: Double(weightProgress)),metricsLabel)
                let labelAttributes = [ NSForegroundColorAttributeName: NRColorUtility.progressLabelColor(), NSFontAttributeName : UIFont.systemFont(ofSize: 14)]
                weightAttrString.append(NSAttributedString(string: labelString, attributes: labelAttributes))
                weightLabel?.attributedText = weightAttrString

            } else {
                let labelAttributes = [ NSForegroundColorAttributeName: NRColorUtility.progressLabelColor(), NSFontAttributeName : UIFont.systemFont(ofSize: 14)]
                let weightAttrString = NSAttributedString(string: "Log your weight", attributes: labelAttributes)
                weightLabel?.attributedText = weightAttrString

            }
            self.addSubview(weightLabel)
        } else {
            if weightModel.dailyWeightLogged == true {
                let weightAttribute = [NSForegroundColorAttributeName: NRColorUtility.progressLabelColor() , NSFontAttributeName : font]
                let weightAttrString = NSMutableAttributedString(string: String(format:"%2.f",metricsLabel == "kg" ? weight : NRConversionUtility.kiloToPounds(valueInKilo: Double(weight))), attributes: weightAttribute)
                let labelString = String(format:"\n%0.1f%@",metricsLabel == "kg" ? weightProgress : NRConversionUtility.kiloToPounds(valueInKilo: Double(weightProgress)),metricsLabel)
                let labelAttributes = [ NSForegroundColorAttributeName: NRColorUtility.progressLabelColor(), NSFontAttributeName : UIFont.systemFont(ofSize: 14)]
                weightAttrString.append(NSAttributedString(string: labelString, attributes: labelAttributes))
                weightLabel?.attributedText = weightAttrString
            } else {
                let labelAttributes = [ NSForegroundColorAttributeName: NRColorUtility.progressLabelColor(), NSFontAttributeName : UIFont.systemFont(ofSize: 14)]
                let weightAttrString = NSAttributedString(string: "Log your weight", attributes: labelAttributes)
                weightLabel?.attributedText = weightAttrString
                
            }
        }
     }
    
    func drawArc(context : CGContext, startAngle : CGFloat, endAngle : CGFloat, color : UIColor, endValue : CGFloat) {
        let center = CGPoint(x:bounds.width/2, y: bounds.height/2 - defaultLegendOffset)
        
        let radius: CGFloat = max(bounds.width, bounds.height) / 2
        
        let arcWidth: CGFloat = defaultArcWidth
        
        let realEndAngle : CGFloat = startAngle + self.valueToRadians(value: endValue) * 4
        let path = UIBezierPath(arcCenter: center,
                                radius: radius - 100 - arcWidth/2,
                                startAngle: startAngle,
                                endAngle: realEndAngle,
                                clockwise: true)
        path.lineWidth = arcWidth
        color.setStroke()
        path.stroke()
    }
    
    func drawGoalLabel() {
        let myString = String(format:"%0.f%@",progress,"%")
        let myAttribute = [ NSForegroundColorAttributeName: NRColorUtility.progressLabelColor() , NSFontAttributeName : UIFont.systemFont(ofSize: 20, weight: UIFontWeightMedium)]
        let myAttrString = NSMutableAttributedString(string: myString, attributes: myAttribute)
        let labelString = " of goal"
        let labelAttributes = [ NSForegroundColorAttributeName: NRColorUtility.progressLabelColor() , NSFontAttributeName : UIFont.systemFont(ofSize: 14)]
        myAttrString.append(NSAttributedString(string: labelString, attributes: labelAttributes))
        
        let labelframe = CGRect(x: self.bounds.origin.x + 50, y: self.bounds.size.height - 50, width: 200, height: 30)
        if goalLabel == nil {
            goalLabel = UILabel(frame: labelframe)
            goalLabel.attributedText = myAttrString
            weightLabel.textColor = NRColorUtility.progressLabelColor()
            goalLabel.textAlignment = .center
            goalLabel.center = CGPoint(x: self.bounds.size.width/2, y: self.bounds.size.height - 50)
            self.addSubview(goalLabel)
        }else {
            goalLabel.attributedText = myAttrString
        }
    }
    
    // MARK: NRWeightProgressProtocol Implementation
    func setupWith(weightProgress: NRWeightProgressModel) {
        
        self.weightModel = weightProgress
        self.progress = CGFloat(weightProgress.progressPercentage)
        self.metricsLabel = weightProgress.metrics
        self.weight = CGFloat(weightProgress.currentWeight)
        self.weightProgress = CGFloat(weightProgress.weightProgress)
        self.stopActivityAnimation()
        
        self.setNeedsDisplay()
    }
    
    func drawTitle() {
        let frame  = CGRect(x: 30, y: 10, width: 150, height: 22)
        let font = UIFont.systemFont(ofSize: 16)
        
        let title = NRDrawingUtility.drawTextLayer(frame: frame, text: "Weight", color: NRColorUtility.progressLabelColor().cgColor, bgColor: UIColor.clear.cgColor, font: font)
        title.alignmentMode = "left"
        self.layer.addSublayer(title)
    }
    
    func registerGesture() {
        let gesture = UITapGestureRecognizer(target: self, action: #selector (self.showWeightDetails (_:)))
        self.addGestureRecognizer(gesture)
    }
    
    private func backToChat () {
        let chatInteractor = ChatBoxInterractor()
        let logWeightMessage = ChatDataBaseItem()
        logWeightMessage.isFromUser = false
        logWeightMessage.chatBoxType = .typeLogWeight
        logWeightMessage.content = "Update your weight today."
        logWeightMessage.date = Date()
        logWeightMessage.userID = weightModel.userId!
        chatInteractor.storeRecordsArray(array: [logWeightMessage])
        NavigationUtility.showChatView()
    }

    func showWeightDetails(_ sender:UITapGestureRecognizer){
        EventLogger.logOnWeightScreenClick()
        if self.weightModel.dailyWeightLogged == true {
            self.controller.openVCWeightStats()
        } else {
            self.backToChat()
        }
    }
}

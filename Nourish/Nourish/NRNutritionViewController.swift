//
//  NRNutritionViewController.swift
//  Nourish
//
//  Created by Vino (Vinodha) Sundaramoorthy on 12/13/16.
//  Copyright © 2016 SSIC. All rights reserved.
//

import UIKit

class NRNutritionViewController: BasePresentationViewController  {
    @IBOutlet weak var nutritionStatsView : NutritionStatsView!
    
    let nutritionPresenter = NRNutritionStatsPresenter()
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(false)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupNavigationBar()
        self.baseViews = [nutritionStatsView]
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        self.navigationController?.navigationBar.topItem?.title = kProgressViewTitle
    }
    
    func setupNavigationBar() {
        self.title = kNutritionStatsTitle
        self.navigationController?.navigationBar.backItem?.title = ""
        self.navigationController?.navigationBar.setBackgroundImage(NRColorUtility.symplifiedNavigationColor().as1ptImage(), for: .default)
        self.navigationController?.navigationBar.shadowImage = NRColorUtility.symplifiedNavigationColor().as1ptImage()
    }
    
    override func respondToSwipeGesture(gesture: UISwipeGestureRecognizer) {
        let touchPoint = gesture.location(in: self.view)
        if touchPoint.x < 50 {
            super.respondToSwipeGesture(gesture: gesture)
        }
    }
    
}

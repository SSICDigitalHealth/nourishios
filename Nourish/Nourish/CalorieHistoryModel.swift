//
//  CalorieHistoryModel.swift
//  Nourish
//
//  Created by Gena Mironchyk on 9/19/17.
//  Copyright © 2017 SSIC. All rights reserved.
//

import UIKit

class CalorieHistoryModel: NSObject {
    var consumed : Double = 0.0
    var maxConsumed : Double = 0.0
    var date : Date? = nil
}

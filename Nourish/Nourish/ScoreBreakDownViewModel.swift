//
//  ScoreBreakDownViewModel.swift
//  Nourish
//
//  Created by Vlad Birukov on 29.08.17.
//  Copyright © 2017 SSIC. All rights reserved.
//

import Foundation

typealias historyScoreViewList = (subtitle: Date, value: Double)

class ScoreBreakDownViewModel : NSObject {
    var scoreUser: Double = 0.0
    var historyScore: [historyScoreViewList] = [historyScoreViewList]()
    var isDairyEmpty = Bool(false)
}


//
//  NutritionBalanceViewModel.swift
//  Nourish
//
//  Created by Vlad Birukov on 29.08.17.
//  Copyright © 2017 SSIC. All rights reserved.
//

import Foundation

class NutritionBalanceViewModel : NSObject {
    var message: String?
    var nameImage: String?
    var nutritionData = NutritionalBalanceViewModel()
    var arrNutritionElement = [NutritionElementViewModel]()
}

class NutritionElementViewModel {
    var consumedAmount = 0.0
    var type : NutritionType?
    var target:Double?
    var unit = ""
    var message = ""
    var nameImageMessage: String?
    var arrNutrition : [ElementViewModel]?
    var dataCharts: [NutrientInformationChartViewModel]?
    var caption: String?
    var richNutritionFood: [ElementModel]?
}

class ElementViewModel {
    var nameElement : String = ""
    var unit: String?
    var consumedState: Double?
    var amountElement: Double?
}

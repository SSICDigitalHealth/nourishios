//
//  WeeklyStressCell.swift
//  Nourish
//
//  Created by Gena Mironchyk on 2/2/17.
//  Copyright © 2017 SSIC. All rights reserved.
//

import UIKit

class WeeklyStressCell: UITableViewCell {
    @IBOutlet weak var maxBPMLabel:UILabel?
    @IBOutlet weak var minBPMLabel:UILabel?
    @IBOutlet weak var avgBPMLabel:UILabel?
    @IBOutlet weak var dateLabel:UILabel?

    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    func setupWithModel(model : WeeklyStressModel) {
        self.minBPMLabel?.text = String.init(format: "%0.0f", model.minBPM!)
        self.maxBPMLabel?.text = String.init(format: "%0.0f", model.maxBPM!)
        self.avgBPMLabel?.text = String.init(format: "%0.0f", model.avgBPM!)
        let startDate = model.weeklyArray?.first?.dateToShow
        let endDate = model.weeklyArray?.last?.dateToShow
        
        self.dateLabel?.text = startDate == nil || endDate == nil ? "" : String.init(format: "%@ - %@", self.formatDate(date: startDate!), self.formatDate(date: endDate!))
    }
    
    
    private func formatDate(date : Date)->String {
        let df = DateFormatter()
        df.dateFormat = "MM/dd"
        return df.string(from: date)
        
    }
}

//
//  NutritionStatsInteractor.swift
//  Nourish
//
//  Created by Vino (Vinodha) Sundaramoorthy on 12/21/16.
//  Copyright © 2016 SSIC. All rights reserved.
//

import UIKit
import RxSwift

class NutritionStatsInteractor: NSObject {
    
    var nutritionInteractor = NutritionInteractor()
    let toModelMapper =  Nutrition_to_nutritionStatsModel()
    
    func getNutritionStatsFor(period : period) -> Observable<nutritionStatsModel> {
        let nutritions = self.nutritionInteractor.nutritionsForPeriod(period: period)
        let calories = self.nutritionInteractor.caloriesForPeriod(period: period)
        let zip = Observable.zip(nutritions, calories, resultSelector : {nutritionData, calorieData -> nutritionStatsModel in
            return self.toModelMapper.transform(nutr: nutritionData, calorie: calorieData, period: period)
        })
        return zip
    }

}

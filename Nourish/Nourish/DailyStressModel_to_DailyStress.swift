//
//  DailyStressModel_to_DailyStress.swift
//  Nourish
//
//  Created by Gena Mironchyk on 1/24/17.
//  Copyright © 2017 SSIC. All rights reserved.
//

import UIKit
class DailyStressModel_to_DailyStress: NSObject {
    func transform(model : DailyStressModel) -> DailyStress {
        let stress = DailyStress()
        stress.bpmValue = model.bpmValue
        stress.stressValue = model.stressValue
        stress.dateToShow = model.dateToShow
        return stress
    }
    
    func transform(modelsArray : [DailyStressModel]) -> [DailyStress] {
        var stressArray : [DailyStress] = []
        for model in modelsArray {
            stressArray.append(self.transform(model: model))
        }
        return stressArray
    }
}

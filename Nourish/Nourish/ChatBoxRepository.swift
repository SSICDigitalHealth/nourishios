//
//  ChatBoxRepository.swift
//  Nourish
//
//  Created by Nova on 2/6/17.
//  Copyright © 2017 SSIC. All rights reserved.
//

import UIKit
import RxSwift
import HealthKit

let backgroundDateKey = "droppedToBackgroundKey"
let kGoalChangedDialogMessage = "goalChangedMessage"

class ChatBoxRepository: NSObject {
    let store = ChatBoxRealmDataStore()
    let activityRepo = UserActivityRepository()

   private func getRecordsFor(date : Date, userID : String, userGoal : UserGoal?) -> Observable<[ChatDataBaseItem]> {
        return self.store.getRecordsFor(date: date, userID : userID, userGoal: userGoal)
    }
    
    func storeRecordsArray(array : [ChatDataBaseItem]) {
        self.store.storeRecordsArray(array: array)
    }
    func getAllUserMessagesCount(userID : String) -> Int {
        return self.store.getAllUserMessagesCount(userID: userID)
    }
    
    func getLastRecordWithContent(content : String) -> ChatDataBaseItem? {
        return self.store.getMessageWithContent(content: content)
    }
    
    
    func getChatHistoryFor(date : Date, userProfile : UserProfile) -> Observable<[ChatDataBaseItem]> {
        let chatBoxMessages = self.getRecordsFor(date: date, userID: userProfile.userID!, userGoal: userProfile.userGoal)
        return chatBoxMessages.flatMap({arrayOfMessages -> Observable<[ChatDataBaseItem]> in
            var chatHistory = arrayOfMessages
            let firsMessagesForDay = self.addFirstMessageOfDayIfNeeded(arrayOfMessages :arrayOfMessages, userID: userProfile.userID ?? "")
            let energyBurnedMessages = self.addActivityReportMessageIfNeeded(arrayOfMessages: arrayOfMessages, userProfile: userProfile)
            let logDailyWeightMessages = self.addLogWeightMessageIfNeeded(arrayOfMessages: arrayOfMessages, userProfile: userProfile)
            let dailyReportMessages = self.addDailyReportMessageIfNeeded(arrayOfMessages: arrayOfMessages, userProfile: userProfile)
            let informMessages = self.addChangedWeightLossMessageIfNeeded(userProfile: userProfile)
            return Observable.zip(firsMessagesForDay,energyBurnedMessages, logDailyWeightMessages, dailyReportMessages, informMessages, resultSelector: {greetingsMessages,energyMessages,weightMessages,reportMessages,infoMessages -> [ChatDataBaseItem] in
                chatHistory.append(contentsOf: greetingsMessages)
                chatHistory.append(contentsOf: energyMessages)
                chatHistory.append(contentsOf: weightMessages)
                chatHistory.append(contentsOf: reportMessages)
                chatHistory.append(contentsOf: infoMessages)
                return chatHistory
            })
            
        })
    }
    
    
    private func addFirstMessageOfDayIfNeeded(arrayOfMessages : [ChatDataBaseItem], userID : String) -> Observable<[ChatDataBaseItem]> {
        var arrayToReturn = [ChatDataBaseItem]()
        let now = Date()
        let cal = Calendar.current
        let onboardingDate = defaults.object(forKey: kOnBoardingKeyDate) as? Date
        if arrayOfMessages.count == 0 && onboardingDate != nil && cal.startOfDay(for: now) != cal.startOfDay(for: onboardingDate!) {
            let message = ChatDataBaseItem()
            message.chatBoxType = .typeString
            message.date = now
            message.isFromUser = false
            message.userID = userID
            message.content = self.messageFrom(hour: cal.component(.hour, from: now))
            self.storeRecordsArray(array: [message])
            arrayToReturn.append(message)
        }
        return Observable.just(arrayToReturn)
    }
    
    
    private func messageFrom(hour : Int) -> String {
        var message = "Hello!"
        if hour > 6 && hour < 12{
            message = "Good Morning!"
        }
        return message
    }
    
    
    
    func addChangedWeightLossMessageIfNeeded(userProfile : UserProfile) -> Observable<[ChatDataBaseItem]> {
        var arrayToReturn : [ChatDataBaseItem] = []
        var dialogString : String = ""
        var content =  "Your goal has been changed."
        if userProfile.needToInformUser == true {
            if userProfile.userGoal == .MaintainWeight {
                content = kMaintainWeightRecommendationMessage
                dialogString = "Great Work!You have made great progress.We've switched your goal back to \"Maintain Weight\""
            } else {
                dialogString = "We noticed you've gone past \"Maintain Weight\" threshold. To better meet your goal we've switched your goal to \"Lose Weight\""
            }
        
            defaults.set(dialogString, forKey: kGoalChangedDialogMessage)
            let message = ChatDataBaseItem()
            message.chatBoxType = .typeString
            message.date = Date()
            message.userID = userProfile.userID!
            message.isFromUser = false
            message.content = content
            self.storeRecordsArray(array: [message])
            arrayToReturn.append(message)
            userProfile.needToInformUser = false
            let userRepo = UserRepository.shared
            let _ = userRepo.storeCurrentUser(user: userProfile, writeToHealth: false, needToRefreshToken: false).subscribe(onNext: {error in}, onError: {error in}, onCompleted: {
                print("Saved")
            }, onDisposed: {})
        }
        return Observable.just(arrayToReturn)
    }
    
    
    
    func addDailyReportMessageIfNeeded(arrayOfMessages : [ChatDataBaseItem], userProfile: UserProfile) -> Observable<[ChatDataBaseItem]> {
        var arrayToReturn : [ChatDataBaseItem] = []
        var dailyReportMessageExists : Bool = false
        var loggedDailyWeight : Bool = false
        for message in arrayOfMessages {
            if message.chatBoxType == .typeDailyReport{
                dailyReportMessageExists = true
            }
        }
        
        for message in arrayOfMessages {
            if message.chatBoxType == .typeWeightRecord && message.content != noThanksMessage {
                loggedDailyWeight = true
            }
        }
        
        if loggedDailyWeight == true && dailyReportMessageExists == false {
            if let onboardingDate = defaults.object(forKey: kOnBoardingKeyDate) as? Date {
                if userProfile.userGoal == .LooseWeight && Calendar.current.startOfDay(for: Date()) != Calendar.current.startOfDay(for: onboardingDate) {
                    let dailyReportMessage = ChatDataBaseItem()
                    dailyReportMessage.chatBoxType = .typeDailyReport
                    dailyReportMessage.date = Date()
                    dailyReportMessage.userID = userProfile.userID!
                    dailyReportMessage.isFromUser = false
                    if userProfile.startWeight! - userProfile.weight! <= 0 {
                        dailyReportMessage.content = "Looks like you're slowing down a bit."
                    } else {
                        dailyReportMessage.lostWeightValue = userProfile.startWeight! - userProfile.weight!
                        dailyReportMessage.targetLostWeightValue = userProfile.startWeight! - userProfile.goalValue!
                        var unitIdent = "kg"
                        if userProfile.metricPreference == .Imperial {
                            unitIdent = "lbs"
                        }
                        dailyReportMessage.content = String(format:"You lost %@ %@ / %@%@.",self.getUserWeight(newWeight: dailyReportMessage.lostWeightValue, user: userProfile),unitIdent,self.getUserWeight(newWeight: dailyReportMessage.targetLostWeightValue, user: userProfile),unitIdent)
                    }
                    self.storeRecordsArray(array: [dailyReportMessage])
                    arrayToReturn.append(dailyReportMessage)
                }
                

            }
        }
        
        
        return Observable.just(arrayToReturn)
    }
    
    private func getUserWeight(newWeight : Double, user : UserProfile) -> String {
        var weight = ""
        weight = user.metricPreference == MetricPreference.Metric ?  String(format:"%.1f", newWeight) :  String(format:"%.1f",NRConversionUtility.kiloToPounds(valueInKilo: newWeight))
        
        return weight
    }

    
    func recordedDailyWeightFor(date: Date, userID : String, userGoal : UserGoal?) -> Observable<Bool> {
        let result = self.getRecordsFor(date: date, userID: userID, userGoal: userGoal).flatMap { chatDataBaseItems -> Observable<Bool> in
            var contains = false
            let arrayOfAnswers = chatDataBaseItems.filter({type in type.chatBoxType == .typeWeightRecord})

            if defaults.value(forKey: kOnBoardingKeyDate) != nil && Calendar.current.startOfDay(for: defaults.value(forKey: kOnBoardingKeyDate) as! Date) == Calendar.current.startOfDay(for: Date()) && arrayOfAnswers.count == 0 {
                contains = true
            } else {
                if arrayOfAnswers.count > 0 && arrayOfAnswers.contains(where: {answer in answer.content != noThanksMessage }){
                    contains = true
                }
            }
            return Observable.just(contains)
        }
        return result
    }
    
    
    func addActivityReportMessageIfNeeded(arrayOfMessages : [ChatDataBaseItem], userProfile:UserProfile) -> Observable<[ChatDataBaseItem]> {
        var messages : [ChatDataBaseItem] = []
        let startDate =  defaults.object(forKey: backgroundDateKey) as? Date
        if startDate != nil  && defaults.object(forKey: kOnBoardingKeyDate) != nil && PermissionsUtility.shared.checkHealthKitPermissionsWasAsked() == true{
            let dateToAdd = Date().addingTimeInterval(1)
            let calories = self.activityRepo.getCalories(startDate:startDate! , endDate: Date())
            return calories.flatMap({cals -> Observable<[ChatDataBaseItem]> in
                let burnedCals = self.burnedCalories(samples: cals)
                if burnedCals > 10 {
                    DispatchQueue.main.async {
                        let activeMessage = ChatDataBaseItem()
                        activeMessage.isFromUser = false
                        activeMessage.chatBoxType = .typeCalorieReport
                        activeMessage.content = String(format:"Great! You have burned %0.0f calories", burnedCals)
                        activeMessage.date = Date()
                        activeMessage.userID = userProfile.userID!
                        self.storeRecordsArray(array: [activeMessage])
                        defaults.removeObject(forKey: backgroundDateKey)
                        defaults.synchronize()
                        messages.append(activeMessage)
                        let _ = userProfile.calculateAndStoreCaloriePlan()
                    }
                }
                return Observable.just(messages)
            }).flatMap {messagesToSend in
                return self.storeChangeScoreMessageIfNeededWith(date: dateToAdd, userID: userProfile.userID!, messages: messagesToSend)
            }
        } else {
            return Observable.just(messages)
        }
    }
    
    func storeChangeScoreMessageIfNeededWith(date : Date, userID : String, messages : [ChatDataBaseItem]) -> Observable<[ChatDataBaseItem]> {
        let scoreRepo = MealHistoryRepository.shared
        let cacheRepo = ProxyCacherRepo.shared
        
        let cacheValue = cacheRepo.retreiveLastCacheFor(date: Date())
        let isNeed = messages.filter { $0.chatBoxType == .typeCalorieReport }.count > 0
        var array = messages
        if cacheValue != nil && isNeed == true {
            let currentValue = scoreRepo.getCaloriesStatsFor(period: .today).map{value -> [ChatDataBaseItem] in
                if let score = cacheValue {
                    if let lastValue = value.first?.score {
                        if round(score) != round(lastValue * 100) {
                                print("Last value ::: \(round(lastValue * 100))")
                                let activeMessage = ChatDataBaseItem()
                                activeMessage.isFromUser = false
                            cacheRepo.storeLastCache()
                            activeMessage.content = String(format:"Your score has been adjusted. Your new score is %.0f points", round(lastValue * 100))
                                activeMessage.date = date
                                activeMessage.chatBoxType = .typeString
                            activeMessage.userID = userID
                                array.append(activeMessage)
                            DispatchQueue.main.async {
                                self.storeRecordsArray(array: [activeMessage])
                            }
                        }
                    }
                }
                return array
            }
            return currentValue
        } else {
            return Observable.just(messages)
        }
    }
    
    func addLogWeightMessageIfNeeded(arrayOfMessages : [ChatDataBaseItem], userProfile:UserProfile) -> Observable<[ChatDataBaseItem]>{
        return self.store.addLogWeightMessageIfNeddedTo(arrayOfMessages:arrayOfMessages, userID: userProfile.userID! ,userGoal:userProfile.userGoal)
    }
    
    
    private func burnedCalories(samples : [HKSample]) -> Double {
        var summary = 0.0
        for sample in samples {
            let key = sample as! HKQuantitySample
            summary = summary + key.quantity.doubleValue(for: HKUnit.kilocalorie())
        }
        return summary
    }

}

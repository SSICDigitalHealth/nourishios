//
//  RecipesDetailViewController.swift
//  Nourish
//
//  Created by Vlad Birukov on 07.09.17.
//  Copyright © 2017 SSIC. All rights reserved.
//

import UIKit

class RecipesDetailViewController: BaseCardViewController {
    @IBOutlet weak var cardNavigationBar: NewCardNavigationBarView!
    @IBOutlet weak var recipesView: RecipesDetailView!
    
    var titleNavigationBar = ""
    var recipiesViewModel: RecipiesViewModel?
    var isShowTabBar = false
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.baseViews = [cardNavigationBar, recipesView]
        
        for v in self.baseViews! {
            v.viewDidLoad()
        }
    }

    override func viewWillAppear(_ animated: Bool) {
        self.setUpNavigationBar(title: self.titleNavigationBar, style: .other,navigationBar: self.cardNavigationBar, hideTabBar: true, controller: self)
        super.viewWillAppear(animated)
        
        self.settingPresenter()
    }
    
    func backActionWithoutShowTabBar(_ sender: Any) {
        self.recipesView.presenter.saveStateFavoriteAndBack(showTabBar: self.isShowTabBar)
    }
    
    private func stateFavorite() {
        self.recipesView.presenter.currentStateFavorite = (self.recipiesViewModel?.isFavorite)!
        self.recipesView.presenter.initalStateFavorite = (self.recipiesViewModel?.isFavorite)!
    }
    
    private func settingPresenter() {
        if recipiesViewModel != nil {
            self.recipesView.presenter.recipesViewModel = self.recipiesViewModel
            self.recipesView.presenter.controller = self
            self.stateFavorite()
            
            self.recipesView.reloadData()
        }
    }
}

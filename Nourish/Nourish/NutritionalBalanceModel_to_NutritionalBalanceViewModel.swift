//
//  NutritionalBalanceModel_to_NutritionalBalanceViewModel.swift
//  Nourish
//
//  Created by Vlad Birukov on 07.06.17.
//  Copyright © 2017 SSIC. All rights reserved.
//

import Foundation

class NutritionalBalanceModel_to_NutritionalBalanceViewModel {
    func transform(model: NutritionalBalanceModel) -> NutritionalBalanceViewModel {
        let nutritionalBalanceViewModel = NutritionalBalanceViewModel()
        if model.data?.count != 0 {
            nutritionalBalanceViewModel.data = [nutritionalViewList]()
            var mass = [(NutritionType, UserNutritionViewModel)]()
            for nutrient in model.data! {
                let variable: nutritionalViewList = nutritionalViewList(nutrient.type, self.transform(model: nutrient.nutrition, type: nutrient.type))
                mass.append(variable)
            }
            nutritionalBalanceViewModel.data = mass
        }
        return nutritionalBalanceViewModel
    }
    
    private func transform (model: UserNutrition, type: NutritionType) -> UserNutritionViewModel {
        let userNutritiomViewModel = UserNutritionViewModel()
        userNutritiomViewModel.consumedNutritional = model.consumedNutritional
        userNutritiomViewModel.targetNutritional = model.targetNutritional
        userNutritiomViewModel.unit = NutritionType.nutritionUnit(type: type)
        return userNutritiomViewModel
    }
    
}

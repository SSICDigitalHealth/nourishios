//
//  NRFeedbackView.swift
//  Nourish
//
//  Created by Vino (Vinodha) Sundaramoorthy on 11/29/16.
//  Copyright © 2016 SSIC. All rights reserved.
//

import UIKit
import RxSwift

class NRFeedbackView: BaseView {
    @IBOutlet weak var feedbackTextView : UITextView!
    var controller = NRFeedbackHelpViewController()
    var presenter = NRFeedbackPresenter()
    @IBOutlet weak var sendButton : UIButton!
    
    override func viewDidLoad() {
        self.sendButton.backgroundColor = NRColorUtility.appBackgroundColor()
         self.basePresenter = presenter
        feedbackTextView.delegate = presenter
        self.addDoneButtonOnKeyboard()
    }
    
    func sendUserFeedBack(completion : @escaping (Bool?, Error?) -> ()) {
        var stored = false
        var errorStored : Error? = nil
        self.prepareLoadView()
       let _ = self.presenter.storeFeedback(message: self.feedbackTextView.text, log: LogUtility.logAsString()).subscribe(onNext: {granted in
        stored = granted
        }, onError: {error in
            self.stopActivityAnimation()
            errorStored = error
        }, onCompleted: {
            EventLogger.logOnSendFeedback()
            self.stopActivityAnimation()
            completion(stored, errorStored)
        }, onDisposed: {})
    }
    
    func addDoneButtonOnKeyboard()
    {
        let doneToolbar: UIToolbar = UIToolbar(frame:CGRect(x: 0, y: 0, width: 320, height: 50))
        doneToolbar.barStyle = UIBarStyle.blackTranslucent
        
        let flexSpace = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil)
        let done: UIBarButtonItem = UIBarButtonItem(title: "Done", style: UIBarButtonItemStyle.done, target: self, action:  #selector(doneButtonAction))
        
        var items : [UIBarButtonItem] = []
        items.append(flexSpace)
        items.append(done)
        
        doneToolbar.items = items
        doneToolbar.sizeToFit()
        
        self.feedbackTextView.inputAccessoryView = doneToolbar
    }
    
    func doneButtonAction()
    {
        self.feedbackTextView.resignFirstResponder()
    }

}

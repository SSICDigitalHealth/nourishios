//
//  CaloriesDailyAverageModel_to_CaloriesDailyAverageViewModel.swift
//  Nourish
//
//  Created by Vlad Birukov on 29.08.17.
//  Copyright © 2017 SSIC. All rights reserved.
//

import Foundation

class CaloriesDailyAverageModel_to_CaloriesDailyAverageViewModel {
    func transform(model: CaloriesDailyAverageModel) -> CaloriesDailyAverageViewModel {
        let caloriesDailyAverageViewModel = CaloriesDailyAverageViewModel()
        caloriesDailyAverageViewModel.caloriesDailyAverage = model.caloriesDailyAverage
        caloriesDailyAverageViewModel.caloriesOverTarget = model.caloriesOverTarget
        
        return caloriesDailyAverageViewModel
    }
}

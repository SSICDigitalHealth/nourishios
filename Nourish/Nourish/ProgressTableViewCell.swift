//
//  ProgressTableViewCell.swift
//  Nourish
//
//  Created by Vino (Vinodha) Sundaramoorthy on 2/19/17.
//  Copyright © 2017 SSIC. All rights reserved.
//

import UIKit

class ProgressTableViewCell: UITableViewCell {
    @IBOutlet weak var progressLabel : UILabel!
    @IBOutlet weak var menuImageView : UIImageView!
    @IBOutlet weak var menuName : UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        self.progressLabel.text = ""
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}

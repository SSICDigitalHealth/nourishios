//
//  NutritionalDetailBalance.swift
//  Nourish
//
//  Created by Vlad Birukov on 19.06.17.
//  Copyright © 2017 SSIC. All rights reserved.
//

import Foundation

class NutritionalBalanceModel_to_NutritionBalanceViewModel {
    let mapperChart = NutrientDetailInformationModel_to_NutrientDetailInformationViewModel()
    let mapperElement = CalorificFoodModel_to_CalorificFoodViewModel()
    let mapperNutrition = NutritionalBalanceModel_to_NutritionalBalanceViewModel()
    
    func transform(model: NutritionBalanceModel) -> NutritionBalanceViewModel{
        let nutritionBalanceViewModel = NutritionBalanceViewModel()
                
        if model.message != nil {
            nutritionBalanceViewModel.message = model.message
        }
        
        if model.nameImage != nil {
            nutritionBalanceViewModel.nameImage = model.nameImage
        }
        
        nutritionBalanceViewModel.nutritionData = self.mapperNutrition.transform(model: model.nutritionData)
        
        if model.arrNutritionElement.count != 0 {
            for element in model.arrNutritionElement {
                nutritionBalanceViewModel.arrNutritionElement.append(self.transform(model: element))
            }
        }
        return nutritionBalanceViewModel
    }
    
    private func transform(model: NutritionElementModel) -> NutritionElementViewModel {
        let nutritionElementViewModel = NutritionElementViewModel()
        
        nutritionElementViewModel.consumedAmount = model.consumedAmount
        
        if model.type != nil {
            nutritionElementViewModel.type = model.type
        }
        
        if model.target != nil {
            nutritionElementViewModel.target = model.target
        }
        
        nutritionElementViewModel.unit = model.unit.replacingOccurrences(of: "_eq", with: "")
        nutritionElementViewModel.message = model.message
        if model.nameImageMessage != nil {
            nutritionElementViewModel.nameImageMessage = model.nameImageMessage
        }
        
        if model.arrNutrition != nil {
            nutritionElementViewModel.arrNutrition = [ElementViewModel]()
            nutritionElementViewModel.arrNutrition = self.mapperElement.transform(model: model.arrNutrition!)
        }
    
        if model.dataCharts != nil {
            nutritionElementViewModel.dataCharts = [NutrientInformationChartViewModel]()
            nutritionElementViewModel.dataCharts = self.mapperChart.transform(model: model.dataCharts!)
        
        if model.type != nil {
            nutritionElementViewModel.caption = String(format: "Your top %@ sources this", NutritionType.description(type: model.type!).lowercased())
        }
        
        if model.richNutritionFood != nil {
            nutritionElementViewModel.richNutritionFood = [ElementModel]()
            nutritionElementViewModel.richNutritionFood = model.richNutritionFood
        }
            
        }
        return nutritionElementViewModel
    }
}

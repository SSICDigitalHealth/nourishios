//
//  NutrientsTableViewCell.swift
//  Nourish
//
//  Created by Vino (Vinodha) Sundaramoorthy on 4/18/17.
//  Copyright © 2017 SSIC. All rights reserved.
//

import UIKit

class NutrientsTableViewCell: UITableViewCell {
    
    @IBOutlet weak var nutrientName : UILabel!
    @IBOutlet weak var info : UILabel!
    @IBOutlet weak var progressBar : UIProgressView!

    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    func setUpNutrientInfoWithModel(nut:nutrientInfo) {
        nutrientName.text = NutritionsUtility.formatNutrientInfo(nutritionName: nut.name)
        nutrientName.textColor = NRColorUtility.nourishTextColor()
        
        //Progress Bar
        let max = nut.max != nil ? nut.max : nut.min
        progressBar.progress = Float(nut.value / max!)
        progressBar.progressTintColor = nut.progressColor()
        
        //Amount
        let limitAttribute = [NSForegroundColorAttributeName: NRColorUtility.taupeGrayColor() , NSFontAttributeName : UIFont.systemFont(ofSize: 12)]
        let valueAttribute = [ NSForegroundColorAttributeName: NRColorUtility.nourishTextColor() , NSFontAttributeName : UIFont.systemFont(ofSize: 12)]
        let amountString = NSMutableAttributedString(string: "", attributes: limitAttribute)
        
        if nut.min != nil && nut.min! > 0 {
            amountString.append(NSAttributedString(string: String(format:"%.0f / ",nut.min!), attributes: limitAttribute))
        }
        
        amountString.append(NSAttributedString(string: String(format:"%.0f %@ ",nut.value,nut.unit), attributes: valueAttribute))
        
        if nut.max != nil {
            amountString.append(NSAttributedString(string: String(format:"/ %.0f",nut.max!), attributes: limitAttribute))
        }
        info.attributedText = amountString
        
        progressBar.transform = CGAffineTransform(scaleX: 1, y: 6)

    }
    
}

//
//  BaseViewController.swift
//  Nourish
//
//  Created by Nova on 11/15/16.
//  Copyright © 2016 SSIC. All rights reserved.
//

import UIKit
import RxSwift


class BasePresentationViewController: BaseViewController {
    
    var baseViews : [BaseViewProtocol]?
    var disposeBag = DisposeBag()
    var showingError : Bool = false

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if baseViews != nil {
            for view in baseViews! {
                view.viewWillAppear(animated)
            }
        }
    }
   
    override func showNoNetworkError() {
        if self.canShowNoNetworkBanner() {
            if self.bannerView == nil {
                self.bannerView = self.view.showBanner(message: "No Internet Connection", showingError: false, completion: nil)
            }
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        if baseViews != nil {
            for view in baseViews! {
                view.viewDidAppear(animated)
            }
        }
        
        
    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        if baseViews != nil {
            for view in baseViews! {
                view.viewWillDisappear(animated)
            }
        }
        
        
    }
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        if baseViews != nil {
            for view in baseViews! {
                view.viewDidDisappear(animated)
            }
        }
        self.disposeBag = DisposeBag()
    }
    
    override func viewDidLayoutSubviews() {
        if baseViews != nil {
            for view in baseViews! {
                view.viewDidLayoutSubviews()
            }
        }
    }

    // MARK: Navigation bar actions
    func closeAction(_ sender : UIButton){
        self.dismiss(animated: true, completion: nil)
    }
    
    func simplyfiedBarTitileAttributes () -> [String : Any] {
        return [NSForegroundColorAttributeName : NRColorUtility.nourishNavBarFontColor() ,NSFontAttributeName :UIFont.systemFont(ofSize: 17, weight: 0.1)]
    }
    
    func navigationBarTitleAttribute() -> [String : Any] {
        return [NSForegroundColorAttributeName : NRColorUtility.navigationTitleColor() ,NSFontAttributeName :UIFont.init(name: "Campton-Bold", size: 20)!]
    }
    
    func disableNavigationBar() {
        self.navigationController?.navigationBar.isUserInteractionEnabled = false
        self.navigationController?.view.isUserInteractionEnabled = false
    }
    
    func enableNavigationBar() {
        self.navigationController?.navigationBar.isUserInteractionEnabled = true
        self.navigationController?.view.isUserInteractionEnabled = true
    }

    
    func parseError(error: Error, completion:((Bool) -> Void)?) {
        if error.code == 429 {
            if self.showingError == false {
                self.showingError = true
                self.showToast(message: "Daily rate exceeded, error 429", showingError: true, completion: {completed in
                    self.showingError = false
                    if completion != nil {
                        completion!(true)
                    }
                })
            }
           
        }
    }
    
    
}

extension Error {
    var code: Int { return (self as NSError).code }
    var domain: String { return (self as NSError).domain }
    
}

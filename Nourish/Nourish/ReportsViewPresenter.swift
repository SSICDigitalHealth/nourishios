//
//  ReportsViewPresenter.swift
//  Nourish
//
//  Created by Vino (Vinodha) Sundaramoorthy on 2/8/17.
//  Copyright © 2017 SSIC. All rights reserved.
//

import UIKit

class ReportsViewPresenter: BasePresenter,UITableViewDataSource,UITableViewDelegate {
    
    var interactor = ReportsInteractor()
    var reportsArray : [reportsModel]!
    var reportsView : ReportsProtocol?
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.subscribtions.append(self.interactor.fetchReports().subscribe(onNext: { [unowned self] models in
            self.reportsArray = models
        }, onError: {error in
            LogUtility.logToFile(error)
        }, onCompleted: {
            self.reportsView?.setReports(report: self.reportsArray)
        }, onDisposed: {}))
        
    }
    
    // MARK: UITableViewDataSource
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return reportsArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let data = reportsArray[indexPath.row]
        let cell = UITableViewCell(style: .value1, reuseIdentifier: "reportCell")
        let formatter  = DateFormatter()
        formatter.dateStyle = .medium
        cell.textLabel?.text = formatter.string(from: data.date)
        
        let myCalendar = NSCalendar(calendarIdentifier: NSCalendar.Identifier.gregorian)!
        let myComponents = myCalendar.components(.weekday, from: data.date)
        let weekDay = myComponents.weekday
        cell.detailTextLabel?.text = self.getWeekDay(day: weekDay!)
        cell.detailTextLabel?.tintColor = NRColorUtility.progressLabelColor()
        cell.textLabel?.textColor = NRColorUtility.progressLabelColor()
        cell.textLabel?.textColor = NRColorUtility.progressLabelColor()

        return cell
    }
    
    // MARK: UITableViewDelegate
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        reportsView?.showDetailedReport(date : self.reportsArray[indexPath.row].date)
    }
    
    func getWeekDay(day:Int) -> String {
        switch day {
            case 1: return "Sunday"
            case 2: return "Monday"
            case 3: return "Tuesday"
            case 4: return "Wednesday"
            case 5: return "Thursday"
            case 6: return "Friday"
            case 7: return "Saturday"
            default: return ""
        }
    }
    
}

//
//  NoOcasionTableViewCell.swift
//  Nourish
//
//  Created by Nova on 11/11/16.
//  Copyright © 2016 Samsung. All rights reserved.
//

import UIKit
import QuartzCore


class NoOcasionTableViewCell: UITableViewCell {
    @IBOutlet weak var ocasionTitle : UILabel?
    @IBOutlet weak var plusImageView : UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.plusImageView.image = self.plusImageView.image?.withRenderingMode(.alwaysTemplate)
        self.plusImageView.tintColor = NRColorUtility.appBackgroundColor()
    }
    
    override func draw(_ rect: CGRect) {
        super.draw(rect)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}

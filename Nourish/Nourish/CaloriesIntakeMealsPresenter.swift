//
//  CaloriesIntakeMealsPresenter.swift
//  Nourish
//
//  Created by Vlad Birukov on 20.06.17.
//  Copyright © 2017 SSIC. All rights reserved.
//

import Foundation
import RxSwift


class CaloriesIntakeMealsPresenter: BasePresenter {
    let objectToModelMapper = CaloriesIntakeMealsModel_to_CaloriesIntakeMealsViewModel()
    let interactor =  CaloriesIntakeMealsInteractor()
    var caloriesIntakeMealsView: CaloriesIntakeMealsProtocol!
    var date: (Date, Date)?
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.getCaloriesIntakeDetail()
    }
    
    private func getCaloriesIntakeDetail() {
        if self.caloriesIntakeMealsView.config() != nil {
            self.date = self.caloriesIntakeMealsView.config()?.getDate()
        }
        
        if self.date != nil {
            self.subscribtions.append(self.interactor.execute(startDate: (self.date?.0)!, endDate: (self.date?.1)!).observeOn(MainScheduler.instance).subscribe(onNext: { [unowned self] caloriesModel in
                self.caloriesIntakeMealsView.setupWithModel(model: self.objectToModelMapper.transform(model: caloriesModel))
                self.caloriesIntakeMealsView.stopActivityAnimation()
            }, onError: {error in
                self.caloriesIntakeMealsView.stopActivityAnimation()
                self.caloriesIntakeMealsView.parseError(error : error, completion: nil)
            }, onCompleted: {
            }, onDisposed: {
                self.caloriesIntakeMealsView.stopActivityAnimation()
            }))
        }
    }
}

//
//  LoggedUserEatStateInteractor.swift
//  Nourish
//
//  Created by Vlad Birukov on 09.06.17.
//  Copyright © 2017 SSIC. All rights reserved.
//

import Foundation
import RxSwift

class LoggedUserEatStateInteractor {
    
    let repository = NestleProgressRepository.shared
    
    func execute(startDate: Date, endDate: Date) -> Observable<LoggedUserEatStateModel> {
        return repository.getProgressFor(startDate: startDate, endDate: endDate, returnHistory: true).flatMap({ observer -> Observable<LoggedUserEatStateModel> in
            return Observable.just(self.transform(model: observer.userOcasion))
        })
    }

    private func transform(model: [Element]) -> LoggedUserEatStateModel {
        var proxyArr: [(Ocasion, Double)] = [(.breakfast, 0.0), (.lunch, 0.0), (.dinner, 0.0), (.snacks, 0.0)]
        
        for i in 0..<proxyArr.count {
           if model.flatMap({$0.nameFoodIntake}).filter({Ocasion.enumFromMealPlanner(string: $0) == proxyArr[i].0}).count > 0 {
                let currentOccasion = model.filter({Ocasion.enumFromMealPlanner(string: $0.nameFoodIntake) == proxyArr[i].0})
            
                if currentOccasion.count > 0 {
                    var calories = 0.0
                    for item in currentOccasion {
                        calories += item.consumCalories
                    }
                    proxyArr[i].1 = calories

                }
            }
        }

        return self.transforms(currentOcasion: proxyArr)
    }
    
    private func transforms(currentOcasion: [(Ocasion, Double)]) -> LoggedUserEatStateModel {
        let userCurrentOcasion = LoggedUserEatStateModel()
        userCurrentOcasion.currentOcasion = currentOcasion
        
        return userCurrentOcasion
    }

}

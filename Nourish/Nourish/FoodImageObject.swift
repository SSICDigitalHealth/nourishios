//
//  FoodImageObject.swift
//  Nourish
//
//  Created by Nova on 5/12/17.
//  Copyright © 2017 SSIC. All rights reserved.
//

import Foundation
import RealmSwift

class FoodImageObject: Object {
    dynamic var userPhotoID : String = ""
    dynamic var imagePath : String?
    dynamic var localCacheID = "local" + UUID().uuidString
}

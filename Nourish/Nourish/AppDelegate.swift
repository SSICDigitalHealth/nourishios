//
//  AppDelegate.swift
//  Nourish
//
//  Created by Vino (Vinodha) Sundaramoorthy on 10/20/16.
//  Copyright © 2016 SSIC. All rights reserved.
//

import UIKit
import CoreData
import HealthKit
import RealmSwift
import UserNotifications
import Fabric
import Crashlytics
import SwiftKeychainWrapper

let kLastActivatedDate = "Last activation date"

@available(iOS 10.0, *)
@UIApplicationMain

class AppDelegate: UIResponder, UIApplicationDelegate, UNUserNotificationCenterDelegate {

    var window: UIWindow?
    var stressCacher = BackgroundStressCacher.shared
    var needToLogFood : Bool = false
    
    let timeZoneInteractor = LambdaTimeZoneUploaderInteractor()
    let bgFetcher = BackgroundChangeScoreFetcher.shared
    let foodCompsDownloader = NestleFoodComponentsInteractor()
    let fitBitCacher = BackgroundFitBitCacher.shared
    let photosInteractor = UserPhotosInteractor()
    let proxyCacher = ProxyCacherRepo.shared
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        
        window = UIWindow(frame: UIScreen.main.bounds)
        
        
        self.realmMigationCheck(completion: { migrated in
            if migrated == true {
                let loginSplashVC = NRLoginSplashViewController(nibName: "NRLoginSplashViewController", bundle: nil)
                self.window?.rootViewController = loginSplashVC
                self.window?.makeKeyAndVisible()
                
            }
        })
        
        if #available(iOS 10.0, *) {
            UNUserNotificationCenter.current().delegate = self
        } else {
            let notification : UILocalNotification? = launchOptions?[UIApplicationLaunchOptionsKey.localNotification] as? UILocalNotification
            if notification != nil {
                self.application(application, didReceive: notification!)
            }
        }
        
        let _ = EventLogger.initAnalyticsSystems().subscribe()
        #if RELEASE
            Fabric.with([Crashlytics.self])
            Fabric.with([Answers.self])
        #endif
        if PermissionsUtility.shared.permissionsWasAsked() {
            PermissionsUtility.shared.initSentianceSDK(launchOptions: launchOptions)
        }
        
        self.setupStatusBar()
        
        self.photosInteractor.execute()
        
        //Initialize Sound Hound
        Hound.setClientID(kSoundHoundClientId)
        Hound.setClientKey(kSoundHoundClientKey)
        
        if defaults.object(forKey: kForceCleanCache) == nil {
            NRUserSession.sharedInstance.cleanCache()
        }
        
        
        return true
    
    }
    
    func application(_ application: UIApplication, didRegister notificationSettings: UIUserNotificationSettings) {
        NotificationCenter.default.post(name: NSNotification.Name("Notifications permissions was asked"), object: nil)
        if notificationSettings.types != UIUserNotificationType() {
            application.registerForRemoteNotifications()
        }
    }
    
    
    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        let token = deviceToken.map { String(format: "%02.2hhx", $0) }.joined()
        LogUtility.logToFile("Successfully subscribed to push notifications with token %@", token)
        if NRUserSession.sharedInstance.accessToken != nil {
            let amazonInteractor = AmazonLambdaInteractor()
            let deviceId = UIDevice.current.identifierForVendor!.uuidString
            let _ = amazonInteractor.uploadPushTokenToLambda(token: token, deviceId: deviceId).subscribe(onNext: {uploaded in
                if uploaded == true {
                    LogUtility.logToFile("Push token successfully uploaded to lambda with device id", deviceId)
                } 
            }, onError: {error in
                LogUtility.logToFile("Push token failed to upload to lambda with error %@", error)
            }, onCompleted: {}, onDisposed: {})
        }
    }
    
    func application(_ application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: Error) {
        
    }
    
    
    private func setupStatusBar() {
        let statusBar: UIView = UIApplication.shared.value(forKey: "statusBar") as! UIView
        if statusBar.responds(to:#selector(setter: UIView.backgroundColor)) {
            statusBar.backgroundColor = NRColorUtility.appBackgroundColor()
        }
        
        UIApplication.shared.statusBarStyle = .lightContent
    }
    
    func userNotificationCenter(_ center: UNUserNotificationCenter, didReceive response: UNNotificationResponse, withCompletionHandler completionHandler: @escaping () -> Void) {
        if UIApplication.shared.applicationState != .active {
            self.needToLogFood = true
            self.navigateToLogFoodFor(date: response.notification.date)
        }
    }
    
    private func navigateToLogFoodFor(date:Date) {
        if self.needToLogFood == true {
            self.processNotification(date: date)
            self.needToLogFood = false
        }
    }
    
    func application(_ app: UIApplication, open url: URL, options: [UIApplicationOpenURLOptionsKey : Any]) -> Bool {
        print("Open application")
        let notification = Notification(
            name: Notification.Name(rawValue: NotificationConstants.fitBitLaunchNotification),
            object:nil,
            userInfo:[UIApplicationLaunchOptionsKey.url:url])
        NotificationCenter.default.post(notification)
        NotificationCenter.default.post(name : NSNotification.Name(rawValue: kCloseSafariViewControllerNotification), object: url)
        return true
    }

    func application(_ application: UIApplication, didReceive notification: UILocalNotification) {
        print("Received notification")
        if application.applicationState != .active && notification.fireDate != nil {
            self.needToLogFood = true
            self.navigateToLogFoodFor(date: notification.fireDate!)
        }
    }
    
    
    private func processNotification(date : Date) {
        let rootController = self.window?.rootViewController
        if rootController != nil{
            if rootController!.isKind(of: NRLoginSplashViewController.self) {
                (rootController as! NRLoginSplashViewController).awakedWithNotification = true
                (rootController as! NRLoginSplashViewController).dateForFoodDiary = date
            } else if rootController!.isKind(of: NourishTabBarViewController.self) {
                NavigationUtility.navigateToFoodDiaryFor(date: date)
            }
        }
    }
    
    
    func applicationWillResignActive(_ application: UIApplication) {
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        //self.stressCacher.stopCaching()
        self.proxyCacher.storeLastCache()
        self.setupNotifications()
        defaults.set(Date(), forKey: backgroundDateKey)
    }               

    func applicationWillEnterForeground(_ application: UIApplication) {    
        if NRUserSession.sharedInstance.accessToken != nil && defaults.object(forKey: kOnBoardingKeyDate) != nil {
            let rootController = self.window?.rootViewController
            if rootController != nil{
                if rootController!.isKind(of: NRLoginSplashViewController.self) {
                    (rootController as! NRLoginSplashViewController).awakedFromBackground = true
                } else if rootController!.isKind(of: NourishTabBarViewController.self) && defaults.bool(forKey: onBoardingKey) == true {
                    NavigationUtility.chatViewControllerInstance().awakedFromBackground = true
                    
                    let dateToCompare = defaults.value(forKey: kLastActivatedDate) as? Date ?? Date()
                    if self.needToShowChatFor(dateToCompare: dateToCompare) {
                        NavigationUtility.dismiss(animated: false)
                        NavigationUtility.hideTabBar(animated: false)
                        NavigationUtility.showChatView()
                    }
                    let _ = NRUserSession.sharedInstance.isEmailLocked().subscribe(onNext: {isLocked in
                        if isLocked == true {
                            NRUserSession.sharedInstance.logout(keepCurrent: false)
                            DispatchQueue.main.async {
                                NavigationUtility.changeRootViewController(userProfile: nil, awakedFromBackground: true)
                            }
                        }
                    }, onError: {error in}, onCompleted: {}, onDisposed: {})
                }
            }
        }
        defaults.set(Date(), forKey: kLastActivatedDate)
        defaults.synchronize()
        //Assuming that the root view controller is the viewController
    }

    
    private func needToShowChatFor(dateToCompare : Date) -> Bool {
        var needToShow = false
        let calendar = Calendar.current

        let date = calendar.date(byAdding: .hour, value: 6, to: calendar.startOfDay(for: Date())) ?? Date()
        if dateToCompare < date {
            needToShow = true
        }
        return needToShow
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        print("Application becoming active")

        NRUserSession.sharedInstance.refreshArtikToken()
        ActivityUtility.forceRefetchData()

      
        //check later
      /*  let _ = NoomRepository.shared.testInit(completion:{succes in
        })*/
        self.timeZoneInteractor.execute()
        self.stressCacher.startCaching()
        
        if KeychainWrapper.standard.string(forKey: kActivitySourceKey) != nil {
            if KeychainWrapper.standard.string(forKey: kActivitySourceKey) == "Fit Bit" {
                self.fitBitCacher.startCaching()
            }
        }
        
        let syncer = BackgroundMealDiarySyncInteractor.shared
        syncer.isInProgress = false
        syncer.testFetch()
        self.foodCompsDownloader.execute()
    }
    
    
    func applicationWillTerminate(_ application: UIApplication) {
        self.setupNotifications()
        self.saveContext()
    }
    
   /* private func loadNoomDB() {
        let noomFix = NoomRepository.shared
        noomFix.loadDataBase()
    }*/
    
    private func setupNotifications() {
        PermissionsUtility.shared.setupNotifications()
    }

    // MARK: - Core Data stack

    lazy var persistentContainer: NSPersistentContainer = {
        /*
         The persistent container for the application. This implementation
         creates and returns a container, having loaded the store for the
         application to it. This property is optional since there are legitimate
         error conditions that could cause the creation of the store to fail.
        */
        let container = NSPersistentContainer(name: "Nourish")
        container.loadPersistentStores(completionHandler: { (storeDescription, error) in
            if let error = error as NSError? {
                // Replace this implementation with code to handle the error appropriately.
                // fatalError() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                 
                /*
                 Typical reasons for an error here include:
                 * The parent directory does not exist, cannot be created, or disallows writing.
                 * The persistent store is not accessible, due to permissions or data protection when the device is locked.
                 * The device is out of space.
                 * The store could not be migrated to the current model version.
                 Check the error message to determine what the actual problem was.
                 */
                fatalError("Unresolved error \(error), \(error.userInfo)")
            }
        })
        return container
    }()
    
    func realmMigationCheck(completion : @escaping (Bool) ->()) {
        var config = Realm.Configuration(
            // Set the new schema version. This must be greater than the previously used
            // version (if you've never set a schema version before, the version is 0).
            schemaVersion: 4,
            
            // Set the block which will be called automatically when opening a Realm with
            // a schema version lower than the one set above
            migrationBlock: { migration, oldSchemaVersion in
                // We haven’t migrated anything yet, so oldSchemaVersion == 0
                if (oldSchemaVersion < 4) {
                    // Nothing to do!
                    // Realm will automatically detect new properties and removed properties
                    // And will update the schema on disk automatically
                }
        },
            shouldCompactOnLaunch: { totalBytes, usedBytes in
                let size = 500 * 1024 * 1024
                let bool = (totalBytes > size) && (Double(usedBytes) / Double(totalBytes)) < 0.6
                return bool
        })
        
        // Tell Realm to use this new configuration object for the default Realm
        Realm.Configuration.defaultConfiguration = config
        
        // Now that we've told Realm how to handle the schema change, opening the file
        // will automatically perform the migration
        let _ = try! Realm()
        completion(true)
        
    }
    // MARK: - Core Data Saving support

    func saveContext () {
        let context = persistentContainer.viewContext
        if context.hasChanges {
            do {
                try context.save()
            } catch {
                // Replace this implementation with code to handle the error appropriately.
                // fatalError() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                let nserror = error as NSError
                fatalError("Unresolved error \(nserror), \(nserror.userInfo)")
            }
        }
    }
    
    func stopCaching() {
        self.stressCacher = BackgroundStressCacher()
    }
/*
    func application(_ application: UIApplication, performFetchWithCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void) {
        self.realmMigationCheck(completion: {migrated in
            if migrated == true {
                _ = self.messageUtility.uploadMessage(completion: {result in
                    completionHandler(result)
                })
            }
        })
        
    }
 */

}


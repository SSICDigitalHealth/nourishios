//
//  OptifastMealPlannerChangeDishPresenter.swift
//  Optifast
//
//  Created by Gena Mironchyk on 1/11/18.
//  Copyright © 2018 SSIC. All rights reserved.
//

import UIKit
import RxSwift
class OptifastMealPlannerChangeDishPresenter: BasePresenter, UITableViewDelegate, UITableViewDataSource {
    
    var model : OptifastMealPlannerViewModel!
    var ocasion : Ocasion!
    private let interactor = OptifastMealPlannerChangeDishInteractor()
    private var arrayToShow : [foodSearchModel]?
    var changeDishView : OptifastMealPlannerChangeDishProtocol!
    var controller : OptifastMealPlannerChangeDishViewController!
    private var selectedFoodId = ""
    private let saveToCacheInteractor = OptifastMealPlannerChangeDishStoreCacheInteractor()
    private let toModelMapper = OptifastMealPlannerViewModel_to_OptifastMealPlannerModel()
    
    
    override func viewWillAppear(_ animated: Bool) {
        let _ = self.interactor.execute().observeOn(MainScheduler.instance).subscribe(onNext: { [unowned self] arrayOfFoodIds in
            self.arrayToShow = [foodSearchModel]()
            if arrayOfFoodIds.count > 0 {
                for foodId in arrayOfFoodIds {
                    if let food = self.model.optifastFoods.first(where: {$0.foodId == foodId}) {
                        self.arrayToShow!.append(food)
                    }
                }
                if let optifFood = self.model.foods![self.ocasion.rawValue - 1].ocasionFoods.first(where: {$0.meal.foodId.contains("[opti]")}) {
                    if !self.arrayToShow!.contains(where: {$0.foodId == optifFood.meal.foodId}) {
                        self.arrayToShow!.append(optifFood.meal)
                    }
                }
            } else {
                self.arrayToShow =  self.model.optifastFoods
            }
            self.changeDishView.stopActivityAnimation()
            self.changeDishView.reloadData()
            
        }, onError: {error in}, onCompleted: {}, onDisposed: {})
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.arrayToShow?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "changeDishCell") as! OptifastMealPlannerChangeDishCell
        let foodToCompare = self.arrayToShow![indexPath.row]
        cell.foodNameLabel.text = self.arrayToShow![indexPath.row].foodTitle
        let occFooods =  self.model.foods![self.ocasion.rawValue - 1].ocasionFoods
        if occFooods.contains(where: {$0.meal.foodId == foodToCompare.foodId}) {
            cell.selectedImageView.image = UIImage(named :  "radio_on_green" )
            self.selectedFoodId = foodToCompare.foodId
        } else {
            cell.selectedImageView.image = UIImage(named :  "radio_off_green" )
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let dish = self.arrayToShow![indexPath.row]
        if dish.foodId != self.selectedFoodId {
            var foods = self.model.foods![ocasion.rawValue - 1].ocasionFoods
            foods = foods.filter{$0.meal.foodId != self.selectedFoodId}
            foods.insert((meal: dish, mealReciepe: nil), at: 0)
            self.model.foods![ocasion.rawValue - 1].ocasionFoods = foods
            self.changeDishView.reloadData()
        }
    }
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 44.0
    }
    
    func processSave() {
        let _ = self.saveToCacheInteractor.execute(model: self.toModelMapper.transform(viewModel: self.model)).observeOn(MainScheduler.instance).subscribe(onNext: {saved in
            print("Changed optifast dish saved")
        }, onError: {error in}, onCompleted: {}, onDisposed: {})
    }
    
}

//
//  StartMealPlannerViewController.swift
//  Nourish
//
//  Created by Vlad Birukov on 18.08.17.
//  Copyright © 2017 SSIC. All rights reserved.
//

import UIKit
import RxSwift
import RealmSwift

class StartMealPlannerViewController: BasePresentationViewController {
    @IBOutlet weak var startMealPlannerView: StartMealPlannerView!
    @IBOutlet weak var mealPlannerNavigationBarView: MealPlannerNavigationBarView!


    override func viewDidLoad() {
        super.viewDidLoad()
        self.baseViews = [mealPlannerNavigationBarView, startMealPlannerView]
        for v in self.baseViews! {
            v.viewDidLoad()
        }
        
        self.startMealPlannerView.presenter.startMealPlannerViewController = self
        self.checkCache()
    }
    
    
    func checkCache() {
        if self.startMealPlannerView != nil {
            self.startMealPlannerView.presenter.isCache()
        }
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.automaticallyAdjustsScrollViewInsets = false
        NavigationUtility.showTabBar(animated: true)
        setUpNavigationBar()
    }

    func setUpNavigationBar() {
        if self.navigationController != nil {
            self.title = ""
            navigationController?.setNavigationBarHidden(true, animated: false)
            mealPlannerNavigationBarView.resetButton.isHidden = true

        }
    }
}

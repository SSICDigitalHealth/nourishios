//
//  NutrientUtility.swift
//  Nourish
//
//  Created by Nova on 3/22/17.
//  Copyright © 2017 SSIC. All rights reserved.
//

import UIKit
import RxSwift


class NutrientUtility: NSObject {
    
    func getCombinedNutrsFrom(mealRecords : [MealRecordModel]) -> Observable<[Nutrient]> {
      //  let noomInt = NRFoodDetailsInteractor()
      //  let nonNoomRepo = MealHistoryRepository.shared
        let mapper = MealRecord_to_foodSearchModel()
        let arrayOfNoomNutritions : [Observable<[Nutrient]>] = []
        var arrayOfRecNutritions : [Observable<[Nutrient]>] = []
        var calories = 0.0
        let mealDiary = MealDiaryInteractor()
        
        for model in mealRecords {
            let searchModel = mapper.transform(record: model)
            if model.isFromNoom == true && model.foodId?.rangeOfCharacter(from: .whitespaces) == nil && model.foodId != nil && (model.foodId?.count)! > 0{
                //arrayOfNoomNutritions.append(noomInt.fetchNutrientDetailsForModel(foodModel: searchModel))
            } else {
                arrayOfRecNutritions.append(mealDiary.fetchOnlyNutrients(foodSearchModel : searchModel))
            }
            
            if model.calories != nil {
                calories += model.calories!
            }
            
        }
        let singleObsNoom = Observable.from(arrayOfNoomNutritions).merge()
        let wholeSequenceNoom: Observable<[[Nutrient]]> = singleObsNoom.toArray()
        
        var noomClearArray : [Nutrient] = []
        let _ = wholeSequenceNoom.subscribe(onNext: { [unowned self] nutrients in
            noomClearArray.append(contentsOf: self.mergeNoomNutrients(noomNutr: nutrients))
        }, onError: {error in}, onCompleted: {}, onDisposed: {})
        
        let singleObsRecs = Observable.from(arrayOfRecNutritions).merge()
        let wholeSequenceRecs: Observable<[[Nutrient]]> = singleObsRecs.toArray()
        
        let zip = Observable.zip(wholeSequenceNoom, wholeSequenceRecs, resultSelector : {noomNutrs, recsNutrs -> [Nutrient] in
            
            var noomClearArray : [Nutrient] = []
            noomClearArray.append(contentsOf: self.mergeNoomNutrients(noomNutr: noomNutrs))
            
            var recsClearArray : [Nutrient] = []
            recsClearArray.append(contentsOf: self.mergeRecsNutritions(array: recsNutrs))
            
            var result = self.mergeNoomAndRecs(noom: noomClearArray, recs: recsClearArray)
            
            if result.count > 0 {
                if result[0].name == calorieString {
                    result.remove(at: 0)
                }
            }
            
            
            result = self.appendCalories(array: result, calories: calories)
            return result
        })
        return zip
    }
    
    private func appendCalories(array : [Nutrient], calories : Double) -> [Nutrient] {
        let cals = Nutrient()
        cals.clearValue = calories
        cals.representationValue = calories
        cals.unit = kCalString
        cals.name = calorieString
        
        var result = array
        result.insert(cals, at: 0)
        
        return result
    }
    
    private func mergeNoomAndRecs(noom : [Nutrient], recs : [Nutrient]) -> [Nutrient] {
        var arrayToReturn : [Nutrient] = []
        NutritionsUtility.formatNutrientName(nutritions: recs)
        
        arrayToReturn.append(contentsOf: noom)
        
        for object in recs {
            let parser = object.name.lowercased()
            let filter = arrayToReturn.filter {$0.name.lowercased() == parser}
            if filter.count > 0 {
                let nutr = filter.first!
                nutr.clearValue += object.clearValue
                //arrayToReturn.append(nutr)
            } else {
                arrayToReturn.append(object)
            }
        }
        
        return self.recountValues(array: arrayToReturn)
    }
    
    private func mergeRecsNutritions(array : [[Nutrient]]) -> [Nutrient] {
        var arrayToMerge : [Nutrient] = []
        for nutrs in array {
            for object in nutrs {
                let parser = object.name
                let filter = arrayToMerge.filter{$0.name == parser}
                if filter.count > 0 {
                    let nutr = filter.first!
                    nutr.clearValue = self.clearValueFrom(value: nutr.representationValue, unit: nutr.unit)
                    let objValue = self.clearValueFrom(value: object
                        .representationValue, unit: nutr.unit)
                    nutr.clearValue += objValue
                } else {
                    object.clearValue = self.clearValueFrom(value: object.representationValue, unit: object.unit)
                    arrayToMerge.append(object)
                }
            }
        }
        let filtered = arrayToMerge.filter {$0.representationValue != 0.0}
        return filtered
    }
    
    private func mergeNoomNutrients(noomNutr : [[Nutrient]]) -> [Nutrient] {
        var arrayToMerge : [Nutrient] = []
        for nutrs in noomNutr {
            for object in nutrs {
                let parser = object.name
                let filter = arrayToMerge.filter{$0.name == parser}
                if filter.count > 0 {
                    let nutr = filter.first!
                    nutr.clearValue += object.clearValue
                } else {
                    arrayToMerge.append(object)
                }
            }
        }
        
        let filtered = arrayToMerge.filter {$0.representationValue != 0.0}
        
        return filtered
    }
    
    private func recNutrsIntoNoom(array : [Nutrient]) -> [Nutrient] {
        var transformedArray : [Nutrient] = []
        
        for object in array {
            object.clearValue = self.clearValueFrom(value: object.representationValue, unit: object.unit)
            transformedArray.append(object)
        }
        
        return transformedArray
    }
    
    private func clearValueFrom(value : Double, unit : String) -> Double {
        var clearValue = 0.0
        if unit == "ug" {
            clearValue = value / 1000
        } else if unit == "g" {
            clearValue = value * 1000
        } else if unit == "kg" {
            clearValue = value*pow(10.0, 6.0)
        } else {
            clearValue = value
        }
        return clearValue
    }
    
    func recountValues(array : [Nutrient]) -> [Nutrient] {
        var returnArray : [Nutrient] = []
        
        for nutr in array {
            var mgram = nutr.clearValue
            var unit = ""
            
            if mgram > 5000 {
                unit = "g"
                mgram = mgram / 1000
            } else if mgram < 5 {
                unit = "μg"
                mgram = mgram * 1000
            } else {
                unit = "mg"
            }
            nutr.representationValue = mgram
            nutr.unit = unit
            returnArray.append(nutr)
            
        }
        
        return returnArray
    }
}

//
//  SaveOptifastSettingsInteractor.swift
//  Nourish
//
//  Created by Gena Mironchyk on 10/19/17.
//  Copyright © 2017 SSIC. All rights reserved.
//

import UIKit
import RxSwift
class SaveOptifastSettingsInteractor: NSObject {
    
    private let userRepository = UserRepository.shared
    
    func execute(settingsViewModel : OptifastSettingsViewModel) -> Observable<Bool> {
        return self.userRepository.getCurrentUser(policy: .DefaultPolicy).flatMapLatest{userProfile -> Observable<Bool> in
            let model = self.transform(settingsViewModel: settingsViewModel)
            userProfile.optifastOcasionsList = model.optifastUserOcasions
            userProfile.optifastFoodIds = model.optifastUserProducts.map({return $0.foodId})
            self.userRepository.storeCurrentUserInBackground(user: userProfile, writeToHealth: false)
            return Observable.just(true)
        }
    }
    
    private func transform(settingsViewModel : OptifastSettingsViewModel) -> OptifastSettingsModel {
        let optifastModel = OptifastSettingsModel()
        optifastModel.optifastUserOcasions = settingsViewModel.optifastUserOcasions
        optifastModel.optifastUserProducts = settingsViewModel.optifastUserProducts
        return optifastModel
    }
    
}

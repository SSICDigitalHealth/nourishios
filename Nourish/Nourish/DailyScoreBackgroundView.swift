//
//  DailyScoreBackgroundView.swift
//  Nourish
//
//  Created by Andrey Zmushko on 9/27/17.
//  Copyright © 2017 SSIC. All rights reserved.
//

import UIKit

class DailyScoreBackgroundView: UIView {

    override func draw(_ rect: CGRect) {
        let color = NRColorUtility.hexStringToUIColor(hex: "70c397")
        color.setFill()
        let path = UIBezierPath()
        path.move(to: CGPoint(x: rect.size.width, y: 0))
        path.addLine(to: CGPoint(x: rect.size.width, y: rect.size.height))
        path.addLine(to: CGPoint(x: 16, y: rect.size.height))
        path.addQuadCurve(to: CGPoint(x: 16, y: 0), controlPoint: CGPoint(x: 0, y: rect.size.height / 2))
        path.close()
        path.fill()
    }

}

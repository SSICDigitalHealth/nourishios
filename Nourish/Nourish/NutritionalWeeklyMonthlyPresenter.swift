//
//  NutritionalWeeklyMonthlyPresenter.swift
//  Nourish
//
//  Created by Vlad Birukov on 16.08.17.
//  Copyright © 2017 SSIC. All rights reserved.
//

import Foundation

class NutritionalWeeklyMonthlyPresenter: BasePresenter, ProgressBarChartDelegate {
    
    var progressBarChart: ProgressBarChart?
    
    let colorNutrition = [NutritionType.grains: NRColorUtility.hexStringToUIColor(hex: "F0C517"), NutritionType.protein: NRColorUtility.hexStringToUIColor(hex: "A64DB6"), NutritionType.dairy: NRColorUtility.hexStringToUIColor(hex: "1172B9"), NutritionType.vegs: NRColorUtility.hexStringToUIColor(hex: "70C397"), NutritionType.fruits: NRColorUtility.hexStringToUIColor(hex: "f6321d")]

    var nutritionalElementModel = NutritionElementViewModel()
    var proportionWithChart = (0.0, 0.0)
    var axisLabel = 0.0

    func setupWith(model: NutritionElementViewModel) {
        self.nutritionalElementModel = model
        self.proportionWithChart = getProportionWithChartLine()
        self.progressBarChart?.widthBarPattern = (0.9, 0)
        self.progressBarChart?.reloadData(DataSource: self)
    }
    
    func numberOfBars(in progressBarChart: ProgressBarChart) -> Int {
        if nutritionalElementModel.dataCharts != nil {
            return (nutritionalElementModel.dataCharts?.count)!
        } else {
            return 0
        }
    }
    
    func numberOfAxis(in progressBarChart: ProgressBarChart) -> Int {
        return 3
    }
    
    func progressBarChart(_ progressBarChart: ProgressBarChart, axisAtIndex:Int) -> ProgressBarChart.Axis? {
        let type: ProgressBarChart.AxisType!
        let value: Double!
        var color = UIColor.gray
        var label: ProgressBarChartLabel?
        
        if let target = nutritionalElementModel.target {
            axisLabel = target
        }
        
        switch axisAtIndex {
        case 2:
            type = .solid
            value = 1.0
            color = UIColor.clear
        case 1:
            type = .dashed
            value = proportionWithChart.0
            color = UIColor.black.withAlphaComponent(0.2)
            label = ProgressBarChartLabel()
            label?.minHeightWith(height: 4,
                                 text: String(format: "%.1f %@", axisLabel, nutritionalElementModel.unit),
                                 numberOfLines: 1,
                                 font: UIFont(name: "Roboto-Regular", size: 14)!,
                                 color: NRColorUtility.hexStringToUIColor(hex: "808080"),
                                 verticalAlignment: ProgressBarChartLabel.LabelAlignment.top)
        default:
            type = .solid
            value = 0.0
            color = UIColor.black.withAlphaComponent(0.2)
        }
        
        return (color, type, value, label, nil) as ProgressBarChart.Axis
    }
    
    func progressBarChart(_ progressBarChart: ProgressBarChart, barAtIndex:Int) -> ProgressBarChart.Bar? {
        var legendaView: ProgressBarChartLabel?
        
        let dataForChart = convertInformationForChart(index: barAtIndex)
        
        if let legenda = nutritionalElementModel.dataCharts?[barAtIndex] {
            legendaView = ProgressBarChartLabel()
            legendaView?.label.textAlignment = .center
            legendaView?.minHeightWith(height: 4.0,
                                       text: legenda.label! ,
                                       numberOfLines: 1,
                                       font: UIFont(name: "Roboto-Regular", size: 11)!,
                                       color: NRColorUtility.hexStringToUIColor(hex: "808080"),
                                       verticalAlignment: ProgressBarChartLabel.LabelAlignment.bottom)
        }
        
        return (dataForChart.0, dataForChart.1 , legendaView as UIView?, ProgressBarChart.LegendaAlignment.center) as ProgressBarChart.Bar
    }
    
    private func getProportionWithChartLine() -> (Double, Double) {
        var maxValue = 0.0
        
        if nutritionalElementModel.dataCharts != nil {
            if nutritionalElementModel.dataCharts?[0].wholeGrainsConsumed != nil {
                let maxNutrienModelConsumed = nutritionalElementModel.dataCharts?.max(by: { $0.consumed + $0.wholeGrainsConsumed! < $1.consumed + $1.wholeGrainsConsumed!})
                maxValue = (maxNutrienModelConsumed?.consumed)! + (maxNutrienModelConsumed?.wholeGrainsConsumed)!
            } else {
                maxValue = (nutritionalElementModel.dataCharts?.max(by: { $0.consumed < $1.consumed })?.consumed)!
            }
        }
        
        var middleLine = 0.0
        
        if let target = nutritionalElementModel.target {
            middleLine = maxValue > target ? (target / maxValue) : 1
            maxValue = maxValue > target ? maxValue : target
        }
        
        
        return (middleLine, maxValue)
    }
    
    private func convertInformationForChart(index: Int) -> ([CGFloat], [UIColor]) {
        var resultProportion = [CGFloat()]
        var resultColor = [UIColor]()
        
        if nutritionalElementModel.dataCharts?[index].wholeGrainsConsumed != nil {
            resultProportion = [CGFloat(nutritionalElementModel.dataCharts![index].wholeGrainsConsumed! / self.proportionWithChart.1) + 0.03, CGFloat(nutritionalElementModel.dataCharts![index].consumed / self.proportionWithChart.1) + 0.03]
        } else {
            resultProportion = [CGFloat(nutritionalElementModel.dataCharts![index].consumed / self.proportionWithChart.1) + 0.03, 0]
        }
        
        if nutritionalElementModel.type == .grains {
            if nutritionalElementModel.dataCharts![index].consumed == 0.0 && nutritionalElementModel.dataCharts![index].wholeGrainsConsumed == 0.0 {
                resultColor = [NRColorUtility.progressNutritionMinColor(), UIColor.clear]

            } else {
                resultColor = [colorNutrition[nutritionalElementModel.type!]!, (colorNutrition[nutritionalElementModel.type!]?.withAlphaComponent(0.6))!]
            }
        } else {
            if nutritionalElementModel.dataCharts![index].consumed == 0.0 {
                resultColor = [NRColorUtility.progressNutritionMinColor(), UIColor.clear]
            } else if nutritionalElementModel.type != nil {
                resultColor = [colorNutrition[nutritionalElementModel.type!]!, UIColor.clear]
            } else {
                resultColor = [UIColor.clear, UIColor.clear]
            }
        }
        
        return (resultProportion, resultColor)
    }

}

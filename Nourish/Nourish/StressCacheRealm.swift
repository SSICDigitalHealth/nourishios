//
//  StressCacheRealm.swift
//  Nourish
//
//  Created by Nova on 2/23/17.
//  Copyright © 2017 SSIC. All rights reserved.
//

import UIKit
import RealmSwift

class StressCacheRealm: Object {
    dynamic var timeStamp : Date?
    dynamic var bpmValue : Double = 0.0
    dynamic var idString : String? = nil
    dynamic var stressLevel : String?
}

//
//  NRStressPresenter.swift
//  Nourish
//
//  Created by Vino (Vinodha) Sundaramoorthy on 12/5/16.
//  Copyright © 2016 SSIC. All rights reserved.
//

import UIKit
import RxSwift

class NRStressPresenter: BasePresenter {
    let interactor = NRStressInteractor.shared
    var stressView : NRStressProtocol!
    let mapperObject = NRStress_to_NRStressModel()
    let mapperModel = NRStressModel_to_NRStress()
    
    override func viewWillAppear(_ animated : Bool) {
        if stressView != nil {
            self.subscribtions.append(self.interactor.stressFor(date: Date()).throttle(0.5, scheduler: MainScheduler.instance).observeOn(MainScheduler.instance).subscribe(onNext: { [unowned self] stress in
                let model = self.mapperObject.transform(stress: stress)
                self.stressView?.setupWith(stressModel: model)
            }, onError: {error in}, onCompleted: {
                LogUtility.logToFile("Stress completed")
            }, onDisposed: {}))
            
            self.subscribtions.append(self.interactor.getLastCachedReading().subscribe(onNext: { [unowned self] cachedStress in
                print(cachedStress)
                print(stressLevel.stringFromEnum(level: cachedStress))
            }, onError: {error in}, onCompleted: {}, onDisposed: {}))
        }
    }
}

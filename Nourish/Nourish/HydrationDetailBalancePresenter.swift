//
//  HydrationDetailBalancePresenter.swift
//  Nourish
//
//  Created by Vlad Birukov on 16.06.17.
//  Copyright © 2017 SSIC. All rights reserved.
//

import Foundation
import RxSwift


class HydrationDetailBalancePresenter : BasePresenter, UITableViewDataSource, UITableViewDelegate {
    let objectToModelMapper = HydrationBalanceModel_to_HydrationBalanceViewModel()
    let interactor = HydrationDetailInteractor()
    var hydrationDetailView : HydrationDetailBalanceProtocol!
    var hydrationDetailModel = HydrationBalanceViewModel()
    var date: (Date, Date)?
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.getHydrationDetail()
    }
    
    private func getHydrationDetail() {
        if self.hydrationDetailView.config() != nil {
            self.date = self.hydrationDetailView.config()?.getDate()
        }
        
        if self.date != nil {
            self.subscribtions.append(self.interactor.execute(startDate: (self.date?.0)!, endDate: (self.date?.1)!).observeOn(MainScheduler.instance).subscribe(onNext: { [unowned self] hydrationDetail in
                self.hydrationDetailModel = self.objectToModelMapper.transform(model: hydrationDetail)
                self.hydrationDetailView.setHydrationMessage(message: self.hydrationDetailModel.message)
                self.hydrationDetailView.reloadTable()
                self.hydrationDetailView.stopActivityAnimation()
            }, onError: {error in
                self.hydrationDetailView.stopActivityAnimation()
                self.hydrationDetailView.parseError(error : error, completion: nil)
            }, onCompleted: {
            }, onDisposed: {
                self.hydrationDetailView.stopActivityAnimation()
            }))
        }
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return  (hydrationDetailModel.arrHydrationElement != nil &&  hydrationDetailModel.arrHydrationElement!.count > 0) ? 1 : 0
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 1
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        
        let  footerCell = tableView.dequeueReusableCell(withIdentifier: "cellHydrationDetail") as! HydrationDetailTableViewCell
        
        if self.date != nil {
            if self.daysBetweenDates(startDate: (self.date?.0)!, endDate: (self.date?.1)!) == 0 {
                footerCell.setupWithModel(model: hydrationDetailModel)
            }
        }

        return footerCell
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return  hydrationDetailModel.arrHydrationElement != nil ? hydrationDetailModel.arrHydrationElement!.count : 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cellHydrationDetail") as! HydrationDetailTableViewCell
        
        var isAmount = false
        
        if self.daysBetweenDates(startDate: (self.date?.0)!, endDate: (self.date?.1)!) == 0 {
            isAmount = true
        } else {
            isAmount = false
        }

        cell.setupWithModel(model: hydrationDetailModel, indexPath: indexPath, isAmount: isAmount)
        
        return cell
    }
    
    private func daysBetweenDates(startDate: Date, endDate: Date) -> Int
    {
        let calendar = Calendar.current
        let components = calendar.dateComponents([.day], from: startDate, to: endDate)
        return components.day!
    }
}

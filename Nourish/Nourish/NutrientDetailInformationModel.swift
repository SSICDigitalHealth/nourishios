//
//  NutrientDetailInformationModel.swift
//  Nourish
//
//  Created by Vlad Birukov on 11.08.17.
//  Copyright © 2017 SSIC. All rights reserved.
//

import Foundation

class NutrientDetailInformationModel: NSObject {
    var nutrientComeFrom = NutrientComeFromModel()
    var dataChart = NutrientChartModel()
    var message: [String]?
}

class NutrientChartModel: NSObject {
    var upperLimit: Double = 0.0
    var lowerLimit: Double = 0.0
    var unit: String = ""
    var dataCharts: [NutrientInformationChartModel]?
}

class NutrientInformationChartModel: NSObject {
    var label: Date?
    var consumed: Double = 0.0
    var wholeGrainsConsumed: Double?
}

class NutrientComeFromModel: NSObject {
    var caption: [String]?
    var nameFood: [[String]]?
}

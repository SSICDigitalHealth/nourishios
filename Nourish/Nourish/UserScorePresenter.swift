//
//  UserScorePresenter.swift
//  Nourish
//
//  Created by Vlad Birukov on 15.06.17.
//  Copyright © 2017 SSIC. All rights reserved.
//

import Foundation
import RxSwift



class UserScorePresenter: BasePresenter, ProgressBarChartDelegate {
    let objectToModelMapper = ScoreBreakDownModel_to_ScoreBreakDownViewModel()
    let interactor = ScoreBreakDownInteractor()
    var userScoreView: UserScoreProtocol!
    var userScoreModel = ScoreBreakDownViewModel()
    var proportionWithChart = (0.0 , 0.0, 0.0)
    var date: (Date, Date)?
    
    @IBOutlet weak var progressBarChart: ProgressBarChart!

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.getUserScore()
    }


    
    private func getUserScore() {
        if self.userScoreView.config() != nil {
            self.date = self.userScoreView.config()?.getDate()
        }
        
        if self.date != nil {
            self.subscribtions.append(self.interactor.execute(startDate: (self.date?.0)!, endDate: (self.date?.1)!).observeOn(MainScheduler.instance).subscribe(onNext: { [unowned self] scoreModel in
                self.userScoreModel = self.objectToModelMapper.transform(userModel: scoreModel)
                self.userScoreView.stopActivityAnimation()
                self.proportionWithChart = self.getProportionWithChartLine()
                self.progressBarChart.widthBarPattern = (0.9, 0)
                self.progressBarChart.reloadData(DataSource: self)
            }, onError: {error in
                self.userScoreView.stopActivityAnimation()
                self.userScoreView.parseError(error : error, completion: nil)
            }, onCompleted: {
            }, onDisposed: {
                self.userScoreView.stopActivityAnimation()
            }))
        }
    }
    
    func numberOfBars(in progressBarChart: ProgressBarChart) -> Int {
        return userScoreModel.historyScore.count
    }
    
    func numberOfAxis(in progressBarChart: ProgressBarChart) -> Int {
        return 3
    }
    
    func progressBarChart(_ progressBarChart: ProgressBarChart, axisAtIndex:Int) -> ProgressBarChart.Axis? {
        let type: ProgressBarChart.AxisType!
        let value: Double!
        var color = UIColor.gray
        var label: ProgressBarChartLabel?
        
        switch axisAtIndex {
        case 2:
            type = .dashed
            value = 1.0
            color = UIColor.black.withAlphaComponent(0.2)
            label = ProgressBarChartLabel()
            label?.minHeightWith(height: 10.0,
                                 text: String(format: "%.1f pt", proportionWithChart.1) ,
                                 numberOfLines: 1,
                                 font: UIFont(name: "Roboto-Regular", size: 12)!,
                                 color: NRColorUtility.hexStringToUIColor(hex: "808080"),
                                 verticalAlignment: ProgressBarChartLabel.LabelAlignment.top)
        case 1:
            type = .dashed
            value = proportionWithChart.0
            color = UIColor.black.withAlphaComponent(0.2)
            label = ProgressBarChartLabel()
            label?.minHeightWith(height: 10.0,
                                 text: String(format: "%.1f pt", proportionWithChart.2),
                                 numberOfLines: 1,
                                 font: UIFont(name: "Roboto-Regular", size: 12)!,
                                 color: NRColorUtility.hexStringToUIColor(hex: "808080"),
                                 verticalAlignment: ProgressBarChartLabel.LabelAlignment.top)
        default:
            type = .solid
            value = 0.0
            color = UIColor.clear
        }
        
        return (color, type, value, nil, label) as ProgressBarChart.Axis
    }
    
    func progressBarChart(_ progressBarChart: ProgressBarChart, barAtIndex:Int) -> ProgressBarChart.Bar? {
        var legendaView: ProgressBarChartLabel?
        
        legendaView = ProgressBarChartLabel()
        legendaView?.label.textAlignment = .center
        legendaView?.minHeightWith(height: 4.0,
                                   text: String(format: "%@", self.dateFormatter(viewList: userScoreModel.historyScore, index: barAtIndex)),
                                   numberOfLines: 2,
                                   font: UIFont(name: "Roboto-Regular", size: 11)!,
                                   color: NRColorUtility.hexStringToUIColor(hex: "808080"),
                                   verticalAlignment: ProgressBarChartLabel.LabelAlignment.bottom)
        
        let dataForChart = self.caclulaceProportion(data: userScoreModel.historyScore[barAtIndex].value)
        return ([dataForChart.0 , 0.0], [dataForChart.1, NRColorUtility.hexStringToUIColor(hex: "EC1C28")], legendaView as UIView?, ProgressBarChart.LegendaAlignment.center) as ProgressBarChart.Bar
    }
    
    private func getProportionWithChartLine() -> (Double, Double, Double) {
        var maxValue = 0.0
        var midline = 0.0
        var midleValue = 0.0

        if userScoreModel.historyScore.count > 0 {
            maxValue = (userScoreModel.historyScore.max(by: {$0.value < $1.value})?.value)!
            maxValue = self.calculateUpperLimit(number: maxValue)
            let sum = userScoreModel.historyScore.reduce(0, {$0 + $1.value})
            midleValue = sum / Double(userScoreModel.historyScore.count)
            
            midline = maxValue >  midleValue ? (midleValue / maxValue) : 1
        }
        
        return (midline, maxValue, midleValue)
    }
    
    private func caclulaceProportion(data: Double) -> (CGFloat, UIColor) {
        var color: UIColor = NRColorUtility.hexStringToUIColor(hex: "#BFBFBF")
        var result = 0.0
        if data == 0 {
            result = 0.03
        } else {
            result = data / proportionWithChart.1
            color = NRColorUtility.appBackgroundColor()
        }
        
        return (CGFloat(result), color)
    }
    
    private func dateFormatter(viewList: [historyScoreViewList], index: Int) -> String {
        let calendar = Calendar.current
        let dateFormatter = DateFormatter()
        let periodView = userScoreView.config()
        
        if let period = periodView {
            if period.period == .weekly {
                let component = calendar.dateComponents([.weekOfMonth], from: viewList[index].subtitle)
                if component.weekOfMonth == 2 || index == 0{
                    dateFormatter.dateFormat = "d\nMMM"
                } else {
                    dateFormatter.dateFormat = "d"
                }
            } else {
                let component = calendar.dateComponents([.month], from: viewList[index].subtitle)
                if component.month == 1 {
                   dateFormatter.dateFormat = "MMM\nyyyy"
                } else {
                    dateFormatter.dateFormat = "MMM"
                }
            }
        }
        return dateFormatter.string(from: viewList[index].subtitle).uppercased()
    }
    
    private func calculateUpperLimit(number: Double) -> Double {
        var upperLimit = 0.0
        
        if number == 0.0 || number < 40 {
            upperLimit = 40
        } else if number < 60 && number >= 40 {
            upperLimit = 60
        } else if number < 80 && number >= 60 {
            upperLimit = 80
        } else if number >= 80 {
            upperLimit = 100
        }
        return upperLimit
    }
}

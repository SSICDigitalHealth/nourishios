//
//  UserOcasionTableViewCell.swift
//  Nourish
//
//  Created by Vlad Birukov on 19.06.17.
//  Copyright © 2017 SSIC. All rights reserved.
//

import UIKit

class UserOcasionTableViewCell: UITableViewCell {

    @IBOutlet weak var nameFood: UILabel!
    @IBOutlet weak var bottomConstraint: NSLayoutConstraint!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func setupWithModel(model: FoodElementViewModel) {
        if model.amountFood != nil && model.unit != nil {
            nameFood.text = String(format: "%d %@ %@", Int(model.amountFood!), model.unit!, model.nameFood)
            nameFood.setLineSpacing(spacing: 4)
        } else {
            nameFood.text = String(format: "%@", model.nameFood)
            nameFood.setLineSpacing(spacing: 4)
        }
        
    }
}

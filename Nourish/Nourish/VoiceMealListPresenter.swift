//
//  VoiceMealListPresenter.swift
//  Nourish
//
//  Created by Vino (Vinodha) Sundaramoorthy on 7/1/17.
//  Copyright © 2017 SSIC. All rights reserved.
//

import UIKit

class VoiceMealListPresenter: BasePresenter , UITableViewDelegate,UITableViewDataSource,NRPickerViewDelegate,NRPickerViewDataSource {
    
    var mealRecordArray : [MealRecord] = []
    var ocassion : Ocasion!
    var mealListView : VoiceMealListProtocol?
    var ocassionArray : [Ocasion] = [.breakfast,.lunch,.dinner,.snacks]
    var measureArray : [String : [String]] = [:]
    var interactor = VoiceInteractor()
    var foodInteractor = NRFoodDetailsInteractor()
    var toFoodSearchMapper = MealRecord_to_foodSearchModel()
    
    override func viewWillAppear(_ animated: Bool) {
        var index = 0
        
        if self.ocassion != nil {
            index = self.ocassionArray.index(of: self.ocassion)!
        }
        self.mealListView?.setUpInitialView(ocasion:index)
    }

    // MARK : UITableViewDataSource Implementation
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return mealRecordArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell : VoiceMealItemCellTableViewCell = tableView.dequeueReusableCell(withIdentifier: "kVoiceMealCell") as! VoiceMealItemCellTableViewCell
        
        let data = mealRecordArray[indexPath.row]
        
        //Calculate serving type index
        var servingType : String = data.unit!
        let servingArray : [SHFoodUnit] = data.soundHoundFoodUnit
        if data.unit == nil {
            servingType = (servingArray[0].servingType)
        }
        let servingArrayindex = servingArray.index(where: {$0.servingType == data.unit})
        let selectedServingType = servingArray[servingArrayindex!]

        // Calculate measure index
        self.populateServingAmountDataSource(servingType: servingType, foodid: data.sourceFoodDBID,changeAmount:false,selectedItem:SHFoodUnit())
        var measureIndex = 0
        let measureSource = self.measureArray[data.sourceFoodDBID]!
        measureIndex = measureSource.index(of: self.getMeasure(number: data.amount))!
        data.calories =  data.caloriesPerGramm * (data.amount * (selectedServingType.grams))
        data.grams = data.amount * (selectedServingType.grams)
        cell.setUpMealCell(model: data)
        cell.servingTypePicker.delegate = self
        cell.servingTypePicker.dataSource = self
        cell.measurePicker.delegate = self
        cell.measurePicker.dataSource = self
        cell.servingTypePicker.selectItem(servingArrayindex!, animated: false, notifySelection: false)
        cell.measurePicker.selectItem(measureIndex, animated: false, notifySelection: false)
        cell.deleteButton.addTarget(self, action: #selector(self.deleteMeal(sender:)), for: .touchUpInside)
        return cell
    }
    
    // MARK : NRPickerViewDataSource 
    
    func pickerView(_ pickerView: NRPickerView, didSelectItem item: Int) {
        if pickerView.tag == 10 {
            let cellView = pickerView.superview?.superview as! VoiceMealItemCellTableViewCell
            let cellData = cellView.mealModel
            let servingType = cellData?.soundHoundFoodUnit
            let selectedItem = servingType?[item]
            let selectedMealRecord = self.mealRecordArray.filter{ $0.sourceFoodDBID == cellData?.sourceFoodDBID
            }
            
            if selectedMealRecord.count > 0 {
                let meal = selectedMealRecord[0] 
                meal.unit = selectedItem?.servingType
                meal.calories =   meal.caloriesPerGramm * (meal.amount * (selectedItem?.grams)!)
                meal.grams = meal.amount * (selectedItem?.grams)!
            }
            
            //Reload the measure picker based on the selected serving type
            self.populateServingAmountDataSource(servingType: (selectedItem?.servingType)!, foodid: (cellData?.sourceFoodDBID)!,changeAmount:true,selectedItem: selectedItem!)
            
            //Reload the table
            self.mealListView?.reloadCell(cell:cellView)
            
        } else if pickerView.tag == 20 {
            let cellView = pickerView.superview?.superview as! VoiceMealItemCellTableViewCell
            let cellData = cellView.mealModel
            let meassure = self.measureArray[(cellData?.sourceFoodDBID)!]
            let selectedItem = meassure?[item]
            let servingType = cellData?.soundHoundFoodUnit
            

            let selectedMealRecord = self.mealRecordArray.filter{ $0.sourceFoodDBID == cellData?.sourceFoodDBID
            }
            
            if selectedMealRecord.count > 0 {
                let meal = selectedMealRecord[0]
                meal.amount = self.getMeasure(meassure: selectedItem!)
                
                let type = servingType?.filter{$0.servingType == cellData?.unit}
                if (type?.count)! > 0 {
                    meal.calories = ((type?[0].grams)! * meal.amount) * meal.caloriesPerGramm
                    meal.grams = (meal.amount * (type?[0].grams)!)
                }
            }
            //Reload the table
            self.mealListView?.reloadCell(cell:cellView)
            
        } else {
            let ocassion = self.ocassionArray[item]
            
            for meal in self.mealRecordArray {
                meal.ocasion = ocassion
            }
        }
    }
    
    func pickerView(_ pickerView: NRPickerView, titleForItem item: Int) -> String {
        
        //Serving type picker
        if pickerView.tag == 10 {
            let cellView = pickerView.superview?.superview as! VoiceMealItemCellTableViewCell
            let cellData = cellView.mealModel
             let servingTypeArray = cellData?.soundHoundFoodUnit
            return servingTypeArray![item].servingType
        } else if pickerView.tag == 20 {
            let cellView = pickerView.superview?.superview as! VoiceMealItemCellTableViewCell
            let cellData = cellView.mealModel
            let measureArray = self.measureArray[(cellData?.sourceFoodDBID)!]
            return measureArray![item]
        } else {
            return  Ocasion.stringDescription(servingType: ocassionArray[item])
        }
    }
    
    func numberOfItemsInPickerView(_ pickerView: NRPickerView) -> Int {
        
        if pickerView.tag == 10 {
            let cellView = pickerView.superview?.superview as! VoiceMealItemCellTableViewCell
            let cellData = cellView.mealModel

             let servingTypeArray = cellData?.soundHoundFoodUnit
             return servingTypeArray!.count
        } else if pickerView.tag == 20 {
            let cellView = pickerView.superview?.superview as! VoiceMealItemCellTableViewCell
            let cellData = cellView.mealModel
            let measureArray = self.measureArray[(cellData?.sourceFoodDBID)!]
            return (measureArray?.count)!
        } else {
            return self.ocassionArray.count
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        let cell = self.tableView(tableView, cellForRowAt: indexPath)
        return cell.frame.size.height
    }
    
    func populateServingAmountDataSource(servingType : String , foodid:String ,changeAmount:Bool,selectedItem:SHFoodUnit)  {
        var measureArray : [String] = []
        if (servingType.contains("bowl")) || (servingType.contains("cup")) || (servingType.contains("tsp")) || (servingType.contains("pie")) || (servingType.contains("slice")) || (servingType.contains("tbsp")) || (servingType.contains("mug")) || (servingType.contains("pizza")) || (servingType.contains("serving")) || (servingType.contains("apple")) {
           measureArray = self.measureDataSource(start: 0.25, end: 10, step: 0.25)
        } else if servingType == "g" {
          measureArray =  self.measureDataSource(start: 10, end: 2000, step: 10)
        } else if servingType == "oz" || servingType == "fl oz" {
          measureArray = self.measureDataSource(start:1, end: 50, step: 1)
        } else if servingType == "ml" {
          measureArray =  self.measureDataSource(start: 5, end: 5000, step: 5)
        } else if servingType == "calories" {
          measureArray =  self.measureDataSource(start: 10, end: 4000, step: 10)
        } else if servingType == "kj" {
           measureArray = self.measureDataSource(start: 10, end: 1000, step: 10)
        } else {
           measureArray = self.measureDataSource(start: 1, end: 10, step: 1)
        }
        
        self.measureArray[foodid] = measureArray
        
        if changeAmount {
            let mealRecord = self.mealRecordArray.filter{$0.sourceFoodDBID == foodid}
            if mealRecord.count > 0 {
                mealRecord[0].amount = self.getMeasure(meassure: measureArray[0])
                mealRecord[0].calories =   mealRecord[0].caloriesPerGramm * (mealRecord[0].amount * (selectedItem.grams))
                mealRecord[0].grams = mealRecord[0].amount * (selectedItem.grams)
            }
        }
    }
    
    
    func measureDataSource(start:Double , end:Double , step:Double) -> [String] {
        var measureArray : [String] = []
        for index in stride(from: start, through: end, by: step) {
            measureArray.append(self.getMeasure(number: index))
        }
        return measureArray
    }
    
    func getMeasure(meassure : String) -> Double {
        let str = String(meassure).components(separatedBy: " ")
        var servingAmount : Double = 0
        
        if str.count > 1 {
            let amount = str[0] == "" ? 0 : Double(str[0])
            if str[1] == "1/2" {
                servingAmount = amount! + 0.50
            }else if str[1] == "1/4" {
                servingAmount = amount! + 0.25
            }else if str[1] == "3/4" {
                servingAmount = amount! + 0.75
            } else {
                servingAmount = Double(str[0])!
            }
        } else {
            servingAmount = Double(meassure)!
        }
        return servingAmount
    }
    
    func getMeasure(number : Double) -> String {
        let str = String(number).components(separatedBy: ".")
        var newString = ""
        
        if str.count > 1 {
            if str[1] == "25" {
                newString = String(format: "%@ 1/4", str[0] == "0" ? "" : str[0])
            } else if str[1] == "5" {
                newString = String(format: "%@ 1/2", str[0] == "0" ? "" : str[0])
            } else if str[1] == "75" {
                newString = String(format: "%@ 3/4", str[0] == "0" ? "" : str[0])
            } else {
                newString = String(format: "%@", str[0] == "0" ? "" : str[0])
            }
        }
        return newString
    }
    
    func deleteMeal(sender : UIButton) {
        let cell = sender.superview?.superview?.superview as! VoiceMealItemCellTableViewCell
        let data = cell.mealModel
        let index = self.mealRecordArray.index(where: {$0.sourceFoodDBID == data?.sourceFoodDBID})
        self.mealRecordArray.remove(at: index!)
        
        if self.mealRecordArray.count > 0 {
            self.mealListView?.reloadTableView()
        } else {
            self.mealListView?.dismiss()
        }
    }
    
    func addToDiary() {
        if self.mealRecordArray.count > 0 {
            self.mealListView?.startActivityAnimation()
            for meal in self.mealRecordArray {
                let _ = self.interactor.uploadMeal(meal: meal).subscribe(onNext: {score in
                }, onError: {error in
                    print(error)
                    DispatchQueue.main.async {
                        self.mealListView?.parseError(error: error, completion: nil)
                    }
                }, onCompleted: {
                    EventLogger.logSaveFoodWithVoiceSearch()
                }, onDisposed: {})
            }
        }
        self.mealListView?.finishSave()
    }
}

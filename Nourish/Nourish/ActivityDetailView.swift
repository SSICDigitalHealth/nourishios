//
//  ActivityDetailView.swift
//  Nourish
//
//  Created by Vlad Birukov on 24.08.17.
//  Copyright © 2017 SSIC. All rights reserved.
//

import UIKit

protocol ActivityDetailProtocol  : BaseViewProtocol {
    func reloadData()
    func reloadCell(indexPath: IndexPath)
}

class ActivityDetailView: BaseView, ActivityDetailProtocol {
    @IBOutlet weak var detailTable: UITableView!
    var presenter: ActivityDetailPresenter!
    var activityDetailDelegate: ActivityDetailDelegate?
    
    override func viewDidLoad() {
        self.xibAutoLayouting(nibName: "ActivityDetailView")
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.basePresenter = presenter
        self.presenter.activityDetailView = self
        self.detailTable.delegate = presenter
        self.detailTable.dataSource = presenter
        self.settingTableCell()
        super.viewWillAppear(animated)
        self.stopActivityAnimation()
    }
    
    func reloadCell(indexPath: IndexPath) {
        if indexPath.row < self.detailTable.numberOfRows(inSection: 0) {
            print("---->Reloaded table cell")
            //self.detailTable.reloadRows(at: [indexPath], with: .none)
            self.detailTable.reloadData()
        }
    }
    
    func reloadData() {
        self.detailTable.reloadData()
    }
    
    private func settingTableCell() {
        self.detailTable.rowHeight = UITableViewAutomaticDimension
        self.detailTable.estimatedRowHeight = 40

    }
    
}

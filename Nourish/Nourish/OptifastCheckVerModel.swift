//
//  OptifastCheckVerModel.swift
//  Optifast
//
//  Created by Gena Mironchyk on 2/28/18.
//  Copyright © 2018 SSIC. All rights reserved.
//

import Foundation

class OptifastCheckVerModel {
    var messageTitle = ""
    var messageText = ""
    var buttonText = ""
    var detailsUrl = ""
    var version = ""
}

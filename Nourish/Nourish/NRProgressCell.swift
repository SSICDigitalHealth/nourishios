//
//  NRProgressCell.swift
//  Nourish
//
//  Created by Vino (Vinodha) Sundaramoorthy on 12/7/16.
//  Copyright © 2016 SSIC. All rights reserved.
//

import UIKit

class NRProgressCell: UITableViewCell {
    @IBOutlet weak var titleLabel : UILabel!
    @IBOutlet weak var bgView : UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.setBgView()
        self.backgroundColor = NRColorUtility.nourishHomeColor()
        if titleLabel != nil {
            titleLabel.textColor = NRColorUtility.progressLabelColor()
        }
    }
    
    func setBgView() {
        let radius: CGFloat = bgView.frame.width / 2.0
        let shadowPath = UIBezierPath(rect: CGRect(x: 0, y: 0, width: 1.6 * radius, height: bgView.frame.height))
        bgView.layer.cornerRadius = 2
        bgView.layer.shadowColor = UIColor.gray.cgColor
        bgView.layer.shadowOffset = CGSize(width: 0.1, height: 0.2)
        bgView.layer.shadowOpacity = 0.5
        bgView.layer.shadowRadius = 5.0
        bgView.layer.masksToBounds =  false
        bgView.layer.shadowPath = shadowPath.cgPath
    }

}

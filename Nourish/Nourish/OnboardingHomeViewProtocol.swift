//
//  OnboardingHomeViewProtocol.swift
//  Nourish
//
//  Created by Gena Mironchyk on 2/21/17.
//  Copyright © 2017 SSIC. All rights reserved.
//

import UIKit

protocol OnboardingHomeViewProtocol {
    func onboardingCompleted ()
    func hideMenu()
    func showScoreFeedback(date : Date)
    func openFitBitLogin()
    func showGoalChangedInfo()
}

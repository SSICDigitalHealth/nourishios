//
//  CaloriesIntakeMealsViewModel.swift
//  Nourish
//
//  Created by Vlad Birukov on 29.08.17.
//  Copyright © 2017 SSIC. All rights reserved.
//

import Foundation

class CaloriesIntakeMealsViewModel: NSObject {
    var arrCaloriesMeal = [CaloriesMealViewModel]()
}

class CaloriesMealViewModel {
    var type: Ocasion = .breakfast
    var percent: Double = 0.0
}

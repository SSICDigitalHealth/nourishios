//
//  NRWeightProgress_toNRWeightProgressModel.swift
//  Nourish
//
//  Created by Vino (Vinodha) Sundaramoorthy on 12/6/16.
//  Copyright © 2016 SSIC. All rights reserved.
//

import UIKit

class NRWeightProgress_toNRWeightProgressModel: NSObject {
    func transform(weight : NRWeightProgress) -> NRWeightProgressModel {
        let object = NRWeightProgressModel()
        object.currentWeight = weight.currentWeight
        object.progressPercentage = weight.progressPercentage
        object.metrics = weight.metrics
        object.weightProgress = weight.weightProgress
        object.dailyWeightLogged = weight.dailyWeightLogged
        object.userId = weight.userId
        
        
        
        return object
    }
    
    func transform(modelArray : [NRWeightProgress]) -> [NRWeightProgressModel] {
        var arrayToReturn : [NRWeightProgressModel] = []
        for model in modelArray {
            arrayToReturn.append(self.transform(weight: model))
        }
        return arrayToReturn
    }
}

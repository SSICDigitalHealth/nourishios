//
//  UploadFoodWithPhotoInteractor.swift
//  Nourish
//
//  Created by Nova on 5/12/17.
//  Copyright © 2017 SSIC. All rights reserved.
//

import UIKit
import RxSwift
import RealmSwift

class UploadFoodWithPhotoInteractor: NSObject {
    let mealRepo = MealHistoryRepository.shared
    let foodInteractor = NRFoodDetailsInteractor()
    let imageRepo = ImageRepository()
    let repo = FavoritesRepository()
    let chat = ChatBoxRepository()
    let mapper = MealRecord_to_MealRecordCache()
    let realmRepo = FoodRecordRepository.shared
    let routine = NMFood_to_MealRecordRoutine()
    
    
    func execute(models : [foodSearchModel], photo : UIImage, ocasion : Ocasion, date : Date) -> Observable<Bool> {
        if models.count > 1 {
            return self.asyncAddGroupToDiary(foods:models, photo:photo, ocasion:ocasion, date:date)//self.addGroupToDiary(foods: models, photo: photo, ocasion: ocasion, date: date)
        } else {
            let food = models.first
            if food != nil {
                return self.addToDiarySingleFood(food : food!, photo : photo, ocasion : ocasion, date : date)
            } else {
                return Observable.just(false)
            }
           
        }
    }
    
    /*
    private func addToDiarySingleFood(food : foodSearchModel, photo : UIImage, ocasion : Ocasion, date : Date) -> Observable<Bool> {
        var foodIDToStore = ""
        let seq = self.foodInteractor.addFnddsToDiaryAndGetFoodID(model: food, occasion: ocasion, date: date).flatMap { foodID -> Observable<String> in
            foodIDToStore = foodID
            return self.imageRepo.uploadPhoto(image: photo, userfoodID: foodID, groupID: nil)
        }.flatMap { photoID -> Observable<Bool> in
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
                try! Realm().write {
                    let chatMessage = self.chat.getLastRecordWithContent(content: food.foodTitle)
                    if chatMessage != nil {
                        chatMessage?.photoID = photoID
                    }
                }
                self.realmRepo.addToCounted(model: self.populate(model: food, ocasion: ocasion, photoID : photoID, groupID: foodIDToStore))
                
            }

            return Observable.just(photoID.characters.count > 0 ? true : false)
        }
        
        return seq
    }
    */
    
    private func addToDiarySingleFood(food : foodSearchModel, photo : UIImage, ocasion : Ocasion, date : Date) -> Observable<Bool> {
        let obj = self.routine.recomendationFoodToMeal(model: food, date: date, occasion: ocasion)
        return self.mealRepo.upload(mealRecord: obj, photo: photo)
    }
    
    private func populate(model : foodSearchModel, ocasion : Ocasion, photoID : String, groupID : String) -> foodSearchModel {
        var copy = model
        copy.userPhotoId = photoID
        copy.occasion = ocasion
        //copy.groupID = groupID
        return copy
    }
    
    private func asyncAddGroupToDiary (foods : [foodSearchModel], photo : UIImage, ocasion : Ocasion, date : Date) -> Observable<Bool> {
        return self.mealRepo.uploadGroup(foods:foods,photo:photo,ocasion:ocasion, date:date)
    }
    
    private func addGroupToDiary(foods : [foodSearchModel], photo : UIImage,ocasion : Ocasion, date : Date) -> Observable<Bool> {
        var name : String = ""
        var cache : [foodSearchModel] = []
        
        for food in foods {
            var foodCache = food
            foodCache.occasion = ocasion
            cache.append(foodCache)
            name = name + food.foodTitle + ","
        }
        
        name = String(name.dropLast())
 /*
        var obsArray : [Observable<MealRecord>] = []
        
        for food in foods {
            obsArray.append(self.foodInteractor.fetchFoodRecordMeal(foodSearchModel: food, occasion: ocasion, date: date))
        }
        
        let singleObs = Observable.from(obsArray).merge()
        let seq = singleObs.toArray()
        
 */
        var array = [MealRecordCache]()
        
        for food in foods {
            let obj = self.routine.recomendationFoodToMeal(model: food, date: date ,occasion : ocasion)
            let meal = self.mapper.transform(record: obj)
            array.append(meal)
        }
        
        let userID = NRUserSession.sharedInstance.userID ?? ""
        
        let some = self.repo.addToDiaryWithPhoto(models: array, name: name, ocasion: ocasion, date: date, userID: userID).flatMap { groupID -> Observable<String> in
            let _ = self.repo.addToDiary(foodGroupd: groupID, groupName: name, occasion: ocasion, date: date, models: foods).subscribe(onNext : {added in
                print("groupd added \(added)")
            })
            return self.imageRepo.uploadPhoto(image: photo, userfoodID: nil, groupID: groupID)
        }.flatMap { photoID -> Observable<Bool> in
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.3) {
                try! Realm().write {
                    let chatMessage = self.chat.getLastRecordWithContent(content: name)
                    if chatMessage != nil {
                        chatMessage?.photoID = photoID
                    }
                }
            }
            return Observable.just(photoID.count > 0 ? true : false)
        }
        return some
    }
}


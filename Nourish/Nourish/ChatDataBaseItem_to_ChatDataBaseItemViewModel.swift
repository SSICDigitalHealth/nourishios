//
//  ChatDataBaseItem_to_ChatDataBaseItemViewModel.swift
//  Nourish
//
//  Created by Gena Mironchyk on 7/31/17.
//  Copyright © 2017 SSIC. All rights reserved.
//

import UIKit

class ChatDataBaseItem_to_ChatDataBaseItemViewModel: NSObject {
    func transform(dataBaseItem : ChatDataBaseItem) -> ChatDataBaseItemViewModel? {
        if !dataBaseItem.isInvalidated {
            let model = ChatDataBaseItemViewModel()
            model.date = dataBaseItem.date
            model.chatBoxType = dataBaseItem.chatBoxType
            model.content = dataBaseItem.content
            model.isFromUser = dataBaseItem.isFromUser
            model.messageID = dataBaseItem.messageID
            model.userID = dataBaseItem.userID
            model.lostWeightValue = dataBaseItem.lostWeightValue
            model.targetLostWeightValue = dataBaseItem.targetLostWeightValue
            model.photoID = dataBaseItem.photoID
            return model
        } else {
            return nil
        }
    }
    
    func transform(items : [ChatDataBaseItem]) -> [ChatDataBaseItemViewModel] {
        var arrayToReturn : [ChatDataBaseItemViewModel] = []
        for item in items {
            let viewModel = self.transform(dataBaseItem:item)
            if viewModel != nil {
                arrayToReturn.append(viewModel!)
            }
        }
        return arrayToReturn
    }
}

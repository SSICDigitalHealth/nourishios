//
//  CaloriesIntakeMealsModel.swift
//  Nourish
//
//  Created by Vlad Birukov on 20.06.17.
//  Copyright © 2017 SSIC. All rights reserved.
//

import Foundation


class CaloriesIntakeMealsModel: NSObject {
    var arrCaloriesMeal = [CaloriesMealModel]()
}

class CaloriesMealModel {
    var type: Ocasion = .breakfast
    var percent: Double = 0.0
}

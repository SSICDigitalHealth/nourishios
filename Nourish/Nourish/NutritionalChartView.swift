//
//  NutritionalChartView.swift
//  Nourish
//
//  Created by Andrey Zmushko on 7/27/17.
//  Copyright © 2017 SSIC. All rights reserved.
//

import UIKit


class NutritionalChartView: UIView {
    
    public typealias Data = [(type: NutritionType, consumed: Double, target: Double)]
    var data: Data?
    var radiusTarget: CGFloat = 144.0 / 2.0
    var centerView = CGPoint()
    
    var radiusDairy = CGFloat()
    var radiusVegs = CGFloat()
    var radiusFruits = CGFloat()
    var radiusGrains = CGFloat()
    var radiusProtein = CGFloat()

    func setupWith(Data data: Data) {
        self.data = data
        self.setNeedsDisplay()
    }
    
    private func parseData(_ type: NutritionType) -> (Double, Double) {
        if let data = self.data {
            var val: (type: NutritionType, consumed: Double, target: Double)? = data.filter{ $0.type == type }.first
            if val != nil {
                if val!.consumed < 0 {
                    val!.consumed = 0
                }
                if val!.target <= 0 {
                    val!.target = Double.leastNonzeroMagnitude
                }
                return (val!.consumed, val!.target)
            }
        }
        return (0.0, 1.0)
    }
    
    override func draw(_ rect: CGRect) {
        self.centerView.x = self.bounds.width / 2
        self.centerView.y = self.bounds.height / 2
        
        var emptyConsumedValues: Bool {
            if let data = self.data {
                for value in data {
                    if value.consumed != 0 {
                        return false
                    }
                }
            }
            return true
        }
        
        if emptyConsumedValues {
            drawEmptyChart()
        }
        else {
            drawRightChart(grains: parseData(.grains), protein: parseData(.protein))
            drawLeftChart(fruits: parseData(.fruits), vegs: parseData(.vegs), dairy: parseData(.dairy))
        }
    }
    
    private func drawEmptyChart() {
        var totalRight: Double {
            return parseData(.grains).1 + parseData(.protein).1
        }
        var totalLeft: Double {
            return parseData(.fruits).1 + parseData(.vegs).1 + parseData(.dairy).1
        }
        var dairyAngle: CGFloat {
            return CGFloat(180 * parseData(.dairy).1 / totalLeft)
        }
        var vegsAngle: CGFloat {
            return CGFloat(180 * parseData(.vegs).1 / totalLeft)
        }
        self.drawSector(fillColor: NRColorUtility.hexStringToUIColor(hex: "A64DB6").withAlphaComponent(0.2),
                                    center: self.centerView,
                                    radius: self.radiusTarget,
                                    startAngle: 90 - CGFloat(180 * parseData(.protein).1 / totalRight),
                                    endAngle: 90,
                                    clockwise: true)
        self.drawSector(fillColor: NRColorUtility.hexStringToUIColor(hex: "1172B9").withAlphaComponent(0.2),
                                    center: self.centerView,
                                    radius: self.radiusTarget,
                                    startAngle: 90,
                                    endAngle: 90 + dairyAngle,
                                    clockwise: true)
        self.drawSector(fillColor: NRColorUtility.hexStringToUIColor(hex: "70C397").withAlphaComponent(0.2),
                                    center: self.centerView,
                                    radius: self.radiusTarget,
                                    startAngle: 90 + dairyAngle,
                                    endAngle: 90 + dairyAngle + vegsAngle,
                                    clockwise: true)
        self.drawSector(fillColor: NRColorUtility.hexStringToUIColor(hex: "f6321d").withAlphaComponent(0.2),
                                    center: self.centerView,
                                    radius: self.radiusTarget,
                                    startAngle: 90 + dairyAngle + vegsAngle,
                                    endAngle: 270,
                                    clockwise: true)
        self.drawSector(fillColor: NRColorUtility.hexStringToUIColor(hex: "F0C517").withAlphaComponent(0.2),
                                    center: self.centerView,
                                    radius: self.radiusTarget,
                                    startAngle: 270,
                                    endAngle: 270 + CGFloat(180 * parseData(.grains).1 / totalRight),
                                    clockwise: true)
    }
    
    private func calculateRadius(_ element: (Double, Double)) -> CGFloat {
        return sqrt(CGFloat(element.0) * pow(self.radiusTarget, 2) / CGFloat(element.1))
    }
    
    private func drawLeftChart(fruits: (Double, Double), vegs: (Double, Double), dairy: (Double, Double)) {
        if fruits.1 == 0 && vegs.1 == 0 && dairy.1 == 0 {
            return
        }
        let dairyEnd = 180 * CGFloat(dairy.1 / (fruits.1 + vegs.1 + dairy.1)) + 90
        if dairy.0 >= 0 && dairy.1 > 0 {
            self.radiusDairy = calculateRadius(dairy)
            drawChartSector(fillColor: NRColorUtility.hexStringToUIColor(hex: "1172B9").withAlphaComponent(0.8),
                            radius: self.radiusDairy,
                            startAngle: 90.0,
                            endAngle: dairyEnd,
                            clockwise: true)
        }
        let vegsEnd = 180 * CGFloat(vegs.1 / (fruits.1 + vegs.1 + dairy.1)) + dairyEnd
        if vegs.0 >= 0 && vegs.1 > 0 {
            self.radiusVegs = calculateRadius(vegs)
            drawChartSector(fillColor: NRColorUtility.hexStringToUIColor(hex: "70C397"),
                            radius: self.radiusVegs,
                            startAngle: dairyEnd,
                            endAngle: vegsEnd,
                            clockwise: true)
        }
        if fruits.0 >= 0 && fruits.1 > 0 {
            self.radiusFruits = calculateRadius(fruits)
            drawChartSector(fillColor: NRColorUtility.hexStringToUIColor(hex: "f6321d"),
                            radius: self.radiusFruits,
                            startAngle: vegsEnd,
                            endAngle: 270.0,
                            clockwise: true)
        }
    }
    
    private func drawRightChart(grains: (Double, Double), protein: (Double, Double)) {
        if grains.1 == 0 && protein.1 == 0 {
            return
        }
        let start = 180 * CGFloat(grains.1 / (grains.1 + protein.1)) - 90
        if grains.0 >= 0 && grains.1 > 0 {
            self.radiusGrains = calculateRadius(grains)
            drawChartSector(fillColor: NRColorUtility.hexStringToUIColor(hex: "F0C517"),
                            radius: self.radiusGrains,
                            startAngle: start,
                            endAngle: -90.0,
                            clockwise: false)
        }
        if protein.0 >= 0 && protein.1 > 0 {
            self.radiusProtein = calculateRadius(protein)
            drawChartSector(fillColor: NRColorUtility.hexStringToUIColor(hex: "A64DB6"),
                            radius: self.radiusProtein,
                            startAngle: start,
                            endAngle: 90.0,
                            clockwise: true)
        }
    }
    
    private func drawChartSector(fillColor: UIColor, radius: CGFloat, startAngle: CGFloat, endAngle: CGFloat, clockwise: Bool) {
        if radius > self.radiusTarget {
            self.drawSector(fillColor: fillColor,
                                        center: self.centerView,
                                        radius: radius,
                                        startAngle: startAngle,
                                        endAngle: endAngle,
                                        clockwise: clockwise)
            self.drawSector(fillColor: UIColor.black.withAlphaComponent(0.14),
                                        center: self.centerView,
                                        radius: radius,
                                        startAngle: startAngle,
                                        endAngle: endAngle,
                                        clockwise: clockwise)
            self.drawSector(fillColor: fillColor,
                                        center: self.centerView,
                                        radius: self.radiusTarget,
                                        startAngle: startAngle,
                                        endAngle: endAngle,
                                        clockwise: clockwise)
        }
        else {
            let targetColor = NRColorUtility.hexStringToUIColor(hex: "BFBFBF").withAlphaComponent(0.25)
            self.drawSector(fillColor: targetColor,
                                        center: self.centerView,
                                        radius: self.radiusTarget,
                                        startAngle: startAngle,
                                        endAngle: endAngle,
                                        clockwise: clockwise)
            self.drawSector(fillColor: fillColor,
                                        center: self.centerView,
                                        radius: radius,
                                        startAngle: startAngle,
                                        endAngle: endAngle,
                                        clockwise: clockwise)
        }
    }
}

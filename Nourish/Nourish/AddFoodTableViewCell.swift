//
//  AddFoodTableViewCell.swift
//  Nourish
//
//  Created by Nova on 11/11/16.
//  Copyright © 2016 Samsung. All rights reserved.
//

import UIKit
import QuartzCore

enum favouriteAction : Int {
    case groupFav = 1
    case fav
    case none
}

class AddFoodTableViewCell: UITableViewCell {
    
    @IBOutlet var ocasionTitle : UILabel?
    @IBOutlet var addButton : UIButton?
    @IBOutlet var actionButton : UIButton? //Used to mark as favourite / group fav
    var ocasion : Ocasion? = nil
    var favAction : favouriteAction = .none
    
    override func awakeFromNib() {
        super.awakeFromNib()
        let image = #imageLiteral(resourceName: "addmeal").withRenderingMode(.alwaysTemplate)
        self.addButton?.setImage(image, for: .normal)
        self.addButton?.imageView?.tintColor = NRColorUtility.appBackgroundColor()
        actionButton?.isHidden = true
    }
    
    func setFavAction(action : favouriteAction) {
        if action == .groupFav {
            actionButton?.isHidden = false
            actionButton?.setImage(UIImage(named:"fav_grp"), for: .normal)
        } else if action == .fav {
            actionButton?.isHidden = false
            actionButton?.setImage(UIImage(named:"fav_sgl"), for: .normal)
        } else {
            actionButton?.isHidden = true
        }
    }
    
}

//
//  SENTInitIssue.h
//  ALSDK
//
//  Created by Gustavo Nascimento on 1/9/17.
//  Copyright © 2017 Argus Labs. All rights reserved.
//

typedef NS_ENUM(NSUInteger, SENTInitIssue) {
    SENTInitIssueInvalidCredentials = 1,
    SENTInitIssueChangedCredentials = 2,
    SENTInitIssueServiceUnreachable = 3
};
